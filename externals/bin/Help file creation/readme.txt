Help with Depiction Help
Kent Diamond, December 2008

Files in this directory are used to modify the helpfile in Depiction. The tutorial directory has a tutorial on HTML Help. The binary file had binaries for the HHW application.

Long ago (Win95/98) the main Microsoft help file format was WinHelp which built a help file from RTF files. In the mid-90s Microsoft began a massive shift towards the Internet. Part of the shift was a growing reliance on HTML. HTML was appearing everywhere. The help files were no exception. Modern (post mid-90s) help files are what Microsoft calls "HTML Help files" and are built from HTML sources. 

The Depiction Help file is a "HTML Help files". The help file is DepictionHelp.chm which is installed in c:\program files\Depiction Inc\Depiction\Docs. 

The sources for the Depiction Help File live in \depiction\geo\source\Docs\Public. The master file is DepictionHelp.hhp. It incorporates the Table of Contents files (TOC.hhc), the help cascading style sheet (help.css), and the stop search list (depiction.stp). The actual help text are the html files in Docs\public like routes.html and geo-align.html. The images in the helpfile can be found in the images subdirectory. 

The easiest way to build the helpfile is to use NANT. Start a command prompt. CD to c:\depiction\geo\build and issue the command "nant buildHelp". In about ten seconds a new helpfile is build. This is how the build process builds the helpfile. 

Modifying the help file to include more content is more difficult. This requires the "HTML Help Workshop" program. Go to the c:\depiction\externals\bin\Help file creation\Binaries and run double click on htmlhelp.exe. This will install HTML Hel Workshop (HHW) in your Program Files. 

Go to the start menu | all programs | HTML Help Workshop | HTML Help Workshop. This will launch help workshop. Let me say right up front the program is terrible. The polish level is very low. It has bugs. Still it is good enough for our purposes. 

In HHW chose "File | Open...". Navigate to \depiction\geo\source\Docs\Public and select "DepictionHelp.hhp".

From here you are on your own. Read over "hhw_tutorial.pdf" in the tutorials directory. It is long but a quick read. 

To add a new topic to the helpfile I did the following: 
1) copy than existing html file -- copy routes.html floods.html.
2) edit up floods.html (don't forget to change the header
3) add the new file to the project. See page 20 (24 of 61 in the pdf) on adding a project file. 
4) add the new topic to the table of conents. See page 34 (38 of 61) in the tutorial.
5) rebuild.

Good luck!
