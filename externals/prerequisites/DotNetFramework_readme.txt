May 16, 2008.

SimioGeo requires the 3.5 .Net Framework.

Up to May, 2008, the 3.5 and subordinate framework packages can be 
installed from \\Cave\Dev\DotNet35\dotnetfx35.exe, or you can run the 
dotNetFx35setup.exe in this directory to connect to internet sources for
any necessary framework components.

March 31st, 2009
(kent) Updated dotnetfx35.exe on \\cave\dev\DotNet35 to a 1/12/2009 version from Microsoft. 
