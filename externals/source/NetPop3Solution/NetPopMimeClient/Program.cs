using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using Net.Mail;
using Net.Mime;

namespace NetPopMimeClient
{
    internal class Program
    {
        private static readonly Pop3ClientParams bluebottleParams = new Pop3ClientParams
        {
            Hostname = "mail.bluebottle.com",
            Port = 110,
            Username = "simulation@bluebottle.com",
            Password = "3305tango",
            UseSsl = false
        };

        // This doesn't work yet.  Tried both SSL and not, neither works.
        private static readonly Pop3ClientParams outlookParams = new Pop3ClientParams
        {
            Hostname = "mail.simio.com",
            Port = 110,
            Username = "sniffer.test",
            Password = "3305Tango",
            UseSsl = false
        };

        // This doesn't work yet.  Tried both SSL and not, neither works.
        private static readonly Pop3ClientParams exerciseParams = new Pop3ClientParams
        {
            Hostname = "mail.simio.com",
            Port = 110,
            Username = "exercise",
            Password = "Depict-it!",
            UseSsl = false
        };

        private static readonly Pop3ClientParams googleParams = new Pop3ClientParams
        {
            Hostname = "pop.gmail.com",
            Port = 995,
            Username = "geodepiction",
            Password = "3305Tango",
            UseSsl = true
        };

        private static readonly Pop3ClientParams google2Params = new Pop3ClientParams
        {
            Hostname = "pop.gmail.com",
            Port = 995,
            Username = "recent:Jayoh7",
            Password = "yellowyellow",
            UseSsl = true
        };

        private static readonly Pop3ClientParams yahooParams = new Pop3ClientParams
        {
            Hostname = "plus.pop.mail.yahoo.com",
            Port = 110,
            Username = "geodepiction",
            Password = "3305Tango",
            UseSsl = false
        };


        private static readonly Pop3ClientParams hotmailParams = new Pop3ClientParams
        {
            Hostname = "pop3.live.com",
            Port = 995,
            Username = "asdf",
            Password = "3305Tango",
            UseSsl = true
        };


        private static readonly Pop3ClientParams liveParams = new Pop3ClientParams
        {
            Hostname = "pop3.live.com",
            Port = 995,
            Username = "jasonoverland@live.com",
            Password = "xxxxxxx",
            UseSsl = true
        };




        private static void Main(string[] args)
        {

            //SendTestCDOMessage("outgoing.verizon.net", 465, true, "acct-t2", "security142", "acct-t2@verizon.net", "jasono@depiction.com");
            TestSMTP("smtp.gmail.com", 587, true, "davidl@depiction.com", "bmw_328ci","davidl@depiction.com","dlipino79@gmail.com");
//            using (Pop3Client client = new Pop3Client(liveParams))
//            {
//                client.Trace += Console.WriteLine;
//                //connects to Pop3 Server, Executes POP3 USER and PASS
//                
//                //
//                try
//                {
//                    client.Authenticate();
//                    client.Stat();
//                }
//                catch (Exception)
//                {
//                        
//                }
//                
//                foreach (Pop3ListItem item in client.List())
//                {
//                    
//                    MailMessageEx message = client.RetrMailMessageEx(item);
//                    MimeEntity entity = client.RetrMimeEntity(item);
//                    Console.WriteLine("Children.Count: {0}", message.Children.Count);
//                    Console.WriteLine("message-id: {0}", message.MessageId);
//                    Console.WriteLine("from: {0}", message.From);
//                    var contentType = message.ContentType;
//
//                    var mediaType = contentType.MediaType;
//
//                    Console.WriteLine("mediaType: {0}", mediaType);
//                    Console.WriteLine("mime: {0}", entity.EncodedMessage);
//                    Console.WriteLine("mime children: {0}", entity.Children.Count);
//                    foreach (MimeEntity child in entity.Children)
//                    {
//                        Console.WriteLine("mime child message: {0}", child.EncodedMessage);
//                    }
//                    Console.WriteLine("subject: {0}", message.Subject);
//                    Console.WriteLine("to: {0}", message.To);
//                    Console.WriteLine("bodyPlainText: {0}", GetPlainTextBody(message, entity));
//                    Console.WriteLine("body: {0}", message.Body);
//                    Console.WriteLine("Attachments.Count: {0}", message.Attachments.Count);
//                    //client.Dele(item);
//                }
//                client.Noop();
//                client.Rset();
//                client.Quit();
//            }

            Console.ReadLine();
        }

        public static void TestSMTP(string host, int port, bool useSSL, string userName, string password, string from, string to)
        {
            var mailClient = new SmtpClient(host) { DeliveryMethod = SmtpDeliveryMethod.Network, EnableSsl = useSSL, Port = port };
            //mailClient.ServicePoint.MaxIdleTime = 10;
            mailClient.UseDefaultCredentials = false;
            mailClient.Credentials = mailClient.Credentials = new NetworkCredential(userName, password);

            try
            {
                var message = new MailMessage();
                message.From = new MailAddress(from);
                message.To.Add(new MailAddress(to));
                message.Subject = "test";
                message.Body = "here is a test body";
                //message.BodyEncoding = System.Text.Encoding.ASCII;

                mailClient.Send(message);
            }
            catch (Exception ex)
            {
            }
        }

        private static string GetPlainTextBody(MailMessageEx message, MimeEntity entity)
        {
            foreach (MimeEntity child in entity.Children)
            {
                if (child.ContentType.MediaType.Equals(MediaTypes.TextPlain))
                    return child.EncodedMessage.ToString();
            }
            
            return message.Body;
        }

        /// <summary>
        /// Send an electronic message using the Collaboration Data Objects (CDO).
        /// </summary>
        /// <remarks>http://support.microsoft.com/kb/310212</remarks>
        private static void SendTestCDOMessage(string host, int port, bool useSSL, string userName, string password, string from, string to)
        {
            try
            {

                CDO.Message message = new CDO.Message();
                CDO.IConfiguration configuration = message.Configuration;
                ADODB.Fields fields = configuration.Fields;

                Console.WriteLine(String.Format("Configuring CDO settings..."));

                // Set configuration.
                // sendusing:               cdoSendUsingPort, value 2, for sending the message using the network.
                // smtpauthenticate:     Specifies the mechanism used when authenticating to an SMTP service over the network.
                //                                  Possible values are:
                //                                  - cdoAnonymous, value 0. Do not authenticate.
                //                                  - cdoBasic, value 1. Use basic clear-text authentication. (Hint: This requires the use of "sendusername" and "sendpassword" fields)
                //                                  - cdoNTLM, value 2. The current process security context is used to authenticate with the service.

                ADODB.Field field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"];
                field.Value = host;

                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"];
                field.Value = port;

                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusing"];
                field.Value = CDO.CdoSendUsing.cdoSendUsingPort;

                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"];
                field.Value = CDO.CdoProtocolsAuthentication.cdoBasic;

                field = fields["http://schemas.microsoft.com/cdo/configuration/sendusername"];
                field.Value = userName;

                field = fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"];
                field.Value = password;

                field = fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"];
                field.Value = useSSL;

                fields.Update();

                Console.WriteLine(String.Format("Building CDO Message..."));

                message.From = from;
                message.To = to;
                message.Subject = "Test message.";
                message.TextBody = "This is a test message. Please disregard.";

                Console.WriteLine(String.Format("Attempting to connect to remote server..."));

                // Send message.
                message.Send();
                
                Console.WriteLine("Message sent.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
