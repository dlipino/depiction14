#include "vtdata\ElevationGrid.h"

void main(char* args, int argc)
{
	vtElevationGrid grid = vtElevationGrid();
	DRECT area = DRECT(0, 20, 20, 0);
	vtProjection newProj = vtProjection();
	newProj.SetTextDescription("wkt","PROJCS[\"World_Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Mercator_1SP\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],PARAMETER[\"latitude_of_origin\",0],UNIT[\"Meter\",1]]");
	bool success = grid.Create(area, 500, 500, true, newProj);
	FPoint3 point1 = FPoint3(0,0,0);
	FPoint3 point2 = FPoint3(10,10,0);
	for (int i = 0; i < 10000; i++)
	{
		grid.LineOfSight(point1, point2);
	}
}