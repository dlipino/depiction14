
#include "vtdata/ElevationGrid.h"
//#include "ElevDrawOptions.h"
#include "vtdata/MathTypes.h"
#include "vtdata/vtDIB.h"
#include "vtdataBitmap.h"



#define SHADING_BIAS	200

class CoverageShader
{
 public:
	 virtual ~CoverageShader() {}
	 bool SaveToJPG(char* filename, int quality);


};

// ElevationShader.h
class  ElevationShader: public CoverageShader
{

public:
	ElevationShader();
	ElevationShader(vtElevationGrid* grid, char* colormap_filename);
	virtual ~ElevationShader();
	vtElevationGrid	*m_pGrid;
	//static ElevDrawOptions m_draw;
	ColorMap *m_cmap;
	vtString cmap_path;
	void SetupDefaultColors(ColorMap *cmap);
	bool SaveToJPG(char* filename, int quality);
	ColorMap* GetColorMap();
	void SetColorMap(ColorMap *cmap);
protected:
	bool SetupBitmap();
// Helper
	FPoint3 LightDirection(float angle, float direction);
	vtBitmap	*m_pBitmap;
	int m_iColumns, m_iRows;
	int m_iImageWidth, m_iImageHeight;
};

