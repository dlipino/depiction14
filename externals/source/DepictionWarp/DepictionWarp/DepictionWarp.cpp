// SimioWarp.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"

#include "gdalwarper.h"
#include "cpl_string.h"
#include "ogr_spatialref.h"

#include "gdal_alg.h"
#include "gdal_priv.h"

#include "DepictionWarp.h"


#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}
void WarpFromGeoToMercator(char* inputFileName, char* outputFileName,char* jpgFileName, 
																	  double metersPerPixelX, double metersPerPixelY, double easting, double northing, double* DstGeoTransform,
																	  int* warpedWidth, int* warpedHeight,
																	  bool* colorMapped,
																	  BYTE **warpedDataBuffer,
																	  double dfErrorThreshold);
#ifdef _MANAGED
#pragma managed(pop)
#endif

// This is an example of an exported variable
SIMIOWARP_API int nSimioWarp=0;

// This is an example of an exported function.
extern  "C" __declspec(dllexport) void __stdcall WarpFromUTMToMercator(char* inputFileName, char* outputFileName,char* jpgFileName, 
																	  double metersPerPixelX, double metersPerPixelY, double easting, double northing, int utmZone, double* DstGeoTransform,
																	  int* warpedWidth, int* warpedHeight,
																	  bool* colorMapped,
																	  BYTE **warpedDataBuffer,
																	  double dfErrorThreshold)

{

	if(utmZone==-1)
	{
		//call geotomercator
		return WarpFromGeoToMercator(inputFileName,outputFileName,jpgFileName,metersPerPixelX,metersPerPixelY, easting,northing,DstGeoTransform,warpedWidth,warpedHeight,colorMapped,warpedDataBuffer,dfErrorThreshold);

	}
	bool fastMode=true;


    GDALDriverH hDriver;
    GDALDataType eDT;
    GDALDatasetH hDstDS;
    GDALDatasetH hSrcDS;

	GDALAllRegister();

    // Open the source file. 

    hSrcDS = GDALOpen( inputFileName, GA_Update );
    CPLAssert( hSrcDS != NULL );

	
    // Get output driver (GeoTIFF format)

    hDriver = GDALGetDriverByName( "GTiff" );
    CPLAssert( hDriver != NULL );


    // Create output with same datatype as first input band. 

    eDT = GDALGetRasterDataType(GDALGetRasterBand(hSrcDS,1));
	

    
	//Set Geo Transform for Input DataSet
	//

	double inputTransform[6];

	inputTransform[0] = easting;//easting of top left pixel
	inputTransform[3] = northing ;//northing of top left pixel
	inputTransform[1] = metersPerPixelX;
	inputTransform[4] = 0.0;
	inputTransform[2] = 0;
	inputTransform[5] = -metersPerPixelY;

	GDALSetGeoTransform(hSrcDS,inputTransform);


    // Set Source coordinate system. 

    const char *pszSrcWKT, *pszDstWKT = NULL;

	// Setup output coordinate system that is Mercator using Datum WGS84. 

    OGRSpatialReference oSRS;

	oSRS.SetMercator(0,0,1,0,0); 
    oSRS.SetWellKnownGeogCS( "WGS84" );
    oSRS.exportToWkt( (char**)&pszDstWKT );

	//Set Source Projection to UTM
	//
	
    OGRSpatialReference iSRS;
	iSRS.SetUTM(utmZone,TRUE);
	iSRS.SetWellKnownGeogCS("WGS84");
    iSRS.exportToWkt( (char**)&pszSrcWKT );

	GDALSetProjection(hSrcDS,pszSrcWKT);


    // Create a transformer that maps from source pixel/line coordinates
    // to destination georeferenced coordinates (not destination 
    // pixel line).  We do that by omitting the destination dataset
    // handle (setting it to NULL). 

    void *hTransformArg, *hGenImgProjArg=NULL;
	void *hApproxTransformArg=NULL;
	GDALTransformerFunc pfnTransformer = NULL;

	hTransformArg = hGenImgProjArg =
			GDALCreateGenImgProjTransformer( hSrcDS, pszSrcWKT, NULL, pszDstWKT, 
											 FALSE, 0, 1 );
	pfnTransformer = GDALGenImgProjTransform;

	
	
    // Get approximate output georeferenced bounds and resolution for file. 

    double adfDstGeoTransform[6];
    int nPixels=0, nLines=0;
    CPLErr eErr;

    eErr = GDALSuggestedWarpOutput( hSrcDS, 
                                    pfnTransformer, hTransformArg, 
                                    adfDstGeoTransform, &nPixels, &nLines );

    CPLAssert( eErr == CE_None );

	for(int i=0;i<6;i++){
		DstGeoTransform[i] = adfDstGeoTransform[i];
	}


	int rasterCount;

	/*****************************************
	* Create the output file.  
	*****************************************/
	rasterCount = GDALGetRasterCount(hSrcDS);
    hDstDS = GDALCreate( hDriver, outputFileName, nPixels, nLines, 
                         (rasterCount)>3?3:rasterCount, eDT, NULL );
    
    CPLAssert( hDstDS != NULL );

    // Write out the projection definition for the output file

    GDALSetProjection( hDstDS, pszDstWKT );
    GDALSetGeoTransform( hDstDS, adfDstGeoTransform );

	
    // Copy the color table, if required.

    GDALColorTableH hCT;
	bool colorTablePresent=false;

    hCT = GDALGetRasterColorTable( GDALGetRasterBand(hSrcDS,1) );
	if( hCT != NULL )
	{
        GDALSetRasterColorTable( GDALGetRasterBand(hDstDS,1), hCT );
		colorTablePresent=true;
	}


    CPLAssert( hTransformArg != NULL );

/******************
/* SETUP WARP OPTIONS
*******************/

    GDALWarpOptions *psWarpOptions = GDALCreateWarpOptions();

    psWarpOptions->hSrcDS = hSrcDS;
    psWarpOptions->hDstDS = hDstDS;

    psWarpOptions->nBandCount = (rasterCount)>3?3:rasterCount;
    psWarpOptions->panSrcBands = (int *) CPLMalloc(sizeof(int) * psWarpOptions->nBandCount);
    psWarpOptions->panSrcBands[0] = 1;
	if(rasterCount>=2)
    psWarpOptions->panSrcBands[1] = 2;
	if(rasterCount>=2)
    psWarpOptions->panSrcBands[2] = 3;
    psWarpOptions->panDstBands = (int *) CPLMalloc(sizeof(int) * psWarpOptions->nBandCount);
    psWarpOptions->panDstBands[0] = 1;
	if(rasterCount>=2)
    psWarpOptions->panDstBands[1] = 2;
	if(rasterCount>=2)
    psWarpOptions->panDstBands[2] = 3;

    psWarpOptions->pfnProgress = GDALTermProgress;   
	 

	/*! Warp Resampling Algorithm */
	
  /*! Nearest neighbour (select on one input pixel) */ //GRA_NearestNeighbour=0,
  /*! Bilinear (2x2 kernel) */                         //GRA_Bilinear=1,
  /*! Cubic Convolution Approximation (4x4 kernel) */  //GRA_Cubic=2,
  /*! Cubic B-Spline Approximation (4x4 kernel) */     //GRA_CubicSpline=3,
	

	if(colorTablePresent || rasterCount>1) psWarpOptions->eResampleAlg = GRA_NearestNeighbour;
	else psWarpOptions->eResampleAlg = GRA_Cubic;


	//
	//SET UP TRANSPARENCY COLOR FOR DESTINATION IMAGE
	//
	double redNoData, greenNoData, blueNoData;
	redNoData=154;
	greenNoData=205;
	blueNoData=50;
	double *nodataColor = (double*)malloc(sizeof(double)*3);//three bands maximum
	nodataColor[0]=redNoData;
	nodataColor[1]=greenNoData;
	nodataColor[2]=blueNoData;
	double *nodataColorReal = (double*)malloc(sizeof(double)*3);//three bands maximum
	nodataColorReal[0]=redNoData;
	nodataColorReal[1]=greenNoData;
	nodataColorReal[2]=blueNoData;

	psWarpOptions->padfDstNoDataImag=nodataColor;
	psWarpOptions->padfDstNoDataReal=nodataColorReal;

	char** otherWarpOptions=NULL;
	otherWarpOptions = CSLAddNameValue(otherWarpOptions,"INIT_DEST","NO_DATA");
	psWarpOptions->papszWarpOptions=otherWarpOptions;



    // Establish reprojection transformer. 
	GDALDestroyGenImgProjTransformer(hGenImgProjArg);
	hTransformArg = hGenImgProjArg = GDALCreateGenImgProjTransformer( hSrcDS, 
                                         GDALGetProjectionRef(hSrcDS), 
                                         hDstDS,
                                         GDALGetProjectionRef(hDstDS), 
                                         FALSE, 0.0, 1 );
	pfnTransformer=GDALGenImgProjTransform;
	
/* -------------------------------------------------------------------- */
/*      Warp the transformer with a linear approximator unless the      */
/*      acceptable error is zero.                                       */
/* -------------------------------------------------------------------- */
	if( dfErrorThreshold != 0.0 )
    {
		hTransformArg = hApproxTransformArg = 
			GDALCreateApproxTransformer( GDALGenImgProjTransform, 
										 hGenImgProjArg, dfErrorThreshold/*pixel threshold*/ );
		pfnTransformer = GDALApproxTransform;
	}
	psWarpOptions->pfnTransformer=pfnTransformer;
	psWarpOptions->pTransformerArg=hTransformArg;

	
    // Initialize and execute the warp operation. 

    GDALWarpOperation oOperation;


    oOperation.Initialize( psWarpOptions );
    oOperation.ChunkAndWarpImage( 0, 0, 
                                  GDALGetRasterXSize( hDstDS ), 
                                  GDALGetRasterYSize( hDstDS ) );


	/****************************
	*Output image file should now be in Mercator projection
	****************************/
	//CREATE A JPEG VERSION OF OUTPUT
	//
	//use PNG instead -- has better accuracy and avoids fuzzy boundaries when
	//transparency is set
	//Bharath May21 2009
    GDALDriverH hDriverJPG;
    hDriverJPG = GDALGetDriverByName( "PNG" );
    CPLAssert( hDriverJPG != NULL );
	GDALDatasetH hDstDSJPG = GDALCreateCopy(hDriverJPG,jpgFileName,hDstDS,true,NULL,NULL,NULL);
	GDALClose(hDstDSJPG);

	//
	//SAVE THE WARPED TIF IMAGE AS A JPEG IMAGE
	//

        // Get the GDAL Band objects from the Dataset
        GDALRasterBandH bandH = GDALGetRasterBand(hDstDS,1);
		
        GDALColorTableH ctH = GDALGetRasterColorTable(bandH);

		int width = GDALGetRasterBandXSize( bandH );
		int height = GDALGetRasterBandYSize( bandH );
		
		
		//adjust warpedWidth to be on a 4-byte boundary
		int padding=(4-width%4);
		int origWidth=width;
        if (width%4 != 0) width += padding;
		
        if (ctH!=NULL && GDALGetPaletteInterpretation(ctH) == GPI_RGB)
        {

			*colorMapped=true;
		}//DO ONLY FOR A COLORMAPPED IMAGE

		*warpedDataBuffer = (unsigned char*)malloc(sizeof(unsigned char)*width*height);
  
		for(int row=0;row<height;row++)
		{//do for all rows
			GDALRasterIO(bandH,GF_Read,0,row,origWidth,1,
						(unsigned char*)(*warpedDataBuffer+row*width),origWidth,1,GDT_Byte,0,0);
		}

		*warpedWidth = width;
		*warpedHeight = height;
	//
	//END SAVE WARPED TIF AS JPEG
	//
		

	/****************************
	* CLEANUP
	****************************/
	if(hGenImgProjArg!=NULL)
    GDALDestroyGenImgProjTransformer( hGenImgProjArg );
	if(hApproxTransformArg!=NULL)
     GDALDestroyApproxTransformer( hApproxTransformArg );
	

	psWarpOptions->padfDstNoDataImag=NULL;
	psWarpOptions->padfDstNoDataReal=NULL;
	

	free(nodataColor); free(nodataColorReal);
    GDALDestroyWarpOptions( psWarpOptions );
    GDALClose( hSrcDS );
    GDALClose( hDstDS );

	return ;
}

void WarpFromGeoToMercator(char* inputFileName, char* outputFileName,char* jpgFileName, 
																	  double metersPerPixelX, double metersPerPixelY, double easting, double northing, double* DstGeoTransform,
																	  int* warpedWidth, int* warpedHeight,
																	  bool* colorMapped,
																	  BYTE **warpedDataBuffer,
																	  double dfErrorThreshold)

{

	bool fastMode=true;


    GDALDriverH hDriver;
    GDALDataType eDT;
    GDALDatasetH hDstDS;
    GDALDatasetH hSrcDS;

	GDALAllRegister();

    // Open the source file. 

    hSrcDS = GDALOpen( inputFileName, GA_Update );
    CPLAssert( hSrcDS != NULL );

	
    // Get output driver (GeoTIFF format)

    hDriver = GDALGetDriverByName( "GTiff" );
    CPLAssert( hDriver != NULL );


    // Create output with same datatype as first input band. 

    eDT = GDALGetRasterDataType(GDALGetRasterBand(hSrcDS,1));
	

    
	//Set Geo Transform for Input DataSet
	//

	double inputTransform[6];

	inputTransform[0] = easting;//easting of top left pixel
	inputTransform[3] = northing ;//northing of top left pixel
	inputTransform[1] = metersPerPixelX;
	inputTransform[4] = 0.0;
	inputTransform[2] = 0;
	inputTransform[5] = -metersPerPixelY;

	GDALSetGeoTransform(hSrcDS,inputTransform);


    // Set Source coordinate system. 

    const char *pszSrcWKT, *pszDstWKT = NULL;

	// Setup output coordinate system that is Mercator using Datum WGS84. 

    OGRSpatialReference oSRS;

	oSRS.SetMercator(0,0,1,0,0); 
    oSRS.SetWellKnownGeogCS( "WGS84" );
    oSRS.exportToWkt( (char**)&pszDstWKT );

	//Set Source Projection to Geographic
	//
	
    OGRSpatialReference iSRS;
	iSRS.SetWellKnownGeogCS("WGS84");
    iSRS.exportToWkt( (char**)&pszSrcWKT );

	GDALSetProjection(hSrcDS,pszSrcWKT);


    // Create a transformer that maps from source pixel/line coordinates
    // to destination georeferenced coordinates (not destination 
    // pixel line).  We do that by omitting the destination dataset
    // handle (setting it to NULL). 

    void *hTransformArg, *hGenImgProjArg=NULL;
	void *hApproxTransformArg=NULL;
	GDALTransformerFunc pfnTransformer = NULL;

	hTransformArg = hGenImgProjArg =
			GDALCreateGenImgProjTransformer( hSrcDS, pszSrcWKT, NULL, pszDstWKT, 
											 FALSE, 0, 1 );
	pfnTransformer = GDALGenImgProjTransform;

	
	
    // Get approximate output georeferenced bounds and resolution for file. 

    double adfDstGeoTransform[6];
    int nPixels=0, nLines=0;
    CPLErr eErr;

    eErr = GDALSuggestedWarpOutput( hSrcDS, 
                                    pfnTransformer, hTransformArg, 
                                    adfDstGeoTransform, &nPixels, &nLines );

    CPLAssert( eErr == CE_None );

	for(int i=0;i<6;i++){
		DstGeoTransform[i] = adfDstGeoTransform[i];
	}


	int rasterCount;

	/*****************************************
	* Create the output file.  
	*****************************************/
	rasterCount = GDALGetRasterCount(hSrcDS);
    hDstDS = GDALCreate( hDriver, outputFileName, nPixels, nLines, 
                         (rasterCount)>3?3:rasterCount, eDT, NULL );
    
    CPLAssert( hDstDS != NULL );

    // Write out the projection definition for the output file

    GDALSetProjection( hDstDS, pszDstWKT );
    GDALSetGeoTransform( hDstDS, adfDstGeoTransform );

	
    // Copy the color table, if required.

    GDALColorTableH hCT;
	bool colorTablePresent=false;

    hCT = GDALGetRasterColorTable( GDALGetRasterBand(hSrcDS,1) );
	if( hCT != NULL )
	{
        GDALSetRasterColorTable( GDALGetRasterBand(hDstDS,1), hCT );
		colorTablePresent=true;
	}


    CPLAssert( hTransformArg != NULL );

/******************
/* SETUP WARP OPTIONS
*******************/

    GDALWarpOptions *psWarpOptions = GDALCreateWarpOptions();

    psWarpOptions->hSrcDS = hSrcDS;
    psWarpOptions->hDstDS = hDstDS;

    psWarpOptions->nBandCount = (rasterCount)>3?3:rasterCount;
    psWarpOptions->panSrcBands = (int *) CPLMalloc(sizeof(int) * psWarpOptions->nBandCount);
    psWarpOptions->panSrcBands[0] = 1;
	if(rasterCount>=2)
    psWarpOptions->panSrcBands[1] = 2;
	if(rasterCount>=2)
    psWarpOptions->panSrcBands[2] = 3;
    psWarpOptions->panDstBands = (int *) CPLMalloc(sizeof(int) * psWarpOptions->nBandCount);
    psWarpOptions->panDstBands[0] = 1;
	if(rasterCount>=2)
    psWarpOptions->panDstBands[1] = 2;
	if(rasterCount>=2)
    psWarpOptions->panDstBands[2] = 3;

    psWarpOptions->pfnProgress = GDALTermProgress;   
	 

	/*! Warp Resampling Algorithm */
	
  /*! Nearest neighbour (select on one input pixel) */ //GRA_NearestNeighbour=0,
  /*! Bilinear (2x2 kernel) */                         //GRA_Bilinear=1,
  /*! Cubic Convolution Approximation (4x4 kernel) */  //GRA_Cubic=2,
  /*! Cubic B-Spline Approximation (4x4 kernel) */     //GRA_CubicSpline=3,
	
	if(colorTablePresent || rasterCount>1) psWarpOptions->eResampleAlg = GRA_NearestNeighbour;
	else psWarpOptions->eResampleAlg = GRA_Cubic;




	//
	//SET UP TRANSPARENCY COLOR FOR DESTINATION IMAGE
	//
	double redNoData, greenNoData, blueNoData;
	redNoData=154;
	greenNoData=205;
	blueNoData=50;
	double *nodataColor = (double*)malloc(sizeof(double)*3);//three bands maximum
	nodataColor[0]=redNoData;
	nodataColor[1]=greenNoData;
	nodataColor[2]=blueNoData;
	double *nodataColorReal = (double*)malloc(sizeof(double)*3);//three bands maximum
	nodataColorReal[0]=redNoData;
	nodataColorReal[1]=greenNoData;
	nodataColorReal[2]=blueNoData;

	psWarpOptions->padfDstNoDataImag=nodataColor;
	psWarpOptions->padfDstNoDataReal=nodataColorReal;

	char** otherWarpOptions=NULL;
	otherWarpOptions = CSLAddNameValue(otherWarpOptions,"INIT_DEST","NO_DATA");
	psWarpOptions->papszWarpOptions=otherWarpOptions;



    // Establish reprojection transformer. 
	GDALDestroyGenImgProjTransformer(hGenImgProjArg);
	hTransformArg = hGenImgProjArg = GDALCreateGenImgProjTransformer( hSrcDS, 
                                         GDALGetProjectionRef(hSrcDS), 
                                         hDstDS,
                                         GDALGetProjectionRef(hDstDS), 
                                         FALSE, 0.0, 1 );
	pfnTransformer=GDALGenImgProjTransform;
	
/* -------------------------------------------------------------------- */
/*      Warp the transformer with a linear approximator unless the      */
/*      acceptable error is zero.                                       */
/* -------------------------------------------------------------------- */
	if( dfErrorThreshold != 0.0 )
    {
		hTransformArg = hApproxTransformArg = 
			GDALCreateApproxTransformer( GDALGenImgProjTransform, 
										 hGenImgProjArg, dfErrorThreshold/*pixel threshold*/ );
		pfnTransformer = GDALApproxTransform;
	}
	psWarpOptions->pfnTransformer=pfnTransformer;
	psWarpOptions->pTransformerArg=hTransformArg;

	
    // Initialize and execute the warp operation. 

    GDALWarpOperation oOperation;


    oOperation.Initialize( psWarpOptions );
    oOperation.ChunkAndWarpImage( 0, 0, 
                                  GDALGetRasterXSize( hDstDS ), 
                                  GDALGetRasterYSize( hDstDS ) );


	/****************************
	*Output image file should now be in Mercator projection
	****************************/
	//CREATE A JPEG VERSION OF OUTPUT
	//
	//use PNG instead -- has better accuracy and avoids fuzzy boundaries when
	//transparency is set
	//Bharath May21 2009
    GDALDriverH hDriverJPG;
    hDriverJPG = GDALGetDriverByName( "PNG" );
    CPLAssert( hDriverJPG != NULL );
	char **papszOptions = NULL;
    /*
	 *retaining this comment since it shows how to pass parameters to GDAL drivers in CreateCopy call
	 *Bharath May21 2009
    papszOptions = CSLSetNameValue( papszOptions, "QUALITY", "100" );*/
	GDALDatasetH hDstDSJPG = GDALCreateCopy(hDriverJPG,jpgFileName,hDstDS,true,papszOptions,NULL,NULL);
	if(hDstDSJPG != NULL) GDALClose(hDstDSJPG);


	//
	//SAVE THE WARPED TIF IMAGE AS A JPEG IMAGE
	//

        // Get the GDAL Band objects from the Dataset
        GDALRasterBandH bandH = GDALGetRasterBand(hDstDS,1);
		
        GDALColorTableH ctH = GDALGetRasterColorTable(bandH);

		int width = GDALGetRasterBandXSize( bandH );
		int height = GDALGetRasterBandYSize( bandH );
		
		
		//adjust warpedWidth to be on a 4-byte boundary
		int padding=(4-width%4);
		int origWidth=width;
        if (width%4 != 0) width += padding;
		
        if (ctH!=NULL && GDALGetPaletteInterpretation(ctH) == GPI_RGB)
        {

			*colorMapped=true;
		}//DO ONLY FOR A COLORMAPPED IMAGE

		*warpedDataBuffer = (unsigned char*)malloc(sizeof(unsigned char)*width*height);
  
		for(int row=0;row<height;row++)
		{//do for all rows
			GDALRasterIO(bandH,GF_Read,0,row,origWidth,1,
						(unsigned char*)(*warpedDataBuffer+row*width),origWidth,1,GDT_Byte,0,0);
		}

		*warpedWidth = width;
		*warpedHeight = height;
	//
	//END SAVE WARPED TIF AS JPEG
	//
		

	/****************************
	* CLEANUP
	****************************/
	if(hGenImgProjArg!=NULL)
    GDALDestroyGenImgProjTransformer( hGenImgProjArg );
	if(hApproxTransformArg!=NULL)
     GDALDestroyApproxTransformer( hApproxTransformArg );
	

	psWarpOptions->padfDstNoDataImag=NULL;
	psWarpOptions->padfDstNoDataReal=NULL;
	

	free(nodataColor); free(nodataColorReal);
    GDALDestroyWarpOptions( psWarpOptions );
    GDALClose( hSrcDS );
    GDALClose( hDstDS );

	return ;
}

// This is the constructor of a class that has been exported.
// see SimioWarp.h for the class definition
CSimioWarp::CSimioWarp()
{
	return;
}
