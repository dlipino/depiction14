// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SIMIOWARP_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SIMIOWARP_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef SIMIOWARP_EXPORTS
#define SIMIOWARP_API __declspec(dllexport)
#else
#define SIMIOWARP_API __declspec(dllimport)
#endif



// This class is exported from the SimioWarp.dll
class SIMIOWARP_API CSimioWarp {
public:
	CSimioWarp(void);
	// TODO: add your methods here.
};

extern SIMIOWARP_API int nSimioWarp;

SIMIOWARP_API int fnSimioWarp(void);

