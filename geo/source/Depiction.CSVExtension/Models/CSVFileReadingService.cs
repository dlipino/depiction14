using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.TypeConverter;
using Depiction.CSVExtension.Exceptions;
using Depiction.CSVExtension.ViewModels;
using Depiction.Externals.Csv;

namespace Depiction.CSVExtension.Models
{
    public class CSVFileReadingService : BaseDepictionBackgroundThreadOperation
    {
        public const char Tab = '\t';
        public const char Comma = ',';

        #region public methods

        //Made public only for testing
        public IEnumerable<IElementPrototype> ReadCSVFileAndCreatePrototypes(string fileName, string elementType, IMapCoordinateBounds depictionRegion,
                                                                             bool cropToRegion, GeoLocationInfoToPropertyHolder locationPropertyHolder,
                    CSVLocationModeType locationMode, IDepictionGeocodingService geocoder, char delimiter)
        {
            //The read happens so fast the user doesn't get to see visual updates from the file read
            Thread.Sleep(1000);
            var prototypes = GetRawPrototypesFromCSVFile(fileName, elementType, 0, delimiter);
            if (prototypes.Count == 0)
                throw new CSVMissingDataException(fileName);
            if (ServiceStopRequested) return null;

            GeocodeCSVPrototypes(locationMode, locationPropertyHolder, prototypes, geocoder);

            if (ServiceStopRequested) return null;

            List<IElementPrototype> finalPrototypes;
            var notInRegionCount = 0;
            var notGeoCoded = 0;
            if (cropToRegion && depictionRegion != null)
            {
                var inRegion = new List<IElementPrototype>();
                //Something is funky, this doesn't work very well with richards lummi example
                foreach (var prototype in prototypes)
                {
                    //TODO make this deal with all shape types instead of just points
                    var latLong = new LatitudeLongitude();
                    prototype.GetPropertyValue("Position", out latLong);
                    //if (latLong == null) latLong = new LatitudeLongitude();
                    if (depictionRegion.Contains(latLong)) //element.Position))
                    {
                        inRegion.Add(prototype);
                    }
                    else
                    {
                        if (latLong == null)
                        {
                            notInRegionCount++;
                            notGeoCoded++;
                            //If lat long was null that means it was not present so it might be an EID update or just
                            //something with no position.
                            inRegion.Add(prototype);
                        }
                        else if (latLong.IsValid)
                        {
                            notInRegionCount++;
                        }
                    }
                }
                finalPrototypes = inRegion;
                var message =
                    new DepictionMessage(string.Format("{0} elements were not contained in depiction region.",
                                                       notInRegionCount), 4);
                DepictionAccess.NotificationService.DisplayMessage(message);
            }
            else
            {
                finalPrototypes = prototypes;
            }
            if (finalPrototypes.Count != 0)
            {
                UpdateStatusReport(string.Format("Loading {0} geo-located elements and {1} elements with no location into the depiction", finalPrototypes.Count, notGeoCoded));
            }
            //The read happens so fast the user doesn't get any input
            Thread.Sleep(1000);
            return finalPrototypes;
        }
        public IEnumerable<IElementPrototype> ReadCSVFileAndCreatePrototypes(string fileName, string elementType, IMapCoordinateBounds depictionRegion,
                                                                     bool cropToRegion, GeoLocationInfoToPropertyHolder locationPropertyHolder,
                CSVLocationModeType locationMode, IDepictionGeocodingService geocoder)
        {
            return ReadCSVFileAndCreatePrototypes(fileName, elementType, depictionRegion, cropToRegion, locationPropertyHolder, locationMode, geocoder, ',');
        }
        #endregion

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }
        //hmmm i guess you don't really need the args since the needed values can come from the class itself.
        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var csvImporterViewModel = parameters["ViewModel"] as ImportFromCsvViewModel;
            if (csvImporterViewModel == null) return null;
            var area = parameters["Area"] as IMapCoordinateBounds;
            return ReadCSVFileAndCreatePrototypes(csvImporterViewModel.FileInfo.FullName, csvImporterViewModel.FullElementTypeName, area,
                                                  csvImporterViewModel.CropDataToRegion, csvImporterViewModel.LocationPropertyHolder,
                                                  csvImporterViewModel.LocationStringMode, DepictionAccess.GeoCodingService, csvImporterViewModel.DelimiterType);
        }

        protected override void ServiceComplete(object args)
        {
            var prototypes = args as List<IElementPrototype>;
            if (prototypes == null) return;
            //Reading the CSV file happens too fast for the UI thread.
            UpdateStatusReport(string.Format("Loading {0} elements into the depiction", prototypes.Count));
            AddReadeCSVPrototypeToDepictionStory(DepictionAccess.CurrentDepiction, prototypes);

        }

        #endregion
        public void AddReadeCSVPrototypeToDepictionStory(IDepictionStory story, List<IElementPrototype> prototypes)
        {
            if (story == null || prototypes == null) return;
            List<IDepictionElement> updatedElements;
            List<IDepictionElement> createdElements;
            story.CreateOrUpdateDepictionElementListFromPrototypeList(prototypes, out createdElements, out updatedElements, true, false);
            foreach (var element in updatedElements)
            {
                element.ElementUpdated = true;
                //Hack to get element properties with events to trigger

                ElementFactory.RunElementZOIGenerationBehaviors(element);
            }
            foreach (var element in createdElements)
            {
                ElementFactory.RunElementZOIGenerationBehaviors(element);
            }
            var createdElementIds = from ce in createdElements
                                    select ce.ElementKey;
            var updatedElementIds = from ce in updatedElements
                                    select ce.ElementKey;
            var elementIds = createdElementIds.Concat(updatedElementIds).ToList();

            story.RequestElementsWithIdsViewing(elementIds, new[] { DepictionDialogType.PropertyEditor });

        }
        #region element getting methods

        private List<IElementPrototype> GetRawPrototypesFromCSVFile(string csvFile, string elementType, int maxRecordsToGet, char delimiter)
        {
            var records = new List<IElementPrototype>();
            double totalCount = BruteForceCount(csvFile);
            if (maxRecordsToGet != 0) totalCount = maxRecordsToGet;
            try
            {
                using (var streamReader = new StreamReader(new FileStream(csvFile, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    using (var reader = new CsvReader(streamReader, true, delimiter) { MissingFieldAction = MissingFieldAction.ReplaceByEmpty })
                    {
                        string[] headers = reader.GetFieldHeaders();
                        if (headers == null || headers.Length == 0)
                            throw new CSVMissingHeaderException(csvFile);

                        MissingFieldCsvException missingFieldCsvException = null;
                        int missingFieldsCount = 0;
                        int i = 0;
                        while (reader.ReadNextRecord())
                        {
                            if (ServiceStopRequested) break;
                            i++;
                            UpdateStatusReport(string.Format("Reading element {0} of {1}", i, totalCount));
                            if (i >= maxRecordsToGet && maxRecordsToGet > 0)
                                break;
                            //IDepictionElement elementBase;
                            IElementPrototype elementBase;
                            try
                            {
                                //if (maxRecordsToGet != 0)
                                {

                                    elementBase = GetDepictionPrototypeFromCSVHeaders(headers, reader, elementType);
                                }
                                //else
                                //{ //Get the record type if this is nto for show
                                //    record = GetDepictionPrototypeFromCSVHeaders(headers, reader, true);
                                //}

                                if (elementBase != null)
                                {
                                    //record.RecordSource = ElementPropertySource.CSV;//
                                    elementBase.Tags.Add(Tags.DefaultFileTag + ":" + Path.GetFileName(csvFile));
                                    records.Add(elementBase);
                                }
                            }
                            catch (MissingFieldCsvException ex)
                            {
                                missingFieldCsvException = ex;
                                missingFieldsCount++;
                            }
                            catch (MalformedCsvException ex)
                            {
                                DepictionAccess.NotificationService.DisplayMessageString(
                                    string.Format("Problem reading a line from csv file \"{0}\":\n{1}",
                                                  csvFile, ex.Message));
                            }

                        }
                        if (missingFieldCsvException != null)
                        {
                            DepictionAccess.NotificationService.DisplayMessageString(
                                string.Format("{0} records in the file \"{1}\" are missing fields:\n{2}",
                                              missingFieldsCount, csvFile, missingFieldCsvException.Message));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                                string.Format("File error : {0}", ex.Message));
                return null;
            }
            if (records.Count == 0)
                throw new CSVMissingDataException(csvFile);

            return records;
        }

        /// <summary>
        /// Returns a full element type containing all the default property types and any new (property,value) pairs for each column in the
        /// csv table. Has the option to set the element record type if it is present (elementType)
        /// </summary>
        private static IElementPrototype GetDepictionPrototypeFromCSVHeaders(string[] headers, CsvReader reader, string elementType)
        {
            //Should probably set record type here
            //record.GeometryType = GeometryType.Point;
            var properties = new Dictionary<string, string>();
            var zoiString = string.Empty;
            for (int i = 0; i < headers.Length; i++)
            {
                if (headers[i].Equals("elementType", StringComparison.OrdinalIgnoreCase) || headers[i].Equals("element Type", StringComparison.OrdinalIgnoreCase))
                {
                    elementType = reader[i].Trim();
                }
                else if (headers[i].Equals("zoi", StringComparison.OrdinalIgnoreCase) || headers[i].Equals("zoneofinfluence", StringComparison.OrdinalIgnoreCase))
                {
                    zoiString = reader[i].Trim();
                    //properties.Add(headers[i], reader[i].Trim());
                }
                else
                {
                    properties.Add(headers[i], reader[i].Trim());
                }
            }
            //Prep users will not be able to import shapes from csv

            if ((DepictionAccess.ProductInformation.ProductType.Equals(ProductInformationBase.Prep)))
            {
                zoiString = string.Empty;
            }

            var zoiDescription = DepictionGeometryType.Point.ToString();
            if (!string.IsNullOrEmpty(zoiString))
            {
                if (zoiString.ToLowerInvariant().Contains("Line"))
                {
                    zoiDescription = DepictionGeometryType.LineString.ToString();
                }
                else if (zoiString.ToLowerInvariant().Contains("Polygon"))
                {
                    zoiDescription = DepictionGeometryType.Polygon.ToString();
                }
            }
            var fakePrototype = DepictionAccess.ElementLibrary.GetPrototypeFromAutoDetect(elementType, zoiDescription);
            IElementPrototype prototype = null;
            //var prototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(elementType);
            if (fakePrototype == null)
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(elementType);
            }
            else
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(fakePrototype.ElementType);
            }
            //  var prototype = DepictionAccess.ElementLibrary.GetPrototypeFromAutoDetect(elementType, "Point");
            // var element = ElementFactory.CreateElementFromPrototype(prototype);

            foreach (var property in properties)
            {

                var key = property.Key;
                if (key.Equals("name", StringComparison.OrdinalIgnoreCase))
                {
                    var elemProp = new DepictionElementProperty("DisplayName", "Name", property.Value);
                    elemProp.Deletable = false;
                    //                    elemProp.IsHoverText = true;
                    elemProp.Rank = -1;
                    //So that this property's attributes don't get replaced by the hack in the element factory
                    //elemProp.PropertySource = PropertySource.CSV;
                    prototype.AddPropertyOrReplaceValueAndAttributes(elemProp, true, false);
                }
                else if (key.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                {
                    var stringVal = property.Value;
                    var prop = new DepictionElementProperty(key, stringVal);
                    prop.Deletable = false;
                    prop.Editable = false;
                    prop.VisibleToUser = true;
                    prop.PropertySource = PropertySource.CSV;
                    prototype.AddPropertyOrReplaceValueAndAttributes(prop, true, false);
                }
                else
                {

                    var stringVal = property.Value;
                    var objectValue = DepictionTypeConverter.ChangeTypeByGuessing(stringVal);

                    if (objectValue == null) objectValue = stringVal;

                    var prop = new DepictionElementProperty(key, objectValue);
                    //prop.Deletable = false;
                    prop.PropertySource = PropertySource.CSV;
                    prototype.AddPropertyOrReplaceValueAndAttributes(prop, true, false);
                }
            }
            prototype.SetInitialPositionAndZOI(null, new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)));
            return prototype;
        }

        //        public string[] FindHeaders(string csvFile)
        //        {
        //            string[] headers;
        //
        //            using (var streamReader = new StreamReader(File.OpenRead(csvFile)))
        //            {
        //                using (var reader = new CsvReader(streamReader, true,delimiter) { MissingFieldAction = MissingFieldAction.ReplaceByEmpty })
        //                {
        //                    headers = reader.GetFieldHeaders();
        //                }
        //            }
        //            return headers;
        //        }

        #endregion

        #region random helpers
        private IElementProperty GetPropDataByName(IElementPrototype proto, string name)
        {
            if (string.IsNullOrEmpty(name)) return null;
            return proto.GetPropertyByInternalName(name, true);
        }

        private void GeocodeCSVPrototypes(CSVLocationModeType locationStringMode, GeoLocationInfoToPropertyHolder propertyHolder,
                                          IEnumerable<IElementPrototype> prototypes, IDepictionGeocodingService geoCodingService)
        {
            //Things don't go so well if geocoder is null
            double totalCount = prototypes.Count();
            int count = 0;
            var locatGeocodeService = geoCodingService;

            if (locatGeocodeService == null)
            {
                locatGeocodeService = DepictionAccess.GeoCodingService;
            }
            var availableGeocoders = locatGeocodeService.FindAGeocodersForRegion(string.Empty);
            var geocodedCount = 0;
            var geocodeLimit = 1000;
            if ((DepictionAccess.ProductInformation.ProductType.Equals(ProductInformationBase.Prep)))
            {
                geocodeLimit = 30;
            }
            var useWebForGeocoding = true;
            foreach (var element in prototypes)
            {
                object latLongObject;// = new LatitudeLongitude();
                if (element.GetPropertyValue("Position", out latLongObject))
                {
                    var latLong = latLongObject as LatitudeLongitude;

                    if (latLong != null)
                    {
                        if (latLong.IsValid) continue;
                    }
                }
                if (geocodedCount >= geocodeLimit) useWebForGeocoding = false;
                // if (element.Position.IsValid) continue;//This might speed things up
                if (ServiceStopRequested) break;
                count++;
                UpdateStatusReport(string.Format("Geocoding element {0} of {1}", count, totalCount));

                try
                {
                    GeocodeResults geocodeResults = null;
                    if (locationStringMode.Equals(CSVLocationModeType.LatitudeLongitudeSingleField))
                    {
                        string latitudeLongitude = string.Empty;

                        var latitudeLongitudeProp = GetPropDataByName(element, propertyHolder.LatitudeLongitude);
                        if (latitudeLongitudeProp != null)
                            latitudeLongitude = latitudeLongitudeProp.Value.ToString();
                        if (!string.IsNullOrEmpty(latitudeLongitude))
                            geocodeResults = locatGeocodeService.GeoCodeRawStringAddress(latitudeLongitude, availableGeocoders);
                    }
                    else if (locationStringMode.Equals(CSVLocationModeType.LatitudeLongitudeSeparateFields))
                    {
                        object latitude, longitude;
                        //
                        if (element.GetPropertyValue(propertyHolder.Latitude, out latitude) && element.GetPropertyValue(propertyHolder.Longitude, out longitude))
                        {
                            if (!string.IsNullOrEmpty(latitude.ToString()) && !string.IsNullOrEmpty(longitude.ToString()))
                            {
                                geocodeResults = locatGeocodeService.GeoCodeRawStringAddress(string.Format("{0}, {1}", latitude, longitude), availableGeocoders);
                            }
                        }
                    }
                    else if ((geocodeResults == null || !geocodeResults.IsValid) &&
                             locationStringMode.Equals(CSVLocationModeType.AddressSingleField))
                    {
                        string address = string.Empty;
                        var addressProp = GetPropDataByName(element, propertyHolder.AddressSingleField);
                        if (addressProp != null)
                            address = addressProp.Value.ToString();
                        if (!string.IsNullOrEmpty(address))
                            geocodeResults = locatGeocodeService.GeoCodeRawStringAddress(string.Format("{0}", address), availableGeocoders, useWebForGeocoding);
                    }
                    else if ((geocodeResults == null || !geocodeResults.IsValid) &&
                             (locationStringMode.Equals(CSVLocationModeType.AddressMultipleFields)))
                    {
                        string streetAddress = string.Empty,
                               city = string.Empty,
                               state = string.Empty,
                               zipCode = string.Empty,
                               country = string.Empty;
                        var streetProp = GetPropDataByName(element, propertyHolder.StreetAddress);
                        if (streetProp != null)
                            streetAddress = streetProp.Value.ToString();
                        var cityProp = GetPropDataByName(element, propertyHolder.City);
                        if (cityProp != null)
                            city = cityProp.Value.ToString();
                        var stateProp = GetPropDataByName(element, propertyHolder.State);
                        if (stateProp != null)
                            state = stateProp.Value.ToString();
                        var zipCodeProp = GetPropDataByName(element, propertyHolder.ZipCode);
                        if (zipCodeProp != null)
                        {
                            zipCode = zipCodeProp.Value.ToString();
                        }

                        var countryProp = GetPropDataByName(element, propertyHolder.Country);
                        if (countryProp != null)
                        {
                            country = countryProp.Value.ToString();
                        }
                        geocodeResults = locatGeocodeService.GeoCodeRawStringAddress(streetAddress, city, state, zipCode, country, availableGeocoders, useWebForGeocoding);
                    }

                    if (geocodeResults != null && geocodeResults.GeocodedByWeb)
                    {
                        geocodedCount++;
                    }
                    if (geocodeResults != null && geocodeResults.Position != null && geocodeResults.Position.IsValid)
                    {
                        element.SetInitialPositionAndZOI(geocodeResults.Position, null);
                        if (!geocodeResults.Accurate)
                        {
                            element.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("IconBorderColor", "IconBorderColor", Colors.Red));
                            element.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("IconBorderShape", "IconBorderShape", DepictionIconBorderShape.Circle));
                        }
                        //element.Position = geocodeResults;
                    }
                }

                catch (Exception e)
                {
                    //DepictionExceptionHandler.HandleException(e, false);
                    var message = string.Format("Geocoding error has for {0} has occurred: {1}", element.ElementType, e.Message);
                    DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                }
            }

            if (geocodedCount > geocodeLimit)
            {
                var message = string.Format("The geocoding limit of {0} was reached. A possible {1} elements were not geocoded", geocodeLimit, count - geocodeLimit);
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
            }
            //The save is normally done when the app closes, but for this, why not just write it out a bit early
            try
            {
                locatGeocodeService.SaveGeneralGeoCodeCache();
            }
            catch { }
        }

        private int BruteForceCount(string csvFileName)
        {
            int count = 0;
            using (var streamReader = new StreamReader(new FileStream(csvFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
            {
                using (var reader = new CsvReader(streamReader, true) { MissingFieldAction = MissingFieldAction.ReplaceByEmpty })
                {
                    while (reader.ReadNextRecord())
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        #endregion
    }
}