﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.CSVExtension.ViewModels;

namespace Depiction.CSVExtension.Converters
{
    public class CSVLocationModeTypeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CSVLocationModeType)
            {
                var type = parameter.ToString();
                var screen = ((CSVLocationModeType)value).ToString().ToLower();
                if (type.ToLower().Equals(screen))
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {

                var selected = (bool) value;
                if (selected)
                    return (CSVLocationModeType) Enum.Parse(typeof (CSVLocationModeType), parameter.ToString());
            }
            return CSVLocationModeType.None;
        }
    }
}