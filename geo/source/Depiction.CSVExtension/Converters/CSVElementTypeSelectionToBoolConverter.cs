﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.CSVExtension.ViewModels;

namespace Depiction.CSVExtension.Converters
{
    public class CSVElementTypeSelectionToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CSVElementTypeSelection)
            {
                var type = parameter.ToString();
                var screen = ((CSVElementTypeSelection)value).ToString().ToLower();
                if (type.ToLower().Equals(screen))
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            bool useValue = (bool)value;
            string targetValue = parameter.ToString();
            if (useValue)
                return Enum.Parse(targetType, targetValue);

            return null;

//            if (value is bool)
//            {
//
//                var selected = (bool)value;
//                if (selected)
//                    return (CSVElementTypeSelection)Enum.Parse(typeof(CSVElementTypeSelection), parameter.ToString());
//            }
//            return CSVElementTypeSelection.CSVDefined;
        }
    }
}