using System;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.CoreModel.DepictionObjects.Elements;

namespace Depiction.CSVExtension
{
    public static class CSVElementSynchronizationService
    {
        public static class ElementIDGenerator
        {
            private static int count;
            public static string GenerateElementID()
            {
                count++;
                return DateTime.Now.ToString("yyyyMMddHHmm") + count;
            }
        }
        public static void PrepareElementForSynchronization(IDepictionElementBase element)
        {
            object elementID;
            if (!element.GetPropertyValue("EID", out elementID))
            {
                var prop = new DepictionElementProperty("EID", ElementIDGenerator.GenerateElementID());
                prop.Editable = false;
                prop.VisibleToUser = true;
                prop.Deletable = false;
                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                return;
            }

            //Hack to deal with eid's that end of as numbers
            if (!(elementID is string))
            {
                element.RemovePropertyWithInternalName("eid", false);
                var prop = new DepictionElementProperty("EID", elementID.ToString());
                prop.Editable = false;
                prop.VisibleToUser = true;
                prop.Deletable = false;
                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                return;
            }
        }
    }
}