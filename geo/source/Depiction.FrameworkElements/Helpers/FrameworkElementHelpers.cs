﻿using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace Depiction.FrameworkElements.Helpers
{
    public static class FrameworkElementHelpers
    {
        #region Help a flow document find its links
        internal static List<string> listOfLinkIdentifiers = new List<string> { "www", "http", "https", ".com", ".org", ".net", ".gov" };
        public static void IdentifyHyperlinks(FlowDocument inText)
        {
            TextPointer navigator = inText.ContentStart;
            while (navigator != null)
            {
                TextRange wordRange = WordBreaker.GetWordRange(navigator);

                //Now actually waits for the end of the flow document to finish//Better question
                //Why was this in here? what did it do or stop?
                if (wordRange == null) break;
                //                if (wordRange == null ||
                //                    (wordRange.IsEmpty && string.IsNullOrEmpty(navigator.GetTextInRun(LogicalDirection.Forward))))
                //                {// No more words in the document.
                //                    break;
                //                }
                string wordText = wordRange.Text;
                bool linkMatch = false;
                foreach (var link in listOfLinkIdentifiers)
                {
                    //var match = Regex.Matches(wordText, link, RegexOptions.IgnoreCase);
                    var match = wordText.ToLower().Contains(link);
                    if (match)//.Count >= 1)
                    {
                        linkMatch = true;
                    }
                }

                if (linkMatch &&
                    !HyperlinkHelper.IsInHyperlinkScope(wordRange.Start) &&
                    !HyperlinkHelper.IsInHyperlinkScope(wordRange.End))
                {
                    var hyperlink = new SuperHyperlink(wordRange.Start, wordRange.End);
                    hyperlink.NavigateUri = TurnTextToLink(wordText);
                    navigator = hyperlink.ElementEnd.GetNextInsertionPosition(LogicalDirection.Forward);
                }
                else
                {
                    navigator = wordRange.End.GetNextInsertionPosition(LogicalDirection.Forward);
                }
            }
        }

        static public Uri TurnTextToLink(string text)
        {
            var link = text;
            if (link.StartsWith("("))
            {
                var index = link.LastIndexOf(")");
                link = link.Substring(1, index - 1);
            }
            if (!link.StartsWith("http://"))
            {
                link = "http://" + link;
            }
            try
            {
                return new Uri(link, UriKind.RelativeOrAbsolute);
            }
            catch
            {
                //Bad things happen when the link is between parethesis or something of the sort
            }
            return null;
        }

        #endregion
    }
}
