﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Navigation;
using Depiction.FrameworkElements.Helpers;

namespace Depiction.FrameworkElements
{
    public class SuperRichTextBox : RichTextBox
    {
       
        static List<string> listOfWordDividers = new List<string> { " ", "\n" };
        /// <summary>
        /// Identifies the <see cref="Document"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty DocumentProperty = DependencyProperty.Register("Document",
                                                                                                 typeof(FlowDocument), typeof(SuperRichTextBox), new PropertyMetadata(null, DocumentChangeCallback));


        static SuperRichTextBox()
        {
            // KeyDown event.
            EventManager.RegisterClassHandler(typeof(SuperRichTextBox), KeyDownEvent, new KeyEventHandler(OnKeyDown), /*handledEventsToo*/true);
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="SuperRichTextBox"/> class.
        /// </summary>
        public SuperRichTextBox()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SuperRichTextBox"/> class.
        /// </summary>
        /// <param title="document">A <see cref="T:System.Windows.Documents.FlowDocument"></see> to be added as the initial contents of the new <see cref="T:System.Windows.Controls.BindableRichTextBox"></see>.</param>
        public SuperRichTextBox(FlowDocument document)
            : base(document)
        {
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.FrameworkElement.Initialized"></see> event. This method is invoked whenever <see cref="P:System.Windows.FrameworkElement.IsInitialized"></see> is set to true internally.
        /// </summary>
        /// <param title="e">The <see cref="T:System.Windows.RoutedEventArgs"></see> that contains the event data.</param>
        protected override void OnInitialized(EventArgs e)
        {
            // Hook up to get notified when DocumentProperty changes.
            DependencyPropertyDescriptor descriptor = DependencyPropertyDescriptor.FromProperty(DocumentProperty, typeof(SuperRichTextBox));
            descriptor.AddValueChanged(this, delegate
                                                 {
                                                     // If the underlying value of the dependency property changes,
                                                     // update the underlying document, also.
                                                     base.Document = (FlowDocument)GetValue(DocumentProperty);
                                                 });

            // By default, we support updates to the source when focus is lost (or, if the LostFocus
            // trigger is specified explicity.  We don't support the PropertyChanged trigger right now.
            LostFocus += BindableRichTextBox_LostFocus;
            PreviewMouseLeftButtonDown += SuperRichTextBox_PreviewMouseLeftButtonDown;
            base.OnInitialized(e);
        }

        void SuperRichTextBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                var run = e.OriginalSource as Run;
                if (run != null)
                {
                    var link = run.Text.ToLower();
                    if (!link.StartsWith("http://"))
                    {
                        link = "http://" + link;
                    }

                    try
                    {
                        var uri = new Uri(link, UriKind.RelativeOrAbsolute);
                        Process.Start(uri.ToString());
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        /// <summary>
        /// Handles the LostFocus event of the BindableRichTextBox control.
        /// </summary>
        /// <param title="sender">The source of the event.</param>
        /// <param title="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        void BindableRichTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            // If we have a binding that is set for LostFocus or Default (which we are specifying as default)
            // then update the source.
            Binding binding = BindingOperations.GetBinding(this, DocumentProperty);
            if (binding.UpdateSourceTrigger == UpdateSourceTrigger.Default ||
                binding.UpdateSourceTrigger == UpdateSourceTrigger.LostFocus)
            {
                BindingOperations.GetBindingExpression(this, DocumentProperty).UpdateSource();
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="T:System.Windows.Documents.FlowDocument"></see> that represents the contents of the <see cref="T:System.Windows.Controls.SuperRichTextBox"></see>.
        /// </summary>
        /// <value></value>
        /// <returns>A <see cref="T:System.Windows.Documents.FlowDocument"></see> object that represents the contents of the <see cref="T:System.Windows.Controls.SuperRichTextBox"></see>.By default, this property is set to an empty <see cref="T:System.Windows.Documents.FlowDocument"></see>.  Specifically, the empty <see cref="T:System.Windows.Documents.FlowDocument"></see> contains a single <see cref="T:System.Windows.Documents.Paragraph"></see>, which contains a single <see cref="T:System.Windows.Documents.Run"></see> which contains no text.</returns>
        /// <exception cref="T:System.ArgumentException">Raised if an attempt is made to set this property to a <see cref="T:System.Windows.Documents.FlowDocument"></see> that represents the contents of another <see cref="T:System.Windows.Controls.RichTextBox"></see>.</exception>
        /// <exception cref="T:System.ArgumentNullException">Raised if an attempt is made to set this property to null.</exception>
        /// <exception cref="T:System.InvalidOperationException">Raised if this property is set while a change block has been activated.</exception>
        public new FlowDocument Document
        {
            get { return (FlowDocument)GetValue(DocumentProperty); }
            set { SetValue(DocumentProperty, value); }
        }


        private static void DocumentChangeCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var superTextbox = d as SuperRichTextBox;
            if (superTextbox == null) return;
            FrameworkElementHelpers.IdentifyHyperlinks(superTextbox.Document);
        }
        #region almost Totally copied from a Microsoft example

        /// <summary>
        /// Event handler for KeyDown event to auto-detect hyperlinks on space, enter and backspace keys. 
        /// </summary>
        private static void OnKeyDown(object sender, KeyEventArgs e)
        {
            SuperRichTextBox myRichTextBox = (SuperRichTextBox)sender;

            if (e.Key != Key.Space && e.Key != Key.Back && e.Key != Key.Return)
            {
                return;
            }

            if (!myRichTextBox.Selection.IsEmpty)
            {
                myRichTextBox.Selection.Text = String.Empty;
            }

            TextPointer caretPosition = myRichTextBox.Selection.Start;

            if (e.Key == Key.Space || e.Key == Key.Return)
            {
                TextPointer wordStartPosition;
                string word = GetPreceedingWordInParagraph(caretPosition, out wordStartPosition);

                foreach (var link in FrameworkElementHelpers.listOfLinkIdentifiers)
                {
                    var match = word.ToLower().Contains(link);//((Regex.Matches(word, link, RegexOptions.IgnoreCase)))reCase));
                    if (match)
                    {
                        try
                        {
                            new SuperHyperlink(
                                wordStartPosition.GetPositionAtOffset(0, LogicalDirection.Backward),
                                caretPosition.GetPositionAtOffset(0, LogicalDirection.Forward));
                        }
                        catch (Exception ex) { }

                        break;
                    }
                }
            }
            else // Key.Back
            {
                TextPointer backspacePosition = caretPosition.GetNextInsertionPosition(LogicalDirection.Backward);
                SuperHyperlink hyperlink;
                if (backspacePosition != null && IsHyperlinkBoundaryCrossed(caretPosition, backspacePosition, out hyperlink))
                {
                    // Remember caretPosition with forward gravity. This is necessary since we are going to delete 
                    // the hyperlink element preceeding caretPosition and after deletion current caretPosition 
                    // (with backward gravity) will follow content preceeding the hyperlink. 
                    // We want to remember content following the hyperlink to set new caret position at.

                    TextPointer newCaretPosition = caretPosition.GetPositionAtOffset(0, LogicalDirection.Forward);

                    // Deleting the hyperlink is done using logic below.

                    // 1. Copy its children Inline to a temporary array.
                    InlineCollection hyperlinkChildren = hyperlink.Inlines;
                    Inline[] inlines = new Inline[hyperlinkChildren.Count];
                    hyperlinkChildren.CopyTo(inlines, 0);

                    // 2. Remove each child from parent hyperlink element and insert it after the hyperlink.
                    for (int i = inlines.Length - 1; i >= 0; i--)
                    {
                        hyperlinkChildren.Remove(inlines[i]);
                        hyperlink.SiblingInlines.InsertAfter(hyperlink, inlines[i]);
                    }

                    // 3. Apply hyperlink's local formatting properties to inlines (which are now outside hyperlink scope).
                    LocalValueEnumerator localProperties = hyperlink.GetLocalValueEnumerator();
                    TextRange inlineRange = new TextRange(inlines[0].ContentStart, inlines[inlines.Length - 1].ContentEnd);

                    while (localProperties.MoveNext())
                    {
                        LocalValueEntry property = localProperties.Current;
                        DependencyProperty dp = property.Property;
                        object value = property.Value;

                        if (!dp.ReadOnly &&
                            dp != Inline.TextDecorationsProperty && // Ignore hyperlink defaults.
                            dp != TextElement.ForegroundProperty &&
                            dp != ToolTipProperty &&
                            !IsHyperlinkProperty(dp))
                        {
                            //This is to catch the strange ishyperlinkpressed exception
                            try
                            {
                                inlineRange.ApplyPropertyValue(dp, value);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }

                    // 4. Delete the (empty) hyperlink element.
                    hyperlink.SiblingInlines.Remove(hyperlink);

                    // 5. Update selection, since we deleted Hyperlink element and caretPosition was at that Hyperlink's end boundary.
                    myRichTextBox.Selection.Select(newCaretPosition, newCaretPosition);
                }
            }
        }

        // Helper that returns true when passed property applies to Hyperlink only.
        private static bool IsHyperlinkProperty(DependencyProperty dp)
        {
            return dp == Hyperlink.CommandProperty ||
                   dp == Hyperlink.CommandParameterProperty ||
                   dp == Hyperlink.CommandTargetProperty ||
                   dp == Hyperlink.NavigateUriProperty ||
                   dp == Hyperlink.TargetNameProperty;
        }

        // Helper that returns true if passed caretPosition and backspacePosition cross a hyperlink end boundary
        // (under the assumption that caretPosition and backSpacePosition are adjacent insertion positions).
        private static bool IsHyperlinkBoundaryCrossed(TextPointer caretPosition, TextPointer backspacePosition, out SuperHyperlink backspacePositionHyperlink)
        {
            SuperHyperlink caretPositionHyperlink = GetHyperlinkAncestor(caretPosition);
            backspacePositionHyperlink = GetHyperlinkAncestor(backspacePosition);

            return (caretPositionHyperlink == null && backspacePositionHyperlink != null) ||
                   (caretPositionHyperlink != null && backspacePositionHyperlink != null && caretPositionHyperlink != backspacePositionHyperlink);
        }

        // Helper that returns a hyperlink ancestor of passed position.
        private static SuperHyperlink GetHyperlinkAncestor(TextPointer position)
        {
            Inline parent = position.Parent as Inline;
            while (parent != null && !(parent is SuperHyperlink))
            {
                parent = parent.Parent as Inline;
            }

            return parent as SuperHyperlink;
        }

        // Helper that returns a word preceeding the passed position in its paragraph, 
        // wordStartPosition points to the start position of word.
        private static string GetPreceedingWordInParagraph(TextPointer position, out TextPointer wordStartPosition)
        {
            wordStartPosition = null;
            string word = String.Empty;

            Paragraph paragraph = position.Paragraph;
            if (paragraph != null)
            {
                TextPointer navigator = position;
                while (navigator.CompareTo(paragraph.ContentStart) > 0)
                {
                    string runText = navigator.GetTextInRun(LogicalDirection.Backward);

                    string breakChar = string.Empty;
                    foreach (var breaker in listOfWordDividers)
                    {
                        if (runText.Contains(breaker))
                        {
                            breakChar = breaker;
                            break;
                        }
                    }
                    if (!string.IsNullOrEmpty(breakChar))//runText.Contains(" ")) // Any globalized application would need more sophisticated word break testing.
                    {
                        int index = runText.LastIndexOf(breakChar);//" ");
                        word = runText.Substring(index + 1, runText.Length - index - 1) + word;
                        wordStartPosition = navigator.GetPositionAtOffset(-1 * (runText.Length - index - 1));
                        break;
                    }
                    else
                    {
                        wordStartPosition = navigator;
                        word = runText + word;
                    }
                    navigator = navigator.GetNextContextPosition(LogicalDirection.Backward);
                }
            }

            return word;
        }
        #endregion
    }

    public class SuperHyperlink : Hyperlink
    {

        public SuperHyperlink()
        {
            ToolTip = "Ctrl Click to Open";

            //seems to be working without this now
            //RequestNavigate += SuperHyperlink_RequestNavigate;
        }

        public SuperHyperlink(TextPointer start, TextPointer end)
            : base(start, end)
        {
            ToolTip = "Ctrl Click to Open";
            //seems to be working without this now
            //RequestNavigate += SuperHyperlink_RequestNavigate;
        }

        void SuperHyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            //            NavigationWindow window = new NavigationWindow();
            //            window.Source = e.Uri;
            //            window.Show();

         //   Process.Start(e.Uri.ToString());
        }
    }

    public static class HyperlinkHelper
    {
        // Helper that returns true when passed property applies to Hyperlink only.
        public static bool IsHyperlinkProperty(DependencyProperty dp)
        {
            return dp == Hyperlink.CommandProperty ||
                   dp == Hyperlink.CommandParameterProperty ||
                   dp == Hyperlink.CommandTargetProperty ||
                   dp == Hyperlink.NavigateUriProperty ||
                   dp == Hyperlink.TargetNameProperty;
        }

        // Helper that returns true if passed caretPosition and backspacePosition cross a hyperlink end boundary
        // (under the assumption that caretPosition and backSpacePosition are adjacent insertion positions).
        public static bool IsHyperlinkBoundaryCrossed(TextPointer caretPosition, TextPointer backspacePosition, out Hyperlink backspacePositionHyperlink)
        {
            Hyperlink caretPositionHyperlink = GetHyperlinkAncestor(caretPosition);
            backspacePositionHyperlink = GetHyperlinkAncestor(backspacePosition);

            return (caretPositionHyperlink == null && backspacePositionHyperlink != null) ||
                   (caretPositionHyperlink != null && backspacePositionHyperlink != null && caretPositionHyperlink != backspacePositionHyperlink);
        }

        // Helper that returns a Hyperlink ancestor of passed TextPointer.
        private static Hyperlink GetHyperlinkAncestor(TextPointer position)
        {
            Inline parent = position.Parent as Inline;
            while (parent != null && !(parent is Hyperlink))
            {
                parent = parent.Parent as Inline;
            }

            return parent as Hyperlink;
        }

        // Helper that returns true when passed TextPointer is within the scope of a Hyperlink element.
        public static bool IsInHyperlinkScope(TextPointer position)
        {
            return GetHyperlinkAncestor(position) != null;
        }
    }
}