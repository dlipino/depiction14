#include "TerrainLayer.h"


namespace DepictionTerrainLayer
{

	vtElevationGrid* TerrainLayer::MyCropElevationGrid(vtElevationGrid* newGrid, double topLeftX, double topLeftY, 
														double bottomRightX, double bottomRightY, int* gridWidth, int* gridHeight, bool useNewSpacing)

	{


		DPoint2 spacing;
			
		DRECT rect;
		DLine2 line;
		DPoint2 pointBottomLeft;
		DPoint2 pointTopLeft;
		DPoint2 pointTopRight;
		DPoint2 pointBottomRight;
		
		//calculate current spacing
		spacing = m_pGrid->GetSpacingD();
		DPoint2 newSpacing = newGrid->GetSpacingD();
		
		//set the new corners to be the passed extent values
		rect.top = topLeftY; 
		rect.bottom = bottomRightY; 
		rect.left = topLeftX; 
		rect.right = bottomRightX;

		
		pointBottomLeft.x	= rect.left;
		pointBottomLeft.y   = rect.bottom;
		pointTopLeft.x	= rect.left;
		pointTopLeft.y  = rect.top;
		pointTopRight.x = rect.right;
		pointTopRight.y = rect.top;
		pointBottomRight.x = rect.right;
		pointBottomRight.y = rect.bottom;


		line.SetAt(0,pointBottomLeft);
		line.SetAt(1,pointTopLeft);
		line.SetAt(2,pointTopRight);
		line.SetAt(3,pointBottomRight);

		int numCols, numRows;
		
		numCols = 1 + 0.01 + fabs(rect.right - rect.left) / ((useNewSpacing) ? newSpacing.x : spacing.x);
		numRows = 1 + 0.01 + fabs(rect.top - rect.bottom) / ((useNewSpacing) ? newSpacing.y : spacing.y);

		*gridWidth = numCols;
		*gridHeight = numRows;
		
		float xCoord, yCoord;
		DPoint2 pointCoord;
		float elevValue;

		
		
		vtElevationGrid* tempGrid = new vtElevationGrid(rect, numCols, numRows, true, newGrid->GetProjection());
		if(!tempGrid->IsValidGrid()) return NULL;
		

		for(int i = 0; i < numCols; i++)
		{
			for(int j = 0;j < numRows; j++)
			{
				
				pointCoord.x = rect.left + i * (newSpacing.x);
				pointCoord.y = rect.bottom + j * (newSpacing.y);
				//
				//if pointCoord is outside the bounds of newGrid
				//
				if (pointCoord.x < pointTopLeft.x || pointCoord.x > pointBottomRight.x) 
					elevValue = NODATA;
				if (pointCoord.y > pointTopLeft.y || pointCoord.y < pointBottomRight.y) 
					elevValue = NODATA;
				else
					elevValue = newGrid->GetClosestValue(pointCoord);

				if (elevValue == UNKNOWN || elevValue == NODATA)//see if the original grid had value at this point
				{
					elevValue = m_pGrid->GetClosestValue(pointCoord);
					//if it is still NODATA, do nothing...SimioGeo has to handle it there
					//Bharath March 17 2008
					//if(elevValue==NODATA)
					//elevValue=0;
				}
				tempGrid->SetFValue(i, j, elevValue);
			}
		}
		return tempGrid;
	}

	///<summary>
	//Crops the current elevation grid to within the bounds specified.
	//Replaces the existing grid with this resampled grid.
	//Pass in the topleft and bottom right coordinates in lat lon.
	///</summary>
	void TerrainLayer::CropElevationGrid(double topLeftX, double topLeftY, double bottomRightX, double bottomRightY)
	{
		int width, height;
		if(m_pGrid==NULL) return;

		//vtElevationGrid* tempGrid = m_pGrid->ResampleElevationGrid(m_pGrid,topLeftX,topLeftY,bottomRightX,bottomRightY,&width,&height);
		vtElevationGrid* tempGrid = TransformAndCrop(m_pGrid, topLeftX, topLeftY, bottomRightX, bottomRightY, &width, &height);
		if (tempGrid == NULL) return;

		m_imageWidth = width;
		m_imageHeight = height;

		m_pGrid->FreeData();
		delete m_pGrid;
		m_pGrid = tempGrid;
	}

	vtElevationGrid* TerrainLayer::MyResampleElevationGrid(vtElevationGrid* newGrid, int* gridWidth, int* gridHeight, bool useExistingGridSpacing)
	{
		//RESAMPLE newGrid into m_pGrid

		DLine2 line;
		DRECT rect;
		
		DLine2 oldCorners;
		m_pGrid->GetCorners(oldCorners,false);

		DPoint2 pointBottomLeft	= oldCorners.GetAt(0);
		DPoint2 pointTopLeft	= oldCorners.GetAt(1);
		DPoint2 pointTopRight	= oldCorners.GetAt(2);
		DPoint2 pointBottomRight= oldCorners.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		vtProjection proj = m_pGrid->GetProjection();
		vtProjection newGridProjection = newGrid->GetProjection();

		DPoint2 spacing;
			
		
		//Existing SPACING of the original grid

		int widthRef;
		int heightRef;

		m_pGrid->GetDimensions(widthRef,heightRef);
		//calculate current spacing

		spacing.x = fabs(rect.right-rect.left) / (widthRef - 1);
		spacing.y = fabs(rect.top-rect.bottom) / (heightRef - 1);


		DLine2 newCorners;
		newGrid->GetCorners(newCorners, false);
		DRECT newBoundsRect;
		DPoint2 pointBottomLeft1	= newCorners.GetAt(0);
		DPoint2 pointTopLeft1	= newCorners.GetAt(1);
		DPoint2 pointTopRight1	= newCorners.GetAt(2);
		DPoint2 pointBottomRight1= newCorners.GetAt(3);

		//convert the corners from newgrid's coord system to m_pGrid's (mercator)
		//
		if (proj != newGridProjection)
		{
			m_pGrid->TransformPoint(&newGridProjection, &proj, &pointBottomLeft1);
			m_pGrid->TransformPoint(&newGridProjection, &proj, &pointTopLeft1);
			m_pGrid->TransformPoint(&newGridProjection, &proj, &pointTopRight1);
			m_pGrid->TransformPoint(&newGridProjection, &proj, &pointBottomRight1);
		}
		DPoint2 newSpacing;
		
		int newWidthRef;
		int newHeightRef;
		newGrid->GetDimensions(newWidthRef,newHeightRef);

		newSpacing.x = (double)fabs(pointBottomRight1.x - pointTopLeft1.x) / (double)(newWidthRef - 1);
		newSpacing.y = (double)fabs(pointTopLeft1.y - pointBottomRight1.y) / (double)(newHeightRef - 1);

		//set new X and Y dimensions
		int numCols, numRows;

		//instead of determining which grid's spacing to use based on resolution
		//use the passed boolean useNewSpacing to determine that

		//Bharath Apr 8 2008
		bool useOldGrid = useExistingGridSpacing; //(spacing.x<newSpacing.x)?true:false;

		//choose the resolution based on the grid with the higher resolution
		numCols = 1 + 0.001 + ((double)fabs(rect.right - rect.left) / (double)((useOldGrid) ? spacing.x : newSpacing.x));
		numRows = 1 + 0.001 + ((double)fabs(rect.top - rect.bottom) / (double)((useOldGrid) ? spacing.y : newSpacing.y));

		//useNewSpacing is always true...the convention in Depiction is that the new grid's spacing overrides
		//the old/existing grid's.
		//
		//verify that numCols does not exceed, say, a max value (4000)?
		//this way, we can handle LARGE depiction regions where a hires elevation is loaded for a small subregion
		//when the whole Depiction region is sampled at the hi resolution, we get HUMONGOUS elevation grid sizes...
		//
		//Bharath Feb 11 2009
		//
		if(numCols > 4000)
		{
			m_downSampled = true;
			double aspect = (double)numCols / numRows;
			numCols = 4000;
			numRows = numCols / aspect;
			//modify newSpacing
			
			newSpacing.x = (double)fabs(rect.right - rect.left) / (double)(numCols - 1 - 0.001);
			newSpacing.y = (double)fabs(rect.top - rect.bottom) / (double)(numRows - 1 - 0.001);

		}
		*gridWidth = numCols;
		*gridHeight = numRows;

		vtElevationGrid* tempGrid = new vtElevationGrid(rect, numCols, numRows, true, proj);
		if(!tempGrid->IsValidGrid())
		{
			delete tempGrid;
			return NULL;
		}

		float xCoord, yCoord;
		DPoint2 pointCoord;
		float elevValue;
		

		double workingSpacingX, workingSpacingY;
		workingSpacingX = (useOldGrid) ? spacing.x : newSpacing.x;
		workingSpacingY = (useOldGrid) ? spacing.y : newSpacing.y;

		//proj is the existing grid's projection
		//newGridProjection is newGrid's projection

		for(int i = 0; i < numCols; i++)
		{
			for(int j = 0; j < numRows; j++)
			{
			
				//pointCoord is in geo coord system
				pointCoord.x = rect.left + i * workingSpacingX;
				pointCoord.y = rect.bottom + j * workingSpacingY;

				elevValue = newGrid->GetClosestValue(pointCoord);
				
				if (elevValue == UNKNOWN || elevValue == NODATA)
					elevValue = m_pGrid->GetFilteredValue(pointCoord);

				tempGrid->SetFValue(i, j, elevValue);
			}
		}
		return tempGrid;
	}

	void TerrainLayer::ResampleElevationGrid(int numRows, int numCols)
	{
		DLine2 line;
		m_pGrid->GetCorners(line, false);
		int size = line.GetSize();

		DRECT rect;
		DPoint2 pointBottomLeft	= line.GetAt(0);
		DPoint2 pointTopLeft	= line.GetAt(1);
		DPoint2 pointTopRight	= line.GetAt(2);
		DPoint2 pointBottomRight= line.GetAt(3);

		rect.top = pointTopLeft.y;
		rect.bottom = pointBottomRight.y;
		rect.left = pointTopLeft.x;
		rect.right = pointTopRight.x;

		vtProjection proj = m_pGrid->GetProjection();

		//preserve aspect ratio?
		float ratio = (float)numCols / (float)m_imageWidth;
		numRows = ratio * m_imageHeight;

		vtElevationGrid* tempGrid = new vtElevationGrid(rect, numCols, numRows, true, proj);
		if(!tempGrid->IsValidGrid()) 
		{
			delete tempGrid;
			return;
		}
		DPoint2 spacing = tempGrid->GetSpacingD();

		spacing.x = fabs(rect.right - rect.left) / (numCols - 1);
		spacing.y = fabs(rect.top - rect.bottom) / (numRows - 1);

		tempGrid->SetCorners(line);
		tempGrid->SetEarthExtents(rect);

		float xCoord, yCoord;
		DPoint2 pointCoord;
		float elevValue;

		int row, col;
		for (int j = 0; j < numRows; j++)
		{
			for (int i = 0; i < numCols; i++)
			{
				col = (int)((double)i / ratio);
				row = (int)((double)j / ratio);
				elevValue = GetElevationFromGridCoordinates(col, row);
				//Leave the elevation value as is when it has NO DATA
				//if(elevValue==NODATA)elevValue=0;
				tempGrid->SetFValue(i, (numRows-j-1), elevValue);
			}
		}

		//replace the existing grid
		m_pGrid->FreeData();
		delete m_pGrid;
		m_pGrid = tempGrid;
	}

	void TerrainLayer::ResampleElevationGrid(vtElevationGrid* newGrid, bool useExistingGridSpacing)
	{
		int width, height;
		m_downSampled = false;
		vtElevationGrid* tempGrid = /*m_pGrid->*/MyResampleElevationGrid(newGrid, &width, &height, useExistingGridSpacing);//MyResample(newGrid,&width,&height); //
		m_imageWidth = width;
		m_imageHeight = height;

		if (tempGrid != NULL)
		{
			m_pGrid->FreeData();
			delete m_pGrid;
			m_pGrid = tempGrid;
		}
	}

	vtElevationGrid* TerrainLayer::TransformAndCrop(vtElevationGrid* newGrid, double topLeftX, double topLeftY, 
														double bottomRightX, double bottomRightY, int* gridWidth, int* gridHeight)
	{
		vtProjection oldProj = m_pGrid->GetProjection();
		vtProjection newProj = newGrid->GetProjection();

		vtProjection *projGeo;
		projGeo = new vtProjection();
		projGeo->SetGeogCSFromDatum(NAD83);//NAD83

		DPoint2 newTopLeft;
		//transform topleft corner of the cropping rectangle
		//from geog coord system to whatever the new grid is in
		newTopLeft.x = topLeftX; newTopLeft.y = topLeftY;

		if(!(*projGeo == newProj))
		newGrid->TransformPoint(projGeo,&newProj,&newTopLeft);

		//transform bottom right
		DPoint2 newBottomRight;
		newBottomRight.x = bottomRightX; newBottomRight.y = bottomRightY;
		
		if(!(*projGeo == newProj))
		newGrid->TransformPoint(projGeo,&newProj,&newBottomRight);

		int newWidth, newHeight;
		//crop the new grid to the extents of the existing grid
		//this is so that LARGE files can be read in and transformed
		//to the GEOG COORD SYSTEM relatively quickly

		//last passed parameter says to use the spacing of the new grid
		vtElevationGrid* tempGrid= newGrid->CropElevationGrid(newGrid,newTopLeft.x,newTopLeft.y,newBottomRight.x,newBottomRight.y,&newWidth,&newHeight, USENEWGRIDSPACING);
		
		*gridWidth = newWidth;
		*gridHeight = newHeight;
		return tempGrid;
	}
	
	bool TerrainLayer::HasBeenDownSampled()
	{
		return m_downSampled;
	}

}