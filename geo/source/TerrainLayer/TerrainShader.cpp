#include "TerrainLayer.h"


namespace DepictionTerrainLayer
{
		TerrainColorMap::TerrainColorMap()
	{
		m_bBlend = true;
		m_bRelative = true;
	}

	/**
	* Add a color entry, keeping the elevation values sorted.
	*/
	void TerrainColorMap::Add(float elev, const RGBi &color)
	{
		float next, previous = -1E9;
		int size = m_elev.size();
		for (int i = 0; i < size+1; i++)
		{
			if (i < size)
				next = m_elev[i];
			else
				next = 1E9;
			if (previous <= elev && elev <= next)
			{
				m_elev.insert(m_elev.begin() + i, elev);
				m_color.insert(m_color.begin() + i, color);
				return;
			}
		}
	}

	bool TerrainColorMap::Load(const char *fname)
	{
		FILE *fp = vtFileOpen(fname, "rb");
		if (!fp)
			return false;

		char buf[80];
		fgets(buf, 80, fp);
		if (strncmp(buf, "colormap1", 9))
			return false;

		while (fgets(buf, 80, fp) != NULL)
		{
			if (!strncmp(buf, "blend", 5))
				sscanf(buf, "blend: %d\n", &m_bBlend);

			else if (!strncmp(buf, "relative", 8))
				sscanf(buf, "relative: %d\n", &m_bRelative);

			else if (!strncmp(buf, "size", 4))
			{
				int size;
				sscanf(buf, "size %d\n", &size);

				m_elev.resize(size);
				m_color.resize(size);
				for (int i = 0; i < size; i++)
				{
					float f;
					short r, g, b;
					fscanf(fp, "\telev %f color %hd %hd %hd\n", &f, &r, &g, &b);
					m_elev[i] = f;
					m_color[i].Set(r, g, b);
				}
			}
		}
		fclose(fp);
		return true;
	}

	void TerrainColorMap::RemoveAt(int num)
	{
		m_elev.erase(m_elev.begin()+num);
		m_color.erase(m_color.begin()+num);
	}

	int TerrainColorMap::Num() const
	{
		return m_elev.size();
	}

	/**
	* Generate an array of interpolated colors from this TerrainColorMap.
	*
	* \param table An empty table ready to fill with the interpolated colors.
	* \param iTableSize The desired number of elements in the resulting table.
	* \param fMin, fMax The elevation range to interpolate over.
	*/
	void TerrainColorMap::GenerateColors(std::vector<RGBi> &table, int iTableSize,
		float fMin, float fMax) const
	{
		if (m_color.size() < 2)
			return;

		float fRange = fMax - fMin;
		float step = fRange/iTableSize;

		int current = 0;
		int num = Num();
		RGBi c1, c2;
		float base=0, next, bracket_size=0, fraction;

		if (m_bRelative == true)
		{
			bracket_size = fRange / (num - 1);
			current = -1;
		}

		RGBi c3;
		for (int i = 0; i < iTableSize; i++)
		{
			float elev = fMin + (step * i);
			if (m_bRelative)
			{
				// use regular divisions
				int bracket = (int) ((elev-fMin) / fRange * (num-1));
				if (bracket != current)
				{
					current = bracket;
					base = fMin + bracket * bracket_size;
					c1 = m_color[current];
					c2 = m_color[current+1];
				}
			}
			else
			{
				// use absolute elevations
				while (current < num-1 && elev >= m_elev[current])
				{
					c1 = m_color[current];
					c2 = m_color[current+1];
					base = m_elev[current];
					next = m_elev[current+1];
					bracket_size = next - base;
					current++;
				}
			}
			if (m_bBlend)
			{
				fraction = (elev - base) / bracket_size;
				c3 = c1 * (1-fraction) + c2 * fraction;
			}
			else
				c3 = c1;
			table.push_back(c3);
		}

		// Add one to catch top data
		c3 = table[iTableSize-1];
		table.push_back(c3);
	}	
	bool TerrainLayer::ColorDIBFromTable(vtDIB *pBM,
		std::vector<RGBi> &table, float fMin, float fMax,
		bool progress_callback(int))
	{
		int w = pBM->GetWidth();
		int h = pBM->GetHeight();
		int gw, gh;
		gw = w;
		gh = h;

		float fRange = fMax - fMin;
		unsigned int iGranularity = table.size()-1;

		// now iterate over the texels
		float elev;
		bool has_invalid = false;
		RGBi c3;
		int i, j;
		double x, y;
		RGBi color;

		double ratiox = (double)(gw-1)/(w-1), ratioy = (double)(gh-1)/(h-1);

		for (i = 0; i < w; i++)
		{
			if (progress_callback != NULL)
			{
				if ((i&7) == 0)
					progress_callback(i * 100 / w);
			}
			x = i * ratiox;		// find corresponding location in height grid

			for (j = 0; j < h; j++)
			{
				y = j * ratioy;

				elev = GetElevationFromGridCoordinates(x,y);
				if (elev == UNKNOWN || elev == NODATA)
				{
					pBM->SetPixel24(i, h-1-j, RGBi(255,0,0));
					has_invalid = true;
					continue;
				}
				unsigned int table_entry = (unsigned int) ((elev - fMin) / fRange * iGranularity);
				if (table_entry > iGranularity-1)
					table_entry = iGranularity-1;
				c3 = table[table_entry];
				pBM->SetPixel24(i, h-1-j, c3);
			}
		}
		return has_invalid;
	}

	/* Quickly produce a shading-like effect by scanning over the bitmap once,
	* using the east-west slope to produce lightening/darkening.
	* The bitmap must be the same size as the elevation grid, or a power of 2 smaller.
	*/
	bool TerrainLayer::ShadeQuick(vtDIB *pBM, float fLightFactor,
		bool bTrue, bool progress_callback(int))
	{
		int w = pBM->GetWidth();
		int h = pBM->GetHeight();

		int stepx = m_imageWidth / w;
		int stepy = m_imageHeight / h;

		int i, j;	// indices into bitmap
		int x, y;	// indices into elevation

		RGBi rgb;

		for (j = 0; j < h; j++)
		{
			if (progress_callback != NULL)
			{
				if ((j&7) == 0)
					progress_callback(j * 100 / h);
			}
			// find corresponding location in heightfield
			y = m_imageHeight-1 - (j * stepy);
			for (i = 0; i < w; i++)
			{
				pBM->GetPixel24(i, j, rgb);

				int x_offset = 0;
				if (i == w-1)
					x_offset = -1;

				x = i * stepx;
				float value = GetElevationFromGridCoordinates(x + x_offset, y);
				if (value == UNKNOWN || value == NODATA)
				{
					pBM->SetPixel24(i, j, RGBi(255, 0, 0));
					continue;
				}

				float value2 = GetElevationFromGridCoordinates(x+1 + x_offset, y);
				if (value == UNKNOWN || value == NODATA)
					value2 = value;
				short diff = (short) ((value2 - value) / stepx * fLightFactor);

				// clip to keep values under control
				if (diff > 128)
					diff = 128;
				else if (diff < -128)
					diff = -128;
				rgb.r = rgb.r + diff;
				rgb.g = rgb.g + diff;
				rgb.b = rgb.b + diff;
				if (rgb.r < 0) rgb.r = 0;
				else if (rgb.r > 255) rgb.r = 255;
				if (rgb.g < 0) rgb.g = 0;
				else if (rgb.g > 255) rgb.g = 255;
				if (rgb.b < 0) rgb.b = 0;
				else if (rgb.b > 255) rgb.b = 255;
				pBM->SetPixel24(i, j, rgb);
			}
		}
		return true;
	}

	void TerrainColorMap::SetupDefaultColors()
	{
		this->m_bRelative = false;
		this->Add(-400*17, RGBi(255, 255, 255));
		this->Add(-400*16, RGBi(20, 20, 30	));
		this->Add(-400*15, RGBi(60, 60, 70	));
		this->Add(-400*14, RGBi(120, 120, 130));
		this->Add(-400*13, RGBi(180, 185, 190));
		this->Add(-400*12, RGBi(160, 80, 0	));
		this->Add(-400*11, RGBi(128, 128, 0	));
		this->Add(-400*10, RGBi(160, 0, 160	));
		this->Add(-400* 9, RGBi(144, 64, 144	));
		this->Add(-400* 8, RGBi(128, 128, 128));
		this->Add(-400* 7, RGBi(64, 128, 60	));
		this->Add(-400* 6, RGBi(0, 128, 0	));
		this->Add(-400* 5, RGBi(0, 128, 128	));
		this->Add(-400* 4, RGBi(0, 0, 160	));
		this->Add(-400* 3, RGBi(43, 90, 142	));
		this->Add(-400* 2, RGBi(81, 121, 172	));
		this->Add(-400* 1, RGBi(108, 156, 195));
		this->Add(-1.0f,  RGBi(182, 228, 255));
		this->Add(0, RGBi(0, 0, 0xee));
		this->Add( 0.1f,  RGBi(40, 224, 40	));
		this->Add( 450* 1, RGBi(0, 128, 0	));
		this->Add( 450* 2, RGBi(100, 144, 76	));
		this->Add( 450* 3, RGBi(204, 170, 136));
		this->Add( 450* 4, RGBi(136, 100, 70	));
		this->Add( 450* 5, RGBi(128, 128, 128));
		this->Add( 450* 6, RGBi(180, 128, 64	));
		this->Add( 450* 7, RGBi(255, 144, 32	));
		this->Add( 450* 8, RGBi(200, 110, 80	));
		this->Add( 450* 9, RGBi(160, 80, 160	));
		this->Add( 450*10, RGBi(144, 40, 128	));
		this->Add( 450*11, RGBi(128, 128, 128));
		this->Add( 450*12, RGBi(255, 255, 255));
		this->Add( 450*13, RGBi(255, 255, 128));
		this->Add( 450*14, RGBi(255, 128, 0));
		this->Add( 450*15, RGBi(0, 128, 0));
	}



}