#include "TinLayer.h"
//#include "mathtypes.h"


namespace DepictionTerrainLayer
{

void TinLayer::AddVertices(System::Collections::ArrayList^  x, System::Collections::ArrayList^  y, System::Collections::ArrayList^  z, int numCols, int numRows)
{
	int numVertices = x->Count;
	double xcoord, ycoord, zvalue;
	DPoint2 point;

	m_numCols=numCols;
	m_numRows=numRows;

	for(int i=0;i<numVertices;i++)
	{
		xcoord = (double)x[i]; 
		ycoord = (double)y[i];
		zvalue = (double)z[i];
		point.x=xcoord; point.y = ycoord;
		m_tin->AddVert(point,(float)zvalue);
		/*if ((i%3)==2)
				m_tin->AddTri(i-2, i-1, i);*/
	}


}

void TinLayer::AddTriangles(System::Collections::ArrayList^ indices)
{
	int numTriangles= indices->Count / 3;

	for(int i=0;i<numTriangles;i++)
	{
		m_tin->AddTri((int)indices[i*3],(int)indices[1+i*3],(int)indices[2+i*3]);
	}

}
vtElevationGrid* TinLayer::Griddify(int xdim, int ydim,bool speed)
{
	if (m_tin->NumVerts()<=0) return NULL;

	m_tin->ComputeExtents();

	DRECT rect;
	rect=m_tin->GetEarthExtents();
	vtProjection* proj = new vtProjection();
	proj->SetGeogCSFromDatum(6269); //NAD83

	vtElevationGrid* grid = new vtElevationGrid(rect,xdim,ydim,true,*proj);

	DPoint2 spacing;
	
	spacing.x = fabs(rect.right-rect.left) / (xdim-1);
	spacing.y = fabs(rect.top-rect.bottom) / (ydim-1);

	float elevation;
	DPoint2 latlonPoint;

	//in preparation for doing numerous findaltitudes on the TIN surface

	if(speed)
	m_tin->SetupTriangleBins(xdim,NULL);
	//inclusion of this causes boundary effects when xdim is really small

	unsigned int numtris = m_tin->NumTris();

	for(int i=0;i<xdim;i++)
		for(int j=0;j<ydim;j++)
		{
			latlonPoint.x = rect.left + i * spacing.x;
			latlonPoint.y = rect.top - j*spacing.y;

			///
			if(m_tin->FindAltitudeOnEarth(latlonPoint,elevation,false))
				grid->SetFValue(i,(ydim-1-j),elevation);

		}

		return grid;
}


}