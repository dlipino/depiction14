using System;
using System.Collections.Generic;
using System.IO;
using CarbonTools.Content.Features;
using CarbonTools.Geometries;
using Depiction.API;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service.FileReading;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using OSGeo.OGR;
using Geometry = GisSharpBlog.NetTopologySuite.Geometries.Geometry;
using IGeometry = GeoAPI.Geometries.IGeometry;
using LinearRing = GisSharpBlog.NetTopologySuite.Geometries.LinearRing;
using LineString = GisSharpBlog.NetTopologySuite.Geometries.LineString;
using NetTopologyPoint = GisSharpBlog.NetTopologySuite.Geometries.Point;
using PointCollection = CarbonTools.Geometries.PointCollection;
using NetTopologyPolygon = GisSharpBlog.NetTopologySuite.Geometries.Polygon;
using OSGGeometry = OSGeo.OGR.Geometry;
using CTDataFeature = CarbonTools.Content.Features.DataFeatures;
//So the nettopology suite is what depiction uses as it reference geometry thing
//all other geometries need to be converted to that
namespace Depiction.CoreModel.Service
{
    public class DepictionCarbonToolsFileReaderService
    {
        public List<IElementPrototype> GetPrototypesFromCarbonToolsDataFeatures(CTDataFeature dataFeatures, string defaultElementType, IMapCoordinateBounds regionExtent, string description, string source)
        {

            var prototypeList = new List<IElementPrototype>();
            List<OgrPropertiesAndIGeometry> features = GetFeaturesFromCarbonToolData(dataFeatures, regionExtent);

            prototypeList = FileReadingHelpers.CreatePrototypesFromKmlFileFeatures(features, defaultElementType,
                                                                       false, null);

            return prototypeList;
        }

        /// <summary>
        /// Construct OgrFeature, which is an entirely Depiction-based element with attributes
        /// and geometry, the latter based on NetTopologySuite.
        /// Instead of using OSGeo.OGR, which is unmanaged, we use Carbontools' ItemMember
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private static OgrPropertiesAndIGeometry GetOgrFeature(ItemMember item)
        {
            var ogrFeature = new OgrPropertiesAndIGeometry();

            var member = new ItemMember { Name = item.Name };
            foreach (ItemMember mem in item.Items)
            {//for each layer
                if (mem == null) continue;
                var nestedMember = new ItemMember();
                nestedMember.Name = mem.Name;
                nestedMember.Attributes = mem.Attributes;
                var items = FeatureAnalysis.GetAllElements(mem);
                foreach (ItemElement it in items)
                {
                    nestedMember.Items.Add(it);
                }

                member.Items.Add(nestedMember);

            }
            var elems = FeatureAnalysis.GetAllElements(member);
            foreach (ItemElement elem in elems)
            {
                if (!ogrFeature.OgrProperties.ContainsKey(elem.Name))
                {
                    //var value = (elem.Value == null) ? "" : elem.Value.ToString();

                    ogrFeature.OgrProperties.Add(elem.Name, (elem.Value == null) ? "" : elem.Value);
                }
            }
            return ogrFeature;
        }


        /// <summary>
        /// Add geometry (DepictionGeometry object) to OgrFeature
        /// </summary>
        /// <param name="feature"></param>
        /// <param name="ctItemMember">Carbontools element</param>
        /// <param name="regionBounds"></param>
        /// <returns></returns>
        private static bool AddGeometryToOgrFeature(OgrPropertiesAndIGeometry feature, ItemMember ctItemMember, DepictionGeometry regionBounds)
        {
            var geomFactory = DepictionAccess.DepictionGeomFactory;
            var itemGeoms = FeatureAnalysis.GetAllGeometries(ctItemMember);
            DepictionGeometry geometry = null;
            try
            {
                foreach (ItemGeometry itemGeom in itemGeoms)
                {
                    IGeometryBase baseGeom = itemGeom.GeometryObject;
                    //Hack that works for getting at least one image type out of a kml (AI8p_85.kml)
                    if (itemGeom.Parent.Name.Contains("GroundOverlayGeometry"))
                    {//Ok big hack, creates a fake overlay bounds, there is also
                        //a rotation element that is picked up before hand.
                        //When the element is finally created from the properties
                        //it must look for the GroundOverlayGeometry in order
                        //to make it a geocoded image
                        var north = baseGeom.Extent.MaxY;//lat
                        var south = baseGeom.Extent.MinY;//lat
                        var east = baseGeom.Extent.MaxX;//long
                        var west = baseGeom.Extent.MinX;//long
                        var topLeft = new LatitudeLongitude(north,west);
                        var bottomRight = new LatitudeLongitude(south,east);
                        feature.OgrProperties.Add("GroundOverlayGeometry",new MapCoordinateBounds(topLeft,bottomRight));
                        //geometry = new DepictionGeometry(geomFactory.CreatePoint(new Coordinate(north, west)));
                        feature.GeoApiGeometry = geomFactory.CreatePoint(new Coordinate(north, west));
                        return true;
                    }

                    if (baseGeom is CarbonTools.Geometries.Envelope)
                    {
                        //do nothing
                    }
                    else if (baseGeom is CarbonTools.Geometries.Polygon)
                    {
                        geometry = ConstructPolygon((CarbonTools.Geometries.Polygon)baseGeom);

                    } //NetTopologyPolygon
                    else if (baseGeom is PolygonCollection)
                    {
                        geometry = ConstructMultiPolygon((PolygonCollection)baseGeom);

                    } //PolygonCollection
                    else if (baseGeom is CarbonTools.Geometries.Point)
                    {
                        var pt = (CarbonTools.Geometries.Point)baseGeom;
                        geometry = new DepictionGeometry(geomFactory.CreatePoint(new Coordinate(pt.X, pt.Y)));
                    } //NetTopologyPoint
                    else if (baseGeom is PointCollection)
                    {
                        var ptArray = new NetTopologyPoint[((PointCollection)baseGeom).Count];
                        foreach (CarbonTools.Geometries.Point pt in (PointCollection)baseGeom)
                        {
                            ptArray[((PointCollection)baseGeom).IndexOf(pt)] = new NetTopologyPoint(pt.X, pt.Y);
                        }
                        geometry = new DepictionGeometry(geomFactory.CreateMultiPoint(ptArray));
                    } //PointCollection
                    else if (baseGeom is CarbonTools.Geometries.LineString)
                    {

                        var lr = ((CarbonTools.Geometries.LineString)baseGeom);
                        geometry = ConstructLineString(lr);

                    } //LineString
                    else if (baseGeom is LineStringCollection)
                    {
                        geometry = ConstructMultiLineString((LineStringCollection)baseGeom);
                    } //LineStringCollection
                }
            }
            catch (Exception)
            {
                return false;
            }
            if (geometry == null) return false;

            try
            {
                if (geometry.Geometry is LineString)
                {
                    var geom = (Geometry)regionBounds.Intersection(geometry);

                    if (geom.IsEmpty)
                        return false;
                    feature.GeoApiGeometry = geom;
                }
                else if (geometry.Geometry is MultiLineString)
                {
                    IGeometry[] geoms = ((MultiLineString)geometry.Geometry).Geometries;
                    var lineStringList = new List<LineString>();
                    for (int i = 0; i < geoms.Length; i++)
                    {
                        var geom = (Geometry)regionBounds.Intersection(new DepictionGeometry(geoms[i]));

                        if (geom.IsEmpty)
                            continue;
                        if (geom is LineString) lineStringList.Add(geom as LineString);
                        else if (geom is MultiLineString)
                        {
                            foreach (LineString mLineString in ((MultiLineString)geom).Geometries)
                            {
                                lineStringList.Add(mLineString);
                            }
                        }
                    }
                    if (lineStringList.Count == 0) return false;
                    feature.GeoApiGeometry = geomFactory.CreateMultiLineString(lineStringList.ToArray());
                }
                else if (geometry.Geometry is MultiPolygon)
                {
                    int numPolys = geometry.NumGeometries;
                    IGeometry[] croppedGeoms = ((MultiPolygon)geometry.Geometry).Geometries;
                    var polylist = new List<NetTopologyPolygon>();
                    for (int i = 0; i < numPolys; i++)
                    {
                        var geom = regionBounds.Intersection(new DepictionGeometry(croppedGeoms[i]));
                        if (geom.IsEmpty)
                            continue;
                        if (geom is NetTopologyPolygon)
                            polylist.Add(geom as NetTopologyPolygon);
                        else if (geom is MultiPolygon)
                        {
                            foreach (NetTopologyPolygon mGeom in ((MultiPolygon)geom).Geometries)
                                polylist.Add(mGeom);
                        }
                    }
                    if (polylist.Count == 0) return false;

                    feature.GeoApiGeometry = geomFactory.CreateMultiPolygon(polylist.ToArray());
                }

                else if (geometry.Geometry is NetTopologyPolygon)
                {
                    var geom = (Geometry)regionBounds.Intersection(geometry);
                    if (!geom.IsEmpty)
                    {
                        feature.GeoApiGeometry = geom;
                    }

                    else return false;
                }

                else if (geometry.Geometry is NetTopologyPoint)
                {
                    var geom = (Geometry)geometry.Intersection(regionBounds);
                    if (!geom.IsEmpty)
                    {
                        feature.GeoApiGeometry = geom;
                    }
                    else return false;
                }
                else if (geometry.Geometry is MultiPoint)
                {
                    var geom = (Geometry)geometry.Intersection(regionBounds);
                    if (!geom.IsEmpty)
                    {
                        feature.GeoApiGeometry = geom;
                    }
                    else return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public List<OgrPropertiesAndIGeometry> GetFeaturesFromCarbonToolData(CTDataFeature ctData, IMapCoordinateBounds regionExtent)
        {
            var regionBoundPoly = FileReadingHelpers.ConstructRegionPolygon(regionExtent);//Thankfully this deals with null
            var featureList = new List<OgrPropertiesAndIGeometry>();
            if (ctData == null)
            {
                Console.WriteLine("Something went wrong with carbontools loading");
                return featureList;
            }
            if (ctData.Features == null || ctData.Features.Count == 0) return featureList;

            try
            {
                foreach (ItemMember item in ctData.Features)
                {
                    if (item.Items.Count == 0)
                        continue;
                    var feature = GetOgrFeature(item);

                    if (AddGeometryToOgrFeature(feature, item, regionBoundPoly))
                        featureList.Add(feature);

                }
            }
            catch (Exception)
            {
            }

            return featureList;
        }

        #region DepictionGeometry From Carbontools objects -- construction methods

        private static DepictionGeometry ConstructPolygon(CarbonTools.Geometries.Polygon ctPolygon)
        {
            if (ctPolygon != null)
            {
                NetTopologyPolygon poly = ConstructGISSharpBlogPolygon(ctPolygon);

                return new DepictionGeometry(poly);
            }
            return null;
        }

        private static NetTopologyPolygon ConstructGISSharpBlogPolygon(CarbonTools.Geometries.Polygon ctPolygon)
        {
            var geometryFactory = DepictionAccess.DepictionGeomFactory;
            CarbonTools.Geometries.LinearRing lr = ctPolygon.Exterior;
            PointCollection pts = lr.Points;

            var cList = new CoordinateList();

            foreach (CarbonTools.Geometries.Point pt in pts)
            {
                cList.Add(new Coordinate(pt.X, pt.Y));
            }

            var outerShellRing = (LinearRing)geometryFactory.CreateLinearRing(cList.ToCoordinateArray());

            //add holes, if any
            LinearRingCollection holes = ctPolygon.Interior;
            var holeRings = new LinearRing[holes.Count];

            if (holes.Count != 0)
            {
                int hole = 0;
                foreach (CarbonTools.Geometries.LinearRing hring in holes)
                {
                    pts = hring.Points;
                    var coordinateList = new CoordinateList();
                    foreach (CarbonTools.Geometries.Point pt in pts)
                    {
                        coordinateList.Add(new Coordinate(pt.X, pt.Y));
                    }

                    holeRings[hole++] = (LinearRing)geometryFactory.CreateLinearRing(coordinateList.ToCoordinateArray());
                }
            }
            return (NetTopologyPolygon)geometryFactory.CreatePolygon(outerShellRing, holeRings);
        }

        private static DepictionGeometry ConstructLineString(CarbonTools.Geometries.LineString ctLineString)
        {
            if (ctLineString != null)
            {
                var linestring = ConstructGISSharpBlogLineString(ctLineString);
                return new DepictionGeometry(linestring);
            }
            return null;
        }
        private static LineString ConstructGISSharpBlogLineString(CarbonTools.Geometries.LineString ctLineString)
        {

            var geometryFactory = DepictionAccess.DepictionGeomFactory;
            var coordArray = new ICoordinate[ctLineString.Points.Count];
            var points = ctLineString.Points;
            var index = 0;
            foreach (var pt in points)
            {
                coordArray[index++] = new Coordinate(pt.X, pt.Y);
            }
            return (LineString)geometryFactory.CreateLineString(coordArray);

        }
        private static DepictionGeometry ConstructMultiPolygon(ICollection<CarbonTools.Geometries.Polygon> ctPolyCollection)
        {
            var geometryFactory = DepictionAccess.DepictionGeomFactory;
            //in GML parlance, this is a MultiPolygon
            var polygonArray = new NetTopologyPolygon[ctPolyCollection.Count];

            int polyNumber = 0;

            foreach (CarbonTools.Geometries.Polygon poly in ctPolyCollection)
            {
                polygonArray[polyNumber++] = ConstructGISSharpBlogPolygon(poly);
            }

            return new DepictionGeometry(geometryFactory.CreateMultiPolygon(polygonArray));
        }

        private static DepictionGeometry ConstructMultiLineString(ICollection<CarbonTools.Geometries.LineString> ctLineStringCollection)
        {
            var geometryFactory = DepictionAccess.DepictionGeomFactory;
            var linestringArray = new LineString[ctLineStringCollection.Count];
            int linenumber = 0;
            foreach (CarbonTools.Geometries.LineString ls in ctLineStringCollection)
            {
                linestringArray[linenumber++] = ConstructGISSharpBlogLineString(ls);
            }
            return new DepictionGeometry(geometryFactory.CreateMultiLineString(linestringArray));

        }
        #endregion
        //        private static bool IsGeometryANonGPXRouteWaypoint(OgrPropertiesAndIGeometry feature, OgrFileType fileType)
        //        {
        //            if (fileType != OgrFileType.GPX) return true;
        //
        //            if (feature.GeoApiGeometry is Point)
        //            {
        //                //is it a waypoint standalone or is it a waypoint in a TRACK/ROUTE?
        //                foreach (var nvp in feature.OgrProperties)
        //                {
        //                    if (nvp.Key.Equals("track_fid") || nvp.Key.Equals("track_seg_id") || nvp.Key.Equals("track_seg_point_id")
        //                        || nvp.Key.Equals("route_fid") || nvp.Key.Equals("route_point_id"))
        //                        return false;
        //                }
        //            }
        //            return true;
        //        }
    }
}