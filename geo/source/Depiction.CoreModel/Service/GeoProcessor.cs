﻿using System;
using System.Collections.Generic;
using Depiction.API.ExtensionMethods;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using ArrayList = System.Collections.ArrayList;
using Point = GisSharpBlog.NetTopologySuite.Geometries.Point;
using PointF = System.Windows.Point;

namespace Depiction.CoreModel.Service
{
    /// <summary>
    /// This is a set of geo utility methods for general purpose use.
    /// </summary>
    public static class GeoProcessor
    {
        public static IPolygon ConvertToPolygon(IList<List<LatitudeLongitude>> list)
        {
            return ConvertContoursToPolygonWithHoles(list);
        }

        /// <summary>
        /// Convert a set of polygons to a single polygon surrounding a seed point
        /// This single polygon can contain holes
        /// If so, the first poly in the returned list is the shell and the rest are holes
        /// </summary>
        public static IPolygon ConvertContoursToPolygonWithHoles(IList<List<LatitudeLongitude>> cList)
        {
            var precModel = new PrecisionModel(); //FLOATING POINT precision
            var geomFactory = new GeometryFactory(precModel, 32767);

            //construct a box around the seed point
            //to account for accuracy shortcomings


            //cList contains all the contours found by an iso-flood algorithm
            //
            //discard the ones that don't contain the seed point



            List<LatitudeLongitude> contourList;
            var polygons = new List<DepictionGeometry>();
            var allPolygons = new List<DepictionGeometry>();


            for (int i = 0; i < cList.Count; i++)
            {
                contourList = cList[i];
                if (contourList.Count <= 3)
                    continue;

                if (!(contourList[0].Longitude == contourList[contourList.Count - 1].Longitude && (contourList[0].Latitude == contourList[contourList.Count - 1].Latitude)))
                {
                    contourList.Add(new LatitudeLongitude(contourList[0].Latitude, contourList[0].Longitude));
                }
                var carr = new Coordinate[contourList.Count];
                for (int j = 0; j < contourList.Count; j++)
                {
                    var p = contourList[j];
                    carr[j] = new Coordinate(p.Longitude, p.Latitude);
                }


                var lRing = geomFactory.CreateLinearRing(carr);
                var poly = new DepictionGeometry(geomFactory.CreatePolygon(lRing, null));

                if (!poly.IsSimple) continue;
                allPolygons.Add(poly);
                //see if this polygon contains the seed point

                polygons.Add(poly);
            }
            var shellPoly = polygons[0];
            if (polygons.Count > 0)
            {
                for (int i = 0; i < allPolygons.Count; i++)
                {
                    if (!(allPolygons[i]).Equals(polygons[0]))
                    {
                        if (allPolygons[i].Within(polygons[0]))
                        {
                            polygons.Add(allPolygons[i]);
                        }
                    }
                }
            }

            ILinearRing shell = ((IPolygon)shellPoly.Geometry).Shell;
            var holes = new List<ILinearRing>();
            for (int i = 1; i < polygons.Count; i++)
            {
                holes.Add(((IPolygon)polygons[i].Geometry).Shell);

            }
            IGeometry geometry = geomFactory.CreatePolygon(shell, holes.ToArray());
            geometry = geometry.Buffer(0.000001);
            return geometry as IPolygon;
        }

        ///<summary>
        /// At a given latlon position, convert meters to decimal degrees
        /// </summary>
        public static double MetersToDecimalDegrees(double lon, double lat, double offset)
        {
            var north = new LatitudeLongitude(lat, lon);
            var offsetPoint = north.TranslatePosition(0, offset, GeoFramework.DistanceUnit.Meters);
            return Math.Abs(offsetPoint.Latitude - north.Latitude);
        }

        public static bool IsPolygonSimple(ArrayList vertices)
        {

            var precModel = new PrecisionModel(); //FLOATING POINT precision
            var geomFactory = new GeometryFactory(precModel, 32767);
            var carr = new Coordinate[vertices.Count];
            for (int j = 0; j < vertices.Count; j++)
            {
                var p = (PointF)vertices[j];
                carr[j] = new Coordinate(p.X, p.Y);
            }
#pragma warning disable 612,618
            var cseq = new DefaultCoordinateSequence(carr);
#pragma warning restore 612,618
            var lRing = new LinearRing(cseq, geomFactory);

            var poly = new DepictionGeometry(new Polygon(lRing));

            return poly.IsSimple;

        }
        /// <summary>
        /// Convert a set of polygons to a single polygon surrounding a seed point.
        /// Will discard any polygons that do not contain tha seed point.
        /// This single polygon can contain holes.
        /// If so, the first poly in the returned list is the shell and the rest are holes.
        /// </summary>
        /// <param name="cList"></param>
        /// <param name="seedLon">The longitude of the seed point.</param>
        /// <param name="seedLat">The latitude of the seed point.</param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static List<List<PointF>> ConvertContoursToPolygonWithHoles(List<List<LatitudeLongitude>> cList, double seedLon, double seedLat, double tolerance)
        {
            bool containmentTest = true;
            if (seedLon == seedLat && seedLon == -1000)
            {
                //ignore the seed point
                containmentTest = false;
            }

            var precModel = new PrecisionModel(); //FLOATING POINT precision
            var geomFactory = new GeometryFactory(precModel, 32767);

            //construct a box around the seed point
            //to account for accuracy shortcomings

            DepictionGeometry gridCellBox = null;
            if (tolerance > 0)
                gridCellBox = CreateGridCellPolygon(geomFactory, seedLon, seedLat, tolerance);

            //cList contains all the contours found by an iso-flood algorithm
            //
            //discard the ones that don't contain the seed point

            var seedPoint = new DepictionGeometry(new Point(seedLon, seedLat));

            List<LatitudeLongitude> contourList;
            var polygons = new List<DepictionGeometry>();
            int smallestPolygonLength = 1000000;
            var allPolygons = new List<DepictionGeometry>();

            int i;
            for (i = 0; i < cList.Count; i++)
            {
                contourList = cList[i];
                if (containmentTest && smallestPolygonLength < contourList.Count)
                    continue;
                if (contourList.Count <= 3)
                    continue;

                if (!(contourList[0].Equals(contourList[contourList.Count - 1 ])))
                {
                    contourList.Add(contourList[0]);
                }
                var carr = new Coordinate[contourList.Count];
                for (int j = 0; j < contourList.Count; j++)
                {
                    var p = contourList[j];
                    carr[j] = new Coordinate(p.Longitude, p.Latitude);
                }
#pragma warning disable 612,618
                var cseq = new DefaultCoordinateSequence(carr);
#pragma warning restore 612,618
                var lRing = new LinearRing(cseq, geomFactory);

                var poly = new DepictionGeometry(new Polygon(lRing));

                if (!poly.IsSimple) continue;
                allPolygons.Add(poly);
                //see if this polygon contains the seed point
                bool contained;
                if (tolerance > 0) contained = poly.Intersects(gridCellBox);
                else contained = poly.Contains(seedPoint);

                if (!containmentTest) contained = true;

                if (!contained) continue;
                smallestPolygonLength = contourList.Count;
                polygons.Add(poly);
            }
            if (polygons.Count > 0)
            {
                for (i = 0; i < allPolygons.Count; i++)
                {
                    if (!(allPolygons[i]).Equals(polygons[0]))
                    {
                        if (allPolygons[i].Within(polygons[0]))
                        {
                            polygons.Add(allPolygons[i]);
                        }
                    }
                }
            }
            var coordList = new List<List<PointF>>();

            for (int j = 0; j < polygons.Count; j++)
            {
                var newPoly = (polygons[j]);

                var newArray = new List<PointF>();
                ICoordinate[] vertices = newPoly.Coordinates; //SimplifyPolygon(newPoly,5/*meters*/)

                if (vertices == null) continue;
                if (vertices.Length <= 3) continue;

                for (i = 0; i < vertices.Length; i++)
                {
                    newArray.Add(new PointF((float)vertices[i].X, (float)vertices[i].Y));
                }
                coordList.Add(newArray);
            }
            return coordList;
        }

        public static List<LatitudeLongitude>[] ConvertToPolygon(List<List<LatitudeLongitude>> list, double x, double y, double tolerance)
        {
            var relevantCoords = ConvertContoursToPolygonWithHoles(list, x, y, tolerance);

            var latlonlist = new List<LatitudeLongitude>[relevantCoords.Count];

            for (int i = 0; i < relevantCoords.Count; i++)
            {
                latlonlist[i] = new List<LatitudeLongitude>();
                var contourList = relevantCoords[i];
                for (int j = 0; j < contourList.Count; j++)
                {
                    var pt = contourList[j];
                    latlonlist[i].Add(new LatitudeLongitude(pt.Y, pt.X));
                }
            }
            return latlonlist;
        }

        public static bool IsCoordinateListClosed(List<LatitudeLongitude> list)
        {
            if (list == null) return false;

            if (list[0].Latitude == list[list.Count - 1].Latitude && list[0].Longitude == list[list.Count - 1].Longitude)
                return true;

            return false;
        }

        public static DepictionGeometry CreateGridCellPolygon(IGeometryFactory factory, double lon, double lat, double spacing)
        {
            var coordList = new List<ICoordinate>();
            var center = new LatitudeLongitude(lat, lon);

            var topLeft = center.TranslateTo(-90d, spacing / 2, GeoFramework.DistanceUnit.Meters);
            topLeft = topLeft.TranslateTo(0d, spacing / 2, GeoFramework.DistanceUnit.Meters);
            //top left
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
            //top right
            topLeft = topLeft.TranslateTo(90d, spacing, GeoFramework.DistanceUnit.Meters);
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
            //bottom right
            topLeft = topLeft.TranslateTo(180d, spacing, GeoFramework.DistanceUnit.Meters);
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
            //bottom left
            topLeft = topLeft.TranslateTo(-90d, spacing, GeoFramework.DistanceUnit.Meters);
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));

            coordList.Add(coordList[0]);

            ILinearRing sqRing = new LinearRing(coordList.ToArray());
            return new DepictionGeometry(factory.CreatePolygon(sqRing, null));
        }
        //
        //given a lon,lat location, create a polygon (square) representing the four corners of the grid cell
        //in Depiction, the terrain/coverage layer has the representative location of a grid cell to be the
        //SOUTHWEST corner of the pixel. So...this is the static method we use to generate a grid cell polygon
        public static Polygon CreateGridCellPolygonSW(IGeometryFactory factory, double lon, double lat, double spacing)
        {
            var coordList = new List<ICoordinate>();
            var sw = new LatitudeLongitude(lat, lon);

            var topLeft = sw.TranslateTo(0d, spacing, GeoFramework.DistanceUnit.Meters);
            //top left
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
            //top right
            topLeft = topLeft.TranslateTo(90d, spacing, GeoFramework.DistanceUnit.Meters);
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
            //bottom right
            topLeft = topLeft.TranslateTo(180d, spacing, GeoFramework.DistanceUnit.Meters);
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));
            //bottom left
            topLeft = topLeft.TranslateTo(-90d, spacing, GeoFramework.DistanceUnit.Meters);
            coordList.Add(new Coordinate(topLeft.Longitude, topLeft.Latitude));

            coordList.Add(coordList[0]);

            ILinearRing sqRing = new LinearRing(coordList.ToArray());
            return (Polygon)factory.CreatePolygon(sqRing, null);
        }
    }
}
