using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.MessagingInterface;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service.FileReading;
using Depiction.CoreModel.ValueTypes;
using GisSharpBlog.NetTopologySuite.Geometries;
using GisSharpBlog.NetTopologySuite.IO;
using OSGeo.OGR;
using OSGeo.OSR;
using OSGGeometry = OSGeo.OGR.Geometry;

namespace Depiction.CoreModel.Service
{
    public class OsGeoFileTypeReaderToDepictionElements
    {
        public List<IDepictionElement> GetElementsFromShapeFile(string filename, string defaultElementType, IMapCoordinateBounds regionExtent, string description, string source, bool useDMLColor)
        {
            return GetElementsFromShapeFile(filename, defaultElementType, regionExtent, description, source, useDMLColor,
                                            null);
        }

        public List<IElementPrototype> GetElementPrototypesFromShapeFile(string filename, string defaultElementType, IMapCoordinateBounds regionExtent,
            string description, string source, bool useShapeFileColors, IDepictionBackgroundService backgroundConnection)
        {
            var prototypeList = new List<IElementPrototype>();
            if (DepictionAccess.ElementLibrary == null) return prototypeList;

            List<OgrPropertiesAndIGeometry> features = GetFeaturesFromOGRReadableFile(filename, regionExtent, description);
            prototypeList = FileReadingHelpers.CreatePrototypesFromShpTypeFileFeatures(features, defaultElementType,
                                                                                   useShapeFileColors,
                                                                                   backgroundConnection);
            return prototypeList;
        }

        public List<IDepictionElement> GetElementsFromShapeFile(string filename, string defaultElementType, IMapCoordinateBounds regionExtent,
            string description, string source, bool useShapeFileColors, IDepictionBackgroundService backgroundConnection)
        {

            var prototypeList = GetElementPrototypesFromShapeFile(filename, defaultElementType, regionExtent, description,
                                                                source, useShapeFileColors, backgroundConnection);
            var elementList = new List<IDepictionElement>();
            if (prototypeList == null) return elementList;

            if (DepictionAccess.ElementLibrary == null) return elementList;
            foreach (var prototype in prototypeList)
            {
                var element = ElementFactory.CreateElementFromPrototype(prototype);
                if (element != null)
                {
                    elementList.Add(element);
                }
            }
            return elementList;
        }
        #region static methods for parsing files


        static public List<OgrPropertiesAndIGeometry> GetFeaturesFromOGRReadableFile(string filename, IMapCoordinateBounds regionExtent, string description)
        {
            return GetFeaturesFromOGRReadableFile(filename, regionExtent, description, true);
        }
        static public List<OgrPropertiesAndIGeometry> GetFeaturesFromOGRReadableFile(string filename, IMapCoordinateBounds regionExtent, string description, bool giveMessages)
        {
            var ogrFileType = FileReadingHelpers.GetFileType(filename);
            //DepictionReadableFileTypes depictionReadableFileType = ProductAndFolderService.GetFileType(filename);

            SpatialReference sr = null, srDst = null;
            CoordinateTransformation ct = null;
            var factory = new GeometryFactory();
            var wktReader = new WKTReader(factory);

            DepictionGeometry regionBoundPoly = FileReadingHelpers.ConstructRegionPolygon(regionExtent);
            var osgGeoBounds = GeometryToOgrConverter.DepictionGeometryToOgrGeometry(regionBoundPoly);
            var featureList = new List<OgrPropertiesAndIGeometry>();
            List<Layer> ogrLayers = GetLayersFromFile(filename, description);

            if (ogrFileType == OgrFileType.Shapefile)
            {
                //if there's an associated PRJ file, read that
                string projectionFileName = filename.ToLower().Replace(".shp", ".prj");
                //only works with SHAPEFILES
                //GML and GPX files, to the extent we know, are always in latlong Geographic CS
                //if theres no such file, proceed without conversion
                if (File.Exists(projectionFileName) && !filename.ToLower().Equals(projectionFileName))
                {
                    sr = ReadShapeProjectionFile(projectionFileName);
                    // OGR target Spatial Reference
                    srDst = new SpatialReference("");
                    //srDst.ImportFromEPSG(4326);  // EPSG:4326 is WGS84

                    srDst.SetWellKnownGeogCS("WGS84");

                    // OGR Coordinate Transformation object
                    ct = new CoordinateTransformation(sr, srDst);
                }
                osgGeoBounds.AssignSpatialReference(srDst);
                int result = osgGeoBounds.TransformTo(sr);
            }


            //Now, add all the layers
            int count = 0;
            foreach (Layer ogrLayer in ogrLayers)
            {
                try
                {
                    FeatureDefn layerFieldDef = ogrLayer.GetLayerDefn();
                    Feature feature;

                    string wellKnownText;
                    //osgGeoBounds.Transform(ct);
                    //osgGeoBounds.TransformTo(sr);
                    if (ogrFileType == OgrFileType.Shapefile && regionExtent != null)
                        ogrLayer.SetSpatialFilter(osgGeoBounds);
                    //ogrLayer.SetSpatialFilter(osgGeoBounds);
                    while ((feature = ogrLayer.GetNextFeature()) != null)
                    {
                        try
                        {
                            OSGGeometry geom;
                            if (regionExtent == null)
                            {
                                geom = feature.GetGeometryRef();
                            }
                            else
                            {
                                geom = GetRegionIntersectedGeometryFromOgrFeature(feature, osgGeoBounds, sr, srDst, ct);
                            }

                            if (geom != null)
                            {
                                if (ogrFileType == OgrFileType.Shapefile)
                                    geom.Transform(ct);
                                //If there is a faster waht to change to IGeometry taht woudl be awesome (davidl)
                                geom.ExportToWkt(out wellKnownText);//oh boy
                                if (!wellKnownText.Equals("GEOMETRYCOLLECTION EMPTY"))
                                {
                                    var geometryAndProperties = new OgrPropertiesAndIGeometry();
                                    GetOgrFeatureProperties(feature, layerFieldDef, geometryAndProperties);
                                    geometryAndProperties.GeoApiGeometry = wktReader.Read(wellKnownText);
                                    //is this even needed?
                                    if (FileReadingHelpers.IsGeometryANonGPXRouteWaypoint(geometryAndProperties, ogrFileType))
                                    {

                                        //this actually takes time
                                        featureList.Add(geometryAndProperties);
                                    }
                                }
                            }
                            count++;
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(count + " had an exception");
                            continue;
                        }
                    }

                    Debug.WriteLine(count + " the numer of features");
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
            if (giveMessages)
            {
                var message = string.Format("{0} of {1} features were added to the {2}", featureList.Count, count, DepictionAccess.ProductInformation.StoryName);
                DepictionAccess.NotificationService.DisplayMessageString(message, 4);
            }
            return featureList;
        }

        private static List<Layer> GetLayersFromFile(string fileName, string requester)
        {
            if (!File.Exists(fileName)) return new List<Layer>();
            DataSource ds;
            try
            {
                ds = Ogr.Open(fileName, 0);
            }
            catch
            {
                return new List<Layer>();
            }

            if (ds == null)
            {
                return new List<Layer>();
            }

            Driver drv = ds.GetDriver();

            if (drv == null)
            {
                throw new Exception();//new NullGMLDataException(requester));
            }

            var layerList = new List<Layer>();
            for (int idx = 0; idx < ds.GetLayerCount(); ++idx)
            {
                layerList.Add(ds.GetLayerByIndex(idx));
            }

            return layerList;
        }
        private static void GetOgrFeatureProperties(Feature feature, FeatureDefn layerFieldDef, OgrPropertiesAndIGeometry inPropHolder)
        {
            for (int iField = 0; iField < feature.GetFieldCount(); iField++)
            {
                FieldDefn fdef = layerFieldDef.GetFieldDefn(iField);
                //if a field is not set, do not import it
                if (feature.IsFieldSet(iField))
                {
                    var type = feature.GetFieldType(iField);
                    switch (type)
                    {
                        case FieldType.OFTReal:
                            inPropHolder.OgrProperties.Add(fdef.GetNameRef(), feature.GetFieldAsDouble(iField));
                            break;
                        case FieldType.OFTInteger:
                            inPropHolder.OgrProperties.Add(fdef.GetNameRef(), feature.GetFieldAsInteger(iField));
                            break;
                        default:
                            inPropHolder.OgrProperties.Add(fdef.GetNameRef(), feature.GetFieldAsString(iField).Trim());
                            break;
                    }
                }
            }
        }
        private static OSGGeometry GetRegionIntersectedGeometryFromOgrFeature(Feature feature, OSGGeometry regionBounds,
                                                   SpatialReference srSrc, SpatialReference srDst, CoordinateTransformation ct)
        {
            OSGGeometry osGeoGeometry = feature.GetGeometryRef();
            if (osGeoGeometry == null) return null;

            //try
            //{
            //    if (srSrc != null)
            //    {
            //        if (srDst != null)
            //        {
            //            osGeoGeometry.Transform(ct);
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    return null;
            //}

            var intersectionGeo = regionBounds.Intersection(osGeoGeometry);

            return intersectionGeo;
        }
        #region projection methods
        /// <summary>
        /// Reads an ESRI .prj file and returns a proper SpatialReference object
        /// </summary>
        /// <param name="filename">Shapefile .prj file to read</param>
        /// <returns></returns>
        static public SpatialReference ReadShapeProjectionFile(string filename)
        {
            var sr = new StreamReader(filename);
            string wkt = sr.ReadToEnd();
            var wktArray = new string[1];
            wktArray[0] = wkt;

            var srs = new SpatialReference("");
            try
            {
                srs.ImportFromESRI(wktArray);
                string wktString;
                srs.ExportToWkt(out wktString);
            }
            catch (Exception e)
            {
                string message = string.Format("Depiction could not read {0} because of the following error:{1}", filename, e.Message);
                //                DepictionAccess.NotificationService.SendNotification(message);
            }
            return srs;
        }

        #endregion
        #endregion
    }
}