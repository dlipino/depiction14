﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;

namespace Depiction.CoreModel.Service
{
    public class TileBackgroundServicce : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        IMapCoordinateBounds requestQueue;
        IMapCoordinateBounds lastRequestQueue;
        private ITileProvider currentTileProvider;
        protected BackgroundWorker tileWorker;
        private TileImageTypes selectableTileProviderType = TileImageTypes.Unknown;
        private bool connectedToTiler;
        //        private TileImageTypes selectableTileProviderType = TileImageTypes.None;
        private readonly List<ITileProvider> allTileProvidersStorage = new List<ITileProvider>();

        private readonly ObservableCollection<TileModel> tileCollection = new ObservableCollection<TileModel>();

        public ObservableCollection<TileModel> TileCollection
        {
            get
            {
                return tileCollection;
            }
        }

        public ObservableCollection<ITileProvider> AllAvailableTileProviders
        {
            get { return SelectableTileProviders; }
        }//; private set; }

        public TileImageTypes SelectableTileProviderType
        {
            get { return selectableTileProviderType; }
            set
            {
                if (selectableTileProviderType == value) return;

                selectableTileProviderType = value;
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("SelectableTileProviderType"));
                }
                UpdateSelectableTileProviders(selectableTileProviderType);


                if (currentTileProvider== null || currentTileProvider.TileImageType.Equals(selectableTileProviderType)) return;

                if (SelectableTileProviders != null && SelectableTileProviders.Any())
                {
                    CurrentTileProvider = SelectableTileProviders.First();
                }
            }
        }
        public ObservableCollection<ITileProvider> SelectableTileProviders { get; private set; }
        public ITileProvider CurrentTileProvider
        {
            get { return currentTileProvider; }
            set
            {
                //Well this would work if we knew what tiler was currently in use and if we new if it 
                //was running
                //if (currentTileProvider == value) return;
                SetTilerStopOldAndStartNew(value);
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs("CurrentTileProvider"));
                }
            }
        }
        #region Constructor

        public TileBackgroundServicce(DepictionApplication depictionApp)
        {
            SelectableTileProviders = new ObservableCollection<ITileProvider>();
            var empty = new EmptyTileProvider();
            AllAvailableTileProviders.Add(empty);
            allTileProvidersStorage.Add(empty);
            depictionApp.DepictionChanged += depictionApp_DepictionChanged;
            //            AllAvailableTileProviders = new ObservableCollection<ITileProvider>();
            //CurrentTileProvider = empty;
        }



        #endregion

        void depictionApp_DepictionChanged(IDepictionStory oldDepiction, IDepictionStory newDepiction)
        {
            Debug.WriteLine("Tile background service starting for a depiction change.");
            if (oldDepiction != null)
            {
                oldDepiction.DepictionViewportChange -= OnViewPortChanged;
            }
            if (newDepiction != null)
            {
                newDepiction.DepictionViewportChange += OnViewPortChanged;
                if (newDepiction.ViewportBounds.IsValid)
                {
                    OnViewPortChanged(null, newDepiction.ViewportBounds);
                }
            }
            else
            {
                if (currentTileProvider != null)
                {
                    StopTiler();
                }
            }
        }
        public void SetTilerStopOldAndStartNew(ITileProvider newTileProvider)
        {
            SetTileProviderAndStopOldOne(newTileProvider);
            StartTiler();
        }
        public void SetTileProviderAndStopOldOne(ITileProvider newTileProvider)
        {
            //Always stop the old one
            if (currentTileProvider != null)
            {
                StopTiler();
            }

            if (newTileProvider != null)
            {            //Could be null for no tiling
                currentTileProvider = newTileProvider;
                Settings.Default.TilingSourceName = newTileProvider.DisplayName;
                Settings.Default.Save();
                if (!connectedToTiler)
                {
                    connectedToTiler = true;
                    currentTileProvider.BackgroundTileReceived += TileProviderTileReceived;
                }
            }
        }
        public void StartTiler()
        {
            //            if (tileWorker == null)
            //            {
            tileWorker = new BackgroundWorker();
            tileWorker.DoWork += SpinTiler;
            tileWorker.RunWorkerCompleted += tileWorker_RunWorkerCompleted;
            tileWorker.WorkerSupportsCancellation = true;
            //            }
            if (currentTileProvider != null && !connectedToTiler)
            {
                connectedToTiler = true;
                currentTileProvider.BackgroundTileReceived += TileProviderTileReceived;

            }
            if (lastRequestQueue != null)
            {
                requestQueue = lastRequestQueue;
            }
            if (!(currentTileProvider is EmptyTileProvider))
            {
                tileWorker.RunWorkerAsync();
            }
        }

        void tileWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GC.Collect();
        }

        public void StopTiler()
        {
            if (currentTileProvider != null)
            {
                currentTileProvider.BackgroundTileReceived -= TileProviderTileReceived;
                currentTileProvider.CancelRequest();
            }
            connectedToTiler = false;
            if (tileWorker != null)
                tileWorker.CancelAsync();
            tileCollection.Clear();
        }

        public void AddToAvailableTilers(ITileProvider[] providers)
        {
            if (providers != null)
            {
                foreach (var provider in providers)
                {
                    AllAvailableTileProviders.Add(provider);
                    allTileProvidersStorage.Add(provider);
                }
            }
            //            SelectableTileProviderType = TileImageTypes.Street;
            SelectLastOrDefaultTiler();
        }
        private void UpdateSelectableTileProviders(TileImageTypes tilerImageType)
        {
            //If type does not match, select the first one that does.
            //kind of a hack
            var matchingTilers =
                allTileProvidersStorage.Where(t => t.TileImageType.Equals(tilerImageType));
            //Hopefully this doesnt cause a race condition, or something bad like that
            SelectableTileProviders.Clear();
            foreach (var matchingTiler in matchingTilers)
            {
                SelectableTileProviders.Add(matchingTiler);
            }

        }
        protected void SelectLastOrDefaultTiler()
        {
            //Overall semi hackish because of legacy code and settings
            var tilerToLoadName = Settings.Default.TilingSourceName;
            ITileProvider selectedTiler = null;
            //Ugly stuff, really ugly stuff, and have not intention of making pretty yet
            if (string.IsNullOrEmpty(tilerToLoadName))
            {
                selectedTiler =SelectFirstAvailableTilerAndUpdateTileService();
            }else
            {
                var matchingTilers = allTileProvidersStorage.Where(t => t.DisplayName.Equals(tilerToLoadName)).ToList();
                if (!matchingTilers.Any())
                {
                    selectedTiler = SelectFirstAvailableTilerAndUpdateTileService();
                }else
                {
                    var firstMatch = matchingTilers.First();

                    var tilerType = firstMatch.TileImageType;
                    SelectableTileProviderType = tilerType;
                    CurrentTileProvider = firstMatch;
                    selectedTiler = firstMatch;
                }
            }
            if(selectedTiler != null)
            {
                UpdateSelectableTileProviders(selectedTiler.TileImageType);
                Settings.Default.TilingSourceName = selectedTiler.DisplayName;
            }
        }
        private ITileProvider SelectFirstAvailableTilerAndUpdateTileService()
        {
            //There should always be something in thislist.
            var firstTiler = allTileProvidersStorage.First();

            var tilerType = firstTiler.TileImageType;
            SelectableTileProviderType = tilerType;
            CurrentTileProvider = firstTiler;
            return firstTiler;
        }

        //Never clear out old tiles, and hope the user doesn't decide to go globe trotting.
        private void TileProviderTileReceived(TileModel tileModel)
        {
            try
            {
                if (!tileCollection.Contains(tileModel)) { tileCollection.Add(tileModel); }
            }
            catch (IOException x)
            {
                //                                        DepictionExceptionHandler.HandleException(x, false);
            }
        }

        public void OnViewPortChanged(IMapCoordinateBounds oldBounds, IMapCoordinateBounds bounds)
        {
            if (currentTileProvider != null) { currentTileProvider.CancelRequest(); }
            if (tileWorker != null && !tileWorker.IsBusy)
            {
                lastRequestQueue = null;
            }
            requestQueue = bounds;
        }

        private void SpinTiler(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            while (!worker.CancellationPending)
            {
                if (CurrentTileProvider == null)
                {
                    return;
                }

                if (requestQueue == null)
                {
                    Thread.Sleep(400);
                    continue;
                }

                try
                {
                    //int lengthAtRequest = requestQueue.Count;

                    //// Delay tiling start in case quick
                    //// zoom/pan stop and go is happening
                    //Thread.Sleep(50);
                    //if (lengthAtRequest != requestQueue.Count || requestQueue.Count < 1) continue;

                    IMapCoordinateBounds pulledBox = requestQueue;
                    lastRequestQueue = requestQueue;
                    requestQueue = null;

                    DoTiling(pulledBox);
                }
                catch (Exception ex)
                {
                    if (ex is WebException && ex.Message.Contains("The remote name could not be resolved"))
                    {
                        //TODO put this back in 
                        //DepictionAccess.NotificationService.SendNotification(
                        //    "Could not access Internet to get images for the world map. Please try again later, or try another tiling source.");
                    }
                }
            }
        }

        private void DoTiling(IMapCoordinateBounds box)
        {
            if (box.IsValid)
                CurrentTileProvider.RetrieveTiles(box);
        }
    }
}
