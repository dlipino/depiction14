using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using Depiction.API;
using Depiction.API.DepictionConfiguration;
using Depiction.API.ExceptionHandling;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.WebService;
using Depiction.Serialization;

namespace Depiction.CoreModel.Service
{
    //Better unit tests are needed, ie unit tests in which multiple geocoders are available
    public class DepictionGeocodingService : IDepictionGeocodingService
    {

        protected const string CacheFileName = "DepictionGeocodeCache.xml";
        //For now the cache is only for initial values, this can change quickly
        protected Dictionary<string, ILatitudeLongitude> startLocationCache = new Dictionary<string, ILatitudeLongitude>();
        protected Dictionary<string, ILatitudeLongitude> generalGeocodeCache = new Dictionary<string, ILatitudeLongitude>();

        protected readonly string filePath = string.Empty;

        public List<string> StartLocationCacheKeys
        {
            get { return startLocationCache.Keys.ToList(); }
        }
        public List<string> GeoCodedCacheKeys
        {
            get { return generalGeocodeCache.Keys.ToList(); }
        }

        #region COnstructor

        public DepictionGeocodingService()
        {
            //            if (DepictionAccess.PathService != null)
            //            {
            //                filePath = Path.Combine(DepictionAccess.PathService.GetUserDataPath(), "GeocoderCache.xml");
            //            }
            UpdateStartLocationCache();
        }

        #endregion
       // http://gws2.maps.yahoo.com/findlocation?pf=1&locale=en_US&flags=&offset=15&gflags=&q=oakridge&start=0&count=100
        public IDepictionGeocoder[] FindAGeocodersForRegion(string region)
        {
            //Not sure how this works
            if (string.IsNullOrEmpty(region))
            {
                region = "US";
            }
            var geocoders = new List<IDepictionGeocoder>();
            try
            {
                if (AddinRepository.IsActive)
                {
                    //TODO This is like really really slow. SO when there are many (1000's) of things that need
                    //geocoding it takes a long time. This seems to be giving errors when 
                    var qsItems = WebServiceFacade.GetPortalQuickstartSourcesByRegionCodes(new[] { region });
                    if (qsItems != null)
                    {
                        foreach (var qsItem in qsItems)
                        {
                            if (qsItem.ElementType.Equals("Geocoder"))
                            {
                                var geocoder = AddinRepository.Instance.GetGeocoder(qsItem.AddinName);
                                geocoder.ReceiveParameters(qsItem.Parameters);
                                geocoders.Add(geocoder);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Geo-coders are not active");
                }
            }
            catch (Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex, false, false);
                return geocoders.ToArray();
            }
            return geocoders.ToArray();
        }
        public GeocodeResults GeoCodeRawStringAddress(string rawAddress)
        {
            return GeoCodeRawStringAddress(rawAddress, null);
        }

        public GeocodeResults GeoCodeRawStringAddress(string rawAddress, IDepictionGeocoder[] geocodersToUse, bool useWebForGeocode)
        {
            if (string.IsNullOrEmpty(rawAddress)) return null;
            if (startLocationCache.ContainsKey(rawAddress))
            {
                var result = new GeocodeResults(startLocationCache[rawAddress], true);
                return result;
            }
            var lowerAddress = rawAddress.ToLowerInvariant();
            if (generalGeocodeCache.ContainsKey(lowerAddress))
            {
                var result = new GeocodeResults(generalGeocodeCache[lowerAddress], true);
                return result;
            }

            try
            {
                var latLong = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(rawAddress);
                if (latLong != null)
                {
                    if (latLong.IsValid) return new GeocodeResults(latLong, true);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Not in lat/long format");

            }
            if (!useWebForGeocode) return null;
            if (geocodersToUse == null)
            {
                geocodersToUse = FindAGeocodersForRegion("US");
            }
            foreach (var geocoder in geocodersToUse)
            {
                var result = geocoder.GeocodeAddress(rawAddress);
                
                if (result != null && result.Position != null && result.Position.IsValid)
                {
                    if (geocoder.StoreResultsInDepiction)
                    {
                        AddLocationPairToGeneralCache(lowerAddress, result.Position);
                    }
                    result.GeocodedByWeb = true;
                    return result;
                }
            }

            return null;
        }

        public GeocodeResults GeoCodeRawStringAddress(string rawAddress, IDepictionGeocoder[] geocodersToUse)
        {
            return GeoCodeRawStringAddress(rawAddress, geocodersToUse, true);
        }

        public void AddLocationPairToStartCache(string location, ILatitudeLongitude position)
        {
            if (position == null || !position.IsValid)
            {
                throw new Exception("AddLocationPairToStartCache called with invalid parameter: position must be valid.");
            }
            var config = DepictionUserConfigurationManager.DepictionUserConfiguration();
            if(config == null) return;
            
            var section = DepictionUserConfigurationManager.GetSectionDefaultDepictionConfigurationSection(config);
            var locationPair = new GeolocatedLocationElement { LocationName = location, LocationLatLong = position };
            section.SelectStartGeolocations.Add(locationPair);
            section.SectionInformation.ForceSave = true;
            config.Save(ConfigurationSaveMode.Full);
            //DepictionUserConfigurationManager.DepictionUserConfiguration().Save(ConfigurationSaveMode.Full);
            UpdateStartLocationCache();
        }
        public void AddLocationPairToGeneralCache(string location, ILatitudeLongitude position)
        {
            if (position == null || !position.IsValid)
            {
                throw new Exception("AddLocationPairToGeneralCache called with invalid parameter: position must be valid.");
            }
            var lowerAddress = location.ToLowerInvariant();
            //The lower case might become an issue is people decide to add their own.

            if (!generalGeocodeCache.ContainsKey(lowerAddress))
            {
                generalGeocodeCache.Add(lowerAddress, position);
            }
        }
        public GeocodeResults GeoCodeRawStringAddress(string streetAddress, string city, string state, string zipCode, string country)
        {
            return GeoCodeRawStringAddress(streetAddress, city, state, zipCode, country, null);
        }
        public GeocodeResults GeoCodeRawStringAddress(string streetAddress, string city, string state, string zipCode, string country, IDepictionGeocoder[] geocodersToUse)
        {
            return GeoCodeRawStringAddress(streetAddress, city, state, zipCode, country, geocodersToUse, true);
        }

        public GeocodeResults GeoCodeRawStringAddress(string streetAddress, string city, string state, string zipCode, string country, IDepictionGeocoder[] geocodersToUse, bool useWebForGeocode)
        {
            string cacheAddress = string.Format("{0} {1} {2} {3} {4}", streetAddress, city, state, zipCode, country);

            if (string.IsNullOrEmpty(cacheAddress)) return null;
            if (startLocationCache.ContainsKey(cacheAddress))
            {
                var result = new GeocodeResults(startLocationCache[cacheAddress], true);
                return result;
            }
            var lowerAddress = cacheAddress.ToLowerInvariant();
            if (generalGeocodeCache.ContainsKey(lowerAddress))
            {
                var result = new GeocodeResults(generalGeocodeCache[lowerAddress], true);
                return result;
            }
            if (!useWebForGeocode)
            {
                return null;
            }
            if (geocodersToUse == null)
            {
                geocodersToUse = FindAGeocodersForRegion("US");
            }
            foreach (var geocoder in geocodersToUse)
            {
                var result = geocoder.GeocodeAddress(streetAddress, city, state, zipCode, country);
                
                if (result != null && result.Position != null && result.Position.IsValid)
                {
                    result.GeocodedByWeb = true;
                    if (geocoder.StoreResultsInDepiction)
                    {
                        AddLocationPairToGeneralCache(lowerAddress, result.Position);
                    }
                    return result;
                }
            }
            return null;
        }

        public GeocodeResults GeoCodeFromAddressParts(FullAddressParts addressParts)
        {
            if (!string.IsNullOrEmpty(addressParts.rawAddress))
            {
                return GeoCodeRawStringAddress(addressParts.rawAddress);
            }
            return GeoCodeRawStringAddress(addressParts.streetAddress, addressParts.city, addressParts.state,
                                           addressParts.zipCode, addressParts.country);
        }

        private void UpdateStartLocationCache()
        {
            var config = DepictionUserConfigurationManager.DepictionUserConfiguration();
            if ( config == null)
                return;
            var section = DepictionUserConfigurationManager.GetSectionDefaultDepictionConfigurationSection(config);
            if (section == null) return;
            var geoRecords = section.SelectStartGeolocations;
            startLocationCache = new Dictionary<string, ILatitudeLongitude>();
            for (int i = 0; i < geoRecords.Count; i++)
            {
                startLocationCache.Add(geoRecords[i].LocationName, geoRecords[i].LocationLatLong);
            }
        }

        public void SaveGeneralGeoCodeCache()
        {
            if (DepictionAccess.PathService == null) return;
            string filePath = Path.Combine(DepictionAccess.PathService.AppDataDirectoryPath, CacheFileName);
           
            // Use invariant format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                try
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                    using (var writer = SerializationService.GetXmlWriter(stream))
                    {
                        var ns = SerializationConstants.DepictionXmlNameSpace;
                        writer.WriteStartElement("DepictionGeoCodeCache", ns);
                        foreach (var cacheLocation in generalGeocodeCache)
                        { 
                            var latLong = cacheLocation.Value as LatitudeLongitude;
                            if(latLong == null) continue;
                            writer.WriteStartElement("LocationPair");
                            writer.WriteAttributeString("name", cacheLocation.Key);
                            writer.WriteAttributeString("location", latLong.ToXmlSaveString());
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                    }
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = culture;
                }
            }

        }
        public void LoadGeneralGeoCodeCache()
        {
            if (DepictionAccess.PathService == null) return;
            string filePath = Path.Combine(DepictionAccess.PathService.AppDataDirectoryPath, CacheFileName);
            if (!File.Exists(filePath)) return;
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                    string mainName = "DepictionGeoCodeCache";
                    using (var reader = SerializationService.GetXmlReader(stream))
                    {
                        if(reader.NodeType.Equals(XmlNodeType.None))
                        {
                            reader.Read();
                        }
                        if (!reader.Name.Equals(mainName)) return;
                        generalGeocodeCache = new Dictionary<string, ILatitudeLongitude>();
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.EndElement && reader.Name.Equals(mainName))
                            {
                                //To read or not to read.
                                reader.ReadEndElement();
                                return;
                            }
                            if (reader.NodeType == XmlNodeType.EndElement) continue;

                            switch (reader.Name)
                            {
                                case "LocationPair":
                                    var name = reader.GetAttribute("name");
                                    var locationString = reader.GetAttribute("location");
                                    generalGeocodeCache.Add(name, new LatitudeLongitude(locationString));
                                    break;
                            }
                        }

                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("From DeserializeObject:" + ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }

        }
        //        /// <summary>
        //        /// Writes the cache to a file, so it can be read next time.
        //        /// </summary>
        //        public void SaveCache()
        //        {
        //            try
        //            {
        //                var dictionary = new SerializableDictionary<string, LatitudeLongitude>(cache);
        //                SerializationService.SaveToXmlFile(dictionary, filePath, "geocoderCache");
        //            }
        //            catch (IOException)
        //            {
        //                //swallow exception when file is being used by another process (i.e. when another depiction is closing)
        //            }
        //        }

    }
    public class FullAddressParts
    {
        public string streetAddress = "";
        public string city = "";
        public string state = "";
        public string zipCode = "";
        public string country = "";
        public string rawAddress = "";

    }
}