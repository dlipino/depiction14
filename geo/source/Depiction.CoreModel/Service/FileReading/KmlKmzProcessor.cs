﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CarbonTools.Content.Features;
using CarbonTools.Content.GoogleEarth.KmlKmz;
using CarbonTools.Data;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.ElementLibrary;

namespace Depiction.CoreModel.Service.FileReading
{
    public class KmlKmzProcessor
    {
        private HandlerKmlKmz handler;
        private String kmlFileString;
        private bool succeeded;
        private DataFeatures dataFeatures;
        private EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
        private List<IElementPrototype> prototypes = new List<IElementPrototype>();


        private IMapCoordinateBounds _depictionRegion;
        private string _elementType;

        public KmlKmzProcessor(string file)
        {
            kmlFileString = file;
        }
        public List<IElementPrototype> ProcessKmlKmzAndGetPrototypes(IMapCoordinateBounds extent, string elementType, bool readInForeground)
        {
            _depictionRegion = extent;
            _elementType = elementType;

            RemoveTabs(kmlFileString);
            var source = new SourceKmlKmz { URI = kmlFileString };
            handler = new HandlerKmlKmz(source) { Synchronous = readInForeground };
            handler.OperationDone += handler_OperationDone;

            handler.ReadFile();
            waitHandle.WaitOne();
            return prototypes;
        }
        //Not used
        public List<IDepictionElementBase> ProcessKmlKmzAndBaseElementTypes(IMapCoordinateBounds extent, string elementType, bool readInForeground, bool updateExisting)
        {
            var resultingPrototypes = ProcessKmlKmzAndGetPrototypes(extent, elementType, readInForeground);
            var elements = new List<IDepictionElementBase>();

            if (updateExisting)
            {
                return resultingPrototypes.Cast<IDepictionElementBase>().ToList();

            }
            foreach (var prototype in resultingPrototypes)
            {
                var element = ElementFactory.CreateElementFromPrototype(prototype);
                if (element != null)
                {
                    elements.Add(element);
                }
            }
            return elements;
        }

        private void RemoveTabs(string kmlfile)
        {
            string[] lines = File.ReadAllLines(kmlfile);
            int index = 0;
            bool tabPresent = false;
            foreach (var line in lines)
            {
                if (line.Contains(@"\t"))
                    tabPresent = true;

                var notabline = line.Replace('\t', ' ');
                lines[index++] = notabline;
            }
            try
            {
                File.WriteAllLines(kmlfile, lines);
            }
            catch (IOException ex) { }
        }

        void handler_OperationDone(object sender, EventArgs e)
        {
            try
            {
                var handle = sender as HandlerKmlKmz;
                if (handle == null)
                {
                    succeeded = false;
                    return;
                }

                var go = new GeoObject(handle);

                //var features = ExtractFeatures(dataFeatures);
                //var doc = FeaturesToGML.GetGML(features, null);

                //if (doc == null)
                //{
                //    DepictionAccess.NotificationService.SendNotification(string.Format("Could not understand the contents of the GML file: {0}", kmlFileString));
                //    succeeded = false;
                //    return;
                //}

                //if (isCancelled) return;
                //doc.Save(gmlFileString);
                dataFeatures = ExtractFeatures(go.Data as DataFeatures);
                var ogrParser = new DepictionCarbonToolsFileReaderService();
                prototypes = ogrParser.GetPrototypesFromCarbonToolsDataFeatures(dataFeatures, _elementType, _depictionRegion, "KML", "File");

            }
            finally
            {
                waitHandle.Set();
            }
        }
        private static DataFeatures ExtractFeatures(DataFeatures df)
        {
            if (df.Features == null) return null;

            DataFeatures features = new DataFeatures();
            features.Features = new ItemList();
            foreach (Item item in df.Features)
            {
                var member = new ItemMember { Name = item.Name };
                foreach (ItemMember mem in ((ItemMember)item).Items)
                {//for each layer
                    if (mem == null) continue;
                    var nestedMember = new ItemMember();
                    nestedMember.Name = mem.Name;
                    nestedMember.Attributes = mem.Attributes;
                    var items = FeatureAnalysis.GetAllElements(mem);
                    foreach (ItemElement it in items)
                    {
                        nestedMember.Items.Add(it);
                    }
                    var members = FeatureAnalysis.GetAllMembers(mem);
                    //get geometry
                    var itemGeom = FeatureAnalysis.GetGeometry(mem);
                    if (itemGeom != null)
                    {
                        var propGeometry = new ItemMember { Name = "GEOMETRY" };
                        propGeometry.Items.Add(itemGeom);
                        nestedMember.Items.Add(propGeometry);
                    }
                    member.Items.Add(nestedMember);
                }
                features.Features.Add(member);
            }

            return features;
        }

    }
}