﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Terrain;
using Depiction.CoreModel.ElementLibrary;

namespace Depiction.CoreModel.Service.FileReading
{
    public class CoverageDataConverter
    {
        static private readonly object addElevationToStoryLock = new object();
        #region fields

        // use a static synchronizing object so that all instances of this converter can be accessed one at a time
        private readonly string _fileName;
        private readonly IMapCoordinateBounds boundingBox;
        private readonly bool cropGridBeforeProjection;
        private readonly IDepictionElement elevationElement;
        private string elevationUnit;

        #endregion

        #region constructor

        public CoverageDataConverter(string req, string unit, IMapCoordinateBounds bounds, bool cropGridBeforeProjection, IDepictionElement elevationElement)
        {
            boundingBox = bounds;
            this.cropGridBeforeProjection = cropGridBeforeProjection;
            this.elevationElement = elevationElement;
            _fileName = req;
            elevationUnit = unit;
        }

        #endregion

        public List<IDepictionElement> ConvertDataToElements()
        {
            List<IDepictionElement> elements = new List<IDepictionElement>();
            lock (addElevationToStoryLock)
            {
                string partialFileName = string.Empty;
                var elevation = elevationElement ?? GetElevationElement();
                //var elevation = GetElevationElement();
                if (elevation == null) return null;
                Terrain terrain;
                if (_fileName != string.Empty)
                {
                    if (_fileName == "GetMap")
                        SaveWithKnownExtent(_fileName);
                    //if (IsCancelled) return new ImportedElements();

                    if (!(elevation.GetPropertyValue("Terrain", out terrain)))
                    {
                        return null; //elevation.Query<Terrain>(".Terrain");}
                    }
                    //if (IsCancelled) return new ImportedElements();

                    bool downSampled;
                    //
                    //TRAC #2183 fix:
                    //when individual files are added via Add/File/Elevation data, you want
                    //the spacing of the loaded file to supercede the spacing of the existing terrain
                    //data in your story
                    //That has been the convention in Depiction -- new terrain spacing overrides the existing resolution.

                    //Why can't we have multiple terrain data, and have elements 'hook' into the one they want?

                    var pos = new LatitudeLongitudeBase(47.60044, -122.33962);
                    var tempTerrainData = terrain.AddTerrainDataFromFile(_fileName, "", elevationUnit, true,
                                                                         out downSampled, false, cropGridBeforeProjection /*don't use existing grid spacing -- use new one*/);
                    if (tempTerrainData == null) return null;
                    //if (IsCancelled) return new ImportedElements();
                    if (!terrain.AddTerrainData(tempTerrainData)) return null;

                    // terrain has been modified, so do NOT allow canceling now.
                    var imageMetadata = terrain.Visual;


                    if (imageMetadata == null)
                    {
                        DepictionAccess.NotificationService.DisplayMessageString("Unable to create visual for elevation data");
                    }
                    else
                        lock (elevation)
                        {
                            var savedImage = BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(imageMetadata.ImageFilename,
                                                                                                              out partialFileName);
                            if (savedImage)
                            {
                                imageMetadata.ImageFilename = partialFileName;
                                elevation.SetImageMetadata(imageMetadata);
                            }
                        }
                    //add description to the Elevation's DisplayName property

                    // var displayNameProp = elevation.GetProperty("displayname");
                    // if (data.Description != null && !displayNameProp.Value.ToString().Contains(data.Description))
                    // {
                    //     var name = string.Format("{0} + {1}", displayNameProp.Value, data.Description);
                    //     displayNameProp.Set(name, SetWeight.User);
                    // }

                }
                else
                {
                    var message = new DepictionMessage("Unable to retrieve elevation data", 4);
                    DepictionAccess.NotificationService.DisplayMessage(message);
                }

                if (DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("Elevation") == null)
                {
                    elevation.Tags.Add("Elevation");
//                    DepictionAccess.CurrentDepiction.CompleteElementRepository.TagElement(elevation, "Elevation");
                }
                if (!string.IsNullOrEmpty(partialFileName))
                {
                    var prop = new DepictionElementProperty("EID", partialFileName);
                    prop.VisibleToUser = false;
                    elevation.AddPropertyOrReplaceValueAndAttributes(prop, false);
                }
                elements.Add(elevation);
            }
            return elements;
        }

        /// <summary>
        /// Returns the single elevation element. 
        /// If one has not been created yet, it creates a new one with
        /// a well-formed and empty Terrain object.
        /// </summary>
        /// <returns></returns>
        private IDepictionElement GetElevationElement()
        {
            var elevation =
                DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("Elevation");
            if (elevation == null)
            {
                System.Console.WriteLine("creating elevation element");
                var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.Elevation");
                elevation = ElementFactory.CreateElementFromPrototype(prototype);
                if (elevation == null) return null;
                var terrainLocal = new Terrain { HasGoodData = false };
                //Initial value was gutting used, but didn't work very well because of referencing and restoring.
                var terrainProperty = new DepictionElementProperty("Terrain", "Terrain", terrainLocal, null, null);
                terrainProperty.VisibleToUser = false;
                elevation.AddPropertyOrReplaceValueAndAttributes(terrainProperty, false);
            }
            return elevation;
        }

        private void SaveWithKnownExtent(string filename)
        {
            //save data.DataFilename as GeoTiff...can we do that?
            var terrain = new Terrain();
            terrain.AddFromFileWithoutExtents(filename, boundingBox.Top, boundingBox.Bottom,
                                              boundingBox.Right, boundingBox.Left);
            //the file would now be saved as a georeferenced GeoTiff file with the
            //supplied extents. It will be in a geographic coord system with a WGS84 datum
        }
    }
}