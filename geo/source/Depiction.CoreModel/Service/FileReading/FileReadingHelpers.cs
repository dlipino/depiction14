using System;
using System.Collections.Generic;
using System.IO;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.MessagingInterface;
using Depiction.API.Service;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.TypeConverter;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.CoreModel.Service.FileReading
{
    public class FileReadingHelpers
    {
        #region setup helpers
        public static DepictionGeometry ConstructRegionPolygon(IMapCoordinateBounds extent)
        {
            var geometryFactory = DepictionAccess.DepictionGeomFactory;
            if (extent == null)
                extent = new MapCoordinateBounds { Bottom = -75, Left = -180, Right = 180, Top = 75 };

            //create the region extent polygon
            var cList = new CoordinateList();
            cList.Add(new Coordinate(extent.Left, extent.Top));
            cList.Add(new Coordinate(extent.Right, extent.Top));
            cList.Add(new Coordinate(extent.Right, extent.Bottom));
            cList.Add(new Coordinate(extent.Left, extent.Bottom));
            cList.Add(new Coordinate(extent.Left, extent.Top));
            var lRing = (LinearRing)geometryFactory.CreateLinearRing(cList.ToCoordinateArray());
            var regionBoundPoly = geometryFactory.CreatePolygon(lRing, null);

            return new DepictionGeometry(regionBoundPoly);
        }

        public static OgrFileType GetFileType(string filename)
        {
            var extension = filename.Substring(filename.LastIndexOf(".") + 1).ToLower();
            switch (extension)
            {
                case "gml":
                    return OgrFileType.GML;
                case "shp":
                    return OgrFileType.Shapefile;
                case "gpx":
                    return OgrFileType.GPX;
            }
            return OgrFileType.GML;
        }

        static protected IElementPrototype GetBasePrototypeFromDefinitionType(string defaultElementType)
        {
            IElementPrototype prototype = null;
            if (DepictionAccess.ElementLibrary != null)
            {
                prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(defaultElementType);
            }
            if (prototype == null)
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(defaultElementType);
            }
            return prototype;
        }
        static protected void SetFillColor(IElementPrototype prototype, int baseValue)
        {
            var color = ProductAndFolderService.SevenColors[baseValue % 7];
            if (prototype.SetPropertyValue("ZOIFill", color) == false)
            {
                var fillProperty = new DepictionElementProperty("ZOIFill", color);
                prototype.AddPropertyOrReplaceValueAndAttributes(fillProperty, false);
            }
        }
        #endregion

        #region How to interpret the ogrpropertiandIGeometry

        static public List<IElementPrototype> CreatePrototypesFromShpTypeFileFeatures(List<OgrPropertiesAndIGeometry> features, string defaultElementType,
            bool useShapeFileColors, IDepictionBackgroundService backgroundConnection)
        {
            var autoDetectElement = DepictionStringService.AutoDetectElementString;
            var prototypeList = new List<IElementPrototype>();
            var typeToUse = autoDetectElement;
            if (!string.IsNullOrEmpty(defaultElementType))
            {
                typeToUse = defaultElementType;
            }
            var count = 0;
            var totalCount = features.Count;
            foreach (var feature in features)
            {
                count++;
                if (backgroundConnection != null)
                {
                    if (backgroundConnection.ServiceStopRequested) return new List<IElementPrototype>();
                    backgroundConnection.UpdateStatusReport(string.Format("Getting feature {0} of {1}", count, totalCount));
                }
                var rawPrototype = ElementFactory.CreateRawPrototypeOfType(typeToUse);

                foreach (var key in feature.OgrProperties.Keys)
                {
                    var lowerKey = key.ToLowerInvariant();
                    if (lowerKey.Equals("elementtype"))
                    {
                        var type = feature.OgrProperties[key].ToString();
                        if (ElementPrototypeLibrary.TypeConversionDictionary122To13.ContainsKey(type))
                        {
                            type = ElementPrototypeLibrary.TypeConversionDictionary122To13[type];
                        }
                        if (!string.IsNullOrEmpty(type))
                        {
                            rawPrototype.UpdateTypeIfRaw(type);
                        }
                    }//legacy stuff
                    else if (lowerKey.Equals("elementkey")) { }
                    //end legacy
                    else if (lowerKey.Equals("name"))
                    {
                        var nameProp = new DepictionElementProperty("DisplayName", "Name", feature.OgrProperties[key].ToString());
                        nameProp.Deletable = false;
                        nameProp.IsHoverText = true;//This is debatable, and also seems to not work
                        nameProp.Rank = -1;
                        rawPrototype.AddPropertyOrReplaceValueAndAttributes(nameProp, false);
                    }
                    else if (lowerKey.Equals("eid"))//Man this is annoying all number eids are coming in as doubles and they should be strings
                    {
                        var stringVal = feature.OgrProperties[key];
                        var property = new DepictionElementProperty(key, stringVal);
                        property.Editable = false;
                        rawPrototype.AddPropertyOrReplaceValueAndAttributes(property, false);
                    }
                    else
                    {
                        var convertedKeyName = key;
                        //hack for color
                        if (lowerKey.Equals("ZOIFill", StringComparison.InvariantCultureIgnoreCase) || lowerKey.Equals("ZOIBorder", StringComparison.InvariantCultureIgnoreCase))
                        {
                            useShapeFileColors = true;
                        }
                        //More legacy for zoi colors
                        if (lowerKey.Equals("StrokeColor", StringComparison.InvariantCultureIgnoreCase))
                        {
                            convertedKeyName = "ZOIBorder";
                            useShapeFileColors = true;
                        }
                        else if (lowerKey.Equals("FillColor", StringComparison.InvariantCultureIgnoreCase))
                        {
                            convertedKeyName = "ZOIFill"; useShapeFileColors = true;
                        }
                        //end legacy

                        //Duplicated in csvfile readingservice
                        var stringVal = feature.OgrProperties[key];
                        var objectValue = DepictionTypeConverter.ChangeTypeByGuessing(stringVal);
                        if (objectValue == null) objectValue = feature.OgrProperties[key];
                        var property = new DepictionElementProperty(convertedKeyName, objectValue);
                        //property.Editable = false;//why was this used?
                        rawPrototype.AddPropertyOrReplaceValueAndAttributes(property, false);
                    }
                }

                if (DepictionStringService.AutoDetectElementString.Equals(rawPrototype.ElementType, StringComparison.InvariantCultureIgnoreCase) ||
                    ElementPrototypeLibrary.RawPrototypeName.Equals(rawPrototype.ElementType, StringComparison.InvariantCultureIgnoreCase))
                {
                    var baseTypeByGeometry = ElementFactory.GeometryTypeToDefaultDepictionType(feature.GeoApiGeometry.GeometryType);
                    rawPrototype.UpdateTypeIfRaw(baseTypeByGeometry);
                }

                rawPrototype.SetInitialPositionAndZOI(null, new ZoneOfInfluence(new DepictionGeometry(feature.GeoApiGeometry)));

                //coloring
                if (!useShapeFileColors)
                {
                    SetFillColor(rawPrototype, count);
                }
                //                rawPrototype.SetPropertyValue("draggable", false, false);
                var prop = new DepictionElementProperty("Draggable", false);
                prop.Deletable = false;
                rawPrototype.AddPropertyOrReplaceValueAndAttributes(prop, false);
                prototypeList.Add(rawPrototype);
            }
            return prototypeList;
        }

        static public List<IElementPrototype> CreatePrototypesFromKmlFileFeatures(List<OgrPropertiesAndIGeometry> features, string defaultElementType,
            bool useShapeFileColors, BaseDepictionBackgroundThreadOperation backgroundConnection)
        {
            var autoDetectElement = DepictionStringService.AutoDetectElementString;
            var prototypeList = new List<IElementPrototype>();
            var typeToUse = autoDetectElement;
            if (!string.IsNullOrEmpty(defaultElementType))
            {
                typeToUse = defaultElementType;
            }

            var count = 0;
            foreach (var feature in features)
            {
                var rawPrototype = ElementFactory.CreateRawPrototypeOfType(typeToUse);
                rawPrototype.SetInitialPositionAndZOI(null,//new LatitudeLongitude(), using a default lat long is bad
                                                 new ZoneOfInfluence(new DepictionGeometry(feature.GeoApiGeometry)));
                
                MapCoordinateBounds fullImageBounds = null;
                string imageUri = string.Empty;
                double rotationRad = 0;
                count++;
                foreach (var key in feature.OgrProperties.Keys)
                {
                    DepictionElementProperty property;
                    if (key.Equals("name", StringComparison.InvariantCultureIgnoreCase))
                    {
                        property = new DepictionElementProperty("DisplayName", "Name", feature.OgrProperties[key].ToString());
                        property.Editable = false;
                        property.IsHoverText = true;
                        rawPrototype.AddPropertyOrReplaceValueAndAttributes(property, false);
                        //                        rawPrototype.SetPropertyValue("DisplayName", feature.OgrProperties[key].ToString());
                    }
                    else
                    {
                        bool dontAdd = false;
                        //Hmm, should we use the defaults? or not use the raw?
                        property = new DepictionElementProperty(key, feature.OgrProperties[key]);
                        property.Editable = false;
                        if (key.Equals("description", StringComparison.InvariantCultureIgnoreCase))
                        {
                            property.IsHoverText = true;
                        }
                        else if (key.Equals("GroundOverlayGeometry", StringComparison.InvariantCultureIgnoreCase))
                        {
                            dontAdd = true;
                            fullImageBounds = feature.OgrProperties[key] as MapCoordinateBounds;
                            //                        var depictionImageMetadata = new DepictionImageMetadata()
                            //                        rawPrototype.SetImageMetadataWithoutNotification();
                        }else if(key.Equals("Rotation",StringComparison.InvariantCultureIgnoreCase))
                        {
                            var rotObj = feature.OgrProperties[key];
                            if(rotObj is double)
                            {
                            dontAdd = true;
                                rotationRad = (double) rotObj;
                            } 
                        }
                        if(!dontAdd) rawPrototype.AddPropertyOrReplaceValueAndAttributes(property, false);
                    }
                    if (key.ToLower().Equals("uri"))
                    {
                        var iconPath = feature.OgrProperties[key].ToString();

                        if (!String.IsNullOrEmpty(iconPath))
                        {
                            bool getImage = true;
                            string iconName = "";
                            //take out spaces in the Uri path
                            if (iconPath.StartsWith("http", StringComparison.InvariantCultureIgnoreCase))
                            {
                                iconPath = iconPath.Replace(" ", "%20");
                            }
                            else
                            {
                                iconPath = Path.GetFullPath(iconPath);
                                if (!File.Exists(iconPath))
                                {//Hakcs to get gml kml to work (at least for the one test file)
                                    getImage = false;
                                    iconName = Path.GetFileName(iconPath);
                                }
                            }

                            if (getImage)
                            {
                                if(DepictionAccess.PathService == null)
                                {
                                    //When combined with topo maps and earthquake quickstart, the icon for the earthquake
                                    //would not get saved due to thread ownership issues.
                                    BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(iconPath, out iconName);
                                }else
                                {
                                    //This seems to fix the bug, but i think it only fixes it because it makes this take more time
                                    //to load than the original code.
                                    var bitmap = BitmapSaveLoadService.DownLoadImageFromURI(iconPath, 20, 20);
                                    var storeDir = Path.Combine(DepictionAccess.PathService.TempFolderRootPath, "TempImages");
                                    var tempName = Path.Combine(storeDir, Path.GetFileName(iconPath));
                                    BitmapSaveLoadService.SaveBitmap(bitmap, tempName);
                                    BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(tempName, out iconName);
                                }
                            }
                            var depIconPath = new DepictionIconPath(IconPathSource.File, iconName);
                            imageUri = iconName;
                            property = new DepictionElementProperty("IconPath", "Icon path", depIconPath);
                            property.Editable = false;
                            rawPrototype.AddPropertyOrReplaceValueAndAttributes(property, false);
                            //                            rawPrototype.SetPropertyValue("IconPath", depIconPath, false);
                        }
                    }
                }
                if (!useShapeFileColors)
                {
                    SetFillColor(rawPrototype, count);
                }
                var baseTypeByGeometry = ElementFactory.GeometryTypeToDefaultDepictionType(feature.GeoApiGeometry.GeometryType);

                if (fullImageBounds != null && !string.IsNullOrEmpty(imageUri))
                {
                    var depictionImageMetadata = new DepictionImageMetadata(imageUri, fullImageBounds.TopLeft,
                                                                            fullImageBounds.BottomRight, imageUri);
                    depictionImageMetadata.RotationDegrees = (180 / Math.PI) * rotationRad;
                    rawPrototype.SetImageMetadataWithoutNotification(depictionImageMetadata);
                    baseTypeByGeometry = "Depiction.Plugin.Image";
                }
                if (DepictionStringService.AutoDetectElementString.Equals(rawPrototype.ElementType, StringComparison.InvariantCultureIgnoreCase) ||
                    ElementPrototypeLibrary.RawPrototypeName.Equals(rawPrototype.ElementType, StringComparison.InvariantCultureIgnoreCase))
                {
                    rawPrototype.UpdateTypeIfRaw(baseTypeByGeometry);
                }
                //                var baseTypeByGeometry = ElementFactory.GeometryTypeToDefaultDepictionType(feature.GeoApiGeometry.GeometryType);
                //
                //                if(curentzoi.type == desired zoitype)
                //                var zoiType = rawPrototype.GetElementZOIDepictionGeometryType();
                //                rawPrototype.SetPropertyValue("draggable", false, false);//Doesn't work because the property doesn't exist
                var prop = new DepictionElementProperty("Draggable", false);
                prop.Deletable = false;
                rawPrototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

                var descriptProperty = rawPrototype.GetPropertyByInternalName("description");
                if (descriptProperty != null)
                {
                    descriptProperty.IsHoverText = true;
                }
                prototypeList.Add(rawPrototype);
            }
            return prototypeList;
        }

        #endregion

        public static bool IsGeometryANonGPXRouteWaypoint(OgrPropertiesAndIGeometry feature, OgrFileType fileType)
        {
            if (fileType != OgrFileType.GPX) return true;

            if (feature.GeoApiGeometry is Point)
            {
                //is it a waypoint standalone or is it a waypoint in a TRACK/ROUTE?
                foreach (var nvp in feature.OgrProperties)
                {
                    if (nvp.Key.Equals("track_fid") || nvp.Key.Equals("track_seg_id") || nvp.Key.Equals("track_seg_point_id")
                        || nvp.Key.Equals("route_fid") || nvp.Key.Equals("route_point_id"))
                        return false;
                }
            }
            return true;
        }
    }
    public enum OgrFileType
    {
        GML,
        Shapefile,
        GPX
    }
    public class OgrPropertiesAndIGeometry
    {
        public Dictionary<string, object> OgrProperties { get; private set; }
        public IGeometry GeoApiGeometry { get; set; }

        public OgrPropertiesAndIGeometry()
        {
            OgrProperties = new Dictionary<string, object>();
        }
    }
}