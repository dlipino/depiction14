﻿using System.ComponentModel;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.CoreModel.ValueTypes.Measurements
{
    /// <summary>
    /// A type representing an area, e.g. square meters. For use as the type of an element's property.
    /// </summary>
    [TypeConverter("Depiction.CoreModel.TypeConverter.AreaConverter")]
    public class Area : BaseMeasurement
    {
        #region Conversion constants
        public const double in2Toft2 = 0.00694444444; /*in^2 to ft^2*/
        public const double ft2Toin2 = 144;/*ft^2 to in^2*/

        public const double in2Tomile2 = 0.000015782828 * 0.000015782828;/*in^2 to miles^2*/
        public const double mile2Toin2 = 63360.0 * 63360.0;/*miles^2 to in^2*/
        public const double mile2Toft2 = 5280.0 * 5280.0; /*miles^2 to ft^2*/
        public const double ft2Tomile2 = 1 / mile2Toft2;

        public const double in2Tocm2 = 6.4516; /*in^2 to cm^2*/
        public const double cm2Toin2 = 0.15500031; /*cm^2 to in^2*/
        public const double in2Tom2 = 0.00064516; /*in^2 to m^2*/
        public const double m2Toin2 = 1550.0031; /*m^2 to in^2*/

        public const double in2Tokm2 = 0.0000254 * 0.0000254;/*in^2 to km^2*/
        public const double km2Toin2 = 39370.078 * 39370.078;/*km^2 to in^2*/
        public const double cm2Tom2 = 0.0001;/*cm^2 to m^2*/
        public const double m2Tocm2 = 10000;/*m^2 to cm^2*/
        public const double cm2Tokm2 = 0.00001 * 0.00001;/*cm^2 to km^2*/
        public const double km2Tocm2 = 100000d * 100000d;/*km^2 to cm^2*/
        public const double cm2Toft2 = 0.00107639104; /*cm^2 to ft^2*/
        public const double ft2Tocm2 = 929.0304; /*ft^2 to cm^2*/
        public const double cm2Tomile2 = 0.000006213 * 0.000006213; /*cm^2 to miles^2*/

        public const double mile2Tocm2 = 160934.4 * 160934.4;/*miles^2 to cm^2*/
        public const double m2Toft2 = 10.7639104; /*m^2 to ft^2*/
        public const double ft2Tom2 = 1 / m2Toft2;
        public const double m2Tomile2 = 0.000621371 * 0.000621371;/*m^2 to miles^2*/
        public const double mile2Tom2 = 1 / m2Tomile2;
        public const double km2Tom2 = 1000d * 1000d;/*km^2 to m^2*/
        public const double m2Tokm2 = 0.001 * 0.001;/*m^2 to km^2*/
        public const double km2Toft2 = 3280.839 * 3280.839; /*km^2 to ft^2*/
        public const double ft2Tokm2 = 1 / km2Toft2;
        public const double km2Tomile2 = 0.6213712 * 0.6213712; /*km^2 to miles^2*/
        public const double mile2Tokm2 = 1 / km2Tomile2;
        
        #endregion

        private static readonly double[,] conversionFactors = new[,]
                                                                  {{1,cm2Tom2,cm2Tokm2,cm2Toin2,cm2Toft2,cm2Tomile2},
                                                                   {m2Tocm2,1,m2Tokm2,m2Toin2,m2Toft2,m2Tomile2},
                                                                   {km2Tocm2,km2Tom2,1,km2Toin2,km2Toft2,km2Tomile2},
                                                                   {in2Tocm2,in2Tom2,in2Tokm2,1,in2Toft2,in2Tomile2},
                                                                   {ft2Tocm2,ft2Tom2,ft2Tokm2,ft2Toin2,1,ft2Tomile2},
                                                                   {mile2Tocm2,mile2Tom2,mile2Tokm2,mile2Toin2,mile2Toft2,1}
                                                                  };

        private static readonly double[,] offsetFactors = new[,] { {0d,0d,0d,0d,0d,0d},
                                                                   {0d,0d,0d,0d,0d,0d},
                                                                   {0d,0d,0d,0d,0d,0d},
                                                                   {0d,0d,0d,0d,0d,0d},
                                                                   {0d,0d,0d,0d,0d,0d},
                                                                   {0d,0d,0d,0d,0d,0d}};
        
        /// <summary>
        /// Empty constructor needed for serialization.
        /// </summary>
        public Area()
        {}

        ///<summary>
        /// Create a new area object.
        ///</summary>
        ///<param name="scale"></param>
        ///<param name="value">The value in this measurement systemAndScale and scale. It</param>
        ///<param name="system"></param>
        public Area(MeasurementSystem system, MeasurementScale scale, double value)
        {
            numericValue = value;
            initialScale = scale;
            initialSystem = system;
        }

        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }

        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale,bool isSingular)
        {
            var systemAndScale = (int) system + (int) scale;
            var nonSingle = "s";
            if (isSingular)
            {
                nonSingle = string.Empty;
            }
            switch (systemAndScale)
            {
                case MetricSmall:
                    return "square centimeter" + nonSingle;
                case MetricNormal:
                    return "square meter" + nonSingle;
                case MetricLarge:
                    return "square km";// +nonSingle;
                case ImperialSmall:
                    return "square inches" + nonSingle;
                case ImperialNormal:
                    if (isSingular) return "square foot";
                    return "square feet";
                case ImperialLarge:
                    return "square mile" + nonSingle;
                
                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Area(initialSystem,initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }
    }
}