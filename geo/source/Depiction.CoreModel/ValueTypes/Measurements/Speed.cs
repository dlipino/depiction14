﻿using System.ComponentModel;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.CoreModel.ValueTypes.Measurements
{
    [TypeConverter("Depiction.CoreModel.TypeConverter.SpeedConverter")]
    public class Speed : BaseMeasurement
    {
        const double MphToFps = 1.46666667;/*mph to f/s*/
        private const double fpsToMph = 1/MphToFps;
        const double MphToKph = 1.609344;/*mph to kph*/
        private const double kphTomph = 1 / MphToKph;
        const double MphToMps = 0.44704; /*mph to m/s*/
        private const double mpsToMph = 1/MphToMps;

        const double MpsToKph = 3.6;/*m/s to kph*/
        private const double kphToMps = 1/MpsToKph;
        const double MpSToFps = 3.2808399; /*m/s to f/s*/
        private const double fpsToMps = 1/MpSToFps;
        const double KphToFps = 0.911344415;/*kph to f/s*/
        private const double fpsTokph = 1/KphToFps;
        //MS =1 (Metric = 0 Small = 1) m/s
        //MN =2(Metric = 0 Medium = 2) kph
        //ML =3(Metric = 0 Large = 3) kph
        //IS = 4 (Imperial = 3 Small = 1) f/s
        //IN = 5(Imperial = 3 Medium = 2) mph 
        //IL = 6(Imperial = 3 Large = 3) mph
        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1d, MpsToKph, MpsToKph, MpSToFps, mpsToMph,mpsToMph},
                                                                      {kphToMps, 1d, 1d, KphToFps, kphTomph, kphTomph},
                                                                      {kphToMps, 1d, 1d, KphToFps, kphTomph, kphTomph},
                                                                      {fpsToMps, fpsTokph, fpsTokph, 1d, mpsToMph,mpsToMph},
                                                                      {MphToMps, MphToKph, MphToKph, MphToFps, 1, 1},
                                                                      {MphToMps, MphToKph, MphToKph, MphToFps, 1, 1}
                                                                  };

        private static readonly double[,] offsetFactors = new[,] 
                                                              { { 0d, 0d, 0d, 0d, 0d, 0d}, 
                                                                { 0d, 0d, 0d, 0d, 0d, 0d},
                                                                { 0d, 0d, 0d, 0d, 0d, 0d},
                                                                { 0d, 0d, 0d, 0d, 0d, 0d},
                                                                { 0d, 0d, 0d, 0d, 0d, 0d},
                                                                { 0d, 0d, 0d, 0d, 0d, 0d}};
        /// <summary>
        /// Empty constructor needed for serialization.
        /// </summary>
        public Speed()
        { }

        public Speed(MeasurementSystem system, MeasurementScale scale, double value)
        {

            initialSystem = system;
            initialScale = scale;
            this.numericValue = value;
        }

//        private static readonly double[,] conversionFactors = new[,] 
//                                                                  { { 1.0 /*mph to mph*/,1.46666667/*mph to f/s*/,1.609344/*mph to kph*/, 0.44704 /*mph to m/s*/,1.0 /*mph to mph*/,1.609344/*mph to kph*/}, 
//                                                                    { 0.681818182/*f/s to mph*/,  1.0/*f/s to f/s*/,1.09728/*f/s to kph*/, 0.3048/*f/s to m/s*/,0.681818182/*f/s to mph*/, 1.09728/*f/s to kph*/},
//                                                                    { 0.621371192/*kph to mph*/, 0.911344415/*kph to f/s*/, 1.0 /*kph to kph*/, 0.277777778/*kph to m/s*/,0.621371192/*kph to mph*/, 1.0 /*kph to kph*/},
//                                                                    { 2.23693629/*m/s to mph*/,3.2808399 /*m/s to f/s*/, 3.6/*m/s to kph*/,1.0/*m/s to m/s*/, 2.23693629/*m/s to mph*/,3.6/*m/s to kph*/},
//                                                                    { 1.0 /*mph to mph*/,1.46666667/*mph to f/s*/,1.609344/*mph to kph*/, 0.44704 /*mph to m/s*/,1.0 /*mph to mph*/,1.609344/*mph to kph*/}, 
//                                                                    { 0.621371192/*kph to mph*/, 0.911344415/*kph to f/s*/, 1.0 /*kph to kph*/, 0.277777778/*kph to m/s*/,0.621371192/*kph to mph*/, 1.0 /*kph to kph*/}
//                                                                  };



        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }

        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }
        //MS =1 (Metric = 0 Small = 1) m/s
        //MN =2(Metric = 0 Medium = 2) kph
        //ML =3(Metric = 0 Large = 3) kph
        //IS = 4 (Imperial = 3 Small = 1) f/s
        //IN = 5(Imperial = 3 Medium = 2) mph 
        //IL = 6(Imperial = 3 Large = 3) mph
        public override string GetUnits(MeasurementSystem system, MeasurementScale scale,bool singular)
        {
            var systemAndScale = (int)system + (int)scale;
            switch (systemAndScale)
            {
                case MetricSmall:
                    return "m/s";
                case MetricNormal:
                case MetricLarge:
                    return "kph";
                case ImperialSmall:
                    return "ft/s";
                case ImperialNormal:
                case ImperialLarge:
                    return "mph";
                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Speed(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }


    }
}