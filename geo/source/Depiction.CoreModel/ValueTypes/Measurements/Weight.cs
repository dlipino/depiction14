﻿using System.ComponentModel;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.ValueTypes.Measurements
{
    [TypeConverter("Depiction.CoreModel.TypeConverter.WeightConverter")]
    public class Weight : BaseMeasurement
    {
        #region Conversion factors
        
        public const double lbTokg = 0.45359237;/*lbs to kg*/
        public const double kgToLb = 2.20462262;/*kg to lbs*/
        public const double metricTonTolb = 2204.62262;/*metric tons to lbs*/
        public const double lbToMetricTon = 0.00045359237; /*lbs to metric tons*/
        public const double tonTolb = 2000;/*tons to lbs*/
        public const double lbToTon = 0.0005; /*lbs to tons*/
        public const double tonTokg = 907.18474; /*tons to kg*/
        public const double kgToTon = 0.00110231131;/*kg to tons*/
        public const double kgToMetricTon = 0.001;/*kg to metric tons*/
        public const double metricTonTokg = 1000; /*metric tons to kg*/
        public const double metricTonToTon = 1.10231131;/*metric tons to tons*/
        public const double tonToMetricTon = 0.90718474;/*tons to metric tons*/
        
        #endregion
        /// <summary>
        /// Empty constructor needed for serialization.
        /// </summary>
        public Weight()
        {}
        //MS =0 (Metric = 0 Small = 0)
        //MN =1(Metric = 0 Medium = 1)
        //ML =2(Metric = 0 Large = 2)
        //IS = 3 (Imperial = 3 Small = 0)
        //IN = 4(Imperial = 3 Medium = 1)
        //IL = 5(Imperial = 3 Large = 2)
        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1,1,kgToMetricTon,kgToLb,kgToLb,kgToTon},
                                                                      {1,1,kgToMetricTon,kgToLb,kgToLb,kgToTon},
                                                                      {metricTonTokg,metricTonTokg,1,metricTonTolb,metricTonTolb,metricTonToTon},
                                                                      {lbTokg,lbTokg,lbToMetricTon,1,1,lbToTon},
                                                                      {lbTokg,lbTokg,lbToMetricTon,1,1,lbToTon},
                                                                      {tonTokg,tonTokg,tonToMetricTon,tonTolb,tonTolb,1},

                                                                  };

        private static readonly double[,] offsetFactors = new[,] 
                                                              {
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d},
                                                                  {0d,0d,0d,0d,0d,0d}
                                                              };
        public Weight(MeasurementSystem system,MeasurementScale scale, double value)
        {
            initialSystem = system;
            initialScale = scale;
            numericValue = value;
        }

        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }

        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale, bool isSingular)
        {
            var systemAndScale = (int) system + (int) scale;
            var nonSingle = "s";
            if (isSingular)
            {
                nonSingle = string.Empty;
            }
            switch (systemAndScale)
            {
                case MetricSmall:
                case MetricNormal:
                    return "kg";// +nonSingle;
                case MetricLarge:
                    return "metric ton" + nonSingle;
                case ImperialSmall:
                case ImperialNormal:
                    return "pound" + nonSingle;
                case ImperialLarge:
                    return "ton" + nonSingle;
                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Weight(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }
    }
}