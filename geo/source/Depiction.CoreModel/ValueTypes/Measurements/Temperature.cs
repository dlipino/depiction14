﻿using System.ComponentModel;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.ValueTypes.Measurements
{
    [TypeConverter("Depiction.CoreModel.TypeConverter.TemperatureConverter")]
    public class Temperature : BaseMeasurement
    {
        #region Conversion constants

        private const double fToCMult = 0.55555;
        private const double fToCOffset = -17.77777;
        private const double cToFMult = 1.8;
        private const double cToFOffset = 32d;
        #endregion

        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1,1,1,cToFMult,cToFMult,cToFMult},
                                                                      {1,1,1,cToFMult,cToFMult,cToFMult},
                                                                      {1,1,1,cToFMult,cToFMult,cToFMult},
                                                                      {fToCMult,fToCMult,fToCMult,1,1,1},
                                                                      {fToCMult,fToCMult,fToCMult,1,1,1},
                                                                      {fToCMult,fToCMult,fToCMult,1,1,1}
                                                                  };

        private static readonly double[,] offsetFactors = new[,]
                                                              {
                                                                  {0,0,0,cToFOffset,cToFOffset,cToFOffset},
                                                                  {0,0,0,cToFOffset,cToFOffset,cToFOffset},
                                                                  {0,0,0,cToFOffset,cToFOffset,cToFOffset},
                                                                  {fToCOffset,fToCOffset,fToCOffset,0,0,0},
                                                                  {fToCOffset,fToCOffset,fToCOffset,0,0,0},
                                                                  {fToCOffset,fToCOffset,fToCOffset,0,0,0}
                                                              };

        public Temperature(MeasurementSystem system, MeasurementScale scale, double value)
        {
            initialSystem = system;
            initialScale = scale;
            numericValue = value;
        }

        public Temperature()
        { }


        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }
        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale,bool singular)
        {
            var systemAndScale = (int)system + (int)scale;
            switch (systemAndScale)
            {
                case MetricSmall:
                case MetricNormal:
                case MetricLarge:
                    return "°C";
                case ImperialSmall:
                case ImperialNormal:
                case ImperialLarge:
                    return "°F";

                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Temperature(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }
    }
}