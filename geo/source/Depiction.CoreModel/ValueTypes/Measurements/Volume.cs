﻿using System.ComponentModel;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.ValueTypes.Measurements
{
    [TypeConverter("Depiction.CoreModel.TypeConverter.VolumeConverter")]
    public class Volume : BaseMeasurement
    {
        const double galToOz = 128;/*gal to oz*/
        const double ozToGal = 1/galToOz;

        const double galToL = 3.78541178;/*gal to l*/
        private const double lToGal = 1 / galToL;
        private const double galToml = 3785.41178; /*gallons to ml*/ 
        private const double mlToGal = 1 / galToml;

        const double LToml = 1000;/*l to ml*/
        const double mlToL = 1 / LToml;/*l to ml*/
        const double mlToOz = 0.0338140227; /*ml to oz*/
        private const double OzToMl = 1 / mlToOz;
       
        const double LToOz = 33.8140227;/*l to oz*/
        private const double OzToL = 1 / LToOz;


        private static readonly double[,] conversionFactors = new[,]
                                                                  {
                                                                      {1,mlToL,mlToL,mlToOz,mlToGal,mlToGal},
                                                                      {LToml,1,1,LToOz,lToGal,lToGal},
                                                                      {LToml,1,1,LToOz,lToGal,lToGal},
                                                                      {OzToMl,OzToL,OzToL,1,ozToGal,ozToGal},
                                                                      {galToml,galToL,galToL,galToOz,1d,1d},
                                                                      {galToml,galToL,galToL,galToOz,1d,1d}
                                                                  };

        private static readonly double[,] offsetFactors = new[,]  { 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}, 
                                                                      { 0d,0d,0d,0d,0d,0d}
                                                                  };
        public Volume()
        { }
        public Volume(MeasurementSystem measurementSystem, MeasurementScale measurementScale, double value)
        {
            initialSystem = measurementSystem;
            initialScale = measurementScale;

            this.numericValue = value;
        }

        protected override double[,] ConversionFactors
        {
            get { return conversionFactors; }
        }

        protected override double[,] OffsetFactors
        {
            get { return offsetFactors; }
        }

        public override string GetUnits(MeasurementSystem system, MeasurementScale scale,bool isSingular)
        {
            int systemAndScale = (int)system + (int)scale;
            var nonSingle = "s";
            if(isSingular)
            {
                nonSingle = string.Empty;
            }
            switch (systemAndScale)
            {
                case MetricSmall:
                    return "milliliter" + nonSingle;
                case MetricNormal:
                case MetricLarge:
                    return "liter" + nonSingle;
                case ImperialSmall:
                    return "ounce" + nonSingle;
                case ImperialNormal:
                case ImperialLarge:
                    return "gallon" + nonSingle;

                default:
                    return "";
            }
        }

        public override IMeasurement DeepClone()
        {
            var measurement = new Volume(initialSystem, initialScale, numericValue);
            measurement.UseDefaultScale = UseDefaultScale;
            return measurement;
        }
    }
}