﻿using System.Windows.Media;
using System.Windows.Media.Imaging;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.OldInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;

namespace Depiction.CoreModel.ValueTypes
{
    public class GridSpatialData : IGridSpatialData
    {
        #region variables

        readonly byte[] zGrid;
        private readonly int dataHeightInPixels;
        private readonly int dataWidthInPixels;
        private readonly ILatitudeLongitude topLeft;
        private readonly ILatitudeLongitude bottomRight;
        private DepictionGeometry boundingBox;

        private bool isEmpty;

        #endregion

        #region Properties

        public ILatitudeLongitude TopLeft
        {
            get { return topLeft; }
        }
        public bool IsEmpty
        {
            get { return isEmpty; }
        }

        public bool IsValid
        {
            get { return true; }
        }

        public string GeometryType
        {
            get { return "Coverage"; }
        }
        public IDepictionGeometry BoundingBox
        {
            get { return boundingBox; }
        }

        public ILatitudeLongitude BottomRight
        {
            get { return bottomRight; }
        }

        public int PixelHeight
        {
            get { return dataHeightInPixels; }
        }

        public int PixelWidth
        {
            get { return dataWidthInPixels; }
        }

        public double HeightLat
        {
            get { return topLeft.Latitude - bottomRight.Latitude; }

        }
        public double WidthLon
        {
            get { return bottomRight.Longitude - topLeft.Longitude; }
        }

        #endregion

        #region COnstructor

        public GridSpatialData(int xsize, int ysize, ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            var cnt = xsize * ysize;
            dataWidthInPixels = xsize;
            dataHeightInPixels = ysize;
            this.topLeft = topLeft;
            this.bottomRight = bottomRight;
            boundingBox = new DepictionGeometry(new MapCoordinateBounds(topLeft, bottomRight));
            zGrid = new byte[cnt];
            for (var i = 0; i < cnt; i++)
            {
                zGrid[i] = 0;
            }
        }

        #endregion

        #region Helper methods, in no particular order

        public bool Intersects(IDepictionSpatialData g)
        {
            return SpatialOps.Intersects(this, g);
        }
        public byte GetValue(ILatitudeLongitude pos)
        {
            var lon = pos.Longitude;
            var lat = pos.Latitude;
            return GetValue(lon, lat);
        }

        public byte GetValue(double lon, double lat)
        {
            double latHeight = topLeft.Latitude - bottomRight.Latitude;
            double lonWidth = bottomRight.Longitude - topLeft.Longitude;
            lon -= topLeft.Longitude;
            lat -= bottomRight.Latitude;
            int col = (int)(lon * dataWidthInPixels / lonWidth);
            int row = (int)(lat * dataHeightInPixels / latHeight);
            return GetValueAtRowColumn(row, col);
        }

        #region Image part of the GridSpatialData, not sure if this should be moved out
        public BitmapSource GenerateBitmap()
        {
            const double dpi = 96;
            const int bytesPerPixel = 4;
            var pixelData = new byte[dataWidthInPixels * dataHeightInPixels * bytesPerPixel];

            int dataRow = 0;
            int bitmapRowWidth = dataWidthInPixels * bytesPerPixel;
            int column = 0;
            for (int dataIndex = 0; dataIndex < zGrid.Length; )
            {
                int bitmapIndex = (dataHeightInPixels - 1 - dataRow) * bitmapRowWidth + column * bytesPerPixel;

                byte alpha = 0;
                if (zGrid[dataIndex] > 0) alpha = 255;

                pixelData[bitmapIndex] = 100;
                pixelData[bitmapIndex + 1] = 100;
                pixelData[bitmapIndex + 2] = 100;
                pixelData[bitmapIndex + 3] = alpha;

                // Handle row transitions
                column++;
                dataIndex++;
                if (column >= dataWidthInPixels)
                {
                    dataRow++;
                    column = 0;
                }
            }
            return BitmapSource.Create(dataWidthInPixels, dataHeightInPixels, dpi, dpi, PixelFormats.Bgra32, null, pixelData, dataWidthInPixels * bytesPerPixel);
        }

        public BitmapSource GenerateOtherBitmap()
        {
            double dpi = 96;
            byte[] pixelData = new byte[dataWidthInPixels * PixelWidth];
            int yd = PixelWidth;


            for (int y = 0; y < PixelWidth; ++y)
            {
                int yIndex = y * dataWidthInPixels;
                yd--;
                int yDIndex = yd * dataWidthInPixels;
                for (int x = 0; x < dataWidthInPixels; ++x)
                {
                    byte val = 133;
                    if (zGrid[x + yIndex] > 0) val = 33;
                    pixelData[x + yDIndex] = val;
                }
            }
            return BitmapSource.Create(dataWidthInPixels, PixelWidth, dpi, dpi, PixelFormats.Indexed8, BitmapPalettes.Halftone125Transparent, pixelData,
                                       dataWidthInPixels);
        }
        #endregion

        public float y(int yVal)
        {
            return yVal;
        }
        public float x(int xVal)
        {
            return xVal;
        }


        public byte GetValueAtRowColumn(int row, int column)
        {
            return zGrid[row * dataWidthInPixels + column];
        }

        public byte GetValueAtColumnRow(int column, int row)
        {
            return zGrid[row * dataWidthInPixels + column];
        }

        public void SetZ(int row, int col, byte elev)
        {
            zGrid[row * dataWidthInPixels + col] = elev;
            isEmpty = false;
        }

        public void GimmeANegative(byte otherValue)
        {
            for (var i = 0; i < zGrid.Length; i++)
            {
                if (zGrid[i] > 0)
                    zGrid[i] = 0;
                else
                    zGrid[i] = otherValue;
            }
        }

        public GridSpatialData ExpandByOnePixel()
        {
            double latHeight = topLeft.Latitude - bottomRight.Latitude;
            double lonWidth = bottomRight.Longitude - topLeft.Longitude;
            var newPixelHeight = PixelHeight + 2;
            var newPixelWidth = PixelWidth + 2;
            var newWidth = newPixelWidth * lonWidth / PixelWidth;
            var newHeight = latHeight / dataHeightInPixels * newPixelHeight;
            var onePixelLonWidth = newWidth / newPixelWidth;
            var onePixelLatHeight = newHeight / newPixelHeight;

            var nTopLeft = new LatitudeLongitude(topLeft.Latitude + onePixelLatHeight, topLeft.Longitude - onePixelLonWidth);
            var nBottomRight = new LatitudeLongitude(bottomRight.Latitude - onePixelLatHeight, bottomRight.Longitude + onePixelLonWidth);
            var newGrid = new GridSpatialData(newPixelWidth, newPixelHeight, nTopLeft, nBottomRight);
            for (var row = 0; row < PixelHeight; row++)
            {
                for (int col = 0; col < PixelWidth; col++)
                {
                    newGrid.SetZ(row + 1, col + 1, GetValueAtRowColumn(row, col));
                }
            }

            return newGrid;
        }
        #endregion
    }
}