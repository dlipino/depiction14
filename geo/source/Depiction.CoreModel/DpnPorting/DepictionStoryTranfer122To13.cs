using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.TypeConverter;
using Depiction.CoreModel.ValueTypes;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.CoreModel.DpnPorting
{
    //This is only expected to be an 80% solution.
    public class DepictionStoryTranfer122To13
    {

        static public IDepictionStory TranformStoryTo13Story(Helper12DeserializationClasses.PersistenceMemento12 storyMemento, string oldFileName)
        {
            var transformer = new DepictionStoryTranfer122To13();

            return transformer.Transfer122StoryDataToCurrentDepictionStoryType(storyMemento, oldFileName);
        }
        public static string ConvertTypeNameFrom122To133(string type122)
        {
            var convertedTypeName = type122;
            if (ElementPrototypeLibrary.TypeConversionDictionary122To13.ContainsKey(type122))
            {
                convertedTypeName = ElementPrototypeLibrary.TypeConversionDictionary122To13[convertedTypeName];
            }
            return convertedTypeName;
        }

        public IDepictionStory Transfer122StoryDataToCurrentDepictionStoryType(Helper12DeserializationClasses.PersistenceMemento12 dpn122, string oldFileName)
        {
            SerializationService.UpdateProgressReport(int.MaxValue,
                                                      string.Format("Converting {0} to a 1.3 depiction", oldFileName));
            var newDepiction = new DepictionStory();

            var oldStory = dpn122.Story;
            var newBounds = TrasformOldRegionExtentToNew(oldStory.RegionExtent);
            newDepiction.DepictionGeographyInfo = new DepictionGeographicInfo(oldStory.RegionCodes);
            newDepiction.DepictionGeographyInfo.DepictionRegionBounds = newBounds;
            newDepiction.DepictionGeographyInfo.DepictionMapWindow = dpn122.VisualData.ViewportExtent.GeoBounds;
            newDepiction.DepictionGeographyInfo.DepictionStartLocation = newBounds.Center;
            newDepiction.Metadata = TransferStoryMetadata(oldStory.Metadata);
            if (oldStory.BitmapResources != null)
            {
                var newResources = newDepiction.ImageResources;
                foreach (var key in oldStory.BitmapResources.Keys)
                {
                    if (!newResources.Contains(key))
                    {
                        var image = oldStory.BitmapResources[key] as ImageSource;
                        var keystring = key.ToString();
                        var path = keystring.Split(':');
                        var name = Path.GetFileName(keystring);
                        if (path.Length > 2)
                        {
                            name = Path.GetFileName(path[0] + ":" + path[1]);
                        }
                        if (image != null)
                        {
                            if (!newResources.Contains(name))
                            {
                                newResources.Add(name, image);
                            }
                        }
                    }
                }
            }

            //Combine the element libraries, anything from 1.2.2 goes into the loaded sections
            //Actually, the combine will be done during the element creation, this reduces the amount
            //of unused element types.

            //Transfer annotations
            foreach (var annotation12 in oldStory.Annotations)
            {
                newDepiction.AddAnnotation(Transfer122AnnotationTo13Annotation(annotation12, newBounds.Center));
            }

            //Tranfer elements
            foreach (var element12 in oldStory.ElementRepository.AllElementsInternal)
            {
                try
                {
                    var newElement = Tranfer122ElementTo13Element(element12, dpn122.InherentPrototypes);
                    var oldElementTags = oldStory.Tags.GetTagsForElementID(newElement.ElementKey);

                    //Order is importatnt
                    newDepiction.CompleteElementRepository.AddElement(newElement);
                    newDepiction.CompleteElementRepository.AddTagsToElement(newElement,oldElementTags.ToList());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            foreach (var element12 in oldStory.UnregisteredElementRepository.AllElementsInternal)
            {
                try
                {
                    var newElement = Tranfer122ElementTo13Element(element12, dpn122.InherentPrototypes);
                    newDepiction.CompleteElementRepository.AddElement(newElement);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            DepictionAccess.ElementLibrary.NotifyPrototypeLibraryChange();
            TransferElementsToMainMapAndRevealers(newDepiction, dpn122);

            var newFileName = Path.GetFileNameWithoutExtension(oldFileName) + "_13" + Path.GetExtension(oldFileName);
            newDepiction.DepictionsFileName = Path.Combine(Path.GetDirectoryName(oldFileName), newFileName);
            newDepiction.DepictionNeedsSaving = true;

            return newDepiction;
        }

        public void TransferElementsToMainMapAndRevealers(IDepictionStory newDepiction, Helper12DeserializationClasses.PersistenceMemento12 dpn122)
        {
            var visualData122 = dpn122.VisualData;

            var primaryDisplayer122 = visualData122.PrimaryDisplayer;
            newDepiction.MainMap.DisplayerName = primaryDisplayer122.FriendlyName;

            var elementQueryResult = from id in primaryDisplayer122.ElementIds
                                     from element in newDepiction.CompleteElementRepository.AllElements
                                     where id.Equals(element.ElementKey)
                                     select new { element };
            //sad
            foreach (var element in elementQueryResult)
            {
                newDepiction.MainMap.AddElementList(new List<IDepictionElement> { element.element });
            }
            foreach (var revealer122 in visualData122.RevealerCollection)
            {
                var revealerElementQuery = from id in revealer122.ElementIds
                                           from element in newDepiction.CompleteElementRepository.AllElements
                                           where id.Equals(element.ElementKey)
                                           select new { element };
                var revealer = new DepictionRevealer();
                revealer.DisplayerName = revealer122.FriendlyName;
                //For now just give it region bounds
                revealer.RevealerBounds = newDepiction.DepictionGeographyInfo.DepictionRegionBounds;
                if (revealer122.RevealerGeoBounds != null)
                {
                    revealer.RevealerBounds = revealer122.RevealerGeoBounds;
                }
                revealer.SetPropertyValue("Maximized", revealer122.RevealerVisible);
                revealer.SetPropertyValue("BorderVisible", revealer122.BorderVisible);
                revealer.SetPropertyValue("Anchored", !revealer122.Moveable);
                revealer.SetPropertyValue("Opacity", revealer122.Opacity);
                var revealerShape = RevealerShapeType.Rectangle;
                if (!revealer122.RectShape) revealerShape = RevealerShapeType.Circle;
                revealer.SetPropertyValue("RevealerShape", revealerShape);
                revealer.SetPropertyValue("TopMenu", !revealer122.ButtonsAndInfoVisible);
                //                revealer.SetPropertyValue("BottomMenu", true);
                revealer.SetPropertyValue("SideMenu", false);
                revealer.SetPropertyValue("PermaTextVisible", revealer122.PermaTextVisible);
                revealer.SetPropertyValue("PermaTextX", revealer122.MinContentLocationX);
                revealer.SetPropertyValue("PermaTextY", revealer122.MinContentLocationY);
                newDepiction.AddRevealer(revealer);
                foreach (var element in revealerElementQuery)
                {
                    revealer.AddElementList(new List<IDepictionElement> { element.element });
                }
            }
        }

        public IDepictionAnnotation Transfer122AnnotationTo13Annotation(IAnnotationElement12 annotation12, ILatitudeLongitude backupLocation)
        {
            var newAnnotation = new DepictionAnnotation();
            newAnnotation.AnnotationText = annotation12.AnnotationText;
            newAnnotation.PixelHeight = annotation12.Height;
            newAnnotation.PixelWidth = annotation12.Width;
            newAnnotation.ForegroundColor = annotation12.ForegroundColor;
            newAnnotation.BackgroundColor = annotation12.BackgroundColor;
            newAnnotation.ScaleWhenCreated = annotation12.PixelWidthAtCreation;
            newAnnotation.ContentLocationX = annotation12.ContentLocationX;
            newAnnotation.ContentLocationY = annotation12.ContentLocationY;
            newAnnotation.MapLocation = backupLocation;
            if (annotation12.MapLocation.IsValid)
            {
                newAnnotation.MapLocation = annotation12.MapLocation;
            }

            return newAnnotation;
        }

        public IDepictionElement Tranfer122ElementTo13Element(IElementInternal12 element12, IElementPrototypeLibrary oldPrototypes)
        {
            var elementLibrary = DepictionAccess.ElementLibrary;
            if (elementLibrary == null)
            {
                throw new Exception("Cannot convert elements witout an element library.");
            }
            //For now assume no noew click actions have been created.
            var convertedTypeName = ConvertTypeNameFrom122To133(element12.ElementType);
            var newElement = ElementFactory.CreateElementFromTypeString(convertedTypeName);
            if (newElement == null)
            {
                //thanks to an apparent bug in 1.2.2 saving image types get their type changed
                newElement = ElementFactory.CreateElementFromTypeString(element12.TypeDisplayName);
                if (newElement == null)
                {
                    //if we get to here, try and get an old prototype
                    IElementPrototype oldPrototype = null;
                    if(oldPrototypes != null) oldPrototype = oldPrototypes.GuessPrototypeFromString(element12.ElementType);

                    if (oldPrototype == null)
                    {
                        oldPrototype = new ElementPrototype(element12.ElementType, element12.TypeDisplayName, false, false); 
                        //throw new Exception(string.Format("Element type {0} could not be created.", element12.ElementType));
                    }

                    newElement = ElementFactory.CreateElementFromPrototype(oldPrototype);
                    if (newElement == null) throw new Exception(string.Format("Element type {0} could not be created.", element12.ElementType));
                    //Ok so i guess the thing exists. add it to the main element library.
                    oldPrototype.PrototypeOrigin = ElementPrototypeLibrary.LoadedPrototypeDescription;
                    DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(oldPrototype);
                }
            }
            Transfer122ElementPropertiesValuesTo13Element(newElement, element12);
            var depictionElement12 = newElement as DepictionElementParent;
            if (depictionElement12 == null) return null;

            depictionElement12.ElementID = element12.ElementKey;

            var oldZOI = element12.ZoneOfInfluence;

            if (oldZOI != null)
            {
                if (string.IsNullOrEmpty(oldZOI.ToString()))
                {//This is actully very bad because that means the roadnetwork could not load the ZOI
                    //haven't figured out why it wouldn't load something like that yet
                    //HACK
                    if (element12.ElementType.Contains("Network"))
                    {
                        foreach (var prop in element12.Properties)
                        {
                            if (prop.Name.Equals("RoadGraph", StringComparison.InvariantCultureIgnoreCase))
                            {

                                var roadGraph = prop.Value as RoadGraph;

                                if (roadGraph != null)
                                {
                                    var zoi = new ZoneOfInfluence(roadGraph.ConvertToGeometry());
                                    newElement.SetInitialPositionAndZOI(null, zoi);
                                    //                                    var lineSegments = roadGraph.Graph.Edges;
                                    //                                    var geomFactory = new GeometryFactory();
                                    //                                    var lsList = new List<LineString>();
                                    //                                    foreach (var rs in lineSegments)
                                    //                                    {
                                    //                                        var coords = new List<Coordinate>();
                                    //                                        coords.Add(new Coordinate(rs.Source.Vertex.Longitude, rs.Source.Vertex.Latitude));
                                    //                                        coords.Add(new Coordinate(rs.Target.Vertex.Longitude, rs.Target.Vertex.Latitude));
                                    //                                        lsList.Add((LineString)geomFactory.CreateLineString(coords.ToArray()));
                                    //                                    }
                                    //                                    var zoi = new ZoneOfInfluence(new DepictionGeometry(geomFactory.CreateMultiLineString(lsList.ToArray())));
                                    //                                    newElement.SetInitialPositionAndZOI(null, zoi);
                                }
                            }
                        }
                    }
                }
                else
                {
                    var oldZOIString = oldZOI.ToString();
                    var geom = GeometryToOgrConverter.WktToZoiGeometry(oldZOIString);
                    if (newElement.Position.IsValid)
                    {
                        newElement.SetZOIGeometry(geom);
                    }
                    else
                    {
                        newElement.SetInitialPositionAndZOI(null, new ZoneOfInfluence(geom));
                    }
                }
            }

            var oldImageMetadata = element12.ImageMetaData;
            if (oldImageMetadata != null && !string.IsNullOrEmpty(oldImageMetadata.ImageFilename))
            {
                newElement.ImageMetadata.BottomRightLatitude = oldImageMetadata.BottomRightLatitude;
                newElement.ImageMetadata.BottomRightLongitude = oldImageMetadata.BottomRightLongitude;
                newElement.ImageMetadata.TopLeftLatitude = oldImageMetadata.TopLeftLatitude;
                newElement.ImageMetadata.TopLeftLongitude = oldImageMetadata.TopLeftLongitude;
                newElement.ImageMetadata.RotationDegrees = oldImageMetadata.Rotation;
                newElement.ImageMetadata.ImageFilename = Path.GetFileName(oldImageMetadata.ImageFilename);
            }

            //For now the only thing that i now has children/waypoints is routes (aloha does too but thats not used)
            //So basically this is kind of a one off hack
            if (element12.Children.Length > 1)
            {
                var children = element12.Children;
                if (newElement.Waypoints.Length == 2)
                {
                    newElement.Waypoints[0].Location = children[0].Position;
                    newElement.Waypoints[1].Location = children[children.Length - 1].Position;
                }

                for (int i = 1; i < children.Length - 1; i++)
                {
                    var waypoint = new DepictionElementWaypoint
                    {
                        Location = children[i].Position,
                        IconSize = 16,
                        Name = "WayPoint"
                    };
                    newElement.InsertWaypointBeforeLast(waypoint);
                }
            }
            newElement.UsePermaText = element12.PermaHoverTextOn;
            newElement.PermaText.PermaTextX = element12.PermaTextX;
            newElement.PermaText.PermaTextY = element12.PermaTextY;
            newElement.UsePropertyNameInHoverText = true;
            return newElement;
        }

        public void Transfer122ElementPropertiesValuesTo13Element(IDepictionElementBase element13, IElementInternal12 element12)
        {
            var failedProps = new List<IElementProperty12>();
            //Get the position first
            var position = element12.Position;
            element13.SetInitialPositionAndZOI(position, null);

            //Doesn't matter if it is shallow copy because the IElementInternal never gets used again and will hopefully just disappear.

            foreach (var prop in element12.Properties)
            {
                var prop13 = element13.GetPropertyByInternalName(prop.Name);
                Type propertyTypeFor13 = null;
                var simplePropName = prop.PropertyType.Name;
                var stringValue = prop.StringValue;

                //Kind of a hack, be we need to convert 1.2 property types to 1.3 property types (they are getting passed as string)
                if (stringValue != null)
                {
                    if (prop13 != null && prop13.ValueType.Equals(typeof(DepictionIconPath)))
                    {
                        simplePropName = "DepictionIconPath";
                        if (stringValue.StartsWith("file"))
                        {
                            var name = Path.GetFileName(stringValue);//I can't actually believe that works
                            stringValue = "file:" + name;
                        }
                    }
                    propertyTypeFor13 = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(simplePropName);
                    if (propertyTypeFor13 == null)
                    {
                        //try older types
                        propertyTypeFor13 = DepictionCoreTypes.FindType(simplePropName);
                        if (propertyTypeFor13 == null)
                        {//Man this is bad if it gets here
                            propertyTypeFor13 = DepictionTypeConverter.Get12ObjectTypeFromStringValue(simplePropName);
                        }
                    }
                }

                //If the property is in the 1.3 version of the element
                if (prop13 != null)
                {
//                    //In the case the the element had to be created because it was a user element update
//
//                    if (prop.Name.Equals("StrokeColor", StringComparison.InvariantCultureIgnoreCase))
//                    {
//                        var strokeColor = prop.Value;
//                        var fillProp = element13.SetPropertyValue("ZOIBorder", strokeColor);
//
//                    }
//                    else if (prop.Name.Equals("FillColor", StringComparison.InvariantCultureIgnoreCase))
//                    {
//                        var oldVal = prop.Value;
//                        var fillProp = element13.SetPropertyValue("ZOIFill", oldVal);
//
//                    }
//                    else if (prop.Name.Equals("BorderColor", StringComparison.InvariantCultureIgnoreCase))
//                    {
//                        var oldVal = prop.Value;
//                        var fillProp = element13.SetPropertyValue("IconBorderColor", oldVal);
//                        Color color;
//                        var success = element13.GetPropertyValue("IconBorderColor", out color);
//                        if (color.A != 0 && success) { fillProp = element13.SetPropertyValue("IconBorderShape", DepictionIconBorderShape.Square); }
//                    }
//                    //end


                    prop13.IsHoverText = prop.IsHoverText;
                    //Third attempt as a way to make icons editable, it did not work
                    prop13.VisibleToUser = prop.Visible;
                    prop13.Editable = prop.Editable;
                    if (prop13.ValueType.Equals(prop.PropertyType))
                    {
                        element13.SetPropertyValue(prop.Name, prop.Value, false);
                    }
                    else
                    {
                        if (propertyTypeFor13 != null)
                        {
                            var newPropValue = DepictionTypeConverter.ChangeType(stringValue, propertyTypeFor13);
                            element13.SetPropertyValue(prop.Name, newPropValue, false);
                        }
                    }
                    //Hack to fix a bug in 1.2 that set iconpath/size to not visible and zoi editability
                    //Sometimes this is not needed, but most of the time it is.

                    if (prop13.InternalName.Equals("IconPath", StringComparison.InvariantCultureIgnoreCase) ||
                        prop13.InternalName.Equals("IconSize", StringComparison.InvariantCultureIgnoreCase) ||
                        prop13.InternalName.Equals("CanEditZoi",StringComparison.InvariantCultureIgnoreCase))
                    {
                        prop13.Editable = true;
                        prop13.VisibleToUser = true;
                        prop13.Deletable = false;
                    }
                }
                else//The property does not exist so we have to create it, or update the appropriate 1.3 propety type
                {
                    //try to copy the prop
                    if (prop.Name.Equals("StrokeColor", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var strokeColor = prop.Value;
                        var fillProp = element13.SetPropertyValue("ZOIBorder", strokeColor);

                    }
                    else if (prop.Name.Equals("FillColor",StringComparison.InvariantCultureIgnoreCase))
                    {
                        var oldVal = prop.Value;
                        var fillProp = element13.SetPropertyValue("ZOIFill", oldVal);

                    }
                    else if (prop.Name.Equals("BorderColor", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var oldVal = prop.Value;
                        var fillProp = element13.SetPropertyValue("IconBorderColor", oldVal);
                        Color color;
                        var success = element13.GetPropertyValue("IconBorderColor", out color);
                        if (color.A != 0 && success) { fillProp = element13.SetPropertyValue("IconBorderShape", DepictionIconBorderShape.Square); }
                    }
                    else
                    {
                        object convertedPropValue = prop.Value;

                        if (prop.Name.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                        {
                            convertedPropValue = stringValue;
                        }
                        else if (propertyTypeFor13 != null)
                        {
                            convertedPropValue = DepictionTypeConverter.ChangeType(stringValue, propertyTypeFor13);
                        }
                        var newProp = new DepictionElementProperty(prop.Name, prop.DisplayName, convertedPropValue);
                        newProp.IsHoverText = prop.IsHoverText;
                        //Tried this first as a way to make icons editable, it did not work
                        newProp.VisibleToUser = prop.Visible;
                        newProp.Editable = prop.Editable;
                        if (element13.AddPropertyOrReplaceValueAndAttributes(newProp) == false)
                        {
                            failedProps.Add(prop);
                        }
                    }
                }
            }
            if (failedProps.Count != 0)
            {
                Debug.WriteLine("Some properties failed to change defautl values");
            }
        }


        private IDepictionStoryMetadata TransferStoryMetadata(IDepictionMetadata12 oldMetadata)
        {
            var newMetadata = new DepictionStoryMetadata();
            newMetadata.Author = oldMetadata.Author;
            newMetadata.Description = oldMetadata.Description;
            newMetadata.SaveDateTime = oldMetadata.SaveDateTime;
            newMetadata.Title = oldMetadata.Title;
            newMetadata.Tags = oldMetadata.Tags;

            return newMetadata;
        }

        private IMapCoordinateBounds TrasformOldRegionExtentToNew(IBoundingBox12 oldBounds)
        {
            var left = oldBounds.Left;
            var right = oldBounds.Right;
            var top = oldBounds.Top;
            var bottom = oldBounds.Bottom;
            return new MapCoordinateBounds(new LatitudeLongitude(top, left), new LatitudeLongitude(bottom, right));
        }
    }
}