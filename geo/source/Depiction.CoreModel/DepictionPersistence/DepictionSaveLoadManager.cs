﻿using System;
using System.ComponentModel;
using System.IO;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DpnPorting;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.Serialization;
using Ionic.Zip;

namespace Depiction.CoreModel.DepictionPersistence
{
    public class DepictionSaveLoadManager
    {
        public event ProgressChangedEventHandler SaveLoadProgressChanged;
        public event RunWorkerCompletedEventHandler DepictionLoadCompleted;
        public event RunWorkerCompletedEventHandler DepictionSaveCompleted;

        BackgroundWorker saveThread = new BackgroundWorker();
        BackgroundWorker loadThread = new BackgroundWorker();
        private TempFolderService folderService;
        private string currentSaveFileName = string.Empty;//Not sure when this gets used

        #region Constructor
        public DepictionSaveLoadManager()
        {
            saveThread.WorkerReportsProgress = true;
            saveThread.WorkerSupportsCancellation = true;
            saveThread.DoWork += saveThread_DoWork;
            saveThread.ProgressChanged += backgroundThread_ProgressChanged;
            saveThread.RunWorkerCompleted += saveThread_RunWorkerCompleted;

            loadThread.WorkerReportsProgress = true;
            loadThread.WorkerSupportsCancellation = true;
            loadThread.DoWork += loadThread_DoWork;
            loadThread.ProgressChanged += backgroundThread_ProgressChanged;
            loadThread.RunWorkerCompleted += loadThread_RunWorkerCompleted;
        }
        #endregion

        #region Public methods

        public void SaveDepiction(string fileName, IDepictionStory depiction)
        {
            if (SerializationService.SetSaveLoadBackgroundThread(saveThread) == false)
            {
                Console.WriteLine("Could not set serialization thread");
            }
            if (saveThread.IsBusy) return;
            var dir = Path.GetDirectoryName(fileName);

            if(dir == null)
            {
                var message = string.Format("Could not find folder for : {0}", fileName);
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                return;
            }


            if (!Directory.Exists(dir))
            {
                if(string.IsNullOrEmpty(dir))
                {
                    var message = string.Format("Could not find folder for : {0}", fileName);
                    DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                    return;
                }
                try
                {

                    Directory.CreateDirectory(dir);
                }catch(Exception ex)
                {
                    var message = string.Format("Could not create file : {0}. Please ensure that the drive letter exists.", fileName);
                    DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                    return; 
                }
            }
            currentSaveFileName = fileName;
            saveThread.RunWorkerAsync(depiction);
        }

        public void LoadDepiction(string fileName)
        {
            if (SerializationService.SetSaveLoadBackgroundThread(loadThread) == false)
            {
                Console.WriteLine("Could not set serialization thread");
            }
            //            if (File.Exists(fileName)) return;//The async has to happen
            loadThread.RunWorkerAsync(fileName);
        }

        #endregion

        #region Progress region
        private void backgroundThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (SaveLoadProgressChanged != null)
            {
                SaveLoadProgressChanged.Invoke(sender, e);
            }
        }
        #endregion
        #region Save region
        private void saveThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var fileFormat = new DepictionFileFormatInfo();
            var depiction = e.Argument as DepictionStory;
            if (depiction == null || string.IsNullOrEmpty(currentSaveFileName))
            {
                e.Result = null;
                return;
            }
            folderService = new TempFolderService(false);
            SerializationService.ThreadConnection = e;
            var tempDir = folderService.FolderName;
            string fileFormatName = Path.Combine(tempDir, "DepictionFormat.xml");
            SerializationService.SaveToXmlFile(fileFormat, fileFormatName, "DepictionFormat");

            DepictionStory.SerializeADepiction(tempDir, depiction, fileFormat.DepictionSubtrees);
            if (SerializationService.UpdateProgressReport(-1, string.Format("Compressing depiction information")) == true)
            {
                DepictionPersistenceHelper.PutDirectoryIntoZipFile(currentSaveFileName, tempDir);
                depiction.DepictionNeedsSaving = false;
                depiction.DepictionsFileName = currentSaveFileName;
                e.Result = folderService;
            }
            folderService.Close();
        }
        private void saveThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (DepictionSaveCompleted != null)
            {
                DepictionSaveCompleted.Invoke(sender, e);
            }
        }


        #endregion

        #region Load region

        private void loadThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var fullFileName = e.Argument as string;

            if (string.IsNullOrEmpty(fullFileName))
            {
                e.Result = null;
                return;
            }
            if (!File.Exists(fullFileName))
            {
                e.Result = null;
                return;
            }
            var temp = new TempFolderService(false);
            SerializationService.ThreadConnection = e;
            var tempDir = temp.FolderName;
            DepictionFileFormatInfo fileFormat = null;
            #region new zipfile stuff
            using (ZipFile zipFile = ZipFile.Read(fullFileName))
            {

                var formatZipEntry = zipFile["DepictionFormat.xml"];
                if (formatZipEntry != null)
                {
                    fileFormat = SerializationService.LoadFromStream<DepictionFileFormatInfo>(formatZipEntry.OpenReader(), "DepictionFormat");
                    if (fileFormat == null)
                    {
                        e.Result = null;
                        temp.Close();
                        return;
                    }
                    if (fileFormat.DepictionFileVersion > DepictionFileFormatInfo.MostModernDepictionVersionFileNumber)
                    {
                        var fileName = Path.GetFileName(fullFileName);
                        var message = string.Format(
                            "Depiction file format for {0} is not compatable with current Depiction", fileName);
                        DepictionAccess.NotificationService.DisplayMessageString(message, 5);
                        e.Result = null;
                        temp.Close();
                        return;
                    }
                }
            }
            #endregion
            try
            {
                DepictionPersistenceHelper.UnzipFileToDirectory(fullFileName, tempDir);
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Not a proper Depiction file.", 4);
                temp.Close();
                return;
            }
            string fileFormatName = Path.Combine(tempDir, "DepictionFormat.xml");
            if (File.Exists(fileFormatName))
            {
                var newDepiction = DepictionStory.DeserializeADepiction(tempDir, fileFormat.DepictionSubtrees);
                e.Result = newDepiction;
                newDepiction.DepictionsFileName = fullFileName;
                newDepiction.DepictionNeedsSaving = false;
            }
            else
            {
                //so it was not a 1.3 file

                var file122Format = Path.Combine(tempDir, "storyMemento.xml");
                if (File.Exists(file122Format))
                {
                    DepictionAccess.NotificationService.DisplayMessageString("Attempting to convert from a 1.2 dpn.", 4);

                    e.Result = Convert122To13Depiction(tempDir, fullFileName);
                }
            }

            temp.Close();
        }

        private void loadThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            if (DepictionLoadCompleted != null)
            {
                DepictionLoadCompleted.Invoke(sender, e);
            }
        }

        #endregion

        static public IDepictionStory Convert122To13Depiction(string tempDir, string fileName)
        {
            var oldProtoLibrary = new ElementPrototypeLibrary();
            try
            {
                oldProtoLibrary.SetLoadedPrototypesFromPath(Path.Combine(tempDir, "ElementTypes"),false);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something will go wrong on user created interactions and types");
                Console.WriteLine(ex.Message);
            }
            var oldPersistanceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(tempDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            oldPersistanceMemento.InherentPrototypes = oldProtoLibrary;
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistanceMemento, fileName);
            return newDepiction;

        }

    }
}
