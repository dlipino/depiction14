﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionPersistence
{
    //Man, i really really want this to work
    //    public class DepictionSubtreeInfo<T>:DepictionSubtreeInfoBase
    //    {
    //        public DepictionSubtreeInfo() : base(typeof(T)){}
    //        public DepictionSubtreeInfo(string parentPath, string dataPath, string xmlFileName, string serializationName):
    //            base( parentPath,  dataPath,  xmlFileName,  serializationName, typeof(T)){}
    //
    //    }
    public class DepictionSubtreeInfoBase : IXmlSerializable
    {
        #region Variables

        protected string parentPath = string.Empty;
        protected string dataPath = string.Empty;
        protected string xmlFileName = string.Empty;
        protected string serializationName = string.Empty;
        protected Type saveType = typeof(int);//Sadly we need this, because of the default constructor
        protected string userReadableName = string.Empty;
        #endregion

        #region ElementProperties
        public string SerializationName
        {
            get { return serializationName; }
        }

        public string XmlFileName
        {
            get { return xmlFileName; }
        }

        public string DataPath
        {
            get { return dataPath; }
        }

        public string ParentPath
        {
            get { return parentPath; }
        }
        public string UserReadableName
        {
            get
            {
                if (string.IsNullOrEmpty(userReadableName))
                {
                    return SerializationName;
                }
                return userReadableName;
            }
            set { userReadableName = value; }
        }
        #endregion

        #region Constructor


        public DepictionSubtreeInfoBase()
        {
            UserReadableName = string.Empty;
        }

        public DepictionSubtreeInfoBase(string parentPath, string dataPath, string xmlFileName, string serializationName, Type saveType)
            : this()
        {
            this.parentPath = parentPath;
            this.dataPath = dataPath;
            this.xmlFileName = xmlFileName;
            this.serializationName = serializationName;
            this.saveType = saveType;
        }

        #endregion

        #region Methods

        #region Saving things

        public void SaveSubtree(DepictionStory depiction, string parentDir)
        {
            string fullDir = Path.Combine(ParentPath, DataPath);
            string depictionDataFolder = Path.Combine(parentDir, fullDir);
            if (!Directory.Exists(depictionDataFolder)) Directory.CreateDirectory(depictionDataFolder);
            string depictionSaveFileName = Path.Combine(depictionDataFolder, xmlFileName);
            TypeToSaveInstructions(saveType, depiction, depictionSaveFileName);
        }
        //Don't know how this works anymore
        protected void TypeToSaveInstructions(Type depictionTypeToSave, DepictionStory depiction, string fileName)
        {
            //I don't like having to case all these things 
            if (depictionTypeToSave.Equals(typeof(DepictionStoryMetadata)))
            {
                SerializationService.SaveToXmlFile(depiction.Metadata as DepictionStoryMetadata, fileName, serializationName);
            }
            else if (depictionTypeToSave.Equals(typeof(DepictionGeographicInfo)))
            {
                SerializationService.SaveToXmlFile(depiction.DepictionGeographyInfo as DepictionGeographicInfo, fileName, serializationName);
            }
            else if (depictionTypeToSave.Equals(typeof(RasterImageResourceDictionary)))
            {
                //                depiction.ImageResources
                var onlyUsedImages = RasterImageResourceDictionary.RemoveUnusedImagesFromResources();
                var defaultElementIcons = RasterImageResourceDictionary.GetIconsForUsedElements();
                foreach (var key in onlyUsedImages.Keys)
                {
                    if (!defaultElementIcons.Contains(key))
                    {
                        defaultElementIcons.Add(key, onlyUsedImages[key]);
                    }
                }
                SerializationService.SaveToXmlFile(defaultElementIcons, fileName, serializationName);
            }
            else if (depictionTypeToSave.Equals(typeof(ElementRepository)))
            {
                ElementRepository.SerializeElementRepositoryIntoParts(fileName, depiction.CompleteElementRepository);
            }
            else if (depictionTypeToSave.Equals(typeof(List<IDepictionAnnotation>)))
            {
                SerializationService.SerializeItemListOneFile<IDepictionAnnotation, DepictionAnnotation>(fileName, depiction.depictionAnnotations, serializationName);
            }
            else if (depictionTypeToSave.Equals(typeof(List<DepictionRevealer>)))
            {
                SerializationService.SerializeItemListOneFile<IDepictionRevealer, DepictionRevealer>(fileName, depiction.depictionRevealers, serializationName);
            }
            else if (depictionTypeToSave.Equals(typeof(DepictionElementBackdrop)))
            {
                SerializationService.SaveToXmlFile(depiction.depictionBackdrop, fileName, serializationName);
            }
            else if (depictionTypeToSave.Equals(typeof(ElementPrototypeLibrary)))
            {//This save is kind of special beceause the element library is a static thing

                var library = DepictionAccess.ElementLibrary;
                if (library != null) library.SaveElementPrototypeLibraryToPath(fileName,true);
            }
            else if (depictionTypeToSave.Equals(typeof(InteractionRuleRepository)))
            {//This save is kind of special beceause the element library is a static thing

                var interactionLibrary = depiction.InteractionRuleRepository;//DepictionAccess.CurrentDepiction.InteractionRuleRepository;
                SerializationService.SaveToXmlFile(interactionLibrary, fileName, serializationName);
            }
        }

        #endregion

        #region Load method and helper

        public void LoadSubtree(DepictionStory depiction, string parentDir)
        {
            string fullDir = Path.Combine(ParentPath, DataPath);
            string depictionDataFolder = Path.Combine(parentDir, fullDir);
            if (!Directory.Exists(depictionDataFolder))
            {
                var message = string.Format("Missing depiction file data. Could not find data path: {0}. Loaded file may be incomplete.", DataPath);
                DepictionAccess.NotificationService.DisplayMessageString(message, 6);
                return;
                //throw new DepictionDeserializationException(message);
            }
            string depictionLoadFileName = Path.Combine(depictionDataFolder, xmlFileName);
            if (saveType == null)
            {
                throw new ArgumentNullException(string.Format("{0} load type is null. File load terminated", serializationName));
            }
            TypeToLoadInstructions(saveType, depiction, depictionLoadFileName);
        }

        protected void TypeToLoadInstructions(Type depictionTypeToLoad, DepictionStory depiction, string fileName)
        {
            if (depictionTypeToLoad.Equals(typeof(DepictionGeographicInfo)))
            {
                depiction.DepictionGeographyInfo = SerializationService.LoadFromXmlFile<DepictionGeographicInfo>(fileName, serializationName);
            }
            else if (depictionTypeToLoad.Equals(typeof(DepictionStoryMetadata)))
            {
                depiction.Metadata = SerializationService.LoadFromXmlFile<DepictionStoryMetadata>(fileName, serializationName);
            }
            else if (depictionTypeToLoad.Equals(typeof(RasterImageResourceDictionary)))
            {
                depiction.ImageResources = SerializationService.LoadFromXmlFile<RasterImageResourceDictionary>(fileName, serializationName);
            }
            else if (depictionTypeToLoad.Equals(typeof(ElementRepository)))
            {
                depiction.CompleteElementRepository = ElementRepository.DeserializeElementRepositoryFromParts(fileName);
            }
            else if (depictionTypeToLoad.Equals(typeof(List<IDepictionAnnotation>)))
            {
                var elementList = SerializationService.DeserializeItemListFromOneFile<IDepictionAnnotation, DepictionAnnotation>(fileName, serializationName);
                depiction.depictionAnnotations = elementList as List<IDepictionAnnotation>;
            }
            else if (depictionTypeToLoad.Equals(typeof(List<DepictionRevealer>)))
            {
                var elementList = SerializationService.DeserializeItemListFromOneFile<IDepictionRevealer, DepictionRevealer>(fileName, serializationName);

                depiction.depictionRevealers = elementList as List<IDepictionRevealer>;
            }
            else if (depictionTypeToLoad.Equals(typeof(DepictionElementBackdrop)))
            {
                depiction.depictionBackdrop = SerializationService.LoadFromXmlFile<DepictionElementBackdrop>(fileName, serializationName);
            }
            else if (depictionTypeToLoad.Equals(typeof(ElementPrototypeLibrary)))
            {
                var library = DepictionAccess.ElementLibrary;
                if (library != null)
                {
                    library.UpdateElementPrototypeLibraryFromTempLoadedDpnLocation(fileName);
                }
            }
            else if (depictionTypeToLoad.Equals(typeof(InteractionRuleRepository)))
            {//TODO
                var loadedRepo = SerializationService.LoadFromXmlFile<InteractionRuleRepository>(fileName, serializationName);
                if (loadedRepo != null)
                {
                    //This makes sure the active rules match the loaded dpn's rules
                    foreach (var previousRule in depiction.InteractionRuleRepository.InteractionRules)
                    {
                        previousRule.IsDisabled = !loadedRepo.InteractionRules.Contains(previousRule);
                        //Hack for prep stuff
                        switch (DepictionAccess.ProductInformation.ProductType)
                        {
                            case ProductInformationBase.Prep:
                                if (previousRule.Name.Contains("Prep:"))
                                {
                                    previousRule.IsDisabled = false;
                                }
                                break;
                        }
                    }

                    foreach (var rule in loadedRepo.InteractionRules)
                    {
                        if (!depiction.InteractionRuleRepository.InteractionRules.Contains(rule))
                        {
                            depiction.InteractionRuleRepository.AddInteractionRule(rule);
                        }
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var subtree = (DepictionSubtreeInfoBase)obj;

            // use this pattern to compare reference members
            if (!Equals(SerializationName, subtree.SerializationName)) return false;
            if (!Equals(saveType, subtree.saveType)) return false;
            if (!XmlFileName.Equals(subtree.XmlFileName)) return false;
            if (!DataPath.Equals(subtree.DataPath)) return false;
            if (!ParentPath.Equals(subtree.ParentPath)) return false;

            return true;
        }
        #endregion
        #region Serialization part
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            parentPath = reader.ReadElementContentAsString("parentPath", ns);
            dataPath = reader.ReadElementContentAsString("dataPath", ns);
            xmlFileName = reader.ReadElementContentAsString("xmlFileName", ns);
            serializationName = reader.ReadElementContentAsString("serializationName", ns);
            if (reader.Name.Equals("loaderTypeName"))
            {

                var typeName = reader.ReadElementContentAsString("loaderTypeName", ns);
                saveType = Type.GetType(typeName);
                //Hack for a conversion between 1.3 and 1.4 do to a class name change (APINew to API)
                //Proof of concept hopefully can make something more legit later.
                //WAAAAYY Hacky, doesnt take into account version number or anything, what a mess
                if(saveType == null && typeName.Contains("APINew"))
                {
                    var typeNameChanged = typeName.Replace("APINew", "API");
                    saveType = Type.GetType(typeNameChanged);
                    if(saveType == null)
                    {
                        throw new ArgumentNullException(string.Format("{0} load type is null. File load terminated", serializationName));
                       // throw new Exception("Could not find save type for Depiction subtree loader");
                    }
                }
            }
            if (reader.Name.Equals("UserReadableName"))
            {
                userReadableName = reader.ReadElementContentAsString("UserReadableName", ns);

            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("DepictionSubtreeInfo");
            writer.WriteElementString("parentPath", ns, parentPath);
            writer.WriteElementString("dataPath", ns, dataPath);
            writer.WriteElementString("xmlFileName", ns, xmlFileName);
            writer.WriteElementString("serializationName", ns, serializationName);
            writer.WriteElementString("loaderTypeName", ns, saveType.AssemblyQualifiedName);
            writer.WriteElementString("UserReadableName", ns, UserReadableName);
            writer.WriteEndElement();

        }

        #endregion
    }
}