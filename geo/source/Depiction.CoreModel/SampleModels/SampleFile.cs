using System;
using System.IO;
using Depiction.CoreModel.DepictionObjects;
using Depiction.Serialization;
using Ionic.Zip;

namespace Depiction.CoreModel.SampleModels
{
    public class SampleFile
    {

        public string FriendlyName { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }

        public SampleFile(string fullFileName)
        {
            if (!File.Exists(fullFileName)) return;
            FilePath = fullFileName;
            FriendlyName = Path.GetFileNameWithoutExtension(fullFileName);

            using (ZipFile zipFile = ZipFile.Read(fullFileName))
            {

                var formatZipEntry = zipFile["DepictionMetadata.xml"];
                var metadata = SerializationService.LoadFromStream<DepictionStoryMetadata>(formatZipEntry.OpenReader(), "DepictionInformation");
                Description = metadata.Description;
            }
        }
        public SampleFile(string fileName, string friendlyName, string description)
        {
            FriendlyName = friendlyName;
            Description = description;

            string path = AppDomain.CurrentDomain.BaseDirectory;
#if DEBUG
            path = Path.Combine(path, @"..\..\..\..\..\samples");
#else
            path = Path.Combine(path, "samples");
#endif
            // todo: use the _productInformation extension when merge to Private Label.
            FilePath = string.Format("{0}{1}", Path.Combine(path, fileName), ".dpn");
        }

    }
}