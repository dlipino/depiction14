using System.Collections.Generic;
using System.IO;

namespace Depiction.CoreModel.SampleModels
{
    public class SampleDepictions
    {
        public List<SampleFile> FindSampleDepictions(string sampleFileDir,int maxSamples)
        {
            var availableSamples = new List<SampleFile>();
            if (!Directory.Exists(sampleFileDir)) return availableSamples;
            var allSampleFileNames = Directory.GetFiles(sampleFileDir, "*.dpn");

            if(maxSamples >0)
            {
                var count = 0;
                var samplesFromCount = new List<string>();
                foreach(var sample in allSampleFileNames)
                {
                    samplesFromCount.Add(sample);
                    count++;
                    if (count > maxSamples) break;
                }
                allSampleFileNames = samplesFromCount.ToArray();
            }

            foreach (var allSampleFileName in allSampleFileNames)
            {
                if (File.Exists(allSampleFileName))
                    availableSamples.Add(new SampleFile(allSampleFileName));
            }

//            var samplesToShow = new List<SampleFile> 
//                                    { 
//                                        new SampleFile("Sample_PhiladelphiaPlume", "Philadelphia Plume Scenario", "Stephen Flynn's The Edge of Disaster has been a significant influence on Depiction's development. This sample depicts a scenario outlined in the book's introduction: a terrorist attack in Philadelphia has unleashed a poison cloud, moving across the city..."),
//                                        new SampleFile("sample_nursinghomeevacuation", "Nursing home evacuation", "Depicts how an earthquake might precipitate a nursing home evacuation in Seattle, WA. Try different routes and blocking routes with Road barriers."),
//                                        new SampleFile("sample_tsunamiscenario", "Tsunami in India", "Depicts a 15-foot tsunami inundation for Nagapatnam, India. Try different tsunami heights."),
//                                        new SampleFile("sample_usingrevealers", "Using revealers", "See different ways to use revealers in a depiction."),
//                                        new SampleFile("sample_kandahar", "Route finding in Afghanistan", "Explore different road and helicopter routes that avoid possible problems between Kandahar, Afghanistan and its airport."),
//                                    };

//
//            foreach (var sample in samplesToShow)
//                if (File.Exists(sample.FilePath))
//                    availableSamples.Add(sample);
            return availableSamples;
        }
    }
}