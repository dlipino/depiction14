﻿using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.CoreModel.TypeConverter;
using Depiction.Serialization;

namespace Depiction.CoreModel.OldValidationRules
{
    /// <summary>
    /// Validates whether variable is greater than the Minimum value.
    /// </summary>
    public class MinValueValidationRule : IValidationRule
    {
        #region fields

        public object min;
        

        #endregion

        #region constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MinValueValidationRule"/> class.
        /// </summary>
        /// <param name="minVal">The min val.</param>
        public MinValueValidationRule(object minVal)
        {
            min = minVal;
        }

        ///<summary>
        /// Create a new instance of this class.
        ///</summary>
        public MinValueValidationRule()
        {}

        #endregion

        #region properties
        public bool IsRuleValid { get { return (min != null); } }
        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public ValidationRuleParameter[] Parameters
        {
            get { return new[] {new ValidationRuleParameter(min.ToString(), min.GetType())}; }
        }

        #endregion

        #region methods

        /// <summary>
        /// Validates the specified value to validate.
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        public DepictionValidationResult Validate(object valueToValidate)
        {
            var result = new DepictionValidationResult { ValidationOutput = ValidationResultValues.InValid, Message = null };

            double minVal;
            double valVal;
            if (min is IMeasurement || valueToValidate is IMeasurement)
            {
                if (!(min is IMeasurement))
                {
                    result.Message = string.Format(
                        "The minimum value {0} cannot be validated because its units have not been specified.", min);
                    return result;
                }
                if (!(valueToValidate is IMeasurement))
                {
                    result.Message = string.Format(
                        "The value {0} cannot be validated because its units have not been specified.", valueToValidate);
                    return result;
                }

                var useSystem = ((IMeasurement)min).InitialSystem;
                var useScale = ((IMeasurement)min).InitialScale;
                minVal = ((IMeasurement)min).GetValue(useSystem,useScale);
                valVal = ((IMeasurement)valueToValidate).GetValue(useSystem, useScale);
            }
            else
            {
                try
                {
                    minVal = Convert.ToDouble(min);
                }
                catch
                {
                    result.Message = string.Format("Unable to convert minimum value {0} to numeric for validation.", min);
                    return result;
                }
                
                try
                {
                    valVal = Convert.ToDouble(valueToValidate);
                }
                catch
                {
                    result.Message = string.Format("Unable to convert value {0} to numeric for validation.", valueToValidate);
                    return result;
                }
                
            }
            if(valVal >= minVal)
            {
                result.ValidationOutput = ValidationResultValues.Valid; 
            }


            if (result.ValidationOutput.Equals(ValidationResultValues.InValid))
            {
                result.Message = string.Format("Value must be at least {0}", min);
            }

            return result;
        }

        #endregion

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            var minTypeString = reader.ReadElementContentAsString("minType", ns);
            var minValue = reader.ReadElementContentAsString("minValue", ns);

            var minType = Type.GetType(minTypeString);
            min = DepictionTypeConverter.ChangeType(minValue, minType);
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteElementString("minType", ns, min.GetType().FullName);
            writer.WriteElementString("minValue", ns, min.ToString());
        }
    }
}