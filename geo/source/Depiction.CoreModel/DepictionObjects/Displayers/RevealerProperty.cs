﻿using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Displayers
{
    public class RevealerProperty : IRevealerProperty
    {
        //public string InternalName { get; set; }
        public string PropertyName { get; set; }
//        public object Value { get; private set; }
        public object Value { get;  set; }
        public Type PropertyType { get { return Value.GetType(); } }


        #region Constructor

        public RevealerProperty()
        {
            PropertyName = "Default";
            Value = 0;
//            Value = default(T);
        }

        #endregion

        #region Methods

        public bool SetValue(object value,bool changeType)
        {
            var type = value.GetType();
            if(!changeType)
            {
                if (!Value.GetType().Equals(type)) return false;
            }
            Value = value;
//            if (!typeof(T).Equals(value.GetType())) return false;
//            //TODO Type conversion later
//            Value = (T)value;
            
            return true;
        }
        #endregion

        #region Equals override

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (RevealerProperty)obj;
            if (!Equals(PropertyName, other.PropertyName)) return false;
            if (!Equals(PropertyType, other.PropertyType)) return false;
            if (!Equals(Value, other.Value)) return false;

            return true;
        }

        #endregion

        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (!reader.Name.Equals("property")) return;
            PropertyName = reader.GetAttribute("name");
            var valueString = reader.GetAttribute("value");
            var valueType = reader.GetAttribute("valueType");
            var type = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(valueType);
            
            if(type.IsEnum)
            {
                SetValue(Enum.Parse(type, valueString),true); 
            }else
            {
                SetValue(Convert.ChangeType(valueString, type), true);
            }
            
            reader.Read();//There is no end element, so we move on becasue we know we are at the end of this property
        }

        public void WriteXml(XmlWriter writer)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
//            var ns = SerializationConstants.DepictionXmlNameSpace;
//            writer.WriteElementString("PropertyName", ns, PropertyName);
//            SerializationService.SerializeObject("Value", Value, writer);
            writer.WriteStartElement("property");
            writer.WriteAttributeString("name", PropertyName);
            writer.WriteAttributeString("value", Value.ToString());
            var typeName = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(Value.GetType());
           
            writer.WriteAttributeString("valueType", typeName);
            writer.WriteEndElement();
        }

        #endregion
    }
}