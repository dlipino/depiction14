﻿using System.Xml;
using Depiction.API.CoreEnumAndStructs;
using Depiction.CoreModel.AbstractDepictionObjects;

namespace Depiction.CoreModel.DepictionObjects.Displayers
{
    public class DepictionElementBackdrop : DepictionDisplayerBase
    {
        #region Constructor

        public DepictionElementBackdrop()
        {
            DisplayerName = "Backdrop";
            DisplayerType = DepictionDisplayerType.MainMap;
        }

        #endregion

        #region Overrides of DepictionDisplayerBase

        public override void DisplayerReader(XmlReader reader)
        {//Does nothing
        }

        public override void DisplayerWrite(XmlWriter writer)
        {//does nothing
        }

        #endregion
    }
}