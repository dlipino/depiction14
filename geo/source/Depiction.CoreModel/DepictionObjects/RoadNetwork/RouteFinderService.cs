﻿using System.Collections.Generic;
using System.Linq;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using QuickGraph.Algorithms;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Algorithms.ShortestPath;

namespace Depiction.CoreModel.DepictionObjects.RoadNetwork
{
    public class RouteFinderService : IRouteFinder
    {
        private readonly RoadGraph roadGraph;
        private RoadNode start, end;
        public RouteFinderService(RoadGraph roadGraph)
        {
            this.roadGraph = roadGraph;
        }


        public IList<RoadSegment> FindRoute(IEnumerable<RoadNode> waypointList, IDepictionElement route, out bool hasFreeformSegments, out double? estimatedTime)
        {
            IEnumerable<RoadSegment> path = null;
            estimatedTime = null;
            hasFreeformSegments = false;
            start = end = null;
            foreach (var wayPoint in waypointList)
            {
                if (start == null)
                {
                    start = wayPoint;
                    continue;
                }
                end = wayPoint;
                DijkstraShortestPathAlgorithm<RoadNode, RoadSegment> dijkstra = null;
                
                if (route.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork"))
                {
                    dijkstra = new DijkstraShortestPathAlgorithm<RoadNode, RoadSegment>(roadGraph.Graph, roadGraph.GetEdgeWeight);
                }
                else if (route.ElementType.Equals("Depiction.Plugin.DriveTimeRoute"))
                {
                    dijkstra = new DijkstraShortestPathAlgorithm<RoadNode, RoadSegment>(roadGraph.Graph, roadGraph.GetDriveTimeFunc(route));
                }

                if (dijkstra == null)
                    return null;
                
                // Attach a Vertex Predecessor Recorder Observer to give us the paths
                var predecessors = new VertexPredecessorRecorderObserver<RoadNode, RoadSegment>();
                using (predecessors.Attach(dijkstra))
                {
                    dijkstra.Compute(start);
                    predecessors.TryGetPath(end, out path);
                }
                
                if (dijkstra.State.Equals(ComputationState.Finished))
                {
                    double cost = dijkstra.Distances[end];
                    if (route.ElementType.Equals("Depiction.Plugin.DriveTimeRoute"))
                        estimatedTime = cost;
                    if (cost > 1000000)
                    {
                        hasFreeformSegments = true;
                    }
                }

                start = end;
            }
            return path != null ? path.ToList() : null;
        }

    }
}
