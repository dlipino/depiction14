﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Displayers;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.CoreModel.HelperClasses;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects
{
    public class DepictionStory : IDepictionStory
    {

        #region Depiction Events

        public event NotifyCollectionChangedEventHandler RevealerListChange;
        public event NotifyCollectionChangedEventHandler AnnotationListChange;

        //This i like, evenhandling pass through hotdog
        public event NotifyCollectionChangedEventHandler ElementListChange
        {
            add { depictionElements.ElementListChange += value; }
            remove { depictionElements.ElementListChange -= value; }
        }

        public event Action<IEnumerable<string>, DepictionDialogType[]> RequestedElementsInDialogType;
        public event Action<IMapCoordinateBounds, IMapCoordinateBounds> DepictionViewportChange;//<Old,New,notifyviewmodel>
        public event Action<IMapCoordinateBounds, IMapCoordinateBounds> DepictionRegionChange;//<Old,New>
        public event Func<IMapCoordinateBounds, IMapCoordinateBounds> DepictionViewportChangeRequested;

        #endregion

        #region Variables

        internal DepictionElementBackdrop depictionBackdrop = new DepictionElementBackdrop();
        private RasterImageResourceDictionary imageResources = new RasterImageResourceDictionary();
        private IElementRepository depictionElements;
        private IInteractionRuleRepository interactionRuleRepository = new InteractionRuleRepository();
        internal List<IDepictionAnnotation> depictionAnnotations = new List<IDepictionAnnotation>();//Used during load
        internal List<IDepictionRevealer> depictionRevealers = new List<IDepictionRevealer>();
        private IDepictionStoryMetadata metadata;
        private IDepictionGeographicInfo _depictionGeographyInfo;
        #endregion

        #region Properties
        public bool DepictionNeedsSaving { get; set; }
        public string DepictionsFileName { get; set; }

        public IDepictionStoryMetadata Metadata
        {
            get { return metadata; }
            set
            {
                if (metadata != null)
                {
                    metadata.DepictionStoryMetadataChange -= DepictionStoryChange;
                }
                metadata = value;
                metadata.DepictionStoryMetadataChange += DepictionStoryChange;
            }
        }
        public IDepictionGeographicInfo DepictionGeographyInfo
        {
            get { return _depictionGeographyInfo; }
            set
            {
                if (_depictionGeographyInfo != null)
                {
                    _depictionGeographyInfo.DepictionSpecificsChange -= DepictionStoryChange;
                }
                _depictionGeographyInfo = value;
                _depictionGeographyInfo.DepictionSpecificsChange += DepictionStoryChange;
            }
        }
        public IElementRepository CompleteElementRepository
        {
            get { return depictionElements; }
            internal set
            {
                if (depictionElements != null)
                {
                    depictionElements.ElementGeoLocationUpdated -= depictionElements_ElementGeoLocationUpdated;
                }
                depictionElements = value as ElementRepository;
                if (depictionElements != null)
                {
                    depictionElements.ElementGeoLocationUpdated += depictionElements_ElementGeoLocationUpdated;
                }
            }
        }

        public ReadOnlyCollection<IDepictionAnnotation> DepictionAnnotations { get { return depictionAnnotations.AsReadOnly(); } }
        public IDepictionBaseDisplayer MainMap { get { return depictionBackdrop; } }
        public ReadOnlyCollection<IDepictionRevealer> Revealers { get { return depictionRevealers.AsReadOnly(); } }
        public IInteractionRuleRepository InteractionRuleRepository { get { return interactionRuleRepository; } set { interactionRuleRepository = value; } }

        public RasterImageResourceDictionary ImageResources
        {
            get { return imageResources; }
            internal set { imageResources = value; }//Used during load
        }
        #region Accessor properties

        public IMapCoordinateBounds RegionBounds
        {
            get { return DepictionGeographyInfo.DepictionRegionBounds; }
            set
            {
                if (DepictionRegionChange != null)
                {
                    DepictionRegionChange.Invoke(DepictionGeographyInfo.DepictionRegionBounds, value);
                }
                DepictionGeographyInfo.DepictionRegionBounds = value;

            }
        }
        public IMapCoordinateBounds ViewportBounds
        {
            get
            {
                return DepictionGeographyInfo.DepictionMapWindow;
            }
        }

        #endregion
        #endregion

        #region Constructor

        public DepictionStory()
            : this(new LatitudeLongitude())
        { }

        public DepictionStory(ILatitudeLongitude origin)
        {
            Metadata = new DepictionStoryMetadata();
            CompleteElementRepository = new ElementRepository();
            DepictionGeographyInfo = new DepictionGeographicInfo();

            if (origin.IsValid) DepictionGeographyInfo.DepictionStartLocation = origin;
            imageResources = new RasterImageResourceDictionary();
            DepictionsFileName = string.Empty;
        }

        #endregion

        #region helper methods

        void depictionElements_ElementGeoLocationUpdated(IDepictionElement element)
        {
            if (!element.IsGeolocated)
            {
                var list = new List<IDepictionElement> { element };
                MainMap.RemoveElementList(list);
                foreach (var revealer in Revealers)
                {
                    revealer.RemoveElementList(list);
                }
                DepictionNeedsSaving = true;
            }
        }

        private void DepictionStoryChange()
        {
            DepictionNeedsSaving = true;
        }

        public void UpdateModelViewportBounds(IMapCoordinateBounds newBounds)
        {
            if (DepictionViewportChange != null)
            {
                DepictionViewportChange(DepictionGeographyInfo.DepictionMapWindow, newBounds);
            }
            DepictionGeographyInfo.DepictionMapWindow = newBounds;
        }

        public void RequestVisibleViewportBounds(IMapCoordinateBounds newBounds)
        {
            if (DepictionViewportChangeRequested != null)
            {
                var possibleViewport = DepictionViewportChangeRequested(newBounds);
                var oldViewPort = DepictionGeographyInfo.DepictionMapWindow;
                if (!oldViewPort.Equals(possibleViewport))
                {
                    UpdateModelViewportBounds(possibleViewport);
                }
            }
            else
            {
                DepictionGeographyInfo.DepictionMapWindow = newBounds;
            }
        }

        public List<IDepictionElement> GetElementsWithIds(IEnumerable<string> elementIds)
        {
            var geoElements = depictionElements.GetElementsFromIds(elementIds).ToList();
            return geoElements;
        }
        public void AddElementGroupToDepictionElementList(IEnumerable<IDepictionElement> elements)
        {
            AddElementGroupToDepictionElementList(elements, false);
        }

        //This does not do any property replacement or EID checking, this might be obsolete.
        public void AddElementGroupToDepictionElementList(IEnumerable<IDepictionElement> elements, bool addToMainMap)
        {
            var failList = new List<IDepictionElement>();
            foreach (var element in elements)
            {
                if (!depictionElements.AddElement(element))
                {
                    failList.Add(element);
                }
            }
            Debug.WriteLine("Done adding to model, sending notification to view model");
            if (addToMainMap)
            {
                AddElementListToMainMap(elements);
            }
        }

        public void AddElementToDepictionElementList(IDepictionElement element, bool addToMainMap)
        {
            AddElementGroupToDepictionElementList(new List<IDepictionElement> { element }, addToMainMap);
        }

        public List<string> CreateOrUpdateDepictionElementListFromPrototypeList(IEnumerable<IElementPrototype> elementPrototypes, bool addToMainMap,bool markAsUpdated)
        {
            List<IDepictionElement> createdElements;
            List<IDepictionElement> updatedElements;
            CreateOrUpdateDepictionElementListFromPrototypeList(elementPrototypes, out createdElements,
                                                                out updatedElements, addToMainMap, markAsUpdated);
            var updateIds = from e in updatedElements
                            select e.ElementKey;
            return updateIds.ToList();
        }

        public void CreateOrUpdateDepictionElementListFromPrototypeList(IEnumerable<IElementPrototype> elementPrototypes, out List<IDepictionElement> createdElements,
                out List<IDepictionElement> updateElements, bool addToMainMap, bool markAsUpdated)
        {
            
            int unusableEmails = 0;
            int createElementCount;
            int updatedElements;
            int totalCount;

#if DEBUG
            var timer = Stopwatch.StartNew();
#endif
            depictionElements.AddOrUpdateRepositoryFromPrototypes(elementPrototypes, markAsUpdated,
                                                                                   out createdElements, out updateElements);
            updatedElements = updateElements.Count();
            createElementCount = createdElements.Count();
            totalCount = updatedElements + createElementCount;
#if DEBUG
            Debug.WriteLine(string.Format("Elapsed time {0} milliseconds", timer.ElapsedMilliseconds));
            timer.Stop();
#endif
            if (unusableEmails > 0)
            {
                var errorString = string.Format("Could not use {0} of the desired elements", unusableEmails);
                DepictionAccess.NotificationService.DisplayMessageString(errorString, 4);
            }
            if (totalCount != 0)
            {
                var message = string.Format("Imported {0} elements: {1} were update and {2} were created.", totalCount, updatedElements, createElementCount);
                DepictionAccess.NotificationService.DisplayMessageString(message, 5);
                Thread.Sleep(1000);
            }

            Debug.WriteLine("Done adding to model, sending notification to view model");
            if (addToMainMap)
            {
                AddElementListToMainMap(createdElements);
            }
        }

        public bool DeleteElementsWithIdsFromDepictionElementList(List<string> elementIds)
        {
            var geoElements = depictionElements.DeleteElementsWithIdsAndReturnElements(elementIds);

            foreach (var revealer in Revealers)
            {
                RemoveElementsFromRevealer(revealer, geoElements);
            }
            DepictionNeedsSaving = true;

            RemoveElementListFromMainMap(geoElements);
            if (DepictionNeedsSaving)
            {
                //What was this for?
            }
            return true;
        }

        public void RequestElementsWithIdsViewing(IEnumerable<string> elementIds, DepictionDialogType[] dialogs)
        {
            if (RequestedElementsInDialogType != null) RequestedElementsInDialogType(elementIds, dialogs);
        }

        public void AddElementListToMainMap(IEnumerable<IDepictionElement> elements)
        {//This method could be very very bad if left exposed via the api because the elements might not be in the main depiction\
            //element list
            if (MainMap.AddElementList(elements))
            {
                DepictionNeedsSaving = true;
            }
        }
        public void RemoveElementListFromMainMap(List<IDepictionElement> elements)
        {
            if (MainMap.RemoveElementList(elements))
            {
                DepictionNeedsSaving = true;
            }
        }

        public void AddAnnotation(IDepictionAnnotation annotation)
        {
            depictionAnnotations.Add(annotation);
            DepictionNeedsSaving = true;
            NotifyDepictionSingleItemChange(annotation, NotifyCollectionChangedAction.Add);
        }
        //SHould this just remove by ID? TODO
        public void RemoveAnnotation(IDepictionAnnotation annotation)
        {
            if (depictionAnnotations.Remove(annotation))
            {
                DepictionNeedsSaving = true;
                NotifyDepictionSingleItemChange(annotation, NotifyCollectionChangedAction.Remove);
            }
        }
        #region Revealer changing region
        public void AddRevealer(IDepictionRevealer revealer)
        {
            depictionRevealers.Add(revealer);
            var r = revealer as DepictionRevealer;
            if (r != null)
            {
                r.CurrentDepiction = this;
            }
            DepictionNeedsSaving = true;
            NotifyDepictionSingleItemChange(revealer, NotifyCollectionChangedAction.Add);

        }
        public void DeleteRevealer(IDepictionRevealer revealer)
        {
            var removeSuccess = depictionRevealers.Remove(revealer);
            if (removeSuccess)
            {
                DepictionNeedsSaving = true;
                NotifyDepictionSingleItemChange(revealer, NotifyCollectionChangedAction.Remove);
            }
        }
        public void AddElementsToRevealer(IDepictionRevealer revealer, List<IDepictionElement> elements)
        {
            if (revealer.AddElementList(elements))
            {
                DepictionNeedsSaving = true;
            }
        }
        public void RemoveElementsFromRevealer(IDepictionRevealer revealer, List<IDepictionElement> elements)
        {
            if (revealer.RemoveElementList(elements))
            {
                DepictionNeedsSaving = true;
            }
        }
        #endregion
        #region Notification helper
        protected void NotifyDepictionSingleItemChange<T>(T itemChanged, NotifyCollectionChangedAction whatHappened)
        {

            if (itemChanged is IDepictionRevealer && RevealerListChange != null)
            {
                RevealerListChange(this, new NotifyCollectionChangedEventArgs(whatHappened, (IDepictionRevealer)itemChanged));
            }
            else if (typeof(T).Equals(typeof(IDepictionAnnotation)) && AnnotationListChange != null)
            {
                AnnotationListChange(this, new NotifyCollectionChangedEventArgs(whatHappened, itemChanged as IDepictionAnnotation));
            }
        }

        #endregion
        #endregion
        #region ToString
        public override string ToString()
        {
            var details = DepictionGeographyInfo.DepictionMapWindow.ToString();
            return details;
        }
        #endregion
        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionStory;
            if (other == null) return false;

            if (!Equals(Metadata, other.Metadata)) return false;
            if (!Equals(DepictionGeographyInfo, other.DepictionGeographyInfo)) return false;
            if (!Equals(ImageResources, other.ImageResources)) return false;

            if (!Equals(depictionBackdrop, other.depictionBackdrop)) return false;

            if (!Equals(depictionElements, other.depictionElements)) return false;

            if (depictionAnnotations.Count != other.depictionAnnotations.Count) return false;
            for (int i = 0; i < depictionAnnotations.Count; i++)
            {
                if (!Equals(depictionAnnotations[i], other.depictionAnnotations[i])) return false;
            }

            if (depictionRevealers.Count != other.depictionRevealers.Count) return false;
            for (int i = 0; i < depictionRevealers.Count; i++)
            {
                if (!Equals(depictionRevealers[i], other.depictionRevealers[i])) return false;
            }
            return true;
        }
        #endregion

        #region Serialization Stuff

        static public void SerializeADepiction(string tempHoldingDir, DepictionStory depiction, List<DepictionSubtreeInfoBase> subtrees)
        {
            try
            {
                //Not sure why we want to clear the type name dictionary, actually im not
                //even sure why it doesnt just stick around as a permanent addition
                //to depiciont access
                foreach (var subtree in subtrees)
                {
                    if (SerializationService.UpdateProgressReport(0, string.Format("Saving {0}", subtree.UserReadableName)) == false) break;
                    subtree.SaveSubtree(depiction, tempHoldingDir);
                }
                var typeFileName = "DepictionDataTypes.xml";
                var fullFileName = Path.Combine(tempHoldingDir, typeFileName);
                DepictionTypeInformationSerialization.SaveSerializationServiceTypeDictionaryToFile(fullFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static public DepictionStory DeserializeADepiction(string tempHoldingDir, List<DepictionSubtreeInfoBase> subtrees)
        {
            var newDepiction = new DepictionStory();
            //load default interaction rules
            DepictionAccess.InteractionsLibrary.RefreshInteractionLibrary();
            newDepiction.InteractionRuleRepository.LoadRules(DepictionAccess.InteractionsLibrary.MergedInteractions);

            var typeFileName = "DepictionDataTypes.xml";
            var fullFileName = Path.Combine(tempHoldingDir, typeFileName);
            DepictionCoreTypes.DoFullTypeUpdateFromFileAndDefaults(fullFileName);
           
            foreach (var subtree in subtrees)
            {
                if (SerializationService.UpdateProgressReport(0, string.Format("Loading {0}", subtree.UserReadableName)) == false) break;
                subtree.LoadSubtree(newDepiction, tempHoldingDir);
            }

            if (SerializationService.IsSaveLoadCancelled() != true)
            {
                //Hack until we get a depiction access type thing
                foreach (var revealer in newDepiction.Revealers)
                {
                    var r = revealer as DepictionRevealer;
                    if (r != null)
                    {
                        r.CurrentDepiction = newDepiction;
                    }
                }
            }
            SerializationService.UpdateProgressReport(int.MaxValue, string.Format("Loading Visuals: Depiction may temporarily become unresponsive"));
            return newDepiction;
        }

        #endregion

    }
}
