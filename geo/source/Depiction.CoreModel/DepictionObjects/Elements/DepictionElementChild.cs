﻿using Depiction.APINew.Interfaces.ElementInterfaces;
using Depiction.CoreModel.AbstractDepictionObjects;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class DepictionElementChild : DepictionElementBase,IDepictionChildElement 
    {
        #region Variables

        private IDepictionElement parent;
        #endregion
        #region Implementation of IDepictionChildElement

        public IDepictionElement Parent
        {
            get { return parent; }
            internal set { parent = value; }
        }

        #endregion
        
        #region Equals Region

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            var otherChild = obj as DepictionElementChild;
            if (otherChild == null) return false;
            //This is actuallya bad call since checking the complete parent involves checking children
            //ie infinite loop
//            if (!Parent.Equals(otherChild.Parent)) return false;
            if (!Parent.ElementKey.Equals(otherChild.Parent.ElementKey)) return false;
            return base.Equals(obj);
        }

        #endregion
    }
}