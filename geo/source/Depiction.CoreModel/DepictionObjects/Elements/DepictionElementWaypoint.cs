using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class DepictionElementWaypoint : DepictionModelBase, IDepictionElementWaypoint
    {
        private string _key = Guid.NewGuid().ToString();

        private double _iconSize = 10;
        private string _nodeOwnerKey = "";
        private string _name = "Uknown";
        private ILatitudeLongitude _location = new LatitudeLongitude();

        private DepictionIconPath _iconPath = new DepictionIconPath();

        public string NodeOwnerKey { get { return _nodeOwnerKey; } set { _nodeOwnerKey = value; } }//the set should be private

        public string Key
        {
            get { return _key; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; NotifyModelPropertyChanged("Name"); }
        }
        public bool IsDeletable { get; private set; }

        public double IconSize
        {
            get { return _iconSize; }
            set { _iconSize = value; NotifyModelPropertyChanged("IconSize"); }
        }

        public ILatitudeLongitude Location
        {
            get { return _location; }
            set { _location = value; NotifyModelPropertyChanged("Location"); }
        }

        public DepictionIconPath IconPath
        {
            get { return _iconPath; }
            set { _iconPath = value; NotifyModelPropertyChanged("IconPath"); }
        }

        //Hack
        /// <summary>
        /// DO NOT USE THIS PROPRETY it is a a fake
        /// </summary>
        protected ILatitudeLongitude LocationNoBehavior { get; set; }
        public void UpdateLocationWithoutChangeNotification(ILatitudeLongitude newLocation)
        {
            _location = newLocation.DeepClone();
            NotifyModelPropertyChanged("LocationNoBehavior");
        }

        #region Constructor
        /// <summary>
        /// Creates a deletable node with a Random name
        /// </summary>
        public DepictionElementWaypoint()
        {
            IsDeletable = true;
            _iconPath = new DepictionIconPath(IconPathSource.EmbeddedResource, "Generic.RouteWayPoint");
            Name = "Random";
        }

        #endregion

        #region Equals Region

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public IDepictionElementWaypoint DeepClone()
        {
            var newWaypoint = new DepictionElementWaypoint();
            newWaypoint._location = new LatitudeLongitude(Location.Latitude, Location.Longitude);
            newWaypoint._iconPath = _iconPath;
            newWaypoint._name = _name;
            newWaypoint._iconSize = _iconSize;
            newWaypoint._key = _key;
            newWaypoint.IsDeletable = IsDeletable;
            return newWaypoint;
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        public override bool Equals(object obj)
        {
            var other = obj as DepictionElementWaypoint;
            if (other == null) return false;

            // if (!Equals(NodeOwnerKey, other.NodeOwnerKey)) return false;
            if (IconSize != other.IconSize) return false;
            if (!Equals(IconPath, other.IconPath)) return false;
            if (!Equals(Name, other.Name)) return false;
            if (!Equals(IsDeletable, other.IsDeletable)) return false;
            if (!Equals(Location, other.Location)) return false;
            return true;
        }

        #endregion

        #region IXmlSerializable
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            if (reader.Name.Equals("elementWaypoint"))
            {
                _name = reader.GetAttribute("name");

                //_key = reader.GetAttribute("NodeId");
                double val;
                if (double.TryParse(reader.GetAttribute("IconSize"), out val))
                {
                    _iconSize = val;
                }
                else
                {
                    _iconSize = 10;
                }
                _iconPath = (DepictionIconPath)DepictionIconPathTypeConverter.ConvertFromString(reader.GetAttribute("IconPath"));
                Location = LatitudeLongitudeTypeConverter.ConvertStringToLatLong(reader.GetAttribute("Location"));
                bool deletable;
                if (bool.TryParse(reader.GetAttribute("deletable"), out deletable))
                {
                    IsDeletable = deletable;
                }
                else
                {
                    IsDeletable = false;
                }
                reader.Read();
            }

        }

        public void WriteXml(XmlWriter writer)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            writer.WriteStartElement("elementWaypoint");
            //            writer.WriteStartAttribute("Attrub");
            //            writer.WriteValue(true);
            //            writer.WriteEndAttribute();
            writer.WriteAttributeString("name", Name);
            //writer.WriteAttributeString("NodeId", Key);
            if (IconPath != null) writer.WriteAttributeString("IconPath", IconPath.ToString());
            writer.WriteAttributeString("IconSize", IconSize.ToString());
            writer.WriteAttributeString("Location", Location.ToXmlSaveString());
            writer.WriteAttributeString("deletable", IsDeletable.ToString());
            writer.WriteEndElement();

        }
        #endregion
    }
}