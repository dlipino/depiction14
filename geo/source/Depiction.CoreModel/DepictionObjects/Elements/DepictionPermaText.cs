﻿using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.Serialization;

namespace Depiction.CoreModel.DepictionObjects.Elements
{
    public class DepictionPermaText : IPermaText
    {
        private const double DefaultX = 25;
        private const double DefaultY = 25;
        private const double DefaultWidth = 200;
        private const double DefaultHeight = 100;
        #region Variables

        private double permaTextX = DefaultX;
        private double permaTextY = DefaultY;
        private double pixelWidth = DefaultWidth;
        private double pixelHeight = DefaultHeight;
        private bool isEnhancedPermaText;

        #endregion
        
        #region Implementation of IPermaText
        
        public bool IsDefault
        {
            get
            {
                if (permaTextX != DefaultX) return false;
                if (permaTextY != DefaultY) return false;
                if (pixelWidth != DefaultWidth) return false;
                if (pixelHeight != DefaultHeight) return false;
                if (isEnhancedPermaText ) return false;
                return true;
            }
        }

        public double PermaTextX
        {
            get { return permaTextX; }
            set { permaTextX = value; }
        }

        public double PermaTextY
        {
            get { return permaTextY; }
            set { permaTextY = value; }
        }

        public double PixelWidth
        {
            get { return pixelWidth; }
            set { pixelWidth = value; }
        }

        public double PixelHeight
        {
            get { return pixelHeight; }
            set { pixelHeight = value; }
        }

        public double MinPixelHeight { get { return 50; } }
        public double MinPixelWidth { get { return 105; } }


        public bool IsEnhancedPermaText
        {
            get { return isEnhancedPermaText; }
            set { isEnhancedPermaText = value; }
        }

        #endregion

        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            if (SerializationService.IsSaveLoadCancelled()==true) return;

            if (!reader.Name.Equals("ElementPermaText")) return;
            var permaTextXText = reader.GetAttribute("permaTextX");
            var permaTextYText = reader.GetAttribute("permaTextY");
            var pixelWidthText = reader.GetAttribute("pixelWidth");
            var pixelHeightText = reader.GetAttribute("pixelHeight");
            var isSimplePermaTextText = reader.GetAttribute("isSimplePermaText");

            permaTextX = double.Parse(permaTextXText);
            permaTextY = double.Parse(permaTextYText);
            pixelWidth = double.Parse(pixelWidthText);
            pixelHeight = double.Parse(pixelHeightText);
            isEnhancedPermaText = bool.Parse(isSimplePermaTextText);


            reader.Read();
//            reader.ReadStartElement();
//            if (reader.Name.Equals("PermaTextX"))
//            {
//                permaTextX = reader.ReadElementContentAsDouble("PermaTextX", ns);
//            }
//            if (reader.Name.Equals("PermaTextY"))
//            {
//                permaTextY = reader.ReadElementContentAsDouble("PermaTextY", ns);
//            }
//            if (reader.Name.Equals("PixelWidth"))
//            {
//                PixelWidth = reader.ReadElementContentAsDouble("PixelWidth", ns);
//            }
//            if (reader.Name.Equals("PixelHeight"))
//            {
//                PixelHeight = reader.ReadElementContentAsDouble("PixelHeight", ns);
//            }
//            if (reader.Name.Equals("IsEnhancedPermaText"))
//            {
//                IsEnhancedPermaText = reader.ReadElementContentAsBoolean("IsEnhancedPermaText", ns);
//            }
//
//            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
//            {
//                reader.Read();
//            }
//            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("ElementPermaText");
            writer.WriteAttributeString("permaTextX", PermaTextX.ToString());
            writer.WriteAttributeString("permaTextY", PermaTextY.ToString());
            writer.WriteAttributeString("pixelWidth", PixelWidth.ToString());
            writer.WriteAttributeString("pixelHeight", PixelHeight.ToString());
            writer.WriteAttributeString("isSimplePermaText", IsEnhancedPermaText.ToString().ToLower());
            writer.WriteEndElement();
//            writer.WriteElementString("PermaTextX", ns, PermaTextX.ToString());
//            writer.WriteElementString("PermaTextY", ns, PermaTextY.ToString());
//            writer.WriteElementString("PixelWidth", ns, PixelWidth.ToString());
//            writer.WriteElementString("PixelHeight", ns, PixelHeight.ToString());
//            //The toLower is needed for the boolean to be read back in as a boolean
//            writer.WriteElementString("IsEnhancedPermaText", ns, IsEnhancedPermaText.ToString().ToLower());
        }

        #endregion

        #region Equals overrdie 

        public IPermaText DeepClone()
        {
            var newPermaText = new DepictionPermaText();
            newPermaText.permaTextX = PermaTextX;
            newPermaText.permaTextY = PermaTextY;
            newPermaText.isEnhancedPermaText = IsEnhancedPermaText;
            newPermaText.permaTextX = PermaTextX;
            newPermaText.permaTextX = PermaTextX;
            newPermaText.pixelHeight = PixelHeight;
            newPermaText.pixelWidth = PixelWidth;
            return newPermaText;
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }

        public override bool Equals(object obj)
        {
            var other = obj as DepictionPermaText;
            return Equals(other);
        }

        public bool Equals(DepictionPermaText obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!PermaTextX.Equals(obj.PermaTextX)) return false;
            if (!PermaTextY.Equals(obj.PermaTextY)) return false;
            if (!PixelHeight.Equals(obj.PixelHeight)) return false;
            if (!PixelWidth.Equals(obj.PixelWidth)) return false;
            if (!IsEnhancedPermaText.Equals(obj.IsEnhancedPermaText)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int result = permaTextX.GetHashCode();
                result = (result*397) ^ permaTextY.GetHashCode();
                result = (result*397) ^ pixelWidth.GetHashCode();
                result = (result*397) ^ pixelHeight.GetHashCode();
                result = (result*397) ^ isEnhancedPermaText.GetHashCode();
                return result;
            }
        }
        #endregion
    }
}