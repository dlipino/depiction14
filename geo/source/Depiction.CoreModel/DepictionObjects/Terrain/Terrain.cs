﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Media3D;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.OldInterfaces;
using Depiction.API.Properties;
using Depiction.API.Service;
using Depiction.API.StaticAccessors;
using Depiction.API.TerrainObjects;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.Serialization;


namespace Depiction.CoreModel.DepictionObjects.Terrain
{
    public class Terrain : ICoverage,IRestorable, IXmlSerializable
    {
        private TerrainData TerrainPlusStructure { get; set; }
        private TerrainData terrainData;

        private IDepictionImageMetadata terrainVisual;

        private readonly object addTerrainDataFromFileLock = new object();

        #region Constructor

        public Terrain()
        {
            Init(new TerrainData());
        }
        public Terrain(TerrainData terrainData)
        {
            Init(terrainData);
            CreateTerrainPlusStructure();
        }

        private void Init(TerrainData initTerrainData)
        {
            terrainData = initTerrainData;
            TerrainPlusStructure = new TerrainData();
        }
        #endregion

        #region Properties

        public bool HasGoodData
        {
            get { return terrainData.HasGoodData; }
            set { terrainData.HasGoodData = value; }
        }

        public IDepictionImageMetadata Visual
        {
            get
            {
                if (terrainVisual == null)
                    terrainVisual = GenerateTerrainVisual();
                return terrainVisual;
            }
        }
        #endregion

        #region private heper methods

        private DepictionImageMetadata GenerateTerrainVisual()
        {
            // To ensure that the newly modified terrain gets shown, we need to give it a unique name.
            string tempFilename = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, string.Format("modifiedTerrain-{0}.jpg",Guid.NewGuid()));

            bool createdVisual = SaveTo24BitRGB(tempFilename, 90,
                                                AppDomain.CurrentDomain.BaseDirectory + "CoverageData\\default_relative.cmt");
            if (createdVisual)
                return new DepictionImageMetadata(tempFilename, GetTopLeftPosition(), GetBottomRightPosition(), "Terrain"){CanBeManuallyGeoAligned = false};
            return null;
        }

        private bool CreateTerrainPlusStructure()
        {
            lock (terrainData)
            {
                if (terrainData == null || !terrainData.HasGoodData) return false;
            }
            var tempTerrainPlusStructure = new TerrainData();
            if (tempTerrainPlusStructure.CreateFromExisting(terrainData))
            {
                lock (TerrainPlusStructure)
                {
                    TerrainPlusStructure = tempTerrainPlusStructure;
                    terrainVisual = null;
                }
                return true;
            }
            return false;
        }

        private static float GetElevationAndReadoutFromDataGrid(TerrainData dataGrid, ILatitudeLongitude location, string units, out string readout)
        {
            readout = ", Elevation: ";
            float elevation;
            if (!dataGrid.HasElevation(location))
            {
                readout += "Unknown";
                return 0;
            }

            try
            {
                if (dataGrid.HasNoData(location) || !dataGrid.HasGoodData)
                {
                    readout += "No Data";
                    return 0;
                }
                elevation = dataGrid.GetClosestElevationValue(location);

                if (units == "ft") elevation = elevation * 3.2808399F;

                readout += String.Format("{0} {1}",
                                         elevation.ToString("F" + Settings.Default.Precision), units);
                return elevation;
            }
            catch (Exception e)
            {
                //DepictionExceptionHandler.HandleException("Unable to get elevation data for location", e, false);
                readout = "";
                return 0;
            }
        }

        /// <summary>
        /// Create a bounding box that is a wee bit bigger than the region bounds,
        /// so that we can then crop the erroneous edge pixels that come from the data source.
        /// </summary>
        /// <param name="bbox"></param>
        /// <param name="paddingPercent"></param>
        /// <returns></returns>
        private static IMapCoordinateBounds CreateDilatedBoundingBox(IMapCoordinateBounds bbox, double paddingPercent)
        {
            double offsetDistanceX = Math.Abs(bbox.Left - bbox.Right) * paddingPercent;
            double offsetDistanceY = Math.Abs(bbox.Top - bbox.Bottom) * paddingPercent;
            //pad the region bbox with paddingPercent*100% on all sides so WCS request can accomodate
            //cropping and warping from Geo to Mercator, etc.
            //
            return new MapCoordinateBounds
            {
                Top = bbox.Top + offsetDistanceY,
                Bottom = bbox.Bottom - offsetDistanceY,
                Left = bbox.Left - offsetDistanceX,
                Right = bbox.Right + offsetDistanceX,
                MapImageSize = bbox.MapImageSize
            };
        }
        #endregion
        
        #region public helper methods
        public void Restore(bool notifyChange)
        {
            CreateTerrainPlusStructure();
            var elevation =
                DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("Elevation");
            
            if (elevation != null)
            {
                lock (elevation)
                {
                    string newFileName;
                    var imageMetadata = Visual;
                    BitmapSaveLoadService.LoadImageStoreInDepictionImageResourcesAndRemoveOldKey(
                        imageMetadata.ImageFilename, elevation.ImageMetadata.ImageFilename, out newFileName);
                    imageMetadata.ImageFilename = newFileName;
                    if(notifyChange)
                    {
                        elevation.SetImageMetadata(imageMetadata);
                    }else
                    {
                        elevation.SetImageMetadataWithoutNotification(imageMetadata);
                    }
//                    elevation.ImageMetadata = imageMetadata;
                }
            }
        }

        public void ClearVisual()
        {//This really doesn't do much
            terrainVisual = null;
        }
        /// <summary>
        /// Get coverage value at a given location in string format
        /// </summary>
        public string GetValueForDisplay(ILatitudeLongitude location)
        {
            float trueElevation;
            float modifiedElevation;
            string units;
            string trueElevationReadout;
            string modifiedElevationReadout;

            if (Settings.Default.MeasurementSystem.Equals(MeasurementSystem.Imperial))
                units = "ft";
            else
                units = "m";

            trueElevation = GetElevationAndReadoutFromDataGrid(terrainData, location, units, out trueElevationReadout);
            modifiedElevation = GetElevationAndReadoutFromDataGrid(TerrainPlusStructure, location, units,
                                                                   out modifiedElevationReadout);
            if (trueElevation == modifiedElevation)
                return trueElevationReadout;

            var elevationChange = modifiedElevation - trueElevation;
            return String.Format("{0} ({1} {2} structure)",
                                 modifiedElevationReadout,
                                 elevationChange.ToString("F" + Settings.Default.Precision),
                                 units);
        }

        public bool LineOfSight(ILatitudeLongitude point1, ILatitudeLongitude point2, ArrayList intersectionList)
        {
            double x1, y1, x2, y2;
            x1 = point1.Longitude;
            y1 = point1.Latitude;
            x2 = point2.Longitude;
            y2 = point2.Latitude;
            var success = terrainData.LineOfSight(x1, y1, x2, y2, intersectionList);
            return success;
        }
        public void Create(double south, double north, double east, double west, int width, int height, bool floatgrid, int defaultValue, string projFilename, int wktLength, bool updateVisual)
        {
            terrainData = new TerrainData();
            terrainData.CreateFromExisting(south, north, east, west, width, height, floatgrid, defaultValue, projFilename, wktLength);
            CreateTerrainPlusStructure();
        }
        public bool AddTerrainData(TerrainData data)
        {
            lock (terrainData)
            {
                Init(data);
            }
            return CreateTerrainPlusStructure();
        }
        #endregion

        #region Terran file methods

        public void SaveToGeoTiff(string filename)
        {
            TerrainPlusStructure.SaveToGeoTiff(filename);
        }

        public bool SaveTo24BitRGB(string filename, int quality, string colormapFileName)
        {
            //            DepictionAccess.NotificationService.SendNotification(
            //                string.Format("Generating visual for elevation data. Please wait..."), 10);
            return TerrainPlusStructure.SaveTo24BitRGB(filename, quality, colormapFileName);
        }

        public bool AddFromFileWithoutExtents(string filename, double top, double bottom, double right, double left)
        {
            var result = terrainData.AddFromFileWithoutExtents(filename, top, bottom, right, left);
            return result;
        }
        public TerrainData AddTerrainDataFromFile(string filename, string coordSystem, string elevationUnit, bool showDownSampleMessage, out bool downSampled, bool useExistingGridSpacing, bool cropGridBeforeProjection)
        {
            TerrainData tempTerrainData;
            downSampled = false;

            lock (addTerrainDataFromFileLock)
            {
                if (!terrainData.HasGoodData)
                {
                    tempTerrainData = new TerrainData();
                    //
                    //this terraindata object gets 'written' over by the Add method when we
                    //add actual data from a file...so, initial dimensions are not important -- the latlong bounds are important, though.
                    //
                    var regionExtent = DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
                    var paddedBox = CreateDilatedBoundingBox(regionExtent, 0.1);
                    var creationSuccess = tempTerrainData.CreateFromExisting(paddedBox.Bottom,
                                                                             paddedBox.Top,
                                                                             paddedBox.Right,
                                                                             paddedBox.Left, 5,
                                                                             5, true, -32768, "", 0);
                    //tempTerrainData is in Mercator, which is default if you don't pass a projection file name
                    if (!creationSuccess)
                    {
                        //DepictionAccess.NotificationService.SendNotification(
                        //    string.Format("Unable to add elevation data due to insufficient memory"), 10);
                        return null;

                    }

                    tempTerrainData.Add(filename, coordSystem, out downSampled, elevationUnit, useExistingGridSpacing, cropGridBeforeProjection);
                    //crop it to the region bounding box
                    tempTerrainData.CropElevationData(regionExtent);
                    if (downSampled && showDownSampleMessage)
                    {
                        // DepictionAccess.NotificationService.SendNotification(
                        //     string.Format("Elevation data from file {0} was too large. Reducing resolution to fit in memory.", filename));

                    }
                    return tempTerrainData;
                }
                //terrainData already exists
                tempTerrainData = new TerrainData();
                if (tempTerrainData.CreateFromExisting(terrainData))
                {
                    tempTerrainData.Add(filename, coordSystem, out downSampled, elevationUnit, useExistingGridSpacing, cropGridBeforeProjection);
                    if (downSampled && showDownSampleMessage)
                    {
                        //DepictionAccess.NotificationService.SendNotification(
                        //    string.Format("Elevation data from file {0} was too large. Reducing resolution to fit in memory.", filename));

                    }
                    return tempTerrainData;
                }
                return null;
            }
        }
        #endregion

        #region TerrainData methods

        public int GetGridWidthInPixels()
        {
            return TerrainPlusStructure.GetGridWidthInPixels();
        }

        public int GetGridHeightInPixels()
        {
            return TerrainPlusStructure.GetGridHeightInPixels();
        }

        public double GetGridResolution()
        {
            return TerrainPlusStructure.GetGridResolution();
        }

        public int GetRow(double lat)
        {
            return TerrainPlusStructure.GetRow(lat);
        }

        public int GetColumn(double lon)
        {
            return TerrainPlusStructure.GetColumn(lon);
        }

        public double GetLatitude(int row)
        {
            return TerrainPlusStructure.GetLatitude(row);
        }

        public double GetLongitude(int col)
        {
            return TerrainPlusStructure.GetLongitude(col);
        }

        public float GetValueAtGridCoordinate(int col, int row)
        {
            return TerrainPlusStructure.GetElevationValueFromGridCoordinate(col, row);

        }

        public float GetElevationValueClosest(ILatitudeLongitude latitudeLongitude)
        {
            return TerrainPlusStructure.GetClosestElevationValue(latitudeLongitude);
        }

        public void SetValueAtGridCoordinate(int col, int row, float value)
        {
            TerrainPlusStructure.SetElevationValue(col, row, value);
        }

        public float GetInterpolatedElevationValue(ILatitudeLongitude latitudeLongitude)
        {
            return TerrainPlusStructure.GetInterpolatedElevationValue(latitudeLongitude);
        }

        public float GetConvolvedValue(int col, int row, int kernel)
        {
            return TerrainPlusStructure.GetConvolvedValue(col, row, kernel);
        }

        #endregion

        #region TerrainPlusStructure methods

        public ILatitudeLongitude GetTopLeftPosition()
        {
            return TerrainPlusStructure.GetTopLeftPos();
        }

        public ILatitudeLongitude GetBottomRightPosition()
        {
            return TerrainPlusStructure.GetBottomRightPos();
        }

        public string GetProjectionInWkt()
        {
            return TerrainPlusStructure.GetProjectionInWkt();
        }

        public bool IsFloatMode()
        {
            return TerrainPlusStructure.IsFloatMode();
        }

        #endregion

        #region XmlSerialization stuff

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            string ns = SerializationConstants.DepictionXmlNameSpace;
            var difList = new List<Point3D>();
            //Legacy 1.2.2 
            if(reader.Name.Equals("Terrain"))
            {
                reader.Read();
                if (reader.Name.Equals("terrainData"))
                {
                    terrainData = SerializationService.DeserializeObject<TerrainData>("terrainData", reader);
                }
                if (reader.Name.Equals("terrainPlusStructure"))
                {
                    TerrainPlusStructure = SerializationService.DeserializeObject<TerrainData>("terrainPlusStructure", reader);
                }
                else if (reader.Name.Equals("TerrainDifferences"))
                {
                    reader.ReadStartElement("TerrainDifferences", ns);
                    while (reader.IsStartElement("terrainDifference"))
                    {
                        difList.Add(SerializationService.Deserialize<Point3D>("terrainDifference", reader));
                    }
                    reader.ReadEndElement();
                    CreateTerrainPlusStructure();
                    TerrainPlusStructure.SetElevationValues(difList);
                }
                else{ CreateTerrainPlusStructure();}
                //reader.ReadEndElement();
                return;
            }

            
            if (reader.Name.Equals("terrainData"))
            {
                terrainData = SerializationService.DeserializeObject<TerrainData>("terrainData", reader);
            }
            if (reader.Name.Equals("terrainPlusStructure"))
            {
                TerrainPlusStructure = SerializationService.DeserializeObject<TerrainData>("terrainPlusStructure", reader);
            }
            else if (reader.Name.Equals("TerrainDifferences"))
            {
                reader.ReadStartElement("TerrainDifferences", ns);
                while (reader.IsStartElement("terrainDifference"))
                {
                    difList.Add(SerializationService.DeserializeObject<Point3D>("terrainDifference", reader));
                }
                
                CreateTerrainPlusStructure();
                TerrainPlusStructure.SetElevationValues(difList);
                reader.ReadEndElement();
            }
            else
                CreateTerrainPlusStructure();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            SerializationService.SerializeObject("terrainData", terrainData, writer);
            var difs = TerrainPlusStructure.FindDifferenceWith(terrainData);
            if (difs.Count > 0)
            {
                writer.WriteStartElement("TerrainDifferences", ns);
                foreach (var dif in difs)
                {
                    SerializationService.SerializeObject("terrainDifference", dif, writer);
                }
                writer.WriteEndElement();
            }
           
        }

        #endregion
    }
}