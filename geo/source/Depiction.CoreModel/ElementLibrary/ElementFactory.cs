﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.Service;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.HelperClasses;

namespace Depiction.CoreModel.ElementLibrary
{
    public class ElementFactory
    {
        public const string ElementFileIsInvalid = "Element file: {0} is invalid. This type of element will not be available until its element file is fixed. Please either delete it or correct it.";

        #region Creating a raw prototype with now real type (no default properties)
        static public IElementPrototype CreateRawPrototype()
        {
            return CreateRawPrototypeOfType(null);
        }
        static public IElementPrototype CreateRawPrototypeOfType(string type)
        {
            return CreateRawPrototypeOfType(type, false);
        }
        static public IElementPrototype CreateRawPrototypeOfType(string type, bool withDefaultProperties)
        {
            var typeBase = type;
            if (string.IsNullOrEmpty(type))
            {//hmmm the rawPrototype status needs to be cleaned up
                typeBase = ElementPrototypeLibrary.RawPrototypeName;
            }
            return new ElementPrototype(typeBase, typeBase, true, withDefaultProperties);
        }
        #endregion

        //This method can be dangerous because it might accidently suck up all the computing power
        static public bool RunElementZOIGenerationBehaviors(IDepictionElement elementModel)
        {
            if (elementModel.GenerateZoiActions != null)
            {
                var behaviorDict = AddinRepository.Instance.GetBehaviors();
                foreach (var action in elementModel.GenerateZoiActions)
                {
                    var key = action.Key;
                    var behaviorList = behaviorDict.Where(t => t.Key.BehaviorName.Equals(key)).ToList();
                    if (behaviorList.Count == 1)
                    {
                        var behavior = behaviorList[0].Value;
                        behavior.DoBehavior(elementModel, action.Value);
                        return true;
                    }
                }
            }
            return false;
        }

        static public List<IElementPrototype> GetAllPrototypesFromDirectory(string directory, string imageDir)
        {
            var prototypes = new List<IElementPrototype>();
            if (!Directory.Exists(directory)) return prototypes;
            var fileList = Directory.GetFiles(directory, "*.dml");
            foreach (var fileName in fileList)
            {
                var protoType = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(fileName);

                if (protoType != null)
                {
                    var proto = protoType as ElementPrototype;
                    if (proto != null)
                    {
                        proto.DMLFileName = fileName;
                        if (ElementPrototypeLibrary.DeprecatedList.Contains(proto.ElementType))
                        {
                            proto.Deprecated = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(imageDir) && Directory.Exists(imageDir))
                    {
                        DepictionIconPath iconPath;
                        if (protoType.GetPropertyValue("IconPath", out iconPath))
                        {
                            if (iconPath.Source.Equals(IconPathSource.File))
                            {
                                var fullPath = Path.Combine(imageDir, iconPath.Path);
                                if (File.Exists(fullPath))
                                {
                                    string name;
                                    BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(fullPath, out name);
                                }
                            }
                            else if (iconPath.Source.Equals(IconPathSource.EmbeddedResource))
                            {//This part is new for 1.4 since depiction products no longer have all default icons
                                var fullPath = Path.Combine(imageDir, iconPath.Path);
                                if (File.Exists(fullPath))
                                {
                                    string name;
                                    BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(fullPath, out name);
                                }
                            }
                        }
                    }

                    prototypes.Add(protoType);
                }
            }
            return prototypes;
        }
        public static void SavePrototypesToDirectory(IEnumerable<IElementPrototype> elements, string path)
        {
            foreach (var prototype in elements)
            {
                var filename = Path.Combine(path, String.Format("{0}.{1}", prototype.ElementType, ElementPrototypeLibrary.ElementFileExtension));
                UnifiedDepictionElementWriter.WriteIDepictionElementBaseToFile(prototype, filename, false);
            }
        }

        //        // YOu don't ever want to do this because it overwrites everything. Well maybe you do want to do this, but it is dangerous
        //        static public IDepictionElementBase ConvertTargetElementToNewElementTypeAndTransferProperties(IDepictionElementBase startingElement, string targetElement, bool notifyPropertyChange)
        //        {
        //            if (DepictionAccess.ElementLibrary == null) return null;
        //            var newPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(targetElement);
        //            var newElement = CreateElementFromPrototype(newPrototype);
        //            TransferAllPropertiesToDifferentPrototype(startingElement, newElement, notifyPropertyChange);
        //            return newElement;
        //        }

        static public void UpdateElementWithPrototypeProperties(IDepictionElementBase baseElement, IElementPrototype elementPrototype,
           bool notifyPropertyChange, bool markAsUpdated, bool replaceAttributes, bool useAllPropertyNames)
        {
            var inProps = elementPrototype.OrderedCustomProperties;
            foreach (var property in inProps)
            {
                var prop = property.DeepClone();
                if (replaceAttributes) baseElement.AddPropertyOrReplaceValueAndAttributes(prop, useAllPropertyNames, notifyPropertyChange);
                else baseElement.AddPropertyOrReplaceValueNotAttributes(prop, useAllPropertyNames, notifyPropertyChange);
            }
            var updateable = baseElement as IDepictionElement;
            if (updateable != null && markAsUpdated)
            {
                updateable.ElementUpdated = true;
            }
        }
        //        static public void UpdateElementWithPrototypeProperties(IDepictionElementBase baseElement, IElementPrototype elementPrototype,
        //            bool notifyPropertyChange, bool markAsUpdated, bool replaceAttributes)
        //        {
        //            
        //        }
        static public void UpdateElementWithPrototypeProperties(IDepictionElementBase baseElement, IElementPrototype elementPrototype,
            bool notifyPropertyChange, bool checkInternalAndDisplayname)
        {
            UpdateElementWithPrototypeProperties(baseElement, elementPrototype, notifyPropertyChange, true, false, checkInternalAndDisplayname);
        }

        static IElementPrototype MatchPrototypeToZOIType(DepictionGeometryType rawPrototypeGeometryType, IElementPrototype initialPrototype)
        {
            if (DepictionAccess.ElementLibrary == null) return initialPrototype;

            if (initialPrototype.GenerateZoiActions != null && initialPrototype.GenerateZoiActions.Count > 0) return initialPrototype;
            //Hack to fix the fact that antenna,flood and a few other elements
            //don't have generatezoiactions.
            if (DepictionAccess.InteractionsLibrary != null)
            {
                var inters = DepictionAccess.InteractionsLibrary;
                foreach (var inter in inters.MergedInteractions)
                {
                    if (inter.SubscriberTriggerPropertyNames.Contains("Position")) return initialPrototype;
                }
            }
            //endhack
            var expectedType = initialPrototype.GetElementZOIDepictionGeometryType();
            foreach (var type in expectedType)
            {
                if (rawPrototypeGeometryType.Equals(type)) return initialPrototype;
            }

            var baseTypeByGeometry = GetElementTypeFromDepictionGeometryType(rawPrototypeGeometryType);
            var newType = DepictionAccess.ElementLibrary.GuessPrototypeFromString(baseTypeByGeometry);
            if (newType == null) return initialPrototype;
            return newType;
        }


        static public IDepictionElement CreateElementFromTypeString(string elementType)
        {
            var prototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(elementType);
            if (prototype == null) return null;
            return CreateElementFromPrototype(prototype);
        }
        static public IDepictionElement CreateElementFromPrototype(IElementPrototype rawPrototype)
        {
            return CreateElementFromPrototype(rawPrototype, string.Empty);
        }
        //THERE IS A HUGE MESS HERE!!! TODO HACK FIX ME PLEASE
        static public IDepictionElement CreateElementFromPrototype(IElementPrototype prototypeToUse, string defaultType)
        {
            IElementPrototype prototype = prototypeToUse;

            //HMmm, iffy
            if (prototypeToUse == null)
            {
                if (DepictionAccess.ElementLibrary == null) return null;
                var defaultDefaultElementType = "PointOfInterest";//ha
                prototypeToUse = DepictionAccess.ElementLibrary.GuessPrototypeFromString(defaultDefaultElementType);
            }

            if (prototypeToUse.IsRawPrototype)//Raw prototypes are missing some properties that are normally brought in from the dml
            {
                prototype = CreateCompletePrototypeFromRawPrototype(prototypeToUse, defaultType);
            }
            else
            {
                prototype = prototypeToUse;
            }
            var element = CreateDesiredTypeFromElementBase<DepictionElementParent>(prototype as ElementPrototype);

            return element;
        }

        static protected IElementPrototype CreateCompletePrototypeFromRawPrototype(IElementPrototype rawPrototype, string defaultType)
        {
            if (DepictionAccess.ElementLibrary == null) return null;
            if (!rawPrototype.IsRawPrototype) return null;
            //See if the requested type for the rawPrototype exists
            var validDefaultPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(rawPrototype.ElementType);
            var defaultDefaultElementType = "Depiction.Plugin.PointOfInterest";

            var elementTypeToUse = defaultDefaultElementType;
            if (validDefaultPrototype == null)
            {
                if (!string.IsNullOrEmpty(defaultType))
                {
                    elementTypeToUse = defaultType;
                }
                validDefaultPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString(elementTypeToUse);
            }
            if (validDefaultPrototype == null)
            {
                var errorMessage = string.Format("Unable to create {0} or the default element {1}", defaultType,
                                                 defaultDefaultElementType);
                DepictionAccess.NotificationService.DisplayMessageString(errorMessage, 3);
                return null;
            }
            //a double check to makes sure the final type matches the input to the prototype
            if (rawPrototype.ZoneOfInfluence != null)
            {
                validDefaultPrototype = MatchPrototypeToZOIType(rawPrototype.ZoneOfInfluence.DepictionGeometryType, validDefaultPrototype);
            }
            if (validDefaultPrototype == null) return null;

            //Ok so we have the complete desired type, which might not have been what we first asked for, but it should match the possible zoi data that
            //may have been provided.

            TransferAllPropertiesToDifferentPrototype(rawPrototype, validDefaultPrototype, true, true);
            var fullPrototype = validDefaultPrototype as ElementPrototype;
            if (fullPrototype == null) return validDefaultPrototype;
            fullPrototype.TransferNonPropertyAndNonActionFromPrototype(rawPrototype);
            return fullPrototype;
        }
        //The transfer methods are very very old, and need to be updated
        static public void TransferAllPropertiesToDifferentPrototype(IElementPropertyHolder fromPropertyHolder, IElementPropertyHolder toPropertyHolder,
            bool replaceExisingPropertyValues, bool replacePropAttributes)
        {
            TransferAllPropertiesToDifferentPrototype(fromPropertyHolder, toPropertyHolder, replaceExisingPropertyValues, replacePropAttributes, false);
        } //The transfer methods are very very old, and need to be updated
        static public void TransferAllPropertiesToDifferentPrototype(IElementPropertyHolder fromPropertyHolder, IElementPropertyHolder toPropertyHolder,
            bool replaceExistingProperties, bool replacePropAttributes, bool notifyPropertyChange)
        {
            var inProps = fromPropertyHolder.GetPropertyListClone();
            var targetPrototype = toPropertyHolder;
            if (targetPrototype == null) return;
            var attribReplace = replacePropAttributes;

            foreach (var property in inProps)
            {
                //Hack to make sure properties retrieved from a csv keep the attributes
                //of the original element type.
                if (property.PropertySource.Equals(PropertySource.CSV))
                {
                    attribReplace = false;
                }
                if (replaceExistingProperties)
                {
                    if (attribReplace) targetPrototype.AddPropertyOrReplaceValueAndAttributes(property, true, notifyPropertyChange);
                    else targetPrototype.AddPropertyOrReplaceValueNotAttributes(property, true, notifyPropertyChange);
                }
                else
                {
                    targetPrototype.AddPropertyIfItDoesNotExist(property, true, notifyPropertyChange);
                }
            }
        }

        static public T CreateDesiredTypeFromElementBase<T>(DepictionElementBase elementBase) where T : DepictionElementBase
        {//Does this deal with raw?

            if (elementBase == null) return null;
            var element = Activator.CreateInstance<T>();
            if (element == null) return null;
            element.ElementType = elementBase.ElementType;
            var typeDisplayName = elementBase.TypeDisplayName;
            element.TypeDisplayName = typeDisplayName;
            element.SetPropertyValue("DisplayName", typeDisplayName, false);

            if (elementBase.CreateActions != null) element.CreateActions = elementBase.CreateActions.DeepClone();
            if (elementBase.DeleteActions != null) element.DeleteActions = elementBase.DeleteActions.DeepClone();
            if (elementBase.ClickActions != null) element.ClickActions = elementBase.ClickActions.DeepClone();
            if (elementBase.GenerateZoiActions != null) element.GenerateZoiActions = elementBase.GenerateZoiActions.DeepClone();

            var sourceProperties = elementBase.GetPropertyListClone();
            var zoiString = string.Empty;
            if (elementBase.ZoneOfInfluence != null)
            {
                zoiString = elementBase.ZoneOfInfluence.ToString();
            }

            object positionLatLong = null;
            element.DisableNotification = true;
            foreach (var prop in sourceProperties)
            {
                if (prop.InternalName.Equals("zoneofInfluence", StringComparison.InvariantCultureIgnoreCase) ||
                    prop.InternalName.Equals("zoi", StringComparison.InvariantCultureIgnoreCase))
                {
                    zoiString = prop.Value.ToString();
                    // element.SetInitialPositionAndZOI(null, new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)));
                }
                else if (prop.InternalName.Equals("Position", StringComparison.InvariantCultureIgnoreCase))
                {
                    positionLatLong = prop.Value;
                }
                else
                {
                    element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                }
            }
            element.DisableNotification = false;
            if (!string.IsNullOrEmpty(zoiString) || positionLatLong != null)
            {
                var posVal = positionLatLong as ILatitudeLongitude;
                if (!string.IsNullOrEmpty(zoiString) && posVal != null )
                {
                    element.SetInitialPositionAndZOI(posVal, new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)));
                }else if (!string.IsNullOrEmpty(zoiString))
                {
                    //This should overwrite position
                    element.SetInitialPositionAndZOI(null, new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)));
                }
                else//What happens if there is no zoi and you are trying to create an element?!?!?!
                {
                    element.SetPropertyValue("Position", positionLatLong);
                    if (element is DepictionElementParent)
                    {
                        ILatitudeLongitude latLong = null;
                        if (element.GetPropertyValue("Position", out latLong) && latLong.IsValid)
                        {
                            element.SetInitialPositionAndZOI(latLong, null);
                        }
                    }
                }
            }

            if (elementBase.ImageMetadata != null)
                element.SetImageMetadataWithoutNotification(elementBase.ImageMetadata.DeepClone());
            else
            {
                element.SetImageMetadataWithoutNotification(new DepictionImageMetadata());
            }
            element.UsePermaText = elementBase.UsePermaText;
            element.UsePropertyNameInHoverText = elementBase.UsePropertyNameInHoverText;
            element.PermaText = elementBase.PermaText.DeepClone();//This might cause problems
            if (elementBase.Waypoints != null)
            {
                element.ReplaceWaypointsWithoutNotification(elementBase.Waypoints);
            }
            if (elementBase.Tags != null)
            {
                element.Tags.Clear();
                foreach (var tag in elementBase.Tags)
                {
                    element.Tags.Add(tag);
                }
            }
            return element;
        }
        static public string GetElementTypeFromDepictionGeometryType(DepictionGeometryType geomType)
        {
            switch (geomType)
            {
                case DepictionGeometryType.Polygon:
                case DepictionGeometryType.MultiPolygon:
                    return "Depiction.Plugin.LoadedPolygon";
                case DepictionGeometryType.LineString:
                case DepictionGeometryType.MultiLineString:
                    return "Depiction.Plugin.LoadedLines";
                case DepictionGeometryType.Point:
                case DepictionGeometryType.MultiPoint:
                    return "Depiction.Plugin.PointOfInterest";
            }
            return "Depiction.Plugin.PointOfInterest";//string.Empty;//what to do?!
        }
        static public string GeometryTypeToDefaultDepictionType(string geometryTypeString)
        {
            var geoType = ZoneOfInfluence.GeometryTypeFromIGeometryTypeString(geometryTypeString);
            return GetElementTypeFromDepictionGeometryType(geoType);
        }
    }
}