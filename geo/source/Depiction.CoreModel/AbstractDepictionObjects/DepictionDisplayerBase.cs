﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.Serialization;

namespace Depiction.CoreModel.AbstractDepictionObjects
{
    public abstract class DepictionDisplayerBase : IXmlSerializable, IDepictionBaseDisplayer
    {
#if DEBUG

        static public Stopwatch stopWatch;
#endif
        public event NotifyCollectionChangedEventHandler VisibleElementsChange;

        protected HashSet<string> elementIDsInDisplayer = new HashSet<string>();
        private string displayerID = Guid.NewGuid().ToString();
        private DepictionDisplayerType displayerType = DepictionDisplayerType.None;
        #region Properties

        public string DisplayerName { get; set; }
        public string DisplayerID { get { return displayerID; } private set { displayerID = value; } }
        public IDepictionBaseDisplayer BaseDisplayer
        {
            get
            {
                return this;
            }
        }
        public DepictionDisplayerType DisplayerType
        {
            get { return displayerType; }
            set { displayerType = value; }//This was protected, perhaps for good reason
        }
        public ReadOnlyCollection<string> ElementIdsInDisplayer { get { return elementIDsInDisplayer.ToList().AsReadOnly(); } }

        #endregion

        #region Methods

        public bool AddElementList(IEnumerable<IDepictionElement> elements)
        {
            if (elements == null) return false;
            var cleanList = new List<IDepictionElement>();
            foreach (var element in elements)
            {
                if (element.IsGeolocated)
                {
                    if (elementIDsInDisplayer.Add(element.ElementKey))
                    {
                        cleanList.Add(element);
                    }
                }
            }
            NotifyElementVisibleChange(cleanList, NotifyCollectionChangedAction.Add);
            return true;
        }

        public bool RemoveElementList(List<IDepictionElement> elements)
        {
            if (elements == null) return false;
            var cleanList = new List<IDepictionElement>();
            foreach (var element in elements)
            {
                //if (element.IsGeolocated)//doesn't work when shifting for geo located to ungeolocated
                //{
                if (elementIDsInDisplayer.Remove(element.ElementKey))
                {
                    cleanList.Add(element);
                }
            }
            NotifyElementVisibleChange(cleanList, NotifyCollectionChangedAction.Remove);
            return true;
        }
        public bool IsElementPresent(string elementKey)
        {
            return elementIDsInDisplayer.Contains(elementKey);
        }

        virtual public string VisibleElementCount()
        {
            return elementIDsInDisplayer.Count.ToString();
        }

        #endregion

        #region Event helper region

        protected void NotifyElementVisibleChange(List<IDepictionElement> itemsChanged, NotifyCollectionChangedAction whatHappened)
        {
            if (VisibleElementsChange != null)
            {
                VisibleElementsChange.Invoke(this, new NotifyCollectionChangedEventArgs(whatHappened, itemsChanged));
            }
        }

        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            var other = obj as DepictionDisplayerBase;
            if (other == null) return false;
            if (ReferenceEquals(this, other)) return true;
            if (!DisplayerName.Equals(other.DisplayerName)) return false;
            if (!DisplayerID.Equals(other.DisplayerID)) return false;

            if (elementIDsInDisplayer.Count != other.elementIDsInDisplayer.Count) return false;
            if (!elementIDsInDisplayer.SetEquals(other.elementIDsInDisplayer)) return false;

            return true;
        }
        #endregion

        #region Implementation of IXmlSerializable

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public abstract void DisplayerReader(XmlReader reader);
        public abstract void DisplayerWrite(XmlWriter writer);


        public void ReadXml(XmlReader reader)
        {
            if (SerializationService.UpdateProgressReport(-1, "Reading displayer") == false) return;
            if (!reader.Name.Equals("DepictionElementDisplayer")) return;

            DisplayerName = reader.GetAttribute("DisplayerName");
            DisplayerType = (DepictionDisplayerType)(Enum.Parse(typeof(DepictionDisplayerType), reader.GetAttribute("DisplayerType")));
            DisplayerID = reader.GetAttribute("DisplayerID");
            reader.Read();
            if (reader.Name.Equals("elementIDsInDisplayer"))
            {
                var idList = SerializationService.DeserializeItemList<string>("elementIDsInDisplayer", typeof(string), reader);
                elementIDsInDisplayer = new HashSet<string>();
                foreach (var element in idList)
                {
                    elementIDsInDisplayer.Add(element);
                }
            }

            DisplayerReader(reader);

            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            if (SerializationService.UpdateProgressReport(-1, "Writing displayer") == false) return;

            writer.WriteStartElement("DepictionElementDisplayer");
            writer.WriteAttributeString("DisplayerName", DisplayerName);
            writer.WriteAttributeString("DisplayerType", DisplayerType.ToString());
            writer.WriteAttributeString("DisplayerID", DisplayerID);

            SerializationService.SerializeItemList("elementIDsInDisplayer", elementIDsInDisplayer, writer);
            DisplayerWrite(writer);
            writer.WriteEndElement();
        }

        #endregion
    }
}