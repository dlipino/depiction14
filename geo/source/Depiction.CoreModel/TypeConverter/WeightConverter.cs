﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.TypeConverter
{
    public class WeightConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var measurement = (IMeasurement)value;
                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                string systemString;
                string valueString = DepictionTypeConverter.GetTheValue(value, "Weight", out systemString, null);
                if (valueString == null) return null;

                var system = MeasurementSystem.Metric;
                var scale = MeasurementScale.Normal;
               
                switch (systemString.ToLower())
                {
                    case "metric tons":
                    case "metric ton":
                    case "metric tonnes":
                    case "metric tonne":
                    case "t":
                    case "metric":
                         system = MeasurementSystem.Metric;
                         scale = MeasurementScale.Normal;
                        break;
                    case "kilograms":
                    case "kilogrammes":
                    case "kilogram":
                    case "kilogramme":
                    case "kg":
                    case "kgs"://This is to fix dlps error with abbreviations and the metric system
                    case "metricsmall":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Small;
                        break;
                    case "tons":
                    case "ton":
                    case "imperial":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Normal;
                        break;
                    case "pound":
                    case "pounds":
                    case "lb":
                    case "lbm":
                    case "#":
                    case "imperialsmall":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Small;
                        break;
                    default:
                        throw new Exception(string.Format("Weight cannot use units of \"{0}\".", systemString));
                }

                double measurementValue;
                if (double.TryParse(valueString, NumberStyles.Any, info, out measurementValue))
                    return new Weight(system,scale, measurementValue);
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}