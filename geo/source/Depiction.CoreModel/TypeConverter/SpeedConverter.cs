﻿using System;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.CoreModel.TypeConverter
{
    public class SpeedConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                var measurement = (IMeasurement) value;
                return string.Format(culture, "{0} {1}", measurement.GetCurrentSystemDefaultScaleValue(), measurement.GetCurrentSystemDefaultScaleUnits());
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                string systemString;
                string valueString = DepictionTypeConverter.GetTheValue(value, "Speed", out systemString, null);
                if (valueString == null) return null;

                var system = MeasurementSystem.Metric;
                var scale = MeasurementScale.Normal;

                switch (systemString.ToLower())
                {
                    case "kph":
                    case "km/h":
                    case "km/hour":
                    case "kilometers/h":
                    case "kilometers/hour":
                    case "kilometres/h":
                    case "kilometres/hour":
                    case "km / h":
                    case "km / hour":
                    case "kilometers / h":
                    case "kilometers / hour":
                    case "kilometres / h":
                    case "kilometres / hour":
                    case "km per h":
                    case "km per hour":
                    case "kilometers per h":
                    case "kilometers per hour":
                    case "kilometres per h":
                    case "kilometres per hour":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Normal;
                        break;
                    case "mph":
                    case "mi/h":
                    case "mi/hour":
                    case "miles/h":
                    case "miles/hour":
                    case "mi / h":
                    case "mi / hour":
                    case "miles / h":
                    case "miles / hour":
                    case "mi per h":
                    case "mi per hour":
                    case "miles per h":
                    case "miles per hour":
                    case "imperial":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Normal;
                        break;
                    case "ft/s":
                    case "ft/sec":
                    case "ft/second":
                    case "feet/s":
                    case "feet/sec":
                    case "feet/second":
                    case "ft / s":
                    case "ft / sec":
                    case "ft / second":
                    case "feet / s":
                    case "feet / sec":
                    case "feet / second":
                    case "ft per s":
                    case "ft per sec":
                    case "ft per second":
                    case "feet per s":
                    case "feet per sec":
                    case "feet per second":
                    case "imperialsmall":
                        system = MeasurementSystem.Imperial;
                        scale = MeasurementScale.Small;
                        break;
                    case "m/s":
                    case "m/sec":
                    case "m/second":
                    case "meters/s":
                    case "meters/sec":
                    case "meters/second":
                    case "metres/s":
                    case "metres/sec":
                    case "metres/second":
                    case "m / s":
                    case "m / sec":
                    case "m / second":
                    case "meters / s":
                    case "meters / sec":
                    case "meters / second":
                    case "metres / s":
                    case "metres / sec":
                    case "metres / second":
                    case "m per s":
                    case "m per sec":
                    case "m per second":
                    case "meters per s":
                    case "meters per sec":
                    case "meters per second":
                    case "metres per s":
                    case "metres per sec":
                    case "metres per second":
                    case "metricsmall":
                        system = MeasurementSystem.Metric;
                        scale = MeasurementScale.Small;
                        break;
                    default:
                        throw new Exception(string.Format("Speed cannot use units of \"{0}\".", systemString));
                }

                double measurementValue;
                if (double.TryParse(valueString, NumberStyles.Any, info, out measurementValue))
                    return new Speed(system,scale, measurementValue);
            }
            return base.ConvertFrom(context, info, value);
        }
    }
}