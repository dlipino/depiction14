using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Depiction.API;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.Service;
using GeoAPI.Geometries;

namespace Depiction.CoreModel.HelperClasses
{
    public class WorldOutlineModel
    {
        private readonly FileInfo fileInfo = new FileInfo(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ShapeData\\" + "countries.shp"));

        public List<IPolygon> IPolygonsForWorld { get; set; }

        public WorldOutlineModel()
        {
            ReadFileWithWorldOutlines(fileInfo);
        }

        protected bool ReadFileWithWorldOutlines(FileInfo file)
        {
            IPolygonsForWorld = new List<IPolygon>();
            try
            {
                //hmm can't remember why the comment is here
                var regionExtent = DepictionGeographicInfo.WorldLatLongBoundingBox;//new BoundingBox { Bottom = -90, Left = -180, Right = 180, Top = 90 };

                string gmlFilename = file.FullName;
                if (!string.IsNullOrEmpty(gmlFilename))
                {
                    var features = OsGeoFileTypeReaderToDepictionElements.GetFeaturesFromOGRReadableFile(gmlFilename, regionExtent, "Country WorldCanvas",false);
                    foreach (var feature in features)
                    {
                        if (feature.GeoApiGeometry is IMultiPolygon)
                        {
                            var multiPolys = (IMultiPolygon)feature.GeoApiGeometry;
                            foreach (IPolygon poly in multiPolys.Geometries)
                                IPolygonsForWorld.Add(poly);
                        }
                        else if (feature.GeoApiGeometry is IPolygon)
                        {
                            IPolygonsForWorld.Add((IPolygon)feature.GeoApiGeometry);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Unable to retrieve Country WorldCanvas.\n\r {0}", e.Message));
                return false;
            }
            return true;
        }
    }
}