using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Media.Imaging;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using OSGeo.GDAL;
using ProjNet.Converters.WellKnownText;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;


[assembly: InternalsVisibleTo("Depiction.UnitTests")]
namespace Depiction.CoreModel.HelperClasses
{
    public class DepictionImageElementAddingService : BaseDepictionBackgroundThreadOperation
    {
        private readonly string defaultElementType;
        private IDepictionElement element;


        public DepictionImageElementAddingService(string defaultElementType)
        {
            this.defaultElementType = defaultElementType;
        }

        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            //var elementType = "Depiction.Plugin.Image";
            
            var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(defaultElementType);
            var elementModel = ElementFactory.CreateElementFromPrototype(prototype);
            element = elementModel;
            return true;
        }

        protected override object ServiceAction(object args)
        {
            try
            {
                var fileName = args as string;
                if (string.IsNullOrEmpty(fileName)) return null;

                if (!File.Exists(fileName)) return null;

                //Hack for now, mostly because the bitmpasaveload service is a mess
                //Start reading in geo aligned images using gdal
                var geoMetadata = GetGeoDataForImageUsingGDAL(fileName);


                string partialName;
                var imageWasAdded = BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(fileName, out partialName);
                if (imageWasAdded)
                {
                    if (geoMetadata != null) return geoMetadata;
                    DepictionImageMetadata metadata = new DepictionImageMetadata(partialName, "File");
                    var worldFileMetadata = GetImageMetadataIfIsWorldFile(fileName, partialName);
                    if (worldFileMetadata != null)
                    {
                        metadata = worldFileMetadata;
                    }
                    return metadata;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        #region GDAL image loading methods, only tries to find geo information, rather only returns the geolocation information
        #region Transform  stuff, should be more global
        const string popularVisualisationWKT = "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
                                                        "GEOGCS[\"Popular Visualisation CRS\",  " +
                                                            "DATUM[\"WGS84\",    " +
                                                                "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
                                                            "AUTHORITY[\"EPSG\",\"6055\"]], " +
                                                       "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
                                                       "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
                                                       "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
                                                   "PROJECTION[\"Mercator\"]," +
                                                   "PARAMETER[\"semi_minor\",6378137]," +
                                                   "PARAMETER[\"False_Easting\", 0]," +
                                                   "PARAMETER[\"False_Northing\", 0]," +
                                                   "PARAMETER[\"Central_Meridian\", 0]," +
                                                   "PARAMETER[\"Latitude_of_origin\", 0]," +
                                                   "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
                                                   "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
                                                   "AUTHORITY[\"EPSG\",\"3785\"]]";

        const string geoWKT =
               "GEOGCS[\"GCS_WGS_1984\"," +
                   "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                   "PRIMEM[\"Greenwich\",0]," +
                   "UNIT[\"Degree\",0.0174532925199433]]";
        IProjectedCoordinateSystem visualizationCS = CoordinateSystemWktReader.Parse(popularVisualisationWKT) as IProjectedCoordinateSystem;

        IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geoWKT) as IGeographicCoordinateSystem;
        CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();

        private ILatitudeLongitude GeoCanvasToWorld(Point pointCart, ICoordinateTransformation transformer)
        {
            var pointLatLong = transformer.MathTransform.Inverse().Transform(new[] { pointCart.X, pointCart.Y });
            return new LatitudeLongitude(pointLatLong[1], pointLatLong[0]);
        }
        #endregion
        private DepictionImageMetadata GetGeoDataForImageUsingGDAL(string fileName)
        {
            DepictionImageMetadata metadata = null;
            var s = Path.GetExtension(fileName);
            if (s != null)
            {
                var extension = s.ToLowerInvariant();
                switch (extension)
                {
                    case ".tiff":
                    case ".tif":
                        var reloadTiff = Gdal.Open(fileName, Access.GA_ReadOnly);
                        var outTrans = new double[6];
                        reloadTiff.GetGeoTransform(outTrans);
                        var width = reloadTiff.RasterXSize;
                        var height = reloadTiff.RasterYSize;
                        var projection = reloadTiff.GetProjectionRef();
                        var proj = reloadTiff.GetProjection();
                        visualizationCS = CoordinateSystemWktReader.Parse(projection) as IProjectedCoordinateSystem;
                        var transformer = ctfac.CreateFromCoordinateSystems(geoCS, visualizationCS);

                        var bounds = ConvertGeoAffineTransfomrToMercatorCoordBounds(outTrans, width, height, transformer);
                        metadata = new DepictionImageMetadata
                        {
                            ImageFilename = Path.GetFileName(fileName),
                            RetrievedFrom = "File",
                            TopLeftLatitude = bounds.TopLeft.Latitude,
                            TopLeftLongitude = bounds.TopLeft.Longitude,
                            BottomRightLatitude = bounds.BottomRight.Latitude,
                            BottomRightLongitude = bounds.BottomRight.Longitude
                        };
                        break;
                }
            }
            return metadata;
        }

        private IMapCoordinateBounds ConvertGeoAffineTransfomrToMercatorCoordBounds(double[] affineTransform,double widthPix,double heightPix, ICoordinateTransformation coordTransformer)
        {
            //    Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)
            //    Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)

            var topLeftPoint = new Point(affineTransform[0], affineTransform[3]);
//            double pixelWidth = Math.Abs(widthDis / imagePixWidth);
//            double pixelHeight = Math.Abs(heightDis / imagePixHeight);
            double widthDis = widthPix*affineTransform[1];
            double heightDis = heightPix*(affineTransform[5]);

            double BRX = widthDis + topLeftPoint.X;
            double BRY = heightDis + topLeftPoint.Y;

            var bottomLeftPoint = new Point(BRX, BRY);
//            var widthDis = bottomRight.X - topleft.X;
//            var heightDis = bottomRight.Y - topleft.Y;

            var topLeftCart = GeoCanvasToWorld(topLeftPoint, coordTransformer);
            var botRightCart = GeoCanvasToWorld(bottomLeftPoint, coordTransformer);

            return new MapCoordinateBounds(topLeftCart,botRightCart);
        }
        #endregion
        #region helpers for world files

        private DepictionImageMetadata GetImageMetadataIfIsWorldFile(string fullFileName, string imageResourceName)
        {
            //See if it has something that makes it a world file
            var extension = Path.GetExtension(fullFileName);
            var origName = Path.GetFileNameWithoutExtension(fullFileName);
            //.tfw files
            if (!string.IsNullOrEmpty(extension))
            {
                var path = Path.GetDirectoryName(fullFileName);

                var worldFile = string.Empty;
                //                        var wfExtension = GetWorldFileExtension(extension);
                //                        var worldFile = origName + wfExtension;
                //                        var fullWorldFile = Path.Combine(path, worldFile);
                //I suck at regex so brute force
                var allFiles = Directory.GetFiles(path);
                foreach (var file in allFiles)
                {
                    if (Path.GetExtension(file).EndsWith("w", StringComparison.InvariantCultureIgnoreCase))
                    {
                        var tempName = Path.GetFileName(file);
                        if (tempName.StartsWith(origName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            worldFile = file;
                            break;
                        }
                    }
                }

                //read the world file and transform the image
                //Works only for UTM
                var worldFileParames = ReadTFWFileParams(worldFile);
                if (worldFileParames == null) return null;

                // Somewhat arbitrarily, ignore anything after the sixth line.  This
                // is primarily so that trailing linefeeds will be ignored, but it will
                // also ignore anything else.  The ESRI spec for world files
                // merely says, "The world file... contains the following lines..." 
                // http://support.esri.com/index.cfm?fa=knowledgebase.techarticles.articleShow&d=17489
                if (worldFileParames.Length >= 6)
                {
                    var image = GetImageFromResource(imageResourceName);
                    if (image == null) return null;

                    var width = image.PixelWidth;
                    var height = image.PixelHeight;
                    return CreateImageMetdataFromWorldFileParams(imageResourceName, width, height, worldFileParames);
                }
            }
            return null;
        }

        private BitmapSource GetImageFromResource(string imageKey)
        {
            var app = Application.Current as IDepictionApplication;
            if (app != null)
            {
                var currentDepiction = app.CurrentDepiction;
                if (currentDepiction != null)
                {
                    var currentDepictionImageResources = currentDepiction.ImageResources;
                    if (currentDepictionImageResources.Contains(imageKey))
                    {
                        return currentDepictionImageResources[imageKey] as BitmapSource;
                    }
                }
            }
            return null;
        }
        static internal string[] ReadTFWFileParams(string worldFile)
        {
            if (File.Exists(worldFile))
            {
                //read the world file and transform the image
                //Works only for UTM
                string[] parameters = File.ReadAllLines(worldFile);
                return parameters;
            }
            return null;
        }
        static internal DepictionImageMetadata CreateImageMetaDataFromCenterAndWorldFileParams(string imageName, double imagePixelWidth, double imagePixelHeight,
            string[] tfwParameters, ILatitudeLongitude center)
        {
            int utmZone = center.ConvertToUTM().UTMZoneNumber;

            //This does not deal with rotation
            //top left UTM coordinate
            var topLeftPosition = new UTMLocation
            {
                EastingMeters = Double.Parse(tfwParameters[4]),
                NorthingMeters = Double.Parse(tfwParameters[5]),
                UTMZoneNumber = utmZone
            };
            var topLeftGeoLocation = topLeftPosition.UTMToLatLong();
            //bottomRight UTM coordinate
            var bottomRightPosition = new UTMLocation
            {
                EastingMeters = Double.Parse(tfwParameters[4]) + Double.Parse(tfwParameters[0]) * imagePixelWidth,
                NorthingMeters = Double.Parse(tfwParameters[5]) + Double.Parse(tfwParameters[3]) * imagePixelHeight,
                UTMZoneNumber = utmZone
            };
            var bottomRightGeoLocation = bottomRightPosition.UTMToLatLong();

            var metadata = new DepictionImageMetadata
            {
                ImageFilename = imageName,
                TopLeftLatitude = topLeftGeoLocation.Latitude,
                TopLeftLongitude = topLeftGeoLocation.Longitude,
                BottomRightLatitude = bottomRightGeoLocation.Latitude,
                BottomRightLongitude = bottomRightGeoLocation.Longitude
            };
            return metadata;
        }
        private DepictionImageMetadata CreateImageMetdataFromWorldFileParams(string imageName,
            double imagePixelWidth, double imagePixelHeight, string[] parameters)
        {
            if (DepictionAccess.CurrentDepiction == null) return null;

            //The utm zone is not give in the world file descriptor so we assume the image is in the same
            //utm zone as the region

            var region = DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
            var center = region.Center;
            return CreateImageMetaDataFromCenterAndWorldFileParams(imageName, imagePixelWidth, imagePixelHeight,
                                                                   parameters, center);


        }
        #endregion

        protected override void ServiceComplete(object args)
        {
            var createdMetadata = args as DepictionImageMetadata;

            if (createdMetadata == null)
            {
                var message = new DepictionMessage(string.Format("Image reading exception for image."), 6);
                DepictionAccess.NotificationService.DisplayMessage(message);
                return;
            }
            var partialName = createdMetadata.DepictionImageName.Path;

            element.SetPropertyValue("DisplayName", partialName);
            element.SetImageMetadata(createdMetadata);
            if (element.ElementType.Equals("Depiction.Plugin.Image2"))
            {

                string labelHTML = string.Format("<img src=\"{0}\">", partialName);
                element.AddPropertyOrReplaceValueNotAttributes(new DepictionElementProperty("Label", "Label", labelHTML));
                element.GetPropertyByInternalName("Label").IsHoverText = true;
                element.UseEnhancedPermaText = true;
                element.UsePermaText = true;
                element.Position = DepictionAccess.CurrentDepiction.ViewportBounds.Center;

            }
            var tag = Tags.DefaultFileTag + Path.GetFileName(partialName);

            element.Tags.Add(tag);

            DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(new List<IDepictionElement> { element }, true);
            if (!element.IsGeolocated)
            {
                var message = string.Format("{0} is not geolocated and can be found in Manage Content", partialName);
                DepictionAccess.CurrentDepiction.RequestElementsWithIdsViewing(new List<string> { element.ElementKey }, new[] { DepictionDialogType.ManageContentNonGeoAligned });
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
            }
        }

        #endregion
        #region old stuff for geo-aligned files
        //           if (UIAccess.ImageFileTypes.Contains(fileInfo.Extension))
        //            {
        //                //see if there's a world file
        //                string extension = fileInfo.Extension;
        //                int length = lowercaseFilePath.IndexOf(extension);
        //                var fileprefix = lowercaseFilePath.Substring(0, length);
        //                var worldFileName = fileprefix + extension + "w";
        //                if (!File.Exists(worldFileName))
        //                {
        //                    //try the alternate naming convention for world file -- jgw for JPEG and tfw for TIF
        //                    worldFileName = fileprefix + GetWorldFileExtension(extension);
        //
        //                }
        //                if (File.Exists(worldFileName))
        //                {
        //                    //read the world file and transform the image
        //                    //Works only for UTM
        //                    string[] parameters = File.ReadAllLines(worldFileName);
        //                    // Somewhat arbitrarily, ignore anything after the sixth line.  This
        //                    // is primarily so that trailing linefeeds will be ignored, but it will
        //                    // also ignore anything else.  The ESRI spec for world files
        //                    // merely says, "The world file... contains the following lines..." 
        //                    // http://support.esri.com/index.cfm?fa=knowledgebase.techarticles.articleShow&d=17489
        //                    if (parameters != null && parameters.Length >= 6)
        //                    {
        //                        imageMetadata = GeoreferenceImage(lowercaseFilePath, parameters);
        //                    }
        //                }
        //
        //                CommandResult result = addImageToMenu(fileInfo, imageMetadata, newPath);
        //                if (result.Succeeded && !imageMetadata.IsGeoReferenced)
        //                    DepictionAccess.NotificationService.SendNotification(
        //                        "To geo-align this image, please go to the Manage Content control's \"elements to geo-align\" tab.");
        //
        //                return result;
        //            }

        //        private static ImageObjectMetadata GeoreferenceImage(string filePath, string[] parameters)
        //        {
        //            var center = CommonData.Story.RegionExtent.Center;
        //            int utmZone = center.ConvertToUTM().UTMZoneNumber;
        //            Image image;
        //            try
        //            {
        //                image = Image.FromFile(filePath);
        //            }
        //            catch (Exception)
        //            {
        //                return null;
        //            }
        //
        //            var imageWidth = image.Width;
        //            var imageHeight = image.Height;
        //
        //            //top left UTM coordinate
        //            var topLeftPosition = new UTMLocation
        //            {
        //                Easting = Double.Parse(parameters[4]),
        //                Northing = Double.Parse(parameters[5]),
        //                UTMZoneNumber = utmZone
        //            };
        //            var topLeftGeoLocation = topLeftPosition.UTMToLatLong();
        //            //bottomRight UTM coordinate
        //            var bottomRightPosition = new UTMLocation
        //            {
        //                Easting = Double.Parse(parameters[4]) + Double.Parse(parameters[0]) * imageWidth,
        //                Northing = Double.Parse(parameters[5]) + Double.Parse(parameters[3]) * imageHeight,
        //                UTMZoneNumber = utmZone
        //            };
        //            var bottomRightGeoLocation = bottomRightPosition.UTMToLatLong();
        //
        //            var metadata = new ImageObjectMetadata
        //            {
        //                ImageFilename = filePath,
        //                TopLeftLatitude = topLeftGeoLocation.Latitude,
        //                TopLeftLongitude = topLeftGeoLocation.Longitude,
        //                BottomRightLatitude = bottomRightGeoLocation.Latitude,
        //                BottomRightLongitude = bottomRightGeoLocation.Longitude,
        //                IsGeoReferenced = true
        //            };
        //            return metadata;
        //        }
        //
        //        private static string GetWorldFileExtension(string extension)
        //        {
        //            string first = extension.Substring(1, 1);
        //            string last = extension.Substring(extension.Length - 1, 1);
        //            //construct the world file name extension
        //            string filename = "." + first + last + "w";
        //            return filename;
        //        }
        #endregion
    }
}