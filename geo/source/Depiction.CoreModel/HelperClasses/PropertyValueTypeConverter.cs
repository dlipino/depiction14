using System;
using System.ComponentModel;
using Depiction.CoreModel.HelperClasses;

namespace Depiction.CoreModel.HelperClasses
{
    //BAD BAD BAD, there are competing converters, one works for element sent by email and the other , does other stuff i guess
    public class PropertyValueTypeConverter
    {
        public static object ConvertType(string valueString)
        {
            var typeAndValue = ParseTypeAndValue(valueString);
            if (typeAndValue == null)
                return valueString;
            Type outType = DepictionCoreTypes.FindType(typeAndValue[0]);
            if (outType == null)
                outType = Type.GetType(typeAndValue[0]);
            if (outType == null)
                return valueString;
            var result = DoConversion(typeAndValue[1], outType, null);
            if (result == null)
                return valueString;
            return result;
        }

        public static object ConvertType(string valueString, Type expectedType, string defaultSystem)
        {
            var typeAndValue = ParseTypeAndValue(valueString);
            object result;
            if (typeAndValue == null)
                result = DoConversion(valueString, expectedType, defaultSystem);
            else
                result = DoConversion(typeAndValue[1], expectedType, null);
            if (result == null)
                return valueString;
            return result;
        }

        /// <summary>
        /// Return null if could not do the conversion.
        /// </summary>
        /// <param name="newValueString"></param>
        /// <param name="expectedType"></param>
        /// <param name="defaultType"></param>
        /// <returns></returns>
        private static object DoConversion(string newValueString, Type expectedType, string defaultType)
        {
            if (expectedType != null)
            {
                System.ComponentModel.TypeConverter converter = TypeDescriptor.GetConverter(expectedType);
                try
                {
                    var convertedValue = converter.ConvertFromInvariantString(newValueString);
                    if (convertedValue == null)
                    {
                        newValueString += " " + defaultType;
                        convertedValue = converter.ConvertFromInvariantString(newValueString);
                    }
                    if (convertedValue != null)
                        return convertedValue;
                }
                    // ReSharper disable EmptyGeneralCatchClause
                catch
                    // ReSharper restore EmptyGeneralCatchClause
                {
                    //fail silently if conversion fails
                }
            }
            return null;
        }

        private static string[] ParseTypeAndValue(string valueString)
        {
            valueString = valueString.Trim();
            var startBracketLocation = valueString.IndexOf('[');
            var endBracketLocation = valueString.IndexOf(']');
            if (startBracketLocation == 0 && endBracketLocation > 0)
            {
                var typeString = valueString.Substring(startBracketLocation + 1, endBracketLocation - 1);
                var newValueString = valueString.Substring(endBracketLocation + 1).Trim();
                return new[] { typeString, newValueString };
            }
            return null;
        }
    }
}