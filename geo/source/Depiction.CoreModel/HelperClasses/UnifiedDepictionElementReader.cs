using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.TypeConverter;
using Depiction.Serialization;

namespace Depiction.CoreModel.HelperClasses
{//This class is still a bit shacky, actually most of the xml reading is shacky, as in it doesn't recover well from errors in the xml
    public class UnifiedDepictionElementReader
    {
        public const string ElementFileIsInvalid = "Element file: {0} is invalid. This type of element will not be available until its element file is fixed. Please either delete it or correct it.";

        private readonly static object fileAccessLock = new object();//oh man, i wonder if this will ever be needed

        public static IElementPrototype CreateElementPrototypeFromDMLFile(string fileName)
        {
            IElementPrototype elemPrototype = null;
            try
            {
                using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = SerializationService.GetXmlReader(stream))
                    {
                        reader.MoveToContent(); //Not sure why this has to be here

                        elemPrototype = (ElementPrototype) Activator.CreateInstance(typeof (ElementPrototype));
                        elemPrototype.ReadXml(reader);

                    }
                }
            }catch(Exception ex)
            {
                var errorMessage = string.Format("{0} is not a valid dml file", Path.GetFileName(fileName));
                DepictionAccess.NotificationService.DisplayMessageString(errorMessage,10);
                return null;
            }
            //Hack for icon
            var fixer = elemPrototype as DepictionElementBase;
            if(fixer != null) fixer.FixIconPathType();
            //endhack
            return elemPrototype;
        }


        public static SerializableDictionary<string, string[]> ReadPostSetActions(XmlReader reader)
        {
            var actionList = new SerializableDictionary<string, string[]>();
            if (!reader.Name.Equals("postSetActions")) return actionList;

            reader.ReadStartElement("postSetActions");
            while (reader.Name.Equals("action"))
            {
                var name = reader.GetAttribute("name");
                reader.ReadStartElement("action");
                var parameters = new List<string>();
                while (reader.Name.Equals("parameters"))
                {
                    reader.ReadStartElement("parameters");
                    while (reader.Name.Equals("parameter"))
                    {
                        var param = string.Empty;
                        var query = reader.GetAttribute("elementQuery");
                        var queryValue = reader.GetAttribute("value");
                        if(!string.IsNullOrEmpty(query))
                        {
                            param += query;
                        }
                        if(!string.IsNullOrEmpty(queryValue))
                        {
                            param += (":" + queryValue);
                        }
                        parameters.Add(param);
                        reader.Read();
                    }
                    reader.ReadEndElement();
                }
                actionList.Add(name, parameters.ToArray());
                reader.ReadEndElement();
            }
            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("postSetActions", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra");
                reader.Read();
            }
            reader.ReadEndElement();
            return actionList;
        }
//         public static List<IValidationRule> ReadValidationRules(XmlReader reader)
//         {
//             return ReadValidationRules(reader, string.Empty);
//         }
        public static List<IValidationRule> ReadValidationRules(XmlReader reader,string simpleExpectedType)
        {
            if (string.IsNullOrEmpty(simpleExpectedType)) simpleExpectedType = "String";

            var validationRules = new List<IValidationRule>();
            if (!reader.Name.Equals("validationRules")) return validationRules;
            reader.ReadStartElement("validationRules");
            while (reader.Name.Equals("validationRule"))
            {
                if (!reader.IsEmptyElement)
                {
                    var parameters = new List<object>();
                    var readRuleType = reader.GetAttribute("type");


                    var ruleType = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(readRuleType);
                    if (ruleType == null)
                    {
                        //try older types
                        ruleType = DepictionCoreTypes.FindType(readRuleType);
                        if (ruleType == null)
                        {//Man this is bad if it gets here
                            ruleType = DepictionTypeConverter.Get12ObjectTypeFromStringValue(readRuleType);
                        }
                    }

                    reader.ReadStartElement("validationRule");
                    if (reader.Name.Equals("parameters"))
                    {
                        if (reader.IsEmptyElement)
                        {
                            reader.Read();
                        }else
                        {
                            reader.ReadStartElement("parameters");

                            while (reader.Name.Equals("parameter"))
                            {
                                object value = reader.GetAttribute("value");
                                var typeString = reader.GetAttribute("type");

                                //Fix for a bug in 1.3.1.11445
                                if (typeString.Equals("ValidationRuleParameter"))
                                {
                                    try
                                    {
                                        //Create an actual type (if it is a real type name)
                                        var type = Type.GetType(value.ToString());
                                        //if it gets this far it is a runtime type
                                        if (type != null)
                                        {
                                            typeString = "RuntimeType";
                                        }
                                        else
                                        {
                                            //We know the second non runtime type of datatypevalidation rule is a string and not the type of
                                            //the property it is checking
                                            if (readRuleType.Equals("DataTypeValidationRule"))
                                            {
                                                typeString = "String";
                                            }
                                            else
                                            {
                                                typeString = simpleExpectedType;
                                            }
                                        }
                                        //Actually the above is kind of redundent with what happens on a runtimetype
                                        //but that is mostly due to legacy code

                                    }
                                    catch
                                    {

                                    }
                                }

                                if (typeString.Contains("RuntimeType"))
                                {
                                    //Used for type validator
                                    var valString = value.ToString();
                                    var result = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(valString);
                                    if (result == null)
                                    {
                                        result = DepictionCoreTypes.FindType(valString);
                                        if (result == null)
                                        {//Man this is bad if it gets here

                                            //try older types
                                            result = DepictionTypeConverter.Get12ObjectTypeFromStringValue(valString);
                                        }
                                    }
                                    parameters.Add(result);
                                }
                                else
                                {
                                    var type = DepictionTypeInformationSerialization.GetFullTypeFromSimpleTypeString(typeString);
                                    if (type == null)
                                    {
                                        type = DepictionCoreTypes.FindType(typeString);
                                        if (type == null)
                                        {//Man this is bad if it gets here

                                            type = DepictionTypeConverter.Get12ObjectTypeFromStringValue(typeString);
                                        }
                                    }
                                    if (type != null)
                                    {
                                        value = DepictionTypeConverter.ChangeType(value, type);
                                        parameters.Add(value);
                                    }
                                }
                                reader.Read();
                            }
                            reader.ReadEndElement();
                        }
                    }
                    var rule = (IValidationRule)Activator.CreateInstance(ruleType, parameters.ToArray());
                    if(rule.IsRuleValid) validationRules.Add(rule);
                }
                reader.ReadEndElement();
            }

            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("validationRules", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra");
                reader.Read();
            }
            reader.ReadEndElement();
            return validationRules;
        }

        public static SerializableDictionary<string, string[]> ReadElementActions(XmlReader reader, string actionElementName)
        {
            if (!reader.Name.Equals(actionElementName, StringComparison.InvariantCultureIgnoreCase))
                return new SerializableDictionary<string, string[]>();

            var dictionary = new SerializableDictionary<string, string[]>();
            var nodeName = reader.Name;
            reader.ReadStartElement(nodeName);
            if(reader.Name.Equals("actions"))
            {
                reader.Read();
                while(reader.Name.Equals("action"))
                {
                    var keyValue = ReadActionKeyValuePair(reader);
                    if (keyValue.Key != null)
                    {
                        dictionary.Add(keyValue.Key, keyValue.Value);
                    }
                }
                reader.ReadEndElement();
            }
            reader.ReadEndElement();
            return dictionary;
        }

        private static KeyValuePair<string, string[]> ReadActionKeyValuePair(XmlReader reader)
        {
            if(!reader.Name.Equals("action")) return new KeyValuePair<string, string[]>();
            var key = reader.GetAttribute("name");
            if(reader.IsEmptyElement)
            {
                reader.Read();
                return new KeyValuePair<string, string[]>(key,new string[0]);
            }
            reader.Read();
            var parameters = new List<string>();
            if(reader.Name.Equals("parameters"))
            {
                if (reader.IsEmptyElement)
                {
                    reader.Read();
                }
                else
                {
                    reader.Read();
                    while (reader.Name.Equals("parameter"))
                    {
                        var query = reader.GetAttribute("elementQuery");
                        if (query != null)
                        {
                            parameters.Add(query);
                        }
                        reader.Read();
                    }
                    reader.Read();
                    
                }
            }
            reader.ReadEndElement();
            if (key == null) return new KeyValuePair<string, string[]>();
            return new KeyValuePair<string, string[]>(key,parameters.ToArray());

        }
    }
}