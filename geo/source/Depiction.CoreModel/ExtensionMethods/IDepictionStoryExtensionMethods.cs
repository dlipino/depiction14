using System.Collections.Generic;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Displayers;

namespace Depiction.CoreModel.ExtensionMethods
{
    static public class IDepictionStoryExtensionMethods
    {
        public static int createRevealerCount = 0;
        public static void CreateAndAddRevealer(this IDepictionStory depictionStory, IEnumerable<IDepictionElement> elements, string revealerName,
            IMapCoordinateBounds revealerBounds)
        {
            DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(elements, false);
            var regionBounds = revealerBounds;//depictionStory.RegionBounds;

            if (revealerBounds == null || !revealerBounds.IsValid)
            {

                regionBounds = MapCoordinateBounds.GetGeoRectThatIsPercentage(depictionStory.RegionBounds, .4, null, createRevealerCount);
                createRevealerCount++;
            }
            var revealer = new DepictionRevealer(revealerName, regionBounds);
            DepictionAccess.CurrentDepiction.AddRevealer(revealer);
            revealer.AddElementList(elements);
        }
    }
}