using System.Windows.Input;
using Depiction.CoreModel.DepictionObjects.Elements;

namespace Depiction.CoreModel.ExtensionMethods
{
    //This is kind of an experiment, and in theory add-ons would be able to add commands to elements
    static public class DepictionElementExtensions
    {
        public static void AddCommandToElement(this DepictionElementParent element, ICommand command)
        {
            //Some sort of error checking should be going on here, but im not sure what kind
            if (!element.elementCommandList.Contains(command))
            {
                element.elementCommandList.Add(command);
            }
        }
        public static void RemoveCommandFromElement(this DepictionElementParent element, ICommand command)
        {
            //Some sort of error checking should be going on here, but im not sure what kind

            element.elementCommandList.Remove(command);
        }
    }
}