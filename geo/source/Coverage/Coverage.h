// TerrainLayer.h

#pragma once




#include "vtdata\ElevationGrid.h"
#include "vtdata\vtDIB.h"
#include "vtdata\Filepath.h"
#include "vtdata\Unarchive.h"


#include <limits.h>			// for SHRT_MIN
#define UNKNOWN	SHRT_MIN
#define NODATA 10000
#define DEFAULTELEVATION 0
#define HIGHELEVATION SHRT_MAX-1
#define FOREGROUND 1
#define BACKGROUND 0
#define DESIREDCOLOR 2
#define MINFLOODRESOLUTION 30 //meters in resolution below which resampling will take place to assist quick flooding

struct Point3d { float x, y, z;} ; // The scattered data points....

#include "surfgrid.h"
#include "contour.h" 


//enum from projections.cpp
enum DATUM { ADINDAN = 0, ARC1950, ARC1960, AUSTRALIAN_GEODETIC_1966,
			 AUSTRALIAN_GEODETIC_1984, CAMP_AREA_ASTRO, CAPE,
			 EUROPEAN_DATUM_1950, EUROPEAN_DATUM_1979, GEODETIC_DATUM_1949,
			 HONG_KONG_1963, HU_TZU_SHAN, INDIAN, NAD27, NAD83,
			 OLD_HAWAIIAN_MEAN, OMAN, ORDNANCE_SURVEY_1936, PUERTO_RICO,
			 PULKOVO_1942, PROVISIONAL_S_AMERICAN_1956, TOKYO, WGS_72, WGS_84,
			 UNKNOWN_DATUM = -1, NO_DATUM = -2, DEFAULT_DATUM = -3 };
namespace DepictionCoverage{



	typedef enum 
	{
		SW,
		CENTER,
		NW
	} ORIGIN;

	public ref class Coverage 
	{
		private:
			
		static bool firstTime ;
		int numRecursions;
		int blobStartX, blobStartY;
		vtElevationGrid* m_pGrid;//pointer to the class
		vtElevationGrid* m_resampledGrid;
		vtProjection* m_proj;
		vtProjection* m_projGeo;
		int m_imageWidth;
		int m_imageHeight;
		System::Collections::ArrayList^ m_contourList;
		SurfaceGrid *m_surfaceGrid;
		SurfaceGrid *m_thresholdedGrid;
		SurfaceGrid *m_backupGrid;
		bool needRefreshing;
		double m_left, m_right, m_top, m_bottom;
		float m_minHeight;
		float m_maxHeight;
		DRECT *m_boundingRect;
		DRECT *m_boundingRectGeo;

		void SetCorners(vtElevationGrid* grid, DLine2* corners);		
		float GetInterpolatedElevationValueInternal(double x, double y, bool localCS);
		void ThresholdQuikGrid(double, double);
		void ConvertImageToWorldCoordinates(System::Collections::ArrayList^ contourList, int offset);
		void ConvertImageToLatLonCoordinates(System::Collections::ArrayList^ contourList, int offset);
		void GetCurrentCornersAndHeightExtents();
		bool LineOfSightGrid(FPoint3 center, double pixelRadius, double heightTolerance,System::Collections::ArrayList^ contourList, double startAngle, double stopAngle);
		double PixelDistance(int x1, int y1, int x2, int y2);		
		double PixelDistanceSquared(int x1, int y1, int x2, int y2);

		bool IsInsideArc(int centerCol, int centerRow, int col, int row, double startAngle, double stopAngle );
		void ThreeDPoint(int col, int row, int gridWidth, int gridHeight, FPoint3 *threedpoint);
		bool InitializeBackupGrid(SurfaceGrid* grid);
		public:
		Coverage() {
						
			m_pGrid = new vtElevationGrid();
			m_resampledGrid = new vtElevationGrid();
			m_projGeo = new vtProjection();
			m_projGeo->SetGeogCSFromDatum(NAD83);//NAD83
			//set m_proj to mercator
			m_proj = new vtProjection();
			m_proj->SetTextDescription("wkt","PROJCS[\"World_Mercator\",GEOGCS[\"GCS_WGS_1984\",DATUM[\"WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.017453292519943295]],PROJECTION[\"Mercator_1SP\"],PARAMETER[\"False_Easting\",0],PARAMETER[\"False_Northing\",0],PARAMETER[\"Central_Meridian\",0],PARAMETER[\"latitude_of_origin\",0],UNIT[\"Meter\",1]]");

						
		}
	    
		~Coverage()//Dispose
		{
			this->!Coverage();
		}

		!Coverage()//Finalize
		{
			if (m_pGrid != NULL)
			{
				m_pGrid->FreeData(); 
				delete m_pGrid;
			} 
			if (m_resampledGrid != NULL) 
			{ 
				m_resampledGrid->FreeData();
				delete m_resampledGrid;
			}
			delete m_surfaceGrid;
			delete m_thresholdedGrid; 
			delete m_backupGrid;
			delete m_projGeo;
			delete m_proj;
		}

		float GetInterpolatedElevationValue(double x, double y, bool localCS);
		float GetElevationFromGridCoordinates(int col, int row);
		float GetConvolvedValue(int col, int row, int kernel);
		float GetClosestElevationValue(double x, double y);
		bool SetElevationValue(double x, double y, float value);
		bool SetElevationValue(int col, int row, float value);
		bool AddElevationValue(double x, double y, float value);
		bool AddElevationValue(int col, int row, float value);
		double ResampleElevationGrid(double samplingIntervalInMeters);
		void ResampleElevationGrid( int numRows, int numCols);
		void ResampleElevationGrid( vtElevationGrid* newGrid);
		vtElevationGrid* MyCropElevationGrid(vtElevationGrid* newGrid, double topLeftX, double topLeftY, 
														double bottomRightX, double bottomRightY, int* gridWidth, int* gridHeight, bool useNewSpacing);
		vtElevationGrid* MyResampleElevationGrid(vtElevationGrid* newGrid, int* width, int* height, bool useNewSpacing);
        vtElevationGrid* TransformAndCrop(vtElevationGrid* newGrid,double topLeftX,double topLeftY,double bottomRightX,double bottomRightY,int* width,int* height);
		
		void CropElevationGrid(double topLeftX, double topLeftY, double bottomRightX, double bottomRightY);		
		IPoint2 GetImageCoordinates(double x, double y);
		IPoint2 GetImageCoordinates(double x, double y, int gridWidth, int gridHeight);
		IPoint2 GetImageCoordinatesUnsafe(double x, double y, int gridWidth, int gridHeight);
		DPoint2 GetWorldCoordinates(int col, int row, bool localCS, ORIGIN desiredOrigin);
		double GetLatitude(int col, int row,int gridWidth,int gridHeight);
		double GetLongitude(int col,int row,int gridWidth,int gridHeight);
		DPoint2 GetWorldCoordinates(int col, int row, int gridWidth, int gridHeight, bool localCS, ORIGIN desiredOrigin);
		double EstimateDegreesToMeters(double latitude);
		
		bool Create(double north, double south, double east, double west, int width, int height, bool floatgrid, float defaultValue, System::String^ projFilename,
			int wktLength);
		bool TransformCoords(vtElevationGrid** newGrid);
		bool SaveToGeoTIFF(System::String^ fileName);
		bool GenerateQuikGridFromVTPGrid(vtElevationGrid *grid, bool useGrid);
		void ConvertRasterToPolygon(double threshold,System::Collections::ArrayList^ cList);
		void FloodContours(double x, double y, double offset, double horizontalResolution, System::Collections::ArrayList^ cList);
		bool LineOfSightContours(double x, double y, double heightOffset, double range, double heightTolerance, System::Collections::ArrayList^ cList, double startAngle, double stopAngle);

		void GetCorners(System::Collections::ArrayList^ cornerPts, bool);
		int GetWidthInPixels();
		int GetHeightInPixels();
		bool HasElevation(double x, double y);
		bool HasNoData(double x, double y);
		void Erode(System::Collections::ArrayList^ se);
		void Dilate(System::Collections::ArrayList^ se);
	};



}
