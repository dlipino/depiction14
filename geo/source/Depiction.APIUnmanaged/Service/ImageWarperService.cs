using System;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using System.Drawing;
using Depiction.API.ValueTypes;
using OSGeo.GDAL;
using Color = System.Drawing.Color;
using PixelFormat = System.Drawing.Imaging.PixelFormat;


namespace Depiction.APIUnmanaged.Service
{
    /// <summary>
    /// Contains results from Projection.Warp
    /// </summary>
    public class ImageWarperService
    {
        #region Variables

        public Image Bitmap;
        public ILatitudeLongitude BottomRightPos;
        public ILatitudeLongitude TopLeftPos;

        #endregion

        #region BitmapToImageSource

        public static BitmapSource ToImageSource(Bitmap bitmap)
        {
            var hbitmap = bitmap.GetHbitmap();
            try
            {
                var imageSource = Imaging.CreateBitmapSourceFromHBitmap(hbitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromWidthAndHeight(bitmap.Width, bitmap.Height));

                return imageSource;
            }
            finally
            {
                NativeMethods.DeleteObject(hbitmap);
            }
        }
        public static Bitmap ImageSourceToBitmap(ImageSource image)
        {
            var bmSource = image as BitmapSource;
            if (bmSource == null) return null;
            MemoryStream mse = new MemoryStream();
            var e = new BmpBitmapEncoder();
            e.Frames.Add(BitmapFrame.Create(bmSource));
            e.Save(mse);
//            var bitmap = new Bitmap(mse);
            return new Bitmap(mse);
        }

        public static class NativeMethods
        {
            [DllImport("gdi32")]
            public static extern int DeleteObject(IntPtr hObject);
        }
        #endregion

        #region Methods

        [DllImport("DepictionWarp.dll")]
        private static extern void WarpFromUTMToMercator(String iFileName, String oFileName, String jpgFileName, double mppX, double mppY, double easting, double northing, int utmZone, double[] transformA, out int warpedWidth, out int warpedHeight, out bool isColorMapped, out IntPtr outBuffer, double dfErrorThreshold);
        
        public bool GetImageFromWebWarpImageGetBoundsAndSave(string url, UTMLocation utmLocation, double metersPerPixelX, double metersPerPixelY,
           out IMapCoordinateBounds warpBounds, double dfErrorThreshold,int imageWidth,int imageHeight,string outFileName )
        {
            using (var rawbitmap = ImageSaveLoadServiceNonWPF.DownloadImage(url,imageWidth,imageHeight))
            {
                using (var bitmap = WarpImage(rawbitmap, utmLocation, metersPerPixelX, metersPerPixelY, out warpBounds, dfErrorThreshold))
                {
                    return ImageSaveLoadServiceNonWPF.SaveBitmapImageToFileName(bitmap, outFileName);
                }
            }
        }

        public Bitmap WarpImage(Image bitmap, UTMLocation utmLocation, double metersPerPixelX, double metersPerPixelY,
            out IMapCoordinateBounds warpedBounds, double dfErrorThreshold)
        {
            return Warp(bitmap, metersPerPixelX, metersPerPixelY, utmLocation, out warpedBounds, dfErrorThreshold);
        }
        public Bitmap WarpImageGeoToMercator(Image bitmap, ILatitudeLongitude sourceTopLeft, double metersPerPixelX, double metersPerPixelY,
            out ILatitudeLongitude warpedTopLeft, out ILatitudeLongitude warpedBottomRight, double dfErrorThreshold)
        {
            return WarpGeo(bitmap, metersPerPixelX, metersPerPixelY, sourceTopLeft, out warpedTopLeft, out warpedBottomRight, dfErrorThreshold);
        }

        private static void MakeTransparent(string warpedImageFilename, string transparentColorString)
        {
            var imageBitmap = new Bitmap(warpedImageFilename);
            imageBitmap.MakeTransparent(Color.FromArgb(0, ColorTranslator.FromHtml(transparentColorString)));

            imageBitmap.Save(warpedImageFilename, ImageFormat.Png);
            imageBitmap.Dispose();
        }

        private static Bitmap Warp(Image inputBitmap, double metersPerPixelX, double metersPerPixelY, UTMLocation utm, out IMapCoordinateBounds warpedBounds, double dfErrorThreshold)
        {
            var transformArray = new double[6];
            try
            {
                string inputFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" + "input");
                string outputFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" + "output");
                string intermediateJpgFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" + "intermediate");

                inputBitmap.Save(inputFileName, ImageFormat.Tiff);

                int warpedWidth;
                int warpedHeight;

                try
                {
                    bool isColorMapped;
                    IntPtr bufPtr;

                    WarpFromUTMToMercator(inputFileName, outputFileName, intermediateJpgFileName, metersPerPixelX, metersPerPixelY, utm.EastingMeters, utm.NorthingMeters, utm.UTMZoneNumber, transformArray, out warpedWidth, out warpedHeight, out isColorMapped, out bufPtr, dfErrorThreshold);
                    ExtractColorMappedImage(outputFileName, intermediateJpgFileName, isColorMapped, warpedWidth, warpedHeight, bufPtr);


                    if (File.Exists(outputFileName)) File.Delete(outputFileName);

                    //WarpFromUTMToMercator, based on GDALWarp, produces a GEOTIFF output
                    //.NET cannot deal with GeoTiff
                    //So, we make WarpFrom method also produce a JPG file
                    File.Copy(intermediateJpgFileName, outputFileName);
                    //
                    //Set transparent color
                    //
                    string transparentColor = "#9acd32";
                    MakeTransparent(outputFileName, transparentColor);

                    //Top Left position of the warped image is not the same
                    //as the topleft of the unwarped image!
                    var topLeftCartesian = new UV(transformArray[0], transformArray[3]);
                    UV topLeftUV = GISCalculations.Projection.Inverse(topLeftCartesian);
                    var topLeft = new LatitudeLongitudeBase((topLeftUV.V) * 180 / Math.PI, (topLeftUV.U) * 180 / Math.PI);

                    //modify the resulting warped image's metersperpixel
                    topLeftCartesian.U = transformArray[0] + warpedWidth * transformArray[1];
                    topLeftCartesian.V = transformArray[3] - warpedHeight * transformArray[1];
                    topLeftUV = GISCalculations.Projection.Inverse(topLeftCartesian);
                    var bottomRight = new LatitudeLongitudeBase((topLeftUV.V) * 180 / Math.PI, (topLeftUV.U) * 180 / Math.PI);

                    warpedBounds = new MapCoordinateBounds(topLeft, bottomRight);
                    using (Image bmp = new Bitmap(outputFileName))
                    {
                        return new Bitmap(bmp);
                    }
                }
                finally
                {
                    File.Delete(inputFileName);
                    File.Delete(outputFileName);
                    File.Delete(intermediateJpgFileName);
                }
            }
            catch (Exception e)
            {
                throw (new ApplicationException("Unable to process image", e));
            }
        }

        private static Bitmap WarpGeo(Image inputBitmap, double metersPerPixelX, double metersPerPixelY,
            ILatitudeLongitude sourceTopLeft, out ILatitudeLongitude topLeft, out ILatitudeLongitude bottomRight, double dfErrorThreshold)
        {
            var transformArray = new double[6];
            try
            {
                string inputFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" + "input");
                string outputFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" + "output");
                string intermediateJpgFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, Guid.NewGuid() + "-" + "intermediate");

                inputBitmap.Save(inputFileName, ImageFormat.Tiff);

                int warpedWidth;
                int warpedHeight;

                try
                {
                    bool isColorMapped;
                    IntPtr bufPtr;
                    WarpFromUTMToMercator(inputFileName, outputFileName, intermediateJpgFileName, metersPerPixelX, metersPerPixelY, sourceTopLeft.Longitude, sourceTopLeft.Latitude, -1, transformArray, out warpedWidth, out warpedHeight, out isColorMapped, out bufPtr, dfErrorThreshold);
                    ExtractColorMappedImage(outputFileName, intermediateJpgFileName, isColorMapped, warpedWidth, warpedHeight, bufPtr);

                    if (File.Exists(outputFileName)) File.Delete(outputFileName);

                    //WarpFromUTMToMercator, based on GDALWarp, produces a GEOTIFF output
                    //.NET cannot deal with GeoTiff
                    //So, we make WarpFrom method also produce a JPG file
                    File.Copy(intermediateJpgFileName, outputFileName);

                    //Top Left position of the warped image is not the same
                    //as the topleft of the unwarped image!
                    var topLeftCartesian = new UV(transformArray[0], transformArray[3]);
                    UV topLeftUV = GISCalculations.Projection.Inverse(topLeftCartesian);
                    topLeft = new LatitudeLongitudeBase((topLeftUV.V) * 180 / Math.PI, (topLeftUV.U) * 180 / Math.PI);

                    //modify the resulting warped image's metersperpixel
                    topLeftCartesian.U = transformArray[0] + warpedWidth * transformArray[1];
                    topLeftCartesian.V = transformArray[3] - warpedHeight * transformArray[1];
                    topLeftUV = GISCalculations.Projection.Inverse(topLeftCartesian);
                    bottomRight = new LatitudeLongitudeBase((topLeftUV.V) * 180 / Math.PI, (topLeftUV.U) * 180 / Math.PI);

                    using (Image bmp = new Bitmap(outputFileName))
                    {
                        return new Bitmap(bmp);
                    }
                }
                finally
                {
                    File.Delete(inputFileName);
                    File.Delete(outputFileName);
                    File.Delete(intermediateJpgFileName);
                }
            }
            catch (Exception e)
            {
                throw (new ApplicationException("Unable to process image", e));
            }
        }

        private static void ExtractColorMappedImage(string outputFileName, string intermediateJpgFileName, bool isColorMapped, int warpedWidth, int warpedHeight, IntPtr bufPtr)
        {
            if (isColorMapped)
            {
                Dataset tifDS = null;
                byte[] warpedDataBuffer = new byte[warpedWidth * warpedHeight];
                //Marshal the IntPtr back to a byte array
                Marshal.Copy(bufPtr, warpedDataBuffer, 0, warpedHeight * warpedWidth);

                ColorTable ct;
                Bitmap bitmap = null;
                BitmapData bitmapData = null;

                try
                {
                    Gdal.AllRegister();
                    //open data source
                    tifDS = Gdal.Open(outputFileName, Access.GA_ReadOnly);

                    // Get the GDAL Band objects from the Dataset
                    Band band = tifDS.GetRasterBand(1);

                    int width = warpedWidth;
                    int height = warpedHeight;
                    ct = band.GetRasterColorTable();
                    if (ct == null || ct.GetPaletteInterpretation() != PaletteInterp.GPI_RGB)
                    {
                        tifDS.Dispose();
                        return;
                    }

                    // Create a Bitmap to store the GDAL image in
                    bitmap = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
                    // Obtaining the bitmap buffer
                    bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

                    int iCol = ct.GetCount();
                    ColorPalette pal = bitmap.Palette;
                    for (int i = 0; i < iCol; i++)
                    {
                        ColorEntry ce = ct.GetColorEntry(i);
                        pal.Entries[i] = Color.FromArgb(ce.c4, ce.c1, ce.c2, ce.c3);
                    }
                    bitmap.Palette = pal;

                    int stride = bitmapData.Stride;
                    IntPtr buf = bitmapData.Scan0;

                    Marshal.Copy(warpedDataBuffer, 0, buf, stride * height);
                }
                catch (Exception ex)
                {
                    throw (new ApplicationException("Unable to Extract Color Mapped Image", ex));
                }
                finally
                {
                    if (bitmap != null) bitmap.UnlockBits(bitmapData);
                    if (tifDS != null) tifDS.Dispose();
                }


                bitmap.Save(intermediateJpgFileName, ImageFormat.Jpeg);
            }
        }

        #endregion

    }
}