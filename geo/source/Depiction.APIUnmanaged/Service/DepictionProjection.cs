using System;
using System.Runtime.InteropServices;

namespace Depiction.APIUnmanaged.Service
{
    #region A helper struct
    [StructLayout(LayoutKind.Sequential)]
    public struct UV
    {
        public double U;
        public double V;

        public UV(double u, double v)
        {
            U = u;
            V = v;
        }
    }
    #endregion
    #region Some gis calc thing, really not sure what it does, or if it can be simplified
    public static class GISCalculations
    {
        private static Projection projection = null;

        public static Projection Projection
        {
            get
            {
                if (projection == null) InitializeProjection();
                return projection;
            }
        }

        private static void InitializeProjection()
        {
            //def for mercator used by VE/GE

            string[] projectionParameters = new string[] { "proj=merc", "ellps=WGS84", "lon_0=0", "k=1", "x_0=0", "y_0=0", "datum=WGS84", "units=m", "no.defs" };
            projection = new Projection(projectionParameters);
        }
    }
    #endregion
    /// <summary>
    /// C# wrapper for proj.4 projection filter
    /// http://proj.maptools.org/
    /// </summary>
    public class Projection : IDisposable
    {
        private IntPtr projPJ;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="initParameters">Proj.4 style list of options.
        /// <sample>new string[]{ "proj=utm", "ellps=WGS84", "no.defs", "zone=32" }</sample>
        /// </param>
        public Projection(string[] initParameters)
        {
            projPJ = pj_init(initParameters.Length, initParameters);
            if (projPJ == IntPtr.Zero)
                throw new ApplicationException("Projection initialization failed.");
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (projPJ != IntPtr.Zero)
            {
                pj_free(projPJ);
                projPJ = IntPtr.Zero;
            }
        }

        #endregion

        [DllImport("proj.dll")]
        private static extern IntPtr pj_init(int argc, string[] args);

        [DllImport("proj.dll")]
        private static extern string pj_free(IntPtr projPJ);

        [DllImport("proj.dll")]
        private static extern UV pj_fwd(UV uv, IntPtr projPJ);

        /// <summary>
        /// XY -> Lat/lon
        /// </summary>
        /// <param name="uv"></param>
        /// <param name="projPJ"></param>
        /// <returns></returns>
        [DllImport("proj.dll")]
        private static extern UV pj_inv(UV uv, IntPtr projPJ);

        /// <summary>
        /// Forward (Go from specified projection to lat/lon)
        /// </summary>
        /// <param name="uv"></param>
        /// <returns></returns>
        public UV Forward(UV uv)
        {
            return pj_fwd(uv, projPJ);
        }

        /// <summary>
        /// Inverse (Go from lat/lon to specified projection)
        /// </summary>
        /// <param name="uv"></param>
        /// <returns></returns>
        public UV Inverse(UV uv)
        {
            return pj_inv(uv, projPJ);
        }
    }
}