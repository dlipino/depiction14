using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.APIUnmanaged.Service;

namespace Depiction.APIUnmanaged.ValueObjects
{
    public class ImageElementInfo
    {
        #region constructor


        public ImageElementInfo(string retrievedFrom, string title)
        {
            Title = title;
            RetrievedFrom = retrievedFrom;
            ImageFormat = ImageFormat.Jpeg;
        }

        #endregion

        #region properties

        public ILatitudeLongitude TopLeft { get; private set; }
        public ILatitudeLongitude BottomRight { get; private set; }
        public string Title { get; private set; }
        public string RetrievedFrom { get; private set; }
        public Image Map { get; private set; }
        public bool IsValid { get; private set; }
        public Exception Error { get; private set; }
        public ImageFormat ImageFormat { get; set; }
        #endregion

        #region method

        public void SetMap(ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight, MemoryStream imageStream)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
            Map = new Bitmap(imageStream);
            IsValid = true;
        }

        public void Fail(Exception error)
        {
            Error = error;
            IsValid = false;
        }

        public bool SaveImageElement(string fileName)
        {
            return ImageSaveLoadServiceNonWPF.SaveBitmapImageToFileName((Bitmap)Map, fileName);
        }
        public void Crop(IMapCoordinateBounds area)
        {
            double targetWidth = Map.Width * (area.Right - area.Left) / (BottomRight.Longitude - TopLeft.Longitude);
            double targetHeight = Map.Height * (area.Top - area.Bottom) / (TopLeft.Latitude - BottomRight.Latitude);
            Crop(area, targetWidth, targetHeight);
        }
        public void Crop(IMapCoordinateBounds area, double targetWidth, double targetHeight)
        {
            double sourceWidth = Map.Width * (area.Right - area.Left) / (BottomRight.Longitude - TopLeft.Longitude);
            double sourceHeight = Map.Height * (area.Top - area.Bottom) / (TopLeft.Latitude - BottomRight.Latitude);
            double left = Map.Width * (TopLeft.Longitude - area.Left) / (BottomRight.Longitude - TopLeft.Longitude);
            double top = Map.Height * (TopLeft.Latitude - area.Top) / (TopLeft.Latitude - BottomRight.Latitude);
            Bitmap target = new Bitmap((int)Math.Round(targetWidth), (int)Math.Round(targetHeight));
            //{
            //create a new destination rectangle
            RectangleF recDest =
                new RectangleF
                    (0.0f, 0.0f, target.Width,
                     target.Height);
            //different resolution fix prior to cropping image
            //float hScale = 1.0f / (float)zoomFactor;
            //float vScale = 1.0f / (float)zoomFactor;

            RectangleF recSrc =
                //new RectangleF
                //((hd * (float)rubberBandLeft) *
                //hScale, (vd * (float)rubberBandTop) *
                //vScale, (hd * (float)rubberBand.Width) *
                //hScale, (vd * (float)rubberBand.Height) *
                //vScale);
                new RectangleF((float)left, (float)top, (float)sourceWidth, (float)sourceHeight);
            using (Graphics gfx =
                Graphics.FromImage(target))
            {
                gfx.DrawImage(Map, recDest, recSrc,
                              GraphicsUnit.Pixel);
            }
            Map = target;
            TopLeft = area.TopLeft;
            BottomRight = area.BottomRight;

        }

        #endregion
    }
}