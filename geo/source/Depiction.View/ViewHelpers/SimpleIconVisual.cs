﻿using System.Windows;
using System.Windows.Media;

namespace Depiction.View.ViewHelpers
{
    public class SimpleIconVisual : FrameworkElement
    {
        #region Dep props

        public static readonly DependencyProperty CenterPointProperty =
            DependencyProperty.Register("CenterPoint",
                typeof(Point),
                typeof(SimpleIconVisual),
                    new FrameworkPropertyMetadata(new Point(),
                        FrameworkPropertyMetadataOptions.AffectsMeasure |
                        FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty IconImageSourceProperty =
            DependencyProperty.Register("IconImageSource",
            typeof(ImageSource),
            typeof(SimpleIconVisual), 
            new FrameworkPropertyMetadata(null,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));

        public static readonly DependencyProperty IconSizeProperty =
                DependencyProperty.Register("IconSize",
                    typeof(double),
                    typeof(SimpleIconVisual),
                    new FrameworkPropertyMetadata(12.0,
                        FrameworkPropertyMetadataOptions.AffectsMeasure |
                        FrameworkPropertyMetadataOptions.AffectsRender));
        
        public static readonly DependencyProperty VisualScaleProperty =
        DependencyProperty.Register("VisualScale",
            typeof(double),
            typeof(SimpleIconVisual),
            new FrameworkPropertyMetadata(1.0,
                FrameworkPropertyMetadataOptions.AffectsMeasure |
                FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        public Point CenterPoint
        {
            set { SetValue(CenterPointProperty, value); }
            get { return (Point)GetValue(CenterPointProperty); }
        }

        public double IconSize
        {
            set { SetValue(IconSizeProperty, value); }
            get { return (double)GetValue(IconSizeProperty); }
        }

        public double VisualScale
        {
            set { SetValue(VisualScaleProperty, value); }
            get { return (double)GetValue(VisualScaleProperty); }
        }

        public ImageSource IconImageSource
        {
            get { return (ImageSource)GetValue(IconImageSourceProperty); }
            set { SetValue(IconImageSourceProperty, value); }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            return new Size(IconSize / VisualScale, IconSize / VisualScale);
        }

        protected override void OnRender(DrawingContext dc)
        {
            var finalIconSize = IconSize/VisualScale;//Not sure if it is faster to do it inline, or here
            if (IconImageSource == null)
            {
                dc.DrawEllipse(Brushes.Yellow, null,
                               new Point(CenterPoint.X, CenterPoint.Y),
                               (finalIconSize) / 2, (finalIconSize) / 2);
            }
            else
            {
                dc.DrawImage(IconImageSource, new Rect(new Point(CenterPoint.X - (finalIconSize) / 2, CenterPoint.Y - (finalIconSize) / 2),
                    new Size(finalIconSize, finalIconSize)));
            }
        }


//        #region Icon drawing helpers
//
//        private DrawingVisual CreateCircleGeomAtCenter(Point center, double shapeDim)
//        {
//
//            EllipseGeometry ellGeo = new EllipseGeometry(center, shapeDim / 2d, shapeDim / 2d);
//            ellGeo.Freeze();
//
//            DrawingVisual drawingVisual = new DrawingVisual();
//
//            using (DrawingContext dc = drawingVisual.RenderOpen())
//            {
//                dc.DrawGeometry(iconBrush, null, ellGeo);
//            }
//            return drawingVisual;
//        }
//        private DrawingVisual CreateImageVisual(Point topLeft, double shapeDim)
//        {
//            Rect drawArea = new Rect(topLeft, new Size(shapeDim, shapeDim));
//
//            DrawingVisual drawingVisual = new DrawingVisual();
//            IconImageSource.Freeze();
//            using (DrawingContext dc = drawingVisual.RenderOpen())
//            {
//                dc.DrawImage(IconImageSource, drawArea);
//            }
//            return drawingVisual;
//        }
//        #endregion
    }
}
