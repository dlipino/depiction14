﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Depiction.View.InteractionStatusIndicator
{
    /// <summary>
    /// UI element to display status of interaction rules.  Other than the
    /// specifics of the UI design (a border with text inside), the function
    /// of this class is to provide an indicator that comes on momentarily
    /// to indicate activity.  The indicator stays on for a minimum time
    /// (set by the LatchInterval property).  To use this control, instantiate
    /// it in XAML, set the value of LatchInterval, and bind LatchedIsVisible
    /// to a property that goes true when busy.  The control will be visible
    /// as long as LatchedIsVisible is true, and for a minimum of LatchInterval
    /// milliseconds after LatchedIsVisible transitions from false to true.
    /// </summary>
    public partial class InteractionsStatusIndicator
    {
        private readonly DispatcherTimer timer = new DispatcherTimer();

        public InteractionsStatusIndicator()
        {
            InitializeComponent();

            LatchInterval = 3000;  // Default
            timer.Tick += timer_Tick;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            var tmr = (DispatcherTimer)sender;
            tmr.Stop();
            Latch = false;
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property == LatchedIsVisibleProperty)
            {
                var oldValue = (bool)e.OldValue;
                var newValue = (bool)e.NewValue;
                if (newValue && !oldValue)
                {
                    Latch = true;
                    timer.Start();
                }
            }
        }


        public bool Latch
        {
            get { return (bool)GetValue(LatchProperty); }
            private set { SetValue(LatchKey, value); }
        }
        private static readonly DependencyPropertyKey LatchKey =
            DependencyProperty.RegisterReadOnly(
                "Latch",
                typeof(bool),
                typeof(InteractionsStatusIndicator),
                new UIPropertyMetadata(null));
        public static readonly DependencyProperty LatchProperty = LatchKey.DependencyProperty;

        /// <summary>
        /// When this property is set to true, the control becomes visible.  It stays visible
        /// as long as LatchedIsVisible is true, or for LatchInterval milliseconds, whichever
        /// is longer.
        /// </summary>
        public bool LatchedIsVisible
        {
            get { return (bool)GetValue(LatchedIsVisibleProperty); }
            set { SetValue(LatchedIsVisibleProperty, value); }
        }
        public static readonly DependencyProperty LatchedIsVisibleProperty =
            DependencyProperty.Register("LatchedIsVisible", typeof(bool), typeof(InteractionsStatusIndicator), new UIPropertyMetadata(false));




        public string StatusMessage
        {
            get { return (string)GetValue(StatusMessageProperty); }
            set { SetValue(StatusMessageProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StatusMessage.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StatusMessageProperty =
            DependencyProperty.Register("StatusMessage", typeof(string), typeof(InteractionsStatusIndicator), new UIPropertyMetadata(""));



        /// <summary>
        /// The latch interval in milliseconds
        /// </summary>
        public double LatchInterval
        {
            get { return timer.Interval.TotalMilliseconds; }
            set { timer.Interval = TimeSpan.FromMilliseconds(value); }
        }
    }
}