﻿using System.Windows;
using System.Windows.Input;
using Depiction.API.Properties;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModels.PrototypeViewModels;

namespace Depiction.View.QuickAddToolbar
{
    /// <summary>
    /// Interaction logic for QuickAddToolbar.xaml
    /// </summary>
    public partial class QuickAddToolbar 
    {
        public static readonly RoutedCommand QuickAddElementClickedCommand = new RoutedCommand("QuickAddElementClickedCommand", typeof(QuickAddToolbar));
        public QuickAddToolbar()
        {
            InitializeComponent();
            var binding = new CommandBinding(QuickAddElementClickedCommand, QuickAddElementClick);
            CommandBindings.Add(binding);
            IsEnabled = false;
            DataContextChanged += QuickAddToolbar_DataContextChanged;
        }

        private void QuickAddElementClick(object sender, ExecutedRoutedEventArgs e)
        {
            var content = e.Parameter as ElementPrototypeViewModel;
            if (content == null) return;

            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                var list = Settings.Default.QuickAddElements;
                var name = content.Model.ElementType;
                var simpleNameList = name.Split('.');
                var simpleName = simpleNameList[simpleNameList.Length - 1];
                var change = false;
                if(list.Contains(name))
                {
                    list.Remove(name);
                    change = true;
                }else if(  list.Contains(simpleName))
                {
                    list.Remove(simpleName);
                    change = true;
                }
                if(change)
                {
                    Settings.Default.QuickAddElements = list;
                    Settings.Default.Save();
                }
            }
            else
            {
                var fe = sender as FrameworkElement;
                if (fe == null) return;
                var dc = fe.DataContext as DepictionEnhancedMapViewModel;
                if (dc == null) return;
                dc.StartElementMouseAdd(e.Parameter as ElementPrototypeViewModel,false);
            }
        }

        void QuickAddToolbar_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                IsEnabled = true;
            }
            else
            {
                IsEnabled = false;
            }
        }
    }
}
