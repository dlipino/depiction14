﻿using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.AlertsExpander
{
    /// <summary>
    /// Interaction logic for BackgroundServiceManagerView.xaml
    /// </summary>
    public partial class BackgroundServiceManagerView
    {
        public BackgroundServiceManagerView()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    Header = "Depiction is downloading data...";
                    break;
                default:
                    break;
            }
//#if PREP
//            Header = "Depiction is downloading data...";
//#endif
        }
    }
}