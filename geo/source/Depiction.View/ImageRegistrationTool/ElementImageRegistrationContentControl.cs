﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.View.Dialogs.ElementImageRegistration;
using Depiction.View.Resources.ThumbResources;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.View.ImageRegistrationTool
{
    public class ElementImageRegistrationContentControl : ContentControl
    {

        private readonly ElementImageRegistrationHelperWindow elementImageRegistartionHelperWindow;
        //I guess this isn't the best use of this, might be ter as a UserControl, although i rally don't know.
        static ElementImageRegistrationContentControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementImageRegistrationContentControl), new FrameworkPropertyMetadata(typeof(ElementImageRegistrationContentControl)));
        }

        #region variables

        private bool isConnectedToKeyboard;
        private double sizeChange = 2;
        #endregion
        #region Constructor
        public ElementImageRegistrationContentControl()
        {
            elementImageRegistartionHelperWindow = new ElementImageRegistrationHelperWindow();
            IsVisibleChanged += ElementImageRegistrationContentControl_IsVisibleChanged;
            Loaded += ElementImageRegistrationContentControl_Loaded;
        }

        #endregion
        #region even connectors

        void ElementImageRegistrationContentControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                elementImageRegistartionHelperWindow.Owner = Application.Current.MainWindow;
            }
        }

        void ElementImageRegistrationContentControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newVal = e.NewValue;
            if (!(newVal is bool)) return;
            var boolVal = (bool) newVal;
            if(boolVal)
            {
                elementImageRegistartionHelperWindow.DataContext = DataContext;//This might only need to be set once
                elementImageRegistartionHelperWindow.Content = Content;
                elementImageRegistartionHelperWindow.Show();
                ConnectToKeyboard();
            }else
            {
                try
                {
                    elementImageRegistartionHelperWindow.Content = null;
                    elementImageRegistartionHelperWindow.DataContext = null;//not sure if this is really needed
                    elementImageRegistartionHelperWindow.Close();
                }catch{}
                DisconnectFromKeyboard();
            }
        }

        void ElementImageRegistrationContentControl_KeyDown(object sender, KeyEventArgs e)
        {
            var view = sender as ElementImageRegistrationContentControl;
            if (view == null) return;
            var viewModel = view.Content as ImageRegistrationViewModel;
            if (viewModel == null) return;
            var key = e.Key;
            if (key.Equals(Key.Up) || key.Equals(Key.Down) || key.Equals(Key.Left) || key.Equals(Key.Right) ||
                key.Equals(Key.PageUp) || key.Equals(Key.PageDown))
            {
                GeneralThumbType fakeThumb = GeneralThumbType.Main;
                var point = new Point(0, 0);
                switch(key)
                {
                    case Key.Up:
                        point = new Point(0, -sizeChange);
                        break;
                        case Key.Down:
                        point = new Point(0, sizeChange);
                        break;
                        case Key.Left:
                        point = new Point(-sizeChange, 0);
                        break;
                        case Key.Right:
                        point = new Point(sizeChange, 0);
                        break;
                        case Key.PageUp:
                        fakeThumb = GeneralThumbType.ScaleUp;
                        point = new Point(sizeChange, sizeChange);
                        break;
                        case Key.PageDown:
                        fakeThumb = GeneralThumbType.ScaleDown;
                        point = new Point(-sizeChange, -sizeChange);
                        break;
                        
                }
                RevealerThumb.ImageRegistration(viewModel,fakeThumb,point);
                e.Handled = true;
            }
        }

        void ElementImageRegistrationContentControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (IsMouseOver)
            {
                var viewModel = Content as ImageRegistrationViewModel;
                if (viewModel == null) return;
                var point = new Point(-sizeChange, -sizeChange);

                GeneralThumbType fakeThumb = GeneralThumbType.ScaleDown;
                if (e.Delta > 0)
                {
                    fakeThumb = GeneralThumbType.ScaleUp;
                    point = new Point(sizeChange, sizeChange);
                }
                RevealerThumb.ImageRegistration(viewModel, fakeThumb, point);
                e.Handled = true;
            }
        }
        #endregion
        protected void ConnectToKeyboard()
        {
            if (isConnectedToKeyboard) return;
            isConnectedToKeyboard = true;

            KeyDown += ElementImageRegistrationContentControl_KeyDown;
            MouseWheel += ElementImageRegistrationContentControl_MouseWheel;
        }

        protected void DisconnectFromKeyboard()
        {
            isConnectedToKeyboard = false;
            KeyDown -= ElementImageRegistrationContentControl_KeyDown;
            MouseWheel -= ElementImageRegistrationContentControl_MouseWheel;
        }
    }
}
