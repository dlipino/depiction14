﻿using System.Windows;
using System.Windows.Controls;
using Depiction.View.Dialogs.ElementPointRegistration;
using Depiction.View.ImageRegistrationTool;

namespace Depiction.View.ElementGeolocateTools
{
    //This is an empty control, but it hides/shows the dialog used for registering elements to a location
    //this is only around due to legacy. The better way would to have the main window just make the goelocate window appear
    //without help
    public class ElementGeolocateContentControl : ContentControl
    {
        private readonly ElementPointRegistrationHelperWindow elementPointRegistartionHelperWindow;
        //I guess this isn't the best use of this, might be ter as a UserControl, although i rally don't know.
        static ElementGeolocateContentControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementGeolocateContentControl), new FrameworkPropertyMetadata(typeof(ElementGeolocateContentControl)));
        }

        #region Constructor
        public ElementGeolocateContentControl()
        {
            elementPointRegistartionHelperWindow = new ElementPointRegistrationHelperWindow();
            IsVisibleChanged += ElementImageRegistrationContentControl_IsVisibleChanged;
            Loaded += ElementImageRegistrationContentControl_Loaded;
        }

        #endregion
        #region even connectors

        void ElementImageRegistrationContentControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                elementPointRegistartionHelperWindow.Owner = Application.Current.MainWindow;
            }
        }

        void ElementImageRegistrationContentControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newVal = e.NewValue;
            if (!(newVal is bool)) return;
            var boolVal = (bool) newVal;
            if(boolVal)
            {
                elementPointRegistartionHelperWindow.DataContext = DataContext;//This might only need to be set once
                elementPointRegistartionHelperWindow.Content = Content;
                elementPointRegistartionHelperWindow.Show();
            }else
            {
                try
                {
                    elementPointRegistartionHelperWindow.Content = null;
                    elementPointRegistartionHelperWindow.DataContext = null;//not sure if this is really needed
                    elementPointRegistartionHelperWindow.Close();
                }catch{}
                
            }
        }
        #endregion
    }
}
