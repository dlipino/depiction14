﻿using System.Windows;
using System.Linq;
using Depiction.View.Dialogs.AddElements;
using Depiction.View.Dialogs.MainWindowDialogs;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.Menu
{
    /// <summary>
    /// Interaction logic for MapMenuAndDialogCanvas.xaml
    /// </summary>
    public partial class MapMenuAndDialogCanvas
    {
        protected readonly AddContentDialog addContentDialog;
        protected readonly DisplayContentDialog displayContentDialog;
        protected readonly ManageContentDialog manageContentDialog;
        protected readonly QuickstartDataDialog quickStartDataDialog;

        protected readonly AddinDisplayerDialog addinDisplayDialog;//This one should not be here

        protected readonly TipControlView depictionTipControlDialog;

        public MapMenuAndDialogCanvas()
        {
            InitializeComponent();
            quickStartDataDialog = new QuickstartDataDialog();

            displayContentDialog = new DisplayContentDialog();
            displayContentDialog.InitialLocation = new Point(100, 80);
            manageContentDialog = new ManageContentDialog();
            manageContentDialog.InitialLocation = new Point(100, 100);

            addContentDialog = new AddContentDialog();
            addContentDialog.InitialLocation = new Point(50, 80);
            depictionTipControlDialog = new TipControlView();

            addinDisplayDialog = new AddinDisplayerDialog();

            DataContextChanged += MapMenuAndDialogCanvas_DataContextChanged;
            Loaded += MapMenuAndDialogCanvas_Loaded;
        }

        void MapMenuAndDialogCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            if (Application.Current != null && Application.Current.MainWindow != null)
            {
                addContentDialog.Owner = Application.Current.MainWindow;
                displayContentDialog.Owner = Application.Current.MainWindow;
                manageContentDialog.Owner = Application.Current.MainWindow;
                depictionTipControlDialog.Owner = Application.Current.MainWindow;
                quickStartDataDialog.Owner = Application.Current.MainWindow;

                addinDisplayDialog.Owner = Application.Current.MainWindow;
            }
        }

        void MapMenuAndDialogCanvas_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldDC = e.OldValue as DepictionMapAndMenuViewModel;
            var newDC = e.NewValue as DepictionMapAndMenuViewModel;
            if (oldDC != null)
            {
                oldDC.DisplayContentDialogViewModel.IsDialogVisible = false;
                oldDC.ManageContentDialogViewModel.IsDialogVisible = false;
                oldDC.AddContentDialogViewModel.IsDialogVisible = false;
                oldDC.TipControlDialogViewModel.IsDialogVisible = false;
                oldDC.QuickStartDialogViewModel.IsDialogVisible = false;
                oldDC.AddinDisplayerDialogViewModel.IsDialogVisible = false;

                oldDC.RequestShowElementProperty -= elementInfoShower_RequestShowElementProperty;
                oldDC.RequestShowAnnotationProperty -= annotationInfoShower_RequestShowAnnotationProperty;

                quickStartDataDialog.DataContext = null;
                quickStartDataDialog.Tag = null;

                displayContentDialog.DataContext = null;
                displayContentDialog.Tag = null;

                manageContentDialog.DataContext = null;
                manageContentDialog.Tag = null;

                addContentDialog.DataContext = null;
                addContentDialog.Tag = null;
                
                addinDisplayDialog.DataContext = null;
                addinDisplayDialog.Tag = null;

                depictionTipControlDialog.DataContext = null;
                depictionTipControlDialog.Tag = null;
                CloseAllItemCreatedDialogs();
            }
            if (newDC != null)
            {
                newDC.RequestShowElementProperty += elementInfoShower_RequestShowElementProperty;
                newDC.RequestShowAnnotationProperty += annotationInfoShower_RequestShowAnnotationProperty;

                quickStartDataDialog.DataContext = newDC.QuickStartDialogViewModel;

                displayContentDialog.DataContext = newDC.DisplayContentDialogViewModel;
                displayContentDialog.Tag = newDC;

                manageContentDialog.DataContext = newDC.ManageContentDialogViewModel;
                manageContentDialog.Tag = newDC;

                addContentDialog.DataContext = newDC.AddContentDialogViewModel;
                addContentDialog.Tag = newDC;

                addinDisplayDialog.DataContext = newDC.AddinDisplayerDialogViewModel; 
                addinDisplayDialog.Tag = newDC;


                depictionTipControlDialog.DataContext = newDC.TipControlDialogViewModel;
                //depictionTipControlDialog.Tag = newDC;
            }
        }

        bool annotationInfoShower_RequestShowAnnotationProperty(MapAnnotationViewModel annotationVM)
        {
            if (!annotationVM.EditingProperties)
            {
                var propertiesDialog = new AnnotationEditingDialog();
                //                var converter = WorldMapViewModel.StaticMainWindowToGeoCanvasConverterViewModel;
                //                propertiesDialog.StartPosition = converter.WorldToGeoCanvas(annotationVM.annotationModel.MapLocation);
                propertiesDialog.DataContext = annotationVM;
                propertiesDialog.Tag = DataContext; //Hack, more or less, to connect the main map data context
                propertiesDialog.ShowDialog();
            }
            return true;
        }
        //Not completely MVVM since there is no way to test this without the visuals around
        void elementInfoShower_RequestShowElementProperty(ElementPropertyDialogContentVM elementsToShowInfoFor)
        {
            var defaultTop = 60;
            var defaultLeft = 30;

            var newDialogDataContextID = elementsToShowInfoFor.ContentKey;
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow == null) return;

            var sameTypeList = mainWindow.OwnedWindows.OfType<ElementPropertiesDialog>().ToList();

            foreach (var control in sameTypeList)
            {
                var controlDC = control.DataContext as ElementPropertyDialogContentVM;
                if (controlDC != null)
                {
                    var key = controlDC.ContentKey;
                    if (!string.IsNullOrEmpty(key) && key.Equals(newDialogDataContextID))
                    {
                        if (!control.IsVisible)
                        {
                            control.Show();
                        }
                        control.Activate();
                        //control.DoFocusVisualStoryboard();

                        return;
                    }
                }
                if (control.Left.Equals(defaultLeft) && control.Top.Equals(defaultLeft))
                {
                    defaultLeft += 20;
                    defaultTop += 20;
                }
            }
            var propertiesDialog = new ElementPropertiesDialog();
            propertiesDialog.DataContext = elementsToShowInfoFor;
            //propertiesDialog.Content = elementsToShowInfoFor;
            propertiesDialog.Tag = DataContext;//Hack, more or less
            propertiesDialog.Top = defaultTop;
            propertiesDialog.Left = defaultLeft;
            propertiesDialog.Show();
        }

        private void CloseAllItemCreatedDialogs()
        {
            //            var propertiesControls = DialogCanvas.Children.OfType<ElementPropertiesDialogControlFrame>().ToList();
            //            foreach (var propControl in propertiesControls)
            //            {
            //                propControl.IsShown = false;
            //            }
            var mainWindow = Application.Current.MainWindow;
            if (mainWindow != null)
            {
                var sameTypeList = mainWindow.OwnedWindows.OfType<ElementPropertiesDialog>().ToList();

                foreach (var control in sameTypeList)
                {
                    control.Close();
                }
            }

            var annotationControls = DialogCanvas.Children.OfType<AnnotationEditingDialog>().ToList();
            foreach (var annotControl in annotationControls)
            {
                annotControl.Close();//.IsShown = false;
            }
        }

        //        private void AddAPropertiesWindowWithAnOffset(ElementPropertiesDialog elementPropertyDialog, string dataContextID)
        //        {
        //            var mainWindow = Application.Current.MainWindow;
        //            if (mainWindow == null) return;
        //
        //            var sameTypeList = mainWindow.OwnedWindows.OfType<ElementPropertiesDialog>().ToList();
        //
        //            foreach (var control in sameTypeList)
        //            {
        //                var controlDC = control.DataContext as ElementPropertyDialogContentVM;
        //                if (controlDC != null)
        //                {
        //                    var key = controlDC.ContentKey;
        //                    if (!string.IsNullOrEmpty(key) && key.Equals(dataContextID))
        //                    {
        //                        if (!control.IsVisible)
        //                        {
        //                            elementPropertyDialog.Show();
        //                        }
        //                        control.Activate();
        //
        //                        return;
        //                    }
        //                }
        //                var location = new Point(elementPropertyDialog.Left, elementPropertyDialog.Top);
        //
        //                if (control.Left.Equals(location.X) && control.Top.Equals(location.Y))
        //                {
        //                    elementPropertyDialog.Top = location.Y + 20;
        //                    elementPropertyDialog.Left = location.X + 20;
        //                }
        //            }
        //            elementPropertyDialog.Show();
        //        }
        //        private void AddAPropertiesDialogWithAnOffset(CommonControlFrame2 elementPropertyControl, string dataContextID)
        //        {
        //            var sameTypeList = DialogCanvas.Children.OfType<ElementPropertiesDialogControlFrame>().ToList();
        //
        //            foreach (var control in sameTypeList)
        //            {
        //                var controlDC = control.DataContext as ElementPropertyDialogContentVM;
        //                if (controlDC != null)
        //                {
        //                    var key = controlDC.ContentKey;
        //                    if (!string.IsNullOrEmpty(key) && key.Equals(dataContextID))
        //                    {
        //                        control.DoShowStoryBoard();
        //                        return;
        //                    }
        //                }
        //                var location = elementPropertyControl.CurrentLocation;
        //                if (control.CurrentLocation.Equals(location))
        //                {
        //                    var current = elementPropertyControl.CurrentLocation;
        //                    elementPropertyControl.CurrentLocation = new Point(current.X + 20, current.Y + 20);
        //                }
        //            }
        //
        //            DialogCanvas.Children.Add(elementPropertyControl);
        //        }
    }
}
