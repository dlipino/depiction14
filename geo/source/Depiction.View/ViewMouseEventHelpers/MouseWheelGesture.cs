﻿using System.Windows.Input;

namespace Depiction.View.ViewMouseEventHelpers
{
    public class MouseWheelGesture : MouseGesture
    {
        public MouseWheelAction Action { get; set; }

        public MouseWheelGesture(MouseWheelAction action)
            : base(MouseAction.WheelClick)
        {
            Action = action;
        }

        public MouseWheelGesture(MouseWheelAction action, ModifierKeys modifiers)
            : base(MouseAction.WheelClick, modifiers)
        {
            Action = action;
        }

        public override bool Matches(object targetElement, InputEventArgs inputEventArgs)
        {
            if (base.Matches(targetElement, inputEventArgs))
            {
                MouseWheelEventArgs wheelArgs = inputEventArgs as MouseWheelEventArgs;
                if (wheelArgs != null)
                {
                    if (Action == MouseWheelAction.AllMovement
                        || (Action == MouseWheelAction.WheelDown && wheelArgs.Delta < 0)
                        || Action == MouseWheelAction.WheelUp && wheelArgs.Delta > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public enum MouseWheelAction
    {
        AllMovement,
        WheelUp,
        WheelDown
    }
}