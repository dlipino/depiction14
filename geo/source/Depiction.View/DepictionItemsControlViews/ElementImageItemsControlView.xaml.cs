﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for ElementImageItemsControlView.xaml
    /// </summary>
    public partial class ElementImageItemsControlView
    {
        public ElementImageItemsControlView()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    break;
                default:
                    MouseDoubleClick += ElementImageItemsControlView_MouseDoubleClick;
                    break;
            }
//#if !IS_READER
//            MouseDoubleClick += ElementImageItemsControlView_MouseDoubleClick;
//#endif
        }

        void ElementImageItemsControlView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var elementViewModel = fe.DataContext as MapElementViewModel;
            if (elementViewModel == null) return;
            //This part is ugly, but i can't think of a good way to give the ElementViewModels the commands they need
            var parent1 = sender as FrameworkElement;
            if (parent1 == null) return;
            var parent = parent1.Parent as FrameworkElement;
            if (parent == null) return;
            var pDC = parent.DataContext as DepictionMapAndMenuViewModel;
            if (pDC == null) return;
            //hmm to geo locate or show elements
            if (!elementViewModel.ElementModel.ImageMetadata.CanBeManuallyGeoAligned || (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)))
            {
                if (elementViewModel.UseEnhancedPermaText) elementViewModel.UsePermaText = true;
                else pDC.ShowElementPropertyInfoCommand.Execute(elementViewModel);
                return;
            }
            pDC.StartGeoLocateElementCommand.Execute(new List<string> { elementViewModel.ElementKey });

        }
        public override void DoSomethingOnDisplayerTypeChange(DepictionDisplayerType oldType, DepictionDisplayerType newType)
        {
            var dataContext = DataContext as IElementDisplayerViewModel;
            if (dataContext == null) return;
            switch (newType)
            {
                case DepictionDisplayerType.MainMap:
                    Panel.SetZIndex(this, ViewModelZIndexs.BackdropImageDisplayerZ);
                    break;
                case DepictionDisplayerType.Revealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.RevealerImageDisplayerZ);
                    break;
                case DepictionDisplayerType.TopRevealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.TopRevealerImageDisplayerZ);
                    break;
            }
        }
    }
}