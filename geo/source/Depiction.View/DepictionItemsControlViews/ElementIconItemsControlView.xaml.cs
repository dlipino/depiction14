﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for ElementIconItemsControlView.xaml
    /// </summary>
    public partial class ElementIconItemsControlView
    {
        #region properties

        public Cursor ElementIconCursor
        {
            get
            {
                if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                {
                    return ((FrameworkElement)Application.Current.Resources["CursorRemove"]).Cursor;
                    //return new Cursor("pack://application:,,,/Depiction.DefaultResources;Component/Cursors/Remove.cur");//Cursors.Hand;// 
                }
                return null;
            }
        }

        #endregion
        #region Constructor
        public ElementIconItemsControlView()
        {
            InitializeComponent();
            Name = "ElementPropertyController";
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    break;
                default:
                    MouseDoubleClick += ElementIconItemsControlView_MouseDoubleClick;
                    break;
            }
//#if (!IS_READER)
//            MouseDoubleClick += ElementIconItemsControlView_MouseDoubleClick;
//#endif
        }
        #endregion
        void SetCursor(FrameworkElement fe)
        {
            if (fe == null) return;
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                var dc = fe.DataContext as IDepictionElementWaypoint;
                if(dc != null)
                {
                    if(!dc.IsDeletable)
                    {
                        fe.Cursor = null;
                        return;
                    }
                }
                fe.Cursor = ((FrameworkElement)Application.Current.Resources["CursorRemove"]).Cursor;
            }
            else
            {
                fe.Cursor = null;
            }
        }

        #region Don't really like having to do this ie semi hack
        void ElementIconItemsControlView_PreviewKeyEvent(object sender, KeyEventArgs e)
        {
            SetCursor(sender as FrameworkElement);
        }
        void ElementIconItemsControlView_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Keyboard.Focus(sender as IInputElement);
            SetCursor(sender as FrameworkElement);
        }

        #endregion

        void ElementIconItemsControlView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var parent = Parent as FrameworkElement;
            if (parent == null) return;
            var parentDC = parent.DataContext as DepictionMapAndMenuViewModel;
            if (parentDC == null) return;
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var senderDC = fe.DataContext as MapElementViewModel;
            
            if (senderDC == null)
            {
                senderDC = fe.Tag as MapElementViewModel;
                if(senderDC == null) return;
            }
            if (senderDC.UseEnhancedPermaText) senderDC.UsePermaText = true;
            else parentDC.ShowElementPropertyInfoCommand.Execute(senderDC);
        }

        public override void DoSomethingOnDisplayerTypeChange(DepictionDisplayerType oldType, DepictionDisplayerType newType)
        {
            var dataContext = DataContext as IElementDisplayerViewModel;
            if (dataContext == null) return;
            switch (newType)
            {
                case DepictionDisplayerType.MainMap:
                    Panel.SetZIndex(this, ViewModelZIndexs.BackdropIconDisplayerZ);
                    break;
                case DepictionDisplayerType.Revealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.RevealerIconDisplayerZ);
                    break;
                case DepictionDisplayerType.TopRevealer:
                    Panel.SetZIndex(this, ViewModelZIndexs.TopRevealerIconDisplayerZ);
                    break;
            }
        }

        public override void DoStuffOnScaleChange(double oldScale, double newScale)
        {
            //            foreach(IMapElementViewModel element in Items)
            //            {
            //                element.SetIconScale(newScale);
            //            }
        }

        private void ElementIconItemsControl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Debug.WriteLine("Delete the waypoint or element.");
            var fe = sender as FrameworkElement;
            var parentFe = Parent as Panel;
            if(fe == null || parentFe==null) return;

            var parentDC = parentFe.DataContext as DepictionMapAndMenuViewModel;

            var tag = fe.Tag as MapElementViewModel;
            var waypoint = fe.DataContext as IDepictionElementWaypoint;
            var elementVm = fe.DataContext as MapElementViewModel;
            if(elementVm != null)
            {
                elementVm.ElementUpdated = false;
            }
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                if(waypoint != null && tag != null)
                {
                    if (waypoint.IsDeletable)
                    {
                        tag.ElementModel.RemoveWaypoint(waypoint);
                        e.Handled = true;
                    }
                }else if(elementVm != null && parentDC != null)
                {
                    parentDC.DeleteElementCommand.Execute(new List<string> { elementVm.ElementKey });
                    e.Handled = true;
                }
            }
        }
    }

}