// ScatterPlotRender.cs by Charles Petzold, December 2008, sort of, some changes had to be made
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Media;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.View.DepictionItemsControlViews.EnhancedItemControls
{
    public class ElementZOIRender : FrameworkElement
    {
        #region Dep props
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource",
                                        typeof(ObservableCollection<MapElementViewModel>),
                                        typeof(ElementZOIRender),
                                        new PropertyMetadata(OnItemsSourceChanged));

        //Actually inverse of map scale
        public static readonly DependencyProperty VisualScaleProperty =
            DependencyProperty.Register("VisualScale",
                                        typeof(double),
                                        typeof(ElementZOIRender),
                                        new FrameworkPropertyMetadata(1.0,
                                                                      FrameworkPropertyMetadataOptions.AffectsMeasure |
                                                                      FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region static things that i don't understand yet
        static void OnItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            (obj as ElementZOIRender).OnItemsSourceChanged(args);
        }
        #endregion

        #region Constructor
        public ElementZOIRender()
        {
            IsHitTestVisible = false;//This really really helps canvas drag performance.
            Focusable = false;
        }
        #endregion

        #region properties

        public ObservableCollection<MapElementViewModel> ItemsSource
        {
            set { SetValue(ItemsSourceProperty, value); }
            get { return (ObservableCollection<MapElementViewModel>)GetValue(ItemsSourceProperty); }
        }

        public double VisualScale
        {
            set { SetValue(VisualScaleProperty, value); }
            get { return (double)GetValue(VisualScaleProperty); }
        }

        #endregion


        void OnItemsSourceChanged(DependencyPropertyChangedEventArgs args)
        {
            if (args.OldValue != null)
            {
                ObservableCollection<MapElementViewModel> coll = args.OldValue as ObservableCollection<MapElementViewModel>;
                coll.CollectionChanged -= OnCollectionChanged;
//                coll.ItemPropertyChanged -= OnItemPropertyChanged;
            }

            if (args.NewValue != null)
            {
                ObservableCollection<MapElementViewModel> coll = args.NewValue as ObservableCollection<MapElementViewModel>;
                coll.CollectionChanged += OnCollectionChanged;
//                coll.ItemPropertyChanged += OnItemPropertyChanged;
            }

            InvalidateVisual();
        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext dc)
        {
            if (ItemsSource == null)
                return;
            foreach (var mapViewModel in ItemsSource)
            {
                var borderPen = new Pen(mapViewModel.ZOI.BorderBrush, 2*VisualScale);
                if(!borderPen.IsFrozen) borderPen.Freeze();
                dc.DrawGeometry(null, borderPen, mapViewModel.ZOI.ZOIDrawGeometry);
            }
        }
    }
}