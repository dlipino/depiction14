// ScatterPlotRender.cs by Charles Petzold, December 2008, sort of, some changes had to be made

using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Windows;
using System.Windows.Media;
using Depiction.API.EnhancedTypes;
using Depiction.API.Properties;
using Depiction.ViewModels.MapShapeViewModels;

namespace Depiction.View.DepictionItemsControlViews.EnhancedItemControls
{
    public class WorldOutlineRender : FrameworkElement
    {
        const double magicNoShowScale = .6 / 100;
        #region Dep props
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource",
                                        typeof(ObservableNotifiableCollection<MapOutlineViewModel>),
                                        typeof(WorldOutlineRender),
                                        new PropertyMetadata(OnItemsSourceChanged));

        //Actually inverse of map scale
        public static readonly DependencyProperty VisualScaleProperty =
            DependencyProperty.Register("VisualScale",
                                        typeof(double),
                                        typeof(WorldOutlineRender),
                                        new FrameworkPropertyMetadata(1.0,
                                                                      FrameworkPropertyMetadataOptions.AffectsMeasure |
                                                                      FrameworkPropertyMetadataOptions.AffectsRender));
        #endregion

        #region static things that i don't understand yet
        static void OnItemsSourceChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            (obj as WorldOutlineRender).OnItemsSourceChanged(args);
        }
        #endregion

        #region Constructor
        public WorldOutlineRender()
        {
            IsHitTestVisible = false;//This really really helps canvas drag performance.
            Focusable = false;
            //            Settings.Default.SettingChanging += Default_SettingChanging;
            Settings.Default.PropertyChanged += Default_PropertyChanged;
        }

        void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("DisplayWorldOutline", StringComparison.InvariantCultureIgnoreCase))
            {
                InvalidateVisual();
            }
        }

        //Changes without saving
        //        void Default_SettingChanging(object sender, SettingChangingEventArgs e)
        //        {
        //            if(e.SettingName.Equals("DisplayWorldOutline",StringComparison.InvariantCultureIgnoreCase))
        //            {
        //                InvalidateVisual();
        //            }
        //        }
        #endregion

        #region properties

        public ObservableNotifiableCollection<MapOutlineViewModel> ItemsSource
        {
            set { SetValue(ItemsSourceProperty, value); }
            get { return (ObservableNotifiableCollection<MapOutlineViewModel>)GetValue(ItemsSourceProperty); }
        }

        public double VisualScale
        {
            set { SetValue(VisualScaleProperty, value); }
            get { return (double)GetValue(VisualScaleProperty); }
        }

        #endregion


        void OnItemsSourceChanged(DependencyPropertyChangedEventArgs args)
        {
            if (args.OldValue != null)
            {
                ObservableNotifiableCollection<MapOutlineViewModel> coll = args.OldValue as ObservableNotifiableCollection<MapOutlineViewModel>;
                coll.CollectionChanged -= OnCollectionChanged;
                coll.ItemPropertyChanged -= OnItemPropertyChanged;
            }

            if (args.NewValue != null)
            {
                ObservableNotifiableCollection<MapOutlineViewModel> coll = args.NewValue as ObservableNotifiableCollection<MapOutlineViewModel>;
                coll.CollectionChanged += OnCollectionChanged;
                coll.ItemPropertyChanged += OnItemPropertyChanged;
            }

            InvalidateVisual();
        }

        void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            InvalidateVisual();
        }

        void OnItemPropertyChanged(object sender, ItemPropertyChangedEventArgs args)
        {
            InvalidateVisual();
        }
        protected override void OnRender(DrawingContext dc)
        {
            if (ItemsSource == null)
                return;
            if (!Settings.Default.DisplayWorldOutline) return;
            if ((1 / VisualScale) > magicNoShowScale) return;
            foreach (var worldOutline in ItemsSource)
            {
                var borderPen = new Pen(worldOutline.OutlineBorderColor, 2 * VisualScale);
                if (!borderPen.IsFrozen) borderPen.Freeze();
                dc.DrawGeometry(null, borderPen, worldOutline.OutlineGeometry);
            }
        }
    }
}