﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.DepictionItemsControlViews
{
    /// <summary>
    /// Interaction logic for AnnotationItemsControl.xaml
    /// </summary>
    public partial class AnnotationItemsControl
    {
        public AnnotationItemsControl()
        {
            InitializeComponent();
        }
        #region overrides

        public override void DoStuffOnScaleChange(double oldScale, double newScale)
        {
//            foreach (MapAnnotationViewModel item in Items)//A race condition? from datacontext setting?
//            {
//                item.AdjustVisibilityBasedOnVisibleScale(newScale);
//            }
        }
        #endregion

        #region event handlers for xaml

        private void AnnotationItemsControl_PreviewDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var fe = e.OriginalSource as FrameworkElement;
            if (fe == null) return;
            var annot = fe.DataContext as MapAnnotationViewModel;
            if (annot == null) return;
            if (fe is Image)
            {
                annot.Collapsed = annot.Collapsed == Visibility.Collapsed ? Visibility.Visible : Visibility.Collapsed;
                e.Handled = true;
                return;
            }
            var parent = Parent as FrameworkElement;
            if (parent == null) return;
            var parentDC = parent.DataContext as DepictionMapAndMenuViewModel;
            if (parentDC == null) return;
            parentDC.ShowAnnotationPropertyInfoCommand.Execute(annot);

            e.Handled = true;
        }
        private void annotationResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var f = sender as FrameworkElement;
            if (f == null) return;
            var tp = f.TemplatedParent as ContentPresenter;
            if (tp == null) return;
            var annotContext = tp.DataContext as MapAnnotationViewModel;
            if (annotContext == null) return;
            annotContext.Width += e.HorizontalChange;
            annotContext.Height += e.VerticalChange;
            e.Handled = true;
        }

        private void Annotation_Drag(object sender, DragDeltaEventArgs e)
        {
            var t = sender as Thumb;
            if (t == null) return;
            var dc = t.DataContext as MapAnnotationViewModel;
            if (dc == null) return;
            dc.ContentLocationX += e.HorizontalChange;
            dc.ContentLocationY += e.VerticalChange;
        }
        #endregion
        public Dictionary<UIElement, Rect> GetVisualsForElement(IMapDraggable dragElement)
        { //This is work for annotation only, when it comes to an element with a ZOI things change a bit
            var visuals = new Dictionary<UIElement, Rect>();
            UIElement fe = null;
            Rect bounds = Rect.Empty;
            if (Items.Contains(dragElement))
            {
                fe = ItemContainerGenerator.ContainerFromItem(dragElement) as UIElement;

                var annotation = dragElement as MapAnnotationViewModel;
                //A possible alternative way of finding the icon size for the drag
                //                var scaler = 1d;
                //                var dragIconSize = element.IconSize * scaler;
                //                bounds = new Rect(new Point(), new Size(dragIconSize, dragIconSize));
                //                bounds = element.ElementIconTransform.TransformBounds(bounds);
                //                bounds.X = bounds.Width / 2;
                //                bounds.Y = bounds.Height / 2;

                if (annotation != null)
                {
                    var contPre = fe as FrameworkElement;
                    var scaler = 1d;
                    var dragIconSize = annotation.IconSize * scaler / WorldScale;
                    bounds = new Rect(new Point(-dragIconSize / 2, -dragIconSize / 2), new Size());
                    if (contPre != null)
                    {
                        bounds.Width = dragIconSize;
                        bounds.Height = dragIconSize;
                    }
                }

                if (fe != null) visuals.Add(fe, bounds);
            }
            return visuals;
        }
    }
}