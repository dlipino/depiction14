using System.Windows;
using Depiction.API.DialogBases;
using Depiction.API.MVVM;

namespace Depiction.View
{
    public class DepictionMessageBoxController : IMessageDialogController
    {
        #region Implementation of IMessageDialogController

        public void ShowNonModalDialog(string message)
        {
            DepictionMessageBox.ShowOk(message,"");
        }

        public void ShowNonModalDialog(string message, string title)
        {
            DepictionMessageBox.ShowOk(message, title);
        }

        public void ShowNonModalDialog(Window owner, string message, string title)
        {
            DepictionMessageBox.ShowOk(owner,message, title);
        }

        public MessageBoxResult ShowModalDialog(string message)
        {
            return DepictionMessageBox.ShowDialog(message);
        }

        public MessageBoxResult ShowModalDialog(string message, string title)
        {
            return DepictionMessageBox.ShowDialog(message, title);
        }
        public MessageBoxResult ShowModalDialog(string message, string title, MessageBoxButton buttonOptions)
        {
            return DepictionMessageBox.ShowDialog(message, title, buttonOptions);
        }
        public MessageBoxResult ShowModalDialog(Window owner, string message, string title)
        {
            return DepictionMessageBox.ShowDialog(owner, message, title);
        }
        public MessageBoxResult ShowModalDialog(Window owner, string message, string title,MessageBoxButton buttonOptions)
        {
            return DepictionMessageBox.ShowDialog(owner, message, title,buttonOptions);
        }

        

        #endregion
    }
}