﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using Depiction.View.ViewHelpers.Adorners;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.BaseViews
{
    abstract public class DragCanvasViewBase : Canvas
    {//I don't like having to break things up
        protected AdornerLayer adornerLayer;
        protected ElementAddAdorner elementAddAdorner;
        protected Brush DefaultFill = Brushes.BurlyWood;
        protected Brush InUseFill = Brushes.GhostWhite;

        #region Dep props
        
        public Brush QuadrantFillBrush
        {
            get { return (Brush)GetValue(QuadrantFillBrushProperty); }
            set { SetValue(QuadrantFillBrushProperty, value); }
        }

        // Using a DependencyProperty as the backing store for QuadrantFillBrush.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty QuadrantFillBrushProperty =
            DependencyProperty.Register("QuadrantFillBrush", typeof(Brush), typeof(DragCanvasViewBase), new UIPropertyMetadata(Brushes.Green));

        public double QuadrantZIndex
        {
            get { return (double)GetValue(QuadrantZIndexProperty); }
            set { SetValue(QuadrantZIndexProperty, value); }
        }

        // Using a DependencyProperty as the backing store for QuadrantZIndex.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty QuadrantZIndexProperty =
            DependencyProperty.Register("QuadrantZIndex", typeof(double), typeof(DragCanvasViewBase), new UIPropertyMetadata(0d));

        public double QuadrantOpacity
        {
            get { return (double)GetValue(QuadrantOpacityProperty); }
            set { SetValue(QuadrantOpacityProperty, value); }
        }

        // Using a DependencyProperty as the backing store for QuadrantOpacity.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty QuadrantOpacityProperty =
            DependencyProperty.Register("QuadrantOpacity", typeof(double), typeof(DragCanvasViewBase), new UIPropertyMetadata(1d));
        #endregion

        #region Constructor
        protected DragCanvasViewBase()
        {
            QuadrantZIndex = ViewModelZIndexs.BottomWorldBackGroundRectangleZ;
            QuadrantOpacity =  .3;
            Background = QuadrantFillBrush = DefaultFill;
        }
        #endregion

        protected void NormalizeVisuals()
        {
            QuadrantZIndex = ViewModelZIndexs.BottomWorldBackGroundRectangleZ;
            Background =  QuadrantFillBrush = DefaultFill;
            if (elementAddAdorner != null) elementAddAdorner.Visibility = Visibility.Hidden; 
        }
        virtual public void SetCanvasVisualsToNormal()
        {
            NormalizeVisuals();
        }
    }
}