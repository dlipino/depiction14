﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.View.ViewHelpers.Adorners;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.BaseViews
{
    public class DepictionCanvasViewBase : WorldCanvasViewBase
    {
        private const int MaxContextMenuLength = 20;
        #region variables
        private bool contextMenuEnabled;
        protected DepictionCanvasMode previousMode = DepictionCanvasMode.Uninitialized;

        #endregion

        #region Dep properties
        public DepictionCanvasMode WorldCanvasMode
        {
            get { return (DepictionCanvasMode)GetValue(WorldCanvasModeProperty); }
            set { SetValue(WorldCanvasModeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for WorldCanvasMode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WorldCanvasModeProperty =
            DependencyProperty.Register("WorldCanvasMode", typeof(DepictionCanvasMode), typeof(DepictionCanvasViewBase),
            new UIPropertyMetadata(DepictionCanvasMode.Uninitialized, WorldCanvasModeChanged));
        #endregion
        #region Constructor

        public DepictionCanvasViewBase()
        {
            EnableContextMenu();
            Background = Brushes.BurlyWood;
        }

        #endregion

        #region DP methods

        protected static void WorldCanvasModeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

            var oldValue = (DepictionCanvasMode)e.OldValue;
            var newValue = (DepictionCanvasMode)e.NewValue;
            var worldCanvas = d as DepictionCanvasViewBase;
            if (worldCanvas == null) return;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    worldCanvas.SetCanvasVisualsToNormal();
                    return;
            }
//#if IS_READER
//            worldCanvas.SetCanvasVisualsToNormal();
//            return;
//#endif
            if (oldValue == newValue) return;
            worldCanvas.previousMode = oldValue;
            //There are some issues with setting the depiction mode normal, some connections that aren't quite right
            switch (newValue)
            {
                case DepictionCanvasMode.Normal:
                    worldCanvas.SetCanvasVisualsToNormal();
                    break;
                case DepictionCanvasMode.SuspendedAdd:
                    worldCanvas.SetCanvasVisualsToSuspendAdd();
                    break;
                case DepictionCanvasMode.AnnotationAdd:
                case DepictionCanvasMode.ElementAdd:
                case DepictionCanvasMode.ZOIAdd:
                    worldCanvas.SetCanvasVisualsToNormal();
                    worldCanvas.SetCanvasVisualsToElementAdd();
                    break;
                case DepictionCanvasMode.ZOIEdit:
                    worldCanvas.SetCanvasVisualsToNormal();
                    worldCanvas.SetCanvasVisualsToEditZOI();
                    break;
            }
        }
        #endregion

        #region Mouse add and visual change methods

        private void SetCanvasVisualsToEditZOI()
        {
            DisableContextMenu();
            //            Application.Current.MainWindow.KeyDown += MainWindow_KeyEvent;
            //            Application.Current.MainWindow.KeyUp += MainWindow_KeyEvent;
            foreach (var rect in grabRects)
            {
                rect.PreviewMouseRightButtonUp += DraggableElementAddCanvas_PreviewMouseRightButtonUp;
            }
            QuadrantZIndex = ViewModelZIndexs.TopWorldBackGroundRectangleZ;
            Background = QuadrantFillBrush = InUseFill;
            QuadrantOpacity = .2;
        }

        private void SetCanvasVisualsToElementAdd()
        {
            //This shoudl be the place to determine the number of mouse clicks needed to complete an element
            DisableContextMenu();
            var context = DataContext as DepictionEnhancedMapViewModel;
            if (context == null) return;
            if (context.CurrentAddPrototype == null) return;
            //SOmething seems odd with this method
            var mouseIcon = context.CurrentAddPrototype.IconSource;
            if (elementAddAdorner == null)
            {
                elementAddAdorner = new ElementAddAdorner(this, mouseIcon, 0.7);
                adornerLayer = AdornerLayer.GetAdornerLayer(this);
                adornerLayer.Add(elementAddAdorner);
                elementAddAdorner.Visibility = Visibility.Hidden;
                Application.Current.MainWindow.KeyDown += MainWindow_KeyEvent;
                Application.Current.MainWindow.KeyUp += MainWindow_KeyEvent;

                foreach (var rect in grabRects)
                {
                    rect.PreviewMouseMove += DraggableElementAddCanvas_PreviewMouseMove;
                    rect.PreviewMouseLeftButtonDown += DraggableElementAddCanvas_PreviewMouseLeftButtonDown;
                    rect.IsMouseDirectlyOverChanged += DraggableElementAddCanvas_IsMouseDirectlyOverChanged;
                    //The up is so the contxt menus don't get shown when they shouldn't
                    rect.PreviewMouseRightButtonUp += DraggableElementAddCanvas_PreviewMouseRightButtonUp;
                    if (Mouse.DirectlyOver == rect && elementAddAdorner != null)
                    {
                        elementAddAdorner.Visibility = Visibility.Visible;
                    }
                }
            }
            else
            {
                elementAddAdorner.SetIcon(mouseIcon);
            }
            QuadrantZIndex = ViewModelZIndexs.TopWorldBackGroundRectangleZ;
            Background = QuadrantFillBrush = InUseFill;
            QuadrantOpacity = .3;

        }

        virtual public void SetCanvasVisualsToSuspendAdd()
        {
            NormalizeVisuals();
        }

        public override void SetCanvasVisualsToNormal()
        {
            NormalizeVisuals();
            Application.Current.MainWindow.KeyDown -= MainWindow_KeyEvent;
            Application.Current.MainWindow.KeyUp -= MainWindow_KeyEvent;
            if (elementAddAdorner != null)
            {
                AdornerLayer.GetAdornerLayer(this).Remove(elementAddAdorner);
                elementAddAdorner = null;
            }
            EnableContextMenu();
            foreach (var rect in grabRects)
            {
                rect.PreviewMouseMove -= DraggableElementAddCanvas_PreviewMouseMove;
                rect.PreviewMouseLeftButtonDown -= DraggableElementAddCanvas_PreviewMouseLeftButtonDown;
                rect.IsMouseDirectlyOverChanged -= DraggableElementAddCanvas_IsMouseDirectlyOverChanged;
                rect.PreviewMouseRightButtonUp -= DraggableElementAddCanvas_PreviewMouseRightButtonUp;
            }
            base.SetCanvasVisualsToNormal();
        }
        #endregion

        #region Events for adding mode

        void MainWindow_KeyEvent(object sender, KeyEventArgs e)
        {
            if (e.Key.Equals(Key.LeftShift) || e.Key.Equals(Key.RightShift))
            {
                var context = DataContext as DepictionBasicMapViewModel;
                if (context == null) return;

                if (e.IsDown) { context.WorldCanvasModeVM = DepictionCanvasMode.SuspendedAdd; }
                if (e.IsUp)
                {
                    context.WorldCanvasModeVM = previousMode;// DepictionCanvasMode.ElementAdd;
                }
            }
        }
        //Up is used to hide the context menu activation
        void DraggableElementAddCanvas_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            var context = DataContext as DepictionEnhancedMapViewModel;
            if (context == null) return;

            switch (WorldCanvasMode)
            {
                case DepictionCanvasMode.ElementAdd:
                case DepictionCanvasMode.ZOIAdd:
                case DepictionCanvasMode.AnnotationAdd:
                    context.EndElementAddCommand.Execute(null);
                    break;
                case DepictionCanvasMode.ZOIEdit:
                    context.EndZOIEditCommand.Execute(null);
                    break;
            }
            e.Handled = true;
        }

        void DraggableElementAddCanvas_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift)) return;
            var fe = sender as FrameworkElement;
            //Store mouse locations. If the element requires more than a single point find some way to draw
            //what is going on
            if (fe != null && fe.IsMouseDirectlyOver)
            {
                var context = DataContext as DepictionEnhancedMapViewModel;
                if (context == null) return;
                if (context.WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIAdd) && context.CurrentAddPrototype != null && context.CurrentEditElement != null)
                {
                    if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                    {
                        context.CurrentEditElement.ZOI.RemoveEndPoint();
                    }
                    else
                    {
                        if (e.ClickCount < 2)
                        {
                            context.CurrentEditElement.ZOI.AddPointToEnd(e.GetPosition(this));
                            context.CurrentEditElement.ZOI.UpdateModelFromViewModel();
                        }
                        else
                        {
                            context.EndElementAddCommand.Execute(null);
                        }
                    }
                }
                else
                {
                    context.AddPrototypeToLocation(context.CurrentAddPrototype, e.GetPosition(this), false);
                }
                e.Handled = true;
            }
        }

        void DraggableElementAddCanvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (elementAddAdorner != null && !e.GetPosition(this).Equals(PreviousPosition))
            {
                elementAddAdorner.UpdateAdorner();
            }
            var fe = sender as FrameworkElement;
            if (fe != null && fe.IsMouseDirectlyOver)
            {
                var context = DataContext as DepictionEnhancedMapViewModel;
                if (context != null)
                {
                    if (context.WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIAdd) &&
                        context.CurrentAddPrototype != null &&
                        context.CurrentEditElement != null)
                    {
                        bool drawClosed = false;
                        if (context.CurrentAddPrototype.ZOIShape().Equals(ZOIShapeType.UserPolygon))
                        {
                            drawClosed = true;
                        }
                        context.CurrentEditElement.ZOI.AddVisualPointToEnd(e.GetPosition(this), drawClosed);
                    }
                }
            }
            PreviousPosition = e.GetPosition(this);
        }

        void DraggableElementAddCanvas_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (elementAddAdorner == null) return;
            var fe = sender as FrameworkElement;
            if (fe != null && fe.IsMouseDirectlyOver)
            {
                var context = DataContext as DepictionBasicMapViewModel;
                if (context == null) return;
                if (context.WorldCanvasModeVM == DepictionCanvasMode.ElementAdd ||
                    context.WorldCanvasModeVM == DepictionCanvasMode.AnnotationAdd ||
                    context.WorldCanvasModeVM == DepictionCanvasMode.ZOIAdd)
                {
                    elementAddAdorner.Visibility = Visibility.Visible;
                }
            }
            else elementAddAdorner.Visibility = Visibility.Hidden;
        }

        #endregion
        #region Scale change methods
        protected override void WorldTransformChanged(bool scaleChange)
        {
            base.WorldTransformChanged(scaleChange);
            var context = DataContext as DepictionBasicMapViewModel;
            if (context == null) return;
            if (scaleChange)
            {
                context.InverseScale = 1 / context.Scale;

                foreach (var item in context.Annotations)
                //There seems to be a race condition with ItemSource, items, and when the scale get set in the items controls
                {
                    item.AdjustVisibilityBasedOnVisibleScale(context.Scale);
                }
            }
        }
        #endregion

        #region Context menu region

        private void EnableContextMenu()
        {
            if (!contextMenuEnabled)
            {
                contextMenuEnabled = true;
            }
        }

        private void DisableContextMenu()
        {
            contextMenuEnabled = false;
            ContextMenu = null;
        }
        #region context menu command helpers
        Dictionary<string, MapElementViewModel> contextMenuElements = new Dictionary<string, MapElementViewModel>();
        Dictionary<string, MapAnnotationViewModel> contextMenuAnnotations = new Dictionary<string, MapAnnotationViewModel>();

        private MenuItem CreateCheckableItem(object commandParam, string propertyName)
        {
            var element = commandParam as DepictionElementParent;
            if (element == null) return null;
            bool draggable;
            if (!element.GetPropertyValue("Draggable", out draggable) || !element.GetPropertyByInternalName("Draggable").VisibleToUser)
            {
                return null;
            }
            var arbitraryMenuItem = new MenuItem();
            arbitraryMenuItem.IsCheckable = true;
            arbitraryMenuItem.IsChecked = draggable;
            arbitraryMenuItem.Click += arbitraryMenuItem_Click;
            arbitraryMenuItem.Header = "Draggable";
            arbitraryMenuItem.DataContext = element;
            return arbitraryMenuItem;
        }

        void arbitraryMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var fe = sender as FrameworkElement;
            if (fe == null) return;
            var element = fe.DataContext as DepictionElementParent;
            if (element == null) return;
            bool draggable;
            if (element.GetPropertyValue("Draggable", out draggable))
            {
                element.SetPropertyValue("Draggable", !draggable);
            }
        }
        private MenuItem CreateSubMenuItemFromCommand(ICommand command, object commandParam)
        {
            var arbitraryMenuItem = new MenuItem();
            arbitraryMenuItem.Command = command;
            MapElementViewModel mapElementVM = null;
            if (commandParam is MapElementViewModel)
            {
                mapElementVM = (MapElementViewModel)commandParam;
                arbitraryMenuItem.CommandParameter = new List<string> { mapElementVM.ElementKey };
            }
            else if (commandParam is DepictionElementParent)
            {
                arbitraryMenuItem.CommandParameter = commandParam as DepictionElementParent;
            }
            else
            {
                arbitraryMenuItem.CommandParameter = commandParam;
            }
            var textCommand = command as CommandInfoBase;
            if (textCommand != null)
            {
                var text = textCommand.Text;
                if (text.Contains("permatext") && mapElementVM != null)
                {
                    arbitraryMenuItem.Header = mapElementVM.UsePermaText
                                                   ? DepictionMapAndMenuViewModel.hidePermatext
                                                   : DepictionMapAndMenuViewModel.showPermatext;// "Hide permatext" : "Show permatext";
                }
                else
                {
                    arbitraryMenuItem.Header = textCommand.Text;
                }
            }
            else
            {
                arbitraryMenuItem.Header = "Unknown action";
            }

            return arbitraryMenuItem;
        }
        #endregion
        //TOTAL HACK and should be removed at some point, please
        internal static IMapCoordinateBounds CreateRequestDetailsBoundingBox(Point startPixelLocation)
        {//I guess this gets details for terra server etc (davidl)
            var latLong = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(startPixelLocation);
            var distance = 500 * 1.41;
            var latLongTopLeft = latLong.TranslateTo(315, distance, MeasurementSystem.Metric, MeasurementScale.Normal);
            var latLongBottomRight = latLong.TranslateTo(135, distance, MeasurementSystem.Metric, MeasurementScale.Normal);
            //            objBoundingBox.SetBoundingBoxExtents(clickedPosition.TranslatePosition(0, 500, MeasurementSystemAndScale.Metric).Latitude, 
            //            clickedPosition.TranslatePosition(0, -500, MeasurementSystemAndScale.Metric).Latitude, 
            //            clickedPosition.TranslatePosition(500, 0, MeasurementSystemAndScale.Metric).Longitude, 
            //            clickedPosition.TranslatePosition(-500, 0, MeasurementSystemAndScale.Metric).Longitude, new Size(1000, 1000));

            return new MapCoordinateBounds(latLongTopLeft, latLongBottomRight);
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
#if READER
            e.Handled = false;
            return;
#endif
            if (!contextMenuEnabled)
            {
                e.Handled = false;
                return;
            }

            var dc = DataContext as DepictionBasicMapViewModel;
            if (dc == null) return;
            contextMenuElements.Clear();
            contextMenuAnnotations.Clear();

            ContextMenu = new ContextMenu();
            var location = Mouse.GetPosition(this);
            //Temp 
            if (WorldCanvasMode.Equals(DepictionCanvasMode.RegionSelection) || WorldCanvasMode.Equals(DepictionCanvasMode.Normal))
            {
                foreach (var command in dc.MapCommands)
                {
                    var menuItem = new MenuItem { Command = command, Header = "Something", CommandParameter = location };
                    var textCommand = command as CommandInfoBase;
                    if (textCommand != null)
                    {
                        menuItem.Header = textCommand.Text;
                    }
                    ContextMenu.Items.Add(menuItem);
                }
                //Create the highres getters
                var enhancedDc = DataContext as DepictionMapQuickAddAndTileViewModel;
                if (enhancedDc != null)
                {
                    enhancedDc.MapBoundsOfInterest = CreateRequestDetailsBoundingBox(location);
#if !PREP
                    var highresItems = new MenuItem { Header = "Get high-res image from " };
                    var tileProviders = AddinRepository.Instance.Tilers;//enhancedDc.QuickStartDialogViewModel.ImageByAreaImporters;
                    if (tileProviders != null)
                    {
                        foreach (var tileProvider in tileProviders)
                        {
                            var item = CreateSubMenuItemFromCommand(enhancedDc.GetTileImageForAnAreaCommand,
                                                                    tileProvider);
                            item.Header = tileProvider.DisplayName;
                            highresItems.Items.Add(item);
                        }
                    }
                    ContextMenu.Items.Add(highresItems);
#endif
                }
            }

            if (WorldCanvasMode.Equals(DepictionCanvasMode.Normal))
            {
                VisualTreeHelper.HitTest(this, null, ElementContextMenuCallBack, new PointHitTestParameters(location));
                if (contextMenuAnnotations.Count != 0) { ContextMenu.Items.Add(new Separator()); }

                foreach (var annotation in contextMenuAnnotations)
                {
                    var annotationItem = new MenuItem { Header = string.Format("Annotation: {0}", annotation.Value.AnnotationText) };
                    annotationItem.MaxWidth = 200;
                    annotationItem.Items.Add(CreateSubMenuItemFromCommand(dc.ShowAnnotationPropertyInfoCommand, annotation.Value));
                    annotationItem.Items.Add(CreateSubMenuItemFromCommand(dc.DeleteAnnotationCommand, annotation.Value));

                    ContextMenu.Items.Add(annotationItem);
                }

                if (contextMenuElements.Count != 0) { ContextMenu.Items.Add(new Separator()); }
                var elementCount = contextMenuElements.Count;
                foreach (var elementVM in contextMenuElements.Values)
                {
                    var longName = elementVM.ToolTipText;
                    var shortName = elementVM.ToolTipText;
                    if (string.IsNullOrEmpty(longName))
                    {
                        shortName = elementVM.DisplayName;
                        longName = elementVM.DisplayName;
                    }
                    else
                    {
                        string[] lines = shortName.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
                        var more = string.Empty;
                        if (lines.Length > 1)
                        {
                            shortName = lines[0];
                            more = "...";
                        }
                        if (shortName.Length > MaxContextMenuLength)
                        {
                            shortName = shortName.Substring(0, MaxContextMenuLength);
                            more = "...";
                        }
                        shortName += more;
                    }

                    var subMenuItems = new List<object>();
                    var draggableItem = CreateCheckableItem(elementVM.ElementModel, null);
                    if (draggableItem != null)
                    {
                        subMenuItems.Add(draggableItem);
                        subMenuItems.Add(new Separator());
                        //                        elementItem.Items.Add(draggableItem);
                        //                        elementItem.Items.Add(new Separator());
                    }
                    foreach (var elemCommand in dc.GenericElementCommands)
                    {
                        var param = new List<string> { elementVM.ElementKey };
                        if (!elemCommand.CanExecute(param))
                        {
                            continue;
                        }
                        subMenuItems.Add(CreateSubMenuItemFromCommand(elemCommand, elementVM));
                        //                        elementItem.Items.Add(CreateSubMenuItemFromCommand(elemCommand, elementVM));
                    }
                    #region questionable part of the letting addons add context menus to elements
                    //Hack for Brian S addon. Basically addons will want to add their own commands to elements
                    //but it is still unclear as to the best way to do this.
                    //The big questions are what parameters will be needed, and if we can make the universal
                    //so that all future add-ons will look the same.
                    var fullElement = elementVM.ElementModel as DepictionElementParent;
                    if (fullElement != null)
                    {
                        foreach (var otherCommand in fullElement.ElementCommands)
                        {
                            subMenuItems.Add(CreateSubMenuItemFromCommand(otherCommand, fullElement));
                        }
                    }
                    #endregion
                    if (elementCount == 1)
                    {
                        foreach (var item in subMenuItems)
                        {
                            ContextMenu.Items.Add(item);
                        }
                    }
                    else
                    {
                        var elementItem = new MenuItem { Header = shortName };
                        elementItem.ToolTip = longName;
                        foreach (var item in subMenuItems)
                        {
                            elementItem.Items.Add(item);
                        }
                        ContextMenu.Items.Add(elementItem);
                    }

                }
            }
            else
            {
                ContextMenu = null;
            }
            base.OnContextMenuOpening(e);
        }

        private HitTestResultBehavior ElementContextMenuCallBack(HitTestResult result)
        {
            var res = result.VisualHit;
            var fe = res as FrameworkElement;
            if (fe == null)
            {
                var dv = res as DrawingVisual;
                if (dv != null)
                {
                    fe = dv.Parent as FrameworkElement;
                }
            }

            if (fe != null && fe.IsHitTestVisible)
            {
                if (fe.DataContext is MapAnnotationViewModel)
                {
                    var annotation = (MapAnnotationViewModel)fe.DataContext;
                    if (annotation != null && !contextMenuAnnotations.ContainsKey(annotation.AnnotationID))
                    {
                        contextMenuAnnotations.Add(annotation.AnnotationID, annotation);
                    }
                }
                else
                {
                    var elementDC = fe.DataContext as MapElementViewModel;
                    if (elementDC != null && !contextMenuElements.ContainsKey(elementDC.ElementKey))
                    {
                        contextMenuElements.Add(elementDC.ElementKey, elementDC);
                    }
                }
            }
            return HitTestResultBehavior.Continue;
        }
        #endregion
    }
}