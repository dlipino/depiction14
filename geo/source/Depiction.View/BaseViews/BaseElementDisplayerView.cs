﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Depiction.View.DepictionItemsControlViews;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.View.BaseViews
{
    public class BaseElementDisplayerView : UserControl
    {
        protected List<UIElement> originalChildren = new List<UIElement>();
        protected Panel mainParent;


        #region Element drag icon producers
        private class UIElementRectPair
        {
            public UIElement VisualElement { get; set; }
            public Rect Bounds { get; set; }
        }
        public Dictionary<UIElement, Rect> GetVisualsForElement(IMapDraggable dragElement)
        {
            var visualList = new List<UIElementRectPair>();
            var visuals = new Dictionary<UIElement, Rect>();
            foreach (var child in originalChildren)
            {
                var baseItemsControl = child as ItemsControl;
                if (baseItemsControl != null)
                {
                    UIElement fe = null;
                    Rect bounds = Rect.Empty;
                    if (baseItemsControl is ElementImageItemsControlView) { }
                    else
                    {
                        if (baseItemsControl.Items.Contains(dragElement))
                        {
                            fe = baseItemsControl.ItemContainerGenerator.ContainerFromItem(dragElement) as UIElement;
                            var element = dragElement as MapElementViewModel;
                            if (element != null)
                            {
                                var pixelLocation = new Point(dragElement.ElementImagePosition.X,
                                                              dragElement.ElementImagePosition.Y);
                                if (baseItemsControl is ElementZOIItemsControlView)
                                {
                                    bounds = element.ZOI.ZOIDrawGeometry.Bounds;
                                    //Get the offset from the elements pixel location (center)
                                    if (bounds.Equals(Rect.Empty))
                                    {
                                        bounds = new Rect(0, 0, 0, 0);
                                    }
                                    else
                                    {
                                        var offsetFromCenter = pixelLocation - bounds.Location;
                                        bounds.Location = new Point(offsetFromCenter.X, offsetFromCenter.Y);
                                    }
                                    if(fe != null) visualList.Add(new UIElementRectPair {Bounds = bounds, VisualElement = fe});
                                    
                                }
                                if (baseItemsControl is ElementIconItemsControlView)
                                {
                                    var itemsControl = baseItemsControl as ElementIconItemsControlView;
                                    var contPre = fe as FrameworkElement;
                                    var scaler = 1d;
                                    var dragIconSize = element.IconSize * scaler / itemsControl.WorldScale;
                                    bounds = new Rect(new Point(-dragIconSize / 2, -dragIconSize / 2), new Size());
                                    if (contPre != null)
                                    {
                                        bounds.Width = dragIconSize;
                                        bounds.Height = dragIconSize;
                                    }
                                    if (fe != null) visualList.Insert(0,new UIElementRectPair { Bounds = bounds, VisualElement = fe });
                                }
                            }
                        }
                    }
                    
                }
            }
            foreach(var pair in visualList)
            {
                visuals.Add(pair.VisualElement,pair.Bounds);
            }
            return visuals;
        }
        #endregion
    }
}