﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction.API.CoreEnumAndStructs;
using Depiction.ViewModels.ViewModels.TileViewModels;

namespace Depiction.View.TileWidget
{
    /// <summary>
    /// Interaction logic for AdvancedTileController.xaml
    /// </summary>
    public partial class AdvancedTileController
    {
//        ObservableCollection<ComboBoxItem> first = new ObservableCollection<ComboBoxItem> { };
//        private ComboBoxItem iFirst = new ComboBoxItem() {Content = "One"};
//        private ComboBoxItem iSecond = new ComboBoxItem() {Content = "Second"};
//        List<ComboBoxItem> second = new List<ComboBoxItem> { new ComboBoxItem() { Content = "1" } };
        public AdvancedTileController()
        {
            InitializeComponent();
            Loaded += AdvancedTileController_Loaded;
            DataContextChanged += AdvancedTileController_DataContextChanged;
//            cboTilingSource.ItemsSource = first;
        }

        void AdvancedTileController_DataContextChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            UpdateAvailableTilerTypes();
        }

        void AdvancedTileController_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            UpdateAvailableTilerTypes();
        }

        private void UpdateAvailableTilerTypes()
        {
            var panel = cboTilingSource.Template.FindName("RButtonStackPanel", cboTilingSource) as StackPanel;
            if (panel == null) return;
            var dc = DataContext as TileDisplayerViewModel;
            if (dc == null) return;
            var childrenToRemove = new List<Control>();
            foreach (Control child in panel.Children)
            {
                if (child == null) continue;
                var tag = child.Tag;
                if (tag == null || !(tag is TileImageTypes)) continue;
                if (!dc.AvailableTilerTypes.Contains((TileImageTypes)tag))
                {
                    childrenToRemove.Add(child);
                }
            }

            foreach (var child in childrenToRemove)
            {
                panel.Children.Remove(child);
            }
        }
        private void GenericRButton_MouseEnter(object sender, MouseEventArgs e)
        {
            cboTilingSource.IsDropDownOpen = false;
            var dc = DataContext as TileDisplayerViewModel;
            if (dc == null) return;
            var label = sender as Label;
            if (label == null) return;
            TileImageTypes tag = TileImageTypes.Unknown;
            if (Enum.TryParse(label.Tag.ToString(), out tag))
            {
                dc.TileProviderType = tag;
            }


//            var rbutton = sender as Label;
//            if (rbutton == null) return;
//            
//            TileImageTypes tag = TileImageTypes.Unknown;
//            if (Enum.TryParse(rbutton.Tag.ToString(), out tag))
//            {
//                //                dc.TileProviderType = tag;
//                if (tag.Equals(TileImageTypes.None))
//                {
//                    //                    cboTilingSource.ItemsSource = first;
//                    if(!first.Contains(iFirst))first.Add(iFirst);
//                    if(first.Contains(iSecond)) first.Remove(iSecond);
//                }
//                else
//                {
//
//                    if(first.Contains(iFirst))first.Remove(iFirst);
//                    if(!first.Contains(iSecond))first.Add(iSecond);
//                    //                    cboTilingSource.ItemsSource = second;
//                }
//                
//            }

            cboTilingSource.IsDropDownOpen = true;
            //Hack
            var timer = new DispatcherTimer();
            timer.Tick+=timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 0,0,75);
            timer.Start(); 
        }

        void timer_Tick(object sender, EventArgs e)
        {
            cboTilingSource.IsDropDownOpen = true;
            var sendingTimer = sender as DispatcherTimer;
            if (sendingTimer == null) return;
            sendingTimer.Stop();
        }

        //This doesnt work very well because the popup goes onto an adorner layers and basically takes over the whole screen
        private void GenericRButton_MouseLeave(object sender, MouseEventArgs e)
        {
            var control = sender as Panel;
            if (control == null) return;
            var tp = control.TemplatedParent as ComboBox;
            if (tp == null) return;
            tp.IsDropDownOpen = false;
        }

        private void LabelMouseDown(object sender, MouseButtonEventArgs e)
        {
            cboTilingSource.IsDropDownOpen = true;
        }
    }
}
