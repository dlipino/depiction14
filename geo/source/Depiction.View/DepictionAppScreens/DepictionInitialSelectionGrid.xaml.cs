﻿using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.DepictionAppScreens
{
    /// <summary>
    /// Interaction logic for DepictionInitialSelectionGrid.xaml
    /// </summary>
    public partial class DepictionInitialSelectionGrid
    {
        public DepictionInitialSelectionGrid()
        {
            InitializeComponent();
            var storyName = DepictionAccess.ProductInformation.StoryName;
           
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
//                     initialCreateDepictionButton.Visibility = Visibility.Collapsed;
//                        settingButton.Visibility = Visibility.Collapsed;
//                        initialTutorialsButton.Visibility = Visibility.Collapsed;
//                        initialSamplesButton.Visibility = Visibility.Collapsed;
                    initialButtonStackPanel.Children.Remove(initialCreateDepictionButton);
                    initialButtonStackPanel.Children.Remove(settingButton);
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(initialSamplesButton);

                    break;
                case ProductInformationBase.Prep:
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    break;
                case ProductInformationBase.DepictionRW:
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    initialButtonStackPanel.Children.Remove(initialSamplesButton);
                    break;
                case ProductInformationBase.OsintInformation:
                    initialCreateDepictionButton.Content = "new " + storyName;
                    initialOpenDepictionButton.Content = "open " + storyName;
                    initialCreateDepictionButton.ToolTip = string.Format("Create {0}", DepictionAccess.ProductInformation.StoryName);
                    initialOpenDepictionButton.ToolTip = string.Format("Open {0} file", DepictionAccess.ProductInformation.StoryName);
                    initialButtonStackPanel.Children.Remove(initialTutorialsButton);
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    initialButtonStackPanel.Children.Remove(initialSamplesButton);
                    break;
                default:
                    initialButtonStackPanel.Children.Remove(purchaseDepictionFullButton);
                    break;
            }

//#if PREP
//            initialButtonStackPanel.Children.Remove(initialTutorialsButton);
//#endif
        }
    }
}
