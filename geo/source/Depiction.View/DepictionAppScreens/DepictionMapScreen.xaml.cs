﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.ViewModels.ViewModels;
using Depiction.ViewModels.ViewModels.MapControlDialogViewModels;

namespace Depiction.View.DepictionAppScreens
{
    /// <summary>
    /// Interaction logic for DepictionMapScreen.xaml
    /// </summary>
    public partial class DepictionMapScreen
    {
        public DepictionMapScreen()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    mainMapPurchaseDepictionBtn.Visibility = Visibility.Visible;
                    freeReaderWaterMark.IsEnabled = true;
                    break;
                case ProductInformationBase.Prep:
                    mainMapPurchaseDepictionBtn.Visibility = Visibility.Collapsed;
                    Drop += DepictionMapScreen_Drop;
                    DragEnter += DepictionMapScreen_DragEnter;
                    break;
                default:
                    mainMapPurchaseDepictionBtn.Visibility = Visibility.Collapsed;
                    Drop += DepictionMapScreen_Drop;
                    DragEnter += DepictionMapScreen_DragEnter;
                    break;
            }
            DataContextChanged += DepictionMapScreen_DataContextChanged;
        }

        void DepictionMapScreen_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                if (fileNames == null) return;
                if (fileNames.Length != 1) return;
                var extension = Path.GetExtension(fileNames[0]);
                if (extension.Equals(".dml", StringComparison.InvariantCultureIgnoreCase))
                {
                    //TODO show the prototype editor
                    //  e.Effects = DragDropEffects.All;
                    return;
                }
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
        }

        void DepictionMapScreen_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldDc = e.OldValue as DepictionAppViewModel;
            var newDc = e.NewValue as DepictionAppViewModel;
            if (oldDc != null)
            {
                oldDc.PropertyChanged -= dataContext_PropertyChanged;
            }
            if (newDc != null)
            {
                newDc.PropertyChanged += dataContext_PropertyChanged;
            }
        }

        void dataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("AcceptFileDrops"))
            {
                var dc = DataContext as DepictionAppViewModel;
                if (dc == null) return;
                if (dc.AcceptFileDrops)
                {
                    AllowDrop = true;
                }
                else
                {
                    AllowDrop = false;
                }
            }
        }

        void DepictionMapScreen_Drop(object sender, DragEventArgs e)
        {
            var dc = DataContext as DepictionAppViewModel;
            if (dc == null) return;

            IMapCoordinateBounds region = null;
            if (DepictionAccess.CurrentDepiction != null)
            {
                region = DepictionAccess.CurrentDepiction.DepictionGeographyInfo.DepictionRegionBounds;
            }
            if (region != null && e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var fileNames = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                if (fileNames == null) return;
                var addContentDialog = dc.MapViewModel.AddContentDialogViewModel;
                if (addContentDialog == null) return;
                var count = 0;
                foreach (var fileName in fileNames)
                {
                    if (count >= 1) return;
                    var extension = Path.GetExtension(fileName);
                    if (extension.Equals(".dml", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //TODO show the prototype editor
                        return;
                    }
                    if (extension.Equals(".csv", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //bring up wizard
                        addContentDialog.LoadFileWithElementInformation(
                            new KeyValuePair<string, IDepictionImporterBase>(fileName, null),
                            DepictionStringService.AutoDetectElementString);
                    }
                    else
                    {
                        //bring up add content
                        addContentDialog.FileNameWithElementsToAdd = fileName;
                        addContentDialog.IsDialogVisible = true;
                        addContentDialog.AddMode = AddContentMode.File;
                    }
                    count++;
                }
            }
        }
    }
}
