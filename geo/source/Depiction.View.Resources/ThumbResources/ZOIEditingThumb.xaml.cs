﻿using System.Windows;
using System.Windows.Media;
using Depiction.API.HelperObjects;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.Resources.ThumbResources
{
    /// <summary>
    /// Interaction logic for ZOIEditingThumb.xaml
    /// </summary>
    public partial class ZOIEditingThumb 
    {
        public EnhancedPointList parentPoints = new EnhancedPointList();

        public TranslateTransform LocationTransform { get; set; }
        
        #region Dep props
        public ZOIEditThumbType EditThumbType
        {
            get { return (ZOIEditThumbType)GetValue(EditThumbTypeProperty); }
            set { SetValue(EditThumbTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for EditThumbType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty EditThumbTypeProperty =
            DependencyProperty.Register("EditThumbType", typeof(ZOIEditThumbType), typeof(ZOIEditingThumb), new UIPropertyMetadata(ZOIEditThumbType.Vertex, ThumbTypeChange));

        public bool ShiftDown
        {
            get { return (bool)GetValue(ShiftDownProperty); }
            set { SetValue(ShiftDownProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShiftDown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShiftDownProperty =
            DependencyProperty.Register("ShiftDown", typeof(bool), typeof(ZOIEditingThumb), new UIPropertyMetadata(false));


        public Point LineEnd
        {
            get { return (Point)GetValue(LineEndProperty); }
            set { SetValue(LineEndProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineEnd.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineEndProperty =
            DependencyProperty.Register("LineEnd", typeof(Point), typeof(ZOIEditingThumb), new UIPropertyMetadata(new Point()));


        public Point LineStart
        {
            get { return (Point)GetValue(LineStartProperty); }
            set { SetValue(LineStartProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineStart.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineStartProperty =
            DependencyProperty.Register("LineStart", typeof(Point), typeof(ZOIEditingThumb), new UIPropertyMetadata(new Point()));


        public double LineThickness
        {
            get { return (double)GetValue(LineThicknessProperty); }
            set { SetValue(LineThicknessProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LineThickness.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LineThicknessProperty =
            DependencyProperty.Register("LineThickness", typeof(double), typeof(ZOIEditingThumb), new UIPropertyMetadata(1d));


        #endregion

        #region methods for propdp

        private static void ThumbTypeChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var thumb = d as ZOIEditingThumb;
            if (thumb == null) return;
            if (e.OldValue == e.NewValue) return;
            var newVal = (ZOIEditThumbType)e.NewValue;
           
            if(Application.Current == null || Application.Current.Resources == null)
            {
                return;
            }
            //Using generics/thems would probably be a more efficient way of setting these things style. But no time yet.
            switch (newVal)
            {
                case ZOIEditThumbType.Edge:
                    if (Application.Current.Resources.Contains("EdgeStyle"))
                    {
                        thumb.Style = Application.Current.Resources["EdgeStyle"] as Style;
                    }
                    break;
                case ZOIEditThumbType.Vertex:
                    if (Application.Current.Resources.Contains("VertexStyle"))
                    {
                        thumb.Style = Application.Current.Resources["VertexStyle"] as Style;
                    }
                    break;
            }
        }

        #endregion

        public ZOIEditingThumb()
        {
            LocationTransform = new TranslateTransform();
            EditThumbType = ZOIEditThumbType.Vertex;
            InitializeComponent();
            Style = TryFindResource("VertexStyle") as Style;
        }

        public ZOIEditingThumb(Point location, EnhancedPointList mainPoints)
            : this()
        {
            Width = 15;
            Height = 15;
            LineStart = location;
            LocationTransform.X = location.X - Width / 2d;
            LocationTransform.Y = location.Y - Height / 2d;
            parentPoints = mainPoints;
        }
        public ZOIEditingThumb(Point start, Point end, EnhancedPointList mainPoints)
            : this()
        {
            LineStart = start;
            LineEnd = end;
            EditThumbType = ZOIEditThumbType.Edge;
            parentPoints = mainPoints;
        }

        public void AdjustVisualsForScale(double inverseScale)
        {
            if (EditThumbType.Equals(ZOIEditThumbType.Vertex))
            {
                Width = 15 * inverseScale;
                Height = 15 * inverseScale;
                LocationTransform.X = LineStart.X -Width / 2d;
                LocationTransform.Y = LineStart.Y -Height / 2d;
            }
            LineThickness = 4 * inverseScale;
        }
    }
}
