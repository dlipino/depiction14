﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.View.Resources.ThumbResources
{
    /// <summary>
    /// Interaction logic for ResizeThumb.xaml
    /// </summary>
    public class RotateThumb : Thumb
    {

        private Point centerPoint;
        private Vector startVector;
//        private double _initialAngle;
        private Panel rotationElementMainParentCanvas;
        //        private ContentControl designerItem;
        //private RotateTransform rotateTransform;


        public RotateThumb()
        {
            DragDelta += RotateThumb_DragDelta;
            DragStarted += RotateThumb_DragStarted;
        }
        #region static helpers for doing a hack for the shift key
        public static double RadiansToDegrees(double Radians)
        {
            return (180 / Math.PI) * Radians;
        }

        public static double DegreesToRadians(double Degrees)
        {
            return (Math.PI / 180) * Degrees;
        }

        static public void DoARotationOnThumbWithInputs(Thumb thumb, Panel mainPanel, Vector startVector,
            Point centerPoint, double initialRotationDegree)
        {
            var tp = thumb.TemplatedParent as FrameworkElement;
            var dc = thumb.DataContext as ImageRegistrationViewModel;
            if (dc == null) return;
            var rotation = dc.Rotation;
            if (rotation == null) return;//Should never be null;

            if (tp != null && mainPanel != null)
            {
                var currentPoint = Mouse.GetPosition(mainPanel);
                var deltaVector = Point.Subtract(currentPoint, centerPoint);
                var angle = Vector.AngleBetween(startVector, deltaVector);
                rotation.Angle = initialRotationDegree + angle;
                tp.InvalidateMeasure();
                //Testing different ways ofdoing this to tired to get it right for now though
                //                var deltaVectorA = Vector.Add(startVector, new Vector(e.HorizontalChange, e.VerticalChange));
                //                var angleA = Vector.AngleBetween(startVector, deltaVectorA);
            }
        }
        #endregion
        private void RotateThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            var thumb = sender as Thumb;
            if (thumb == null) return;
            var dc = thumb.DataContext as ImageRegistrationViewModel;
            if (dc == null) return;
            var rotation = dc.Rotation;
            if (rotation == null) return;//Should never be null;
//            _initialAngle = rotation.Angle;
            //Need to find what ever the thumb is rotating (for the size)

            var thumbParent = thumb.Parent as FrameworkElement;

            if (thumbParent == null) return;

            rotationElementMainParentCanvas = VisualTreeHelper.GetParent(thumbParent) as Panel;

            if (rotationElementMainParentCanvas != null)
            {
                centerPoint = thumbParent.TranslatePoint(
                    new Point(thumbParent.ActualWidth * thumbParent.RenderTransformOrigin.X,
                              thumbParent.ActualHeight * thumbParent.RenderTransformOrigin.Y),
                    rotationElementMainParentCanvas);

                Point startPoint = Mouse.GetPosition(rotationElementMainParentCanvas);
                startVector = Point.Subtract(startPoint, centerPoint);
            }
        }

        private void RotateThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var thumb = sender as Thumb;
            if (thumb == null) return;
            if(rotationElementMainParentCanvas == null) return;
//            DoARotationOnThumbWithInputs(thumb, rotationElementMainParentCanvas, startVector, centerPoint);
            var tp = thumb.TemplatedParent as FrameworkElement;
            var dc = thumb.DataContext as ImageRegistrationViewModel;
            if (dc == null) return;
            var rotation = dc.Rotation;
            if (rotation == null) return;//Should never be null;
            var initialAngle = rotation.Angle;

            if (tp != null && rotationElementMainParentCanvas != null)
            {
                var currentPoint = Mouse.GetPosition(rotationElementMainParentCanvas);
                var deltaVector = Point.Subtract(currentPoint, centerPoint);
                var angle = Vector.AngleBetween(startVector, deltaVector);
                rotation.Angle = initialAngle + angle;
                tp.InvalidateMeasure();
                //Testing different ways ofdoing this to tired to get it right for now though
                var deltaVectorA = Vector.Add(startVector, new Vector(e.HorizontalChange, e.VerticalChange));
                var angleA = Vector.AngleBetween(startVector, deltaVectorA);
            }
        }
    }
}