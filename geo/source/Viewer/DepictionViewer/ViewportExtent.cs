﻿using System;

namespace DepictionSilverlight
{
    /// <summary>
    /// Contract for a Geographic extent.
    /// </summary>
    public class ViewportExtent
    {
        private BoundingBox viewPortBoundingBox;
        private double heightInPixels;
        private double widthInPixels;
        public ViewportExtent(BoundingBox viewPortBoundingBox, double widthInPixels, double heightInPixels)
        {
            this.viewPortBoundingBox = viewPortBoundingBox;
            this.widthInPixels = widthInPixels;
            this.heightInPixels = heightInPixels;
            SetExtent(viewPortBoundingBox, widthInPixels, heightInPixels);
        }

        private void SetExtent(BoundingBox rect, double newWidthInPixels, double newHeightInPixels)
        {
            if (newWidthInPixels == 0)
                throw new Exception("Attempt to create ViewportExtent with zero width");
            if (newHeightInPixels == 0)
                throw new Exception("Attempt to create ViewportExtent with zero height");
            viewPortBoundingBox = rect;
            heightInPixels = newHeightInPixels;
            widthInPixels = newWidthInPixels;
        }

        public LatitudeLongitude NorthWest
        {
            get { return viewPortBoundingBox.TopLeft; }
        }

        public LatitudeLongitude SouthEast
        {
            get { return viewPortBoundingBox.BottomRight; }
        }

        public double WidthInPixels
        {
            get { return widthInPixels; }
            set { widthInPixels = value; }
        }

        public double HeightInPixels
        {
            get { return heightInPixels; }
            set { heightInPixels = value; }
        }

    }
}
