﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DepictionSilverlight
{
    public class ImageElementInfo
    {
        #region constructor

        public ImageElementInfo(string retrievedFrom, string title)
        {
            Title = title;
            RetrievedFrom = retrievedFrom;
        }

        #endregion

        #region properties

        public LatitudeLongitude TopLeft { get; private set; }
        public LatitudeLongitude BottomRight { get; private set; }
        public string Title { get; private set; }
        public string RetrievedFrom { get; private set; }
        public BitmapSource Map { get; private set; }
        public bool IsValid { get; private set; }
        public Exception Error { get; private set; }

        #endregion

        #region method

        public void SetMap(LatitudeLongitude topLeft, LatitudeLongitude bottomRight, BitmapSource map)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
            Map = map;
            IsValid = true;
        }

        public void Fail(Exception error)
        {
            Error = error;
            IsValid = false;
        }


        public void Crop(BoundingBox area)
        {
            double targetWidth = Map.PixelWidth * (area.Right - area.Left) / (BottomRight.Longitude - TopLeft.Longitude);
            double targetHeight = Map.PixelHeight * (area.Top - area.Bottom) / (TopLeft.Latitude - BottomRight.Latitude);
            Crop(area, targetWidth, targetHeight);
        }


        public void Crop(BoundingBox area, double targetWidth, double targetHeight)
        {
            //double sourceWidth = Map.PixelWidth * (area.Right - area.Left) / (BottomRight.Longitude - TopLeft.Longitude);
            //double sourceHeight = Map.PixelHeight * (area.Top - area.Bottom) / (TopLeft.Latitude - BottomRight.Latitude);
            double left = Map.PixelWidth * (TopLeft.Longitude - area.Left) / (BottomRight.Longitude - TopLeft.Longitude);
            double top = Map.PixelHeight * (TopLeft.Latitude - area.Top) / (TopLeft.Latitude - BottomRight.Latitude);
            //BitmapImage target = new BitmapImage();
            
            WriteableBitmap target = new WriteableBitmap((int)Math.Round(targetWidth), (int)Math.Round(targetHeight));
            
            Transform transform = new TranslateTransform { X = -left, Y = -top};
            var image = new Image();
            image.Source = Map;

            target.Render(image, transform);
            Map = target;
            TopLeft = area.TopLeft;
            BottomRight = area.BottomRight;

        }

        #endregion
    }
}
