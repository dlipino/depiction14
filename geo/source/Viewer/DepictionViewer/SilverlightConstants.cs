﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DepictionSilverlight
{
    public static class SilverlightConstants
    {
        public const double MinZoomInFactor = .001;
        public const double MaxZoomInFactor = 1300.0;
        public const double MapSize = 300000;
    }
}
