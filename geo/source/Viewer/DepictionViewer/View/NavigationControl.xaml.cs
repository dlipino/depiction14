﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace DepictionSilverlight.UI
{
    public partial class NavigationControl : UserControl
    {
        private const double panelBackgroundOpacity = 0.5D;
        private const double buttonBackgroundOpacity = 0.4D;
        private const double foregroundOpacity = 1.0D;

        public NavigationControl()
        {
            InitializeComponent();

            zoomInButton.Click += zoomInButton_Click;
            zoomOutButton.Click += zoomOutButton_Click;
            panLeftButton.Click += panLeftButton_Click;
            panUpButton.Click += panUpButton_Click;
            panRightButton.Click += panRightButton_Click;
            panDownButton.Click += panDownButton_Click;

            // Mouse-over behavior
            zoomInButton.MouseEnter += button_MouseEnter;
            zoomInButton.MouseLeave += button_MouseLeave;
            zoomOutButton.MouseEnter += button_MouseEnter;
            zoomOutButton.MouseLeave += button_MouseLeave;
            panLeftButton.MouseEnter += button_MouseEnter;
            panLeftButton.MouseLeave += button_MouseLeave;
            panUpButton.MouseEnter += button_MouseEnter;
            panUpButton.MouseLeave += button_MouseLeave;
            panRightButton.MouseEnter += button_MouseEnter;
            panRightButton.MouseLeave += button_MouseLeave;
            panDownButton.MouseEnter += button_MouseEnter;
            panDownButton.MouseLeave += button_MouseLeave;
            navWidgetInnerGrid.MouseEnter += navWidgetInnerGrid_MouseEnter;
            navWidgetInnerGrid.MouseLeave += navWidgetInnerGrid_MouseLeave;

            donutHole.Click += donutHole_Click;
        }

        static void donutHole_Click(object sender, RoutedEventArgs e)
        {

            SilverlightAccess.DepictionViewerViewModel.CenterOnPoint(SilverlightAccess.DepictionViewerViewModel.RegionMetadata.RegionExtent.Center);
        }

        static void button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var element = (FrameworkElement)sender;
            element.Opacity = foregroundOpacity;
        }

        static void button_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var element = (FrameworkElement)sender;
            element.Opacity = buttonBackgroundOpacity;
        }

        private static void navWidgetInnerGrid_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var element = (FrameworkElement)sender;
            element.Opacity = foregroundOpacity;
        }

        private static void navWidgetInnerGrid_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var element = (FrameworkElement)sender;
            element.Opacity = panelBackgroundOpacity;
        }

        private void panLeftButton_Click(object sender, RoutedEventArgs e)
        {
            RaisePan(NavigationControlPanDirection.Left);
        }

        private void panUpButton_Click(object sender, RoutedEventArgs e)
        {
            RaisePan(NavigationControlPanDirection.Up);
        }

        private void panRightButton_Click(object sender, RoutedEventArgs e)
        {
            RaisePan(NavigationControlPanDirection.Right);
        }

        private void panDownButton_Click(object sender, RoutedEventArgs e)
        {
            RaisePan(NavigationControlPanDirection.Down);
        }

        private void zoomInButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseZoom(new NavigationControlZoomEventArgs { ZoomDelta = 11.0 / 9 } );
        }

        private void zoomOutButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseZoom(new NavigationControlZoomEventArgs { ZoomDelta = 9.0 / 11 });
        }

        public event EventHandler<NavigationControlPanEventArgs> Pan;
        private void RaisePan(NavigationControlPanDirection direction)
        {
            EventHandler<NavigationControlPanEventArgs> panHandler = Pan;
            // heh, panHandler... I like that.
            if (panHandler != null)
            {
                panHandler(this, new NavigationControlPanEventArgs { Direction = direction });
            }
        }

        public event EventHandler<NavigationControlZoomEventArgs> Zoom;
        private void RaiseZoom(NavigationControlZoomEventArgs e)
        {
            EventHandler<NavigationControlZoomEventArgs> zoomHandler = Zoom;
            if (zoomHandler != null)
            {
                zoomHandler(this, e);
            }
        }
    }

    public class NavigationControlZoomEventArgs : EventArgs
    {
        public double ZoomDelta { get; set; }
    }

    public enum NavigationControlPanDirection
    {
        Left,
        Up,
        Right,
        Down
    }

    public class NavigationControlPanEventArgs : EventArgs
    {
        public NavigationControlPanDirection Direction { get; set; }
    }
}
