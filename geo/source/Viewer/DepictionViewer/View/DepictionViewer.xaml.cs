﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using DepictionSilverlight.Data;
using DepictionSilverlight.Model;
using DepictionSilverlight.Tilers;
using DepictionSilverlight.UI;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight.View
{
    public partial class DepictionViewer : IDepictionViewer
    {

        #region fields

        private bool draggingTileCanvas;
        private Point lastDragPoint;
        private LatitudeLongitude lastMousePositionInLatitudeLongitude;

        private DepictionViewerViewModel viewModel;
        #endregion

        #region constructor



        public DepictionViewer()
        {
            InitializeComponent();
            
            DataContext = viewModel = SilverlightAccess.DepictionViewerViewModel = new DepictionViewerViewModel();
            DepictionLoader.DepictionLoaded += DepictionLoaderDepictionLoaded;
            SilverlightAccess.DepictionViewer = this;
            SilverlightAccess.Dispatcher = Dispatcher;
            //Binding regionBinding = new Binding("RegionBlackoutVisibility");
            //regionBinding.Source = regionMetaData;
            

            worldCanvasHolder.MouseWheel += DepictionViewer_MouseWheel;
            navigationControl.Zoom += navigationControl_Zoom;
            navigationControl.Pan += navigationControl_Pan;
            MouseLeftButtonDown += worldCanvas_MouseLeftButtonDown;
            MouseLeftButtonUp += worldCanvas_MouseLeftButtonUp;
            MouseMove += worldCanvas_MouseMove;
            fullScreenButton.Click += fullScreenButton_Click;
            
            //TODO ugh
            //DataContext = viewModel.RegionMetadata;
            
            animateProgress.Begin();
        }

        //not a fan of this being here
        private void DepictionLoaderDepictionLoaded()
        {
            var iconElements = viewModel.ElementViewModels.Where(t => t.InMainCanvas && t.IconVisibility == Visibility.Visible);
            SilverlightAccess.Dispatcher.BeginInvoke(() => elementDisplayerIcon.ItemsSource = iconElements);
            var imageElements = viewModel.ElementViewModels.Where(t => t.InMainCanvas && t.ImageVisibility == Visibility.Visible);
            SilverlightAccess.Dispatcher.BeginInvoke(() => elementDisplayerImage.ItemsSource = imageElements);
            var zoiElements = viewModel.ElementViewModels.Where(t => t.InMainCanvas && t.PathGeometry != null);
            SilverlightAccess.Dispatcher.BeginInvoke(() => elementDisplayerZOI.ItemsSource = zoiElements);
            SilverlightAccess.Dispatcher.BeginInvoke(() => elementDisplayerPermatext.ItemsSource = iconElements);

            annotationDisplayer.ItemsSource = DepictionData.Instance.Annotations;
            foreach (RevealerMetadata revealer in DepictionData.Instance.Revealers)
            {
                var elements =
                    viewModel.ElementViewModels.Where(
                        t => t.RevealerIds != null && t.RevealerIds.Contains(revealer.RevealerId)).ToList();

                revealer.IconElements = elements.Where(t => t.IconVisibility == Visibility.Visible);
                revealer.ImageElements = elements.Where(t => t.ImageVisibility == Visibility.Visible);
                revealer.ZoiElements = elements.Where(t => t.PathGeometry != null);
                var revealerDisplayer = new RevealerDisplayer { DataContext = revealer };

                //metadata.Elements = elements;
                SilverlightAccess.Dispatcher.BeginInvoke(() => worldCanvas.Children.Add(revealerDisplayer));
            }

            legend.BuildLegend();
            descriptionControl.Description = DepictionData.Instance.Description;
            descriptionControl.Title = DepictionData.Instance.Title;

            SilverlightAccess.Dispatcher.BeginInvoke(() => loadingPanel.Visibility = Visibility.Collapsed);

        }

        public IEnumerable<UIElement> FindElementsAtMousePosition(MouseEventArgs mouseEventArgs)
        {
            return VisualTreeHelper.FindElementsInHostCoordinates(mouseEventArgs.GetPosition(worldCanvasHolder), worldCanvasHolder);
        }


        static void fullScreenButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Host.Content.IsFullScreen = !Application.Current.Host.Content.IsFullScreen;
        }

        #endregion

        #region events
        

        

        



        private DateTime lastTime = DateTime.Now;
        private readonly TimeSpan duration = new TimeSpan(0, 0, 0, 0, 100);
        void DepictionViewer_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;
            if (DateTime.Now - lastTime < duration)
                return;
            lastTime = DateTime.Now;
            
            double mouseDelta = e.Delta;
            mouseDelta = Math.Sign(mouseDelta);
            if (mouseDelta == 1)
                mouseDelta = 11.0/9;
            else
            {
                mouseDelta = 9.0/11;
            }

            viewModel.Zoom(mouseDelta, false, e.GetPosition(worldCanvas));
        }

        void navigationControl_Pan(object sender, NavigationControlPanEventArgs e)
        {
            viewModel.Pan(e.Direction);
        }

        private void navigationControl_Zoom(object sender, NavigationControlZoomEventArgs e)
        {
            viewModel.Zoom(e.ZoomDelta, true, new Point());
        }

        private void worldCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            //Debug.WriteLine("mouse move");
            Point tileCanvasPosition = e.GetPosition(worldCanvas);


            lastMousePositionInLatitudeLongitude = SilverlightAccess.PixelConverter.GeoCanvasToWorld(tileCanvasPosition);
            //lastMousePointOnLayoutRoot = e.GetPosition(worldCanvasHolder);

            if (lastMousePositionInLatitudeLongitude == null) return;

            viewModel.LastMousePosition = lastMousePositionInLatitudeLongitude.ToString();

            if (!draggingTileCanvas) return;

            Point currentMousePosition = e.GetPosition(worldCanvasHolder);

            double leftDelta = currentMousePosition.X - lastDragPoint.X;
            double topDelta = currentMousePosition.Y - lastDragPoint.Y;

            lastDragPoint = currentMousePosition;

            viewModel.Pan(leftDelta, topDelta);
        }

        private void worldCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lastDragPoint = e.GetPosition(worldCanvasHolder);
            draggingTileCanvas = worldCanvas.CaptureMouse();
        }

        private void worldCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!draggingTileCanvas) return;

            worldCanvas.ReleaseMouseCapture();
            draggingTileCanvas = false;
            SilverlightAccess.Dispatcher.BeginInvoke(viewModel.TileVisibleArea);
        }
        #endregion


        private void cboTilingSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            var newTiler = e.AddedItems[0] as string;
            
            if (newTiler == OpenStreetMapTiler.Name)
            {
                viewModel.TileDisplayerViewModel.Tiler = new OpenStreetMapTiler();
            }
            else if (newTiler == MapQuestTiler.Name)
            {
                viewModel.TileDisplayerViewModel.Tiler = new MapQuestTiler();
            }
            tileCredits.Content = viewModel.TileDisplayerViewModel.Tiler.Credits;
            tileCredits.NavigateUri = new Uri(viewModel.TileDisplayerViewModel.Tiler.CreditsURL);
            Dispatcher.BeginInvoke(viewModel.TileVisibleArea);
        }
    }

    public interface IDepictionViewer
    {
        IEnumerable<UIElement> FindElementsAtMousePosition(MouseEventArgs mouseEventArgs);
    }

    internal static class PointExtensionMethods
    {
        public static LatitudeLongitude ToLatLon(this SharpMap.Geometries.Point point)
        {
            return new LatitudeLongitude(point.Y, point.X);
        }
    }
}
