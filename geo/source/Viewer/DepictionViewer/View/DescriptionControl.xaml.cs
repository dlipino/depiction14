﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DepictionSilverlight.UI
{
    public partial class DescriptionControl
    {
        private readonly ImageSource VisibleImageSource;
        private readonly ImageSource InvisibleImageSource;

        public DescriptionControl()
        {
            InitializeComponent();
            VisibleImageSource = new BitmapImage(new Uri("../Icons/Visible.png", UriKind.Relative));
            InvisibleImageSource = new BitmapImage(new Uri("../Icons/HideIcon2.png", UriKind.Relative));
            DescriptionButtonImageSource = InvisibleImageSource;
        }

        private void showHideDescription_Click(object sender, RoutedEventArgs e)
        {
            if (DescriptionVisibility.Equals(Visibility.Visible))
                DescriptionVisibility = Visibility.Collapsed;
            else
                DescriptionVisibility = Visibility.Visible;

            DescriptionButtonImageSource = DescriptionVisibility == Visibility.Visible ? InvisibleImageSource : VisibleImageSource;
        }

        public ImageSource DescriptionButtonImageSource
        {
            get { return (ImageSource)GetValue(DescriptionButtonImageSourceProperty); }
            set { SetValue(DescriptionButtonImageSourceProperty, value); }
        }

        public static readonly DependencyProperty DescriptionButtonImageSourceProperty =
            DependencyProperty.Register("DescriptionButtonImageSource", typeof(ImageSource), typeof(DescriptionControl), new PropertyMetadata(null));


        public Visibility DescriptionVisibility
        {
            get { return (Visibility)GetValue(DescriptionVisibilityProperty); }
            set { SetValue(DescriptionVisibilityProperty, value); }
        }

        public static readonly DependencyProperty DescriptionVisibilityProperty =
            DependencyProperty.Register("DescriptionVisibility", typeof(Visibility), typeof(DescriptionControl), new PropertyMetadata(Visibility.Collapsed));

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public static readonly DependencyProperty DescriptionProperty =
            DependencyProperty.Register("Description", typeof(string), typeof(DescriptionControl), new PropertyMetadata(string.Empty, descriptionChanged));

        public Visibility ToggleVisibility
        {
            get { return (Visibility)GetValue(ToggleVisibilityProperty); }
            set { SetValue(ToggleVisibilityProperty, value); }
        }
        public static readonly DependencyProperty ToggleVisibilityProperty =
            DependencyProperty.Register("ToggleVisibilityProperty", typeof(Visibility), typeof(DescriptionControl), new PropertyMetadata(Visibility.Collapsed));

        private static void descriptionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var descriptionControl = d as DescriptionControl;
            if (descriptionControl == null) return;
            if (string.IsNullOrEmpty(e.NewValue as string) || ((string)e.NewValue).Equals("No description.", StringComparison.OrdinalIgnoreCase))
                descriptionControl.ToggleVisibility = Visibility.Collapsed;
            else
            {
                descriptionControl.ToggleVisibility = Visibility.Visible;
            }
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(DescriptionControl), new PropertyMetadata(string.Empty));

        private void Border_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var border = sender as FrameworkElement;
            if (border == null) return;
            border.Opacity = 0.8;
        }

        private void Border_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            var border = sender as FrameworkElement;
            if (border == null) return;
            border.Opacity = 0.5;
        }
    }
}
