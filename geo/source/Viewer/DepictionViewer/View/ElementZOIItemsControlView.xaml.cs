﻿using System.Windows.Controls;

namespace DepictionSilverlight.UI
{
    public partial class ElementZOIItemsControlView
    {
        public ElementZOIItemsControlView()
        {
            InitializeComponent();
            Canvas.SetZIndex(this, ViewModelZIndexs.BackdropZOIDisplayerZ);
        }

        //protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        //{
        //    base.PrepareContainerForItemOverride(element, item);
        //    var depictionElement = item as DepictionElement;
        //    var elementPresenter = element as ContentPresenter;

        //    if (depictionElement == null || elementPresenter == null) return;

        //    var binding = new Binding("ZIndex");
        //    elementPresenter.SetBinding(Canvas.ZIndexProperty, binding);

        //    if (depictionElement.ImageMetadata != null)
        //    {
        //        elementPresenter.ContentTemplate = (DataTemplate)Resources["ImageElement"];
        //    }
        //    else
        //    {
        //        if (item != null && element != null)
        //        {
        //            elementPresenter.ContentTemplate = (DataTemplate)Resources["IconAndZOIElement"];
        //        }
        //    }
        //}
    }
}
