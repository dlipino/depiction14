﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight.View
{
    public partial class ElementPermatextItemsControlView
    {
        public ElementPermatextItemsControlView()
        {
            InitializeComponent();
            Canvas.SetZIndex(this, ViewModelZIndexs.BackdropLabelDisplayerZ);
        }

        private void PermaText_Drag(object sender, DragDeltaEventArgs e)
        {
            var t = sender as Thumb;
            if (t == null) return;
            var dc = t.DataContext as DepictionElementMapViewModel;
            if (dc == null) return;
            dc.PermaTextX += e.HorizontalChange;
            dc.PermaTextY += e.VerticalChange;
            dc.PermaTextCenterX += e.HorizontalChange;
            dc.PermaTextCenterY += e.VerticalChange;
        }

        private void holdingGrid_Loaded(object sender, RoutedEventArgs e)
        {
            var t = sender as FrameworkElement;
            if (t == null) return;
            var dc = t.DataContext as DepictionElementMapViewModel;
            if (dc == null) return;
            dc.PermaTextCenterY = dc.PermaTextY + t.ActualHeight / 2;
            dc.PermaTextCenterX = dc.PermaTextX + t.ActualWidth / 2;
        }

        //the close button was removed but i have a feeling it could return
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            var t = sender as FrameworkElement;
            if (t == null) return;
            var dc = t.DataContext as DepictionElementMapViewModel;
            if (dc == null) return;
            dc.PermaHoverTextOn = Visibility.Collapsed;
        }

    }
}
