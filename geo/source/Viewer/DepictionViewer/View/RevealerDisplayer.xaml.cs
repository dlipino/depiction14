﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using DepictionSilverlight.Data;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.UI
{
    public partial class RevealerDisplayer
    {
        #region private fields

        private UIElement oldHoverTextValue;

        private DispatcherTimer timer = new DispatcherTimer();
        #endregion

        #region constructor

        public RevealerDisplayer()
        {
            InitializeComponent();
            Loaded += RevealerDisplayer_Loaded;
            Canvas.SetZIndex(this, ViewModelZIndexs.RevealerBorderThumbZ);
            timer.Tick += timer_Tick;
            timer.Interval = new TimeSpan(0,0,3);
        }

        void timer_Tick(object sender, EventArgs e)
        {
            var toolTip = ToolTipService.GetToolTip(this) as ToolTip;
            if (toolTip != null)
                toolTip.IsOpen = false;
            timer.Stop();
        }

        #endregion

        void RevealerDisplayer_Loaded(object sender, RoutedEventArgs e)
        {
            if (Parent == null) return;
            //Since this is a revealer, the real parent is two trips up.
            var firstParent = Parent as Panel;//the special revealer holder.
            if (firstParent == null) return;
            //var panelParent = firstParent.Parent as Panel;
            //if (panelParent == null) return;
            while (RevealerHoldingGrid.Children.Count != 0)
            {

                var child = RevealerHoldingGrid.Children[0];
                
                RevealerHoldingGrid.Children.Remove(child);
                firstParent.Children.Add(child);
                //originalChildren.Add(child);
                var itemsControl = child as ItemsControl;
                if (itemsControl != null) itemsControl.Tag = this;
                Canvas.SetZIndex(child, Canvas.GetZIndex(child) + 1);
                Binding binding = new Binding("DataContext");
                binding.Source = this;
                BindingOperations.SetBinding(child, DataContextProperty, binding);
            }
            //}
        }

        #region mouse events

        private void mouseEnter(object sender, MouseEventArgs e)
        {
            var revealerMetadata = ((FrameworkElement)sender).DataContext as RevealerMetadata;
            if (revealerMetadata != null)
                revealerMetadata.Stroke = new SolidColorBrush(Colors.Red);
        }
        private void mouseLeave(object sender, MouseEventArgs e)
        {
            var revealerMetadata = ((FrameworkElement)sender).DataContext as RevealerMetadata;
            if (revealerMetadata == null) return;

            revealerMetadata.Stroke = new SolidColorBrush(Colors.Orange);
            revealerMetadata.TitleOpacity = .4;

            var toolTip = ToolTipService.GetToolTip(this) as ToolTip;
            if (toolTip != null)
                toolTip.IsOpen = false;
        }

        private void collapseButton_Click(object sender, RoutedEventArgs e)
        {
            var collapseButton = sender as Button;
            if (collapseButton == null) return;

            var revealer = collapseButton.DataContext as RevealerMetadata;
            if (revealer == null) return;

            revealer.Visibility = revealer.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
        }

        #endregion

        #region Events used to setup the initial display for each item ...

        private void contentLoaded(object sender, RoutedEventArgs e)
        {
            var revealerRegion = sender as ContentControl;
            if (revealerRegion == null) return;

            var revealer = revealerRegion.DataContext as RevealerMetadata;
            if (revealer == null) return;

            revealerRegion.Template = revealer.RevealerShape.Equals(RevealerShapeType.Rectangle) ? rectangleRevealer : ellipseRevealer;
        }

        #endregion

        private void dragThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var frameworkElement = (FrameworkElement) sender;
            var name = frameworkElement.Name;
            RevealerMetadata revealerMetadata = frameworkElement.DataContext as RevealerMetadata;
            if (revealerMetadata == null) return;

            var left = revealerMetadata.Left;
            var top = revealerMetadata.Top;
            var w = revealerMetadata.Width;
            var h = revealerMetadata.Height;
            var horzChange = e.HorizontalChange;
            var vertChange = e.VerticalChange;
            var minWidth = 0;
            var minHeight = 0;

            if (name == null || name.Contains("main"))
            {
                left = left + horzChange;
                top = top + vertChange;
            }
            else
            {
                if (name.ToLower().Contains("left"))
                {
                    w -= horzChange;
                    if (w >= minWidth)
                    {
                        left += horzChange;
                        revealerMetadata.Width = w;
                        if (!revealerMetadata.RevealerShape.Equals(RevealerShapeType.Rectangle))
                        {
                            revealerMetadata.Height = w;
                        }
                    }
                    
                }
                else if (name.ToLower().Contains("right"))
                {
                    w += horzChange;
                    if (w > minWidth)
                    {
                        revealerMetadata.Width = w;
                        if (!revealerMetadata.RevealerShape.Equals(RevealerShapeType.Rectangle))
                        {
                            revealerMetadata.Height = w;
                        }
                    }

                }

                if (name.ToLower().Contains("top"))
                {
                    h -= vertChange;
                    if (h > minHeight)
                    {
                        revealerMetadata.Height = h;
                        top += vertChange;
                        if (!revealerMetadata.RevealerShape.Equals(RevealerShapeType.Rectangle))
                        {
                            revealerMetadata.Width = h;
                        }
                    }
                }
                else if (name.ToLower().Contains("bottom"))
                {
                    h += vertChange;
                    if (h > minHeight)
                    {
                        revealerMetadata.Height = h;

                        if (!revealerMetadata.RevealerShape.Equals(RevealerShapeType.Rectangle))
                        {
                            revealerMetadata.Width = h;
                        }
                    }
                }
            }
            revealerMetadata.Left = left;
            revealerMetadata.Right = left + w;
            revealerMetadata.Top = top;

            revealerMetadata.RecalculateClipGeometry();

        }

        private void main_MouseMove(object sender, MouseEventArgs e)
        {
            var thumb = sender as Thumb;
            if (thumb != null && thumb.IsDragging)
                return;
            // Determine hover text inside a revealer ...
            bool foundHoverText = false;
            string hoverText = null;
            IEnumerable<UIElement> uiElements = SilverlightAccess.DepictionViewer.FindElementsAtMousePosition(e);
            UIElement hovertextElement = null;
            foreach (UIElement uiElement in uiElements)
            {
                var frameworkElement = uiElement as FrameworkElement;
                if (frameworkElement == null || frameworkElement.DataContext == null) continue;

                // Check if we're over an element that should have hover text displayed
                if (frameworkElement.DataContext is DepictionElement)
                {
                    hovertextElement = uiElement;
                    hoverText = ((DepictionElement)frameworkElement.DataContext).HoverText;
                    foundHoverText = true;
                    break;
                }
            }

            var toolTip = ToolTipService.GetToolTip(this) as ToolTip;

            if (toolTip != null)
            {
                if (foundHoverText && !string.IsNullOrEmpty(hoverText))
                {
                    if (hovertextElement != oldHoverTextValue)
                    {
                        oldHoverTextValue = hovertextElement;
                        toolTip.Content = hoverText;
                        toolTip.IsOpen = false;
                        toolTip.IsOpen = true;
                    }
                }
                else
                {
                    oldHoverTextValue = null;
                    toolTip.Content = string.Empty;
                    toolTip.IsOpen = false;
                }
            }
            else
            {
                if (foundHoverText && !string.IsNullOrEmpty(hoverText))
                {
                    var tooltip = new ToolTip { Content = hoverText };

                    ToolTipService.SetToolTip(this, tooltip);
                }
                timer.Start();
            }
        }

        private void main_DragStarted(object sender, DragStartedEventArgs e)
        {
            var thumb = (Thumb)sender;
            thumb.MouseMove -= main_MouseMove;
        }

        private void main_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            var thumb = (Thumb)sender;
            thumb.MouseMove += main_MouseMove;
        }

        private void titleBar_MouseEnter(object sender, MouseEventArgs e)
        {
            var border = sender as FrameworkElement;
            if (border == null) return;
            border.Opacity = 0.8;
        }

        private void titleBar_MouseLeave(object sender, MouseEventArgs e)
        {
            var border = sender as FrameworkElement;
            if (border == null) return;
            border.Opacity = 0.4;
        }

    }
}