﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DepictionSilverlight
{
    public static class GeneralExtensionMethods
    {
        public static T Cast<T>(this object obj, T type)
        {
            return (T)obj;
        }
    }
}
