﻿using System.Windows.Threading;
using DepictionSilverlight.UI;
using DepictionSilverlight.View;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight
{
    public static class SilverlightAccess
    {
        public static MercatorCoordinateConverter PixelConverter { get; set; }
        
        public static VisualAccess VisualAccess { get; set; }
        public static Dispatcher Dispatcher { get; set; }
        public static double ApplicationWidth { get; set; }

        public static double ApplicationHeight { get; set; }

        public static DepictionViewerViewModel DepictionViewerViewModel { get; set; }

        public static IDepictionViewer DepictionViewer { get; set; }
    }
}
