﻿using System;
using System.Globalization;
using System.Reflection;
using System.Windows.Media;

namespace DepictionSilverlight.Services
{
    public class ColorConverter
    {
        public static Color ConvertFromString(string value)
        {
            Type colorType = (typeof(Colors));
            if (colorType.GetProperty(value) != null)
            {
                object o = colorType.InvokeMember(value, BindingFlags.GetProperty, null, null, null);
                if (o != null)
                {
                    return (Color) o;
                }
            }
            string val = value;
            val = val.Replace("#", "");
            val = val.Replace("sc", "");
            byte a, r, g, b;
            a = Convert.ToByte("ff", 16);
            byte pos = 0;
            string[] delimiters;
            if ((delimiters = val.Split(',')).Length > 1)
            {
                a = Convert.ToByte(Convert.ToSingle(delimiters[0])*255);
                r = Convert.ToByte(Convert.ToSingle(delimiters[1])*255);
                g = Convert.ToByte(Convert.ToSingle(delimiters[2])*255);
                b = Convert.ToByte(Convert.ToSingle(delimiters[3])*255);
                return Color.FromArgb(a, r, g, b);
                
            }
            if (val.Length == 8)
            {
                a = Convert.ToByte(val.Substring(pos, 2), 16);
                pos = 2;
            }

            r = Convert.ToByte(val.Substring(pos, 2), 16);
            pos += 2;

            g = Convert.ToByte(val.Substring(pos, 2), 16);
            pos += 2;

            b = Convert.ToByte(val.Substring(pos, 2), 16);

            return Color.FromArgb(a, r, g, b);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = value as SolidColorBrush;
            return "#" + val.Color.A + val.Color.R + val.Color.G + val.Color.B;
        }

    }
}