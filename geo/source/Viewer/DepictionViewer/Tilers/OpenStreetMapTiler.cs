﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.Tilers
{
    public class MapQuestTiler : OpenStreetMapTilerBase
    {
        private string mapquestURL = "http://oatile{0}.mqcdn.com/tiles/1.0.0/sat/{1}/{2}/{3}.jpg";
        public override string URL { get { return mapquestURL; } }
        public override string Credits { get { return "Tiles provided by MapQuest"; } }
        public override string CreditsURL { get { return "http://www.mapquest.com"; } }

        public static string Name { get { return "Imagery (MapQuest)"; } }
        protected override bool useLetters { get { return false; } }
    }
    public class OpenStreetMapTiler : OpenStreetMapTilerBase
    {
        public override string URL { get { return "http://{0}.tile.openstreetmap.org/{1}/{2}/{3}.png"; } }
        public override string Credits { get { return "Tiles provided by OpenStreetMap"; } }
        public override string CreditsURL { get { return "http://www.openstreetmap.org"; } }

        protected override bool useLetters { get { return true; } }

        public static string Name { get { return "Street Map (OpenStreetMap)"; } }
    }

    public abstract class OpenStreetMapTilerBase : ITiler
    {
        public abstract string URL { get; }
        public abstract string Credits { get; }
        public abstract string CreditsURL { get; }
        protected abstract bool useLetters { get; }
        private static int LongitudeToColAtZoom(LatitudeLongitude latLong, int zoom)
        {
            int col = (int)(Math.Floor((latLong.Longitude + 180.0) / 360.0 * Math.Pow(2.0, zoom)));
            if (col >= (int)Math.Pow(2, zoom))
                col = (int)Math.Pow(2, zoom) - 1;
            if (col <= 0)
                col = 0;
            return col;
        }

        private static int LatitudeToRowAtZoom(LatitudeLongitude latLong, int zoom)
        {
            int row = (int)(Math.Floor((1.0 - Math.Log(Math.Tan(latLong.Latitude* Math.PI / 180.0) + 1.0 / Math.Cos(latLong.Latitude * Math.PI / 180.0)) / Math.PI) / 2.0 * Math.Pow(2.0, zoom)));
            if (row >= (int)Math.Pow(2, zoom))
                row = (int)Math.Pow(2, zoom) - 1;
            if (row <= 0)
                row = 0;
            return row;
        }

        private static double TileColToTopLeftLong(int col, int zoomLevel)
        {
            return col/Math.Pow(2.0, zoomLevel)*360.0 - 180;
        }

        private static double TileRowToTopLeftLat(int row, int zoom)
        {
            double n = Math.PI - 2.0*Math.PI*row/Math.Pow(2.0, zoom);
            return 180.0/Math.PI*Math.Atan(0.5*(Math.Exp(n) - Math.Exp(-n)));
        }

        private Random random = new Random();
        
        private string GetTileUrl(int row, int column, int zoom)
        {
            int next = random.Next(1, 4);
            object nextObj;
            if (useLetters)
                nextObj = Convert.ToChar(next + 96);
            else
                nextObj = next;

            return string.Format(CultureInfo.InvariantCulture, URL, nextObj, zoom, column, row);
        }

        public int GetZoomLevel(BoundingBox boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int i;

            for (i = 1; i < maxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right)*(1 << i)/360D;
                if (tilesAcross > minTilesAcross) break;
            }

            return i;
        }

        #region methods
        
        public IList<TileModel> GetTiles(BoundingBox boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = LatitudeToRowAtZoom(boundingBox.TopRight, zoomLevel);
            int col = LongitudeToColAtZoom(boundingBox.BottomLeft, zoomLevel);
            int lastCol = LongitudeToColAtZoom(boundingBox.TopRight, zoomLevel);
            int lastRow = LatitudeToRowAtZoom(boundingBox.BottomLeft, zoomLevel);

            var tiles = new List<TileModel>();

            var tilesToGet = TilesToGet(lastCol, col, lastRow, row);
            double widthInDegrees = TileColToTopLeftLong(col + 1, zoomLevel) - TileColToTopLeftLong(col, zoomLevel);

            foreach (var tileToGet in tilesToGet)
            {
                double heightInDegrees = TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);

                var uri = new Uri(GetTileUrl(tileToGet.Row, tileToGet.Column, zoomLevel));
                var tile = new TileModel(zoomLevel, 
                                uri, 
                                new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel),TileColToTopLeftLong(tileToGet.Column, zoomLevel)), 
                                new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
                tiles.Add(tile);
            }

            return tiles;
        }
        

        //<summary>
         //Returns the list of tiles to get ordered in increasing distance from the center
         //of the area being tiled.
         //</summary>
        private IEnumerable<TileXY> TilesToGet(int lastCol, int col, int lastRow, int row)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j }));
                }
            }

            IEnumerable<TileXY> sortedTiles = 
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }

        #endregion

        private struct TileXY
        {
            public int Column { get; set; }
            public int Row { get; set; }
        }
    }
}