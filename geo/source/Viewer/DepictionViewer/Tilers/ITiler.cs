using System;
using System.Collections.Generic;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.Tilers
{
    public interface ITiler
    {
        int GetZoomLevel(BoundingBox boundingBox, int minTilesAcross, int maxZoomLevel);
        IList<TileModel> GetTiles(BoundingBox boundingBox, int minTilesAcross, int maxZoomLevel);
        string Credits { get; }
        string CreditsURL { get; }
    }
}