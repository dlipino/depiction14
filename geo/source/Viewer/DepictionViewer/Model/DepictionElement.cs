﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.Data
{
    public class DepictionElement : INotifyPropertyChanged
    {
        #region fields

        private int zIndex;

        #endregion

        #region constructor

        public DepictionElement()
        {
            Opacity = 1;
        }





        #endregion

        #region properties

        public int ZIndex
        {
            get { return zIndex; }
            set
            {
                zIndex = value;
                NotifyPropertyChanged("ZIndex");
            }
        }
        
        public string HoverText { get; set; }
        public ImageSource IconSource { get; set; }
        public ZoneOfInfluence ZoneOfInfluence { get; set; }
        //public DepictionElementMapViewModel DepictionElementMapViewModel { get; set; }
        public LatitudeLongitude Location { get; set; }
        public string FillColor { get; set; }
        public string StrokeColor { get; set; }
        public ImageMetadata ImageMetadata { get; set; }
//        public Visibility IconVisibility { get { return ImageMetadata != null ? Visibility.Collapsed : Visibility.Visible; } }
        private Visibility iconVisibility;
        public Visibility IconVisibility
        {
            get
            {
                return iconVisibility;
            }
            set
            {
                iconVisibility = value;
                NotifyPropertyChanged("IconVisibility");
            }
        }
        public Visibility ImageVisibility { get { return ImageMetadata == null ? Visibility.Collapsed : Visibility.Visible; } }
        public IconData IconData { get; set; }
        public double IconSize { get; set; }
        public string[] RevealerIds { get; set; }
        public bool InMainCanvas { get; set; }
        public double Opacity { get; private set; }
        public Visibility Active { get; set; }
        public SolidColorBrush BorderColor { get; set; }
        public DepictionIconBorderShape BorderShape { get; set; }

        public string TypeDisplayName { get; set; }
        public IList<ElementWaypoint> ElementWaypoints { get; set; }
        
        private Visibility permaHoverTextOn;
        public Visibility PermaHoverTextOn 
        { 
            get
            {
                return permaHoverTextOn;
            }
            set
            {
                permaHoverTextOn = value;
                NotifyPropertyChanged("PermaHoverTextOn");
            }
        }

        private double permaTextX;
        public double PermaTextX
        {
            get { return permaTextX; }
            set
            {
                permaTextX = value;
                NotifyPropertyChanged("PermaTextX");
            }
        }

        private double permaTextY;
        public double PermaTextY
        {
            get { return permaTextY; }
            set
            {
                permaTextY = value;
                NotifyPropertyChanged("PermaTextY");
            }
        }

        private double permaTextCenterX;
        public double PermaTextCenterX
        {
            get { return permaTextCenterX; }
            set
            {
                permaTextCenterX = value;
                NotifyPropertyChanged("PermaTextCenterX");
            }
        }

        private double permaTextCenterY;
        public double PermaTextCenterY
        {
            get { return permaTextCenterY; }
            set
            {
                permaTextCenterY = value;
                NotifyPropertyChanged("PermaTextCenterY");
            }
        }

        #endregion


        #region methods

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void DoAnimate()
        {
            if (Animate != null)
                Animate(this);
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region events

        public event Action<DepictionElement> Animate;

        #endregion
    }
}