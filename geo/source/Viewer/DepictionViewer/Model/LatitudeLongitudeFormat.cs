﻿namespace DepictionSilverlight.Data
{
    public enum LatitudeLongitudeFormat
    {
        ColonFractionalSeconds,
        ColonIntegerSeconds,
        Decimal,
        DegreesFractionalMinutes,
        DegreesMinutesSeconds,
        DegreesWithDCharMinutesSeconds,
        SignedDecimal,
        SignedDegreesFractionalMinutes,
    }
}