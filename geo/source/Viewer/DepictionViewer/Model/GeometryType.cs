﻿namespace DepictionSilverlight.Model
{
    public enum GeometryType
    {
        Polygon,
        LineString,
        Point,
        MultiLineString,
        MultiPolygon
    }
}