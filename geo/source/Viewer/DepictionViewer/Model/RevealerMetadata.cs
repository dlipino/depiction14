﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using DepictionSilverlight.Data;
using DepictionSilverlight.ViewModel;

namespace DepictionSilverlight.Model
{
    public enum RevealerShapeType
    {
        Rectangle, Circle
    }

    public class RevealerMetadata : INotifyPropertyChanged
    {
        #region fields

        private LatitudeLongitude bottomRight;
        private Point bottomRightScreenPoint;
        private SolidColorBrush stroke;
        private double titleOpacity;
        private LatitudeLongitude topLeft;
        private Point topLeftScreenPoint;
        private Visibility visibility = Visibility.Visible;
        private Visibility borderVisibility = Visibility.Visible;
        private double zIndex;

        #endregion

        #region constructor

        public RevealerMetadata()
        {
            ExpandCollapseImage = DepictionData.Instance.InvisibleImageSource;
            ExpandCollapseTooltip = "Collapse";
            Stroke = new SolidColorBrush(Colors.Orange);
            TitleOpacity = .4;
            SilverlightAccess.VisualAccess.PropertyChanged += VisualAccess_PropertyChanged;
            ToolbarHeight = 25;
            
        }

        void VisualAccess_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("OnePixelWidthOnWorldCanvas"))
            {
                StrokeThickness = 2 * ((VisualAccess) sender).OnePixelWidthOnWorldCanvas;
                InverseScale = new ScaleTransform
                                     {
                                         ScaleX = ((VisualAccess) sender).OnePixelWidthOnWorldCanvas,
                                         ScaleY = ((VisualAccess) sender).OnePixelWidthOnWorldCanvas
                                     };
                
                ToolbarTop = topLeftScreenPoint.Y - ToolbarHeight * InverseScale.ScaleY;
            }
        }

        #endregion

        #region properties

        public IEnumerable<DepictionElementMapViewModel> IconElements { get; set; }
        public IEnumerable<DepictionElementMapViewModel> ImageElements { get; set; }
        public IEnumerable<DepictionElementMapViewModel> ZoiElements { get; set; }
        public LatitudeLongitude TopLeft
        {
            get { return topLeft; }
            set
            {
                topLeft = value;
                topLeftScreenPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(TopLeft);
            }
        }

        public LatitudeLongitude BottomRight
        {
            get { return bottomRight; }
            set
            {
                bottomRight = value;
                bottomRightScreenPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(bottomRight);
            }
        }

        public double ZIndex
        {
            get { return zIndex; }
            set
            {
                zIndex = value;
                NotifyPropertyChanged("ZIndex");
            }
        }


        public double TitleOpacity
        {
            get { return titleOpacity; }
            set
            {
                titleOpacity = value;
                NotifyPropertyChanged("TitleOpacity");
            }
        }

        public SolidColorBrush Stroke
        {
            get { return stroke; }
            set
            {
                stroke = value;
                NotifyPropertyChanged("Stroke");
            }
        }

        private ScaleTransform inverseScale;
        public ScaleTransform InverseScale
        {
            get { return inverseScale; }
            set
            {
                inverseScale = value;
                NotifyPropertyChanged("InverseScale");
            }
        }

        private double strokeThickness;
        public double StrokeThickness
        {
            get { return strokeThickness; }
            set
            {
                strokeThickness = value;
                NotifyPropertyChanged("StrokeThickness");
            }
        }

        public Visibility Visibility
        {
            get { return visibility; }
            set
            {
                visibility = value;

                if (visibility == Visibility.Collapsed)
                {
                    ExpandCollapseImage = DepictionData.Instance.VisibleImageSource;
                    ExpandCollapseTooltip = "Expand";
                }
                else
                {
                    ExpandCollapseImage = DepictionData.Instance.InvisibleImageSource;
                    ExpandCollapseTooltip = "Collapse";
                }

                NotifyPropertyChanged("Visibility");
                NotifyPropertyChanged("ExpandCollapseImage");
                NotifyPropertyChanged("ExpandCollapseTooltip");
                NotifyPropertyChanged("ClipGeometry");
            }
        }

        public Visibility BorderVisible
        {
            get { return borderVisibility; }
            set
            {
                borderVisibility = value;
                NotifyPropertyChanged("BorderVisible");
            }
        }

        public Geometry ClipGeometry
        {
            get
            {
                Geometry clipGeometry;

                if (RevealerShape.Equals(RevealerShapeType.Rectangle))
                {
                    clipGeometry = new RectangleGeometry
                                       {
                                           Rect =
                                               Visibility == Visibility.Collapsed
                                                   ? new Rect(topLeftScreenPoint.X, topLeftScreenPoint.Y, 0, 0)
                                                   : new Rect(topLeftScreenPoint.X, topLeftScreenPoint.Y, Width, Height)
                    };
                }
                else
                {
                    var center = new Point((Left + Left +  Width) / 2, (Top + Top + Height) / 2);
                    clipGeometry = new EllipseGeometry {
                        Center = center,
                        RadiusX = Visibility == Visibility.Collapsed ? 0 : center.X - Left,
                        RadiusY = Visibility == Visibility.Collapsed ? 0 : center.Y - Top
                    };
                }

                return clipGeometry;
            }
        }

        public string RevealerId { get; set; }
        private double left;
        public double Left
        {
            get { return left; }
            set
            {
                left = value;
                NotifyPropertyChanged("Left");
                topLeftScreenPoint.X = value;
            }
        }

        private double right;
        public double Right
        {
            get { return right; }
            set
            {
                right = value;
                NotifyPropertyChanged("Right");
            }
        }

        private double top;
        public double Top
        {
            get { return top; }
            set
            {
                top = value;
                NotifyPropertyChanged("Top");
                //argh
                ToolbarTop = value - ToolbarHeight * StrokeThickness / 2;
                topLeftScreenPoint.Y = value;


            }
        }

        private double width;
        public double Width
        {
            get { return width; }
            set
            {
                width = value;
                NotifyPropertyChanged("Width");
                bottomRightScreenPoint.X = Left + value;
            }
        }

        private double height;
        public double Height
        {
            get { return height; }
            set
            {
                height = value;
                NotifyPropertyChanged("Height");
                bottomRightScreenPoint.Y = Top + value;
            }
        }

        public ImageSource ExpandCollapseImage { get; set; }
        public string ExpandCollapseTooltip { get; set; }
        public double ToolbarHeight { get; private set; }
        private double toolbarTop;
        public double ToolbarTop
        {
            get { return toolbarTop; }
            private set
            {
                toolbarTop = value;

                NotifyPropertyChanged("ToolbarTop");
            }
        }

        public string RevealerName { get; set; }

        private double opacity;
        public double Opacity
        {
            get
            {
                return opacity;
            } 
            set
            {
                opacity = value;
                NotifyPropertyChanged("Opacity");
            }
        }

        public RevealerShapeType RevealerShape { get; set; }

        public bool Moveable { get; set; }

        #endregion

        #region event handlers

        public void GeoConversionsViewportChanged()
        {
            bottomRightScreenPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(bottomRight);
            topLeftScreenPoint = SilverlightAccess.PixelConverter.WorldToGeoCanvas(TopLeft);
            ChangeExtents();
        }

        #endregion

        #region methods

        public void RecalculateClipGeometry()
        {
            NotifyPropertyChanged("ClipGeometry");
        }

        private void ChangeExtents()
        {
            Left = topLeftScreenPoint.X;
            Right = bottomRightScreenPoint.X;
            Top = topLeftScreenPoint.Y;
            ToolbarTop = topLeftScreenPoint.Y - ToolbarHeight * StrokeThickness / 2;
            Width = Math.Abs(topLeftScreenPoint.X - bottomRightScreenPoint.X);
            Height = Math.Abs(topLeftScreenPoint.Y - bottomRightScreenPoint.Y);

            NotifyPropertyChanged("Left");
            NotifyPropertyChanged("Top");
            NotifyPropertyChanged("Width");
            NotifyPropertyChanged("Height");
            NotifyPropertyChanged("ClipGeometry");
            NotifyPropertyChanged("ToolbarTop");
        }

        private void NotifyPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}