﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DepictionSilverlight.UI;
using DepictionSilverlight.Model;
using System.Collections.Generic;

namespace DepictionSilverlight.Data
{
    public class DepictionData
    {
        #region fields

        private static DepictionData instance;

        #endregion

        #region constructor

        private DepictionData()
        {
            VisibleImageSource = new BitmapImage(new Uri("../Icons/Visible.png", UriKind.Relative));
            InvisibleImageSource = new BitmapImage(new Uri("../Icons/HideIcon2.png", UriKind.Relative));
        }

        #endregion

        #region properties

        public static DepictionData Instance
        {
            get
            {
                if (instance == null)
                    instance = new DepictionData();

                return instance;
            }
        }

        public IList<DepictionElement> Elements { get; set; }
        public LatitudeLongitude RegionTopLeft { get; set; }
        public LatitudeLongitude RegionBottomRight { get; set; }
        public LatitudeLongitude ViewportTopLeft { get; set; }
        public LatitudeLongitude ViewportBottomRight { get; set; }
        public IList<RevealerMetadata> Revealers { get; set; }
        public IList<Annotation> Annotations { get; set; }
        public ImageSource InvisibleImageSource { get; private set; }
        public ImageSource VisibleImageSource { get; private set; }
        public static LatitudeLongitudeFormat LatitudeLongitudeFormat { get; set; }
        public static LatitudeLongitudeSeparator LatitudeLongitudeSeparator { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }

        public LegendType LegendType { get; set; }

        public BackgroundBlackoutModel BackgroundBlackoutModel { get; set; }

        #endregion
    }
}