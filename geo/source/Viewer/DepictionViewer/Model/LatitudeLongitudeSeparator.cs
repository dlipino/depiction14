﻿namespace DepictionSilverlight.Data
{
    public enum LatitudeLongitudeSeparator
    {
        CommaAndSpace,
        Space,
        Comma,
    }
}