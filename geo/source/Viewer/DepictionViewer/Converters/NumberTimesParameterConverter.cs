﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DepictionSilverlight.Converters
{
    public class NumberTimesParameterConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double inValue;
            double inParam;
            if (!double.TryParse(value.ToString(), NumberStyles.Number, culture, out inValue) ||
                !double.TryParse(parameter.ToString(), NumberStyles.Number, culture, out inParam))
                return value;
            return inValue * inParam;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
