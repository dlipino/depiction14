﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using DepictionSilverlight.Tilers;

namespace DepictionSilverlight
{
    public class RegionMetadata : INotifyPropertyChanged
    {
        #region fields

        private double height;
        private string latLong;
        private double left;
        private double top;
        private double width;
        private double windowHeight;
        private double windowWidth;
        //private double infoBarTop;

        #endregion

        #region contstructor

        public RegionMetadata()
        {
            DepictionLogoImageSource = new BitmapImage(new Uri("../Icons/DepictionLogoWhite.png", UriKind.Relative));
            SilverlightAccess.VisualAccess.PropertyChanged += VisualAccess_PropertyChanged;
            
        }

        void VisualAccess_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("OnePixelWidthOnWorldCanvas"))
            {
                StrokeThickness = 3 * ((VisualAccess)sender).OnePixelWidthOnWorldCanvas;
            }
        }

        #endregion

        #region properties

        private BoundingBox regionExtent;
        public BoundingBox RegionExtent
        {
            get { return regionExtent; }
            set
            {
                regionExtent = value;
                NotifyPropertyChanged("RegionExtent");
            }
        }

        private double strokeThickness;
        public double StrokeThickness
        {
            get { return strokeThickness; }
            set
            {
                strokeThickness = value;
                NotifyPropertyChanged("StrokeThickness");
            }
        }


        public double Width
        {
            get { return width; }
            set
            {
                width = value;
                NotifyPropertyChanged("Width");
            }
        }

        public double Height
        {
            get { return height; }
            set
            {
                height = value;
                NotifyPropertyChanged("Height");
            }
        }

        public double Top
        {
            get { return top; }
            set
            {
                top = value;
                NotifyPropertyChanged("Top");
            }
        }

        public double Left
        {
            get { return left; }
            set
            {
                left = value;
                NotifyPropertyChanged("Left");
            }
        }

        private double right;
        public double Right
        {
            get { return right; }
            set
            {
                right = value;
                NotifyPropertyChanged("Right");
            }
        }

        private double bottom;
        public double Bottom
        {
            get { return bottom; }
            set
            {
                bottom = value;
                NotifyPropertyChanged("Bottom");
            }
        }

        public double WindowWidth
        {
            get { return windowWidth; }
            set
            {
                windowWidth = value;
                NotifyPropertyChanged("WindowWidth");
            }
        }

        public double WindowHeight
        {
            get { return windowHeight; }
            set
            {
                windowHeight = value;
                NotifyPropertyChanged("WindowHeight");
            }
        }

        //public double InfoBarTop
        //{
        //    get { return infoBarTop; }
        //    set
        //    {
        //        infoBarTop = value;
        //        NotifyPropertyChanged("InfoBarTop");
        //    }
        //}

        public ImageSource DepictionLogoImageSource { get; private set; }


        
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region methods

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}