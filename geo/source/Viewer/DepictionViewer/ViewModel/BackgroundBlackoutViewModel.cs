﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.ViewModel
{
    public class BackgroundBlackoutViewModel : ViewModelBase
    {
        private GeometryGroup clip;
        private BackgroundBlackoutModel backgroundBlackoutModel;

        public BackgroundBlackoutViewModel(BackgroundBlackoutModel backgroundBlackoutModel)
        {
            
            this.backgroundBlackoutModel = backgroundBlackoutModel;
        }
        public Visibility Visibility { get { return backgroundBlackoutModel.Visibility; } }

        public GeometryGroup Clip
        {
            get
            {
                return clip;
            }
            set
            {
                clip = value;
                OnNotifyPropertyChanged("Clip");
            }
        }

    }
}
