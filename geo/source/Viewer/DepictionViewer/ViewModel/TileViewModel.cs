﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using DepictionSilverlight.Model;

namespace DepictionSilverlight.ViewModel
{
    public class TileViewModel : IEquatable<TileViewModel>
    {
        private readonly TileModel tileModel;

        public TileViewModel(TileModel tileModel)
        {
            this.tileModel = tileModel;
            Point topLeftPos = SilverlightAccess.PixelConverter.WorldToGeoCanvas(tileModel.TopLeft);
            Point bottomRight = SilverlightAccess.PixelConverter.WorldToGeoCanvas(tileModel.BottomRight);
            Top = topLeftPos.Y;
            Left = topLeftPos.X;
            Width = Math.Abs(bottomRight.X - topLeftPos.X);
            Height = Math.Abs(topLeftPos.Y - bottomRight.Y);
        }

        public TileModel TileModel { get { return tileModel; } }

        public double Width { get; set; }
        public double Height { get; set; }
        public double Top { get; set; }
        public double Left { get; set; }

        public BitmapSource TileSource { get; set; }

        public bool ImageOpened { get; set; }

        public int ZoomLevel { get { return tileModel.TileZoomLevel; } }
        public bool Equals(TileViewModel other)
        {
            return Top.Equals(other.Top) && Left.Equals(other.Left) && tileModel.TileZoomLevel.Equals(other.tileModel.TileZoomLevel);
        }
    }
}
