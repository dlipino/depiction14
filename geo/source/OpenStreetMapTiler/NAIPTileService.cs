using System;
using System.ComponentModel.Composition;
using System.Globalization;
using System.IO;
using System.Windows;
using Depiction.APINew.Interfaces;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.Service;
using Depiction.APINew.TileServiceHelpers;
using Depiction.APINew.ValueTypes;
using Depiction.CoreModel.ValueTypes;

namespace OpenStreetMapTiler
{
    [Export(typeof(ITileProvider))]
    public class NAIPTileService : TileProvider
    {
        private const int MinimumTilesAcross = 4;
        private const int MaximumZoomLevel = 17;

        private NAIP_Coverage_MapServer server;
        private NAIP_Coverage_MapServer Server
        {
            get
            {
                if (server == null)
                    server = new NAIP_Coverage_MapServer();
                return server;
            }
        }

        #region Overrides of TileProvider

        protected override double PixelsPerTile
        {
            get { return 256; }
        }
                #region Constructor

        public NAIPTileService()
        {
            cacheService = new TileCacheService(@"C:\tileCache\NAIPTiles"); 
        }

        #endregion
        protected override int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / ((1 << zoom));
            double metersX = EARTHRADIUS * DegToRad(latLong.Longitude);
            var x = (int)((EARTHHALFCIRC + metersX) / eqMetersPerTile);
            return x;
        }

        protected override int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            double latRad = DegToRad(latLong.Latitude);
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double prj = Math.Log(Math.Tan(latRad) + 1 / Math.Cos(latRad));
            double metersY = EARTHRADIUS * prj;
            var y = (int)((EARTHHALFCIRC - metersY) / eqMetersPerTile);
            return y;
        }

        protected override double TileColToTopLeftLong(int col, int zoomLevel)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoomLevel);
            double metersX = (eqMetersPerTile * col) - EARTHHALFCIRC;
            double lonRad = metersX / EARTHRADIUS;
            return RadToDeg(lonRad);
        }

        protected override double TileRowToTopLeftLat(int row, int zoom)
        {
            double eqMetersPerTile = EARTHCIRCUM / (1 << zoom);
            double metersY = EARTHHALFCIRC - (row * eqMetersPerTile);
            double latRad = Math.Atan(Math.Sinh(metersY / EARTHRADIUS));
            double latDeg = RadToDeg(latRad);
            return latDeg;
        }

        protected override TileModel GetTile(int row, int column, int zoom)
        {
            var topLeft = new LatitudeLongitude(TileRowToTopLeftLat(row, zoom),
                                                TileColToTopLeftLong(column, zoom));
            var bottomRight = new LatitudeLongitude(TileRowToTopLeftLat(row + 1, zoom),
                                                    TileColToTopLeftLong(column + 1, zoom));

            var cacheName = "NAIPV2_" + row + "_" + column + "_" + zoom + ".jpg";

            string imagePath = cacheService.GetCacheFullStoragePath(cacheName);

            if (File.Exists(imagePath))
                return new TileModel(zoom, cacheName, imagePath, topLeft, bottomRight);


            var tileImage =
                BitmapSaveLoadService.DownLoadImageFromURI(
                    string.Format(CultureInfo.InvariantCulture,
                                  "http://wms.jpl.nasa.gov/wms.cgi?request=GetMap&layers=global_mosaic&srs=EPSG:4326&width=512&height=512&bbox={0},{1},{2},{3}&format=image/jpeg&version=1.1.1&styles=visual",
                                  west, south, east, topLeft.), 256, 256);

            cacheService.SaveTileImage(tileImage, cacheName);

            var boundingBox = new MapCoordinateBounds(topLeft, bottomRight);
            boundingBox.MapImageSize = new Size(256, 256);
            var imageElementInfo = GetNAIPImage(boundingBox);


            return new TileModel(zoom, cacheName,
                                DepictionAccess.CacheTile((Bitmap)imageElementInfo.Map, tileKey),
                                imageElementInfo.TopLeft, imageElementInfo.BottomRight);
        }

        protected override int GetZoomLevel(IMapCoordinateBounds boundingBox)
        {
            int i;

            for (i = 1; i < MaximumZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right) * (1 << i) / 360D;
                if (tilesAcross > MinimumTilesAcross) break;
            }

            return i;
        }

        public override string UserReadableSourceName
        {
            get { return "NAIP Tile Service"; }
        }

        public override string SourceName
        {
            get { return "NAIPV2"; }
        }

        #endregion

        #region Helper methods

        public ImageElementInfo GetNAIPImage(IMapCoordinateBounds boundingBox)
        {
            BoundingBox resultingBox;
            var bitmap = GetNAIPBitmap(boundingBox, out resultingBox);
            ImageElementInfo imageElementInfo = new ImageElementInfo("NAIP", "Imagery (NAIP)");
            imageElementInfo.SetMap(resultingBox.TopLeft, resultingBox.BottomRight, bitmap);
            imageElementInfo.Crop(boundingBox, boundingBox.MapImageSize.Width, boundingBox.MapImageSize.Height);
            return imageElementInfo;
        }
        private Bitmap GetNAIPBitmap(IMapCoordinateBounds boundingBox, out IMapCoordinateBounds resultingBox)
        {
            resultingBox = new MapCoordinateBounds();
            try
            {


                var defaultMapName = Server.GetDefaultMapName();
                MapDescription mapDescription = new MapDescription();
                //var geographicCoordinateSystem = new GeographicCoordinateSystem() { };

                var mapExtent = new MapExtent();
                var envelope = new EnvelopeN();
                envelope.XMin = boundingBox.Left;
                envelope.XMax = boundingBox.Right;
                envelope.YMin = boundingBox.Bottom;
                envelope.YMax = boundingBox.Top;
                mapExtent.Extent = envelope;
                mapDescription.MapArea = mapExtent;
                //envelope.SpatialReference = geographicCoordinateSystem;

                //var extent = new EnvelopeN() { XMin = -132.342642270881, XMax = -60.4046960034264, YMin = 1.68104843656771, YMax = 73.6189947040223 } };

                mapDescription.Name = defaultMapName;
                LayerDescription[] layerDescriptions = new[] 
                { 
                    new LayerDescription { LayerID = 2, Visible = false},
                    new LayerDescription { LayerID = 3, Visible = false},
                    new LayerDescription { LayerID = 4, Visible = false},
                    new LayerDescription { LayerID = 5, Visible = false},
                    new LayerDescription { LayerID = 6, Visible = false},
                    new LayerDescription { LayerID = 7, Visible = false},
                    new LayerDescription { LayerID = 8, Visible = false},
                    new LayerDescription { LayerID = 393, Visible = true},
                    new LayerDescription { LayerID = 394, Visible = true},
                    new LayerDescription { LayerID = 395, Visible = true},
                    new LayerDescription { LayerID = 396, Visible = true},
                    new LayerDescription { LayerID = 397, Visible = true},
                    new LayerDescription { LayerID = 398, Visible = true},
                    new LayerDescription { LayerID = 399, Visible = true},
                    new LayerDescription { LayerID = 400, Visible = true},
                    new LayerDescription { LayerID = 401, Visible = true},
                    new LayerDescription { LayerID = 402, Visible = true},
                    new LayerDescription { LayerID = 403, Visible = true},
                };

                mapDescription.LayerDescriptions = layerDescriptions;
                ImageDescription imageDescription = new ImageDescription();
                ImageDisplay imageDisplay = new ImageDisplay { ImageDPI = 96, ImageHeight = (int)boundingBox.MapImageSize.Height, ImageWidth = (int)boundingBox.MapImageSize.Width };
                imageDescription.ImageDisplay = imageDisplay;
                imageDescription.ImageType = new ImageType { ImageFormat = esriImageFormat.esriImageJPG, ImageReturnType = esriImageReturnType.esriImageReturnMimeData };

                var mapImage = Server.ExportMapImage(mapDescription, imageDescription);
                resultingBox.Left = ((EnvelopeN)mapImage.Extent).XMin;
                resultingBox.Right = ((EnvelopeN)mapImage.Extent).XMax;
                resultingBox.Top = ((EnvelopeN)mapImage.Extent).YMax;
                resultingBox.Bottom = ((EnvelopeN)mapImage.Extent).YMin;
                var stream = new MemoryStream(mapImage.ImageData);
                return new Bitmap(stream);

            }
            catch (Exception e)
            {

            }
            return null;
        }
        #endregion
    }
}