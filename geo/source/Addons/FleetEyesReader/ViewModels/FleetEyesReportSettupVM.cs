using System.Windows.Input;
using Depiction.API.MVVM;
using FleetEyesReader.EmailSettings;

namespace FleetEyesReader.ViewModels
{
    public class FleetEyesReportSettupVM : ViewModelBase
    {
        #region Variables

        IncomingFleetEyesEmailAccounts records = new IncomingFleetEyesEmailAccounts();
        private FleetEyesEmailSettings currentEmailAccount;

        #endregion
        #region commands

        public ICommand ReadEmailsCommand { get; set; }
        #endregion
        #region Properties

        public string EmailPassword { get; set; }
        public string RefreshInterval { get; set; }
        public bool CropToRegion { get; set; }

        public FleetEyesEmailSettings SelectedEmailAccount
        {
            get { return currentEmailAccount; }
            set
            {
                currentEmailAccount = value; NotifyPropertyChanged("SelectedEmailAccount");
            }
        }

        public IncomingFleetEyesEmailAccounts IncomingFleetEyesEmailAccountList
        {
            get
            {
                return records;
            }
            private set
            {
                records = value; NotifyPropertyChanged("IncomingFleetEyesEmailAccountList");
            }
        }

        #endregion

        #region Constructor

        public FleetEyesReportSettupVM()
        {
            UpdateIncomingAccounts();
            RefreshInterval = "2";

        }
        #endregion

        #region Helpers

        public void UpdateIncomingAccounts()
        {
            var incomingFleetEyesEmailAccounts = new IncomingFleetEyesEmailAccounts();////No clue how this is supposed to work
            var currentAccounts = FleetEyesEmailInformationSettings.GetIncomingFleetEyesAccounts() ?? new IncomingFleetEyesEmailAccounts();
            // Add an email account with empty user name and server; this will create a
            // binding with "New account..." on the dialog ...
            incomingFleetEyesEmailAccounts.Insert(new FleetEyesEmailSettings { ShowAdvancedFields = true, IsNewRecord = true, NewRecordName = "Other Pop3 account...", ExplanatoryText = "Live reports requires your email service to use the POP3 protocol. For more information, see the section \"Live Reports\" in Depiction help." }, 0);
            incomingFleetEyesEmailAccounts.Insert(new FleetEyesEmailSettings { IsNewRecord = true, NewRecordName = "Yahoo! Mail Plus account", Pop3Server = "plus.pop.mail.yahoo.com", SuggestedDomain = "@yahoo.com", UseSSL = false, ExplanatoryText = "Note: Live reports requires the paid Yahoo! Mail Plus account, since the free Yahoo! Mail accounts do not support the POP3 access that Live reports uses." }, 0);
            incomingFleetEyesEmailAccounts.Insert(new FleetEyesEmailSettings { IsNewRecord = true, NewRecordName = "Microsoft Hotmail account", Pop3Server = "pop3.live.com", SuggestedDomain = "@hotmail.com", UseSSL = true, ExplanatoryText = "Note: Free Microsoft Hotmail accounts can only be logged in to every 15 minutes." }, 0);
            incomingFleetEyesEmailAccounts.Insert(new FleetEyesEmailSettings { IsNewRecord = true, NewRecordName = "Google Gmail account", Pop3Server = "pop.gmail.com", SuggestedDomain = "@gmail.com", ExplanatoryText ="", UseSSL = true }, 0);

            foreach (var account in currentAccounts)
            {
                incomingFleetEyesEmailAccounts.AddOrReplaceExisting((FleetEyesEmailSettings)account);
            }

            IncomingFleetEyesEmailAccountList = incomingFleetEyesEmailAccounts;
        }
        #endregion

    }
}