using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Depiction.API;
using Depiction.API.DepictionConfiguration;
using Depiction.API.ExceptionHandling;

namespace FleetEyesReader.EmailSettings
{
    static public class FleetEyesEmailInformationSettings
    {
        public const string FleetEyesSectionName = "FleetEyesSettings";

        private static bool _assemblyResolver;
        #region incoming email account stuff
//
        public static IncomingFleetEyesEmailAccounts GetIncomingFleetEyesAccounts()
        {//grr this is causing problems
            FleetEyesSettingsConfigurationSection emailSection;
            var config = GetUserSettingsLostConfig(out emailSection);

            if (config == null) return null;
            return config.Sections[FleetEyesSectionName] != null ? ((FleetEyesSettingsConfigurationSection)config.GetSection(FleetEyesSectionName)).IncomingFleetEyesEmailAccounts : null;
        }
        public static void AddIncomingEmailAccount(FleetEyesEmailSettings emailAddressToAdd)
        {
            FleetEyesSettingsConfigurationSection emailSection;
            var config = GetUserSettingsLostConfig(out emailSection);

            emailSection.IncomingFleetEyesEmailAccounts.AddOrReplaceExisting(emailAddressToAdd);
            emailSection.SectionInformation.ForceSave = true;
            try
            {
                config.Save(ConfigurationSaveMode.Full);
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString("Could not update incoming email list.");
            }
        }
        #endregion
        
        private static Configuration GetUserSettingsLostConfig(out FleetEyesSettingsConfigurationSection emailSection)
        {
            if (!_assemblyResolver)
            {
                AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
                _assemblyResolver = true;
            }
            Configuration config = DepictionUserConfigurationManager.DepictionUserConfiguration(); 
            if (config == null)
            {
                emailSection = null;
                return null;
            }
            try
            {
                var sections = config.Sections;
                if (config.GetSection(FleetEyesSectionName) as FleetEyesSettingsConfigurationSection == null) //Sections.Get(FleetEyesSectionName)==null )//config.Sections[FleetEyesSectionName] == null)
                {
                    emailSection = new FleetEyesSettingsConfigurationSection();
                    emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                    config.Sections.Add(FleetEyesSectionName, emailSection);
//                    config.Save();//??
                }
                else
                {
                    var section = config.GetSection(FleetEyesSectionName);
                    if ((section is FleetEyesSettingsConfigurationSection))
                    {
                        emailSection = (FleetEyesSettingsConfigurationSection)section;
                    }else
                    {
                        config.Sections.Remove(FleetEyesSectionName);
                        config.Save();
                        emailSection = new FleetEyesSettingsConfigurationSection();
                        emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                        config.Sections.Add(FleetEyesSectionName, emailSection);
                    }
                }
            }
            catch(ConfigurationErrorsException configError )
            {
                DepictionExceptionHandler.HandleException(configError, false, true);
                var sections = config.SectionGroups;
                config.Sections.Remove(FleetEyesSectionName);
                config.Save();
                emailSection = new FleetEyesSettingsConfigurationSection();
                emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                config.Sections.Add(FleetEyesSectionName, emailSection);
                return config;
            }
            catch(Exception ex){
                DepictionExceptionHandler.HandleException(ex,false,true);
                //This isn't actually fixing the problem, but not the right time to dig into this.
                config.Sections.Remove(FleetEyesSectionName);
                config.Save();
                emailSection = new FleetEyesSettingsConfigurationSection();
                emailSection.SectionInformation.AllowExeDefinition = ConfigurationAllowExeDefinition.MachineToLocalUser;
                config.Sections.Add(FleetEyesSectionName, emailSection);
            }
          
            return config;
        }
        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            return loadedAssemblies.FirstOrDefault(asm => asm.FullName == args.Name);
        }
    }

    #region outgoing email info region

    //        public static OutgoingEmailAccountRecords GetOutgoingEmailAccounts()
    //        {
    //            Configuration config = FleetEyesConfigurationManager.FleetEyesEmailConfiguration();//.LiveReportUserConfiguration();
    //            return config.Sections[fleetEyesSectionName] != null ? ((EmailSettingsConfigurationSection)config.GetSection(fleetEyesSectionName)).OutgoingEmailAccountRecords : null;
    //        }
    //        public static void RemoveOutgoingEmailAccount(OutgoingEmailSettings emailAddressToRemove)
    //        {
    //            EmailSettingsConfigurationSection emailSection;
    //            Configuration config = GetUserSettingsLostConfig(out emailSection);
    //
    //            emailSection.OutgoingEmailAccountRecords.Remove(emailAddressToRemove);
    //            emailSection.SectionInformation.ForceSave = true;
    //            try
    //            {
    //                config.Save(ConfigurationSaveMode.Full);
    //            }
    //            catch (Exception ex)
    //            {
    //                DepictionAccess.NotificationService.DisplayMessageString("Could not update outgoing email list.");
    //            }
    //        }
    //        public static void AddOutgoingEmailAccount(OutgoingEmailSettings emailAddressToAdd)
    //        {
    //            EmailSettingsConfigurationSection emailSection;
    //            Configuration config = GetUserSettingsLostConfig(out emailSection);
    //
    //            emailSection.OutgoingEmailAccountRecords.AddOrReplaceExisting(emailAddressToAdd);
    //            emailSection.SectionInformation.ForceSave = true;
    //            try
    //            {
    //                config.Save(ConfigurationSaveMode.Full);
    //            }catch(Exception ex)
    //            {
    //                DepictionAccess.NotificationService.DisplayMessageString("Could not update outgoing email list.");
    //            }
    //        }

    #endregion
    public class FleetEyesSettingsConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("IncomingFleetEyesEmailAccounts")]
        public IncomingFleetEyesEmailAccounts IncomingFleetEyesEmailAccounts
        {
            get { return (IncomingFleetEyesEmailAccounts)this["IncomingFleetEyesEmailAccounts"] ?? new IncomingFleetEyesEmailAccounts(); }
        }
    }
}