﻿using System.Configuration;

namespace FleetEyesReader.EmailSettings
{
    public class IncomingFleetEyesEmailAccounts : ConfigurationElementCollection
    {
        public FleetEyesEmailSettings this[int index]
        {
            get { return BaseGet(index) as FleetEyesEmailSettings; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new FleetEyesEmailSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((FleetEyesEmailSettings)element).UserName + ((FleetEyesEmailSettings)element).Pop3Server;
        }
        public void AddOrReplaceExisting(FleetEyesEmailSettings emailAccount)
        {
            try
            {
                var index = BaseIndexOf(emailAccount);
                if (index != -1)
                {
                    this[index] = emailAccount;
                }
                else
                {
                    BaseAdd(emailAccount, true);
                }
            }
            catch
            {//Hack for sure
                var index = BaseIndexOf(emailAccount);
                this[index] = emailAccount;
            }
        }
        public void Remove(FleetEyesEmailSettings emailAccount)
        {
            var index = BaseIndexOf(emailAccount);
            if (index >= 0)
            {
                BaseRemove(emailAccount);
            }
        }
//        public void Add(FleetEyesEmailSettings EmailAccount)
//        {
//            BaseAdd(EmailAccount);
//        }

        public void Insert(FleetEyesEmailSettings emailAccount, int index)
        {
            BaseAdd(index, emailAccount);
        }
    }
}