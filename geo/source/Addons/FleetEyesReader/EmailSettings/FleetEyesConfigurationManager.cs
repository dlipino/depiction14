using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Depiction.API;
using Depiction.API.DepictionConfiguration;

namespace FleetEyesReader.EmailSettings
{
    public class FleetEyesConfigurationManager
    {
//        private const string DefaultSettings = "FleetEyesSettings";
        private static Configuration depictionConfiguration;
        private static string configFile = "";
        private static string configFileName = "";
        private static bool assemblyResolver;

        protected static bool SetupConfigPath()
        {
            configFile = string.Concat("FleetEyesEmail", ".config");
//            var dir = AppDomain.CurrentDomain.ExecuteAssembly();
            string baseDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (string.IsNullOrEmpty(baseDir)) return false;
            configFileName = Path.Combine(baseDir, configFile);
            return true;
        }
        public static void CreateFleetEyesEmailConfigFile()
        {
            SetupConfigPath();

            if (!File.Exists(configFileName))
            {
                var configExe = new ExeConfigurationFileMap();// ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
                configExe.ExeConfigFilename = configFile;
                configExe.LocalUserConfigFilename = configFile;
                configExe.RoamingUserConfigFilename = configFile;

                var config = ConfigurationManager.OpenMappedExeConfiguration(configExe, ConfigurationUserLevel.None);

                config.SectionGroups.Clear();
                config.Sections.Clear();
                config.SaveAs(configFileName, ConfigurationSaveMode.Minimal, true);
                config.Sections.Add(FleetEyesEmailInformationSettings.FleetEyesSectionName, new FleetEyesSettingsConfigurationSection());
                config.Save(ConfigurationSaveMode.Modified);
            }
//            try
//            {
//                AppDomain.CurrentDomain.AssemblyResolve += (o, args) =>
//                {
//                    var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
//                    return loadedAssemblies.FirstOrDefault(asm => asm.FullName == args.Name);
//                };
//                var config = FleetEyesEmailConfiguration();
//                var section = config.GetSection(FleetEyesEmailInformationSettings.FleetEyesSectionName);
//            }catch(Exception ex)
//            {
//                
//            }
        }

        public static Configuration FleetEyesEmailConfiguration()
        {
            SetupConfigPath();
            //fixes the cant find typename error when for the extension
            if(!assemblyResolver)
            {
                AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
                assemblyResolver = true;
            }
            //Not really sure what mapped does yet, but im sure it is better than what im current doing
            if (!File.Exists(configFileName)) return null;
            //Always get a new one because sometime people use multiple depictions.
            //if (depictionConfiguration == null)
            //{
            var configExe = new ExeConfigurationFileMap();
            // ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            configExe.ExeConfigFilename = configFileName;


            depictionConfiguration = ConfigurationManager.OpenMappedExeConfiguration(configExe, ConfigurationUserLevel.None);

            //}
            return depictionConfiguration;
        }

        static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
            return loadedAssemblies.FirstOrDefault(asm => asm.FullName == args.Name);
        }
     

        public static void MapExeConfiguration()
        {
            if (DepictionAccess.PathService == null) return;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);

            var folder = DepictionAccess.PathService.AppDataDirectoryPath;
            var fullFileName = Path.Combine(folder, configFile);
            config.SaveAs(fullFileName, ConfigurationSaveMode.Full);
        }
    }
//    public class FleetEyesSettingsConfigurationSection : ConfigurationSection
//    {
//        [ConfigurationProperty("IncomingFleetEyesEmailAccounts")]
//        public IncomingFleetEyesEmailAccounts IncomingFleetEyesEmailAccounts
//        {
//            get { return (IncomingFleetEyesEmailAccounts)this["IncomingFleetEyesEmailAccounts"] ?? new IncomingFleetEyesEmailAccounts(); }
//        }
//    }
}