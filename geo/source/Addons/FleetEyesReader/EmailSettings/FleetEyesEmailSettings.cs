using System.Configuration;

namespace FleetEyesReader.EmailSettings
{
    public class FleetEyesEmailSettings : ConfigurationElement
    {
        [ConfigurationProperty("UserName", DefaultValue = "", IsRequired = true)]
        public string UserName
        {
            get { return (string)this["UserName"]; }
            set { this["UserName"] = value; }
        }

        [ConfigurationProperty("UseSSL", DefaultValue = false, IsRequired = true)]
        public bool UseSSL
        {
            get { return (bool)this["UseSSL"]; }
            set { this["UseSSL"] = value; }
        }

        [ConfigurationProperty("Pop3Server", DefaultValue = "", IsRequired = true)]
        public string Pop3Server
        {
            get { return (string)this["Pop3Server"]; }
            set { this["Pop3Server"] = value; }
        }
        [ConfigurationProperty("PortNumber", DefaultValue = "Default", IsRequired = true)]
        public string PortNumber
        {
            get { return (string)this["PortNumber"]; }
            set { this["PortNumber"] = value; }
        }

        public bool IsNewRecord { get; set; }

        public string NewRecordName { get; set; }

        public bool ShowAdvancedFields { get; set; }

        public string SuggestedDomain { get; set; }

        public string ExplanatoryText { get; set; }

        public FleetEyesEmailSettings()
        {
            IsNewRecord = false;
            ShowAdvancedFields = false;
        }

        public FleetEyesEmailSettings(string userName, string pop3Server, bool useSSL)
            : this()
        {
            UserName = userName;
            Pop3Server = pop3Server;
            UseSSL = useSSL;
        }

    }
}