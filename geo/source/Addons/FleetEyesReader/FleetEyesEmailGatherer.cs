using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.DepictionConfiguration;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using FleetEyesReader.EmailSettings;
using FleetEyesReader.Models;
using FleetEyesReader.ViewModels;
using FleetEyesReader.Views;

//The general principle behind this is having one config file that can be modified on the fly.
//that means having 2 aprslive services running does nothing
//The config is attached to the singleton gatherer
//Announcements, Bulletins, Messages, Alerts, Weather, and of course a map of all this activity including objects, satellites, nets, meetings, Hamfests, etc.
//types of aprs stations
namespace FleetEyesReader
{
    [DepictionNonDefaultImporterMetadata("FleetEyesEmailReader", DisplayName = "Fleet Eyes gather",
      ImporterSources = new[] { InformationSource.Web },
      Description = "Simple gmail Fleet Eyes reader.",
      Author = "Depictin Inc")]
    public class FleetEyesEmailGatherer : NondefaultDepictionImporterBase,IDisposable
    {
        private const string name = "APRS Live Stations";

        #region Variables

        private FleetEyesGmailReaderService fleetEyesService;

        private Window configWin;
        private FleetEyesReportSettupVM configViewModel;

        #endregion

        #region Propeties

        #region Properties for IDepictionAddonBase
        override public string AddonConfigViewText
        {
            get { return "Configure Fleet Eyes Reader"; }
        }

        #endregion
        
        public Window ConfigWin
        {
            get
            {
                if(configWin == null)
                {
                    configWin = GetNewConfigWin();
                }
                return configWin;
            }
        }

        public string DisplayName
        {
            get { return name; }
        }

        public string HoverText
        {
            get
            {
                return "Simple Fleet Eyes gmail reader";
            }
        }
        public string ToolTipText
        {
            get { return "Fleet Eyes Email Reader"; }
        }

        override public bool Activated { get { return true; } }

        override public object AddonConfigView { get { return ConfigWin; } }


        #endregion

        #region Constructor

        public FleetEyesEmailGatherer(){}

       
        #endregion

        #region Public methods
       
        //** This is the part that is turned on from the quickstart service
         
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            StartReadingEmails(depictionRegion);
           
//            if (fleetEyesService != null)
//            {
//                DepictionAccess.NotificationService.DisplayMessageString(string.Format("APRS Live is already running,please cancel APRS Live before continuing"));
//                return;
//            }
//
//            try
//            {
//                if (fleetEyesService == null) fleetEyesService = new FleetEyesGmailReaderService(Config, depictionRegion);//packetIO);
//                DepictionAccess.BackgroundServiceManager.AddBackgroundService(fleetEyesService);
//                fleetEyesService.UpdateStatusReport(name);
//                fleetEyesService.StartBackgroundService(null);
//            }
//            catch (Exception e)
//            {
//                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Could not start email reader: {0}", e.Message));
//                throw;
//            }
        }
        protected void StartReadingEmailsForStoryRegion()
        {
            IMapCoordinateBounds region = null;
            if(DepictionAccess.CurrentDepiction != null)
            {
                region = DepictionAccess.CurrentDepiction.RegionBounds;
            }
            StartReadingEmails(region);
        }
        protected void StartReadingEmails(IMapCoordinateBounds region)
        {
            var sourceInfo = new EmailSourceInfo();

            sourceInfo.Pop3Server = configViewModel.SelectedEmailAccount.Pop3Server;
            var rawName = configViewModel.SelectedEmailAccount.UserName;
            sourceInfo.SimpleUserName = rawName;
            sourceInfo.UseSSL = configViewModel.SelectedEmailAccount.UseSSL;
            sourceInfo.Password = configViewModel.EmailPassword;
            int port = -1;
            if (!int.TryParse(configViewModel.SelectedEmailAccount.PortNumber, out port))
            {
                port = -1;
            }
            sourceInfo.PortNumber = port;
            var regionbounds = region;
            if (!configViewModel.CropToRegion) regionbounds = null;
            var readerModel = new FleetEyesGmailReaderService(sourceInfo, regionbounds);
            double minuteRefresh;
            if (double.TryParse(configViewModel.RefreshInterval, out minuteRefresh))
            {
                readerModel.RefreshRateMinutes = minuteRefresh;
            }

            if (!readerModel.IsValidEmailAddressPassword()) return;
            configViewModel.UpdateIncomingAccounts();
            var name = string.Format("Email: {0}", sourceInfo.CompleteUserName);

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(readerModel);
            readerModel.UpdateStatusReport(name);
            readerModel.StartBackgroundService(readerModel);
        }
        #endregion
        #region Config stuff

        private Window GetNewConfigWin()
        {
            if (configViewModel == null)//this should never happen
            {
                try
                {
                    configViewModel = new FleetEyesReportSettupVM()
                    {
                        ReadEmailsCommand = new DelegateCommand(StartReadingEmailsForStoryRegion)
                        //                OpenAboutBoxCommand = new DelegateCommand(OpenAboutBox),
                        //                OpenOptionsDialogCommand = new DelegateCommand(OpenOptionsDialog),
                        //                StopALCommand = new DelegateCommand(StopAPRSLive, IsAPRSLiveRunning)
                    };
                }
                catch
                {
                    return null;
                }
            }

            

            var view = new FleetEyesEmailConfigDialog
            {
                DataContext = configViewModel,
//                RunPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            return view;
        }

        #endregion

        public void Dispose()
        {
            configWin = null;
          
        }
    }
}