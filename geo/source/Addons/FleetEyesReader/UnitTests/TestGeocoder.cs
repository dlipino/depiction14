﻿using System;
using System.Collections.Generic;
using Depiction.API.Interfaces;
using Depiction.API.ValueTypes;

namespace FleetEyesReader.UnitTests
{
    public class TestGeocoder : IDepictionGeocoder
    {
        //Designed for a test addresses.
        public string testAddress1 = "89 Avenue I, Newark, NJ 07105, USA";
        public string testAddress2 = "435 Elizabeth Avenue, Elizabeth, NJ 07206, USA";
        static public GeocodeResults testResult1 = new GeocodeResults(new LatitudeLongitude(2, 3), true);
        static public GeocodeResults testResult2 = new GeocodeResults(new LatitudeLongitude(10,11), true);
        public void ReceiveParameters(Dictionary<string, string> parameters)
        {

        }

        public GeocodeResults GeocodeAddress(string addressString)
        {
            if (addressString.Equals(testAddress1, StringComparison.InvariantCultureIgnoreCase))
            {
                return testResult1;
            } 
            if (addressString.Equals(testAddress2, StringComparison.InvariantCultureIgnoreCase))
            {
                return testResult2;
            }
            return null;
        }

        public GeocodeResults GeocodeAddress(string addressString, bool isLatLong)
        {
            return null;
        }

        public GeocodeResults GeocodeAddress(string street, string city, string state, string zipcode, string country)
        {
            return null;
        }

        public bool StoreResultsInDepiction { get; private set; }
        public string GeocoderName { get; private set; }
        public bool IsAddressGeocoder { get; private set; }
        public bool IsLatLongGeocoder { get; private set; }
    }
}