﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service;
using Depiction.Serialization;
using FleetEyesReader.Service;
using NUnit.Framework;

namespace FleetEyesReader.UnitTests
{
    //From: <noreply@fleeteyes.com>
    //Date: Sat, Dec 14, 2013 at 9:08 PM
    //Subject: [FLEETEYES] Alert Notification
    //To: hb1@njlincs.net, cortachp@uhnj.org, NJ-FLEETEYES-ALERT@googlegroups.com, njemsmacc@gmail.com
    //
    //
    //
    //Fleeteyes CAD - INCIDENT ASSIGNMENT - Local Assignment
    //
    //Incident BUS ACCIDENT/BUS STRUCK OVERPASS
    //Reservation #: 2013-12-14 000903
    //Reason: BUS ACCIDENT/BUS STRUCK OVERPASS
    //Priority: EMERGENCY
    //Pickup Address: 89 Avenue I, Newark, NJ 07105, USA
    //Pickup Time: 2013-12-15 01:59
    //Dropoff Address: 89 Avenue I, Newark, NJ 07105, USA
    //Comments: BUS ACCIDENT/BUS STRUCK OVERPASS

    [TestFixture]
    public class FleetEyesEmailReaderTest
    {
        private string validSubject = "[FLEETEYES] Alert Notification";
        private string randomSubject = "Hello sir";
        private string validBody = "Fleeteyes CAD - INCIDENT ASSIGNMENT - Local Assignment" + Environment.NewLine + Environment.NewLine +
            "Incident BUS ACCIDENT/BUS STRUCK OVERPASS" + Environment.NewLine +
            "Reservation #: 2013-12-14 000903" + Environment.NewLine +
           "Reason: BUS ACCIDENT/BUS STRUCK OVERPASS" + Environment.NewLine +
            "Priority: EMERGENCY" + Environment.NewLine +
            "Pickup Address: 89 Avenue I, Newark, NJ 07105, USA" + Environment.NewLine +
            "Pickup Time: 2013-12-15 01:59" + Environment.NewLine +
            string.Format("Dropoff Address: {0}" + Environment.NewLine, dropOffAddress) +
            //           "Dropoff Address: 89 Avenue I, Newark, NJ 07105, USA" + Environment.NewLine +
            "Comments: BUS ACCIDENT/BUS STRUCK OVERPASS" + Environment.NewLine;



        private static string validEid = "2013-12-14 000903";
        private static string dropOffAddress = "89 Avenue I, Newark, NJ 07105, USA";

        private TempFolderService tempFolder;
        [SetUp]
        public void TestSetup()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder = new TempFolderService(true);
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory("FleetEyesReader", "dml", true, tempFolder.FolderName);
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            DepictionAccess.ElementLibrary.SetDefaultPrototypesFromPath(tempFolder.FolderName, false);
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            //            DepictionAccess.ElementLibrary.UpdateElementPrototypeLibraryFromTempLoadedDpnLocation(tempFolder.FolderName);
        }
        [TearDown]
        public void TestCleanUp()
        {
            DepictionAccess.ElementLibrary = null;
            DepictionAccess.GeoCodingService = null;
            tempFolder.Close();
        }
        [Test]
        public void RawBodyReadTest()
        {
            var parseResults = FleetEyesEmailParser.ParseEmailBodyStringIntoProperties(validBody);
            Assert.AreEqual(7, parseResults.Count);

        }
        [Test]
        public void GeolocationtestFromBody()
        {
            var parseResults = FleetEyesEmailParser.GetRawPrototypeFromEmailSubjectAndBody(validSubject, validBody,
                                                                                           DateTime.Now,
                                                                                           new[] { new TestGeocoder() });


            Assert.IsNotNull(parseResults);
            var hasPosProp = parseResults.HasPropertyByInternalName("Position");
            Assert.IsTrue(hasPosProp);
            ILatitudeLongitude location = null;
            parseResults.GetPropertyValue("Position", out location);
            Assert.AreEqual(TestGeocoder.testResult1.Position, location);

        }

        private string validBodyBadNewLines = "Incident FIRE STAND BY" + Environment.NewLine +
                            "Reservation #: 2013-12-27 000917" + Environment.NewLine +
                            "Reason: FIRE STAND BY" + Environment.NewLine +
                            "Priority: EMERGENCY" + Environment.NewLine +
                            "Pickup Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA Pickup Time:" +Environment.NewLine +
                            "2013-12-28 05:06 Dropoff Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA" +Environment.NewLine;
   

        [Test]
        public void IncorrectNewLineRawBodyReadTest()
        {
            var parseResults = FleetEyesEmailParser.ParseEmailBodyStringIntoProperties(validBodyBadNewLines);
            Assert.AreEqual(6, parseResults.Count);

        }
        [Test]
        public void IncorrectNewLineBodyParseTest()
        {
            var parseResults = FleetEyesEmailParser.GetRawPrototypeFromEmailSubjectAndBody(validSubject, validBodyBadNewLines,
                                                                                        DateTime.Now,
                                                                                        new[] { new TestGeocoder() });

            Assert.IsNotNull(parseResults);
            var hasPosProp = parseResults.HasPropertyByInternalName("Position");
            Assert.IsTrue(hasPosProp);
            ILatitudeLongitude location = null;
            parseResults.GetPropertyValue("Position", out location);
            Assert.AreEqual(TestGeocoder.testResult2.Position, location);
        }

        private string validBodyBadNewLinesNoDropOff = "Incident FIRE STAND BY" + Environment.NewLine +
                    "Reservation #: 2013-12-27 000917" + Environment.NewLine +
                    "Reason: FIRE STAND BY" + Environment.NewLine +
                    "Priority: EMERGENCY" + Environment.NewLine +
                    "Pickup Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA Pickup Time:" + Environment.NewLine +
                    "2013-12-28 05:06 Comments: Please work" + Environment.NewLine;
       [Test]
        public void IncorrectNewLineNoDropOffRawBodyReadTest()
        {
            var parseResults = FleetEyesEmailParser.ParseEmailBodyStringIntoProperties(validBodyBadNewLinesNoDropOff);
            Assert.AreEqual(6, parseResults.Count);

        }
       [Test]
       public void IncorrectNewLineNoDropOffBodyParseTest()
       {
           var parseResults = FleetEyesEmailParser.GetRawPrototypeFromEmailSubjectAndBody(validSubject, validBodyBadNewLinesNoDropOff,
                                                                                       DateTime.Now,
                                                                                       new[] { new TestGeocoder() });

           Assert.IsNotNull(parseResults);
           var hasPosProp = parseResults.HasPropertyByInternalName("Position");
           Assert.IsFalse(hasPosProp);
//           ILatitudeLongitude location = null;
//           parseResults.GetPropertyValue("Position", out location);
//           Assert.AreEqual(TestGeocoder.testResult2.Position, location);
       }

       #region useful quick methods
       private string validBodyBadNewLinesClean =
                      "Reservation #: 2013-12-27 000917" + Environment.NewLine +

                      "Reason: FIRE STAND BY" + Environment.NewLine +

                      "Priority: EMERGENCY" + Environment.NewLine +

                      "Pickup Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA Pickup Time:" +
                      Environment.NewLine +

                      "2013-12-28 05:06 Dropoff Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA" +
                      Environment.NewLine;
        [Test]
        [Ignore("Simple proof of concept test method")]
        public void FunctySplit()
        {
            var data = "THExxQUICKxxBROWNxxFOX";

            var res = data.Split(new string[] { "xx" }, StringSplitOptions.None);
            var testSplit = new[] { "Reservation #:", "yy" };
            testSplit = FleetEyesEmailParser.FleetEyesTags.ToArray();
            var prevTag = string.Empty;
            var endResult = new List<string>();
            var toSplit = validBodyBadNewLinesClean;
            foreach (var tag in testSplit)
            {
                var spRes = toSplit.Split(new[] { tag }, StringSplitOptions.None);
                if (spRes.Length == 2)
                {
                    if (!string.IsNullOrEmpty(prevTag))
                    {
                        endResult.Add(prevTag + spRes[0].Trim());
                    }
                    prevTag = tag + " ";
                    toSplit = spRes[1];
                }
            }
            foreach (var l in endResult)
            {
                Debug.WriteLine(l);
            }
            //           var normal = FleetEyesEmailParser.FleetEyesTags.ToArray();
            //           res = validBodyBadNewLines.Split(normal, StringSplitOptions.None);
            //           var outString = new List<string>();
            //           foreach(var tag in normal)
            //           {
            //              var spRes= validBodyBadNewLines.Split(new[] {tag}, StringSplitOptions.None);
            //              
            //           }
            //          var rev= normal.Reverse().ToArray();
            //          res = validBodyBadNewLines.Split(rev, StringSplitOptions.None);
        }
       #endregion
    }
}