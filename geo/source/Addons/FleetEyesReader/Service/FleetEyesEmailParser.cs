using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DpnPorting;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.TypeConverter;

#region the email
//From: <noreply@fleeteyes.com>
//Date: Sat, Dec 14, 2013 at 9:08 PM
//Subject: [FLEETEYES] Alert Notification
//To: hb1@njlincs.net, cortachp@uhnj.org, NJ-FLEETEYES-ALERT@googlegroups.com, njemsmacc@gmail.com
//
//
//
//Fleeteyes CAD - INCIDENT ASSIGNMENT - Local Assignment
//
//Incident BUS ACCIDENT/BUS STRUCK OVERPASS
//Reservation #: 2013-12-14 000903
//Reason: BUS ACCIDENT/BUS STRUCK OVERPASS
//Priority: EMERGENCY
//Pickup Address: 89 Avenue I, Newark, NJ 07105, USA
//Pickup Time: 2013-12-15 01:59
//Dropoff Address: 89 Avenue I, Newark, NJ 07105, USA
//Comments: BUS ACCIDENT/BUS STRUCK OVERPASS


//Incident WORKING STRUCTURE FIRE
//
//Reservation #: 2014-01-09 000919
//
//Reason: WORKING STRUCTURE FIRE
//
//Priority: EMERGENCY
//
//Pickup Address: 35 3rd Avenue, Long Branch, NJ 07740, USA
//
//Pickup Time: 2014-01-09 09:30 Dropoff Address: 35 3rd Avenue, Long Branch, NJ 07740, USA
//
//Comments: WORKING STRUCTURE FIRE/APARTMENT COMPLEX/BUILDING 20

//Incident FIRE STAND BY
//
//Reservation #: 2013-12-27 000917
//
//Reason: FIRE STAND BY
//
//Priority: EMERGENCY
//
//Pickup Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA Pickup Time:
//
//2013-12-28 05:06 Dropoff Address: 435 Elizabeth Avenue, Elizabeth, NJ 07206, USA
#endregion

//[assembly:InternalsVisibleTo("Friend1a")]
namespace FleetEyesReader.Service
{

    public static class FleetEyesEmailParser
    {
        private static string ValidFleetEyesEmail = "[FLEETEYES]";
        static private string positionKey = "Dropoff Address";
        static private string displayNameKey = "displayname";
        internal static List<string> FleetEyesTags = new List<string>
                                                   {
                                                       "Reservation #:",
                                                       "Reason:",
                                                       "Priority:",
                                                       "Pickup Address:",
                                                       "Pickup Time:",
                                                       "Dropoff Address:",
                                                       "Comments:"
                                                   };

        internal static string FleetEyesFirstSplitTag = "Reservation #:";
        #region Main Parsers
        public static IElementPrototype GetRawPrototypeFromEmailSubjectAndBody(string subject, string body)
        {
            return GetRawPrototypeFromEmailSubjectAndBody(subject, body, DateTime.MinValue, null);
        }
        public static IElementPrototype GetRawPrototypeFromEmailSubjectAndBody(string subject, string body, DateTime emailTime)
        {
            return GetRawPrototypeFromEmailSubjectAndBody(subject, body, emailTime, null);
        }
        public static bool IsSubjectUsable(string subject)
        {
            var subjectProperties = ParseEmailSubjectIntoProperties(subject);
            if (subjectProperties == null) return false;
            return true;
        }
        public static IElementPrototype GetRawPrototypeFromEmailSubjectAndBody(string baseSubject, string body, DateTime emailTime, IDepictionGeocoder[] nonStandardGeocoders)
        {
//            var propertiesToIgnore = new List<string> { "email subject", "email body", "email from", "email to", "email error", "email sent" };
            //            string cleanedSubject;
            //            var rawTags = GetDepictionEmailTagsFromSubjectAndCleanSubject(baseSubject, out cleanedSubject);

            var subjectProperties = ParseEmailSubjectIntoProperties(baseSubject);
            if (subjectProperties == null) return null;
            var mainBodyProperties = ParseEmailBodyStringIntoProperties(body);

            var combinedProperties = new Dictionary<string, object>(StringComparer.CurrentCultureIgnoreCase);
            var zoiString = string.Empty;
            var elementType = "PointOfInterest";

            foreach (var key in mainBodyProperties.Keys)
            {
                var lowerKey = key.ToLowerInvariant();
                if (combinedProperties.ContainsKey(lowerKey))
                {
                    combinedProperties[lowerKey] = mainBodyProperties[key];
                }
                else
                {
                    combinedProperties.Add(key.ToLowerInvariant(), mainBodyProperties[key]);
                }
            }
            
            IElementPrototype fakePrototype = null;
            if (DepictionAccess.ElementLibrary != null)
            {
                fakePrototype = DepictionAccess.ElementLibrary.GetPrototypeFromAutoDetect(elementType, DepictionGeometryType.Point.ToString());
            }
            IElementPrototype prototype = null;

            if (fakePrototype == null)
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(elementType);
            }
            else
            {
                prototype = ElementFactory.CreateRawPrototypeOfType(fakePrototype.ElementType);
            }

            foreach (var property in combinedProperties)
            {
                var actualValue = property.Value;
                IElementProperty realProp = null;
                if (fakePrototype != null)
                {
                    var key = property.Key;
                    var propData = fakePrototype.GetPropertyByInternalName(key, true);
                    if (propData != null)
                    {
                        realProp = propData.DeepClone();
                        realProp.PropertySource = PropertySource.EMAIL;
                        if (realProp is DepictionElementProperty)
                        {
                            ((DepictionElementProperty)realProp).LastModified = emailTime;
                        }
                        try
                        {
                            object match = actualValue.ToString();
                            if (!realProp.InternalName.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                            {
                                match = DepictionTypeConverter.ChangeType(actualValue, realProp.ValueType);
                            }
                            if (match == null) match = actualValue;
                            realProp.SetPropertyValue(match);
                        }
                        catch (Exception ex)
                        {

                        }
                        prototype.AddPropertyOrReplaceValueAndAttributes(realProp, true, false);
                    }
                }
                if (realProp == null)
                {
                    object objectValue = actualValue.ToString();
                    if (!property.Key.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                    {
                        objectValue = DepictionTypeConverter.ChangeTypeByGuessing(actualValue);//This is strange 
                    }

                    if (objectValue == null) objectValue = actualValue;
                    var spaceLessKey = property.Key.Replace(" ", "");
                    var prop = new DepictionElementProperty(spaceLessKey, property.Key, objectValue);
                    //prop.Deletable = false;
                    prop.PropertySource = PropertySource.EMAIL;
                    prop.LastModified = emailTime;
                    prototype.AddPropertyOrReplaceValueAndAttributes(prop, true, false);
                }
            }
            var locationString = string.Empty;
            if (combinedProperties.ContainsKey(positionKey))
            {
                locationString = combinedProperties[positionKey].ToString();
            }
            var result = DepictionAccess.GeoCodingService.GeoCodeRawStringAddress(locationString, nonStandardGeocoders);
            if (result == null) return prototype;
            zoiString = string.Format("POINT ({0} {1})", result.Position.Longitude, result.Position.Latitude);
            if (!string.IsNullOrEmpty(zoiString))
            {
                object finalPos;
                combinedProperties.TryGetValue(positionKey, out finalPos);
                prototype.SetInitialPositionAndZOI(finalPos as ILatitudeLongitude,
                                                   new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(zoiString)));
            }

            return prototype;
        }

        #endregion

        public static Dictionary<string, object> ParseEmailSubjectIntoProperties(string subjectString)
        {
            var subjectPropertyPairs = new Dictionary<string, object>();
            string subjectRemainder = subjectString;

            subjectRemainder = StripLeadingMARS(subjectRemainder);
            subjectRemainder = StripLeadingReFw(subjectRemainder);
            subjectRemainder = subjectRemainder.Trim();
            if (subjectRemainder.StartsWith(ValidFleetEyesEmail))
            {
                return subjectPropertyPairs;
            }
            return null;

        }

        #region Subject parsing helpers


        private static string StripLeadingReFw(string subject)
        {
            return Regex.Replace(subject, "re:|fw:|fwd:", "", RegexOptions.IgnoreCase);
        }

        // to comply with trac #2157 - support for MARS from Winlink
        private static string StripLeadingMARS(string subject)
        {
            return Regex.Replace(subject, "^//MARS [MOPRZ]/", "");
        }

        #endregion
        private static bool DoesLineHaveATag(string line)
        {
            
            foreach (var tag in FleetEyesTags)
            {
                if (line.Contains(tag))
                {
                    return true;
                }
            }
            return false;
        }
        public static Dictionary<string, object> ParseEmailBodyStringIntoProperties(string bodyString)
        {
            string body = bodyString;

            var subjectPropertyPairs = new Dictionary<string, object>();
            //fleet eyes emails don't always play nice with new lines so do a bit of hacking to get the lines
            //right

            string[] lines = body.Split("\n".ToCharArray());
            var goodLines = new List<string>();
            var builder = new StringBuilder();
            foreach(var line in lines)
            {
                var hasTag = DoesLineHaveATag(line);
                if(hasTag)
                {
                    builder.Append(line);
                }else
                {
                    goodLines.Add(line);
                }
            }
            var splitter = FleetEyesTags.ToArray();
            var prevTag = string.Empty;
            var toSplit = builder.ToString();
            foreach (var tag in splitter)
            {
                var spRes = toSplit.Split(new[] { tag }, StringSplitOptions.None);
                if (spRes.Length == 2)
                {
                    if (!string.IsNullOrEmpty(prevTag))
                    {
                        goodLines.Add(prevTag + spRes[0].Trim());
                    }
                    prevTag = tag + " ";
                    toSplit = spRes[1];
                    //Reached the last one? maybe, something like
                    //that. Found no match, is it the last one
                    //or do we go on.
                    if(!DoesLineHaveATag(spRes[1]))
                    {
                        if (!string.IsNullOrEmpty(prevTag))
                        {
                            goodLines.Add(prevTag + spRes[0].Trim());
                        }
                    }
                }
                if(spRes.Length == 1)
                {
                    if(DoesLineHaveATag(spRes[0]))
                    {
//                        prevTag = string.Empty;
                    }else
                    {
                        //is the last one?? maybe
                        if (!string.IsNullOrEmpty(prevTag))
                        {
                            goodLines.Add(prevTag + spRes[0].Trim());
                        }
                    }
                }

            }
            var typeNameList = new List<string> { "Element type", "ElementType" };//"type", 


            var validLines = goodLines.ToArray();
            var lineCopies = new List<string>(validLines);
            var typeFound = false;
            foreach (string line in validLines)
            {
                KeyValuePair<string, string> property;
                var originalElementComplete = ParseEachLine( line, out property);
                if (originalElementComplete)
                {
                    break;
                }
                if (!string.IsNullOrEmpty(property.Key) && property.Value != null)
                {
                    if (!typeFound)
                    {
                        foreach (var typeName in typeNameList)
                        {
                            if (typeName.Equals(property.Key, StringComparison.InvariantCultureIgnoreCase))
                            {
                                var convertedType =
                                    DepictionStoryTranfer122To13.ConvertTypeNameFrom122To133(property.Value);
                                if (subjectPropertyPairs.ContainsKey("elementtype"))
                                {
                                    subjectPropertyPairs["elementtype"] = convertedType;
                                }
                                else
                                {
                                    subjectPropertyPairs.Add("elementtype", convertedType);
                                }
                                typeFound = true;
                                break;
                            }
                        }
                        if (typeFound) continue;
                    }
                    //Hmm i think this is done again later on.
                    if (property.Key.Equals("zoneofInfluence", StringComparison.InvariantCultureIgnoreCase) ||
                        property.Key.Equals("ZOI", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (subjectPropertyPairs.ContainsKey("zoneofinfluence"))
                        {
                            subjectPropertyPairs["zoneofinfluence"] = property.Value;
                        }
                        else
                        {
                            subjectPropertyPairs.Add("zoneofinfluence", property.Value);
                        }
                    }
                    else
                    {
                        var lowerKey = property.Key.ToLowerInvariant();
                        object propValue = "";
                        //ugh this is ugly
                        if (lowerKey.Equals("eid"))
                        {
                            propValue = property.Value;
                        }
                        else
                        {
                            propValue = PropertyValueTypeConverter.ConvertType(property.Value);
                            if (propValue.Equals(property.Value))
                            {
                                propValue = DepictionTypeConverter.ChangeTypeByGuessing(property.Value);
                            }
                        }

                        if (subjectPropertyPairs.ContainsKey(lowerKey))
                        {
                            subjectPropertyPairs[lowerKey] = propValue;
                        }
                        else
                        {
                            subjectPropertyPairs.Add(lowerKey, propValue);
                        }
                    }
                }
                lineCopies.RemoveAt(0);
            }

            return subjectPropertyPairs;
        }
  
        private static bool ParseEachLine(string line, out KeyValuePair<string, string> extractedProperty)
        {
            //If true is returned then it is a child element (this is from legacy).
            string[] parts = line.Split(":".ToCharArray());
            extractedProperty = new KeyValuePair<string, string>();
            var regex = new Regex(":");
            parts = regex.Split(line, 2);

            string valueString = "";// parts[1].Trim();
            if (parts.Length > 1)
            {
                valueString = parts[1].Trim();
            }
            else
            {
                return false;
            }
            string propertyName = parts[0].ToLowerInvariant().Trim();
//            propertyName = propertyName.Replace(" ", "");
            if (propertyName.Length == 0)//|| valueString.Length == 0
                return false;


            //            //This is not needed and can sometimes mess things up
            //            if (!Regex.Match(propertyName, "^[a-zA-Z][a-zA-Z0-9 ]*$").Success)
            //                return false;
            //
            //            if (propertyNamesToIgnore != null
            //                && propertyNamesToIgnore.Contains(propertyName))
            //                return false;
            extractedProperty = new KeyValuePair<string, string>(propertyName, valueString);
            return false;
        }

        public static void CreateEIDIfMissing(IElementPropertyHolder element, string messageID)
        {
            object elementID;

            if (!element.GetPropertyValue("EID", out elementID))
            {
                var prop = new DepictionElementProperty("EID", messageID);
                prop.Editable = false;
                prop.VisibleToUser = true;
                prop.Deletable = false;
                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                return;
            }

            //Hack to deal with eid's that end of as numbers
            if (!(elementID is string))
            {
                element.RemovePropertyWithInternalName("eid", false);
                var prop = new DepictionElementProperty("EID", elementID.ToString());
                prop.Editable = false;
                prop.VisibleToUser = true;
                prop.Deletable = false;
                element.AddPropertyOrReplaceValueAndAttributes(prop, false);
                return;
            }
        }
    }
}