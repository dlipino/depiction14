using System.Text.RegularExpressions;

namespace FleetEyesReader.Models
{
    public class EmailSourceInfo
    {
        public string CompleteUserName { get { return UpdateUserNameAccordingToServerType(SimpleUserName, Pop3Server); } }
        public string SimpleUserName { get; set; }
        public string Password { get; set; }
        public string Pop3Server { get; set; }
        public bool UseSSL { get; set; }
        public int PortNumber { get; set; }
        public double RefreshRate { get; set; }
        public string DefaultElementType { get; set; }

        private string UpdateUserNameAccordingToServerType(string name, string server)
        {
            //Gmail requires a recent: before the username to process previously read messages in the inbox
            var matches = Regex.Matches(server, @"^(.*)gmail.com");
            if (matches.Count == 1)
            {
                var usernamematches = Regex.Matches(name, @"recent:(.*)");
                //don't prepend the recent: tag if the username already has it
                if (usernamematches.Count == 0)
                {
                    var newusername = "recent:" + name;
                    return newusername;
                }
            }
            return name;
        }
    }
}