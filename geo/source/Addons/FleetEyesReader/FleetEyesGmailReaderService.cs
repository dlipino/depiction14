using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Sockets;
using System.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using FleetEyesReader.EmailSettings;
using FleetEyesReader.Models;
using FleetEyesReader.Service;
using Net.Mail;
using Net.Mime;

namespace FleetEyesReader
{
    public class FleetEyesGmailReaderService : BaseDepictionBackgroundThreadOperation
    {
        public const string FleetEyesTag = "source [Fleet Eyes]: ";
        #region Variables
        private Timer collectionStopWatch;
        private EventWaitHandle timeWaiter;
        private bool readingEmails = false;
        private readonly Hashtable emailLog = new Hashtable();
        private EmailSourceInfo sourceInfo = new EmailSourceInfo();
        private int reportsRead;
        private List<Pop3ListItem> usedPop3Items = new List<Pop3ListItem>();
        private IMapCoordinateBounds regionLimit = null;
        #endregion

        #region properties

        public EmailSourceInfo SourceInfo { get { return sourceInfo; } }
        public double RefreshRateMinutes { get; set; }

        public override bool IsServiceRefreshable
        {
            get { return true; }
        }

        public int ReportsRead
        {
            get { return reportsRead; }
            private set { reportsRead = value; }
        }

        #endregion

        #region Constructor

        public FleetEyesGmailReaderService() { }

        public FleetEyesGmailReaderService(EmailSourceInfo emailSourceInfo, IMapCoordinateBounds regionOfInterest)
        {
            sourceInfo = emailSourceInfo;
            regionLimit = regionOfInterest;
            RefreshRateMinutes = 2;
        }

        #endregion
#region Helpers
        public class Pop3ListItemComparer<T> : IEqualityComparer<T> where T : Pop3ListItem
        {
            #region Implementation of IEqualityComparer<MapElementViewModel>

            public bool Equals(T x, T y)
            {
                if(!Equals(x.MessageId, y.MessageId)) return false;
                if (!Equals(x.Octets, y.Octets)) return false;
                return true;
            }

            public int GetHashCode(T obj)
            {
                int hashCode = obj.MessageId.GetHashCode() >> 3;
                hashCode ^= obj.Octets.GetHashCode();
                return hashCode;
            }
            #endregion
        }
#endregion
        protected Dictionary<MailMessageEx, MimeEntity> GetNonreadMessagesFromEmailLocation()
        {
            var messages = new Dictionary<MailMessageEx, MimeEntity>();
            //retrieve MimeMessage/MimeEntities from the server
            int totalMessages = 0;
            int totalRead = 0;
            int readCount = 0;
            int emailErrorCount = 0;
            var port = sourceInfo.PortNumber;
            if(port <0)
            {
                port = sourceInfo.UseSSL ? 995 : 110;
            }
            using (var client = new Pop3Client(new Pop3ClientParams(sourceInfo.Pop3Server,port, sourceInfo.UseSSL, sourceInfo.CompleteUserName, sourceInfo.Password)))
            {
                try
                {
                    client.Authenticate();
                    client.Stat();

                    var itemList = client.List();//What does this get?
                    if (itemList == null) return messages;
                    //With the tolist you get a possible reenumeration issue, i cant
                    //remember what it is or how to fix at the moment.
                    var unreadItems = itemList.Except(usedPop3Items,new Pop3ListItemComparer<Pop3ListItem>()).ToList();
                    
                    totalMessages = unreadItems.Count;
                    foreach (Pop3ListItem item in unreadItems)
                    {
                        readCount++;
                        if (ServiceStopRequested) break;
                        UpdateStatusReport(string.Format("Reading {0} of {1} emails", readCount, totalMessages));
                        try
                        {
                            var key = client.RetrMailMessageEx(item);
                            if (ServiceStopRequested) break;
                            var value = client.RetrMimeEntity(item);
                            if (ServiceStopRequested) break;
                            messages.Add(key, value);
                            totalRead++;
                        }catch(NotSupportedException nsex)
                        {
                            emailErrorCount++;
//                            var errorMessage = string.Format("Could not convert email {0} of {1} into an element",
//                                                             messageCount, totalMessages);
//                            DepictionAccess.NotificationService.DisplayMessageString(errorMessage, 10);
                        }
                    }
                    usedPop3Items.AddRange(unreadItems);
                }
                catch (Pop3Exception ex)
                {
                    if (ex.Message.Contains("-ERR [IN-USE]"))
                    {
                        DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Email account \"{0}\" is in use. Will retry shortly.", SourceInfo.CompleteUserName)));
                        return new Dictionary<MailMessageEx, MimeEntity>(); ;
                    }
                }
                catch (NullReferenceException nullEx)
                {
                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Could not access all emails from {0}. Returning {1} of {2}",
                                                                                                          SourceInfo.CompleteUserName, totalRead, totalMessages), 15));
                    return new Dictionary<MailMessageEx, MimeEntity>();

                }
                catch (Exception ex)
                {
                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(string.Format("Could not access all emails from {0}. Will retry shortly.", SourceInfo.CompleteUserName), 15));
                    //                    DepictionExceptionHandler.HandleException(ex, false);
#if DEBUG
                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage(
                                                                           string.Format("DEBUG: problem reading emails from account {0} from POP3 server {1}.\n\nInternal error was: {2}",
                                                                                         SourceInfo.CompleteUserName, SourceInfo.Pop3Server, ex.Message)));
#endif
                    //What to return
                    //                    if (ServiceStopRequested) return new Dictionary<MailMessageEx, MimeEntity>();
                    return new Dictionary<MailMessageEx, MimeEntity>();
                }
                finally
                {
                    try
                    {
                        if (client.CurrentState == Pop3State.Transaction)
                        {
                            client.Noop();
                        }
                        client.Quit();
                    }
                    catch (Exception ex)
                    {
                        messages = new Dictionary<MailMessageEx, MimeEntity>();
                        //DepictionExceptionHandler.HandleException(ex, false);
                    }
                }
            }
            UpdateStatusReport(string.Format("Read {0} of {1} emails", totalRead, totalMessages));
            if (ServiceStopRequested) return new Dictionary<MailMessageEx, MimeEntity>();
            return messages;
        }

        #region static helpers
        public static IElementPrototype GetRawPrototypeFromEmailWithUniqueID(string subject, string body, string from, string to, string deliveryDate, string messageID)
        {
            return GetRawPrototypeFromEmailWithUniqueID(subject, body, from, to, deliveryDate, messageID, null);
        }

        public static IElementPrototype GetRawPrototypeFromEmailWithUniqueID(string subject, string body, string from, string to, string deliveryDate, string messageID,
                                                                             IDepictionGeocoder[] usableGeocoders)
        {
            DateTime emailTime = DateTime.MinValue;
            DateTime.TryParse(deliveryDate, out emailTime);
                    
            var prototype = FleetEyesEmailParser.GetRawPrototypeFromEmailSubjectAndBody(subject, body,emailTime, usableGeocoders);
            

            var prop = new DepictionElementProperty("Email subject", subject);
            prop.Editable = false;
            prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

            prop = new DepictionElementProperty("Email body", body);
            prop.Editable = false;
            prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

            prop = new DepictionElementProperty("Email from", from);
            prop.Editable = false;
            prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

            prop = new DepictionElementProperty("Email to", to);
            prop.Editable = false;
            prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

            prop = new DepictionElementProperty("Email sent", deliveryDate);
            prop.Editable = false;
            prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

            FleetEyesEmailParser.CreateEIDIfMissing(prototype, messageID);

            prop = new DepictionElementProperty("draggable", false);
            prototype.AddPropertyOrReplaceValueAndAttributes(prop, false);

            prototype.Tags.Add(FleetEyesTag + to);
            return prototype;
        }
        #region these arent used all that much in the real app
        public static IDepictionElement GetElementFromEmail(string subject, string body, string from, string to, string deliveryDate, string messageID)
        {
            return GetElementFromEmail(subject, body, from, to, deliveryDate, messageID, string.Empty, null);
        }
        public static IDepictionElement GetElementFromEmail(string subject, string body, string from, string to, string deliveryDate, string messageID,
                                                            IDepictionGeocoder[] usableGeocoders)
        {
            return GetElementFromEmail(subject, body, from, to, deliveryDate, messageID, string.Empty, usableGeocoders);
        }

        public static IDepictionElement GetElementFromEmail(string subject, string body, string from, string to, string deliveryDate, string messageID, string defaultElementType,
                                                            IDepictionGeocoder[] usableGeocoders)
        {
            var rawDataFilledPrototype = GetRawPrototypeFromEmailWithUniqueID(subject, body, from, to, deliveryDate, messageID, usableGeocoders);

            var element = ElementFactory.CreateElementFromPrototype(rawDataFilledPrototype);

            if (element == null) return null;

            element.SetPropertyValue("draggable", false, false);

            return element;
        }
        #endregion

        private static string GetPlainTextBody(MailMessage message, MimeEntity entity)
        {
            Stream stream = null;
            foreach (MimeEntity child in entity.Children)
            {
                if (child.ContentType.MediaType.Equals(MediaTypes.TextPlain))
                {
                    stream = child.Content;
                }
            }

            if (stream == null)
            {
                foreach (AlternateView view in message.AlternateViews)
                {
                    if (view.ContentType.MediaType.Equals(MediaTypes.TextPlain))
                    {
                        stream = view.ContentStream;
                    }
                }
            }

            if (stream != null)
            {
                stream.Position = 0;
                var streamReader = new StreamReader(stream);
                return streamReader.ReadToEnd();
            }

            //fall back to sending the entire mime content
            return message.Body;
        }

        #endregion

        #region Methods
        /// <summary>
        /// This is the method that gets used by Live Reports. The element prototypes are given the properties from the email, then converted into real elements and
        /// added to the depiction story.
        /// </summary>
        /// <returns></returns>
        public List<IElementPrototype> GetEmailsAsRawPrototypes(IDepictionGeocoder[] usableGeocoders)
        {
            var prototypes = new List<IElementPrototype>();
            var messages = GetNonreadMessagesFromEmailLocation();
            if (messages == null) return prototypes;
            foreach (var key in messages.Keys)
            {
                if (ServiceStopRequested) break;
                if (key.MessageId != null && !emailLog.ContainsKey(key.MessageId))
                {
                    var subj = key.Subject ?? "";
                    if (!FleetEyesEmailParser.IsSubjectUsable(subj)) continue;
                    var from = key.From.ToString();
                    var to = key.To.ToString();
                    var deliveryDate = key.Headers[MailHeaders.Date];
                    string body = GetPlainTextBody(key, messages[key]) ?? "";
                    var prototype = GetRawPrototypeFromEmailWithUniqueID(subj, body, from, to, deliveryDate, key.MessageId, usableGeocoders);
                    if (prototype != null)
                    {
                        //check the region limit and remove if tghe region is not null
                        if(regionLimit == null)
                        {
                            prototypes.Add(prototype);
                        }else
                        {
                            LatitudeLongitudeBase position = null;
                            if(prototype.GetPropertyValue("Position", out position))
                            {
                                if(regionLimit.Contains(position))
                                {
                                    prototypes.Add(prototype);
                                }
                            }else
                            {
                                prototypes.Add(prototype);
                            }
                        }
                    }

                    emailLog.Add(key.MessageId, true);
                }
            }
            ReportsRead += prototypes.Count;
            return prototypes;
        }

        public bool IsValidEmailAddressPassword()
        {
            if (!DepictionInternetConnectivityService.IsInternetAvailable)
            {
                DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Please check internet connection"));
                //return false;
            }
            try
            {
                // Validate the account settings
                using (var email = new Pop3Client(new Pop3ClientParams(SourceInfo.Pop3Server, SourceInfo.UseSSL ? 995 : 110, SourceInfo.UseSSL, SourceInfo.CompleteUserName, SourceInfo.Password)))
                {
                    email.Authenticate();
                    email.Stat();
                    email.Noop();
                    email.Quit();
                }
                FleetEyesEmailInformationSettings.AddIncomingEmailAccount(new FleetEyesEmailSettings(SourceInfo.SimpleUserName, SourceInfo.Pop3Server, SourceInfo.UseSSL));
            }
            catch (Exception ex)
            {
                if (ex is Pop3Exception)
                {
                    if (ex.Message.Contains("-ERR [IN-USE]"))
                    {
                        return true;
                    }
                    if (ex.InnerException != null && ex.InnerException is SocketException)
                    {
                        DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Could not connect to server " + SourceInfo.Pop3Server, 10));

                    }
                    else
                        DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Invalid login credentials for: " + SourceInfo.CompleteUserName, 10));
                }
                else if (ex is ArgumentNullException)
                {
                    var exc = ex as ArgumentNullException;

                    DepictionAccess.NotificationService.DisplayMessage(
                        new DepictionMessage(string.Format("Please enter a {0} for: {1} ", exc.ParamName, SourceInfo.CompleteUserName), 10));
                }
                else
                {
                    DepictionAccess.NotificationService.DisplayMessage(new DepictionMessage("Unknown login exception for: " + SourceInfo.CompleteUserName, 10));
                }
                return false;
            }
            return true;
        }

        public void AddReadEmailsToDepictionStory(IDepictionStory currentDepiction, List<IElementPrototype> rawPrototypes)
        {
            if (ServiceStopRequested) return;
            if (currentDepiction != null)
            {
                List<IDepictionElement> updatedList;
                List<IDepictionElement> createdList;
                //Sort the rawprototypes by email sent property, which should hopefully exist
                currentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(rawPrototypes, out createdList, out updatedList, true, true);
                foreach (var element in createdList)
                {
                    var readOnlyProp = element.GetPropertyByInternalName("name", true);
                   
                    if (readOnlyProp != null)
                    {
                        if (!readOnlyProp.IsHoverText)
                        {
                            readOnlyProp.IsHoverText = true;
                            element.UpdateToolTip();
                        }
                    }
                    ElementFactory.RunElementZOIGenerationBehaviors(element);
                }
                foreach (var element in updatedList)
                {
                    ElementFactory.RunElementZOIGenerationBehaviors(element);
                }
            }
        }
        #endregion

        #region protected and private helpers

        private int ConvertMinutesToMilliSeconds(double minutes)
        {
            return (int)(minutes * 60 * 1000);
        }

        protected void TimedEmailReader(object args)
        {
            UpdateStatusReport("Reading from Fleet Eyes source");
            Thread.Sleep(100);
            collectionStopWatch.Change(Timeout.Infinite, Timeout.Infinite);
            var randomNumberGenerator = new Random();
            readingEmails = true;
           
            try
            {
                if(ServiceStopRequested)
                {
                    readingEmails = false;
                    return;
                }
                var rawPrototypes = GetEmailsAsRawPrototypes(null);
                if (ServiceStopRequested)
                {
                    readingEmails = false;
                    return;
                }
                UpdateStatusReport(string.Format("Read {0} email elements", rawPrototypes.Count));
                if (ServiceStopRequested)
                {
                    readingEmails = false;
                    return;
                }
                AddReadEmailsToDepictionStory( DepictionAccess.CurrentDepiction,rawPrototypes);

            }
            catch (Exception ex)
            {
                Debug.WriteLine("There was an exception in Live Report reader");
                Debug.WriteLine(ex.Message);
            }

            if (collectionStopWatch != null)
            {
                if (!ServiceStopRequested)
                {
                    UpdateStatusReport("Waiting for next live report poll");
                    Thread.Sleep(100);
                    // Randomly vary the sleep period to reduce the chance of colliding
                    // with other Depictions accessing the same POP3 account.
                    var millisecondsToSleep = randomNumberGenerator.Next(100, 200);
                    var refreshRateMillisecond = (int) (RefreshRateMinutes*60*1000);
                    var waitTime = refreshRateMillisecond + millisecondsToSleep;
                    collectionStopWatch.Change(waitTime, waitTime);
                }
            }
            readingEmails = false;
        }


        #endregion
        #region Overrides of BaseDepictionBackgroundThreadOperation

        public override void RefreshResult()
        {
            if (ServiceStopRequested || readingEmails) return;

            if (collectionStopWatch != null)
            {
                if (!ServiceStopRequested)
                {
                    UpdateStatusReport("Reading Live Report elements");
                    Thread.Sleep(100);
                    collectionStopWatch.Change(0, ConvertMinutesToMilliSeconds(RefreshRateMinutes));
                }
            }
        }
        override public void StopBackgroundService()
        {
            Debug.WriteLine(DateTime.Now + " Stopping the service");
            ServiceStopRequested = true;
            collectionStopWatch.Dispose();
            collectionStopWatch = null;
            timeWaiter.Set();
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            //Not sure why this was around
//            if (DepictionAccess.GeoCodingService != null)
//            {
//                var geoCoders = DepictionAccess.GeoCodingService.GeoCodeRawStringAddress("US");
//            }
            if(!IsValidEmailAddressPassword()) return false;
            UpdateStatusReport("Updating Fleet Eyes reports");
            Thread.Sleep(100);
            timeWaiter = new EventWaitHandle(false, EventResetMode.ManualReset);
          
            return true;
        }
        protected override object ServiceAction(object args)
        {
            collectionStopWatch = new Timer(TimedEmailReader, this, 0, ConvertMinutesToMilliSeconds(RefreshRateMinutes));
            timeWaiter.WaitOne();
            return null;
        }
        protected override void ServiceComplete(object args)
        {
            Debug.WriteLine("done with service");
        }

        #endregion
    }
}