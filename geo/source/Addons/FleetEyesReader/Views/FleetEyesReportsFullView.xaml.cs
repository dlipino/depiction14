﻿using System.Windows;
using System.Windows.Controls;
using FleetEyesReader.EmailSettings;
using FleetEyesReader.ViewModels;

namespace FleetEyesReader.Views
{
    public partial class FleetEyesReportsFullView
    {
        public FleetEyesReportsFullView()
        {
            InitializeComponent();
        }

        private void cboExistingEmailAddresses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dc = DataContext as FleetEyesReportSettupVM;
            if (dc == null) return;
            var elem = sender as ComboBox;
            if (elem == null) return;
            dc.SelectedEmailAccount = elem.SelectedItem as FleetEyesEmailSettings;
            txtPassword.Password = "";
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as FleetEyesReportSettupVM;
            if (dc == null) return;
            var elem = sender as PasswordBox;
            if (elem == null) return;
            dc.EmailPassword = elem.Password;
        }
    }
}