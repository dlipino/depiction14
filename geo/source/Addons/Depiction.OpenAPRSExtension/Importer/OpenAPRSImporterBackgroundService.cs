using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Xml;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.TypeConverter;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace Depiction.OpenAPRSExtension.Importer
{
    public class OpenAPRSImporterBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        //This  thread operation will work differently since it is the first attempt at a timer based thread operation.
        private Timer collectionStopWatch;
        private EventWaitHandle timeWaiter;
        private double minTimeMinutes = 2d;
        private int desiredMilliRefreshTime = 2 * 60 * 1000;
        private IMapCoordinateBounds regionOfInterest = null;
        private bool updatingElements;//half assed lock
        private int labelWidth = 350;
        private int labelHeight = 300;
        private string prototypeTag = string.Empty;
        public override bool IsServiceRefreshable
        {
            get
            {
                return true;
            }
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {

            timeWaiter = new EventWaitHandle(false, EventResetMode.ManualReset);
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            regionOfInterest = parameters["RegionBounds"] as IMapCoordinateBounds;
            if (parameters.ContainsKey("Tag"))
            {
                var tag = parameters["Tag"] as string;
                if (!string.IsNullOrEmpty(tag))
                {
                    prototypeTag = tag;
                }
            }
            if (parameters.ContainsKey("RefreshTimeMinutes"))
            {
                double desiredRefreshTime;
                if (double.TryParse(parameters["RefreshTimeMinutes"].ToString(), out desiredRefreshTime))
                {
                    if (desiredRefreshTime > minTimeMinutes)
                    {
                        desiredMilliRefreshTime = ConvertMinutesToMilliSeconds(desiredRefreshTime);
                    }
                }
            }

            collectionStopWatch = new Timer(TimedAPRSGetter, regionOfInterest, 0, desiredMilliRefreshTime);
            timeWaiter.WaitOne();
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            Debug.WriteLine("Stopping Open APRS Service");
        }

        public override void StopBackgroundService()
        {
            Debug.WriteLine(DateTime.Now + " Stopping the service");
            ServiceStopRequested = true;
            collectionStopWatch.Dispose();
            collectionStopWatch = null;
            timeWaiter.Set();
        }

        public override void RefreshResult()
        {
            if(ServiceStopRequested || updatingElements) return;

            if (collectionStopWatch != null)
            {
                if (!ServiceStopRequested)
                {
                    UpdateStatusReport("Updating Open APRS elements");
                    Thread.Sleep(100);
                    collectionStopWatch.Change(0, desiredMilliRefreshTime);
                }
            }
        }

        #region private helpers

        private int ConvertMinutesToMilliSeconds(double minutes)
        {
            return (int)(minutes * 60 * 1000);
        }

        protected void TimedAPRSGetter(object area)
        {
            if (ServiceStopRequested) return;
            updatingElements = true;
            Debug.WriteLine(DateTime.Now + " from timer start");
            UpdateStatusReport("Updating Open APRS elements");
            Thread.Sleep(100);
            collectionStopWatch.Change(Timeout.Infinite, Timeout.Infinite);
            //            Thread.Sleep(3000);
            var region = area as IMapCoordinateBounds;
            if (region == null)
            {
                updatingElements = false;
                return;
            }
            var prototypes = GatherOpenAprsElements(region);
            var story = DepictionAccess.CurrentDepiction;
            if (story == null)
            {
                updatingElements = false;
                return;
            }
            story.CreateOrUpdateDepictionElementListFromPrototypeList(prototypes, true,false);

            Debug.WriteLine(DateTime.Now + " from after sleep");
            if (collectionStopWatch != null)
            {
                if (!ServiceStopRequested)
                {
                    UpdateStatusReport("Waiting for next APRS poll");
                    Thread.Sleep(100);
                    collectionStopWatch.Change(desiredMilliRefreshTime, desiredMilliRefreshTime);
                }
            }
            updatingElements = false;
        }

        protected IElementPrototype[] GatherOpenAprsElements(IMapCoordinateBounds area)
        {
            
            var elements = new List<IElementPrototype>();
            string url = string.Format("http://www.openaprs.net/ajax/query.php?BBOX={0},{1},{2},{3}&limit=255", area.Left, area.Bottom, area.Right, area.Top);

            byte[] buffer = GetXMLBuffer(url, 0);
            if (buffer == null || buffer.Length == 0)
                return elements.ToArray();
#if DEBUG
            //            WriteToTempFile(buffer);
#endif
            char c = '\0';
            int i = 0;
            while (c != '<')
            {
                c = Convert.ToChar(buffer[i]);
                i++;
            }
            i--;


            using (var memoryStream = new MemoryStream(buffer, i, buffer.Length - i))
            {
                try
                {

                    using (var xmlReader = new XmlTextReader(memoryStream))
                    {
                        
                        while (xmlReader.Read())
                        {
                            if (ServiceStopRequested) return null;
                            if (xmlReader.Name != "station") continue;
                            var prototype = BuildOpenAPRSElement(xmlReader);//, existingElements);
                            if (prototype == null) continue;

                            //                            var cDataProperty = prototype.GetProperty("CDATA");
                            //                            if (cDataProperty != null && cDataProperty.Value is string)
                            //                                prototype.LabelData = (string)cDataProperty.Value;
                            elements.Add(prototype);
                        }
                    }
                }
                catch
                {
                    // Sometimes an exception here may be a web request that the server didn't handle correctly.
                    // Retry it a few times for each set just in case ...
                    //if (exceptionCount < 5)
                    //    return getElements(url, exceptionCount + 1);
                }
            }
            return elements.ToArray();
        }

        private IElementPrototype BuildOpenAPRSElement(XmlTextReader xmlReader)//, IEnumerable<IElement> existingElements)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();
            while (xmlReader.Read() && !(xmlReader.Name == "station" && xmlReader.NodeType == XmlNodeType.EndElement))
            {
                if (xmlReader.NodeType != XmlNodeType.Element) continue;
                if (xmlReader.Depth > 2) continue;
                switch (xmlReader.Name)
                {
                    default:
                        try
                        {
                            properties[xmlReader.Name] = xmlReader.ReadElementContentAsString();
                        }
                        catch { }
                        break;
                }
            }

            string eid = "callsign_id:";
            if (properties.ContainsKey("callsign_id"))
                eid += properties["callsign_id"];

            var prototype = ElementFactory.CreateRawPrototypeOfType("Depiction.Plugin.OpenAPRSStation", false);
            if (prototype == null) return null;
            var eidProp = new DepictionElementProperty("EID", "User element id", eid);
            eidProp.Deletable = false;
            eidProp.Editable = false;
            prototype.AddPropertyOrReplaceValueNotAttributes(eidProp, false);

            if (properties.ContainsKey("latitude") && properties.ContainsKey("longitude"))
            {
                var latLong = new LatitudeLongitude(properties["latitude"], properties["longitude"]);
                prototype.SetInitialPositionAndZOI(latLong, null);
                properties.Remove("latitude");
                properties.Remove("longitude");
            }
            if (properties.ContainsKey("symbol_path"))
            {
                var path = properties["symbol_path"];

                string iconName = "";

                var successfulSave = BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(path, out iconName);

                var depIconPath = new DepictionIconPath(IconPathSource.File, iconName);
                var property = new DepictionElementProperty("IconPath", "Icon path", depIconPath);
                property.Deletable = false;
                prototype.AddPropertyOrReplaceValueNotAttributes(property, false);

                property = new DepictionElementProperty("IconSize", "Icon size", 24d);
                property.Deletable = false;
                prototype.AddPropertyOrReplaceValueNotAttributes(property, false);

                properties.Remove("symbol_path");
            }
            if (properties.ContainsKey("course"))
            {
                var property = new DepictionElementProperty("Course", "Course", properties["course"]);
                property.IsHoverText = true;
                prototype.AddPropertyOrReplaceValueNotAttributes(property, false);

                properties.Remove("course");
            }
            if (properties.ContainsKey("source"))
            {
                var property = new DepictionElementProperty("Source", "Source", properties["source"]);
                property.IsHoverText = true;
                prototype.AddPropertyOrReplaceValueNotAttributes(property);
                properties.Remove("source");
            }
            if (properties.ContainsKey("speed") && properties.ContainsKey("speed_unit"))
            {
                var speed = DepictionTypeConverter.ChangeType(string.Format("{0} {1}", properties["speed"], properties["speed_unit"]),
                                                              typeof(Speed));

                var property = new DepictionElementProperty("Speed", "Speed", speed);
                property.IsHoverText = true;
                prototype.AddPropertyOrReplaceValueNotAttributes(property);

                properties.Remove("speed");
                properties.Remove("speed_unit");
            }
            if (properties.ContainsKey("create_ts"))
            {
                DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                dateTime = dateTime.AddSeconds(double.Parse(properties["create_ts"]));

                var property = new DepictionElementProperty("Timestamp", "Timestamp",
                                                            dateTime.ToLocalTime().ToString("h:mm tt d MMMM yyyy"));
                property.IsHoverText = true;
                prototype.AddPropertyOrReplaceValueNotAttributes(property);
                properties.Remove("create_ts");
            }

            foreach (var kvp in properties)
            {
                var property = new DepictionElementProperty(kvp.Key, kvp.Key, kvp.Value);

                prototype.AddPropertyOrReplaceValueNotAttributes(property);
                if (kvp.Key.Equals("CDATA", StringComparison.InvariantCultureIgnoreCase))
                {
                    property.IsHoverText = true;
                    prototype.UseEnhancedPermaText = true;
                    prototype.PermaText.PixelWidth = labelWidth;
                    prototype.PermaText.PixelHeight = labelHeight;
                }
            }
            if (!string.IsNullOrEmpty(prototypeTag))
            {
                prototype.Tags.Add(prototypeTag);
            }
            return prototype;
        }

        private byte[] GetXMLBuffer(string url, int exceptionCount)
        {
            var webClient = new WebClient();
            var buffer = new byte[0];

            try
            {
                buffer = webClient.DownloadData(url);
            }
            catch (WebException)
            {
                if (ServiceStopRequested) return null;
                // Retry 3 times if there is a web exception (time-out)
                if (exceptionCount < 3)
                    return GetXMLBuffer(url, exceptionCount + 1);
            }
            if (ServiceStopRequested) return null;
            return buffer;
        }

        #endregion
    }
}