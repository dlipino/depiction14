﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    public class WeatherParserHelpers
    {
//         c = wind direction (in degrees).
//s = sustained one-minute wind speed (in mph).
//   g = gust (peak wind speed in mph in the last 5 minutes).
//   t = temperature (in degrees Fahrenheit). Temperatures below zero are expressed as -01 to -99.
//   r = rainfall (in hundredths of an inch) in the last hour.
//   p = rainfall (in hundredths of an inch) in the last 24 hours.
//   P = rainfall (in hundredths of an inch) since midnight.
//   h = humidity (in %. 00 = 100%).
//   b = barometric pressure (in tenths of millibars/tenths of hPascal).
//Other parameters that are available on some weather station units include:
//   L = luminosity (in watts per square meter) 999 and below.
//   l=(lower-case letter “L”)  = luminosity (in watts per square meter)
//1000 and above.(L is inserted in place of one of the rain values).
//s = snowfall (in inches) in the last 24 hours.
//# = raw rain counter

        static public bool wx_parse(APRSPacketFAPDetails inPacket, string bodyPart)
        {
            string windDirS = null, windSpeedS = null, tempS = null, windGustS = null;
            //Supposed to do a search and replace
            //s/^_{0,1}([\d \.\-]{3})\/([\d \.]{3})g([\d \.]+)t(-{0,1}[\d \.]+)//
            var wxp1 = @"^_{0,1}([\d \.\-]{3})\/([\d \.]{3})g([\d \.]+)t(-{0,1}[\d \.]+)";
            //             s/^_{0,1}c([\d \.\-]{3})s([\d \.]{3})g([\d \.]+)t(-{0,1}[\d \.]+)//
            var wxp2 = @"^_{0,1}c([\d \.\-]{3})s([\d \.]{3})g([\d \.]+)t(-{0,1}[\d \.]+)";
            //            s/^_{0,1}([\d \.\-]{3})\/([\d \.]{3})t(-{0,1}[\d \.]+)//
            var wxp3 = @"^_{0,1}([\d \.\-]{3})\/([\d \.]{3})t(-{0,1}[\d \.]+)";
            //            s/^_{0,1}([\d \.\-]{3})\/([\d \.]{3})g([\d \.]+)//
            var wxp4 = @"^_{0,1}([\d \.\-]{3})\/([\d \.]{3})g([\d \.]+)";
            //            s/^g(\d+)t(-{0,1}[\d \.]+)//
            var wxp5 = @"^g(\d+)t(-{0,1}[\d \.]+)";
            var leftOver = bodyPart;
            GroupCollection groups;
            var wxInfo = new APRSFapWeatherInfo();
            if ((groups = Regex.Match(leftOver, wxp1).Groups).Count > 4)
            {
                windDirS = groups[1].Value;
                windSpeedS = groups[2].Value;
                windGustS = groups[3].Value;
                tempS = groups[4].Value;

                leftOver = Regex.Replace(leftOver, wxp1, "");
            }
            else if ((groups = Regex.Match(leftOver, wxp2).Groups).Count > 4)
            {
                windDirS = groups[1].Value;
                windSpeedS = groups[2].Value;
                windGustS = groups[3].Value;
                tempS = groups[4].Value;
                leftOver = Regex.Replace(leftOver, wxp2, "");

            }
            else if ((groups = Regex.Match(leftOver, wxp3).Groups).Count > 3)
            {
                windDirS = groups[1].Value;
                windSpeedS = groups[2].Value;
                tempS = groups[3].Value;
                leftOver = Regex.Replace(leftOver, wxp3, "");
            }
            else if ((groups = Regex.Match(leftOver, wxp4).Groups).Count > 3)
            {
                windDirS = groups[1].Value;
                windSpeedS = groups[2].Value;
                windGustS = groups[3].Value;
                leftOver = Regex.Replace(leftOver, wxp4, "");
            }
            else if ((groups = Regex.Match(leftOver, wxp5).Groups).Count > 2)
            {
                windGustS = groups[1].Value;
                tempS = groups[2].Value;
                leftOver = Regex.Replace(leftOver, wxp5, "");
            }
            else
            {
                return false;
            }

            //Check for missing tempurature
            //s/t(-{0,1}\d{1,3})//
            var tempurPattern = @"t(-{0,1}\d{1,3})";
            if (string.IsNullOrEmpty(tempS) && (groups = Regex.Match(leftOver, tempurPattern).Groups).Count > 1)
            {
                tempS = groups[1].Value;
                double.TryParse(tempS, out wxInfo.Temperature);
                leftOver = Regex.Replace(leftOver, tempurPattern, "");
            }
            double holder;
            if (!string.IsNullOrEmpty(windDirS) && double.TryParse(windDirS, out holder))
            {
                wxInfo.WindDirection = holder;
            }
            if (!string.IsNullOrEmpty(windGustS) && double.TryParse(windGustS, out holder))
            {
                wxInfo.WindGust = holder;// *MiniHelpers.MPH_TO_MS;
            }
            if (!string.IsNullOrEmpty(windSpeedS) && double.TryParse(windSpeedS, out holder))
            {
                wxInfo.WindSpeed = holder;// * MiniHelpers.MPH_TO_MS;
            }
            if (!string.IsNullOrEmpty(tempS) && double.TryParse(tempS, out holder))
            {
                wxInfo.Temperature = holder;// MiniHelpers.FahrenheitToCelcius(holder);
            }
            string rain1hS = null, rain24hS = null, rainMidnight = null;
            //            s/r(\d{1,3})//
            var rain1hPattern = @"r(\d{1,3})";
            if ((groups = Regex.Match(leftOver, rain1hPattern).Groups).Count > 1)
            {
                rain1hS = groups[1].Value;
                double tempR1hs;
                double.TryParse(rain1hS, out tempR1hs);
                wxInfo.RainLastHour = tempR1hs/100d;// *MiniHelpers.HINCH_TO_MM;
                leftOver = Regex.Replace(leftOver, rain1hPattern, "");
                //		$w{'rain_1h'} = sprintf('%.1f', $1*$hinch_to_mm); # during last 1h
            }
            //            s/p(\d{1,3})//
            var rain24hPattern = @"p(\d{1,3})";
            if ((groups = Regex.Match(leftOver, rain24hPattern).Groups).Count > 1)
            {
                rain24hS = groups[1].Value;
                double tempR24hs;
                double.TryParse(rain24hS, out tempR24hs);
                wxInfo.RainLast24Hours = tempR24hs/100d;// *MiniHelpers.HINCH_TO_MM;
                leftOver = Regex.Replace(leftOver, rain24hPattern, "");
                //		$w{'rain_24h'} = sprintf('%.1f', $1*$hinch_to_mm); # during last 24h
            }
            //s/P(\d{1,3})//
            var rainMidnightPattern = @"P(\d{1,3})";
            if ((groups = Regex.Match(leftOver, rainMidnightPattern).Groups).Count > 1)
            {
                rainMidnight = groups[1].Value;
                double tempRMidhs;
                double.TryParse(rainMidnight, out tempRMidhs);
                wxInfo.RainSinceMidnight = tempRMidhs/100d;// *MiniHelpers.HINCH_TO_MM;
                leftOver = Regex.Replace(leftOver, rainMidnightPattern, "");
                //		$w{'rain_midnight'} = sprintf('%.1f', $1*$hinch_to_mm); # since midnight
            }
            //            s/h(\d{1,3})//

            var humPattern = @"h(\d{1,3})";
            if ((groups = Regex.Match(leftOver, humPattern).Groups).Count > 1)
            {
                //                $w{'humidity'} = sprintf('%.0f', $1); # percentage
                //		$w{'humidity'} = 100 if ($w{'humidity'} eq 0);
                //		undef $w{'humidity'} if ($w{'humidity'} > 100 || $w{'humidity'} < 1);

                double hummid;
                double.TryParse(groups[1].Value, out hummid);
                if (hummid == 0) hummid = 100;
                if (hummid > 100 || hummid < 1) hummid = double.NaN;
                wxInfo.Humidity = hummid;
                leftOver = Regex.Replace(leftOver, humPattern, "");
            }
            //            s/b(\d{4,5})//
            var pressPattern = @"b(\d{4,5})";
            if ((groups = Regex.Match(leftOver, pressPattern).Groups).Count > 1)
            {
                //                $w{'pressure'} = sprintf('%.1f', $1/10); # results in millibars
                double pressure;
                double.TryParse(groups[1].Value, out pressure);
                wxInfo.Pressure = pressure / 10.0;
                leftOver = Regex.Replace(leftOver, pressPattern, "");
            }
            //            s/([lL])(\d{1,3})//
            var lumPattern = @"([lL])(\d{1,3})";
            if ((groups = Regex.Match(leftOver, lumPattern).Groups).Count > 2)
            {
                //                $w{'luminosity'} = sprintf('%.0f', $2); # watts / m2
                //		$w{'luminosity'} += 1000 if ($1 eq 'l');
                double tLum;
                double.TryParse(groups[2].Value, out tLum);
                if (groups[1].Value.Equals('l'))
                {
                    tLum += 1000;
                }
                wxInfo.Luminosity = tLum;
                leftOver = Regex.Replace(leftOver, lumPattern, "");
            }
            //            s/v([\-\+]{0,1}\d+)//
            var whatPattern = @"v([\-\+]{0,1}\d+)";
            if ((groups = Regex.Match(leftOver, whatPattern).Groups).Count > 1)
            {
                //who knows what is supposed to happen here
                leftOver = Regex.Replace(leftOver, whatPattern, "");
            }

            //             s/s(\d{1,3})//
            var snowPattern = @"s(\d{1,3})";
            if ((groups = Regex.Match(leftOver, snowPattern).Groups).Count > 1)
            {
                //                # snowfall
                //		$w{'snow_24h'} = sprintf('%.1f', $1*$hinch_to_mm);
                double tempRMidhs;
                double.TryParse(groups[1].Value, out tempRMidhs);
                wxInfo.SnowLast24Hours = tempRMidhs/100d;// *MiniHelpers.HINCH_TO_MM;
                leftOver = Regex.Replace(leftOver, snowPattern, "");
            }
            //           s/#(\d+)//
            var rawRainPattern = @"#(\d+)";
            if ((groups = Regex.Match(leftOver, rawRainPattern).Groups).Count > 1)
            {
                //     raw rain counter
                leftOver = Regex.Replace(leftOver, rawRainPattern, "");
            }
            //            $s =~ s/^([rPphblLs#][\. ]{1,5})+//;
            leftOver = Regex.Replace(leftOver, @"^([rPphblLs#][\. ]{1,5})+", "");
            //            $s =~ s/^\s+//;
            leftOver = Regex.Replace(leftOver, @"^\s+", "");
            //	$s =~ s/\s+/ /;
            leftOver = Regex.Replace(leftOver, @"\s+", " ");
            //            /^[a-zA-Z0-9\-_]{3,5}$/

            if (Regex.IsMatch(leftOver, @"^[a-zA-Z0-9\-_]{3,5}$"))
            {
                //                $w{'soft'} = substr($s, 0, 16) if ($s ne '');
                var max = 16;
                if (leftOver.Length < max)
                {
                    max = leftOver.Length;
                }
                var sub = leftOver.Substring(0, max);
                if (!string.IsNullOrEmpty(sub))
                {
                    wxInfo.SoftwareIndicator = sub;
                }
            }
            else
            {
                inPacket.comment = MiniHelpers.CleanupComment(leftOver);
            }
            if (!string.IsNullOrEmpty(tempS) ||
                !string.IsNullOrEmpty(windSpeedS) &&
                !string.IsNullOrEmpty(windDirS))
            {
                inPacket.wx_report = wxInfo;
                return true;
            }
            return false;
        }

        static public bool wx_parse_peet_packet(APRSPacketFAPDetails inPacket, string bodyPart)
        {
            var bodyBreak = bodyPart;
            GroupCollection groups;
            var foundValues = new List<double>();
            //s/^([0-9a-f]{4}|----)//i
            var parsePattern = @"^([0-9a-f]{4}|----)";
            while ((groups = Regex.Match(bodyBreak, parsePattern, RegexOptions.IgnoreCase).Groups).Count > 1)
            {
                var val = groups[1].Value;
                if (val.Equals("----"))
                {
                    foundValues.Add(double.NaN);
                }
                else
                {
                    int num;
                    num = Convert.ToInt32(val, 16);
                    if (num >= 32768) num =num- 65536;
//                    //  my $v = unpack('n', pack('H*', $1));
//                    //			push @vals, ($v < 32768) ? $v : $v - 65536;
                    foundValues.Add(num);
                }
                bodyBreak = Regex.Replace(bodyBreak, parsePattern, "", RegexOptions.IgnoreCase);
            }
            const int wind_gust = 0;//wind speed [0.1 kph] (last 5 min)
            const int wind_direction = 1;//	wind direction of peak [0-255]
            const int temp = 2;//current outdoor temp [0.1 deg F]
            const int rain_24h = 3;//rain long term total [0.01 inches]
            const int pressure = 4;//current barometer [0.1 mbar]
            const int barometerDelta = 5;//barometer delta value [0.1 mbar]
            const int BarometerCorrFactorLSW = 6;//barometer correction factor [lsw]
            const int BarometerCorrFactorMSW = 7;//barometer correction factor [msw]
            const int humidity = 8;//current outdoor humidity [0.1%]
            const int date = 9;//date [day of year]
            const int time = 10;//	time [minute of day]
            const int rain_midnight = 11;//today's rain total [0.01 inches]
            const int wind_speed = 12;//1 minute wind speed average [0.1kph]
            var weatherHolder = new APRSFapWeatherInfo();
            for (int i = 0; i < foundValues.Count; i++)
            {
                switch (i)
                {
                    case wind_speed:
                        if (!foundValues[wind_speed].Equals(double.NaN))
                        {
                            weatherHolder.WindSpeed = (foundValues[wind_speed] / 10.0) * MiniHelpers.KMH_TO_MPH;// * MiniHelpers.KMH_TO_MS) / 10.0;
                        }
                        break;
                    case wind_direction:
                        if (!foundValues[wind_direction].Equals(double.NaN))
                        {
                            weatherHolder.WindDirection = foundValues[wind_direction] * 360.0 / 255.0;//((((int)foundValues[wind_direction]) & 0xff) * 1.41176);
                        }
                        break;
                    case temp:
                        if (!foundValues[temp].Equals(double.NaN))
                        {
                            weatherHolder.Temperature = foundValues[temp]/10.0;//MiniHelpers.FahrenheitToCelcius(foundValues[temp] / 10);
                        }
                        break;
                    case rain_midnight:
                        if (!foundValues[rain_midnight].Equals(double.NaN))
                        {
                            weatherHolder.RainSinceMidnight = foundValues[rain_midnight]/100.0;
                        }
                        break;
                    case pressure:
                        var press = foundValues[pressure];
                        if (!press.Equals(double.NaN) && press >= 10)
                        {
                            weatherHolder.Pressure = press / 10.0;
                        }
                        break;
                    case humidity:
                        if (!foundValues[humidity].Equals(double.NaN))
                        {
                            var hum = (foundValues[humidity]) / 10.0;
                            if (hum <= 100 && hum >= 1)
                            {
                                weatherHolder.Humidity = hum;
                            }
                        }
                        break;
                    case rain_24h:
                        if (!foundValues[rain_24h].Equals(double.NaN))
                        {
                            weatherHolder.RainLast24Hours = foundValues[rain_24h] / 100d;
                        }
                        break;
                    case wind_gust:
                        if (!foundValues[wind_gust].Equals(double.NaN))
                        {
                            weatherHolder.WindGust = (foundValues[wind_gust] / 10.0d) * MiniHelpers.KMH_TO_MPH;//(foundValues[wind_speed] * MiniHelpers.KMH_TO_MS) / 10.0;
                        }
                        break;
                }
            }

            if (!weatherHolder.Temperature.Equals(double.NaN) ||
               (!weatherHolder.WindSpeed.Equals(double.NaN) && !weatherHolder.WindDirection.Equals(double.NaN)) ||
                !weatherHolder.Pressure.Equals(double.NaN) ||
                  !weatherHolder.Humidity.Equals(double.NaN))
            {
                inPacket.wx_report = weatherHolder;
                return true;
            }
            return false;
        }

        static public bool wx_parse_peet_logging(APRSPacketFAPDetails inPacket, string bodyPart)
        {
            var bodyBreak = bodyPart;
            GroupCollection groups;
            var foundValues = new List<double>();
            //s/^([0-9a-f]{4}|----)//i
            var parsePattern = @"^([0-9a-f]{4}|----)";
            while ((groups = Regex.Match(bodyBreak, parsePattern, RegexOptions.IgnoreCase).Groups).Count > 1)
            {
                var val = groups[1].Value;
                if (val.Equals("----"))
                {
                    foundValues.Add(double.NaN);
                }
                else
                {
                    int num;
                    num = Convert.ToInt32(val, 16);
                    if (num >= 32768) num -= 65536;
//                    //                    my $v = unpack('n', pack('H*', $1));
//                    //			push @vals, ($v < 32768) ? $v : $v - 65536;

                    foundValues.Add(num);
                }
                bodyBreak = Regex.Replace(bodyBreak, parsePattern, "", RegexOptions.IgnoreCase);
            }

            const int wind_gust = 0;//wind speed [0.1 kph]
            const int wind_direction = 1;//wind direction [0-255]
            const int temp = 2;//current outdoor temp [0.1 deg F]
            const int rain_24h = 3;//rain long term total [0.01 inches]
            const int pressure = 4;//current barometer [0.1 mbar]
            const int temp_in = 5;//current indoor temp [0.1 deg F]
            const int humidity = 6;//current outdoor humidity [0.1%]
            const int humidity_in = 7;//current indoor humidity [0.1%]
            const int date = 8;//date [day of year, -1]
            const int time = 9;//time [minute of day, 0-1440]
            const int rain_midnight = 10;//today's rain total [0.01 inches]
            const int wind_speed = 11;//1 min wind speed average [0.1 kph]

            var weatherHolder = new APRSFapWeatherInfo();
            for (int i = 0; i < foundValues.Count; i++)
            {
                switch (i)
                {
                    case wind_speed:
                        if (!foundValues[wind_speed].Equals(double.NaN))
                        {
                            weatherHolder.WindSpeed = (foundValues[wind_speed]  / 10d)*MiniHelpers.KMH_TO_MPH;
                        }
                        break;
                    case wind_direction:
                        if (!foundValues[wind_direction].Equals(double.NaN))
                        {
                            weatherHolder.WindDirection = foundValues[wind_direction]*360d/255d;//((((int)foundValues[wind_direction]) & 0xff) * 1.41176);
                        }
                        break;
                    case temp:
                        if (!foundValues[temp].Equals(double.NaN))
                        {
                            weatherHolder.Temperature = foundValues[temp] / 10d;
                        }
                        break;
                    case rain_midnight:
                        if (!foundValues[rain_midnight].Equals(double.NaN))
                        {
                            weatherHolder.RainSinceMidnight = foundValues[rain_midnight]/100d;
                        }
                        break;
                    case pressure:
                        var press = foundValues[pressure];
                        if (!press.Equals(double.NaN) && press>=10)
                        {
                            weatherHolder.Pressure =press / 10d;
                        }
                        break;
                    case temp_in:
                        if (!foundValues[temp_in].Equals(double.NaN))
                        {
                            weatherHolder.IndoorTemperature = foundValues[temp_in]/ 10d;
                        }
                        break;
                    case humidity:
                        if (!foundValues[humidity].Equals(double.NaN))
                        {
                            var hum = (foundValues[humidity]) / 10d;
                            if (hum <= 100 && hum >= 1)
                            {
                                weatherHolder.Humidity = hum;
                            }
                        }
                        break;
                    case humidity_in:
                        if (!foundValues[humidity_in].Equals(double.NaN))
                        {
                            var hum = (foundValues[humidity_in]) / 10d;
                            if (hum <= 100 && hum >= 1)
                            {
                                weatherHolder.Humidity = hum;
                            }
                        }
                        break;
                    case rain_24h:
                        if (!foundValues[rain_24h].Equals(double.NaN))
                        {
                            weatherHolder.RainLast24Hours = foundValues[rain_24h]/100d; //* MiniHelpers.HINCH_TO_MM);
                        }
                        break;
                    case wind_gust:
                        if (!foundValues[wind_gust].Equals(double.NaN))
                        {
                            weatherHolder.WindSpeed = (foundValues[wind_gust] / 10d) * MiniHelpers.KMH_TO_MPH;// * MiniHelpers.KMH_TO_MS) / 10.0;
                        }
                        break;
                }
            }

            if(weatherHolder.Temperature.Equals(double.NaN) && !weatherHolder.IndoorTemperature.Equals(double.NaN))
            {
                weatherHolder.Temperature = weatherHolder.IndoorTemperature;
            }
            if (weatherHolder.Humidity.Equals(double.NaN) && !weatherHolder.InsideHumidity.Equals(double.NaN))
            {
                weatherHolder.Humidity = weatherHolder.InsideHumidity;
            }
            if (!weatherHolder.Temperature.Equals(double.NaN) ||
               (!weatherHolder.WindSpeed.Equals(double.NaN) && !weatherHolder.WindDirection.Equals(double.NaN)) ||
                !weatherHolder.Pressure.Equals(double.NaN) ||
                  !weatherHolder.Humidity.Equals(double.NaN))
            {
                inPacket.wx_report = weatherHolder;
                return true;
            }
            return false;
        }

    }
}