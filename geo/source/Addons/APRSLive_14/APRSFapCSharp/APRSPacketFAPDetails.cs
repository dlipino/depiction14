﻿using System;
using System.Collections.Generic;

namespace APRSFapCSharp
{
    public class APRSPacketFAPDetails//fap_packet_t;
    {
        #region Variables
        /// Error code. Can be vastly improved to give more info about the apcket
        public ErrorCode fap_error_code;

        /// Packet type.
        public PacketType fap_packet_type;

        /// Exact copy of the original packet, if such thing was given.
        public string orig_packet;
        /// Length of orig_packet.
        public int orig_packet_len;

        /// Header part of the packet.
        public string header;

        /// Body of the packet.
        public string body;

        /// AX.25-level source callsign.
        public string src_callsign;

        /// AX.25-level destination callsign.
        public string dst_callsign;

        /// Array of path elements.
        public Digipeater[] digipeaters;

        /// Number of path elements.
        public int path_len;

        /// Latitude, west is negative.
        public double latitude;

        /// Longitude, south is negative.
        public double longitude;

        /// Position format.
        public PositionFormat fap_pos_format;

        /// Position resolution in meters.
        public double pos_resolution;

        /// Position ambiguity, number of digits.
        public int pos_ambiguity;

        /// Datum character from !DAO! extension. 0x00 = undef.
        public byte dao_datum_byte;

        /// Altitude in meters.//feet
        public double altitude;

        /// Course in degrees, zero is unknown and 360 is north.
        public double course;

        /// Land speed in km/h.//make the mph
        public double speed;

        /// Symbol table designator. 0x00 = undef.
        public char symbol_table = ' ';

        /// Slot of symbol table 0x00 = undef.
        public char symbol_code = ' ';

        /// Zero for no messaging capability, 1 for yes.
        public bool? messaging;

        /// Destination of message.
        public string destination;

        /// The actual message text.
        public string message;

        /// Id of the message which is acked with this packet.
        public string message_ack;

        /// Id of the message which was rejected with this packet.
        public string message_rej;

        /// Id of this message.
        public string message_id;

        /// Station, object or item comment. No null termination.
        public string comment;

        /// Length of comment.
        public uint comment_len;

        /// Name of object or item in packet.
        public string object_or_item_name;

        /// Object or item status. 1 = alive, 0 = killed.
        public bool alive;

        /// Zero if GPS has no fix, one if it has.
        public bool gps_fix_status;

        /// Radio range of the station in km.//miles
        public double radio_range;

        /// TX power, antenna height, antenna gain and possibly beacon rate.
        public string phg;

        /// Timestamp of the packet i believe it is in UTC time
        public DateTime receivedTime; 
        public DateTime sentTime;

        /// NMEA checksum validity indicator, 1 = valid.
        public bool? nmea_checksum_ok = null;

        /// Weather report.
        public APRSFapWeatherInfo wx_report;

        /// Telemetry report.
        public APRSFapTelemetryReport telemetry;

        /// Message bits in case of mic-e packet.
        //	char* messagebits;
        public string messagebits = string.Empty;

        /// Status message. No 0-termination.
        public string status;

        /// Amount of bytes in status message.
        public uint status_len;

        /// Capabilities dictionary
        public Dictionary<string, string> capabilities;
        public string dxSource = null;
        public string dxTimeStamp = null;
        public string dxFreq = null;
        public string dxCall = null;
        public string dxInfo = null;

        #endregion

        #region constructor

        public APRSPacketFAPDetails()
        {
            /* Prepare result object. */
            fap_error_code = ErrorCode.NoError;//->error_code = NULL;
            fap_packet_type = PacketType.Undefined;

            orig_packet = string.Empty;
            orig_packet_len = 0;

            header = string.Empty;
            body = string.Empty;
            src_callsign = string.Empty;
            dst_callsign = string.Empty;
            digipeaters = null;
            path_len = 0;

            latitude = Double.NaN;
            longitude = Double.NaN;
            fap_pos_format = PositionFormat.Undefined;
            pos_resolution = Double.NaN;
            pos_ambiguity = int.MaxValue;
            dao_datum_byte = 0;

            altitude = Double.NaN;
            course = double.NaN;
            speed = Double.NaN;

            symbol_table = ' ';
            symbol_code = ' ';

            messaging = null;
            destination = string.Empty;
            message = string.Empty;
            message_ack = string.Empty;
            message_rej = string.Empty;
            message_id = string.Empty;
            comment = null;
            comment_len = 0;

            object_or_item_name = string.Empty;
            alive = false;

            gps_fix_status = false;
            radio_range = uint.MinValue;
            phg = null;
            receivedTime = DateTime.MinValue;
            sentTime = DateTime.MinValue;
            nmea_checksum_ok = null;

            wx_report = new APRSFapWeatherInfo();
            telemetry = new APRSFapTelemetryReport();

            messagebits = string.Empty;
            status = string.Empty;
            status_len = 0;
            capabilities = null;
        }
        #endregion
    }
}