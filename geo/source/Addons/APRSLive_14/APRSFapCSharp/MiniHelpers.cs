﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    public class MiniHelpers
    {
        #region consts
        public const int MAX_DIGIS = 9;

        /// Nautical miles per hour to kilometers per hour.
        public const double KNOT_TO_KMH = 1.852;

        /// Miles per hour to kilometers per hour.
        public const double MPH_TO_KMH = 1.609344;

        public const double KMH_TO_MPH = 1/MPH_TO_KMH;
        /// Kilometers per hour to meters per second.
        public const double KMH_TO_MS = 10.0 / 36.0;

        /// Miles per hout to meters per second.
        public const double MPH_TO_MS = (MPH_TO_KMH * KMH_TO_MS);

        /// Feets to meters.
        public const double FT_TO_M = 0.3048;


        /// Hundredths of an inch to millimeters
        public const double HINCH_TO_MM = 0.254;
        #endregion
        static public string PerlPackH(string inString)
        {
            //a bit of copy/paste plus google magic
            var raw = new byte[inString.Length / 2];
            for (int i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(inString.Substring(i * 2, 2), 16);
            }
            return Encoding.ASCII.GetString(raw);
        }

        public static double FahrenheitToCelcius(double degreeFahrenheit)
        {
            return ((degreeFahrenheit - 32.0) / 1.8);
        }
        public static double CelciusToFahrenheit(double degreeCelcius)
        {
            return ((degreeCelcius*9)/5) + 32;
        }
        static public string CleanupComment(string input)
        {
            //Dont really what char this kills off
            //                        var rFirst = @"[\x20-\x7e\x80-\xfe]";
            //            var rSecond = @"^\s+";
            //            var rThird = @"\s+$";
            //            var subString = Regex.Replace(input, rFirst, "");          
            //            subString = Regex.Replace(subString, rSecond, "");
            //            subString = Regex.Replace(subString, rThird, "");
            return input.Trim();
            //            $_[0] =~ tr/[\x20-\x7e\x80-\xfe]//cd;
            //	$_[0] =~ s/^\s+//;
            //	$_[0] =~ s/\s+$//;
            //	
            //	return $_[0];
        }

        static public double fapint_get_pos_resolution(int minute_digit_count)
        {
            double retval = KNOT_TO_KMH;
            if (minute_digit_count <= -2)
            {
                retval *= 600;
            }
            else
            {
                retval *= 1000;
            }
            return retval * Math.Pow(10, -1 * minute_digit_count);
        }
        static public string CommentTelemetry(APRSPacketFAPDetails inPacket, string telemIn)
        {
            var rest = telemIn;
            //TODO don't really know if this works at all
            var commentRegex =
                @"^(.*)\|([!-{]{2})([!-{]{2})([!-{]{2}|)([!-{]{2}|)([!-{]{2}|)([!-{]{2}|)([!-{]{2}|)\|(.*)$";
            var groups = Regex.Match(telemIn, commentRegex).Groups;
            if (groups.Count > 9)
            {
                rest = String.Format("{0}{1}", groups[1], groups[9]);
                var seqVal = groups[2].Value;
                var seq = (seqVal.Substring(0, 1)[0] - 33) * 91 + (seqVal.Substring(1, 1)[0] - 33);
                double[] vals = new double[5];
                for (int i = 0; i < vals.Length; i++)
                {
                    seqVal = groups[i + 3].Value;
                    if (String.IsNullOrEmpty(seqVal))
                    {
                        vals[i] = Double.NaN;
                    }
                    else
                    {
                        vals[i] = (seqVal.Substring(0, 1)[0] - 33) * 91 + (seqVal.Substring(1, 1)[0] - 33);
                    }
                }
                inPacket.telemetry.seq = (uint)seq;
                inPacket.telemetry.values = vals;

                //                $rethash->{'telemetry'} = {
                //			'seq' => (ord(substr($2, 0, 1)) - 33) * 91 +
                //				(ord(substr($2, 1, 1)) - 33),
                //			'vals' => [
                //				(ord(substr($3, 0, 1)) - 33) * 91 +
                //				(ord(substr($3, 1, 1)) - 33),
                //				$4 ne '' ? (ord(substr($4, 0, 1)) - 33) * 91 +
                //				(ord(substr($4, 1, 1)) - 33) : undef,
                //				$5 ne '' ? (ord(substr($5, 0, 1)) - 33) * 91 +
                //				(ord(substr($5, 1, 1)) - 33) : undef,
                //				$6 ne '' ? (ord(substr($6, 0, 1)) - 33) * 91 +
                //				(ord(substr($6, 1, 1)) - 33) : undef,
                //				$7 ne '' ? (ord(substr($7, 0, 1)) - 33) * 91 +
                //				(ord(substr($7, 1, 1)) - 33) : undef,
                //			]
                var bitint = 0;
                if (!String.IsNullOrEmpty(groups[8].Value))
                {
                    seqVal = groups[8].Value;
                    bitint = (seqVal.Substring(0, 1)[0] - 33) * 91 + (seqVal.Substring(1, 1)[0] - 33);

                    //                    # bits: first, decode the base-91 integer
                    //			my $bitint = (ord(substr($8, 0, 1)) - 33) * 91 +
                    //				(ord(substr($8, 1, 1)) - 33);
                    //			# then, decode the 8 bits of telemetry
                    //			$rethash->{'telemetry'}->{'bits'} = unpack('B8', pack('C', $bitint));
                }
                var s = Convert.ToString(bitint, 2);
                var bits = s.PadLeft(8, '0').ToCharArray();
                inPacket.telemetry.bits = bits;
            }

            return rest;
        }

        static public string CheckForDAO(APRSPacketFAPDetails inPacket, string packetString)
        {
            //          Check for !DAO!, take the last occurrence (per recommendation)
            ///^(.*)\!([\x21-\x7b][\x20-\x7b]{2})\!(.*?)$/o
            var daoPattern = @"^(.*)\!([\x21-\x7b][\x20-\x7b]{2})\!(.*?)$";
            var rest = packetString;
            var groups = Regex.Match(packetString, daoPattern).Groups;
            if (groups.Count > 3)
            {
                if (fap_dao_parse(inPacket, groups[2].Value))
                {
                    rest = String.Format("{0}{1}", groups[1].Value, groups[3].Value);
                }
            }
            return rest;
        }
        static public bool fap_dao_parse(APRSPacketFAPDetails inPacket, string daoCandidate)
        {
            //^([A-Z])(\d)(\d)$/o
            var doaHumanRedableMatchRegex = @"^([A-Z])(\d)(\d)$";
            //^([a-z])([\x21-\x7b])([\x21-\x7b])$/o
            var doaBase91Regex = @"^([a-z])([\x21-\x7b])([\x21-\x7b])$";
            //^([\x21-\x7b])  $/o
            var doaOnlyDatum = @"^([\x21-\x7b])  $";
            double latOff, longOff;
            var groups = Regex.Match(daoCandidate, doaHumanRedableMatchRegex).Groups;
            if (groups.Count > 3)
            {
                inPacket.dao_datum_byte = (byte)(groups[1].Value[0]);
                inPacket.pos_resolution = fapint_get_pos_resolution(3);

                Double.TryParse(groups[2].Value, out latOff);
                Double.TryParse(groups[3].Value, out longOff);
                latOff = latOff * 0.001 / 60.0;
                longOff = longOff * 0.001 / 60.0;
            }
            else if ((groups = Regex.Match(daoCandidate, doaBase91Regex).Groups).Count > 3)
            {
                inPacket.dao_datum_byte = (byte)(groups[1].Value.ToUpperInvariant()[0]);
                inPacket.pos_resolution = fapint_get_pos_resolution(4);
                var latI = groups[2].Value[0] - 33;
                var longI = groups[3].Value[0] - 33;
                latOff = ((double)latI) / 91 * 0.01 / 60;
                longOff = ((double)longI) / 91 * 0.01 / 60;
            }
            else if ((groups = Regex.Match(daoCandidate, doaOnlyDatum).Groups).Count > 3)
            {
                inPacket.dao_datum_byte = (byte)(groups[0].Value.ToUpperInvariant()[0]);
                return true;
            }
            else { return false; }

            // check N/S and E/W
            if (inPacket.latitude < 0)
            {
                inPacket.latitude -= latOff;
            }
            else
            {
                inPacket.latitude += latOff;
            }
            if (inPacket.longitude < 0)
            {
                inPacket.longitude -= longOff;
            }
            else
            {
                inPacket.longitude += longOff;
            }
            return true;
        }

        static public string fap_check_ax25_call(string input)
        {
            GroupCollection groups;
            string call = null;
            string ssid_str = String.Empty;

            /* Check input. */
            if (String.IsNullOrEmpty(input))
            {
                return null;
            }

            /* Validate callsign. */
            groups = Regex.Match(input, APRSRegexConstants.fapint_regex_ax25call).Groups;
            if (groups.Count > 1)
            {
                /* Get callsign. */
                call = groups[1].Value;
                //Call sign cannot be more than 6 characters
                if (call.Length > 6) return null;
                /* Get SSID. */
                if (groups.Count > 2)
                {
                    ssid_str = groups[2].Value;

                    /* If we got ssid, check that it is valid. */
                    if (!String.IsNullOrEmpty(ssid_str))
                    {
                        int ssid;
                        if (!Int32.TryParse(ssid_str, out ssid)) return null;
                        ssid = 0 - ssid;
                        if (ssid > 15)
                        {
                            return null;
                        }
                        call = String.Format("{0}-{1}", call, ssid);
                    }
                }
            }
            /* We're done. */
            return call;
        }

        public static bool fapint_parse_header(APRSPacketFAPDetails packet, bool is_ax25)
        {
            bool retval = true;
            string rest = String.Empty;
            var srcCallSign = String.Empty;
            GroupCollection groups;
            var cleanHeader =string.Empty;
            //Do a clean of some strange <> stuff
            //@"^[\s ](\<.*\>[\s]*?:)(.*)$"
            cleanHeader = Regex.Replace(packet.header, @"(\<.*\>)", "").Trim();
            groups = Regex.Match(packet.header, @"(^.*)(\<.*\>)$").Groups;
//            if (groups.Count > 1 && groups[2].Success)
//                cleanHeader = groups[2].Value;
//            if (cleanHeader.Length < 0)
//            {
//                cleanHeader = packet.header;
//            }

            // Separate source callsign and the rest. 
            groups = Regex.Match(cleanHeader, APRSRegexConstants.fapint_regex_header, RegexOptions.IgnoreCase).Groups;
            if (groups.Count > 1)
            {
                if (groups.Count > 2)
                {
                    rest = groups[2].Value;
                }

                srcCallSign = groups[1].Value;
                /* Save (and validate if we're AX.25) the callsign. */
                if (is_ax25)
                {
                    packet.src_callsign = fap_check_ax25_call(srcCallSign.ToUpperInvariant());
                    if (String.IsNullOrEmpty(packet.src_callsign))
                    {
                        packet.fap_error_code = ErrorCode.fapSRCCALL_NOAX25;
                        retval = false;
                    }
                }
                else
                {
                    packet.src_callsign = srcCallSign;
                }
            }
            else
            {
                packet.fap_error_code = ErrorCode.fapSRCCALL_BADCHARS;
                retval = false;
            }
            if (!retval)
            {
                return false;
            }

            //now get deistination callsign and digipeaters from the remainder of the 
            //header ie the rest.
            var pathComponents = rest.Split(',');
            //            More than 9 (dst callsign + 8 digipeaters) path components
            //	 from AX.25 or less than 1 from anywhere is invalid.
            if (is_ax25 && pathComponents.Length > MAX_DIGIS)
            {
                packet.fap_error_code = ErrorCode.fapDSTPATH_TOOMANY;
                return false;
            }

            if (pathComponents.Length < 1)
            {
                packet.fap_error_code = ErrorCode.fapDSTCALL_NONE;
                return false;
            }

            /* Validate dst call. We are strict here, there should be no need to use
 * a non-AX.25 compatible destination callsigns in the APRS-IS. */
            var dstCallSign = pathComponents[0];
            packet.dst_callsign = fap_check_ax25_call(dstCallSign);
            if (String.IsNullOrEmpty(packet.dst_callsign))
            {
                packet.fap_error_code = ErrorCode.fapDSTCALL_NOAX25;
                return false;
            }
            //Get digipeaters
            //shift contents
            var length = pathComponents.Length - 1;
            var nonDestinationComponents = new string[0];
            if (length > 0)
            {
                nonDestinationComponents = new string[length];
                for (int i = 0; i < length; i++)
                {
                    nonDestinationComponents[i] = pathComponents[i + 1];
                }
                packet.digipeaters = new Digipeater[length];
            }

            if (is_ax25)
            {
                for (int i = 0; i < nonDestinationComponents.Length; i++)
                {
                    var digipeater = nonDestinationComponents[i];
                    if ((groups = Regex.Match(digipeater, APRSRegexConstants.fapint_regex_digiax25call).Groups).Count > 2)
                    {
                        var digitested = fap_check_ax25_call(groups[1].Value.ToUpperInvariant());
                        if (String.IsNullOrEmpty(digitested))
                        {
                            packet.fap_error_code = ErrorCode.fapDIGICALL_NOAX25;
                            return false;
                        }
                        var wasDigied = false;
                        if (groups.Count > 3)
                        {
                            if (groups[2].Value.Equals("*"))
                            {
                                wasDigied = true;
                            }
                        }
                        packet.digipeaters[i] = new Digipeater { call = digitested, wasdigied = wasDigied };
                    }
                    else
                    {
                        packet.fap_error_code = ErrorCode.fapDIGICALL_BADCHARS;
                        return false;
                    }
                }
            }
            else
            {
                bool seen_qconstr = false;
                for (int i = 0; i < nonDestinationComponents.Length; i++)
                {
                    var digipeater = nonDestinationComponents[i];
                    if ((groups = Regex.Match(digipeater, APRSRegexConstants.fapint_regex_digicall).Groups).Count > 1)
                    {
                        var wasDigied = false;
                        var digiOut = groups[1].Value;
                        if (groups.Count > 2)
                        {
                            if (groups[2].Value.Contains("*"))
                            {
                                wasDigied = true;
                            }
                        }
                        packet.digipeaters[i] = new Digipeater { call = digiOut, wasdigied = wasDigied };
                        if (Regex.IsMatch(digiOut, APRSRegexConstants.fapint_regex_digiqconstruct))
                        {
                            seen_qconstr = true;
                        }
                    }
                    else
                    {
                        if (seen_qconstr &&
                            (groups = Regex.Match(digipeater, APRSRegexConstants.fapint_regex_digicallv6).Groups).Count > 1)
                        {
                            packet.digipeaters[i] = new Digipeater { call = groups[1].Value };
                        }
                        else
                        {
                            packet.fap_error_code = ErrorCode.fapDIGICALL_BADCHARS;
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        static public void comments_to_decimal(APRSPacketFAPDetails inPacket, string bodyPart)
        {
            GroupCollection groups;
            var subBody = bodyPart;
            //^([0-9. ]{3})\/([0-9. ]{3})/o
            var firstPattern = @"^([0-9. ]{3})\/([0-9. ]{3})";
            //^PHG(\d[\x30-\x7e]\d\d[0-9A-Z])\//o
            var secondPattern = @"^(PHG)(\d[\x30-\x7e]\d\d[0-9A-Z])\/";
            //            ^PHG(\d[\x30-\x7e]\d\d)/o
            var thirdPattern = @"^(PHG)(\d[\x30-\x7e]\d\d)";
            //^RNG(\d{4})/o
            var fourthPattern = @"^(RNG)(\d{4})";
            // First check the possible APRS data extension,immediately following the packet
            if (subBody.Length >= 7)
            {
                groups = Regex.Match(subBody, firstPattern).Groups;
                var digitPattern = @"\d{3}$";
                if (groups.Count > 2)
                {
                    var courseS = groups[1].Value;
                    double course;
                    Double.TryParse(courseS, out course);
                    if (Regex.IsMatch(courseS, digitPattern) && course <= 360
                        && course >= 1)
                    {
                        inPacket.course = course;
                    }
                    else { inPacket.course = 0; }

                    var speedS = groups[2].Value;
                    double speed;
                    Double.TryParse(speedS, out speed);
                    if (Regex.IsMatch(speedS, digitPattern))
                    {
                        inPacket.speed = speed *KNOT_TO_KMH*KMH_TO_MPH;
                    }
                    else { inPacket.speed = Double.NaN; }
                    subBody = subBody.Substring(7);
                }
                else if ((groups = Regex.Match(subBody, secondPattern).Groups).Count > 2)
                {
                    // PHGR
                    inPacket.phg = groups[2].Value;
                    subBody = subBody.Substring(8);
                }
                else if ((groups = Regex.Match(subBody, thirdPattern).Groups).Count > 2)
                {
                    //  don't do anything fancy with PHG, just store it
                    inPacket.phg = groups[2].Value;
                    subBody = subBody.Substring(7);

                }
                else if ((groups = Regex.Match(subBody, fourthPattern).Groups).Count > 2)
                {
                    // radio range, in miles, so convert to km, ok maybe not
                    double range;
                    Double.TryParse(groups[1].Value, out range);
                    inPacket.radio_range = range;// *MPH_TO_KMH;
                    subBody = subBody.Substring(7);
                }
            }
            //Check for optional altitude anywhere in the comment, take the first occurrence

            // ^(.*?)\/A=(-\d{5}|\d{6})(.*)$/o
            var altitudePattern = @"^(.*?)\/A=(-\d{5}|\d{6})(.*)$";
            if ((groups = Regex.Match(subBody, altitudePattern).Groups).Count > 3)
            {
                //                convert to meters as well and no, keep it in feet
                double altitude;
                Double.TryParse(groups[2].Value, out altitude);
                inPacket.altitude = altitude;// *0.3048;
                subBody = groups[1].Value + groups[3].Value;
            }
            subBody = CheckForDAO(inPacket, subBody);
            //  Check for new-style base-91 comment telemetry
            subBody = CommentTelemetry(inPacket, subBody);
            //            # Strip a / or a ' ' from the beginning of a comment
            //	# (delimiter after PHG or other data stuffed within the comment)
            // s/^[\/\s]//;
            subBody = Regex.Replace(subBody, @"^[\/\s]", "");
            if (subBody.Length > 0)
            {
                inPacket.comment = CleanupComment(subBody);
            }
            //           # Always succeed as these are optional
        }

        static public Tuple<string, string> get_sympbol_fromdst(string dstCallSign)
        {
            GroupCollection groups;
            //^(GPS|SPC)([A-Z0-9]{2,3})/o
            var dstCallSignPattern = @"^(GPS|SPC)([A-Z0-9]{2,3})";
            if ((groups = Regex.Match(dstCallSign, dstCallSignPattern).Groups).Count > 2)
            {
                var leftOverString = groups[2].Value; //This might not be right
                var type = leftOverString.Substring(0, 1);
                var leftOverLength = leftOverString.Length;
                if (leftOverLength == 3)
                {
                    if (type.Equals("C") || type.Equals("E"))
                    {
                        var numberidS = leftOverString.Substring(1, 2);
                        double numberid;
                        if (Double.TryParse(numberidS, out numberid) &&
                            numberid > 0 && numberid < 95)
                        {
                            var code = groups[1].Value[0] + 32;
                            var table = "\\";
                            if (type.Equals("C"))
                            {
                                table = "/";
                            }
                            return new Tuple<string, string>(code.ToString(), table);
                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        //                # secondary symbol table, with overlay
                        //				# Check first that we really are in the
                        //				# secondary symbol table
                        var dstType = leftOverString.Substring(0, 2);
                        var overlay = leftOverString.Substring(2, 1);
                        if (type.Equals("O") ||
                            type.Equals("A") ||
                            type.Equals("N") ||
                            type.Equals("D") ||
                            type.Equals("S") ||
                            type.Equals("Q") && Regex.IsMatch(overlay, "^[A-Z0-9]$"))
                        {
                            //$dstsymbol{$dsttype}//what the heck is that?
                            if (dstType != null)
                            {
                                var code = dstType.Substring(1, 1);
                                return new Tuple<string, string>(overlay, code);
                            }
                            else { return null; }
                        }
                        else { return null; }
                    }
                }
                else
                {
                    //                    # primary or secondary symbol table, no overlay
                    //                    if (defined($dstsymbol{$leftoverstring})) {
                    if (leftOverString != null)
                    {
                        var table = leftOverString.Substring(0, 1);
                        var code = leftOverString.Substring(1, 1);
                        return new Tuple<string, string>(table, code);
                    }
                    else { return null; }
                }
            }
            else { return null; }
            return null;
        }
    }
}