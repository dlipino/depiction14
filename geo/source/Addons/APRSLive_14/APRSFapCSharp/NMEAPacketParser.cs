﻿using System;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    public class NMEAPacketParser
    {
        //http://aprs.gids.nl/nmea/#gga
        static public bool nmea_to_decimal(APRSPacketFAPDetails inPacket, string bodyPart, bool useRawTime)
        {
            // verify checksum first, if it is provided
            GroupCollection groups;
            var bodyForNMEA = Regex.Replace(bodyPart, @"\s+$", "");//remove possible white space from the end
            //^([\x20-\x7e]+)\*([0-9A-F]{2})$/io
            var checkSumPattern = @"^([\x20-\x7e]+)\*([0-9A-F]{2})$";
            if ((groups = Regex.Match(bodyForNMEA, checkSumPattern, RegexOptions.IgnoreCase).Groups).Count > 2)
            {
                var checksumArea = groups[1].Value;
                var checksumGivenS = groups[2].Value;
                var checkSumGiven = Convert.ToInt32(checksumGivenS, 16);
                var checkSumCalculated = 0;
                for (int i = 0; i < checksumArea.Length; i++)
                {
                    checkSumCalculated ^= checksumArea.Substring(i, 1)[0];
                }
                if (checkSumGiven != checkSumCalculated)
                {
                    inPacket.fap_error_code = ErrorCode.fapNMEA_INV_CKSUM;
                    return false;
                }
                inPacket.nmea_checksum_ok = true;
                inPacket.fap_pos_format = PositionFormat.fapPOS_NMEA;
                var tableAndCode = MiniHelpers.get_sympbol_fromdst(inPacket.dst_callsign);
                if (tableAndCode == null)
                {
                    inPacket.symbol_table = '/';
                    inPacket.symbol_code = '/';
                }
                else
                {
                    inPacket.symbol_table = tableAndCode.Item1[0];
                    inPacket.symbol_code = tableAndCode.Item2[0];
                }
                //Split to NMEA files
                //s/\*[0-9A-F]{2}$//;
                var removeCheckSumPatter = @"\*[0-9A-F]{2}$";
                bodyForNMEA = Regex.Replace(bodyForNMEA, removeCheckSumPatter, "");
                var nmeaFields = bodyForNMEA.Split(new[] { ',' });
                if (nmeaFields.Length < 1)
                {
                    inPacket.fap_error_code = ErrorCode.fapGPRMC_FEWFIELDS;
                    return false;
                }

                if (nmeaFields[0].Equals("GPRMC"))
                {
                    if (nmeaFields.Length < 10)
                    {
                        inPacket.fap_error_code = ErrorCode.fapGPRMC_FEWFIELDS;
                        return false;
                    }
                    if (!nmeaFields[2].Equals("A"))
                    {
                        inPacket.fap_error_code = ErrorCode.fapGPRMC_NOFIX;
                        return false;
                    }
                    //Check and save time stamp
                    int hour, minute, second;
                    //^\s*(\d{2})(\d{2})(\d{2})(|\.\d+)\s*$/o
                    var timeStampPattern = @"^\s*(\d{2})(\d{2})(\d{2})(|\.\d+)\s*$";
                    if ((groups = Regex.Match(nmeaFields[1], timeStampPattern).Groups).Count > 3)
                    {
                        int.TryParse(groups[1].Value, out hour);
                        int.TryParse(groups[2].Value, out minute);
                        int.TryParse(groups[3].Value, out second);
                        if (hour > 23 || minute > 59 || second > 59)
                        {
                            inPacket.fap_error_code = ErrorCode.fapGPRMC_INV_TIME;
                            return false;
                        }
                    }
                    else
                    {
                        inPacket.fap_error_code = ErrorCode.fapGPRMC_INV_TIME;
                        return false;
                    }

                    int year, month, day;
                    //^\s*(\d{2})(\d{2})(\d{2})\s*$/o
                    var datePattern = @"^\s*(\d{2})(\d{2})(\d{2})\s*$";
                    if ((groups = Regex.Match(nmeaFields[9], datePattern).Groups).Count > 3)
                    {
                        int.TryParse(groups[1].Value, out day);
                        int.TryParse(groups[2].Value, out month);
                        int.TryParse(groups[3].Value, out year);
                        if (year >= 70) { year += 1900; }
                        else { year += 2000; }

                        //See if making the year blows up
                        try
                        {
                            var date = new DateTime(year, month, day);
                        }
                        catch
                        {
                            inPacket.fap_error_code = ErrorCode.fapGPRMC_INV_DATE;
                            return false;
                        }
                    }
                    else
                    {
                        inPacket.fap_error_code = ErrorCode.fapGPRMC_INV_DATE;
                        return false;
                    }
                    //Legacy comments
                    //       Date_to_Time() can only handle 32-bit unix timestamps,
                    //		 so make sure it is not used for those years that
                    //		 are outside that range.
                    if (year >= 2038 | year < 1970)
                    {
                        inPacket.sentTime = DateTime.MinValue;
                        inPacket.fap_error_code = ErrorCode.fapGPRMC_DATE_OUT;
                        return false;
                    }
                    else
                    {
                        inPacket.sentTime = new DateTime(year, month, day, hour, minute, second, DateTimeKind.Utc);
                    }
                    //    speed (knots) and course, make these optional
                    //	 in the parsing sense (don't fail if speed/course
                    //	 can't be decoded).
                    //^\s*(\d+(|\.\d+))\s*$/o
                    double speed;
                    var nmeaSpeedCoursePattern = @"^\s*(\d+(|\.\d+))\s*$";
                    if ((groups = Regex.Match(nmeaFields[7], nmeaSpeedCoursePattern).Groups).Count > 1)
                    {
                        double.TryParse(groups[1].Value, out speed);
                        inPacket.speed = speed * MiniHelpers.KNOT_TO_KMH*MiniHelpers.KMH_TO_MPH;
                    }
                    double course;
                    if ((groups = Regex.Match(nmeaFields[8], nmeaSpeedCoursePattern).Groups).Count > 1)
                    {
                        double.TryParse(groups[1].Value, out course);
                        // round to nearest integer
                        course += .5;
                        if (Math.Floor(course) == 0)
                        {
                            course = 360;
                        }
                        else if (course > 360)
                        {
                            course = 0;//invalid
                        }
                        inPacket.course = course;
                    }
                    else
                    {
                        inPacket.course = 0;
                    }
                    //latitude longitude values
                    var latitude = nmea_getLatLon(inPacket, nmeaFields[3], nmeaFields[4]);
                    if(latitude.Equals(double.NaN))
                    {
                        return false;
                    }
                    inPacket.latitude = latitude;
                    var longitude = nmea_getLatLon(inPacket, nmeaFields[5], nmeaFields[6]);
                    if (longitude.Equals(double.NaN))
                    {
                        return false;
                    }
                    inPacket.longitude = longitude;
                    return true;
                }
                else if (nmeaFields[1].Equals("GPGGA"))
                {
                    if (nmeaFields.Length < 11)
                    {
                        inPacket.fap_error_code = ErrorCode.fapGPGGA_FEWFIELDS;
                        return false;
                    }
                    //check for position validity
                    if (!Regex.IsMatch(nmeaFields[6], @"^\s*(\d+)\s*$"))
                    {
                        inPacket.fap_error_code = ErrorCode.fapGPGGA_NOFIX;
                        return false;
                    }

                    //   Use the APRS time parsing routines to check
                    //	 the time and convert it to timestamp.
                    //	 But before that, remove a possible decimal part
                    //s/\.\d+$//;
                    var parsedTime = Regex.Replace(nmeaFields[1], @"\.\d+$", "");
                    var dateTime = APRSPacketParserHelpers.ParseTimeStamp(parsedTime, useRawTime);
                    if (dateTime.Equals(DateTime.MinValue))
                    {
                        inPacket.fap_error_code = ErrorCode.fapTIMESTAMP_INV_GPGGA;
                        return false;
                    }
                    inPacket.sentTime = dateTime;
                    //latitude longitude values
                    var latitude = nmea_getLatLon(inPacket, nmeaFields[2], nmeaFields[3]);
                    if (latitude.Equals(double.NaN))
                    {
                        return false;
                    }
                    inPacket.latitude = latitude;
                    var longitude = nmea_getLatLon(inPacket, nmeaFields[4], nmeaFields[5]);
                    if (longitude.Equals(double.NaN))
                    {
                        return false;
                    }
                    inPacket.longitude = longitude;
                    //^(-?\d+(|\.\d+))$/o
                    var altitudePattern = @"^(-?\d+(|\.\d+))$";
                    if(nmeaFields[10].Equals("M") && 
                        (groups=Regex.Match(nmeaFields[9],altitudePattern).Groups).Count>1)
                    {
                        double altitude;
                        double.TryParse(groups[1].Value, out altitude);
                        //Comes in as meters above mean sea level
                        inPacket.altitude = altitude / MiniHelpers.FT_TO_M;
                    }
                    return true;
                }else if(nmeaFields[0].Equals("GPGLL"))
                {
                    if(nmeaFields.Length<5)
                    {
                        inPacket.fap_error_code=ErrorCode.fapGPGLL_FEWFIELDS;
                        return false;
                    }
                    var latitude = nmea_getLatLon(inPacket, nmeaFields[1], nmeaFields[2]);
                    if (latitude.Equals(double.NaN))
                    {
                        return false;
                    }
                    inPacket.latitude = latitude;
                    var longitude = nmea_getLatLon(inPacket, nmeaFields[3], nmeaFields[4]);
                    if (longitude.Equals(double.NaN))
                    {
                        return false;
                    }
                    inPacket.longitude = longitude;
// Use the APRS time parsing routines to check
//	the time and convert it to timestamp.
// But before that, remove a possible decimal part
                    if(nmeaFields.Length>=6)
                    {
                        var parsedTime = Regex.Replace(nmeaFields[5], @"\.\d+$", "");
                        var dateTime = APRSPacketParserHelpers.ParseTimeStamp(parsedTime, useRawTime);
                        if (dateTime.Equals(DateTime.MinValue))
                        {
                            inPacket.fap_error_code = ErrorCode.fapTIMESTAMP_INV_GPGLL;
                            return false;
                        }
                        inPacket.sentTime = dateTime;
                    }
                    if(nmeaFields.Length>=7)
                    {
                        //GPS fix validity supplied
                        if(!nmeaFields[6].Equals("A"))
                        {
                            inPacket.fap_error_code = ErrorCode.fapGPGLL_NOFIX;
                            return false;
                        }
                    }
                }
    //} elsif ($nmeafields[0] eq 'GPVTG') {
	//} elsif ($nmeafields[0] eq 'GPWPT') {
                else
                {
                    inPacket.fap_error_code=ErrorCode.fapNMEA_UNSUPP;
                    return false;
                }
            }
            return true;
        }
        
        static public double nmea_getLatLon(APRSPacketFAPDetails inPacket,string stringToDecode, string singToDecode)
        {
            GroupCollection groups;
            var sign = singToDecode.ToUpperInvariant();
            double convertedValue=double.NaN;
//      Be leninent on what to accept, anything
// goes as long as degrees has 1-3 digits,
//	 minutes has 2 digits and there is at least
//	 one decimal minute.
            //^\s*(\d{1,3})([0-5][0-9])\.(\d+)\s*$/o
            var minDegSecPattern = @"^\s*(\d{1,3})([0-5][0-9])\.(\d+)\s*$";
            if((groups=Regex.Match(stringToDecode,minDegSecPattern).Groups).Count>3)
            {
                var minutesS = groups[2] + "." + groups[3];
                double minutes;
                double.TryParse(minutesS, out minutes);
                double degrees;
                double.TryParse(groups[1].Value, out degrees);
                convertedValue = degrees + (minutes/60.0);
                inPacket.pos_resolution = MiniHelpers.fapint_get_pos_resolution(groups[3].Value.Length);
            }else
            {
                inPacket.fap_error_code = ErrorCode.fapNMEA_INV_CVAL;
                return convertedValue;
            }
            //^\s*[EW]\s*$/o
            if (Regex.IsMatch(sign, @"^\s*[EW]\s*$"))
            {
                if(convertedValue>179.999999)
                {
                    inPacket.fap_error_code = ErrorCode.fapNMEA_LARGE_EW;
                    return double.NaN;
                }
                //west negative //^\s*W\s*$/o
                if(Regex.IsMatch(sign, @"^\s*W\s*$"))
                {
                    convertedValue *= -1;
                }
            }
            //^\s*[NS]\s*$/o
            else if (Regex.IsMatch(sign, @"^\s*[NS]\s*$"))
            {
                if (convertedValue > 89.999999)
                {
                    inPacket.fap_error_code = ErrorCode.fapNMEA_LARGE_NS;
                    return double.NaN;
                }
                //south negative //^\s*S\s*$/o
                if (Regex.IsMatch(sign, @"^\s*S\s*$"))
                {
                    convertedValue *= -1;
                }
            }else
            {
                inPacket.fap_error_code=ErrorCode.fapNMEA_INV_SIGN;
                return double.NaN;
            }

            return convertedValue;
        }
    }
}