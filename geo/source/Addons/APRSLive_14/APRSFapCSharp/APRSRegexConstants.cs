﻿namespace APRSFapCSharp
{
    public class APRSRegexConstants
    {
        //^([A-Z0-9-]{1,9})>(.*)$/io
        //"^([A-Z0-9\\-]{1,9})>(.*)$";
        public const string fapint_regex_header = "^([A-Z0-9\\-]{1,9})>(.*)$";
        //^([A-Z0-9]{1,6})(-\d{1,2}|)$/o
        public const string fapint_regex_ax25call = @"^([A-Z0-9]{1,6})(-\d{1,2}|)$";
        //^([A-Z0-9-]+)(\*|)$/io
        public const string fapint_regex_digiax25call = @"^([A-Z0-9-]+)(\*)$";
        //^([A-Z0-9a-z-]{1,9})(\*|)$/o
        public const string fapint_regex_digicall = @"^([A-Z0-9a-z-]{1,9})(\*+|)$";
        //^q..$/
        public const string fapint_regex_digiqconstruct = @"^q..$";
        //^([0-9A-F]{32})$/
        public const string fapint_regex_digicallv6 = "^([0-9A-F]{32})$";
        //^(\d{2})(\d{2})(\d{2})(z|h|\/)$/o
        public const string fapint_regex_timestamp = @"/^(\d{2})(\d{2})(\d{2})(>z|h|\/)$";
        //^[0-9A-LP-Z]{3}[0-9LP-Z]{3}$/io    
        public const string fapint_regex_mice_dstcall = "^[0-9A-LP-Z]{3}[0-9LP-Z]{3}$";
        //^(\d+)(_*)$/io
        public const string fapint_regex_mice_amb = "^([0-9]+)(_*)$";
        //^(\d{2})([0-7 ][0-9 ]\.[0-9 ]{2})([NnSs])(.)(\d{3})([0-7 ][0-9 ]\.[0-9 ]{2})([EeWw])([\x21-\x7b\x7d])/o
        // from c port "^([0-9]{2})([0-7 ][0-9 ]\\.[0-9 ]{2})([NnSs])(.)([0-9]{3})([0-7 ][0-9 ]\\.[0-9 ]{2})([EeWw])(.)";
         public const string fapint_regex_normalpos = @"^(\d{2})([0-7 ][0-9 ]\.[0-9 ]{2})([NnSs])(.)(\d{3})([0-7 ][0-9 ]\.[0-9 ]{2})([EeWw])([\x21-\x7b\x7d])";
        //		regcomp(&fapint_regex_normalamb, "^([0-9]{0,4})( {0,4})$", REG_EXTENDED);
        public const string fapint_regex_normalamb = "^([0-9]{0,4})( {0,4})$";

        //		regcomp(&fapint_regex_mice_body, "^[\\/\\\\A-Z0-9]", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_mice_body = "^[\\/\\\\A-Z0-9]";

//		
        //		regcomp(&fapint_regex_comment, "^([0-9\\. ]{3})\\/([0-9\\. ]{3})", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_comment = "^([0-9\\. ]{3})\\/([0-9\\. ]{3})";
        //		regcomp(&fapint_regex_phgr, "^PHG([0-9].[0-9]{2}[1-9A-Z])\\/", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_phgr = "^PHG([0-9].[0-9]{2}[1-9A-Z])\\/";
        //		regcomp(&fapint_regex_phg, "^PHG([0-9].[0-9]{2})", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_phg = "^PHG([0-9].[0-9]{2})";
        //		regcomp(&fapint_regex_rng, "^RNG([0-9]{4})", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_rng = "^RNG([0-9]{4})";
        //		regcomp(&fapint_regex_altitude, "\\/A=(-[0-9]{5}|[0-9]{6})", REG_EXTENDED);
        public const string fapint_regex_altitude = "\\/A=(-[0-9]{5}|[0-9]{6})";
//		
        //		regcomp(&fapint_regex_mes_dst, "^:([A-Za-z0-9_ -]{9}):", REG_EXTENDED);
        public const string fapint_regex_mes_dst = "^:([A-Za-z0-9_ -]{9}):";
        //		regcomp(&fapint_regex_mes_ack, "^ack([A-Za-z0-9}]{1,5}) *$", REG_EXTENDED);
        public const string fapint_regex_mes_ack = "^ack([A-Za-z0-9}]{1,5}) *$";
        //		regcomp(&fapint_regex_mes_nack, "^rej([A-Za-z0-9}]{1,5}) *$", REG_EXTENDED);
        public const string fapint_regex_mes_nack = "^rej([A-Za-z0-9}]{1,5}) *$";
//
        //		regcomp(&fapint_regex_wx1, "^_{0,1}([0-9 \\.\\-]{3})\\/([0-9 \\.]{3})g([0-9 \\.]+)t(-{0,1}[0-9 \\.]+)", REG_EXTENDED);
        public const string fapint_regex_wx1 = "^_{0,1}([0-9 \\.\\-]{3})\\/([0-9 \\.]{3})g([0-9 \\.]+)t(-{0,1}[0-9 \\.]+)";
        //		regcomp(&fapint_regex_wx2, "^_{0,1}c([0-9 \\.\\-]{3})s([0-9 \\.]{3})g([0-9 \\.]+)t(-{0,1}[0-9 \\.]+)", REG_EXTENDED);
        public const string fapint_regex_wx2 = "^_{0,1}c([0-9 \\.\\-]{3})s([0-9 \\.]{3})g([0-9 \\.]+)t(-{0,1}[0-9 \\.]+)";
        //		regcomp(&fapint_regex_wx3, "^_{0,1}([0-9 \\.\\-]{3})\\/([0-9 \\.]{3})t(-{0,1}[0-9 \\.]+)", REG_EXTENDED);
        public const string fapint_regex_wx3 = "^_{0,1}([0-9 \\.\\-]{3})\\/([0-9 \\.]{3})t(-{0,1}[0-9 \\.]+)";
        //		regcomp(&fapint_regex_wx4, "^_{0,1}([0-9 \\.\\-]{3})\\/([0-9 \\.]{3})g([0-9 \\.]+)", REG_EXTENDED);
        public const string fapint_regex_wx4 = "^_{0,1}([0-9 \\.\\-]{3})\\/([0-9 \\.]{3})g([0-9 \\.]+)";
        //		regcomp(&fapint_regex_wx5, "^g([0-9]+)t(-?[0-9 \\.]{1,3})", REG_EXTENDED);
        public const string fapint_regex_wx5 = "^g([0-9]+)t(-?[0-9 \\.]{1,3})";
// 
        //		regcomp(&fapint_regex_wx_r1, "r([0-9]{1,3})", REG_EXTENDED);
        public const string fapint_regex_wx_r1 = "r([0-9]{1,3})";
        //		regcomp(&fapint_regex_wx_r24, "p([0-9]{1,3})", REG_EXTENDED);
        public const string fapint_regex_wx_r24 = "p([0-9]{1,3})";
        //		regcomp(&fapint_regex_wx_rami, "P([0-9]{1,3})", REG_EXTENDED);
        public const string fapint_regex_wx_rami = "P([0-9]{1,3})";
//
        //		regcomp(&fapint_regex_wx_humi, "h([0-9]{1,3})", REG_EXTENDED);
        public const string fapint_regex_wx_humi = "h([0-9]{1,3})";
        //		regcomp(&fapint_regex_wx_pres, "b([0-9]{4,5})", REG_EXTENDED);
        public const string fapint_regex_wx_pres = "b([0-9]{4,5})";
        //		regcomp(&fapint_regex_wx_lumi, "([lL])([0-9]{1,3})", REG_EXTENDED);
        public const string fapint_regex_wx_lumi = "([lL])([0-9]{1,3})";
        //		regcomp(&fapint_regex_wx_what, "v([\\-\\+]{0,1}[0-9]+)", REG_EXTENDED);
        public const string fapint_regex_wx_what = "v([\\-\\+]{0,1}[0-9]+)";
//		
        //		regcomp(&fapint_regex_wx_snow, "s([0-9]+)", REG_EXTENDED);
        public const string fapint_regex_wx_snow = "s([0-9]+)";
        //		regcomp(&fapint_regex_wx_rrc, "#([0-9]+)", REG_EXTENDED);
        public const string fapint_regex_wx_rrc = "#([0-9]+)";
        //		regcomp(&fapint_regex_wx_any, "^([rPphblLs#][\\. ]{1,5})+", REG_EXTENDED);
        public const string fapint_regex_wx_any = "^([rPphblLs#][\\. ]{1,5})+";
        //		regcomp(&fapint_regex_wx_soft, "^[a-zA-Z0-9\\-\\_]{3,5}$", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_wx_soft = "^[a-zA-Z0-9\\-\\_]{3,5}$";
//		
        //		regcomp(&fapint_regex_nmea_chksum, "^(.+)\\*([0-9A-F]{2})$", REG_EXTENDED);
        public const string fapint_regex_nmea_chksum = "^(.+)\\*([0-9A-F]{2})$";
        //		regcomp(&fapint_regex_nmea_dst, "^(GPS|SPC)([A-Z0-9]{2,3})", REG_EXTENDED);
        public const string fapint_regex_nmea_dst = "^(GPS|SPC)([A-Z0-9]{2,3})";
        //		regcomp(&fapint_regex_nmea_time, "^[:space:]*([0-9]{2})([0-9]{2})([0-9]{2})(()|\\.[0-9]+)[:space:]*$", REG_EXTENDED);
        public const string fapint_regex_nmea_time = "^[:space:]*([0-9]{2})([0-9]{2})([0-9]{2})(()|\\.[0-9]+)[:space:]*$";
        //		regcomp(&fapint_regex_nmea_date, "^[:space:]*([0-9]{2})([0-9]{2})([0-9]{2})[:space:]*$", REG_EXTENDED);
        public const string fapint_regex_nmea_date = "^[:space:]*([0-9]{2})([0-9]{2})([0-9]{2})[:space:]*$";
//
        //		regcomp(&fapint_regex_nmea_specou, "^[:space:]*([0-9]+(()|\\.[0-9]+))[:space:]*$", REG_EXTENDED);
        public const string fapint_regex_nmea_specou = "^[:space:]*([0-9]+(()|\\.[0-9]+))[:space:]*$";
        //		regcomp(&fapint_regex_nmea_fix, "^[:space:]*([0-9]+)[:space:]*$", REG_EXTENDED);
        public const string fapint_regex_nmea_fix = "^[:space:]*([0-9]+)[:space:]*$";
        //		regcomp(&fapint_regex_nmea_altitude, "^(-?[0-9]+(()|\\.[0-9]+))$", REG_EXTENDED);
        public const string fapint_regex_nmea_altitude = "^(-?[0-9]+(()|\\.[0-9]+))$";
        //		regcomp(&fapint_regex_nmea_flag, "^[:space:]*([NSEWnsew])[:space:]*$", REG_EXTENDED);
        public const string fapint_regex_nmea_flag = "^[:space:]*([NSEWnsew])[:space:]*$";
        //		regcomp(&fapint_regex_nmea_coord, "^[:space:]*([0-9]{1,3})([0-5][0-9]\\.([0-9]+))[:space:]*$", REG_EXTENDED);
        public const string fapint_regex_nmea_coord = "^[:space:]*([0-9]{1,3})([0-5][0-9]\\.([0-9]+))[:space:]*$";
//
        //		regcomp(&fapint_regex_telemetry, "^([0-9]+),(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,([01]{0,8})", REG_EXTENDED);
        public const string fapint_regex_telemetry = "^([0-9]+),(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,(-?)([0-9]{1,6}|[0-9]+\\.[0-9]+|\\.[0-9]+)?,([01]{0,8})";
        //		regcomp(&fapint_regex_peet_splitter, "^([0-9a-f]{4}|----)", REG_EXTENDED|REG_ICASE);
        public const string fapint_regex_peet_splitter = "^([0-9a-f]{4}|----)";
        //		regcomp(&fapint_regex_kiss_callsign, "^([A-Z0-9]+) *(-[0-9]+)?$", REG_EXTENDED);
        public const string fapint_regex_kiss_callsign = "^([A-Z0-9]+) *(-[0-9]+)?$";
//
        //		regcomp(&fapint_regex_detect_comp, "^[\\/\\\\A-Za-j]$", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_detect_comp = "^[\\/\\\\A-Za-j]$";
        //		regcomp(&fapint_regex_detect_wx, "^_([0-9]{8})c[- .0-9]{1,3}s[- .0-9]{1,3}", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_detect_wx = "^_([0-9]{8})c[- .0-9]{1,3}s[- .0-9]{1,3}";
        //		regcomp(&fapint_regex_detect_telem, "^T#(.*?),(.*)$", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_detect_telem = "^T#(.*?),(.*)$";
        //		regcomp(&fapint_regex_detect_exp, "^\\{\\{", REG_EXTENDED|REG_NOSUB);
        public const string fapint_regex_detect_exp = "^\\{\\{";
//	
        //		regcomp(&fapint_regex_kiss_hdrbdy, "^([A-Z0-9,*>-]+):(.+)$", REG_EXTENDED);
        public const string fapint_regex_kiss_hdrbdy = "^([A-Z0-9,*>-]+):(.+)$";
        //		regcomp(&fapint_regex_hdr_detail, "^([A-Z0-9]{1,6})(-[0-9]{1,2})?>([A-Z0-9]{1,6})(-[0-9]{1,2})?(,.*)?$", REG_EXTENDED);
        public const string fapint_regex_hdr_detail = "^([A-Z0-9]{1,6})(-[0-9]{1,2})?>([A-Z0-9]{1,6})(-[0-9]{1,2})?(,.*)?$";
        //		regcomp(&fapint_regex_kiss_digi, "^([A-Z0-9]{1,6})(-[0-9]{1,2})?(\\*)?$", REG_EXTENDED);
        public const string fapint_regex_kiss_digi = "^([A-Z0-9]{1,6})(-[0-9]{1,2})?(\\*)?$";
//		
        //		regcomp(&fapint_regex_hopcount1, "^([A-Z0-9-]+)\\*$", REG_EXTENDED);
        public const string fapint_regex_hopcount1 = "^([A-Z0-9-]+)\\*$";
        //		regcomp(&fapint_regex_hopcount2, "^WIDE([1-7])-([0-7])$", REG_EXTENDED);     
        public const string fapint_regex_hopcount2 = "^WIDE([1-7])-([0-7])$";
    }
}