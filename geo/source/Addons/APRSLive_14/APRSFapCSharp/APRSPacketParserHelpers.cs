﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    //These will all need to be unit tested
    public class APRSPacketParserHelpers
    {
        private static string CleanCRLFAndTrailingSpaces(string toClean)
        {

            toClean = toClean.Replace(Environment.NewLine, "");
            toClean = toClean.TrimEnd(' ');
            return toClean;
        }
        //1==true
        //0==false
        public static bool telemety_parse(APRSPacketFAPDetails inPacket, string bodyToParse)
        {
            var outTelem = new APRSFapTelemetryReport();
            // s/^(\d+),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),([01]{0,8})//
            var telemPattern =
                @"^(\d+),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),(-|)(\d{1,6}|\d+\.\d+|\.\d+|),([01]{0,8})";

            var groups = Regex.Match(bodyToParse, telemPattern).Groups;
            if (groups.Count > 12)
            {
                uint parseduint;
                UInt32.TryParse(groups[1].Value, out parseduint);
                outTelem.seq = parseduint;
                var vals = new[]
                               {
                                   groups[2].Value + groups[3].Value,
                                   groups[4].Value + groups[5].Value, 
                                   groups[6].Value + groups[7].Value,
                                   groups[8].Value + groups[9].Value,
                                   groups[10].Value + groups[11].Value
                               };
                double holder;
                var doubleVals = new double[vals.Length];
                for (int i = 0; i < vals.Length; i++)
                {
                    if (Double.TryParse(vals[i], out holder))
                    {
                        if (holder >= 999999 || holder <= -999999)
                        {
                            inPacket.fap_error_code = ErrorCode.fapTLM_LARGE;
                            return false;
                        }
                        doubleVals[i] = holder;
                    }
                }
                outTelem.values = doubleVals;
                outTelem.bits = groups[12].Value.ToCharArray();
                inPacket.telemetry = outTelem;


            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapTLM_INV;
                return false;
            }
            return true;
        }

        //# possible TODO: ack piggybacking
        public static bool message_parse(APRSPacketFAPDetails inPacket, string bodyToParse)
        {
            //^:([A-Za-z0-9_ -]{9}):([\x20-\x7e\x80-\xfe]+)$/o
            var packetPattern = @"^:([A-Za-z0-9_ -]{9}):([\x20-\x7e\x80-\xfe]+)$";
            //^ack([A-Za-z0-9}]{1,5})\s*$/o
            var ackPattern = @"^ack([A-Za-z0-9}]{1,5})\s*$";
            //^rej([A-Za-z0-9}]{1,5})\s*$/o
            var rejPattern = @"^rej([A-Za-z0-9}]{1,5})\s*$";
            //^([^{]*)\{([A-Za-z0-9}]{1,5})\s*$/o
            var messagePattern = @"^([^{]*)\{([A-Za-z0-9}]{1,5})\s*$";
            //^(BITS|PARM|UNIT|EQNS)\./i
            var telemPattern = @"^(BITS|PARM|UNIT|EQNS)\.";

            GroupCollection groups;
            if ((groups = Regex.Match(bodyToParse, packetPattern).Groups).Count > 2)
            {
                var destination = groups[1].Value.Trim();
                var message = groups[2].Value;
                inPacket.destination = destination;
                //check whether this is an ack
                if ((groups = Regex.Match(message, ackPattern).Groups).Count > 1)
                {
                    // trailing spaces are allowed because some
                    //	 broken software insert them..
                    inPacket.message_ack = groups[1].Value;
                    return true;
                }
                // check whether this is a message reject
                if ((groups = Regex.Match(message, rejPattern).Groups).Count > 1)
                {
                    inPacket.message_rej = groups[1].Value;
                    return true;
                }
                //separate message-id from the body, if present
                if ((groups = Regex.Match(message, messagePattern).Groups).Count > 2)
                {
                    inPacket.message = groups[1].Value;
                    inPacket.message_id = groups[2].Value;
                }
                else
                {
                    inPacket.message = message;
                }
                //catch telemetry messages
                if (Regex.IsMatch(message, telemPattern, RegexOptions.IgnoreCase))
                {
                    inPacket.fap_packet_type = PacketType.fapTELEMETRY_MESSAGE;
                }
                return true;
            }
            inPacket.fap_error_code = ErrorCode.fapMSG_INV;
            return false;
        }

        public static bool capabilities_parse(APRSPacketFAPDetails inPacket, string bodyToParse)
        {
            GroupCollection groups;
            bodyToParse = CleanCRLFAndTrailingSpaces(bodyToParse);
            //# Then just split the packet, we aren't too picky about the format here.
            //# Also duplicates and case changes are not handled in any way,
            //# so the last part will override an earlier part and different
            //# cases can be present. Just remove trailing/leading spaces.
            var caps = bodyToParse.Split(',');
            Dictionary<string, string> capabilities = new Dictionary<string, string>();
            foreach (var cap in caps)
            {
                //^\s*([^=]+?)\s*=\s*(.*?)\s*$/o
                var capPatternTV = @"^\s*([^=]+?)\s*=\s*(.*?)\s*$";
                //^\s*([^=]+?)\s*$/o
                var capPatternT = @"^\s*([^=]+?)\s*$";
                if ((groups = Regex.Match(cap, capPatternTV).Groups).Count > 2)
                {//token and value
                    capabilities.Add(groups[1].Value, groups[2].Value);
                }
                else if ((groups = Regex.Match(cap, capPatternTV).Groups).Count > 1)
                {
                    //Just token
                    capabilities.Add(groups[1].Value, String.Empty);
                }

            }
            if (capabilities.Keys.Count > 0)
            {
                inPacket.capabilities = capabilities;
                return true;
            }
            //             at least one capability has to be defined for a capability
            //	 packet to be counted as valid
            return false;
        }

        public static bool status_parse(APRSPacketFAPDetails inPacket, string bodyToParse, bool useRawTime)
        {
            GroupCollection groups;
            var packetString = CleanCRLFAndTrailingSpaces(bodyToParse);
            //Check for timestamp
            if ((groups = Regex.Match(packetString, @"^(\d{6}z)").Groups).Count > 1)
            {
                inPacket.sentTime = ParseTimeStamp(groups[1].Value, useRawTime);
                if (inPacket.sentTime.Equals(DateTime.MinValue))
                {
                    inPacket.fap_error_code = ErrorCode.fapTIMESTAMP_INV_STA;
                }
                packetString = packetString.Substring(7);
            }
            inPacket.status = packetString;
            return true;
        }
        // Parses the body of a DX spot packet. Returns the following
        // hash elements: dxsource (source of the info), dxfreq (frequency),
        // dxcall (DX callsign) and dxinfo (info string).
        public static bool dx_parse(APRSPacketFAPDetails inPacket, string sourceCall, string info)
        {
            if (!string.IsNullOrEmpty(MiniHelpers.fap_check_ax25_call(sourceCall)))
            {
                inPacket.fap_error_code = ErrorCode.fapDX_INV_SRC;
                return false;
            }
            inPacket.dxSource = sourceCall;
            GroupCollection groups;
            //Strip white space
            //s/^\s*(.*?)\s*$/$1/
            var modInfo = info.Trim();

            //time
            //s/\s*(\d{3,4}Z)//
            var timePattern = @"\s*(\d{3,4}Z)";
            if ((groups = Regex.Match(modInfo, timePattern).Groups).Count > 1)
            {
                inPacket.dxTimeStamp = groups[1].Value;
                Regex.Replace(modInfo, timePattern, "");
            }
            //freq
            //s/^(\d+\.\d+)\s*//
            var freqPattern = @"^(\d+\.\d+)\s*";
            if ((groups = Regex.Match(modInfo, freqPattern).Groups).Count > 1)
            {
                inPacket.dxFreq = groups[1].Value;
                Regex.Replace(modInfo, freqPattern, "");
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapDX_INV_FREQ;
            }
            //dxcall
            //s/^([a-zA-Z0-9-\/]+)\s*//
            var dxCallPattern = @"^([a-zA-Z0-9-\/]+)\s*";
            if ((groups = Regex.Match(modInfo, dxCallPattern).Groups).Count > 1)
            {

                inPacket.dxCall = groups[1].Value;
                Regex.Replace(modInfo, dxCallPattern, "");
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapDX_NO_DX;
            }
            //            $info =~ s/\s+/ /g;
            //	$rh->{'dxinfo'} = $info;
            inPacket.dxInfo = modInfo;
            return true;
        }

        //TODO still needs to be tests more
        //Returns UTC time
        static public DateTime ParseTimeStamp(string timeStampString, bool rawTimeStamp)
        {
            var dateTime = DateTime.MinValue;
            // ^(\d{2})(\d{2})(\d{2})(z|h|\/)$/o
            var timeStampPattern = @"^(\d{2})(\d{2})(\d{2})(z|h|\/)$";
            var groups = Regex.Match(timeStampString, timeStampPattern).Groups;
            if (groups.Count < 5) return dateTime;

            var timeStampType = groups[4].Value;

            if (timeStampType.Equals("h"))
            {
                int hour, minute, second;//HMS format
                Int32.TryParse(groups[1].Value, out hour);
                Int32.TryParse(groups[2].Value, out minute);
                Int32.TryParse(groups[3].Value, out second);
                if (hour > 23 || minute > 59 || second > 59) return dateTime;
                //       All calculations here are in UTC, but
                //		if this is run under old MacOS (pre-OSX), then
                //		 Date_to_Time could be in local time..
                //No clue what that comment means (davidl)
                var nowTime = DateTime.UtcNow;
                var tStamp = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, hour, minute, second, DateTimeKind.Utc);
                //                //    If the time is more than about one hour
                //                //	 into the future, roll the timestamp
                //                //	 one day backwards.
                //                if (nowTime + new TimeSpan(0, 0, 3900) < tStamp)
                //                {
                //                    tStamp = tStamp - new TimeSpan(0, 0, 86400);
                //                    //	If the time is more than about 23 hours
                //                    //		 into the past, roll the timestamp one
                //                    //	 day forwards.
                //                }
                //                else if (nowTime - new TimeSpan(0, 0, 82500) > tStamp)
                //                {
                //                    tStamp = tStamp + new TimeSpan(0, 0, 86400);
                //                }
                return tStamp;
            }
            if (timeStampType.Equals("z") || timeStampType.Equals("/"))
            {
                //     Timestamp is DHM, UTC (z) or local (/).
                //	Always intepret local to mean local to this computer.
                int day, hour, minute; //DHM format
                Int32.TryParse(groups[1].Value, out day);
                Int32.TryParse(groups[2].Value, out hour);
                Int32.TryParse(groups[3].Value, out minute);
                if (day < 1 || day > 31 || hour > 23 || minute > 59)
                {
                    return DateTime.MinValue;// dateTime.ToLocalTime();
                }
                // If time is under about 12 hours into the future, go there.
                //	Otherwise get the first matching time in the past. 


                if (timeStampType.Equals("z"))
                {
                    var curTime = DateTime.UtcNow;
                    //User to get year and month
                    var timeStamp = new DateTime(curTime.Year, curTime.Month, day, hour, minute, 0, DateTimeKind.Utc);
                    return timeStamp;
                }
                if (timeStampType.Equals("/"))
                {//im glad they are telling people to not send time in local time
                    var curTime = DateTime.Now;
                    //User to get year and month
                    var timeStamp = new DateTime(curTime.Year, curTime.Month, day, hour, minute, 0, DateTimeKind.Local);
                    return timeStamp.ToUniversalTime();
                }
            }
            return DateTime.MinValue;
        }
    }
}