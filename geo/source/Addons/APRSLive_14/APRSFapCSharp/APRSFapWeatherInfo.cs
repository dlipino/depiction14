﻿namespace APRSFapCSharp
{
    public class APRSFapWeatherInfo
    {
        //    	/// Wind gust mph
        public double WindGust = double.NaN;
        //	/// Wind direction in degrees.
        public double WindDirection = double.NaN;
        //	/// Wind speed in mph
        public double WindSpeed = double.NaN;
        //	/// Temperature in degrees Fahrenheit.
        public double Temperature = double.NaN;
        //	/// Indoor temperature in degrees Fahrenheit.
        public double IndoorTemperature = double.NaN;
        //	/// Rain from last 1 hour, in inches.
        public double RainLastHour = double.NaN;
        //	/// Rain from last day, in inches.
        public double RainLast24Hours = double.NaN;
        //	/// Rain since midnight, in inches.
        public double RainSinceMidnight = double.NaN;
        //	/// Relative humidity percentage.
        public double Humidity = double.NaN;
        //	/// Relative inside humidity percentage.
        public double InsideHumidity = double.NaN;
        //	/// Air pressure in millibars. 
        public double Pressure = double.NaN;
        //	/// Luminosity in watts per square meter.
        public double Luminosity = double.NaN;
        //	/// Show depth increasement from last day, in inches.
        public double SnowLast24Hours = double.NaN;

        //	/// Software type indicator.
        //	char* soft;
        public string SoftwareIndicator = null;
    }
}