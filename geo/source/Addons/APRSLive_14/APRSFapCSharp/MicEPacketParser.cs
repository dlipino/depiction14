﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    public class MicEPacketParser
    {     //1==true
        //0==false

        public static bool fapint_parse_mice(APRSPacketFAPDetails packet, string input)
        {
            string rest = string.Empty;
            MatchCollection matches = null;
            GroupCollection groups = null;

            var dstSplitResults = packet.dst_callsign.Split(new[] { '-' },2, StringSplitOptions.RemoveEmptyEntries);
            /* Body must be at least 8 chars long. Destination callsign must exist. */
            if (input.Length < 8 || dstSplitResults.Length < 1 || dstSplitResults[0].Length != 6)
            {
                packet.fap_error_code = ErrorCode.fapMICE_SHORT;
                return false;
            }
            var baseDstCallSign = dstSplitResults[0];
            /* Validate target call. */
            if (!Regex.IsMatch(baseDstCallSign, APRSRegexConstants.fapint_regex_mice_dstcall, RegexOptions.IgnoreCase))
            {
                /* A-K characters are not used in the last 3 characters and MNO are never used. */
                packet.fap_error_code = ErrorCode.fapMICE_INV;
                return false;
            }

            /* Get symbol table. */
            packet.symbol_table = input[7];
            //^[\x26-\x7f][\x26-\x61][\x1c-\x7f]{2}[\x1c-\x7d][\x1c-\x7f][\x21-\x7b\x7d][\/\\A-Z0-9]/o
            var validPacketPatterWithoutSymbolCheck =
                @"^[\x26-\x7f][\x26-\x61][\x1c-\x7f]{2}[\x1c-\x7d][\x1c-\x7f][\x21-\x7b][\/\\A-Z0-9]";

            var packetReplaceRegex =
                @"^([\x26-\x7f][\x26-\x61][\x1c-\x7f]{2})\x20([\x21-\x7b\x7d][\/\\A-Z0-9])(.*)"; //
            //            /$1\x20\x20$2$3";
            var symbolTableCheck = @"^[\/\\A-Z0-9]";

            // Save symbol code and table although i don't exactly how they are used
            //http://www.aprs.org/symbols/symbolsX.txt
            packet.symbol_code = input[6];
            packet.symbol_table = input[7];

            //TODO this is very incomplete
            if (!Regex.IsMatch(input, validPacketPatterWithoutSymbolCheck))
            {
                var accept_broken_mice = false;
                if (accept_broken_mice)
                {
//        If the accept_broken_mice option is given, check for a known
//		 corruption in the packets and try to fix it - aprsd is
//		 replacing some valid but non-printable mic-e packet
//		 characters with spaces, and some other software is replacing
//		 the multiple spaces with a single space. This regexp
//		 replaces the single space with two spaces, so that the rest
//		of the code can still parse the position data.
//               $packet =~ s/^([\x26-\x7f][\x26-\x61][\x1c-\x7f]{2})\x20([\x21-\x7b\x7d][\/\\A-Z0-9])(.*)/$1\x20\x20$2$3/o) {
			
                }
                else
                {
                    if (!Regex.IsMatch(packet.symbol_table.ToString(), symbolTableCheck))
                    {
                        packet.fap_error_code = ErrorCode.fapSYM_INV_TABLE;

                    }
                    else
                    {
                        packet.fap_error_code = ErrorCode.fapMICE_INV_INFO;
                        return false;
                    }
                }
            }
            packet.fap_pos_format = PositionFormat.fapPOS_MICE;

            var mbitstring = baseDstCallSign.Substring(0, 3);
            mbitstring = Regex.Replace(mbitstring, "[0-9]", "0");
            mbitstring = Regex.Replace(mbitstring, "L", "0");
            mbitstring = Regex.Replace(mbitstring, "[P-Z]", "1");
            mbitstring = Regex.Replace(mbitstring, "[A-K]", "2");
            packet.messagebits = mbitstring;
            #region lat/long course and speed
            /* Start process from the target call to find latitude, message bits, N/S
             * and W/E indicators and long. offset. */
            //            tr/A-JP-YKLZ/0-90-9___/
            ////             Regex.Replace(strInput,"(?<first>\S+) (?<last>\S+)","${last},${first}")
            //            var replace = @"[P-Y]";// "A-JP-YKLZ";
            //            var replaceWith = "$[0-9]";//0-90-9___";
            //            templat = Regex.Replace(templat, replace, replaceWith);
            //            var m = Regex.Matches(templat, @"^(\d+)(_*)$");
            /* First create a translated copy to find latitude. */

            char[] latitude = new char[baseDstCallSign.Length];
            for (int i = 0; i < baseDstCallSign.Length; i++)
            {
                if (baseDstCallSign[i] >= 'A' && baseDstCallSign[i] <= 'J')
                {
                    // A-J -> 0-9 
                    latitude[i] = (char)(baseDstCallSign[i] - (char)17);
                }
                else if (baseDstCallSign[i] >= 'P' && baseDstCallSign[i] <= 'Y')
                {
                    // P-Y -> 0-9 
                    latitude[i] = (char)(baseDstCallSign[i] - (char)32);
                }
                else if (baseDstCallSign[i] == 'K' || baseDstCallSign[i] == 'L' || baseDstCallSign[i] == 'Z')
                {
                    // pos amb 
                    latitude[i] = '_';
                }
                else
                {
                    latitude[i] = baseDstCallSign[i];
                }
            }

            // Check the amount of position ambiguity. 
            var latString = new string(latitude);
            groups = Regex.Match(latString, APRSRegexConstants.fapint_regex_mice_amb, RegexOptions.IgnoreCase).Groups;
            if (groups.Count > 2)
            {

                var amount = 6 - groups[1].Value.Length;
                if (amount > 4)
                {
                    packet.fap_error_code = ErrorCode.fapMICE_AMB_LARGE;
                    return false;
                }
                packet.pos_ambiguity = amount;
                packet.pos_resolution = MiniHelpers.fapint_get_pos_resolution(2 - packet.pos_ambiguity);
            }
            else
            {
                // no digits in the beginning, baaad..
                // or the ambiguity digits weren't continuous
                packet.fap_error_code = ErrorCode.fapMICE_AMB_INV;
                return false;
            }
            /* Convert the latitude to the midvalue if position ambiguity is used. */
            //Not sure what this does yet, will have to wait for tests
            if (packet.pos_ambiguity >= 4)
            {
                /* The minute is between 0 and 60, so the midpoint is 30. */
                var index = latString.IndexOf("_");
                if (index > 0)
                {
                    latitude[index] = '3';
                }
                latString = new string(latitude);
                //                $tmplat =~ s/_/3/;
            }
            else
            {
                var index = latString.IndexOf("_");
                if (index > 0)
                {
                    latitude[index] = '5';
                }
                latString = new string(latitude);
                //      	$tmplat =~ s/_/5/;  # the first is changed to digit 5
            }
            latString = latString.Replace("_", "0");
            //$tmplat =~ s/_/0/g; # the rest are changed to digit 0

            double latDegrees;
            double.TryParse(latString.Substring(0, 2), out latDegrees);
            double latMinutes;
            double.TryParse(latString.Substring(2, 2) + "." + latString.Substring(4, 2), out latMinutes);
            // Convert latitude degrees into number and save it.
            latDegrees += (latMinutes / 60.0);
            packet.latitude = latDegrees;
            if (baseDstCallSign[3] <= 0x4c)
            {
                packet.latitude = 0 - packet.latitude;
            }

            /* Decode the longitude, the first three bytes of the body after the data
            * type indicator. First longitude degrees, remember the longitude offset. */
            var longDegrees = input[0] - 28;
            double longitude = 0;
            if (baseDstCallSign[4] >= 0x50)
            {
                longDegrees += 100;
            }
            if (longDegrees >= 180 && longDegrees <= 189)
            {
                longDegrees -= 80;
            }
            else if (longDegrees >= 190 && longDegrees <= 199)
            {
                longDegrees -= 190;
            }
            longitude = longDegrees;
            /* Get longitude minutes. */
            var lonMinutes = input[1] - 28;
            if (lonMinutes >= 60)
            {
                lonMinutes -= 60;
            }
            var lonSec = input[2] - 28;
            var lonString = string.Format("{0}.{1}", lonMinutes, lonSec);

            /* Apply pos amb and save. */
            if (packet.pos_ambiguity == 4)
            {
                /* Minutes are not used. */
                packet.longitude += 0.5;
            }
            else if (packet.pos_ambiguity == 3)
            {
                /* 1 minute digit is used. */
                var lonTmp = lonString.Substring(0, 1) + ".5";
                double lonTmpD;
                double.TryParse(lonTmp, out lonTmpD);
                longitude += (lonTmpD / 60.0);
            }
            else if (packet.pos_ambiguity == 2)
            {
                /* Whole minutes are used. */
                var lonTmp = lonString.Substring(0, 2) + ".5";
                double lonTmpD;
                double.TryParse(lonTmp, out lonTmpD);
                longitude += (int)(lonTmpD / 60.0);
            }
            else if (packet.pos_ambiguity == 1)
            {
                /* Whole minutes and 1 decimal are used. */
                var lonTmp = lonString.Substring(0, 4) + ".5";
                double lonTmpD;
                double.TryParse(lonTmp, out lonTmpD);
                longitude += (lonTmpD / 60.0);
            }
            else if (packet.pos_ambiguity == 0)
            {
                double lonTmpD;
                double.TryParse(lonString, out lonTmpD);
                longitude += (lonTmpD / 60.0);
            }
            else
            {
                packet.fap_error_code = ErrorCode.fapMICE_AMB_ODD;
                return false;
            }
            packet.longitude = longitude;

            /* Check E/W sign. */
            if (baseDstCallSign[5] >= 0x50)
            {
                packet.longitude = 0 - packet.longitude;
            }

            /* Now onto speed and course. */
            double speed = (input[3] - 28) * 10;
            double course_speed = input[4] - 28;
            double course_speed_tmp = Math.Floor(course_speed / 10);
            speed += course_speed_tmp;
            course_speed -= course_speed_tmp * 10;
            double course = 100 * course_speed;
            course += input[5] - 28;

            /* Some adjustment. */
            if (speed >= 800)
            {
                speed -= 800;
            }
            if (course >= 400)
            {
                course -= 400;
            }

            packet.speed = speed * MiniHelpers.KNOT_TO_KMH*MiniHelpers.KMH_TO_MPH;
            if (course >= 0)
            {
                packet.course = course;
            }
            #endregion
            //   Check for possible altitude and comment data.
            //	 It is base-91 coded and in format 'xxx}' where
            //	 x are the base-91 digits in meters, origin is 10000 meters
            //	 below sea.
            if (input.Length > 8)
            {
                rest = input.Substring(8);
                //   check for Mic-E Telemetry Data
                //^'([0-9a-f]{2})([0-9a-f]{2})(.*)$/i
                var telemChannels1and3 = "^'([0-9a-f]{2})([0-9a-f]{2})(.*)$";
                groups = Regex.Match(rest, telemChannels1and3, RegexOptions.IgnoreCase).Groups;
                if (groups.Count > 3)
                {
                    rest = groups[3].Value;
                    //really need to figure this pack thing out
                    //                    $rethash->{'telemetry'} = {'vals' => [ unpack('C*', pack('H*', $1 . '00' . $2)) ]};
                    var parsedHexValues = new[] { groups[1].Value, "00", groups[2].Value };
                    var telem = packet.telemetry.values;
                    byte holder;
                    for (int i = 0; i < parsedHexValues.Length; i++)
                    {
                        byte.TryParse(parsedHexValues[i], NumberStyles.HexNumber, null, out holder);
                        //not sure what is up with
                        telem[i] = holder;
                    }
                }
                //^‘([0-9a-f]{10})(.*)$/i
                var telem5Channels = "^‘([0-9a-f]{10})(.*)$";
                groups = Regex.Match(rest, telem5Channels, RegexOptions.IgnoreCase).Groups;
                if (groups.Count > 2)
                {
                    //                    $rethash->{'telemetry'} = {'vals' => [ unpack('C*', pack('H*', $1)) ]			};
                    var thingToPack = groups[1].Value;
                    //A fake pack and unpack
                    var parsedHexValues = Regex.Matches(thingToPack, "([0-9a-z][0-9a-z])", RegexOptions.IgnoreCase);
                    var telem = packet.telemetry.values;
                    byte holder;
                    for (int i = 0; i < parsedHexValues.Count; i++)
                    {

                        byte.TryParse(parsedHexValues[i].Value, NumberStyles.HexNumber, null, out holder);
                        telem[i] = holder;
                    }
                    rest = groups[2].Value;
                }
                //Check for altitude //From meters about baseline
                //BASELINE_DEPTH_BELOW_SEALEVEL = 10000; meters
                //metersAboveBaseline - BASELINE_DEPTH_BELOW_SEALEVEL
                ///^(.*?)([\x21-\x7b])([\x21-\x7b])([\x21-\x7b])\}(.*)$/o
                var altitudeRegex = @"^(.*?)([\x21-\x7b])([\x21-\x7b])([\x21-\x7b])\}(.*)$";
                groups = Regex.Match(rest, altitudeRegex).Groups;
                if (groups.Count > 5)
                {
                    var altMeters = (groups[2].Value[0] - 33) * Math.Pow(91, 2) +
                            (groups[3].Value[0] - 33) * 91 +
                            (groups[4].Value[0] - 33) - 10000;
                    packet.altitude = altMeters/MiniHelpers.FT_TO_M;
                    rest = string.Format("{0}{1}", groups[1].Value, groups[5].Value);
                }
                rest = MiniHelpers.CheckForDAO(packet, rest);
                rest = MiniHelpers.CommentTelemetry(packet, rest);
                if (rest.Length > 0)
                {
                    rest = MiniHelpers.CleanupComment(rest);
                    packet.comment = rest;
                }
            }
            return true;
        }
    }
}