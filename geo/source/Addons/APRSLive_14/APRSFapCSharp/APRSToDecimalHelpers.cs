﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    public class APRSToDecimalHelpers
    {
        static public bool normalpos_to_decimal(APRSPacketFAPDetails inPacket, string bodyString)
        {
            if (bodyString.Length < 19)
            {
                inPacket.fap_error_code = ErrorCode.fapLOC_SHORT;
                return false;
            }
            inPacket.fap_pos_format = PositionFormat.fapPOS_UNCOMPRESSED;
            var isSouth = false;
            var isWest = false;
            double latDeg, latMin, lonDeg, lonMin;
            string latDegS, latMinS, lonDegs, lonMins;
            string symbolTable = null;
            var groups = Regex.Match(bodyString, APRSRegexConstants.fapint_regex_normalpos).Groups;
            if (groups.Count > 8)
            {
                var sind = groups[3].Value.ToUpperInvariant();
                var wind = groups[7].Value.ToUpperInvariant();
                symbolTable = groups[4].Value;
                inPacket.symbol_code = groups[8].Value[0];
                if (sind.Equals("S"))
                {
                    isSouth = true;
                }
                if (wind.Equals("W"))
                {
                    isWest = true;
                }
                latDegS = groups[1].Value;
                latMinS = groups[2].Value;
                lonDegs = groups[5].Value;
                lonMins = groups[6].Value;
                Double.TryParse(latDegS, out latDeg);
                Double.TryParse(latMinS, out latMin);
                Double.TryParse(lonDegs, out lonDeg);
                Double.TryParse(lonMins, out lonMin);
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapSYM_INV_TABLE;
                return false;
            }
            if (String.IsNullOrEmpty(symbolTable) || !Regex.IsMatch(symbolTable, @"^[\/\\A-Z0-9]$"))
            {
                inPacket.fap_error_code = ErrorCode.fapSYM_INV_TABLE;
                return false;
            }
            inPacket.symbol_table = symbolTable[0];
            //            	# Check the degree values
            if (latDeg > 89 || lonDeg > 179)
            {
                inPacket.fap_error_code = ErrorCode.fapLOC_LARGE;
                return false;
            }
            //  Find out the amount of position ambiguity
            var tmplat = latMinS.Replace(".", "");// remove the period
            //^(\d{0,4})( {0,4})$/io
            groups = Regex.Match(tmplat, @"^(\d{0,4})( {0,4})$").Groups;
            if (groups.Count > 2)
            {
                inPacket.pos_ambiguity = groups[2].Value.Length;
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapLOC_AMB_INV;
                return false;
            }
            double latitude = 0, longitude = 0;
            if (inPacket.pos_ambiguity == 0)
            {
                //  No position ambiguity. Check longitude for invalid spaces
                //		if ($lon_min =~ / /io) {
                //			_a_err($rethash, 'loc_amb_inv', 'longitude 0');
                //			return 0;
                //		}
                latitude = latDeg + latMin / 60.0;
                longitude = lonDeg + lonMin / 60.0;
            }
            else if (inPacket.pos_ambiguity == 4)
            {
                latitude = latDeg + 0.5;
                longitude = lonDeg + 0.5;
            }
            else if (inPacket.pos_ambiguity == 1)
            {
                latMinS = latMinS.Substring(0, 4);
                lonMins = lonMins.Substring(0, 4);
                Double.TryParse(latMinS, out latMin);
                Double.TryParse(lonMins, out lonMin);
                // if ($lat_min =~ / /io || $lon_min =~ / /io) {
                //			_a_err($rethash, 'loc_amb_inv', 'lat/lon 1');
                //			return 0;
                //		}
                latitude = latDeg + (latMin + .5) / 60.0;
                longitude = lonDeg + (lonMin + .5) / 60.0;
            }
            else if (inPacket.pos_ambiguity == 2)
            {
                latMinS = latMinS.Substring(0, 2);
                lonMins = lonMins.Substring(0, 2);
                Double.TryParse(latMinS, out latMin);
                Double.TryParse(lonMins, out lonMin);
                //                if ($lat_min =~ / /io || $lon_min =~ / /io) {
                //			_a_err($rethash, 'loc_amb_inv', 'lat/lon 1');
                //			return 0;
                //		}
                latitude = latDeg + (latMin + .5) / 60.0;
                longitude = lonDeg + (lonMin + .5) / 60.0;

            }
            else if (inPacket.pos_ambiguity == 3)
            {
                latMinS = latMinS.Substring(0, 1) + '5';
                lonMins = lonMins.Substring(0, 1) + '5';
                Double.TryParse(latMinS, out latMin);
                Double.TryParse(lonMins, out lonMin);
                //                if ($lat_min =~ / /io || $lon_min =~ / /io) {
                //			_a_err($rethash, 'loc_amb_inv', 'lat/lon 1');
                //			return 0;
                //		}
                latitude = latDeg + (latMin) / 60.0;
                longitude = lonDeg + (lonMin) / 60.0;
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapLOC_AMB_INV;
                return false;
            }
            if (isSouth)
            {
                latitude = -latitude;
            }
            if (isWest)
            {
                longitude = -longitude;
            }
            inPacket.longitude = longitude;
            inPacket.latitude = latitude;
            inPacket.pos_resolution = MiniHelpers.fapint_get_pos_resolution(2 - inPacket.pos_ambiguity);
            return true;
        }

        public static bool compressed_to_decimal(APRSPacketFAPDetails inPacket, string bodyToParse)
        {
            //^[\/\\A-Za-j]{1}[\x21-\x7b]{8}[\x21-\x7b\x7d]{1}[\x20-\x7b]{3}/o
            var compressedPattern = @"^[\/\\A-Za-j]{1}[\x21-\x7b]{8}[\x21-\x7b\x7d]{1}[\x20-\x7b]{3}";
            if (!Regex.IsMatch(bodyToParse, compressedPattern))
            {
                inPacket.fap_error_code = ErrorCode.fapCOMP_INV;
                return false;
            }

            inPacket.fap_pos_format = PositionFormat.fapPOS_COMPRESSED;

            var symbolTable = (bodyToParse.Substring(0, 1)[0]);
            var lat1 = bodyToParse.Substring(1, 1)[0] - 33;
            var lat2 = bodyToParse.Substring(2, 1)[0] - 33;
            var lat3 = bodyToParse.Substring(3, 1)[0] - 33;
            var lat4 = bodyToParse.Substring(4, 1)[0] - 33;
            var long1 = bodyToParse.Substring(5, 1)[0] - 33;
            var long2 = bodyToParse.Substring(6, 1)[0] - 33;
            var long3 = bodyToParse.Substring(7, 1)[0] - 33;
            var long4 = bodyToParse.Substring(8, 1)[0] - 33;
            var symbolcode = bodyToParse.Substring(9, 1)[0];
            var c1 = bodyToParse.Substring(10, 1)[0] - 33;
            var s1 = bodyToParse.Substring(11, 1)[0] - 33;
            var comptype = bodyToParse.Substring(12, 1)[0] - 33;

            inPacket.symbol_code = symbolcode;
            var map = new Dictionary<string, string>
                          {
                              {"a", "0"},
                              {"b", "1"},
                              {"c", "2"},
                              {"d", "3"},
                              {"e", "4"},
                              {"f", "5"},
                              {"g", "6"},
                              {"h", "7"},
                              {"i", "8"},
                              {"j", "9"}
                          };
            inPacket.symbol_table = Regex.Replace(symbolTable.ToString(), "[a-j]", m => map[m.Value])[0];

            var latitude = 90 - ((lat1 * Math.Pow(91, 3) + lat2 * Math.Pow(91, 2) + lat3 * 91 + lat4) / 380926);
            var longitude = -180 + ((long1 * Math.Pow(91, 3) + long2 * Math.Pow(91, 2) + long3 * 91 + long4) / 190463);
            inPacket.latitude = latitude;
            inPacket.longitude = longitude;

            /* Save best-case position resolution in meters: 1852 meters * 60 minutes in a degree * 180 degrees / 91^4. */
            inPacket.pos_resolution = 0.291;
            //# GPS fix status, only if csT is used
            if (c1 != -1)
            {
                if ((comptype & 0x20) == 0x20)
                {
                    inPacket.gps_fix_status = true;
                }
                else
                {
                    inPacket.gps_fix_status = true;
                }
            }
            //check the compression type, if GPGGA, then
            //the cs bytes are altitude. Otherwise try
            //to decode it as course and speed. And
            //finally as radio range
            //if c is space, then csT is not used.
            //Also require that s is not a space.
            if (c1 == -1 || s1 == -1)
            {
                //csT not used
            }
            else if ((comptype & 0x18) == 0x10)
            {
                // cs is altitude
                var cs = c1 * 91 + s1;
                inPacket.altitude = Math.Pow(1.002, cs);// *.3048;
            }
            else if (c1 >= 0 && c1 <= 89)
            {
                if (c1 == 0)
                {
                    //   special case of north, APRS spec
                    //	 uses zero for unknown and 360 for north.
                    //	so remember to convert north here.
                    inPacket.course = 360;
                }
                else
                {
                    inPacket.course = c1 * 4;
                }
                //                convert directly to km/h
                inPacket.speed = (Math.Pow(1.08, s1) - 1) * MiniHelpers.KNOT_TO_KMH*MiniHelpers.KMH_TO_MPH;
            }
            else if (c1 == 90)
            {
                //convert directly to km
                inPacket.radio_range = 2 * Math.Pow(1.08, s1) * MiniHelpers.MPH_TO_KMH;
            }

            return true;
        }

        public static bool item_to_decimal(APRSPacketFAPDetails inPacket, string bodyToParse)
        {
            //Minimum length for an item is 18 characters
            //or 24 if non compressed
            if (bodyToParse.Length < 18)
            {
                inPacket.fap_error_code = ErrorCode.fapITEM_SHORT;
                return false;
            }

            GroupCollection groups;
            //^\)([\x20\x22-\x5e\x60-\x7e]{3,9})(!|_)/o
            var itemPattern = @"^\)([\x20\x22-\x5e\x60-\x7e]{3,9})(!|_)";
            if ((groups = Regex.Match(bodyToParse, itemPattern).Groups).Count > 2)
            {
                inPacket.object_or_item_name = groups[1].Value;
                inPacket.alive = false;
                if (groups[2].Value.Equals("!"))
                {
                    inPacket.alive = true;
                }
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapITEM_INV;
                return false;
            }
            //Forward the location parsing onwards
            var locationOffset = 2 + inPacket.object_or_item_name.Length;
            var locationChar = bodyToParse.Substring(locationOffset + 1);
            var retVal = false;
            if (Regex.IsMatch(locationChar, @"^[\/\\A-Za-j]$"))
            {
                //compressed
                retVal = compressed_to_decimal(inPacket, bodyToParse.Substring(locationOffset, 19));
                locationOffset += 13;
            }
            else if (Regex.IsMatch(locationChar, @"^\d$"))
            {
                //normal
                retVal = normalpos_to_decimal(inPacket, bodyToParse.Substring(locationOffset));
                locationOffset += 19;
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapITEM_DEC_ERR;
                return false;
            }
            if (!retVal) return false;
            if (!inPacket.symbol_code.Equals("_"))
            {
                MiniHelpers.comments_to_decimal(inPacket, bodyToParse.Substring(locationOffset));
            }

            return true;
        }

        public static bool object_to_decimal(APRSPacketFAPDetails inPacket, string bodyToParse,bool useRawTime)
        {
            //Minimum length for an object is 31 characters
            //(or 46 characters for non-compressed)
            if (bodyToParse.Length < 31)
            {
                inPacket.fap_error_code = ErrorCode.fapOBJ_SHORT;
                return false;
            }

           
            var timestamp = String.Empty;
            GroupCollection groups;
            //Parse the object upt to the location
            //^;([\x20-\x7e]{9})(\*|_)(\d{6})(z|h|\/)/o
            var splitPatterm = @"^;([\x20-\x7e]{9})(\*|_)(\d{6})(z|h|\/)";
            if ((groups = Regex.Match(bodyToParse, splitPatterm).Groups).Count > 4)
            {
                inPacket.object_or_item_name = groups[1].Value;
                if (groups[2].Value.Equals("*"))
                {
                    inPacket.alive = true;
                }
                else
                {
                    inPacket.alive = false;
                }
                timestamp = groups[3].Value + groups[4].Value;
            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapOBJ_INV;
                return false;
            }
            //Need to make sure that actually works.
            var dateTime = APRSPacketParserHelpers.ParseTimeStamp(timestamp, useRawTime);
            if (dateTime.Equals(DateTime.MinValue))
            {
                //warn of some sort of error
                inPacket.fap_error_code = ErrorCode.fapOBJ_INV;
            }
            inPacket.sentTime = dateTime;
            //Forward the location parsing onwards
            var locationOffset = 18;
            var locationChar = bodyToParse.Substring(locationOffset, 1);
            var retVal = false;
            //^[\/\\A-Za-j]$/o)
            var compressedPattern = @"^[\/\\A-Za-j]$";
            if (Regex.IsMatch(locationChar, compressedPattern))
            {
                //Compressed
                retVal = compressed_to_decimal(inPacket, bodyToParse.Substring(locationOffset, 13));
                locationOffset += 13;//now points to aprs data extension/comment
            }
            else if (Regex.IsMatch(locationChar, @"^\d$", RegexOptions.IgnoreCase))
            {
                //Normal
                retVal = normalpos_to_decimal(inPacket, bodyToParse.Substring(locationOffset));
                locationOffset += 19;

            }
            else
            {
                inPacket.fap_error_code = ErrorCode.fapOBJ_DEC_ERR;
                return false;
            }
            if (!retVal) return false;
            //# Check the APRS data extension and possible comments,
            //# unless it is a weather report (we don't want erroneus
            //# course/speed figures and weather in the comments..)
            if (!inPacket.symbol_code.Equals("_"))
            {
                MiniHelpers.comments_to_decimal(inPacket, bodyToParse.Substring(locationOffset));
            }
            else
            {
                WeatherParserHelpers.wx_parse(inPacket, bodyToParse.Substring(locationOffset));
            }
            return true;
        }
    }
}