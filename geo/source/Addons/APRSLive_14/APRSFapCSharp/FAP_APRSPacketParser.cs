﻿using System;
using System.Text.RegularExpressions;

namespace APRSFapCSharp
{
    public class FAP_APRSPacketParser
    {
        public bool FAP_APRSParser(ref APRSPacketFAPDetails inPacket, string input, bool is_ax25)
        {
            return FAP_APRSParser(ref inPacket, input, is_ax25, false);
        }

        public bool FAP_APRSParser(ref APRSPacketFAPDetails inPacket, string input, bool is_ax25, bool useRawTime)
        {
            if(inPacket == null)
            {
                inPacket = new APRSPacketFAPDetails();
            }
            
            // Check for missing params. 
            if (string.IsNullOrEmpty(input))
            {
                inPacket.fap_error_code = ErrorCode.fapPACKET_NO;
                return false;
            }
            
            inPacket.receivedTime = DateTime.UtcNow;
            inPacket.sentTime = DateTime.MinValue;
            //remove the cmd: stuff that sometimes get in there
            input = input.Replace("cmd:", "");
            //Find the end of header checking for NULL bytes while doing it. 

            var headerBodySplit = input.Split(new[] {':'}, 2);
            var splitCount = headerBodySplit.Length;
            string tempBody = string.Empty;
            if (splitCount != 2 && string.IsNullOrEmpty(inPacket.header))
            {
                inPacket.fap_error_code = ErrorCode.fapPACKET_INVALID;
                return false;
            }
            if (splitCount == 1 && !string.IsNullOrEmpty(inPacket.header))
            {
                tempBody = headerBodySplit[0];
            }
            else
            {
                inPacket.header = headerBodySplit[0];
                tempBody = headerBodySplit[1];
            }
            inPacket.message = tempBody;
            // a test to filter out things like "<UI>:" which are actually ax.25 frame information
            // which Kantronics adds to packets by default. setting: MHEADER ON.  These do not get printed if MHEADER OFF.
            var groups = Regex.Match(tempBody, @"^[\s ](\<.*\>[\s]*?:)(.*)$").Groups;
            if (groups.Count > 1 && groups[2].Success)
                tempBody = groups[2].Value;
           
            inPacket.body = tempBody;

            // Parse source, target and path. 
            if (!MiniHelpers.fapint_parse_header(inPacket, is_ax25))
            {
                return false;
            }
            if (tempBody.Length <= 0)
            {
                return false;
            }

            return FAP_ParseBody(inPacket,inPacket.body,is_ax25,useRawTime);
        }

        public bool FAP_ParseBody(APRSPacketFAPDetails inPacket, string bodyToParse, bool is_ax25, bool useRawTime)
        {
            if (inPacket == null) return false;
            bool retValue = true;
            GroupCollection groups;
            char packetTypechar;
            inPacket.body = bodyToParse;
            var body = inPacket.body;
            var pacLen = body.Length;
            
            //Detect packet type char. 
            packetTypechar = body[0];
            //Check for mic-e packet. 
            if ((packetTypechar == 0x27 || packetTypechar == 0x60))
            {
                if (pacLen >= 9)
                {
                    inPacket.fap_packet_type = PacketType.fapLOCATION;
                    return MicEPacketParser.fapint_parse_mice(inPacket, body.Substring(1));
                }
            }
            //Check for normal or compressed location packet.
            else if (packetTypechar.Equals('!') ||
                packetTypechar.Equals('=') ||
                packetTypechar.Equals('/') ||
                packetTypechar.Equals('@'))
            {
                // Check for messaging. 
                if (packetTypechar.Equals('!') || packetTypechar.Equals('/'))
                {
                    inPacket.messaging = false;
                }
                else { inPacket.messaging = true; }

                //Validate body.
                if (pacLen >= 14)
                {
                    string subBody = body;
                    inPacket.fap_packet_type = PacketType.fapLOCATION;
                    if (packetTypechar == '/' || packetTypechar == '@')
                    {
                        var calcedTimeStamp = APRSPacketParserHelpers.ParseTimeStamp(subBody.Substring(1, 7), useRawTime);
                        if (calcedTimeStamp.Equals(DateTime.MinValue))
                        {
                            inPacket.fap_error_code = ErrorCode.fapTIMESTAMP_INV_LOC;
                        }
                        else
                        {
                            inPacket.sentTime = calcedTimeStamp;
                        }
                        subBody = subBody.Substring(7);
                    }
                    subBody = subBody.Substring(1);//remove first char
                    //   Get position type character.
                    var poschar = subBody[0];

                    //     If timestamp check didn't fail, go on with location parsing. 
                    //       Detect position type. 
                    if (poschar >= 48 && poschar <= 57)
                    {
                        // It's normal position. 
                        if (subBody.Length >= 19)
                        {
                            var goodPos = APRSToDecimalHelpers.normalpos_to_decimal(inPacket, subBody);
                            //    continue parsing with possible comments, but only
                            //		 if this is not a weather report (course/speed mixup,
                            //	weather as comment)
                            //	if the comments don't parse, don't raise an error
                            if (goodPos && !inPacket.symbol_code.Equals('_'))
                            {
                                MiniHelpers.comments_to_decimal(inPacket, subBody.Substring(19));
                            }
                            else
                            {
                                WeatherParserHelpers.wx_parse(inPacket, subBody.Substring(19));
                            }
                        }
                    }
                    else if (poschar == 47 ||
                        poschar == 92 ||
                        (poschar >= 65 && poschar <= 90) ||
                        (poschar >= 97 && poschar <= 106))
                    {
                        //   It's compressed position. 
                        if (subBody.Length >= 13)
                        {
                            var success = APRSToDecimalHelpers.compressed_to_decimal(inPacket, subBody.Substring(0, 13));
                            //body.Length > 13 && not sure if it is needed, but should be subBody
                            if (success && inPacket.symbol_code != '_')
                            {
                                MiniHelpers.comments_to_decimal(inPacket, subBody.Substring(13));
                            }
                            else if (body.Length > 13 && success)
                            {
                                WeatherParserHelpers.wx_parse(inPacket, subBody.Substring(13));
                            }
                        }
                        else
                        {
                            inPacket.fap_error_code = ErrorCode.fapPACKET_SHORT;
                            return false;
                        }
                    }
                    else if (poschar == 33)
                    {
                        inPacket.fap_packet_type = PacketType.fapWX;
                        return WeatherParserHelpers.wx_parse_peet_logging(inPacket, subBody.Substring(1));
                    }
                    else
                    {
                        // Does not match any known type. 
                        inPacket.fap_error_code = ErrorCode.fapPACKET_INVALID;
                        return false;
                    }
                }
                else
                {
                    inPacket.fap_error_code = ErrorCode.fapPACKET_SHORT;
                    return false;
                }
            }
            //  Check for weather packet. 
            else if (packetTypechar == '_')
            {
                //  _(\d{8})c[\- \.\d]{1,3}s[\- \.\d]{1,3}
                var weatherPattern = @"_(\d{8})c[\- \.\d]{1,3}s[\- \.\d]{1,3}";
                if (Regex.IsMatch(body, weatherPattern))
                {
                    inPacket.fap_packet_type = PacketType.fapWX;
                    return WeatherParserHelpers.wx_parse(inPacket, body.Substring(9));

                }
                inPacket.fap_error_code = ErrorCode.fapWX_UNSUPP;
                return false;
            }
            //Check for object packet. 
            else if (packetTypechar == ';')
            {
                if (pacLen >= 31)
                {
                    inPacket.fap_packet_type = PacketType.fapOBJECT;
                    return APRSToDecimalHelpers.object_to_decimal(inPacket, body, useRawTime);
                }
            }
            // Check for NMEA data packet.
            else if (packetTypechar == '$')
            {
                //    don't try to parse the weather stations, require "$GP" start
                if (body.Substring(0, 3).Equals("$GP"))
                {
                    inPacket.fap_packet_type = PacketType.fapLOCATION;
                    return NMEAPacketParser.nmea_to_decimal(inPacket, body.Substring(1), useRawTime);
                }
                if (body.Substring(0, 5).Equals("$ULTW"))
                {
                    inPacket.fap_packet_type = PacketType.fapWX;
                    return WeatherParserHelpers.wx_parse_peet_packet(inPacket, body.Substring(5));
                }
            }
            //Check for item packet. 
            else if (packetTypechar == ')')
            {
                if (pacLen >= 18)
                {
                    inPacket.fap_packet_type = PacketType.fapITEM;
                    return APRSToDecimalHelpers.item_to_decimal(inPacket, body);

                }
            }
            //Check for message, bulletin or announcement packet. 
            else if (packetTypechar == ':')
            {
                inPacket.fap_packet_type = PacketType.fapMESSAGE;
                if (pacLen >= 11)
                {
                    inPacket.fap_packet_type = PacketType.fapMESSAGE;
                    return APRSPacketParserHelpers.message_parse(inPacket, body);
                }
            }
            //Check for capabilities packet. 
            else if (packetTypechar == '<')
            {
                //At least one other character besides '<' required. 
                if (pacLen >= 2)
                {
                    inPacket.fap_packet_type = PacketType.fapCAPABILITIES;
                    return APRSPacketParserHelpers.capabilities_parse(inPacket, body.Substring(1));
                }
            }
            //Check for status packet. 
            else if (packetTypechar == '>')
            {
                //  We can live with empty status reports. 
                if (pacLen >= 1)
                {
                    inPacket.fap_packet_type = PacketType.fapSTATUS;
                    return APRSPacketParserHelpers.status_parse(inPacket, body.Substring(1), useRawTime);
                }
            }
            //Check for telemetry packet. 
            //^T#(.*?),(.*)$/
            else if (Regex.IsMatch(body, @"^T#(.*?),(.*)$"))
            {
                inPacket.fap_packet_type = PacketType.fapTELEMETRY;
                return APRSPacketParserHelpers.telemety_parse(inPacket, body.Substring(2));
            }
            //^DX\s+de\s+(.*?)\s*[:>]\s*(.*)$/i
            // DX spot
            else if ((groups = Regex.Match(body, @"^DX\s+de\s+(.*?)\s*[:>]\s*(.*)$", RegexOptions.IgnoreCase).Groups).Count > 2)
            {
                inPacket.fap_packet_type = PacketType.fapDX_SPOT;
                return APRSPacketParserHelpers.dx_parse(inPacket, groups[1].Value, groups[2].Value);
            }
            //Experimental  
            //^{{/i
            else if (Regex.IsMatch(body, @"^{{", RegexOptions.IgnoreCase))
            {
                inPacket.fap_error_code = ErrorCode.fapEXP_UNSUPP;
                return false;
            }
            else
            {
                var pos = body.IndexOf("!");
                if (pos >= 0 && pos <= 39)
                {
                    inPacket.fap_packet_type = PacketType.fapLOCATION;
                    inPacket.messaging = false;
                    var pchar = body.Substring(pos + 1, 1);
                    // /^[\/\\A-Za-j]$/o
                    var pcharPattern1 = @"^[\/\\A-Za-j]$";
                    // /^\d$/io
                    var pcharPattern2 = @"^\d$";
                    if (Regex.IsMatch(pchar, pcharPattern1))
                    {
                        //Compressed position
                        if (body.Length > pos + 1 + 13)
                        {
                            retValue = APRSToDecimalHelpers.compressed_to_decimal(inPacket,
                                                                                     body.Substring(pos + 1 + 13));

                            if (retValue && !inPacket.symbol_code.Equals("_"))
                            {
                                MiniHelpers.comments_to_decimal(inPacket, body.Substring(pos + 14));
                            }
                        }
                    }
                    // /^\d$/io
                    else if (Regex.IsMatch(pchar, pcharPattern2, RegexOptions.IgnoreCase))
                    {
                        //normal uncompressed position
                        if (body.Length > pos + 1 + 19)
                        {
                            var retVal = APRSToDecimalHelpers.normalpos_to_decimal(inPacket,
                                                                          body.Substring(pos + 1));
                            if (retVal && !inPacket.symbol_code.Equals("_"))
                            {
                                MiniHelpers.comments_to_decimal(inPacket, body.Substring(pos + 20));
                            }
                        }
                    }
                }
                else
                {
                    retValue = false;
                }
            }
            return retValue;
        }
    }

}