﻿namespace APRSFapCSharp
{
    /// Telemetry report type.
    public class APRSFapTelemetryReport //fap_telemetry_t;
    {
        /// Id of report.
        public uint seq;
        public double[] values = new double[5];
    
        /// Telemetry bits as ASCII 0s and 1s. Unknowns are marked with question marks.
        public char[] bits = new char[8];
        #region constuctro
        public APRSFapTelemetryReport()
        {
            for(int i =0;i<values.Length;i++)
            {
                values[i] = 0;
            }
        }
        #endregion
    }
}