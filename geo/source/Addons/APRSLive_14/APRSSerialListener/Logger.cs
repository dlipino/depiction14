﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Depiction.API.ExceptionHandling;

namespace APRSPacketIO
{
    public class Logger
    {
        public static event OnWriteLogEventHandler OnWriteLog;
        public delegate void OnWriteLogEventHandler(string message);

        private static List<LogItem> logItems = new List<LogItem>();
        private static string logFileName;

        static public string FullLogName { get { return logFileName; } }

        static Logger()
        {
            var folder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (folder == null) return;
            logFileName = Path.Combine(folder, "AprsLive_log.txt");
            if (File.Exists(logFileName))
                File.Delete(logFileName);
            File.CreateText(logFileName).Close();
        }

        public static void WriteLogItem(string message)
        {
            logItems.Add(new LogItem { Message = message, Timestamp = DateTime.Now });
            if (OnWriteLog != null)
                OnWriteLog(message);

            if (logItems.Count > 0)
                Flush();
        }

        public static void Flush()
        {
            if (!File.Exists(logFileName))
            {
                return;
            }
            try
            {
                using (var sw = new StreamWriter(logFileName, true))
                {
                    foreach (var item in logItems)
                    {
                        sw.WriteLine(string.Format("{0:T} {1}", item.Timestamp, item.Message));
                    }
                }
                logItems.Clear();
            }
            catch (Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex, true, true);
            }
        }
    }
}
