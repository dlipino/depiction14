﻿using System.IO;
using APRSPacketIO;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class APRSLoggerTest
    {
        [Test]
        public void DoesLoggerWriteFile()
        {
            Logger.WriteLogItem("RandomMEssage");
            Assert.IsTrue(File.Exists(Logger.FullLogName));
            File.Delete(Logger.FullLogName);
        }
    }
}