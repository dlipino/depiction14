﻿using System.IO;
using APRSLiveSetupCustomActions;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class CustomSetupActionsTest
    {

        public string GetTempDirectory()
        {
            string path;
            
            path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(path);
            return path;
        }



        [Test]
        [Ignore]
        public void DoesInstallGrantUserRightsToAddonDirectory()
        {
            System.Diagnostics.Process.Start("http://www.microsoft.com");
            var tmpDir = GetTempDirectory();
            var aprsLiveDir = Path.Combine(tmpDir, "APRSLive");
            Directory.CreateDirectory(aprsLiveDir);
            File.WriteAllText(Path.Combine(aprsLiveDir, "test.txt"), "test");
            Directory.CreateDirectory(Path.Combine(tmpDir, "Help"));

            var testDepictionDir = GetTempDirectory();
//            SetupActions.InstallToDirectory(tmpDir);
//            SetupActions.CopyAPRSToDepictionDirAndUpdatePrivilages(tmpDir, testDepictionDir);
            
        }
    }
}
