﻿using System;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsPacketTimestampDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;
        private string tstamp = string.Empty;
        private DateTime expectedLocalTime;

        private DateTime gmtTime;
        private DateTime nowTime;
        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();
            nowTime = DateTime.Now;
            gmtTime = nowTime.ToUniversalTime();

            tstamp = gmtTime.Day.ToString("00") + gmtTime.Hour.ToString("00") + gmtTime.Minute.ToString("00");
            expectedLocalTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, nowTime.Hour, nowTime.Minute, 0);
//            expectedLocalTime = expectedLocalTime.AddSeconds(-nowTime.Second%60);
            //my $now = time();
            //my @gm = gmtime($now);
            //my $mday = $gm[3];
            //my $tstamp = sprintf('%02d%02d%02d', $gm[3], $gm[2], $gm[1]);
            //my $outcome = $now - ($now % 60); # will round down to the minute

            //

            //
        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        public void CanRawTimestampBeDecoded()
        {
            var packetString = "KB3HVP-14>APU25N,N8TJG-10*,WIDE2-1,qAR,LANSNG:@" + tstamp + "z4231.16N/08449.88Wu227/052/A=000941 {UIV32N}";

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, packetString, false, true));
            var time = packet.sentTime;
            Assert.AreEqual(expectedLocalTime.ToUniversalTime(), time);
        }
        //# First, try to get the raw timestam through
        //$aprspacket = 'KB3HVP-14>APU25N,N8TJG-10*,WIDE2-1,qAR,LANSNG:@' . $tstamp . 'z4231.16N/08449.88Wu227/052/A=000941 {UIV32N}';
        //$retval = parseaprs($aprspacket, \%h, 'raw_timestamp' => 1);
        //ok($retval, 1, "failed to parse a position packet with @..z timestamp");
        //ok($h{'timestamp'}, $tstamp, "wrong @..z raw timestamp parsed from position packet");
        //
        [Test]
        public void CanUnixTimestampBeDecoded()
        {
            var packetString = "KB3HVP-14>APU25N,N8TJG-10*,WIDE2-1,qAR,LANSNG:@" + tstamp + "z4231.16N/08449.88Wu227/052/A=000941 {UIV32N}";

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, packetString, false, false));
            var time = packet.sentTime;
            Assert.AreEqual(expectedLocalTime.ToUniversalTime(), time);
        }
        //# Then, try the one decoded to an UNIX timestamp
        //$aprspacket = 'KB3HVP-14>APU25N,N8TJG-10*,WIDE2-1,qAR,LANSNG:@' . $tstamp . 'z4231.16N/08449.88Wu227/052/A=000941 {UIV32N}';
        //$retval = parseaprs($aprspacket, \%h);
        //ok($retval, 1, "failed to parse a position packet with @..z timestamp");
        //ok($h{'timestamp'}, $outcome, "wrong @..z UNIX timestamp parsed from position packet");
        [Test]
        public void CanRawTimestampFromAHMSBeDecoded()
        {
            var aprspacket = "G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/055816h5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacket, false, true));
            var time = packet.sentTime;
            var eString = time.Hour.ToString("00") + time.Minute.ToString("00") + time.Second.ToString("00");

            Assert.AreEqual("055816", eString);

        }
        //# raw again, from a HMS version
        //$aprspacket = 'G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/055816h5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09';
        //$retval = parseaprs($aprspacket, \%h, 'raw_timestamp' => 1);
        //ok($retval, 1, "failed to parse a position packet with /..h timestamp");
        //ok($h{'timestamp'}, '055816', "wrong /..h raw timestamp parsed from position packet");
        //
        [Test]
        public void CanUnixTimestampFromHMSDecoded()
        {
            tstamp = gmtTime.Hour.ToString("00") + gmtTime.Minute.ToString("00") + gmtTime.Second.ToString("00");
            var aprspacket = "G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/" + tstamp + "h5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacket, false, false));
            var time = packet.sentTime;
            expectedLocalTime = new DateTime(nowTime.Year, nowTime.Month, nowTime.Day, nowTime.Hour, nowTime.Minute, nowTime.Second);
            //         
            Assert.AreEqual(expectedLocalTime.ToUniversalTime(), time);

        }
        //# decoded UNIX timestamp from HMS
        //$now = time();
        //@gm = gmtime($now);
        //$mday = $gm[3];
        //$tstamp = sprintf('%02d%02d%02d', $gm[2], $gm[1], $gm[0]);
        //$aprspacket = 'G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/' . $tstamp . 'h5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09';
        //$retval = parseaprs($aprspacket, \%h);
        //ok($retval, 1, "failed to parse a position packet with /..h timestamp");
        //ok($h{'timestamp'}, $now, "wrong /..h UNIX timestamp parsed from position packet");
        //
        [Test]
        public void CanRawTimestampFromLocalTimeDMHBeDecoded()
        {
            var aprspacket = "G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/060642/5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacket, false, true));
            var time = packet.sentTime.ToLocalTime();
            var eString = time.Day.ToString("00") + time.Hour.ToString("00") + time.Minute.ToString("00");

            Assert.AreEqual("060642", eString);
        }
        //# raw again, from a local-time DMH
        //$aprspacket = 'G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/060642/5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09';
        //$retval = parseaprs($aprspacket, \%h, 'raw_timestamp' => 1);
        //ok($retval, 1, "failed to parse a position packet with /../ local timestamp");
        //ok($h{'timestamp'}, '060642', "wrong /..h raw local timestamp parsed from position packet");
        //

        [Test]
        public void CanUnixTimestampFromLocalTimeDMHBeDecoded()
        {
            tstamp = nowTime.Day.ToString("00") + nowTime.Hour.ToString("00") + nowTime.Minute.ToString("00");
            var aprspacket = "G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/" + tstamp + "/5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacket, false, false));
            var time = packet.sentTime.ToLocalTime();
            Assert.AreEqual(expectedLocalTime, time);

        }
        //# decoded UNIX timestamp from local-time DMH
        //$now = time();
        //@gm = localtime($now);
        //$mday = $gm[3];
        //$tstamp = sprintf('%02d%02d%02d', $gm[3], $gm[2], $gm[1]);
        //$outcome = $now - ($now % 60); # will round down to the minute
        //$aprspacket = 'G4EUM-9>APOTC1,G4EUM*,WIDE2-2,qAS,M3SXA-10:/' . $tstamp . '/5134.38N/00019.47W>155/023!W26!/A=000188 14.3V 27C HDOP01.0 SATS09';
        //$retval = parseaprs($aprspacket, \%h);
        //ok($retval, 1, "failed to parse a position packet with /../ local timestamp");
        //ok($h{'timestamp'}, $outcome, "wrong /../ UNIX timestamp parsed from position packet");

        //# test timestamp option decoding
        //
        //use Test;
        //
        //BEGIN { plan tests => 2 + 2 + 2 + 2 + 2 + 2 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my($aprspacket, $tlm);
        //my %h;
        //my $retval;
        //






    }
}