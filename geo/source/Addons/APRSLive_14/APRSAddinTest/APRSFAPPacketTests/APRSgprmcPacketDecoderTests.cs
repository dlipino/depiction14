﻿using System;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class APRSGprmcPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;

        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        [Ignore("DateTime manipulation experiments")]
        public void TestTimeConversionFromPerl()
        {
            var utcTime = new DateTime(2007, 12, 12, 14, 55, 26,  DateTimeKind.Utc);
            var other = utcTime.Ticks;
        }
        [Test]
        public void CanGPRMCPacketBeRead()
        {
            var srccall = "OH7LZB-11";
            var dstcall = "APRS";
            var header = string.Format("{0}>{1},W4GR*,WIDE2-1,qAR,WA4DSY", srccall, dstcall);
            var body = string.Format("$GPRMC,145526,A,3349.0378,N,08406.2617,W,23.726,27.9,121207,4.9,W*7A");
            var aprspacketString = string.Format("{0}:{1}", header, body);

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(header, packet.header);
            Assert.AreEqual(body, packet.body);
            Assert.AreEqual(PacketType.fapLOCATION, packet.fap_packet_type);
            Assert.AreEqual(PositionFormat.fapPOS_NMEA, packet.fap_pos_format);
        
            Assert.AreEqual(int.MaxValue,packet.pos_ambiguity);
            Assert.IsNull(packet.messaging);
            //14:55:26 UTC
            //12/12/2007
            Assert.AreEqual(true,packet.nmea_checksum_ok);
            var expectedDateTime =  new DateTime(2007, 12, 12, 14, 55, 26,  DateTimeKind.Utc);
            //Assert.AreEqual("1197471326",paclet.timestamp);//the old one, done using perl
            Assert.AreEqual(expectedDateTime, packet.sentTime);

            Assert.AreEqual("33.8173", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-84.1044", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("0.1852", packet.pos_resolution.ToString("0.0000"));
            var exp = 43.94*MiniHelpers.KMH_TO_MPH;
            Assert.AreEqual(exp.ToString("0.00"), packet.speed.ToString("0.00"));
            Assert.AreEqual("28", packet.course.ToString("0"));
            Assert.AreEqual(double.NaN, packet.altitude);
        }
        //# a $GPRMC NMEA decoding test
        //# Wed Dec 12 2007, Hessu, OH7LZB
        //
        //use Test;
        //
        //BEGIN { plan tests => 15 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "OH7LZB-11";
        //my $dstcall = "APRS";
        //my $header = "$srccall>$dstcall,W4GR*,WIDE2-1,qAR,WA4DSY";
        //my $body = '$GPRMC,145526,A,3349.0378,N,08406.2617,W,23.726,27.9,121207,4.9,W*7A';
        //my $aprspacket = "$header:$body";
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //# the parser always appends an SSID - make sure the behaviour doesn't change
        //$dstcall .= '-0';
        //
        //ok($retval, 1, "failed to parse a GPRMC NMEA packet");
        //
        //ok($h{'header'}, $header, "incorrect header parsing");
        //ok($h{'body'}, $body, "incorrect body parsing");
        //ok($h{'type'}, 'location', "incorrect packet type parsing");
        //ok($h{'format'}, 'nmea', "incorrect packet format parsing");
        //
        //# check for undefined value, when there is no such data in the packet
        //ok($h{'posambiguity'}, undef, "incorrect posambiguity parsing");
        //ok($h{'messaging'}, undef, "incorrect messaging bit parsing");
        //
        //ok($h{'checksumok'}, "1", "incorrect GPRMC checksumok bit parsing");
        //ok($h{'timestamp'}, "1197471326", "incorrect GPRMC timestamp parsing");
        //
        //ok(sprintf('%.4f', $h{'latitude'}), "33.8173", "incorrect latitude parsing");
        //ok(sprintf('%.4f', $h{'longitude'}), "-84.1044", "incorrect longitude parsing");
        //ok(sprintf('%.4f', $h{'posresolution'}), "0.1852", "incorrect position resolution");
        //
        //# check for undefined value, when there is no such data in the packet
        //ok(sprintf('%.2f', $h{'speed'}), "43.94", "incorrect speed");
        //ok($h{'course'}, "28", "incorrect course");
        //ok($h{'altitude'}, undef, "incorrect altitude");
    }
}