﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsMsgPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;
        string srccall = "OH7AA-1";
        string destination = "OH7LZB   ";
        private string trimDest;
        string dstcall = "APRS";
        string message = "Testing, 1 2 3";
        object[] messageids = new object[] { 1, 42, 10512, 'a', "1Ff84", "F00b4" };

        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();
            trimDest = destination.Trim();
        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        public void CanNormalMessageBeDecoded()
        {
            foreach (var id in messageids)
            {
//                var aprspacketString = string.Format(@"{0}>{1},WIDE1-1,WIDE2-2,qAo,OH7AA::{2}:{3}\{{{4}", srccall, dstcall, destination, message, id);
                var aprspacketString = string.Format(@"{0}>{1},WIDE1-1,WIDE2-2,qAo,OH7AA::{2}:{3}", srccall, dstcall, destination, message);
//                aprspacketString += @"\{" + id;//was 
                //\{'
                //ok so in perl '\{' gives \{ but "\{" gives {
//                thus for the test the correct string conversion is "{" not @"\{"
                aprspacketString += @"{" + id;
                Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
                Assert.AreEqual(ErrorCode.NoError, packet.fap_error_code);
                Assert.AreEqual(PacketType.fapMESSAGE, packet.fap_packet_type);
                Assert.AreEqual(trimDest, packet.destination);
                Assert.AreEqual(id.ToString(), packet.message_id);
                Assert.AreEqual(message, packet.message);
            }
        }
        //	my $aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:$message\{$messageid";
        //	my %h;
        //	my $retval = parseaprs($aprspacket, \%h);
        //	
        //	# whitespace will be stripped, ok...
        //	$destination =~ s/\s+$//;
        //	
        //	ok($retval, 1, "failed to parse a message packet");
        //	ok($h{'resultcode'}, undef, "wrong result code");
        //	ok($h{'type'}, 'message', "wrong packet type");
        //	ok($h{'destination'}, $destination, "wrong message dst callsign");
        //	ok($h{'messageid'}, $messageid, "wrong message id");
        //	ok($h{'message'}, $message, "wrong message");
        [Test]
        public void CanAckMessageBeDecoded()
        {
            foreach (var id in messageids)
            {
                var aprspacketString = string.Format("{0}>{1},WIDE1-1,WIDE2-2,qAo,OH7AA::{2}:ack{3}", srccall, dstcall,
                                                     destination, id);
                Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
                Assert.AreEqual(ErrorCode.NoError, packet.fap_error_code);
                Assert.AreEqual(PacketType.fapMESSAGE, packet.fap_packet_type);
                Assert.AreEqual(trimDest, packet.destination);
                Assert.AreEqual(id.ToString(), packet.message_ack);
            }
        }
        //	# ack
        //	$destination = "OH7LZB   ";
        //	$aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:ack$messageid";
        //	$retval = parseaprs($aprspacket, \%h);
        //	$destination =~ s/\s+$//; # whitespace will be stripped, ok...
        //	
        //	ok($retval, 1, "failed to parse a message packet");
        //	ok($h{'resultcode'}, undef, "wrong result code");
        //	ok($h{'type'}, 'message', "wrong packet type");
        //	ok($h{'destination'}, $destination, "wrong message dst callsign");
        //	ok($h{'messageack'}, $messageid, "wrong message id in ack");
        [Test]
        public void CanRejectMessageBeDecoded()
        {
            foreach (var id in messageids)
            {
                var aprspacketString = string.Format("{0}>{1},WIDE1-1,WIDE2-2,qAo,OH7AA::{2}:rej{3}", srccall, dstcall,
                                                     destination, id);
                Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
                Assert.AreEqual(ErrorCode.NoError, packet.fap_error_code);
                Assert.AreEqual(PacketType.fapMESSAGE, packet.fap_packet_type);
                Assert.AreEqual(trimDest, packet.destination);
                Assert.AreEqual(id.ToString(), packet.message_rej);
            }
        }
        //	# reject
        //	$destination = "OH7LZB   ";
        //	$aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:rej$messageid";
        //	$retval = parseaprs($aprspacket, \%h);
        //	$destination =~ s/\s+$//; # whitespace will be stripped, ok...
        //	
        //	ok($retval, 1, "failed to parse a message packet");
        //	ok($h{'resultcode'}, undef, "wrong result code");
        //	ok($h{'type'}, 'message', "wrong packet type");
        //	ok($h{'destination'}, $destination, "wrong message dst callsign");
        //	ok($h{'messagerej'}, $messageid, "wrong message id in reject");

        //# message decoding
        //# Tue Dec 11 2007, Hessu, OH7LZB
        //
        //use Test;
        //
        //my @messageids = (1, 42, 10512, 'a', '1Ff84', 'F00b4');
        //
        //BEGIN { plan tests => 6 * (6 + 5 + 5)};
        //
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //
        //foreach my $messageid (@messageids) {
        //	my $srccall = "OH7AA-1";
        //	my $destination = "OH7LZB   ";
        //	my $dstcall = "APRS";
        //	my $message = "Testing, 1 2 3";
        //	
        //	my $aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:$message\{$messageid";
        //	my %h;
        //	my $retval = parseaprs($aprspacket, \%h);
        //	
        //	# whitespace will be stripped, ok...
        //	$destination =~ s/\s+$//;
        //	
        //	ok($retval, 1, "failed to parse a message packet");
        //	ok($h{'resultcode'}, undef, "wrong result code");
        //	ok($h{'type'}, 'message', "wrong packet type");
        //	ok($h{'destination'}, $destination, "wrong message dst callsign");
        //	ok($h{'messageid'}, $messageid, "wrong message id");
        //	ok($h{'message'}, $message, "wrong message");
        //	
        //	# ack
        //	$destination = "OH7LZB   ";
        //	$aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:ack$messageid";
        //	$retval = parseaprs($aprspacket, \%h);
        //	$destination =~ s/\s+$//; # whitespace will be stripped, ok...
        //	
        //	ok($retval, 1, "failed to parse a message packet");
        //	ok($h{'resultcode'}, undef, "wrong result code");
        //	ok($h{'type'}, 'message', "wrong packet type");
        //	ok($h{'destination'}, $destination, "wrong message dst callsign");
        //	ok($h{'messageack'}, $messageid, "wrong message id in ack");
        //	
        //	# reject
        //	$destination = "OH7LZB   ";
        //	$aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:rej$messageid";
        //	$retval = parseaprs($aprspacket, \%h);
        //	$destination =~ s/\s+$//; # whitespace will be stripped, ok...
        //	
        //	ok($retval, 1, "failed to parse a message packet");
        //	ok($h{'resultcode'}, undef, "wrong result code");
        //	ok($h{'type'}, 'message', "wrong packet type");
        //	ok($h{'destination'}, $destination, "wrong message dst callsign");
        //	ok($h{'messagerej'}, $messageid, "wrong message id in reject");
        //}
        //
        //

    }
}