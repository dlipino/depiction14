﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsObjectInvPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;

        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        
        [Test]
        public void AreBadObjectPacketsTreatedCorrectly()
        {
            var srccall = "OH2KKU-1";
            var dstcall = "APRS";
            var aprspacketString = string.Format("{0}>{1},TCPIP*,qAC,FIRST:;SRAL HQ *110507zS0%E/Th4_a AKaupinmaenpolku9,open M-Th12-17,F12-14 lcl",srccall,dstcall);
            Assert.IsFalse(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(ErrorCode.fapOBJ_INV,packet.fap_error_code);
            Assert.AreEqual(PacketType.fapOBJECT,packet.fap_packet_type);
        
        }
//      
//# object decoding - bad packet
//# the packet contains has some binary characters, which were destroyed in
//# a cut 'n paste operation
//# Tue Dec 11 2007, Hessu, OH7LZB
//
//use Test;
//
//BEGIN { plan tests => 3 };
//use Ham::APRS::FAP qw(parseaprs);
//
//my $srccall = "OH2KKU-1";
//my $dstcall = "APRS";
//my $aprspacket = "$srccall>$dstcall,TCPIP*,qAC,FIRST:;SRAL HQ *110507zS0%E/Th4_a AKaupinmaenpolku9,open M-Th12-17,F12-14 lcl";
//my %h;
//my $retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 0, "succeeded to parse a broken object packet");
//ok($h{'resultcode'}, 'obj_inv', "wrong result code");
//ok($h{'type'}, 'object', "wrong packet type");
//
   
    }
}