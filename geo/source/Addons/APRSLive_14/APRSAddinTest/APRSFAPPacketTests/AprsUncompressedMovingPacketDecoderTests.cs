﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsUncompressedMovingPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        private string comment = "RELAY,WIDE, OH2AP Jarvenpaa";
        string phg = "7220";
        string srccall = "OH7FDN";
        string dstcall = "APZMDR";
        APRSPacketFAPDetails packet;
        #region setup and teardown
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        public void CanABasicUncompressedPacketForMovingTargetBeDecoded()
        {
            var srccall = "OH7FDN";
            var dstcall = "APZMDR";
            var digiBase = new[] { "OH7AA-1", "WIDE2-1", "qAR", "OH7AA" };
            var digiBaseWasDigies = new[] { true, false,false,false };
            var header = string.Format("{0}>{1},{2}*,{3},{4},{5}",srccall,dstcall,digiBase[0],digiBase[1],digiBase[2],digiBase[3]);
            //# The comment field contains telemetry just to see that it doesn't break
            //# the actual position parsing.
            var body = "!6253.52N/02739.47E>036/010/A=000465 |!!!!!!!!!!!!!!|";
            var aprspacketString = string.Format("{0}:{1}",header,body);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet,aprspacketString,false));

            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);
            Assert.AreEqual(header, packet.header);
            Assert.AreEqual(body, packet.body);
            Assert.AreEqual(PacketType.fapLOCATION, packet.fap_packet_type);

            var digis = packet.digipeaters;
            Assert.AreEqual(4, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
            Assert.AreEqual('/', packet.symbol_table);
            Assert.AreEqual('>', packet.symbol_code);

            Assert.AreEqual(0, packet.pos_ambiguity);
            Assert.AreEqual(false, packet.messaging);
            
            //Check lat long speed, course
            Assert.AreEqual("62.8920", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("27.6578", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            //# check for undefined value, when there is no such data in the packet
            var exp = 18.52*MiniHelpers.KMH_TO_MPH;
            Assert.AreEqual(exp.ToString("0.00"), packet.speed.ToString("0.00"));
            Assert.AreEqual(36, packet.course);
            exp = 141.732/MiniHelpers.FT_TO_M;
            Assert.AreEqual(exp.ToString("0.00"), packet.altitude.ToString("0.00"));
        }
         
//# a basic uncompressed packet decoding test for a moving target
//# Tue Dec 11 2007, Hessu, OH7LZB
//
//use Test;
//
//BEGIN { plan tests => 25 };
//use Ham::APRS::FAP qw(parseaprs);
//
//my $srccall = "OH7FDN";
//my $dstcall = "APZMDR";
//my $header = "$srccall>$dstcall,OH7AA-1*,WIDE2-1,qAR,OH7AA";
//# The comment field contains telemetry just to see that it doesn't break
//# the actual position parsing.
//my $body = "!6253.52N/02739.47E>036/010/A=000465 |!!!!!!!!!!!!!!|";
//my $aprspacket = "$header:$body";
//my %h;
//my $retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse a moving target's uncompressed packet");
//ok($h{'srccallsign'}, $srccall, "incorrect source callsign parsing");
//ok($h{'dstcallsign'}, $dstcall, "incorrect destination callsign parsing");
//
//ok($h{'header'}, $header, "incorrect header parsing");
//ok($h{'body'}, $body, "incorrect body parsing");
//ok($h{'type'}, 'location', "incorrect packet type parsing");
//
//my @digis = @{ $h{'digipeaters'} };
//ok(${ $digis[0] }{'call'}, 'OH7AA-1', "Incorrect first digi parsing");
//ok(${ $digis[0] }{'wasdigied'}, '1', "Incorrect first digipeated bit parsing");
//ok(${ $digis[1] }{'call'}, 'WIDE2-1', "Incorrect second digi parsing");
//ok(${ $digis[1] }{'wasdigied'}, '0', "Incorrect second digipeated bit parsing");
//ok(${ $digis[2] }{'call'}, 'qAR', "Incorrect third digi parsing");
//ok(${ $digis[2] }{'wasdigied'}, '0', "Incorrect third digipeated bit parsing");
//ok(${ $digis[3] }{'call'}, 'OH7AA', "Incorrect igate call parsing");
//ok(${ $digis[3] }{'wasdigied'}, '0', "Incorrect igate digipeated bit parsing");
//ok($#digis, 3, "Incorrect amount of digipeaters parsed");
//
//ok($h{'symboltable'}, '/', "incorrect symboltable parsing");
//ok($h{'symbolcode'}, '>', "incorrect symbolcode parsing");
//
//ok($h{'posambiguity'}, '0', "incorrect posambiguity parsing");
//ok($h{'messaging'}, '0', "incorrect messaging bit parsing");
//
//ok(sprintf('%.4f', $h{'latitude'}), "62.8920", "incorrect latitude parsing");
//ok(sprintf('%.4f', $h{'longitude'}), "27.6578", "incorrect longitude parsing");
//ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
//
//ok(sprintf('%.2f', $h{'speed'}), "18.52", "incorrect speed");
//ok(sprintf('%.0f', $h{'course'}), "36", "incorrect course");
//ok(sprintf('%.3f', $h{'altitude'}), "141.732", "incorrect altitude");


    }
}