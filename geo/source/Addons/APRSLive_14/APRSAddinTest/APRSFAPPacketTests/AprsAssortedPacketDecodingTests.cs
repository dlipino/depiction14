﻿using System.Text.RegularExpressions;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.APRSFAPPacketTests
{
    [TestFixture]
    public class AprsAssortedPacketDecodingTests
    {
        [Test]
        public void Check_ax25CallFunction()
        {
            //ok(check_ax25_call('OH7LZB'), 'OH7LZB', 'Failed to check a callsign without SSID');
            var callSign = "OH7LZB";
            var parsedSign = MiniHelpers.fap_check_ax25_call(callSign);
            Assert.AreEqual(callSign,parsedSign);
            //ok(check_ax25_call('OH7LZB-9'), 'OH7LZB-9', 'Failed to check a callsign with SSID -9');
            callSign = "OH7LZB-9";
            parsedSign = MiniHelpers.fap_check_ax25_call(callSign);
            Assert.AreEqual(callSign, parsedSign);
            //ok(check_ax25_call('OH7LZB-15'), 'OH7LZB-15', 'Failed to check a callsign with SSID -15');
            callSign = "OH7LZB-15";
            parsedSign = MiniHelpers.fap_check_ax25_call(callSign);
            Assert.AreEqual(callSign, parsedSign);
            //
            //ok(check_ax25_call('OH7LZB-16'), undef, 'Failed to check a callsign with SSID -16, should return undef');
            callSign = "OH7LZB-16";
            parsedSign = MiniHelpers.fap_check_ax25_call(callSign);
            Assert.IsNull(parsedSign);
            //ok(check_ax25_call('OH7LZB-166'), undef, 'Failed to check a callsign with SSID -166, should return undef');
            callSign = "OH7LZB-166";
            parsedSign = MiniHelpers.fap_check_ax25_call(callSign);
            Assert.IsNull(parsedSign);
            //ok(check_ax25_call('OH7LZBXXX'), undef, 'Failed to check an invalid callsign, should return undef');
            callSign = "OH7LZBXXX";
            parsedSign = MiniHelpers.fap_check_ax25_call(callSign);
            Assert.IsNull(parsedSign);
        }
        //BEGIN { plan tests => 6 };
        //use Ham::APRS::FAP qw(check_ax25_call);
        //
        //ok(check_ax25_call('OH7LZB'), 'OH7LZB', 'Failed to check a callsign without SSID');
        //ok(check_ax25_call('OH7LZB-9'), 'OH7LZB-9', 'Failed to check a callsign with SSID -9');
        //ok(check_ax25_call('OH7LZB-15'), 'OH7LZB-15', 'Failed to check a callsign with SSID -15');
        //
        //ok(check_ax25_call('OH7LZB-16'), undef, 'Failed to check a callsign with SSID -16, should return undef');
        //ok(check_ax25_call('OH7LZB-166'), undef, 'Failed to check a callsign with SSID -166, should return undef');
        //ok(check_ax25_call('OH7LZBXXX'), undef, 'Failed to check an invalid callsign, should return undef');
        //
        [Test]
        public void CheckAprsISPathSpecialCases()
        {
            var ip6Addresss = "200106F8020204020000000000000002";
             var aprspacketstring = string.Format("IQ3VQ>APD225,TCPIP*,qAI,IQ3VQ,THIRD,92E5A2B6,T2HUB1,{0},T2FINLAND:!4526.66NI01104.68E#PHG21306/- Lnx APRS Srv - sez. ARI VR EST",ip6Addresss);
            var parser = new FAP_APRSPacketParser();
            APRSPacketFAPDetails packet = null;
            var result = parser.FAP_APRSParser(ref packet,aprspacketstring, false);
            Assert.IsTrue(result);
            Assert.AreEqual(ip6Addresss,packet.digipeaters[6].call);
            //And one that should fail
            aprspacketstring = string.Format("IQ3VQ>APD225,{0},TCPIP*,qAI,IQ3VQ,THIRD,92E5A2B6,T2HUB1,{0},T2FINLAND:!4526.66NI01104.68E#PHG21306/- Lnx APRS Srv - sez. ARI VR EST",ip6Addresss);
            Assert.IsFalse(parser.FAP_APRSParser(ref packet, aprspacketstring, false));
        }
        //my $aprspacket = "IQ3VQ>APD225,TCPIP*,qAI,IQ3VQ,THIRD,92E5A2B6,T2HUB1,200106F8020204020000000000000002,T2FINLAND:!4526.66NI01104.68E#PHG21306/- Lnx APRS Srv - sez. ARI VR EST";
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse a packet with an IPv6 address in the path");
        //ok($h{'digipeaters'}->[6]->{'call'}, '200106F8020204020000000000000002', "wrong IPv6 address parsed from qAI trace path");
        //
        //$aprspacket = "IQ3VQ>APD225,200106F8020204020000000000000002,TCPIP*,qAI,IQ3VQ,THIRD,92E5A2B6,T2HUB1,200106F8020204020000000000000002,T2FINLAND:!4526.66NI01104.68E#PHG21306/- Lnx APRS Srv - sez. ARI VR EST";
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 0, "managed to parse a packet with an IPv6 address in the path before qAI");
        //

        [Test]
        [Ignore("Timestamp stuff is not solid enough for this test yet")]
        public void StatusMessageTimestampNotAffectedByRawTimestampFlag()
        {

        }//# the Status message's timestamp is not affected by the raw_timestamp flag.
        //my $now = time();
        //my @gm = gmtime($now);
        //my $mday = $gm[3];
        //my $tstamp = sprintf('%02d%02d%02dz', $gm[3], $gm[2], $gm[1]);
        //my $outcome = $now - ($now % 60); # will round down to the minute
        //
        //my $msg = '>>Nashville,TN>>Toronto,ON';
        //
        //$aprspacket = 'KB3HVP-14>APU25N,WIDE2-2,qAR,LANSNG:>' . $tstamp . $msg;
        //$retval = parseaprs($aprspacket, \%h, 'raw_timestamp' => 1);
        //ok($retval, 1, "failed to parse a status message packet");
        //ok($h{'type'}, 'status', "the type of a status message packet is wrong");
        //ok($h{'timestamp'}, $outcome, "the timestamp was miscalculated from the status message packet");
        //ok($h{'status'}, $msg, "the status message was not parsed correctly from a status packet");


    }

    //# test status messag


    //# APRS-IS path special cases
    //# Fri Sep 10 2010, Hessu, OH7LZB
    //
    //use Test;
    //
    //BEGIN { plan tests => 3 };
    //use Ham::APRS::FAP qw(parseaprs);
    //use Data::Dumper;
    //
    
  
    //    # validate the check_ax25_call function
    //
    //use Test;
    //
    


    //# test status message decoding
    //
    //use Test;
    //use Data::Dumper;
    //
    //BEGIN { plan tests => 4 };
    //use Ham::APRS::FAP qw(parseaprs);
    //
    //my($aprspacket, $tlm);
    //my %h;
    //my $retval;
    //
    
}