﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsTlmMicePacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;
        private string srccall;
        private string dstcall;
        private string header;
        private string body;
        private string comment;
        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();
            srccall = "OH7LZB-13";
            dstcall = "SX15S6";
            header = string.Format("{0}>{1},TCPIP*,qAC,FOURTH", srccall, dstcall);
            body = "'I',l \x1C>/";
            comment = "comment";
        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        public void CanSequence005ChannelsAndOneChannelOfBinaryBitsBeDecoded()
        {
            //# sequence 00, 5 channels of telemetry and one channel of binary bits
            var tlm = "|!!!!!!!!!!!!!!|";
            var aprspacketString = string.Format("{0}:{1} {2} {3}", header, body, comment, tlm);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(comment, packet.comment);
            var expectedVals = new[] { "0", "0", "0", "0", "0" };
            var telem = packet.telemetry;

            Assert.AreEqual(0, telem.seq);
            for (int i = 0; i < expectedVals.Length; i++)
            {
                Assert.AreEqual(expectedVals[i], telem.values[i].ToString("0"));
            }
        }
        [Test]
        public void CanSequence00AndOneChannelOfTelemetyBeDecoded()
        { //# sequence 00, 1 channel of telemetry
            var tlm = "|!!!!|";
            var aprspacketString = string.Format("{0}:{1} {2} {3}", header, body, comment, tlm);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(comment, packet.comment);
            var expectedVals = new[] { 0.0, double.NaN, double.NaN, double.NaN, double.NaN };
            var telem = packet.telemetry;
            Assert.AreEqual(0, telem.seq);
            Assert.AreEqual("00000000",telem.bits);

            for (int i = 0; i < expectedVals.Length; i++)
            {
                Assert.AreEqual(expectedVals[i], telem.values[i]);
            }
        }
        [Test]
        [Ignore("I think this test (from port) is missing something")]
        public void AHarderOne()
        {
            //not entirely sure how this is supposed to work
            //"N6BG-1>S6QTUX:`+,^l!cR/'\";z}||!\$:'e'b|!w>f!|3"
            var aprspacketString = "N6BG-1>S6QTUX" + ":`+,^l!cR/'\"" + ";z}||!" + "\\$" + ":'e'b|!w>f!|3";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(comment, packet.comment);
        }
        //# test mic-e telemetry decoding
        //
        //use Test;
        //
        //BEGIN { plan tests => 9 + 8 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "OH7LZB-13";
        //my $dstcall = "SX15S6";
        //my $header = "$srccall>$dstcall,TCPIP*,qAC,FOURTH";
        //my $body = "'I',l \x1C>/";
        //my $comment = "comment";
        //
        //my($aprspacket, $tlm);
        //my %h;
        //my $retval;
        //
        //# The new mic-e telemetry format:
        //# }ss112233445566}
        //#
        //
        //# sequence 00, 5 channels of telemetry and one channel of binary bits
        //$tlm = '|!!!!!!!!!!!!!!|';
        //
        //$aprspacket = "$header:$body $comment $tlm";
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse mic-e packet with 5-ch telemetry");
        //ok($h{'comment'}, $comment, "incorrect comment parsing");
        //
        //ok($h{'telemetry'}{'seq'}, 0, "wrong sequence parsed from telemetry");
        //
        //my(@v) = @{ $h{'telemetry'}{'vals'} };
        //ok($v[0], "0", "wrong value 0 parsed from telemetry");
        //ok($v[1], "0", "wrong value 1 parsed from telemetry");
        //ok($v[2], "0", "wrong value 2 parsed from telemetry");
        //ok($v[3], "0", "wrong value 3 parsed from telemetry");
        //ok($v[4], "0", "wrong value 4 parsed from telemetry");
        //
        //ok($h{'telemetry'}{'bits'}, '00000000', "wrong bits parsed from telemetry");
        //
        //# sequence 00, 1 channel of telemetry
        //$tlm = '|!!!!|';
        //
        //$aprspacket = "$header:$body $comment $tlm";
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse mic-e packet with 5-ch telemetry");
        //ok($h{'comment'}, $comment, "incorrect comment parsing");
        //
        //ok($h{'telemetry'}{'seq'}, 0, "wrong sequence parsed from telemetry");
        //
        //@v = @{ $h{'telemetry'}{'vals'} };
        //ok($v[0], "0", "wrong value 0 parsed from telemetry");
        //ok($v[1], undef, "wrong value 1 parsed from telemetry");
        //ok($v[2], undef, "wrong value 2 parsed from telemetry");
        //ok($v[3], undef, "wrong value 3 parsed from telemetry");
        //ok($v[4], undef, "wrong value 4 parsed from telemetry");
        //
        //## harder one:
        //#$aprspacket = "N6BG-1>S6QTUX:`+,^l!cR/'\";z}||!\$:'e'b|!w>f!|3";
        //#$retval = parseaprs($aprspacket, \%h);
        //
        //#ok($retval, 1, "failed to parse mic-e packet with 5-ch telemetry");
        //#ok($h{'comment'}, $comment, "incorrect comment parsing");

    }
}