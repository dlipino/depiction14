﻿using System;
using System.Text;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsObjectPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;
        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();
        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion


        [Test]
        public void AreBasicPacketsDecoded()
        {
            var srccall = "OH2KKU-1";
            var dstcall = "APRS";
            var comment = "Kaupinmaenpolku9,open M-Th12-17,F12-14 lcl";
            var toPack =
                "415052532C54435049502A2C7141432C46495253543A3B5352414C20485120202A3130303932377A533025452F5468345F612020414B617570696E6D61656E706F6C6B75392C6F70656E204D2D546831322D31372C4631322D3134206C636C";
            var packedStuff = MiniHelpers.PerlPackH(toPack);
            // var aprspacketString = string.Format("$srccall>" . pack('H*', "415052532C54435049502A2C7141432C46495253543A3B5352414C20485120202A3130303932377A533025452F5468345F612020414B617570696E6D61656E706F6C6B75392C6F70656E204D2D546831322D31372C4631322D3134206C636C"));

            var aprspacketString = string.Format("{0}>{1}", srccall, packedStuff);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(ErrorCode.NoError,packet.fap_error_code);
            Assert.AreEqual(PacketType.fapOBJECT,packet.fap_packet_type);

            Assert.AreEqual("SRAL HQ  ",packet.object_or_item_name);
            Assert.IsTrue(packet.alive);
            //# timestamp test has been disabled, because it cannot be
            //# done this way - the timestamp in the packet is not
            //# fully specified, so the resulting value will depend
            //# on the time the parsing is executed.
            //The original date is not going to work
            //"1197278820"
//           100927 is the number in the packet, not sure how the rest gets in there though
           // Assert.AreEqual(,packet.timestamp);

            Assert.AreEqual('S', packet.symbol_table);
            Assert.AreEqual('a', packet.symbol_code);

            Assert.AreEqual("60.2305", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("24.8790", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("0.291", packet.pos_resolution.ToString("0.000"));

            Assert.AreEqual(comment,packet.comment);
            Assert.IsNull(packet.phg);
        }
        //# object decoding
        //# Tue Dec 11 2007, Hessu, OH7LZB
        //
        //use Test;
        //
        //BEGIN { plan tests => 12 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "OH2KKU-1";
        //my $dstcall = "APRS";
        //my $comment = "Kaupinmaenpolku9,open M-Th12-17,F12-14 lcl";
        //my $aprspacket = "$srccall>" . pack('H*', "415052532C54435049502A2C7141432C46495253543A3B5352414C20485120202A3130303932377A533025452F5468345F612020414B617570696E6D61656E706F6C6B75392C6F70656E204D2D546831322D31372C4631322D3134206C636C");
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse an object packet");
        //ok($h{'resultcode'}, undef, "wrong result code");
        //ok($h{'type'}, 'object', "wrong packet type");
        //
        //ok($h{'objectname'}, 'SRAL HQ  ', "wrong object name");
        //ok($h{'alive'}, 1, "wrong alive bit");
        //
        //# timestamp test has been disabled, because it cannot be
        //# done this way - the timestamp in the packet is not
        //# fully specified, so the resulting value will depend
        //# on the time the parsing is executed.
        //# ok($h{'timestamp'}, 1197278820, "wrong timestamp");
        //
        //ok($h{'symboltable'}, 'S', "incorrect symboltable parsing");
        //ok($h{'symbolcode'}, 'a', "incorrect symbolcode parsing");
        //
        //ok(sprintf('%.4f', $h{'latitude'}), "60.2305", "incorrect latitude parsing (northern)");
        //ok(sprintf('%.4f', $h{'longitude'}), "24.8790", "incorrect longitude parsing (eastern)");
        //ok(sprintf('%.3f', $h{'posresolution'}), "0.291", "incorrect position resolution");
        //ok($h{'phg'}, undef, "incorrect PHG parsing");
        //ok($h{'comment'}, $comment, "incorrect comment parsing");
        //

    }
}