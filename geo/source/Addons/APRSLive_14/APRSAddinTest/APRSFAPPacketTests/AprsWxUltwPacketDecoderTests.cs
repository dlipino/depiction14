﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsWxUltwPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSFapWeatherInfo weatherInfo;
        APRSPacketFAPDetails packet;
        private double exp = double.NaN;
        #region setup and teardown
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion

        [Test]
        public void CanUltWWXPacketBeDecoded()
        {
            //# first, the $ULTW format
            //
            var aprspacketString = "WC4PEM-14>APN391,WIDE2-1,qAo,K2KZ-3:$ULTW0053002D028D02FA2813000D87BD000103E8015703430010000C";

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            weatherInfo = packet.wx_report;

            Assert.AreEqual("64", weatherInfo.WindDirection.ToString("0"));
            exp = 64;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.WindDirection, 0);

//            Assert.AreEqual("0.3", weatherInfo.WindSpeed.ToString("0.0"));
            exp = 0.3 / MiniHelpers.MPH_TO_MS;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.WindSpeed, 0);

//            Assert.AreEqual("2.3", weatherInfo.WindGust.ToString("0.0"));
            exp = 2.3 / MiniHelpers.MPH_TO_MS;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.WindGust, 0);

//            Assert.AreEqual("18.5", weatherInfo.Temperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(18.5);
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Temperature, 1);

            Assert.AreEqual(100, weatherInfo.Humidity);
            exp = 100;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Humidity, 0);
//            Assert.AreEqual("1025.9", weatherInfo.Pressure.ToString("0.0"));
            exp = 1025.9;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Pressure, 1);

            exp = 7.6;
//            Assert.AreEqual(double.NaN, weatherInfo.RainLast24Hours);
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.RainLast24Hours, 1);

            Assert.AreEqual(double.NaN, weatherInfo.RainLastHour);
//            Assert.AreEqual("4.1", weatherInfo.RainSinceMidnight.ToString("0.0"));
            exp = (4.1/MiniHelpers.HINCH_TO_MM)/100d;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.RainSinceMidnight, 1);

            Assert.AreEqual(null, weatherInfo.SoftwareIndicator);
        }

        [Test]
        public void CanUltWWXPacketWithTempBelow0FBeDecoded()
        {
            var aprspacketString = "SR3DGT>APN391,SQ2LYH-14,SR4DOS,WIDE2*,qAo,SR4NWO-1:$ULTW00000000FFEA0000296F000A9663000103E80016025D";

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            weatherInfo = packet.wx_report;

            Assert.AreEqual(0, weatherInfo.WindDirection);
            Assert.AreEqual(double.NaN, weatherInfo.WindSpeed);
//            Assert.AreEqual("0.0", weatherInfo.WindGust.ToString("0.0"));
            exp = 0.0 / MiniHelpers.MPH_TO_MS;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.WindGust, 0);

//            Assert.AreEqual("-19.0", weatherInfo.Temperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(-19.0);
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Temperature, 1);

//            Assert.AreEqual(100, weatherInfo.Humidity);
            exp = 100;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Humidity, 0);

//            Assert.AreEqual("1060.7", weatherInfo.Pressure.ToString("0.0"));
            exp = 1060.7;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Pressure, 1);
            exp = 0.0;
            Assert.AreEqual(exp, weatherInfo.RainLast24Hours);
            Assert.AreEqual(double.NaN, weatherInfo.RainLastHour);
            Assert.AreEqual(double.NaN, weatherInfo.RainSinceMidnight);
//            Assert.AreEqual("0.0", weatherInfo.RainSinceMidnight.ToString("0.0"));

            Assert.AreEqual(null, weatherInfo.SoftwareIndicator);
        }

        [Test]
        public void CanUltWWXPacketWithLoggingDecoded()
        {
            var aprspacketString = "MB7DS>APRS,TCPIP*,qAC,APRSUK2:!!00000066013D000028710166--------0158053201200210";

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            weatherInfo = packet.wx_report;

//            Assert.AreEqual("144", weatherInfo.WindDirection.ToString("0"));
            exp = 144;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.WindDirection, 0);

//            Assert.AreEqual("14.7", weatherInfo.WindSpeed.ToString("0.0"));
            exp = 14.7 / MiniHelpers.MPH_TO_MS;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.WindSpeed, 0);

            Assert.AreEqual(double.NaN, weatherInfo.WindGust);

//            Assert.AreEqual("-0.2", weatherInfo.Temperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(-0.2);
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Temperature, 0);

//            Assert.AreEqual("2.1", weatherInfo.IndoorTemperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(2.1);
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.IndoorTemperature, 1);

            Assert.AreEqual(double.NaN, weatherInfo.Humidity);
//            Assert.AreEqual("1035.3", weatherInfo.Pressure.ToString("0.0"));
            exp = 1035.3;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.Pressure, 1);

            Assert.AreEqual(0, weatherInfo.RainLast24Hours);
            Assert.AreEqual(double.NaN, weatherInfo.RainLastHour);
//            Assert.AreEqual("73.2", weatherInfo.RainSinceMidnight.ToString("0.0"));
            exp = (73.2/MiniHelpers.HINCH_TO_MM)/100d;
            AprsWxBasicPacketDecoderTests.AssertDoubleEquality(exp, weatherInfo.RainSinceMidnight, 2);

            Assert.AreEqual(null, weatherInfo.SoftwareIndicator);
        }
    }
}
  
//# an ultw wx packet decoding test
//# Tue Dec 11 2007, Hessu, OH7LZB
//
//use Test;
//
//BEGIN { plan tests => 23 + 11 };
//use Ham::APRS::FAP qw(parseaprs);
//
//# first, the $ULTW format
//
//my $aprspacket = 'WC4PEM-14>APN391,WIDE2-1,qAo,K2KZ-3:$ULTW0053002D028D02FA2813000D87BD000103E8015703430010000C';
//my %h;
//my $retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse an ULTW wx packet");
//
//ok($h{'wx'}->{'wind_direction'}, 64, "incorrect wind direction parsing");
//ok($h{'wx'}->{'wind_speed'}, "0.3", "incorrect wind speed parsing");
//ok($h{'wx'}->{'wind_gust'}, "2.3", "incorrect wind gust parsing");
//
//ok($h{'wx'}->{'temp'}, "18.5", "incorrect temperature parsing");
//ok($h{'wx'}->{'humidity'}, 100, "incorrect humidity parsing");
//ok($h{'wx'}->{'pressure'}, "1025.9", "incorrect pressure parsing");
//
//ok($h{'wx'}->{'rain_24h'}, undef, "incorrect rain_24h parsing");
//ok($h{'wx'}->{'rain_1h'}, undef, "incorrect rain_1h parsing");
//ok($h{'wx'}->{'rain_midnight'}, "4.1", "incorrect rain_midnight parsing");
//
//ok($h{'wx'}->{'soft'}, undef, "incorrect wx software id");
//
       

//# another, with a temperature below 0F
//
//$aprspacket = 'SR3DGT>APN391,SQ2LYH-14,SR4DOS,WIDE2*,qAo,SR4NWO-1:$ULTW00000000FFEA0000296F000A9663000103E80016025D';
//%h = ();
//$retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse an ULTW wx packet");
//
//ok($h{'wx'}->{'wind_direction'}, 0, "incorrect wind direction parsing");
//ok($h{'wx'}->{'wind_speed'}, undef, "incorrect wind speed parsing");
//ok($h{'wx'}->{'wind_gust'}, "0.0", "incorrect wind gust parsing");
//
//ok($h{'wx'}->{'temp'}, "-19.0", "incorrect temperature parsing");
//ok($h{'wx'}->{'humidity'}, 100, "incorrect humidity parsing");
//ok($h{'wx'}->{'pressure'}, "1060.7", "incorrect pressure parsing");
//
//ok($h{'wx'}->{'rain_24h'}, undef, "incorrect rain_24h parsing");
//ok($h{'wx'}->{'rain_1h'}, undef, "incorrect rain_1h parsing");
//ok($h{'wx'}->{'rain_midnight'}, "0.0", "incorrect rain_midnight parsing");
//
//ok($h{'wx'}->{'soft'}, undef, "incorrect wx software id");
//
    
//# then, the !!... logging format
//
//$aprspacket = 'MB7DS>APRS,TCPIP*,qAC,APRSUK2:!!00000066013D000028710166--------0158053201200210';
//%h = ();
//$retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse an ULTW wx packet");
//
//ok($h{'wx'}->{'wind_direction'}, 144, "incorrect wind direction parsing");
//ok($h{'wx'}->{'wind_speed'}, "14.7", "incorrect wind speed parsing");
//ok($h{'wx'}->{'wind_gust'}, undef, "incorrect wind gust parsing");
//
//ok($h{'wx'}->{'temp'}, "-0.2", "incorrect temperature parsing");
//ok($h{'wx'}->{'temp_in'}, "2.1", "incorrect indoor temperature parsing");
//ok($h{'wx'}->{'humidity'}, undef, "incorrect humidity parsing");
//ok($h{'wx'}->{'pressure'}, "1035.3", "incorrect pressure parsing");
//
//ok($h{'wx'}->{'rain_24h'}, undef, "incorrect rain_24h parsing");
//ok($h{'wx'}->{'rain_1h'}, undef, "incorrect rain_1h parsing");
//ok($h{'wx'}->{'rain_midnight'}, "73.2", "incorrect rain_midnight parsing");
//
//ok($h{'wx'}->{'soft'}, undef, "incorrect wx software id");


