﻿using System.Text.RegularExpressions;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.APRSFAPPacketTests
{
    [TestFixture]
    public class AprsTlmPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;

        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        [Ignore("This is only a helper test")]
        public void FindTelemetryPacketCorrectly()
        {
            var thing = "T#324,000,038,257,255,50.12,01000001";
            var result = Regex.IsMatch(thing, @"^T#(.*?),(.*)$");
        }

        [Test]
        public void CanPacketWithTelemetryBeDecoded()
        {
            var srccall = "SRCCALL";
            var dstcall = "APRS";
            var aprspacketString = string.Format("{0}>{1}:T#324,000,038,257,255,50.12,01000001", srccall, dstcall);

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(ErrorCode.NoError, packet.fap_error_code);
            
            Assert.IsNotNull(packet.telemetry);
            var telemetry = packet.telemetry;
            Assert.AreEqual(324, telemetry.seq);
            Assert.AreEqual("01000001", new string(telemetry.bits));
            Assert.IsNotNull(telemetry.values);

            var expectedVals = new[] {"0.00", "38.00", "257.00", "255.00", "50.12"};
            for(int i =0;i<expectedVals.Length;i++)
            {
                Assert.AreEqual(expectedVals[i],telemetry.values[i].ToString("0.00"));
            }

        }
        //# telemetry decoding
        //# Wed Mar 12 16:22:53 EET 2008
        //
        //use Test;
        //
        //BEGIN { plan tests => 11 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "SRCCALL";
        //my $dstcall = "APRS";
        //my $aprspacket = "$srccall>$dstcall:T#324,000,038,257,255,50.12,01000001";
        //
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse a telemetry packet");
        //ok($h{'resultcode'}, undef, "wrong result code");
        //
        //ok(defined $h{'telemetry'}, 1, "no telemetry data in rethash");
        //
        //my %t = %{ $h{'telemetry'} };
        //
        //ok($t{'seq'}, 324, "wrong sequence number parsed from telemetry");
        //ok($t{'bits'}, '01000001', "wrong bits parsed from telemetry");
        //ok(defined $t{'vals'}, 1, "no value array parsed from telemetry");
        //
        //my(@v) = @{ $t{'vals'} };
        //ok($v[0], "0.00", "wrong value 0 parsed from telemetry");
        //ok($v[1], "38.00", "wrong value 1 parsed from telemetry");
        //ok($v[2], "257.00", "wrong value 2 parsed from telemetry");
        //ok($v[3], "255.00", "wrong value 3 parsed from telemetry");
        //ok($v[4], "50.12", "wrong value 4 parsed from telemetry");


    }
}