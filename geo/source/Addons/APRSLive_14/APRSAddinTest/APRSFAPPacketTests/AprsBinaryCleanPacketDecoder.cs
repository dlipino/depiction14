﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    //#
    //# Test all binary characters (except \r\n) in:
    //# 1) message content
    //# 2) bulletin content
    //# 3) status message content
    //# 4) comment string
    //# 5) some other beacon packet
    //#
    //# The test definition in the beginning skips ascii 127 (delete character)
    //# and 255 (0xFF), which is invalid: not defined by UTF-8 specification.
    //# Other chars starting for 32 seem to go through fine.
    [TestFixture]

    public class AprsBinaryCleanPacketDecoder
    {
        [Test]
        public void DoesPacketDecoderDealWithBinary()
        {
            var ascii = string.Empty;
            for(int i =32;i<=126;i++)
            {
                ascii += (char) i;
            }
            string binary1 = string.Empty, binary2 = string.Empty, 
                binary3 = string.Empty, binary4 = string.Empty;
            for (int i = 32; i < 32+67; i++) {binary1 += (char)(i); }
            for (int i  = 32; i < 32+67; i++) { binary2 += (char)(i); }
            for (int i  = 32; i < 32+67; i++) { binary3 += (char)(i); }
            for (int i = 32; i < 32 + 67; i++) { binary4 += (char)(i); }

            var binaryContent = new List<string>{ascii,binary1,binary2,binary3,binary4};
            var binaryChars = new List<char>();

            for (int i = 32; i < 255; i++)
            {
                if(i!=127) binaryChars.Add((char)i);
            }
            foreach(var binary in binaryContent)
            {
                DoTheBinaryTest(binary);
            }
            foreach(var binary in binaryChars)
            {
                DoTheBinaryTest(binary.ToString());
            }
        }

        public void DoTheBinaryTest(string binary)
        {
            var srccall = "OH7AA-1";
            var destination = "OH7LZB   ";
            var dstcall = "APRS";
            var message = binary;
            var messageid = "42";

//            $message =~ s/[{~|]//g;
            //Remove chars that are not allowed
            message = Regex.Replace(message,@"[{~|]", "");
            var packetString = string.Format("{0}>{1},WIDE1-1,WIDE2-2,qAo,OH7AA::{2}:{3}", srccall, dstcall, destination,
                                           message);
            packetString += "{" + messageid;
            APRSPacketFAPDetails packet = null;
            var parser = new FAP_APRSPacketParser();
//            # whitespace will be stripped, ok...
//	$destination =~ s/\s+$//
            destination = Regex.Replace(destination, @"\s+$", "");
            Assert.IsTrue(parser.FAP_APRSParser(ref packet, packetString,false,false));
            Assert.AreEqual(ErrorCode.NoError,packet.fap_error_code);
            Assert.AreEqual(PacketType.fapMESSAGE,packet.fap_packet_type);
            Assert.AreEqual(destination,packet.destination);
            Assert.AreEqual(messageid,packet.message_id);
            Assert.AreEqual(message,packet.message);

        }
//#
//

//#
//
//use Test;
//
//my $ascii = '';
//for (my $i = 32; $i <= 126; $i++) { $ascii .= chr($i); }
//my $binary1 = '';
//my $binary2 = '';
//my $binary3 = '';
//my $binary4 = '';
//for (my $i = 32; $i < 32+67; $i++) { $binary1 .= chr($i); }
//for (my $i = 32+67; $i < 32+67+67; $i++) { $binary2 .= chr($i) if ($i != 127); }
//for (my $i = 32+67+67; $i < 32+67+67+67; $i++) { $binary3 .= chr($i); }
//for (my $i = 32+67+67+67; $i < 255; $i++) { $binary4 .= chr($i); }
//
//my @binarycontents = ( $ascii, $binary1, $binary2, $binary3, $binary4 );
//
//my @binarychars;
//for (my $i = 32; $i < 255; $i++) { push @binarychars, chr($i) if ($i != 127); }
//
//# 5 content sets, 6 message tests
//BEGIN { plan tests => (5 + (255-32-1)) * (6) };
//
//use Ham::APRS::FAP qw(parseaprs);
//
//sub do_tests($$)
//{
//	my($setname, $binary) = @_;
//	
//	my $srccall = "OH7AA-1";
//	my $destination = "OH7LZB   ";
//	my $dstcall = "APRS";
//	my $message = $binary;
//	my $messageid = 42;
//	
//	# these characters are not allowed in a message, so strip them:
//	$message =~ s/[{~|]//g;
//	
//	my $aprspacket = "$srccall>$dstcall,WIDE1-1,WIDE2-2,qAo,OH7AA::$destination:$message\{$messageid";
//	my %h;
//	my $retval = parseaprs($aprspacket, \%h);
//	
//	# whitespace will be stripped, ok...
//	$destination =~ s/\s+$//;
//	
//	ok($retval, 1, "$setname: failed to parse a message packet");
//	ok($h{'resultcode'}, undef, "$setname: wrong result code");
//	ok($h{'type'}, 'message', "$setname: wrong packet type");
//	ok($h{'destination'}, $destination, "$setname: wrong message dst callsign");
//	ok($h{'messageid'}, $messageid, "$setname: wrong message id");
//	ok($h{'message'}, $message, "$setname: wrong message");
//}
//
//my $set = 0;
//foreach my $binary (@binarycontents) {
//	$set++;
//	
//	do_tests("msg set $set", $binary);
//}
//
//
//foreach my $binary (@binarychars) {
//	do_tests("binary char " . ord($binary), "char: $binary");
//}
//

    }
}