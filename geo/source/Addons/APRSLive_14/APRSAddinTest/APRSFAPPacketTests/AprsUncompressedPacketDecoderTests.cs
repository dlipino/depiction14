﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsUncompressedPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        private string comment = "RELAY,WIDE, OH2AP Jarvenpaa";
        string phg = "7220";
        string srccall = "OH2RDP-1";
        string dstcall = "BEACON-15";
        APRSPacketFAPDetails packet;
        #region setup and teardown
        [SetUp]
        protected void Setup()
        {
            packet = null;
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
            packet = null;
        }
        #endregion

        #region Helper tests
        [Test]
        public void DangRegex()
        {
            var toParse = "6028.51N/02505.68E#PHG7220/RELAY,WIDE, OH2AP Jarvenpaa";
            //^(\d{2})([0-7 ][0-9 ]\.[0-9 ]{2})([NnSs])(.)(\d{3})([0-7 ][0-9 ]\.[0-9 ]{2})([EeWw])([\x21-\x7b\x7d])/o
            // from c port "^([0-9]{2})([0-7 ][0-9 ]\\.[0-9 ]{2})([NnSs])(.)([0-9]{3})([0-7 ][0-9 ]\\.[0-9 ]{2})([EeWw])(.)";
            string pattern = @"^(\d{2})([0-7 ][0-9 ]\.[0-9 ]{2})([NnSs])(.)(\d{3})([0-7 ][0-9 ]\.[0-9 ]{2})([EeWw])(?[\x21-\x7b\x7d])";

            var groups = Regex.Match(toParse, APRSRegexConstants.fapint_regex_normalpos).Groups;
            var results = new List<string>();
            for (int i = 0; i < groups.Count; i++)// (var group in groups)
            {
                results.Add(groups[i].Value);
            }
        }
        #endregion

        [Test]
        public void CanBasicUncompressedPacketBeDecoded()
        {
            var aprspacketString = "YC0SHR>APU25N,TCPIP*,qAC,ALDIMORI:=0606.23S/10644.61E-GW SAHARA PENJARINGAN JAKARTA 147.880 MHz";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual("-6.10383", packet.latitude.ToString("0.00000"));
            Assert.AreEqual("106.74350", packet.longitude.ToString("0.00000"));
        }
        [Test]
        public void CanBasicUncompressedNortheastPacketBeDecoded()
        {
            var digiBase = new[] { "OH2RDG", "WIDE" };
            var digiBaseWasDigies = new[] { true, false };
            var aprspacketString = string.Format("{0}>{1},{4}*,{5}:!6028.51N/02505.68E#PHG{2}/{3}", srccall, dstcall, phg, comment, digiBase[0], digiBase[1]);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(PositionFormat.fapPOS_UNCOMPRESSED, packet.fap_pos_format);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);

            //And now check for position
            Assert.AreEqual("60.4752", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("25.0947", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            Assert.AreEqual(phg, packet.phg);
            Assert.AreEqual(comment, packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(2, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
        }
        [Test]
        public void CanBasicUncompressedSouthwesterntPacketBeDecoded()
        {
            var digiBase = new[] { "OH2RDG", "WIDE" };
            var digiBaseWasDigies = new[] { true, false };
            var aprspacketString = string.Format("{0}>{1},{4}*,{5}:!6028.51S/02505.68W#PHG{2}/{3}", srccall, dstcall, phg, comment, digiBase[0], digiBase[1]);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(PositionFormat.fapPOS_UNCOMPRESSED, packet.fap_pos_format);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);

            //And now check for position
            Assert.AreEqual("-60.4752", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-25.0947", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            Assert.AreEqual(phg, packet.phg);
            Assert.AreEqual(comment, packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(2, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
        }

        [Test]
        public void CanBasicUncompressedPacketsWithAmbiguityBeDecodedPt1()
        {
            var digiBase = new[] { "OH2RDG", "WIDE" };
            var digiBaseWasDigies = new[] { true, false };
            var aprspacketString = string.Format("{0}>{1},{4}*,{5}:!602 .  S/0250 .  W#PHG{2}/{3}", srccall, dstcall, phg, comment, digiBase[0], digiBase[1]);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(PositionFormat.fapPOS_UNCOMPRESSED, packet.fap_pos_format);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);

            //And now check for position
            Assert.AreEqual("-60.4167", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-25.0833", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18520", packet.pos_resolution.ToString("0"));
            Assert.AreEqual(3, packet.pos_ambiguity);

            Assert.AreEqual(phg, packet.phg);
            Assert.AreEqual(comment, packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(2, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
        }

        [Test]
        public void CanBasicUncompressedPacketsWithAmbiguityBeDecodedPt2()
        {
            var digiBase = new[] { "OH2RDG", "WIDE" };
            var digiBaseWasDigies = new[] { true, false };
            var aprspacketString = string.Format("{0}>{1},{4}*,{5}:!60  .  S/025  .  W#PHG{2}/{3}", srccall, dstcall, phg, comment, digiBase[0], digiBase[1]);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(PositionFormat.fapPOS_UNCOMPRESSED, packet.fap_pos_format);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);

            //And now check for position
            Assert.AreEqual("-60.5000", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-25.5000", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("111120", packet.pos_resolution.ToString("0"));
            Assert.AreEqual(4, packet.pos_ambiguity);

            Assert.AreEqual(phg, packet.phg);
            Assert.AreEqual(comment, packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(2, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
        }
        [Test]
        public void CanBasicUncompressedPacketBeDecodedLastResort()
        {
            var digiBase = new[] { "OH2RDG", "WIDE" };
            var digiBaseWasDigies = new[] { true, false };
            var aprspacketString = string.Format("{0}>{1},{4}*,{5}:hoponassualku!6028.51S/02505.68W#PHG{2}/{3}", srccall, dstcall, phg, comment, digiBase[0], digiBase[1]);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(PositionFormat.fapPOS_UNCOMPRESSED, packet.fap_pos_format);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);

            //And now check for position
            Assert.AreEqual("-60.4752", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-25.0947", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            Assert.AreEqual(0, packet.pos_ambiguity);

            Assert.AreEqual(phg, packet.phg);
            Assert.AreEqual(comment, packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(2, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
        }

        [Test]
        public void CanBasicUncompressedPacketsWithCommentOnStationWithWXSymbol()
        {
            var aprspacketString = "A0RID-1>KC0PID-7,WIDE1,qAR,NX0R-6:=3851.38N/09908.75W_Home of KA0RID";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            //And now check for position
            Assert.AreEqual("38.8563", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-99.1458", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            Assert.IsNull(packet.comment);
        }

        [Test]
        public void IsPacketWithWhiteSpaceinCommentTrimmed()
        {
            var aprspacketString = string.Format("{0}>{1},OH2RDG*,WIDE:!6028.51N/02505.68E#PHG{2}   {3}  \t ",dstcall,srccall,phg,comment);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(phg, packet.phg);
            Assert.AreEqual(comment, packet.comment);
        }

        [Test]
        public void IsPacketWithTimestampAndAltitudeParsedPt1()
        {
            var aprspacketString = "YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=000079 13.8V 15CYB1RUS-9 Mobile Tracker";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            
            Assert.AreEqual("-6.15517", packet.latitude.ToString("0.00000"));
            Assert.AreEqual("106.71417", packet.longitude.ToString("0.00000"));
            var exp = 24.07920/MiniHelpers.FT_TO_M;
            Assert.AreEqual(exp.ToString("0.00000"), packet.altitude.ToString("0.00000"));
        }

        [Test]
        public void IsPacketWithTimestampAndAltitudeParsedPt2()
        {
            var aprspacketString = "YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=-00079 13.8V 15CYB1RUS-9 Mobile Tracker";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            var exp = -24.07920 / MiniHelpers.FT_TO_M;
            Assert.AreEqual(exp.ToString("0.00000"), packet.altitude.ToString("0.00000"));
//            Assert.AreEqual("-24.07920", packet.altitude.ToString("0.00000"));
        }
        [Test]
        [Ignore]
        public void CanBasicUncompressPacketBeDecodedAll()
        {
            var comment = "RELAY,WIDE, OH2AP Jarvenpaa";
            var phg = "7220";
            var srccall = "OH2RDP-1";
            var dstcall = "BEACON-15";
            var aprspacket = string.Format("{0}>{1},OH2RDG*,WIDE:!6028.51N/02505.68E#PHG{2}/{3}", srccall, dstcall, phg, comment);

            //# and the same, southwestern...
            //%h = (); # clean up
            aprspacket = string.Format("{0}>{1},OH2RDG*,WIDE:!6028.51S/02505.68W#PHG{2}/{3}", srccall, dstcall, phg, comment);

            //# and the same, with ambiguity
            //%h = (); # clean up
            aprspacket = string.Format("{0}>{1},OH2RDG*,WIDE:!602 .  S/0250 .  W#PHG{2}{3}", srccall, dstcall, phg, comment);

            //# and the same with "last resort" !-location parsing
            //%h = (); # clean up
            aprspacket = string.Format("{0}>{1},OH2RDG*,WIDE:hoponassualku!6028.51S/02505.68W#PHG{2}{3}", srccall, dstcall, phg, comment);
            //$retval = parseaprs($aprspacket, \%h);

            //# Here is a comment on a station with a WX symbol. The comment is ignored,
            //# because it easily gets confused with weather data.
            //%h = ();
            aprspacket = "A0RID-1>KC0PID-7,WIDE1,qAR,NX0R-6:=3851.38N/09908.75W_Home of KA0RID";
            //$retval = parseaprs($aprspacket, \%h);

            //# validate that whitespace is trimmed from comment
            aprspacket = string.Format("{0}>{1},OH2RDG*,WIDE:!6028.51N/02505.68E#PHG{2}   {3}  \t ", srccall, dstcall, phg, comment);

            //# position with timestamp and altitude
            //%h = (); # clean up
            aprspacket = "YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=000079 13.8V 15CYB1RUS-9 Mobile Tracker";

            //# position with timestamp and altitude,but bad
            //%h = (); # clean up
            aprspacket = "YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=-00079 13.8V 15CYB1RUS-9 Mobile Tracker";
            //$retval = parseaprs($aprspacket, \%h);

            //# rather basic position packet
            //%h = (); # clean up
            aprspacket = "YC0SHR>APU25N,TCPIP*,qAC,ALDIMORI:=0606.23S/10644.61E-GW SAHARA PENJARINGAN JAKARTA 147.880 MHz";
            //$retval = parseaprs($aprspacket, \%h);
        }
    }
    //# a basic uncompressed packet decoding test
    //# Mon Dec 10 2007, Hessu, OH7LZB
    //
    //use Test;
    //
    //BEGIN { plan tests => 28 + 5 + 5 + 3 + 4 + 2 + 3 };
    //use Ham::APRS::FAP qw(parseaprs);
    //
    //my $comment = "RELAY,WIDE, OH2AP Jarvenpaa";
    //my $phg = "7220";
    //my $srccall = "OH2RDP-1";
    //my $dstcall = "BEACON-15";
    //my $aprspacket = "$srccall>$dstcall,OH2RDG*,WIDE:!6028.51N/02505.68E#PHG$phg/$comment";
    //my %h;
    //my $retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse a basic uncompressed packet (northeast)");
    //ok($h{'format'}, 'uncompressed', "incorrect packet format parsing");
    //ok($h{'srccallsign'}, $srccall, "incorrect source callsign parsing");
    //ok($h{'dstcallsign'}, $dstcall, "incorrect destination callsign parsing");
    //ok(sprintf('%.4f', $h{'latitude'}), "60.4752", "incorrect latitude parsing (northern)");
    //ok(sprintf('%.4f', $h{'longitude'}), "25.0947", "incorrect longitude parsing (eastern)");
    //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
    //ok($h{'phg'}, $phg, "incorrect PHG parsing");
    //ok($h{'comment'}, $comment, "incorrect comment parsing");
    //
    //my @digis = @{ $h{'digipeaters'} };
    //ok(${ $digis[0] }{'call'}, 'OH2RDG', "Incorrect first digi parsing");
    //ok(${ $digis[0] }{'wasdigied'}, '1', "Incorrect first digipeated bit parsing");
    //ok(${ $digis[1] }{'call'}, 'WIDE', "Incorrect second digi parsing");
    //ok(${ $digis[1] }{'wasdigied'}, '0', "Incorrect second digipeated bit parsing");
    //ok($#digis, 1, "Incorrect amount of digipeaters parsed");
    //
    //# and the same, southwestern...
    //%h = (); # clean up
    //$aprspacket = "$srccall>$dstcall,OH2RDG*,WIDE:!6028.51S/02505.68W#PHG$phg$comment";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse a basic uncompressed packet (southwest)");
    //ok(sprintf('%.4f', $h{'latitude'}), "-60.4752", "incorrect latitude parsing (southern)");
    //ok(sprintf('%.4f', $h{'longitude'}), "-25.0947", "incorrect longitude parsing (western)");
    //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
    //

    //# and the same, with ambiguity
    //%h = (); # clean up
    //$aprspacket = "$srccall>$dstcall,OH2RDG*,WIDE:!602 .  S/0250 .  W#PHG$phg$comment";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse a basic ambiguity packet (southwest)");
    //ok(sprintf('%.4f', $h{'latitude'}), "-60.4167", "incorrect latitude parsing (southern)");
    //ok(sprintf('%.4f', $h{'longitude'}), "-25.0833", "incorrect longitude parsing (western)");
    //ok(sprintf('%.0f', $h{'posambiguity'}), "3", "incorrect position ambiguity");
    //ok(sprintf('%.0f', $h{'posresolution'}), "18520", "incorrect position resolution");
    //
    //# and the same, with even more ambiguity
    //%h = (); # clean up
    //$aprspacket = "$srccall>$dstcall,OH2RDG*,WIDE:!60  .  S/025  .  W#PHG$phg$comment";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse a very ambiguous packet (southwest)");
    //ok(sprintf('%.4f', $h{'latitude'}), "-60.5000", "incorrect latitude parsing (southern)");
    //ok(sprintf('%.4f', $h{'longitude'}), "-25.5000", "incorrect longitude parsing (western)");
    //ok(sprintf('%.0f', $h{'posambiguity'}), "4", "incorrect position ambiguity");
    //ok(sprintf('%.0f', $h{'posresolution'}), "111120", "incorrect position resolution");
    //

    //# and the same with "last resort" !-location parsing
    //%h = (); # clean up
    //$aprspacket = "$srccall>$dstcall,OH2RDG*,WIDE:hoponassualku!6028.51S/02505.68W#PHG$phg$comment";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse an uncompressed packet (last resort)");
    //ok(sprintf('%.4f', $h{'latitude'}), "-60.4752", "incorrect latitude parsing (last resort)");
    //ok(sprintf('%.4f', $h{'longitude'}), "-25.0947", "incorrect longitude parsing (last resort)");
    //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution (last resort)");
    //ok($h{'comment'}, $comment, "incorrect comment parsing (last resort)");
    //
    //# Here is a comment on a station with a WX symbol. The comment is ignored,
    //# because it easily gets confused with weather data.
    //%h = ();
    //$aprspacket = "A0RID-1>KC0PID-7,WIDE1,qAR,NX0R-6:=3851.38N/09908.75W_Home of KA0RID";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse an uncompressed packet (comment instead of wx)");
    //ok(sprintf('%.4f', $h{'latitude'}), "38.8563", "incorrect latitude parsing (comment instead of wx)");
    //ok(sprintf('%.4f', $h{'longitude'}), "-99.1458", "incorrect longitude parsing (comment instead of wx)");
    //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution (comment instead of wx)");
    //ok($h{'comment'}, undef, "incorrect comment parsing (comment instead of wx)");
    //

    //# validate that whitespace is trimmed from comment
    //$aprspacket = "$srccall>$dstcall,OH2RDG*,WIDE:!6028.51N/02505.68E#PHG$phg   $comment  \t ";
    //$retval = parseaprs($aprspacket, \%h);
    //ok($retval, 1, "failed to parse a basic uncompressed packet with extra whitespace");
    //ok($h{'phg'}, $phg, "incorrect PHG parsing");
    //ok($h{'comment'}, $comment, "incomment comment whitespace trimming");
    //
    //# position with timestamp and altitude
    //%h = (); # clean up
    //$aprspacket = "YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=000079 13.8V 15CYB1RUS-9 Mobile Tracker";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse an uncompressed packet (with timestamp, position, alt)");
    //ok(sprintf('%.5f', $h{'latitude'}), "-6.15517", "incorrect latitude parsing (uncompressed packet with timestamp, position, alt)");
    //ok(sprintf('%.5f', $h{'longitude'}), "106.71417", "incorrect longitude parsing (uncompressed packet with timestamp, position, alt)");
    //ok(sprintf('%.5f', $h{'altitude'}), "24.07920", "incorrect altitude parsing (uncompressed packet with timestamp, position, alt)");
    //
    //# position with timestamp and altitude
    //%h = (); # clean up
    //$aprspacket = "YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=-00079 13.8V 15CYB1RUS-9 Mobile Tracker";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse an uncompressed packet with negative altitude");
    //ok(sprintf('%.5f', $h{'altitude'}), "-24.07920", "incorrect negative altitude (uncompressed packet)");
    //
    /**/
    //# rather basic position packet
    //%h = (); # clean up
    //$aprspacket = "YC0SHR>APU25N,TCPIP*,qAC,ALDIMORI:=0606.23S/10644.61E-GW SAHARA PENJARINGAN JAKARTA 147.880 MHz";
    //$retval = parseaprs($aprspacket, \%h);
    //
    //ok($retval, 1, "failed to parse an uncompressed packet (YC0SHR)");
    //ok(sprintf('%.5f', $h{'latitude'}), "-6.10383", "incorrect latitude parsing (YC0SHR)");
    //ok(sprintf('%.5f', $h{'longitude'}), "106.74350", "incorrect longitude parsing (YC0SHR)");
    //
}