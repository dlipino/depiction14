﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    //Ported by David Li 8/28/2012
    //# test packets with DAO extensions
    //# Wed May 5 2008, Hessu, OH7LZB
    [TestFixture]
    public class AprsDaoPacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;
        private string comment = null;

        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        //# uncompressed packet with human-readable DAO
        //# DAO in beginning of comment
        [Test]
        public void CanReadUncompressedPacketWIthHumanReadableDAO()
        {
            comment = "12.3V 21C";
            var aprspacketString = string.Format("K0ELR-15>APOT02,WIDE1-1,WIDE2-1,qAo,K0ELR:/102033h4133.03NX09029.49Wv204/000!W33! {0}/A=000665",comment);

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(comment, packet.comment);
            Assert.AreEqual('W', packet.dao_datum_byte);

            Assert.AreEqual("41.55055", packet.latitude.ToString("0.00000"));
            Assert.AreEqual("-90.49155", packet.longitude.ToString("0.00000"));
//            var exp = 203/MiniHelpers.FT_TO_M;
            Assert.AreEqual("665", packet.altitude.ToString("0"));
            Assert.AreEqual("1.852", packet.pos_resolution.ToString("0.000"));
        }
//# test packets with DAO extensions
//# Wed May 5 2008, Hessu, OH7LZB
//
//use Test;
//
//BEGIN { plan tests => 19 };
//use Ham::APRS::FAP qw(parseaprs);
//
//my($aprspacket, $retval);
//my %h;
//
//# uncompressed packet with human-readable DAO
//# DAO in beginning of comment
//$aprspacket = "K0ELR-15>APOT02,WIDE1-1,WIDE2-1,qAo,K0ELR:/102033h4133.03NX09029.49Wv204/000!W33! 12.3V 21C/A=000665";
//$retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse an uncompressed packet with WGS84 human-readable DAO");
//ok($h{'daodatumbyte'}, 'W', "incorrect DAO datum byte");
//ok($h{'comment'}, '12.3V 21C', "incorrect comment parsing");
//ok(sprintf('%.5f', $h{'latitude'}), "41.55055", "incorrect latitude parsing");
//ok(sprintf('%.5f', $h{'longitude'}), "-90.49155", "incorrect longitude parsing");
//ok(sprintf('%.0f', $h{'altitude'}), 203, "incorrect altitude");
//ok(sprintf('%.3f', $h{'posresolution'}), "1.852", "incorrect position resolution");
//
        [Test]
        public void CanReadCompressedPacketWithBase91DAO()
        {
            var comment = "http://aprs.fi/";
            var aprspacketString = "OH7LZB-9>APZMDR,WIDE2-2,qAo,OH2RCH:!/0(yiTc5y>{2O http://aprs.fi/!w11!";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(comment, packet.comment);
            Assert.AreEqual('W', packet.dao_datum_byte);

            Assert.AreEqual("60.15273", packet.latitude.ToString("0.00000"));
            Assert.AreEqual("24.66222", packet.longitude.ToString("0.00000"));
            Assert.AreEqual("0.1852", packet.pos_resolution.ToString("0.0000"));
        }
//# compressed packet with BASE91 DAO
//# DAO in end of comment
//%h = ();
//$aprspacket = "OH7LZB-9>APZMDR,WIDE2-2,qAo,OH2RCH:!/0(yiTc5y>{2O http://aprs.fi/!w11!";
//$retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse an uncompressed packet with WGS84 BASE91 DAO");
//ok($h{'daodatumbyte'}, 'W', "incorrect DAO datum byte");
//ok($h{'comment'}, 'http://aprs.fi/', "incorrect comment parsing");
//ok(sprintf('%.5f', $h{'latitude'}), "60.15273", "incorrect latitude parsing");
//ok(sprintf('%.5f', $h{'longitude'}), "24.66222", "incorrect longitude parsing");
//ok(sprintf('%.4f', $h{'posresolution'}), "0.1852", "incorrect position resolution");
//
        [Test]
        public void CanReadMicEPacketWithBase91DAO()
        {
            var comment = "]Foo Bar";
            var aprspacketString = "OH2JCQ-9>VP1U88,TRACE2-2,qAR,OH2RDK-5:'5'9\"^Rj/]\"4-}Foo !w66!Bar";
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(comment, packet.comment);
            Assert.AreEqual('W', packet.dao_datum_byte);

            Assert.AreEqual("60.26471", packet.latitude.ToString("0.00000"));
            Assert.AreEqual("25.18821", packet.longitude.ToString("0.00000"));
            Assert.AreEqual("0.1852", packet.pos_resolution.ToString("0.0000"));
        }
//# mic-e packet with BASE91 DAO
//# DAO in middle of comment
//%h = ();
//$aprspacket = "OH2JCQ-9>VP1U88,TRACE2-2,qAR,OH2RDK-5:'5'9\"^Rj/]\"4-}Foo !w66!Bar";
//$retval = parseaprs($aprspacket, \%h);
//
//ok($retval, 1, "failed to parse a mic-e packet with WGS84 BASE91 DAO");
//ok($h{'daodatumbyte'}, 'W', "incorrect DAO datum byte");
//ok($h{'comment'}, ']Foo Bar', "incorrect comment parsing");
//ok(sprintf('%.5f', $h{'latitude'}), "60.26471", "incorrect latitude parsing");
//ok(sprintf('%.5f', $h{'longitude'}), "25.18821", "incorrect longitude parsing");
//ok(sprintf('%.4f', $h{'posresolution'}), "0.1852", "incorrect position resolution");
//

    }
}