﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class APRSMicePacketDecoderTests
    {
        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;

        #region setup
        [SetUp]
        protected void Setup()
        {
            packet = null;
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null; packet = null;
        }
        #endregion
        [Test]
        [Ignore("Sanity check test")]
        public void RegexVSPerlComp()
        {
            //A simple test to show how to convert perl regex to c# regex

            var input = "]\"3x}=";
            var regexStringPerl = @"^(.*?)([\x21-\x7b])([\x21-\x7b])([\x21-\x7b])\}(.*)$/o";
            var cline = @"^(.*?)([\x21-\x7b])([\x21-\x7b])([\x21-\x7b])\}(.*)$";
            var match = Regex.Match(input, cline);
            var rest = string.Empty;
            var acceptedValue = 6;
            var finalValue = 0.0;
            if (match.Groups.Count > 5)
            {
                finalValue = (match.Groups[2].Value[0] - 33) * Math.Pow(91, 2) +
                        (match.Groups[3].Value[0] - 33) * 91 +
                        (match.Groups[4].Value[0] - 33) - 10000;
                rest = string.Format("{0}{1}", match.Groups[1].Value, match.Groups[5].Value);
            }

            Assert.AreEqual(acceptedValue, finalValue);
            Assert.AreEqual("]=", rest);
        }
        [Test]
        [Ignore("ando another one")]
        public void MassiveReplace()
        {
            var symbolTable = "abce";
            var map = new Dictionary<string, string>
                          {
                              {"a", "0"},
                              {"b", "1"},
                              {"c", "2"},
                              {"d", "3"},
                              {"e", "4"},
                              {"f", "5"},
                              {"g", "6"},
                              {"h", "7"},
                              {"i", "8"},
                              {"j", "9"}
                          };
            var symbol = Regex.Replace(symbolTable.ToString(), "[a-j]", m => map[m.Value]);
        }
        [Test]
        public void CanNonMovingMicEPacketsBeDecoded()
        {
            var srccall = "OH7LZB-13";
            var dstcall = "SX15S6";
            var digiBase = new[] { "TCPIP", "qAC", "FOURTH" };
            var digiBaseWasDigies = new[] { true, false, false };
            var header = string.Format("{0}>{1},{2}*,{3},{4}", srccall, dstcall, digiBase[0], digiBase[1], digiBase[2]);
            var body = "'I',l \x1C>/]";
            var aprspacket = string.Format("{0}:{1}", header, body);

            var packetParser = new FAP_APRSPacketParser();
            APRSPacketFAPDetails packet = null;
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacket, false));
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);
            Assert.AreEqual(header, packet.header);
            Assert.AreEqual(body, packet.body);
            Assert.AreEqual(PacketType.fapLOCATION, packet.fap_packet_type);
            Assert.AreEqual(PositionFormat.fapPOS_MICE, packet.fap_pos_format);
            Assert.AreEqual("]", packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(3, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }

            Assert.AreEqual('/', packet.symbol_table);
            Assert.AreEqual('>', packet.symbol_code);
            //# check for undefined value, when there is no such data in the packet
            Assert.AreEqual(0, packet.pos_ambiguity);
            Assert.AreEqual(null, packet.messaging);

            //Check lat long speed, course
            Assert.AreEqual(-38.2560, packet.latitude);
            Assert.AreEqual(145.1860, packet.longitude);
            Assert.AreEqual(18.52, packet.pos_resolution);
            //# check for undefined value, when there is no such data in the packet
            Assert.AreEqual(0, packet.speed);
            Assert.AreEqual(0, packet.course);
            Assert.AreEqual(double.NaN, packet.altitude);
        }

        [Test]
        public void CanMovingMicEPacketsBeDecoded()
        { //Test another
            var srccall = "OH7LZB-2";
            var dstcall = "TQ4W2V";

            var digiBase = new[] { "WIDE2-1", "qAo", "OH7LZB" };
            var digiBaseWasDigies = new[] { false, false, false };
            var header = string.Format("{0}>{1},{2},{3},{4}", srccall, dstcall, digiBase[0], digiBase[1], digiBase[2]);
            //            var body = "`c51!f?>/]\"3x}=";
            var body = "`c51!f?>/]\"3x}=";
            var aprspacketString = string.Format("{0}:{1}", header, body);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);
            Assert.AreEqual(header, packet.header);
            Assert.AreEqual(body, packet.body);
            Assert.AreEqual(PacketType.fapLOCATION, packet.fap_packet_type);
            Assert.AreEqual(PositionFormat.fapPOS_MICE, packet.fap_pos_format);
            Assert.AreEqual("]=", packet.comment);

            var digis = packet.digipeaters;
            Assert.AreEqual(3, digis.Length);
            for (int i = 0; i < digiBase.Length; i++)
            {
                Assert.AreEqual(digiBase[i], digis[i].call);
                Assert.AreEqual(digiBaseWasDigies[i], digis[i].wasdigied);
            }
            Assert.AreEqual('/', packet.symbol_table);
            Assert.AreEqual('>', packet.symbol_code);
            Assert.AreEqual(0, packet.pos_ambiguity);
            Assert.AreEqual(null, packet.messaging);
            Assert.AreEqual("110", packet.messagebits);

            //Check lat long speed, course
            Assert.AreEqual("41.7877", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("-71.4202", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            //# check for undefined value, when there is no such data in the packet
            var exp = 105.56*MiniHelpers.KMH_TO_MPH;
            Assert.AreEqual(exp.ToString("0.00"), packet.speed.ToString("0.00"));
            Assert.AreEqual(35, packet.course);
            exp = 6/MiniHelpers.FT_TO_M;
            Assert.AreEqual(exp.ToString("0.0"), packet.altitude.ToString("0.0"));
        }
        //# a mic-e decoding test
        //# Tue Dec 11 2007, Hessu, OH7LZB
        //
        //use Test;
        //
        //BEGIN { plan tests => 50 + 8 + 7 + 5 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "OH7LZB-13";
        //my $dstcall = "SX15S6";
        //my $header = "$srccall>$dstcall,TCPIP*,qAC,FOURTH";
        //my $body = "'I',l \x1C>/]";
        //my $aprspacket = "$header:$body";
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse a non-moving target's mic-e packet");
        //ok($h{'srccallsign'}, $srccall, "incorrect source callsign parsing");
        //ok($h{'dstcallsign'}, $dstcall, "incorrect destination callsign parsing");
        //
        //ok($h{'header'}, $header, "incorrect header parsing");
        //ok($h{'body'}, $body, "incorrect body parsing");
        //ok($h{'type'}, 'location', "incorrect packet type parsing");
        //ok($h{'format'}, 'mice', "incorrect packet format parsing");
        //
        //ok($h{'comment'}, ']', "incorrect comment parsing");
        //
        //my @digis = @{ $h{'digipeaters'} };
        //ok(${ $digis[0] }{'call'}, 'TCPIP', "Incorrect first digi parsing");
        //ok(${ $digis[0] }{'wasdigied'}, '1', "Incorrect first digipeated bit parsing");
        //ok(${ $digis[1] }{'call'}, 'qAC', "Incorrect second digi parsing");
        //ok(${ $digis[1] }{'wasdigied'}, '0', "Incorrect second digipeated bit parsing");
        //ok(${ $digis[2] }{'call'}, 'FOURTH', "Incorrect igate call parsing");
        //ok(${ $digis[2] }{'wasdigied'}, '0', "Incorrect igate digipeated bit parsing");
        //ok($#digis, 2, "Incorrect amount of digipeaters parsed");
        //
        //ok($h{'symboltable'}, '/', "incorrect symboltable parsing");
        //ok($h{'symbolcode'}, '>', "incorrect symbolcode parsing");
        //
        //# check for undefined value, when there is no such data in the packet
        //ok($h{'posambiguity'}, 0, "incorrect posambiguity parsing");
        //ok($h{'messaging'}, undef, "incorrect messaging bit parsing");
        //
        //ok(sprintf('%.4f', $h{'latitude'}), "-38.2560", "incorrect latitude parsing");
        //ok(sprintf('%.4f', $h{'longitude'}), "145.1860", "incorrect longitude parsing");
        //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
        //
        //# check for undefined value, when there is no such data in the packet
        //ok($h{'speed'}, 0, "incorrect speed");
        //ok($h{'course'}, 0, "incorrect course");
        //ok($h{'altitude'}, undef, "incorrect altitude");
        /*Fd*/

        //$srccall = "OH7LZB-2";
        //$dstcall = "TQ4W2V";
        //$header = "$srccall>$dstcall,WIDE2-1,qAo,OH7LZB";
        //$body = "`c51!f?>/]\"3x}=";
        //$aprspacket = "$header:$body";
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse a moving target's mic-e");
        //ok($h{'srccallsign'}, $srccall, "incorrect source callsign parsing");
        //ok($h{'dstcallsign'}, $dstcall, "incorrect destination callsign parsing");
        //
        //ok($h{'header'}, $header, "incorrect header parsing");
        //ok($h{'body'}, $body, "incorrect body parsing");
        //ok($h{'type'}, 'location', "incorrect packet type parsing");
        //
        //ok($h{'comment'}, ']=', "incorrect comment parsing");
        //
        //@digis = @{ $h{'digipeaters'} };
        //ok(${ $digis[0] }{'call'}, 'WIDE2-1', "Incorrect first digi parsing");
        //ok(${ $digis[0] }{'wasdigied'}, '0', "Incorrect first digipeated bit parsing");
        //ok(${ $digis[1] }{'call'}, 'qAo', "Incorrect second digi parsing");
        //ok(${ $digis[1] }{'wasdigied'}, '0', "Incorrect second digipeated bit parsing");
        //ok(${ $digis[2] }{'call'}, 'OH7LZB', "Incorrect igate call parsing");
        //ok(${ $digis[2] }{'wasdigied'}, '0', "Incorrect igate digipeated bit parsing");
        //ok($#digis, 2, "Incorrect amount of digipeaters parsed");
        //
        //ok($h{'symboltable'}, '/', "incorrect symboltable parsing");
        //ok($h{'symbolcode'}, '>', "incorrect symbolcode parsing");
        //
        //# check for undefined value, when there is no such data in the packet
        //ok($h{'posambiguity'}, 0, "incorrect posambiguity parsing");
        //ok($h{'messaging'}, undef, "incorrect messaging bit parsing");
        //ok($h{'mbits'}, "110", "incorrect mic-e message type bits");
        //
        //ok(sprintf('%.4f', $h{'latitude'}), "41.7877", "incorrect latitude parsing");
        //ok(sprintf('%.4f', $h{'longitude'}), "-71.4202", "incorrect longitude parsing");
        //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
        //
        //# check for undefined value, when there is no such data in the packet
        //ok(sprintf("%.2f", $h{'speed'}), "105.56", "incorrect speed");
        //ok($h{'course'}, "35", "incorrect course");
        //ok($h{'altitude'}, "6", "incorrect altitude");
        //
        [Test]
        //Something that is not well understood is going on with symboltable
        public void CanPacketsWithInvalidSymbolTableBeDecoded()
        {
            var srccall = "OZ2BRN-4";
            var dstcall = "5U2V08";
            var header = string.Format("{0}>{1},OZ3RIN-3,OZ4DIA-2*,WIDE2-1,qAR,DB0KUE", srccall, dstcall);
            var body = "`'O<l!{,,\"4R}";
            var aprspacketString = string.Format("{0}:{1}", header, body);
            //            Assert.IsFalse(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            //            var errorCode = packet.fap_error_code;
            Assert.AreEqual(ErrorCode.fapSYM_INV_TABLE, packet.fap_error_code);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);
            Assert.AreEqual(header, packet.header);
            Assert.AreEqual(body, packet.body);


            Assert.AreEqual(PacketType.fapLOCATION, packet.fap_packet_type);
            Assert.IsTrue(string.IsNullOrEmpty(packet.comment));

        }
        //#
        //#### test decoding a packet which has an invalid symbol table (',')
        //#### configured
        //#
        // 
        //$srccall = "OZ2BRN-4";
        //$dstcall = "5U2V08";
        //$header = "$srccall>$dstcall,OZ3RIN-3,OZ4DIA-2*,WIDE2-1,qAR,DB0KUE";
        //$body = "`'O<l!{,,\"4R}";
        //$aprspacket = "$header:$body";
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 0, "parsed an unparseable mic-e packet");
        //ok($h{'resultcode'}, 'sym_inv_table', "wrong result code");
        //ok($h{'srccallsign'}, $srccall, "incorrect source callsign parsing");
        //ok($h{'dstcallsign'}, $dstcall, "incorrect destination callsign parsing");
        //
        //ok($h{'header'}, $header, "incorrect header parsing");
        //ok($h{'body'}, $body, "incorrect body parsing");
        //ok($h{'type'}, 'location', "incorrect packet type parsing");
        //
        //ok($h{'comment'}, undef, "incorrect comment parsing");
        //
        [Test]
        public void Can5ChannelMicETelemtryBeDecoded()
        {
            var srccall = "OZ2BRN-4";
            var dstcall = "5U2V08";
            var header = string.Format("{0}>{1},WIDE2-1,qAo,OH7LZB", srccall, dstcall);
            var telemetry = "‘102030FFff";// "â€˜102030FFff";

            var comment = "commeeeent";
            var body = string.Format("`c51!f?>/{0}{1}", telemetry, comment);
            var aprspacketString = string.Format("{0}:{1}", header, body);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            //Does not handle the space very well.
            Assert.AreEqual(comment, packet.comment);

            var telem = packet.telemetry;
            var expectedTelem = new[] { 16, 32, 48, 255, 255 };
            for (int i = 0; i < expectedTelem.Length; i++)
            {
                Assert.AreEqual(expectedTelem[i], telem.values[i]);
            }
        }
        //#
        //#### test decoding a packet with 5-channel Mic-E Telemetry
        //#
        // 
        //$srccall = "OZ2BRN-4";
        //$dstcall = "5U2V08";
        //$header = "$srccall>$dstcall,WIDE2-1,qAo,OH7LZB";
        //my $telemetry = "â€˜102030FFff";
        //$comment = "commeeeent";
        //$body = "`c51!f?>/$telemetry $comment";
        //$aprspacket = "$header:$body";
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse mic-e packet with 5-ch telemetry");
        //
        //ok($h{'comment'}, $comment, "incorrect comment parsing");
        //
        //my(@v) = @{ $h{'telemetry'}{'vals'} };
        //ok($v[0], "16", "wrong value 0 parsed from telemetry");
        //ok($v[1], "32", "wrong value 1 parsed from telemetry");
        //ok($v[2], "48", "wrong value 2 parsed from telemetry");
        //ok($v[3], "255", "wrong value 3 parsed from telemetry");
        //ok($v[4], "255", "wrong value 4 parsed from telemetry");
        //
        //
        [Test]
        public void Can2ChannelMicETelemtryBeDecoded()
        {
            var srccall = "OZ2BRN-4";
            var dstcall = "5U2V08";
            var header = string.Format("{0}>{1},WIDE2-1,qAo,OH7LZB", srccall, dstcall);
            var telemetry = "'1020";
            var comment = "commeeeent";
            var body = string.Format("`c51!f?>/{0}{1}", telemetry, comment);
            var aprspacketString = string.Format("{0}:{1}", header, body);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(comment, packet.comment);

            var telem = packet.telemetry;
            var expectedTelem = new[] { 16, 0, 32 };
            for (int i = 0; i < expectedTelem.Length; i++)
            {
                Assert.AreEqual(expectedTelem[i], telem.values[i]);
            }
        }
        //#
        //#### test decoding a packet with 2-channel Mic-E Telemetry
        //#
        // 
        //$srccall = "OZ2BRN-4";
        //$dstcall = "5U2V08";
        //$header = "$srccall>$dstcall,WIDE2-1,qAo,OH7LZB";
        //$telemetry = "'1020";
        //$comment = "commeeeent";
        //$body = "`c51!f?>/$telemetry $comment";
        //$aprspacket = "$header:$body";
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse mic-e packet with 2-ch telemetry");
        //
        //ok($h{'comment'}, $comment, "incorrect comment parsing");
        //
        //@v = @{ $h{'telemetry'}{'vals'} };
        //ok($v[0], "16", "wrong value 0 parsed from 2-ch telemetry");
        //ok($v[1], "0", "wrong value 1 parsed from 2-ch telemetry");
        //ok($v[2], "32", "wrong value 2 parsed from 2-ch telemetry");



    }
}