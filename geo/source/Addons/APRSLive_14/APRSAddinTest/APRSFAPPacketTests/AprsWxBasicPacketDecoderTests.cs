﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsWxBasicPacketDecoderTests
    {
        // original units were m/s
        //celsius
        //mm
        private FAP_APRSPacketParser packetParser;
        private string comment = "RELAY,WIDE, OH2AP Jarvenpaa";
        string phg = "7220";
        string srccall = "OH2RDP-1";
        string dstcall = "BEACON-15";
        APRSPacketFAPDetails packet;
        private double exp = double.NaN;
        const string noDec = "0";
        const string oneDec = "0.0";
        const string twoDec = "0.00";
        const string threeDec = "0.000";
        const string fourDec = "0.0000";
        const string fiveDec = "0.00000";
        #region setup and teardown
        [SetUp]
        protected void Setup()
        {
            packet = null;
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion

       static public void AssertDoubleEquality(double expected,double calced,int decimals)
        {
            switch(decimals)
            {
                case 0:
                    Assert.AreEqual(expected.ToString(noDec),calced.ToString(noDec));
                    break;
                case 1:
                    Assert.AreEqual(expected.ToString(oneDec), calced.ToString(oneDec));
                    break;
                case 2:
                    Assert.AreEqual(expected.ToString(twoDec), calced.ToString(twoDec));
                    break;
                case 3:
                    Assert.AreEqual(expected.ToString(threeDec), calced.ToString(threeDec));
                    break;
                case 4:
                    Assert.AreEqual(expected.ToString(fourDec), calced.ToString(fourDec));
                    break;
                case 5:
                    Assert.AreEqual(expected.ToString(fiveDec), calced.ToString(fiveDec));
                    break;
            }
        }
        [Test]
        public void DoesBasicWXPacketsDecode()
        {
            var digiBase = new[] { "WIDE2-1", "qAo", "OH2MQK-1" };
            var aprspacketString = string.Format("{0}>{1},{2},{3},{4}:=6030.35N/02443.91E_150/002g004t039r001P002p004h00b10125XRSW", 
                srccall, dstcall,digiBase[0],digiBase[1],digiBase[2]);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);

            Assert.AreEqual("60.5058", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("24.7318", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));

            var weatherInfo = packet.wx_report;

            Assert.AreEqual(150, weatherInfo.WindDirection);//degrees

            exp = .9 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindSpeed, 0);

            exp = 1.8 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindGust, 0);

//            Assert.AreEqual("3.9", weatherInfo.Temperature.ToString("0.0"));
            
            exp = MiniHelpers.CelciusToFahrenheit(3.9);
            AssertDoubleEquality(exp, weatherInfo.Temperature, 1);

//            Assert.AreEqual(100, weatherInfo.Humidity);
            exp = 100;
            AssertDoubleEquality(exp, weatherInfo.Humidity, 0);

//            Assert.AreEqual("1012.5", weatherInfo.Pressure.ToString("0.0"));
            exp = 1012.5;
            AssertDoubleEquality(exp, weatherInfo.Pressure, 1);

//            Assert.AreEqual("1.0", weatherInfo.RainLast24Hours.ToString("0.0"));
            exp = 1.0/(MiniHelpers.HINCH_TO_MM*100d);
            AssertDoubleEquality(exp, weatherInfo.RainLast24Hours, 2);

//            Assert.AreEqual("0.3", weatherInfo.RainLastHour.ToString("0.0"));
            exp = 0.3 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLastHour, 2);

//            Assert.AreEqual("0.5", weatherInfo.RainSinceMidnight.ToString("0.0"));
            exp = 0.5 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainSinceMidnight, 2);

            Assert.AreEqual("XRSW", weatherInfo.SoftwareIndicator);
        }

        [Test]
        public void DoesBasicWXPacketsWithCommentDecode()
        {
            var stringToPack = "41505532354E2C54435049502A2C7141432C4F48324741583A403130313331377A363032342E37384E2F30323530332E3937455F3135362F30303167303035743033387230303070303030503030306839316231303039332F74797065203F7361646520666F72206D6F726520777820696E666F";
            var packed = MiniHelpers.PerlPackH(stringToPack);
            
            //pack('H*', '41505532354E2C54435049502A2C7141432C4F48324741583A403130313331377A363032342E37384E2F30323530332E3937455F3135362F30303167303035743033387230303070303030503030306839316231303039332F74797065203F7361646520666F72206D6F726520777820696E666F');

            var aprspacketString = string.Format("OH2GAX>{0}", packed);
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual("60.4130", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("25.0662", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));
            var comment = "/type ?sade for more wx info";
            Assert.AreEqual(comment, packet.comment);

            var weatherInfo = packet.wx_report;
            Assert.AreEqual(156, weatherInfo.WindDirection);
            exp = 156;
            AssertDoubleEquality(exp, weatherInfo.WindDirection, 1);

//            Assert.AreEqual("0.4", weatherInfo.WindSpeed.ToString("0.0"));
            exp = 0.4 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindSpeed, 0);

//            Assert.AreEqual("2.2", weatherInfo.WindGust.ToString("0.0"));
            exp = 2.2 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindGust, 0);

//            Assert.AreEqual("3.3", weatherInfo.Temperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(3.3);
            AssertDoubleEquality(exp, weatherInfo.Temperature, 0);

//            Assert.AreEqual(91, weatherInfo.Humidity);
            exp = 91;
            AssertDoubleEquality(exp, weatherInfo.Humidity, 0);

//            Assert.AreEqual("1009.3", weatherInfo.Pressure.ToString("0.0"));
            exp = 1009.3;
            AssertDoubleEquality(exp, weatherInfo.Pressure, 1);


//            Assert.AreEqual("0.0", weatherInfo.RainLast24Hours.ToString("0.0"));
            exp = 0.0 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLast24Hours, 1);

//            Assert.AreEqual("0.0", weatherInfo.RainLastHour.ToString("0.0"));
            exp = 0.0 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLastHour, 1);

//            Assert.AreEqual("0.0", weatherInfo.RainSinceMidnight.ToString("0.0"));
            exp = 0.0 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainSinceMidnight, 1);

        }

        [Test]
        public void DoesBasicWXPacketWithCommentDecodePt1()
        {
            comment = "Oregon WMR100N Weather Station {UIV32N}";
            var aprspacketString = string.Format("JH9YVX>APU25N,TCPIP*,qAC,T2TOKYO3:@011241z3558.58N/13629.67E_068/001g001t033r000p020P020b09860h98{0}",comment);
        
            
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            Assert.AreEqual(comment,packet.comment);

            Assert.AreEqual("35.9763", packet.latitude.ToString("0.0000"));
            Assert.AreEqual("136.4945", packet.longitude.ToString("0.0000"));
            Assert.AreEqual("18.52", packet.pos_resolution.ToString("0.00"));

            var weatherInfo = packet.wx_report;
            Assert.AreEqual(68, weatherInfo.WindDirection);
            exp = 68;
            AssertDoubleEquality(exp, weatherInfo.WindDirection, 1);

//            Assert.AreEqual("0.4", weatherInfo.WindSpeed.ToString("0.0"));
            exp = 0.4 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindSpeed, 0);

//            Assert.AreEqual("0.4", weatherInfo.WindGust.ToString("0.0"));
            exp = 0.4 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindGust, 0);

//            Assert.AreEqual("0.6", weatherInfo.Temperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(0.6);
            AssertDoubleEquality(exp, weatherInfo.Temperature, 0);

//            Assert.AreEqual(98, weatherInfo.Humidity);
            exp = 98;
            AssertDoubleEquality(exp, weatherInfo.Humidity, 1);

//            Assert.AreEqual("986.0", weatherInfo.Pressure.ToString("0.0"));
            exp = 986.0;
            AssertDoubleEquality(exp, weatherInfo.Pressure, 1);

//            Assert.AreEqual("0.0", weatherInfo.RainLastHour.ToString("0.0"));
            exp = 0.0 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLastHour, 2);

//            Assert.AreEqual("5.1", weatherInfo.RainLast24Hours.ToString("0.0"));
            exp = 5.1 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLast24Hours, 2);

//            Assert.AreEqual("5.1", weatherInfo.RainSinceMidnight.ToString("0.0"));
            exp = 5.1 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainSinceMidnight, 2);

        }
        
        [Test]
        public void DoesPositionlessWXPacketWithSnowDecode()
        {
            var aprspacketString = "JH9YVX>APU25N,TCPIP*,qAC,T2TOKYO3:_12032359c180s001g002t033r010p040P080b09860h98Os010L500";
        
            Assert.IsTrue(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));

            Assert.AreEqual(double.NaN, packet.latitude);
            Assert.AreEqual(double.NaN, packet.longitude);
            Assert.AreEqual(double.NaN, packet.pos_resolution);

            var weatherInfo = packet.wx_report;
            Assert.AreEqual(180, weatherInfo.WindDirection);
            exp = 180;
            AssertDoubleEquality(exp, weatherInfo.WindDirection, 1);

//            Assert.AreEqual("0.4", weatherInfo.WindSpeed.ToString("0.0"));
            exp = 0.4 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindSpeed, 0);

//            Assert.AreEqual("0.9", weatherInfo.WindGust.ToString("0.0"));
            exp = 0.9 / MiniHelpers.MPH_TO_MS;
            AssertDoubleEquality(exp, weatherInfo.WindGust, 0);


//            Assert.AreEqual("0.6", weatherInfo.Temperature.ToString("0.0"));
            exp = MiniHelpers.CelciusToFahrenheit(0.6);
            AssertDoubleEquality(exp, weatherInfo.Temperature, 0);

//            Assert.AreEqual(98, weatherInfo.Humidity);
            exp = 98;
            AssertDoubleEquality(exp, weatherInfo.Humidity, 1);

//            Assert.AreEqual("986.0", weatherInfo.Pressure.ToString("0.0"));
            exp = 986.0;
            AssertDoubleEquality(exp, weatherInfo.Pressure, 1);


//            Assert.AreEqual("2.5", weatherInfo.RainLastHour.ToString("0.0"));
            exp = 2.5 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLastHour, 1);

//            Assert.AreEqual("10.2", weatherInfo.RainLast24Hours.ToString("0.0"));
            exp = 10.2 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainLast24Hours, 1);

//            Assert.AreEqual("20.3", weatherInfo.RainSinceMidnight.ToString("0.0"));
            exp = 20.3 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.RainSinceMidnight, 1);

//            Assert.AreEqual("2.5", weatherInfo.SnowLast24Hours.ToString("0.0"));
            exp = 2.5 / (MiniHelpers.HINCH_TO_MM * 100d);
            AssertDoubleEquality(exp, weatherInfo.SnowLast24Hours, 1);

//            Assert.AreEqual(500, weatherInfo.Luminosity);
            exp = 500;
            AssertDoubleEquality(exp, weatherInfo.Luminosity, 1);

        }
        
   
//        public void DoBasicWXPacketsDecode()
//        {
//            var srccall = "OH2RDP-1";
//            var dstcall = "BEACON-15";
//            var aprspacket = string.Format("{0}>{1},WIDE2-1,qAo,OH2MQK-1:=6030.35N/02443.91E_150/002g004t039r001P002p004h00b10125XRSW", srccall, dstcall);
//
//            //# another case, with a comment
//            //
//            //$aprspacket = "OH2GAX>" . pack('H*', '41505532354E2C54435049502A2C7141432C4F48324741583A403130313331377A363032342E37384E2F30323530332E3937455F3135362F30303167303035743033387230303070303030503030306839316231303039332F74797065203F7361646520666F72206D6F726520777820696E666F');
//
//            //# and a third one with comment
//            //
//            aprspacket = "JH9YVX>APU25N,TCPIP*,qAC,T2TOKYO3:@011241z3558.58N/13629.67E_068/001g001t033r000p020P020b09860h98Oregon WMR100N Weather Station {UIV32N}";
//            //# positionless format with snowfall
//            //
//            aprspacket = "JH9YVX>APU25N,TCPIP*,qAC,T2TOKYO3:_12032359c180s001g002t033r010p040P080b09860h98Os010L500";
//        
//        }
        //# a basic wx packet decoding test
        //# Tue Dec 11 2007, Hessu, OH7LZB
        //
        //use Test;
        //
        //BEGIN { plan tests => 16 + 14 + 14 + 15 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "OH2RDP-1";
        //my $dstcall = "BEACON-15";
        //my $aprspacket = "$srccall>$dstcall,WIDE2-1,qAo,OH2MQK-1:=6030.35N/02443.91E_150/002g004t039r001P002p004h00b10125XRSW";
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse a basic wx packet");
        //ok($h{'srccallsign'}, $srccall, "incorrect source callsign parsing");
        //ok($h{'dstcallsign'}, $dstcall, "incorrect destination callsign parsing");
        //ok(sprintf('%.4f', $h{'latitude'}), "60.5058", "incorrect latitude parsing (northern)");
        //ok(sprintf('%.4f', $h{'longitude'}), "24.7318", "incorrect longitude parsing (eastern)");
        //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
        //
        //ok($h{'wx'}->{'wind_direction'}, 150, "incorrect wind direction parsing");
        //ok($h{'wx'}->{'wind_speed'}, "0.9", "incorrect wind speed parsing");
        //ok($h{'wx'}->{'wind_gust'}, "1.8", "incorrect wind gust parsing");
        //
        //ok($h{'wx'}->{'temp'}, "3.9", "incorrect temperature parsing");
        //ok($h{'wx'}->{'humidity'}, 100, "incorrect humidity parsing");
        //ok($h{'wx'}->{'pressure'}, "1012.5", "incorrect pressure parsing");
        //
        //ok($h{'wx'}->{'rain_24h'}, "1.0", "incorrect rain_24h parsing");
        //ok($h{'wx'}->{'rain_1h'}, "0.3", "incorrect rain_1h parsing");
        //ok($h{'wx'}->{'rain_midnight'}, "0.5", "incorrect rain_midnight parsing");
        //
        //ok($h{'wx'}->{'soft'}, "XRSW", "incorrect wx software id");
        //
        /**/

        //# another case, with a comment
        //
        //$aprspacket = "OH2GAX>" . pack('H*', '41505532354E2C54435049502A2C7141432C4F48324741583A403130313331377A363032342E37384E2F30323530332E3937455F3135362F30303167303035743033387230303070303030503030306839316231303039332F74797065203F7361646520666F72206D6F726520777820696E666F');
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse second basic wx packet");
        //ok(sprintf('%.4f', $h{'latitude'}), "60.4130", "incorrect latitude parsing (northern)");
        //ok(sprintf('%.4f', $h{'longitude'}), "25.0662", "incorrect longitude parsing (eastern)");
        //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
        //ok($h{'comment'}, "/type ?sade for more wx info", "incorrect comment parsing from weather packet");
        //
        //ok($h{'wx'}->{'wind_direction'}, 156, "incorrect wind direction parsing");
        //ok($h{'wx'}->{'wind_speed'}, "0.4", "incorrect wind speed parsing");
        //ok($h{'wx'}->{'wind_gust'}, "2.2", "incorrect wind gust parsing");
        //
        //ok($h{'wx'}->{'temp'}, "3.3", "incorrect temperature parsing");
        //ok($h{'wx'}->{'humidity'}, 91, "incorrect humidity parsing");
        //ok($h{'wx'}->{'pressure'}, "1009.3", "incorrect pressure parsing");
        //
        //ok($h{'wx'}->{'rain_24h'}, "0.0", "incorrect rain_24h parsing");
        //ok($h{'wx'}->{'rain_1h'}, "0.0", "incorrect rain_1h parsing");
        //ok($h{'wx'}->{'rain_midnight'}, "0.0", "incorrect rain_midnight parsing");
        //
        /**/

        //# and a third one with comment
        //
        //$aprspacket = 'JH9YVX>APU25N,TCPIP*,qAC,T2TOKYO3:@011241z3558.58N/13629.67E_068/001g001t033r000p020P020b09860h98Oregon WMR100N Weather Station {UIV32N}';
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse second basic wx packet");
        //ok(sprintf('%.4f', $h{'latitude'}), "35.9763", "incorrect latitude parsing (northern)");
        //ok(sprintf('%.4f', $h{'longitude'}), "136.4945", "incorrect longitude parsing (eastern)");
        //ok(sprintf('%.2f', $h{'posresolution'}), "18.52", "incorrect position resolution");
        //ok($h{'comment'}, "Oregon WMR100N Weather Station {UIV32N}", "incorrect comment parsing from weather packet");
        //
        //ok($h{'wx'}->{'wind_direction'}, 68, "incorrect wind direction parsing");
        //ok($h{'wx'}->{'wind_speed'}, "0.4", "incorrect wind speed parsing");
        //ok($h{'wx'}->{'wind_gust'}, "0.4", "incorrect wind gust parsing");
        //
        //ok($h{'wx'}->{'temp'}, "0.6", "incorrect temperature parsing");
        //ok($h{'wx'}->{'humidity'}, 98, "incorrect humidity parsing");
        //ok($h{'wx'}->{'pressure'}, "986.0", "incorrect pressure parsing");
        //
        //ok($h{'wx'}->{'rain_1h'}, "0.0", "incorrect rain_1h parsing");
        //ok($h{'wx'}->{'rain_24h'}, "5.1", "incorrect rain_24h parsing");
        //ok($h{'wx'}->{'rain_midnight'}, "5.1", "incorrect rain_midnight parsing");
        //
        /**/
        //# positionless format with snowfall
        //
        //$aprspacket = 'JH9YVX>APU25N,TCPIP*,qAC,T2TOKYO3:_12032359c180s001g002t033r010p040P080b09860h98Os010L500';
        //%h = ();
        //$retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 1, "failed to parse positionless wx packet");
        //ok(defined $h{'latitude'}, "", "found latitude from positionless wx packet");
        //ok(defined $h{'longitude'}, "", "found longitude from positionless wx packet");
        //ok(defined $h{'posresolution'}, "", "found position resolution in positionless wx packet");
        //
        //ok($h{'wx'}->{'wind_direction'}, 180, "incorrect wind direction parsing");
        //ok($h{'wx'}->{'wind_speed'}, "0.4", "incorrect wind speed parsing");
        //ok($h{'wx'}->{'wind_gust'}, "0.9", "incorrect wind gust parsing");
        //
        //ok($h{'wx'}->{'temp'}, "0.6", "incorrect temperature parsing");
        //ok($h{'wx'}->{'humidity'}, 98, "incorrect humidity parsing");
        //ok($h{'wx'}->{'pressure'}, "986.0", "incorrect pressure parsing");
        //
        //ok($h{'wx'}->{'rain_1h'}, "2.5", "incorrect rain_1h parsing");
        //ok($h{'wx'}->{'rain_24h'}, "10.2", "incorrect rain_24h parsing");
        //ok($h{'wx'}->{'rain_midnight'}, "20.3", "incorrect rain_midnight parsing");
        //
        //ok($h{'wx'}->{'snow_24h'}, "2.5", "incorrect snow_24h parsing");
        //ok($h{'wx'}->{'luminosity'}, "500", "incorrect l luminosity parsing");


    }
}