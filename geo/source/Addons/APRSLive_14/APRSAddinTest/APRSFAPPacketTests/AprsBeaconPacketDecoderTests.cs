﻿using APRSFapCSharp;
using NUnit.Framework;

namespace APRSTest.ARSPacketReadingTests
{
    [TestFixture]
    public class AprsBeaconPacketDecoderTests
    {

        private FAP_APRSPacketParser packetParser;
        APRSPacketFAPDetails packet;

        #region setup
        [SetUp]
        protected void Setup()
        {
            packetParser = new FAP_APRSPacketParser();

        }
        [TearDown]
        public void TearDown()
        {
            packetParser = null;
        }
        #endregion
        [Test]
        public void BeaconPacketsAreIgnored()
        {
            var srccall = "OH2RDU";
            var dstcall = "UIDIGI";
            var message = " UIDIGI 1.9";
            var aprspacketString = string.Format("{0}>{1}:{2}",srccall,dstcall,message);
            Assert.IsFalse(packetParser.FAP_APRSParser(ref packet, aprspacketString, false));
            
            Assert.AreEqual(ErrorCode.NoError,packet.fap_error_code);
            Assert.AreEqual(srccall, packet.src_callsign);
            Assert.AreEqual(dstcall, packet.dst_callsign);
            Assert.AreEqual(message, packet.body);
        
        }
        //# beacon decoding (well, they're non-APRS packets, so they're ignored)
        //# Tue Dec 11 2007, Hessu, OH7LZB
        //
        //use Test;
        //
        //BEGIN { plan tests => 5 };
        //use Ham::APRS::FAP qw(parseaprs);
        //
        //my $srccall = "OH2RDU";
        //my $dstcall = "UIDIGI";
        //my $message = " UIDIGI 1.9";
        //my $aprspacket = "$srccall>$dstcall:$message";
        //
        //my %h;
        //my $retval = parseaprs($aprspacket, \%h);
        //
        //ok($retval, 0, "failed to parse a message packet");
        //ok($h{'resultcode'}, undef, "wrong result code");
        //ok($h{'srccallsign'}, "$srccall", "wrong source callsign");
        //ok($h{'dstcallsign'}, "$dstcall", "wrong destination callsign");
        //ok($h{'body'}, $message, "wrong body");
        //

    }
}