﻿using System.IO;
using APRSBase;
using APRSPacketIO;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class APRSPacketBaseTest
    {
        [Test]
        public void IsSSIDRemovedFromCallSign()
        {
            var callNosid1 = "KA6PTJ";
            var call1 = callNosid1 + "-3";
            var packetBase = new APRSPacketBase();
            packetBase.SrcCallSignWithSSID = call1;
            Assert.AreEqual(callNosid1, packetBase.SrcCallSignNoSSID);
            var callNosid2 = "K7SDW";
            var call2 = callNosid2 + "-13";
            packetBase = new APRSPacketBase();
            packetBase.SrcCallSignWithSSID = call2;
            Assert.AreEqual(callNosid2, packetBase.SrcCallSignNoSSID);

        }
    }
}