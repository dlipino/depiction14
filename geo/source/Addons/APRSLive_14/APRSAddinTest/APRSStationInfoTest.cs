﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using APRS;
//using APRSLiveAddinTestJigWpf;
using APRS.FAPConnection;
using APRSBase;
using APRSLiveConfiguration.Model;
using APRSLiveFeedGatherer;
using APRSLiveFeedGatherer.Behaviour;
using Depiction.API.ValueTypes;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class APRSStationInfoTest
    {
        #region setup
        [TestFixtureSetUp]
        public void FSetup()
        {
            var fe = new FrameworkElement();
            //DepictionAccess.Initialize(new LimitedDepictionAccess());
        }
        #endregion

        [Test]
        public void DoesPositionHistoryWith3PointsWork()
        {
            var positionString = new List<string> { "1,1" };
            var geom = CreateZOIFromPositionHistory.CreatePositionHistoryFromPositionStringList(positionString);
            Assert.IsNull(geom);

            positionString = new List<string> { "1,1", "1,2" };
            geom = CreateZOIFromPositionHistory.CreatePositionHistoryFromPositionStringList(positionString);
            Assert.IsNotNull(geom);

            //This did not fail as expected, but changing things anyway
            positionString = new List<string> { "1,1", "1,2", "1,3" };
            geom = CreateZOIFromPositionHistory.CreatePositionHistoryFromPositionStringList(positionString);
            Assert.IsNotNull(geom);
        }
        [Test]
        public void APRSStationInfoDoesNotAddToHistoryIfPositionUnchanged()
        {
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:24:43"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
        }

        [Test]
        [Ignore("Do not do history at this point")]
        public void APRSStationInfoAddsToHistoryIfPositionChanged()
        {
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.3), DateTime.Parse("10/24/2009 10:24:43"));
            Assert.AreEqual(2, stationInfo.PositionHistory.Count);

        }

        [Ignore("Do not do history at this point")]
        [Test]
        public void APRSStationInfoHistoryMaintainsRollingTimeWindow()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();//config);
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.3), DateTime.Parse("10/24/2009 10:24:44"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.43), DateTime.Parse("10/24/2009 11:20:43"));
            Assert.AreEqual(3, stationInfo.PositionHistory.Count);
            stationInfo.UpdateHistory(DateTime.Parse("10/24/2009 11:24:43"));
            Assert.AreEqual(2, stationInfo.PositionHistory.Count);
        }

        [Test]
        public void APRSStationInfoCreatePacketInfoGrabsWeatherData()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            //            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();
            var str =
                "KE6NYT-5>APU25N,WIDE2-2 <UI C>:@311925z3414.14N/11901.65W_246/005g015t074r000p000P000h45b10133-/WX Report {UIV32N}";
            var packet = PacketInfo.ParseFromString(str);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.WindDirection);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.WindSpeed);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.WindGust);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.RainSinceMidnight);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.RainLastHour);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.BarometricPressure);
            Assert.AreNotEqual(Int32.MaxValue, packet.WeatherInfo.Humidity);
            //            var stationInfo = APRSStationInfo.CreateNewStationInfoFromPacket(gatherer.TimeWindowMinutes, packet);

            //            object propValue;
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindDirection", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindSpeed", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindGust", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainSinceMidnight", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainLastHour", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("BarometricPressure", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("Humidity", out propValue));
        }
        [Test]
        [Ignore("This test doesn't work because it requires the element library")]
        public void APRSStationInfoUpdateStationFromPacketGrabsWeatherData()
        {
            //            var config = new APRSAddinConfig();
            //            config.TimeWindowMinutes = 60;
            //            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();
            //            
            //            var elem = ElementFactory.CreateElementFromTypeString("Depiction.Plugin.PointOfInterest");
            //            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes) { Element = elem, lastUpdatedAt = DateTime.Now };
            //
            //            var str =
            //                "KE6NYT-5>APU25N,WIDE2-2 <UI C>:@311925z3414.14N/11901.65W_246/005g015t074r000p000P000h45b10133-/WX Report {UIV32N}";
            //            var packet = PacketInfo.ParseFromString(str);
            //
            //            stationInfo.UpdateStationFromPacket(ConversionHelpers.ConvertPacketInfoToPacketBase(packet),true);
            //
            //            object propValue;
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindDirection", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindSpeed", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("WindGust", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainSinceMidnight", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("RainLastHour", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("BarometricPressure", out propValue));
            //            Assert.IsTrue(stationInfo.Element.GetPropertyValue("Humidity", out propValue));

        }

        [Ignore("Do not do history at this point")]
        [Test]
        public void APRSStationInfoHistoryNeverDeletesLastPositionEvenAfterFallingOutOfTimeWindow()
        {
            var config = new APRSAddinConfig();
            config.TimeWindowMinutes = 60;
            var gatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer();//config);
            var stationInfo = new APRSStationInfo(gatherer.TimeWindowMinutes);
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.44, 18.3), DateTime.Parse("10/24/2009 10:23:43"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.3), DateTime.Parse("10/24/2009 10:24:44"));
            stationInfo.UpdatePositionHistory(new LatitudeLongitude(33.434, 18.43), DateTime.Parse("10/24/2009 11:20:43"));
            Assert.AreEqual(3, stationInfo.PositionHistory.Count);
            stationInfo.UpdateHistory(DateTime.Parse("10/24/2009 11:24:45"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
            stationInfo.UpdateHistory(DateTime.Parse("10/24/2009 12:24:45"));
            Assert.AreEqual(1, stationInfo.PositionHistory.Count);
        }
        [Test]
        [Ignore("Incomplete test, but holds messages sent from the same person from different locations (house and vehicle)")]
        public void DoesIconChangeWithMessage()
        {
            var origPacket =
                @"K7SDW-12>BEACON,K7SDW,W6SCE-10,WIDE2-2: <UI>:!3410.76N/11857.52W-14.1V P1";
            var secondPacket = @"K7SDW-13>S4QPXZ,WIDE1-1,WIDE2-1: <<UI>>:`.UVl!R+/>""62}=";
        }
        [Test]
        [Ignore("Doesnt really test anytyhing")]
        public void DoesPacketRetrieveCorrectIcon()
        {
            //KT6JA>S42RWY,KF6RAL-15,WIDE1,W6SCE-10*:`.=|!U<k/]"8C}=
            var packetString = @"KT6JA>S42RWY,KF6RAL-15,WIDE1,W6SCE-10*:`.=|!U<k/]""8C}=";
            var parsers = new List<PacketParserBase>();
            parsers.Add(new PacketParser());
            parsers.Add(new PacketParserFAP());
            var list = new List<APRSPacketBase>();
            string iconId;
            foreach (var parser in parsers)
            {
                list = parser.GetPackets(packetString + "\n");
                Assert.AreEqual(1, list.Count);
                foreach (var packetBase in list)
                {
                    var stationInfo = new APRSStationInfo(10);
                    stationInfo.Icon = APRSStationInfo.GetAPRSIconAndOverlay(packetBase.symbol_table, packetBase.symbol_code, 0, false, out iconId);
                }
            }
        }

        [Test]
        public void APRSStationInfoGetCroppedBitmapworks()
        {
            //var uri = new Uri("k://application:,,,/APRSLiveFeedGatherer;component/ResourceDictionary.xaml");
            // This application.loadcomponents is needed to load

            var tempPath = Path.Combine(Path.GetTempPath(), "testImages");
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
            //            var uri = new Uri("/APRSLiveFeedGatherer;component/ResourceDictionary.xaml", UriKind.Relative);
            //            var resourceDictionary = Application.LoadComponent(uri) as ResourceDictionary;
            char primSymbol = '\\';
            char secSymbol = '/';
            var thing = '1';
            var symbols = new[] { primSymbol, secSymbol, thing };
            string iconId;
            var angle = 0;

            foreach (var symbol in symbols)
            {
                for (int i = -30; i < 200; i++)
                {
                    angle =270;
                    var bmp = APRSStationInfo.GetAPRSIconAndOverlay(symbol, (char)i, angle, false, out iconId);
                    if (bmp != null)
                    {
                        var fileName = string.Format("{0}.png", iconId);
                        SaveBitmap(bmp, Path.Combine(tempPath, fileName));
                    }
                }
            }
            Directory.Delete(tempPath, true);
        }

        public static bool SaveBitmap(BitmapSource bitmap, string fileName)
        {
            if (bitmap != null)
            {
                BitmapEncoder encoder = new JpegBitmapEncoder();
                ((JpegBitmapEncoder)encoder).QualityLevel = 80;

                if (Path.HasExtension(fileName))
                {
                    switch (Path.GetExtension(fileName).ToLower())
                    {
                        case ".gif":
                        case ".giff":
                            encoder = new GifBitmapEncoder();
                            break;
                        case ".tiff":
                        case ".tif":
                            encoder = new TiffBitmapEncoder();
                            break;
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                }

                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(stream);
                }

                return true;
            }

            return false;
        }
    }
}
