﻿using System;
using System.Collections.Generic;
using System.Threading;
using APRS.FAPConnection;
using APRSBase;
using APRSLiveFeedGatherer;
using APRSTest.ARSPacketReadingTests;
using AgwpePort.Aprs;
using APRS;
using APRSLiveConfiguration.Model;
using APRSLiveFeedGatherer.Model;
using APRSPacketIO;
using NUnit.Framework;

namespace APRSTest
{
    [TestFixture]
    public class APRSParserTest
    {
        private int cnt;
        private List<PacketParserBase> parsers;
        //"N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local="//need to understand symboltable thing
        //why are the inputs repeated?

        private List<string> packetStrings = new List<string>
                                                 {
                                                     "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}",
                                                     "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}",
                                                     "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=",
                                                     "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1",
                                                     "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}",
                                                     "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}",
                                                     "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=",
                                                     "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:" + "`2'1 -Wj/]\"54}",
                                                     "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}",
                                                     "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=",
                                                     "N7KP-5>APT311,MISION,WIDE1,XTAL,BALDI,WIDE2*,qAR,KD7DVD-6:>TinyTrak3 v1.1",
                                                     "K7BKE-9>TW3YWS,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1 -Wj/]\"54}",
                                                     "WA7JEW-1>TW5RSP,KD7NM,ETIGER,WIDE2*,qAR,KD7DVD-6:`2)Wl O>/\"4w}",
                                                     "N7NJO-14>T7QUTX,ETIGER,WIDE1*,WIDE2-1,qAR,KD7DVD-6:`23l?8u/]\"4!}Local=",
                                                     "N9VW-7>T7RVXQ,XTAL,WIDE1,BALDI*,WIDE2-1,qAR,KD7DVD-6:`1OglqsK\"9U}HAPPY TRAILS=\"^]",
                                                     "K7BKE-9>TW4PPU,WIDE1-1,WIDE2-1,qAR,KD7DVD-6:`2'1nfj/]\"5,}",
                                                     "KB7ZKA-9>GPSLK,SEA*,qAR,KD7DVD-6:$GPGGA,231932,4732.038,N,12237.121,W,1,07,1.2,52.5,M,-18.7,M,,*43\""  // this data type does not
            // get parsed, data type not yet supported.//well it is with the fap decoder
                                                 };
        private List<string> packetStrings2 = new List<string>
                                                 {
          @"WA6MHA-11>APOT01,W6SCE-10*:!3410.50N/11828.90W_106/001g003t073P000h56OU2k",
          @"W6SCE-10>APN382,WIDE2-1:!3419.82N111836.06W#PHG6860/W1 on Oat Mtn./A=003747/k6ccc@amsat.org for info",
          @"KQ6NO-13>APRS,WA6ZSN,K6ERN*,WIDE2:TinyPack",
          @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@220610z3439.50N/11813.04W_225/000g001t075r000p000P000h32b10057WX at Quartz Hill {UIV32N}",
          @"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR""6f}TinyTrak4 Alpha!wIL!",
          @"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
@"KC6YIR-9>APTT4,WA6ZSN,WIDE1*,WIDE2-1:T#819,133,089,255,118,083,00100011",
@"VA3CC-14>S4UQRS,KA6PTJ-3,KF6RAL-15,W6SCE-10*:`-\ql#Tu/]"";a}Just runnin around",
@"W6MAF>APTW01,KA6PTJ-3,KF6RAL-15,W6SCE-10*:_09212314c248s007g007t069r000p000P000h54b10136tU2k",
@"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
@"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
@"K6ERN>APN382,WIDE2-2:@220614z3420.87N/11920.10W_000/000g000t083r000p000P000h21b10076.DsVP",
@"KA6PTJ-3>APN382,KF6RAL-15,W6SCE-10*:!3502.75NS11827.65W#PHG5380/Wn,SCAn/Tehachapi/A=007600",
@"W6KLG-2>GPSLV,KD6RSQ,K6ERN*,WIDE2:$GPRMC,061715.00,A,3302.50058,N,11651.22682,W,000.0,000.0,220912,013.2,E*40"
                                                 };

        private List<string> packetStrings3 = new List<string>
                                                  {
                                                      @"K7JAJ-1>APU25N,KA6PTJ-3,W6SCE-10*:=3439.50N/11813.04W_Jim @ Quartz Hill {UIV32N}",
                                                      @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:=3439.50N/11813.04W_Jim @ Quartz Hill {UIV32N}",
                                                      @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:>020302zQuartz Hill WX",

                                                  };
        //take 10/19/2012 12:18PM pacific time
        private List<string> timePackets = new List<string>
        {
            @"K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_263/004g008t079P000h54b10089OTW1",
            @"KF6CM-12>APT312,K6ERN,W6SCE-10*:/191917z3413.32N/11907.61W>179/026/A=000082",//12:17:54
            @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@191918z3439.50N/11813.04W_346/001g005t084r000p000P000h29b10076WX at Quartz Hill {UIV32N}",//12:18:37
                     @"N6UPG-1>RELAY,KA6PTJ-3,W6SCE-10*:@192036z3441.66N/11807.53W_022/000g002t099r000p000P000h19b10132OLIVER {UIV32N}",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_242/004g008t081P000h51b10076OTW1",
                                                  @"K6TZ-11>APTW01,K6ERN,W6SCE-10*:_10201240c336s001g002t...r...p...P...h..b10154tU2k",
                                                  @"K6TZ-11>APTW01,WA6ZSN,W6SCE-10*:_10201240c336s001g002t...r...p...P...h..b10154tU2k",
                                                  @"KF6ILA>GPSHW,W6SCE-10*:@191918z3303.70N/11634.43W_266/009g015t075r000p000P000h36b10158L744.DsVP",
                                                  @"KF7SPP-9>SSSPPS,KF6ILA-10,WIDE1,W6SCE-10*:`-]pl cv`""7r}448.800MHz - SHTVan mobile",
                                                  @"KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC6YIR-9>STQVSW,K6ERN,W6SCE-10*:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"CREBC-2>APN382,CREBC-1,W6SCE-10*:!3231.41NS11659.70W#PHG5530/W2,WAN, Tijuana BC  xe2si@fmre.mx,",
                                                  @"WA6YLB-5>BEACON,KA6PTJ-3,W6SCE-10*:T#008,132,100,113,092,038,00000101",
                                                  @"WA6YLB-5>BEACON,KELLER,W6SCE-10*:T#008,132,100,113,092,038,00000101",
                                                  @"KI6USZ-9>S2TWUQ,KA6DAC-1,WIDE1,W6SCE-10*:`,W'nH >/`""62}BSA Volunteer!_""",
                                                  @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@192038z3439.50N/11813.04W_180/001g000t089r000p000P000h25b10058WX at Quartz Hill {UIV32N}",
                                                  @"KC6URU-7>STTYTS,KF6RAL-15,W6SCE-10*,WIDE2-2:`.+yl#Mj/`"";s}VX8R 146.520 T100.0Hz_ ",
                                                  @"KC6URU-7>STTYTS,KF6RAL-15,N6EX-5,W6SCE-10*:`.+yl#Mj/`"";s}VX8R 146.520 T100.0Hz_ ",
                                                  @"KC6URU-7>STTYTS,KF6RAL-15,K6ERN,W6SCE-10*:`.+yl#Mj/`"";s}VX8R 146.520 T100.0Hz_ ",
                                                  @"KG6KDB>STQQXU,W6SCE-10*:`.9Tl!j>/""6N}",
                                                  @"K6GYL>3TPXYQ,N6EX-1,W6SCE-10*:`.[>m*tj/]""5s}=",
                                                  @"W6SH-4>APWW10,CREBC-1,W6SCE-10*:@203812h3240.65NI11710.66W&PHG5460 CERO iGate ceroinc.org 160' HAAT, 6dB omni, 25w",
        };

        public static List<string> packetswithcmd = new List<string>
                                                    {
                                                        @"cmd:cmd:KD6RSQ-5>APRS,TCPIP,N3PV*:!3308.94N/11610.91W_WX",
                                                        @"WA6DKS-9>S4QUPQ,W6SCE-10*:`.:)l""e>/""6*}"
                                                    };
        #region setup and teardown
        [SetUp]
        protected void Setup()
        {
            var q = @"VA3CC-14>S4UQRS,KA6PTJ-3,KF6RAL-15,W6SCE-10*:`-\ql#Tu/]"";a}Just runnin around ";
            parsers = new List<PacketParserBase>();
            parsers.Add(new PacketParser());
            parsers.Add(new PacketParserFAP());

        }
        [TearDown]
        public void TearDown()
        {
            for (int i = 0; i < parsers.Count; i++)
            {
                parsers[i] = null;
            }
            parsers = null;
        }
        #endregion

        [Test]
        public void TimeStampFromPacketsAreOK()
        {
            var str = string.Empty;
            var lineEnd = "\n";
            var expect = new[] {true, true, false};
            foreach (var packet in packetStrings3)
            {
                str += (packet + lineEnd);
            }
           
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                if (parser is PacketParser)
                {
                    expect = new[] { true, true, true };
                }else
                {
                    expect = new[] { true, true, false };
                }
                Console.WriteLine("the " + parser.ParserName);
                for (int i =0;i<packetInfos.Count;i++)
                {
                    var kind = packetInfos[i].sentTime.Kind;
                   Assert.AreEqual(expect[i],packetInfos[i].sentTime.Equals(DateTime.MinValue));
                }
            }
//            Assert.AreEqual(3,packetInfos.Count);
//            Assert.AreEqual(DateTime.MinValue,packetInfos[0].timeStamp);
//            var ct1 = new DateTime(2012, 10, 19, 12, 17, 0);
//            var dt1 = packetInfos[1].timeStamp;
//            Assert.AreEqual(ct1,dt1);
//
//            var ct2 = new DateTime(2012, 10, 19, 12, 18, 0);
//            var dt2 = packetInfos[2].timeStamp;
//
//            Assert.AreEqual(ct2, dt2);
        }

        [Test]
        public void DoescmdGetRemovedFromPacket()
        {
            var str = string.Empty;
            var lineEnd = "\n";
            foreach (var packet in packetswithcmd)
            {
                str += (packet + lineEnd);
            }

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                Assert.AreEqual(packetswithcmd.Count, packetInfos.Count);
            }
        }

        [Test]
        [Ignore("This is great for a manual test")]
        public void IconFromBeaconDoesNotChangeOriginalIcon()
        {
            var weather = @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:=3439.50N/11813.04W_Jim @ Quartz Hill {UIV32N}";
            var beacon = @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:>020302zQuartz Hill WX";

            foreach (var parser in parsers)
            {
                var base1 = parser.GetPackets(weather + "\n")[0];
                //            
                var base2 = parser.GetPackets(beacon + "\n")[0];
            }

            //            APRSStationInfo.CreateNewStationInfoFromPacket(10, );
            //this works like not so well because it needs the serialization thing
            // var st1 = APRSStationInfo.CreateNewStationInfoFromPacket(10,base1);
        }

        [Test]
        public void packetInfoCanHandleMultiLineStrings()
        {
            var str = string.Empty;
            var lineEnd = "\n";
            foreach (var packet in packetStrings)
            {
                str += (packet + lineEnd);
            }

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                if (parser is PacketParserFAP)
                {
                    Assert.AreEqual(17, packetInfos.Count);
                }
                else
                {
                    Assert.AreEqual(16, packetInfos.Count);
                }
            }
        }

        [Test]
        public void packetInfoCanHandleMultiLineStringsWithCRLFCombo()
        {
            var str = string.Empty;
            var lineEnd = "\r\n";
            var packetCount = packetStrings2.Count;
            //            var packetCount = packetStrings.Count;
            foreach (var packet in packetStrings2)
            {
                str += (packet + lineEnd);
            }

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                if (parser is PacketParserFAP)
                {
                    Assert.AreEqual(packetCount - 1, packetInfos.Count);
                }
                else
                {
                    Assert.AreEqual(packetCount - 3, packetInfos.Count);
                }
            }
        }

        [Test]
        public void packetInfoCanHandleMultiLineStringsWithNoNewline()
        {
            var str = string.Empty;
            var lineEnd = "\r";
            foreach (var packet in packetStrings)
            {
                str += (packet + lineEnd);
            }

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                if (parser is PacketParserFAP)
                {
                    Assert.AreEqual(17, packetInfos.Count);
                }
                else
                {
                    Assert.AreEqual(16, packetInfos.Count);
                }
            }
        }

        [Test]
        public void TryPositionWithTimestampIncludingWeatherData()
        {
            var str =
            "ASTRWX>APRS,AA7OA,KOPEAK,BALDI*: <UI>:@172205z4615.77N/12353.22W_068/001g009t053r000p000P000h..b10140.U21h";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                Assert.IsNotNull(packetInfos[0]);
            }
        }
        [Test]
        public void TryWeatherData()
        {
            var str =
                "KE6NYT-5>APU25N,WIDE2-2 <UI C>:@311925z3414.14N/11901.65W_246/005g015t074r000p000P000h45b10133-/WX Report {UIV32N}";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                Assert.IsNotNull(packetInfos[0]);
            }
        }

        [Test]
        public void TryTinyTrackerData()
        {
            var str = "K7SDW-15>3T1PWS-2,WIDE1-1,WIDE2-1 <UI>:`.UHl >/\"7R}CVC SAG Vehicle";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                Assert.IsNotNull(packetInfos[0]);
            }
        }

        //No idea why this is here
        [Test]
        public void TryAlternateParser()
        {
            var str = "$GPGGA,231932,4732.038,N,12237.121,W,1,07,1.2,52.5,M,-18.7,M,,*43\"\n";
            var v = new AprsData(str);
        }

        [Test]
        public void DoesParseLinesWork1()
        {
            var str =
            "WA7RVV>APW285,WA7RVV-8*,WIDE2-1: <UI>:_03160551c153s004g009t060r000p000P000h00b00000wU2K\r\n\r\n";
            foreach (var parser in parsers)
            {
                var packets = parser.GetPackets(str);
                Assert.AreEqual(1, packets.Count);
            }
        }


        [Test]
        public void DoesParseLinesWorkWithFragments()
        {
            var str =
                "7OA>RED,RED,SCOOP,WA7RVV-8*,WIDE3: <<UI>>:=4918.68N/11739.56W-Castlegar,BC \"146.520 SIMPLEX\" {UIV32N}\r\n" +
                "\r\nWA7RVV>APW285,WA7RVV-8*,WIDE2-1: <UI>:_03160551c153s004g009t060r000p000P000h00b00000wU2K\r\n";
            foreach (var parser in parsers)
            {
                var packets = parser.GetPackets("V");
                Assert.AreEqual(0, packets.Count);
                parser.GetPackets("A");
                Assert.AreEqual(0, packets.Count);
                packets = parser.GetPackets(str);
                Assert.AreEqual(2, packets.Count);
                Assert.AreEqual("VA7OA", packets[0].SrcCallSignWithSSID);
            }
        }

        [Test]
        public void CanHandleActualRepresentativeDataStrings()
        {
            var str = "SOMTN>APN383,BALDI,WIDE2*,qAR,KD7DVD-6:!4719.20NS12320.73W#PHG2860/W2,WAn South Mtn KB7UVC\n" +
                      "VE6VQ-7>APOT21,WIDE1-1*,WIDE2-1,qAR,KD7DVD-6:/162544h4745.69N/12207.95Wk271/026/A=000418 13C EPE007 Larry 443.325/224.78\n";

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                Assert.IsTrue(string.IsNullOrEmpty(parser.msgFragment));
                Assert.AreEqual(2, packetInfos.Count);
            }
        }

        [Test]
        public void CanHandleDataString2()
        {
            var str = "ETIGER>BEACON,qAR,KD7DVD-6:>147080t103.5 ETIGER Mnt W7GLB";

            str = "W7JGY-14>TW3YQP,ETIGER,WIDE1*,WIDE2-1:`2/?!!lu/]\"4I}";

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                Assert.IsNotNull(packetInfos[0]);
            }
        }

        [Test]
        public void CanHandleActualMessageReceivedAtArlington()
        {
            var str = "KN7S-7>TW5YTS,WIDE1-1,WIDE2-1:'2&z!fk/]\"4,}444.700+/103.5";
            str = @"WE7U-12>APOT2A,KD7NM,ETIGER,WIDE2*:!/6<?;/VWSjR?G SCVSAR";
            str = "N7GME-1>T7SQQW,WW7RA*,WIDE2-2:`2Asm Xk/]\"4h}";
            str = "KN7S-7>TW5YTS,WIDE1-1,WIDE2-1:'2&z!fk/]\"4,}444.700+/103.5";

            str = "KC7OO-7>TX1SRP,WIDE2-2:'2)6!SZk/]\"4@}";

            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                Assert.IsNotNull(packetInfos[0]);
            }
        }

        [Test]
        public void CanHandleTextMessage()
        {
            var str = "KD7DVD-8>APY008,WIDE1-1,WIDE2-1::KD7DVD-7 :test message{79";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                var parsed = packetInfos[0];
                Assert.IsNotNull(parsed);
                Assert.AreEqual("test message", parsed.message);
                Assert.AreEqual("KD7DVD-7", parsed.destination);//parsed.MessageRecipient);
                Assert.AreEqual("79", parsed.messageId);
            }
        }

        [Test]
        public void CanHandleActualReceivedTextMessage()
        {
            var str = "KD7DVD-6>APND13,WIDE2-1,qAR,KD7DVD::KF7GDY   :Use: H1, H2 H3..H8 or   use \"List\"  for all{40";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                var packetInfo = packetInfos[0];
                Assert.IsNotNull(packetInfo);

                Assert.AreEqual("Use: H1, H2 H3..H8 or   use \"List\"  for all", packetInfo.message);
                Assert.AreEqual("KF7GDY", packetInfo.destination);//packetInfo.MessageRecipient);
                Assert.AreEqual("40", packetInfo.messageId);
            }
        }

        [Test]
        public void CanHandleReceivedMessageAck()
        {
            var str = "KD7DVD-6>APND13,WIDE2-1,qAR,KD7DVD::KF7GDY   :ack";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                var packetInfo = packetInfos[0];

                Assert.IsNotNull(packetInfo);
                Assert.AreEqual("ack", packetInfo.message);
                Assert.AreEqual("KF7GDY", packetInfo.destination);
                Assert.IsTrue(string.IsNullOrEmpty(packetInfo.messageId));
            }
        }

        [Test]
        public void CanReconstructSerialMessage()
        {
            foreach (var parser in parsers)
            {
                var str = "C7KP>AP";
                var packetInfos = parser.GetPackets(str);
                packetInfos = parser.GetPackets("W");
                Assert.AreEqual(0, packetInfos.Count);
                packetInfos = parser.GetPackets("251,BAL");
                Assert.AreEqual(0, packetInfos.Count);
                packetInfos = parser.GetPackets("D");
                packetInfos = parser.GetPackets("I,ETIGE");
                packetInfos = parser.GetPackets("R");
                packetInfos = parser.GetPackets(",WIDE2*");
                packetInfos = parser.GetPackets(":");
                packetInfos = parser.GetPackets("=4654.9");
                packetInfos = parser.GetPackets("1");
                packetInfos = parser.GetPackets("N/12221");
                packetInfos = parser.GetPackets(".");
                packetInfos = parser.GetPackets("40W_PHG");
                packetInfos = parser.GetPackets("0");
                packetInfos = parser.GetPackets("000/Win");
                packetInfos = parser.GetPackets("A");
                packetInfos = parser.GetPackets("PRS 2.5");
                packetInfos = parser.GetPackets(".");
                packetInfos = parser.GetPackets("1 -WAPI");
                packetInfos = parser.GetPackets("E");
                packetInfos = parser.GetPackets("EATONVI");
                packetInfos = parser.GetPackets("L");
                Assert.AreEqual(0, packetInfos.Count);
                packetInfos = parser.GetPackets("-251\n");
                Assert.AreEqual(1, packetInfos.Count);
            }
        }

        [Test]
        public void CanReCombineMessageFragment()
        {
            foreach (var parser in parsers)
            {
                var str = "SOMTN>APN383,BALDI,WIDE2*,qAR,"; //partial string
                var packetInfos = parser.GetPackets(str);
                Assert.AreEqual(0, packetInfos.Count);
                Assert.IsNotNull(parser.msgFragment);
                var str2 = "KD7DVD-6:!4719.20NS12320.73W#PHG2860/W2,WAn South Mtn KB7UVC\n" +
                           "VE6VQ-7>APOT21,WIDE1-1*,WIDE2-1,qAR,KD7DVD-6:/162544h4745.69N/12207.95Wk271/026/A=000418 13C EPE007 Larry 443.325/224.78\n";
                packetInfos = parser.GetPackets(str2);
                Assert.AreEqual(2, packetInfos.Count);
            }
        }

        [Test]
        public void CanParseUltimeterMessage()
        {
            var str = "IIDAR>AP3383,ETIEER,WIDE**:$ULTW004500DB021922ED28260006871F000103C7009402120002000F";
            foreach (var parser in parsers)
            {
                var packets = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packets.Count);
                var pi = packets[0];
                Assert.IsNotNull(pi);
                Assert.IsNotNull(pi.weatherInfo);
                var weatherInfo = pi.weatherInfo;
                //                Assert.AreEqual(4, pi.weatherInfo.windSpeed);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(4, weatherInfo.windGust, 0);
                //                Assert.AreEqual(219, pi.weatherInfo.windDirection);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(219 * (360d / 255d), weatherInfo.windDirection, 0);
                //                Assert.AreEqual(54, pi.weatherInfo.temperature);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(54, weatherInfo.temperature, 0);
                //                Assert.AreEqual(10278, pi.weatherInfo.pressure);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(1027.8, weatherInfo.pressure, 1);
                //                Assert.AreEqual(97, pi.weatherInfo.humidity);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(97, weatherInfo.humidity, 0);
                //                Assert.AreEqual(2, pi.weatherInfo.rainSinceMidnight);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(2d / 100d, weatherInfo.rainSinceMidnight, 2);
            }
        }
        [Test]
        public void CanParseWeatherReport()
        {
            foreach (var parser in parsers)
            {
                var str = "_09230335c195s000g000t040r000p001P000h71b10172tU2k"; //weather report without position
                var pi = parser.ParseRawPacket(new APRSPacketRaw("FROM", "TO", "VIA", str));
                var weatherInfo = pi.weatherInfo;
                Assert.IsNotNull(weatherInfo);
                //                Assert.AreEqual(195, weather.windDirection);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(195, weatherInfo.windDirection, 0);
                //                Assert.AreEqual(0, weather.windSpeed);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(0, weatherInfo.windSpeed, 0);
                //                Assert.AreEqual(0, weather.windGust);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(0, weatherInfo.windGust, 0);
                //                Assert.AreEqual(40, weather.temperature);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(40, weatherInfo.temperature, 0);
                //                Assert.AreEqual(0, weather.rainLastHour);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(0, weatherInfo.rainLastHour, 0);
                //                Assert.AreEqual(1, weather.rainLast24Hours);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(1 / 100d, weatherInfo.rainLast24Hours, 2);
                //                Assert.AreEqual(0, weather.rainSinceMidnight);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(0, weatherInfo.rainSinceMidnight, 0);
                //                Assert.AreEqual(71, weather.humidity);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(71, weatherInfo.humidity, 0);
                //                Assert.AreEqual(10172, weather.pressure);
                AprsWxBasicPacketDecoderTests.AssertDoubleEquality(1017.2, weatherInfo.pressure, 1);
            }
        }

        [Test]
        [Ignore("Well therse are just goign to be obsolete soon")]
        public void ParseMessageScratchpad()
        {
            var str = "KB7SQE-10>BEACON,CREST,BAKER*,WIDE3-1,qAR,VE7NEK:KB7SQE APRS VOICE 146.840 - LIBBY MT";
            var pi = PacketInfo.ParseFromString(str);
            Assert.IsNotNull(pi.Latitude);
        }


        [Test]
        public void CanParseMessage()
        {
            foreach (var parser in parsers)
            {
                var str = "@291931z3303.70N/11634.43W_066/005g012t049r000p000P000h59b10167L441.DsVP";
                var rawPacket = new APRSPacketRaw("FROM", "TO", "VIA", str);
                var packetInfo = parser.ParseRawPacket(rawPacket);
                Assert.IsNotNull(packetInfo);
                Assert.AreNotEqual(double.NaN, packetInfo);

                //var str = "_09230335c195s000g000t040r000p001P000h71b10172tU2k";  weather report without position

                //var str = "$GPRMC,193427,A,3347.6442,N,11805.4995,W,000.0,107.9,290110,013.4,E*67";  //rawgpsorultimeter2000

                str = "@291934z3350.98N/11752.79W_132/000g000t063r000p000P000h49b10165/PHG61604/Anaheim WX & Email {UIV32N}";
                rawPacket = new APRSPacketRaw("FROM", "TO", "VIA", str);
                packetInfo = parser.ParseRawPacket(rawPacket);
                Assert.IsNotNull(packetInfo);
                Assert.AreNotEqual(double.NaN, packetInfo);

                str = "/291937z3346.02N/11754.21W>089/016!W78!/A=000078 14.1V 23C CNT00000";
                rawPacket = new APRSPacketRaw("FROM", "TO", "VIA", str);
                packetInfo = parser.ParseRawPacket(rawPacket);
                Assert.IsNotNull(packetInfo);
                Assert.AreNotEqual(double.NaN, packetInfo);
            }
        }

        [Test]
        public void CanParseMessageWithFrameInfoPt1()
        {
            foreach (var parser in parsers)
            {
                var str = @"K7HRT-8>TW0RYZ,ETIGER,SEDRO,WIDE2*: <<UI>>:`2MGlIZk/]""3t} 73 de PAT=";
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                var packetInfo = packetInfos[0];
                Assert.IsNotNull(packetInfo);
                Assert.IsNotNull(packetInfo.latitude);
            }
        }
        [Test]
        public void CanParseMessageWithFrameInfoPt2()
        {
            foreach (var parser in parsers)
            {
                var str =
                    "VE6VQ-7>APOT21,BALDI,WIDE1,BALDI*: <UI>:/035137h4730.26N/12211.82Wk349/051/A=000141 16C EPE013 Larry 443.325/224.78";
                //var packetInfo = PacketParser.ParseLine(str);
                var packetInfos = parser.GetPackets(str + "\n");
                Assert.AreEqual(1, packetInfos.Count);
                var packetInfo = packetInfos[0];
                Assert.IsNotNull(packetInfo);
                Assert.IsNotNull(packetInfo.latitude);
            }
        }

        [Test]
        public void CanParseBlockOf2LineMessages()
        {
            var str = "VE6VQ-7>APOT21,BALDI,WIDE1,BALDI*: <UI>:\n" +
               "/035137h4730.26N/12211.82Wk349/051/A=000141 16C EPE013 Larry 443.325/224.78\n" +
               "K7HRT-8>TW0RYZ,BALDI*: <<UI>>:\n" +
               "`2MGl mk/]\"4&} 73 de PAT=\n" +
               "K7HRT-8>TW0RYZ,ETIGER*,WIDE2-1: <<UI>>:\n" +
               "`2MGl mk/]\"4&} 73 de PAT=\n" +
               "BALDI>APOT21,WIDE2-1: <UI>:\n" +
               "!4713.13NS12150.61W#PHG5660/W2,WAn,   Baldi N7FSP WX\n" +
               "BALDI>APOT21,WIDE2-1: <UI>:\n" +
               "!4713.13N/12150.61W_182/004g007t027p000h59b10044T2WX\n" +
               "K7HRT-8>TW0RYZ,ETIGER*,WIDE2-1: <<UI>>:\n" +
               "`2MGlIZk/]\"3t} 73 de PAT=\n" +
               "K7HRT-8>TW0RYZ,ETIGER,SEDRO,WIDE2*: <<UI>>:\n" +
               "`2MGlIZk/]\"3t} 73 de PAT=\n" +
               "W7ROC>APW251,BALDI*: <<UI>>:\n" +
               "=4802.20N/12240.71W-PHG2130/WinAPRS  -WAJEFNORDLAND-251-<530>\n";
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(str);
                Assert.AreEqual(8, packetInfos.Count);
            }
        }

        [Test]
        public void CanRoundTripTextMessage()
        {
            //for now use old packet info for sending
            var str = "KD7DVD-8>APY008,WIDE1-1,WIDE2-1::KD7DVD-7 :test message{79";
            var packetStart = new PacketInfo
                                 {
                                     Callsign = "XWXX",
                                     DataType = DataType.Message,
                                     MessageRecipient = "vvvvv",
                                     Message = "orchid in my hand",
                                     MessageID = "23"
                                 };

            //var pktString = PacketEncoder.EncodePacket(packetIn,"APRS,TCPIP*");
            var packetEncoder = new TcpPacketEncoder("XWXX");
            var pktString = packetEncoder.EncodePacket(packetStart);

            var basePacket = ConversionHelpers.ConvertPacketInfoToPacketBase(packetStart);
            foreach (var parser in parsers)
            {
                var packetInfos = parser.GetPackets(pktString + "\n");//i guess the \n is needed
                Assert.AreEqual(1, packetInfos.Count);
                var packetInfo = packetInfos[0];
                Assert.IsNotNull(packetInfo);
                Assert.AreEqual(basePacket.SrcCallSignWithSSID, packetInfo.SrcCallSignWithSSID);
                Assert.AreEqual(basePacket.packetType, packetInfo.packetType);
                Assert.AreEqual(basePacket.destination, packetInfo.destination);
                Assert.AreEqual(basePacket.message, packetInfo.message);
                Assert.AreEqual(basePacket.messageId, packetInfo.messageId);
            }
        }
        //outgoing packet for test
        //KF7GDY>WIDE2-2:/240931z4813.04N/12210.77W_014/007g016t040P007b10105h59cwMServer

        [Test]
        public void CanReceiveMessageSent()
        {
            var config = new APRSAddinConfig();
            config.InputType = APRSInputType.Loopback;
            config.CallSign = "XXXXX";
            var aprsService = new APRSLiveBackgroundService(null, config);
            Assert.IsTrue(aprsService.SetupPacketIO());
            Assert.IsTrue(aprsService.StartPackeService());
            var msg = new APRSMessage { DestinationCallSign = "XXXXX", Message = "hello" };
            aprsService.MessengerService.SendMessage(msg);
            Thread.Sleep(75);
            aprsService.StopBackgroundService();
            //            var aprsGatherer = new APRSLiveFeedGatherer.APRSLiveFeedGatherer(config); 
            //            aprsGatherer.Start();
            //            var msg = new APRSMessage {DestinationCallSign = "XXXXX", Message = "hello"};
            //            aprsGatherer.SendMessage(msg);
            //            Thread.Sleep(75);
            //            aprsGatherer.Stop();
        }
    }
}
