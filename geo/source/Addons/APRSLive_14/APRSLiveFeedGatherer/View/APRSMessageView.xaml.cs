﻿using System;
using System.Windows;
using APRSLiveFeedGatherer.ViewModel;

namespace APRSLiveFeedGatherer.View
{
    /// <summary>
    /// Interaction logic for APRSMessageView.xaml
    /// </summary>
    public partial class APRSMessageView 
    {
        public APRSMessageView()
        {
            InitializeComponent();
            DataContextChanged += Window_DataContextChanged;
        }

        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            var newValue = e.NewValue as APRSMessageVM;
            if (newValue == null) return;
            newValue.RequestClose +=newValue_RequestClose;
        }

        void newValue_RequestClose(object sender, EventArgs e)
        {
            var viewModel = sender as APRSMessageVM;
            if (viewModel != null) viewModel.RequestClose -= newValue_RequestClose;
            //Close();
            
        }

       
        
        
    }
}
