﻿using System;
using System.Reflection;

namespace APRSLiveFeedGatherer
{
    public class APRSLiveVersion
    {
        public static string GetVersionStr()
        {
            var version = Assembly.GetAssembly(typeof(APRSLiveVersion)).GetName().Version;

            var versionNum = String.Format("{0}.{1}.{2}", version.Major, version.Minor, version.Revision);
            var versionString = string.Format("APRSLive version {0} for Depiction 1.4.x", versionNum);

            return versionString;
        }
    }
}
