using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using APRS;
using APRS.Listeners;
using Depiction.API.ExceptionHandling;

namespace APRSLiveFeedGatherer
{
    internal class LoopbackPacketListener : PacketListenerBase
    {
        private string filename = "loopbackPackets";
        private string fullPath = string.Empty;
        private readonly FileStream readStream;
        private readonly FileStream writeStream;
        #region Packet set example
        private List<string> packetStrings3 = new List<string>
                                                  {
                                                      @"K7JAJ-1>APU25N,KA6PTJ-3,W6SCE-10*:=3439.50N/11813.04W_Jim @ Quartz Hill {UIV32N}",
                                                      @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:=3439.50N/11813.04W_Jim @ Quartz Hill {UIV32N}",
                                                      @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:>020302zQuartz Hill WX"
                                                  };
        private List<string> packetStrings2 = new List<string>
                                                 {
          @"WA6MHA-11>APOT01,W6SCE-10*:!3410.50N/11828.90W_106/001g003t073P000h56OU2k",
          @"W6SCE-10>APN382,WIDE2-1:!3419.82N111836.06W#PHG6860/W1 on Oat Mtn./A=003747/k6ccc@amsat.org for info",
          @"KQ6NO-13>APRS,WA6ZSN,K6ERN*,WIDE2:TinyPack",
          @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@220610z3439.50N/11813.04W_225/000g001t075r000p000P000h32b10057WX at Quartz Hill {UIV32N}",
          @"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
          @"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
@"KC6YIR-9>APTT4,WA6ZSN,WIDE1*,WIDE2-1:T#819,133,089,255,118,083,00100011",
@"VA3CC-14>S4UQRS,KA6PTJ-3,KF6RAL-15,W6SCE-10*:`-\ql#Tu/]"";a}Just runnin around",
@"W6MAF>APTW01,KA6PTJ-3,KF6RAL-15,W6SCE-10*:_09212314c248s007g007t069r000p000P000h54b10136tU2k",
@"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
@"KC6YIR-9>STQVSW,WA6ZSN,WIDE1*,WIDE2-1:`.Ipl TR/""6f}TinyTrak4 Alpha!wIL!",
@"K6ERN>APN382,WIDE2-2:@220614z3420.87N/11920.10W_000/000g000t083r000p000P000h21b10076.DsVP",
@"KA6PTJ-3>APN382,KF6RAL-15,W6SCE-10*:!3502.75NS11827.65W#PHG5380/Wn,SCAn/Tehachapi/A=007600",
@"W6KLG-2>GPSLV,KD6RSQ,K6ERN*,WIDE2:$GPRMC,061715.00,A,3302.50058,N,11651.22682,W,000.0,000.0,220912,013.2,E*40",
@"YB1RUS-9>APOTC1,WIDE2-2,qAS,YC0GIN-1:/180000z0609.31S/10642.85E>058/010/A=-00079 13.8V 15CYB1RUS-9 Mobile Tracker",
//@"K7SDW-13>3T1PWZ,W6SCE-10*,WIDE2-1,qAR,BOXSPG:'.UOl!2[/>""68}TEST=",
 @"K7SDW-12>BEACON,K7SDW,W6SCE-10,WIDE2-2: <UI>:!3410.76N/11857.52W-14.1V P1"
,@"K7SDW-13>3T1PWZ,W6SCE-10*,WIDE2-1,qAR,BOXSPG:'.UOl!2[/>""68}TEST="
                                                 };

        #endregion
        #region Constructor
        public LoopbackPacketListener()
        {
            CreateLoopPacketFile();
            writeStream = new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
            readStream = new FileStream(fullPath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite);
            //readStream.ReadTimeout = 100;

        }
        #endregion
        #region Destructor
        ~LoopbackPacketListener()
        {
            DeleteLoopbackPacketFile();
        }
        #endregion

        public override void Start()
        {
            foreach (var line in packetStrings2)
            {
                var bytes = Encoding.ASCII.GetBytes(line + "\n");
                writeStream.Write(bytes, 0, bytes.Length);
            }
            writeStream.Flush();
            base.Start();
        }
        public override Stream GetReadStream()
        {
            return readStream;
        }

        public override Stream GetWriteStream()
        {
            return writeStream;
        }
        #region private helpers
        private void CreateLoopPacketFile()
        {
            var tempPath = Path.GetTempPath();
            fullPath = Path.Combine(tempPath, filename);
            if (File.Exists(fullPath)) File.Delete(fullPath);
        }
        private void DeleteLoopbackPacketFile()
        {
            try
            {
                if (File.Exists(fullPath)) File.Delete(fullPath);
            }
            catch (Exception ex)
            {
                DepictionExceptionHandler.HandleException("Could not delete loopback file", ex, true, true);
            }
        }
        #endregion


        static public List<string> newPackets = new List<string>
                                              {
                                                  @"N3PV>APU25N,N6OEI-10,W6SCE-10*:}",
                                                  @"KD6RSQ-5>APRS,TCPIP,N3PV*:!3308.94N/11610.91W_WX",
                                                  @"WA6DKS-9>S4QUPQ,W6SCE-10*:`.:)l""e>/""6*}",
                                                  @"N6UPG-1>RELAY,KA6PTJ-3,W6SCE-10*:@192036z3441.66N/11807.53W_022/000g002t099r000p000P000h19b10132OLIVER {UIV32N}",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_242/004g008t081P000h51b10076OTW1",
                                                  @"K6TZ-11>APTW01,K6ERN,W6SCE-10*:_10201240c336s001g002t...r...p...P...h..b10154tU2k",
                                                  @"K6TZ-11>APTW01,WA6ZSN,W6SCE-10*:_10201240c336s001g002t...r...p...P...h..b10154tU2k",
                                                  @"KF6ILA>GPSHW,W6SCE-10*:@191918z3303.70N/11634.43W_266/009g015t075r000p000P000h36b10158L744.DsVP",
                                                  @"KF7SPP-9>SSSPPS,KF6ILA-10,WIDE1,W6SCE-10*:`-]pl cv`""7r}448.800MHz - SHTVan mobile",
                                                  @"KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC6YIR-9>STQVSW,K6ERN,W6SCE-10*:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"CREBC-2>APN382,CREBC-1,W6SCE-10*:!3231.41NS11659.70W#PHG5530/W2,WAN, Tijuana BC  xe2si@fmre.mx,",
                                                  @"WA6YLB-5>BEACON,KA6PTJ-3,W6SCE-10*:T#008,132,100,113,092,038,00000101",
                                                  @"WA6YLB-5>BEACON,KELLER,W6SCE-10*:T#008,132,100,113,092,038,00000101",
                                                  @"KI6USZ-9>S2TWUQ,KA6DAC-1,WIDE1,W6SCE-10*:`,W'nH >/`""62}BSA Volunteer!_""",
                                                  @"K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@192038z3439.50N/11813.04W_180/001g000t089r000p000P000h25b10058WX at Quartz Hill {UIV32N}",
                                                  @"KC6URU-7>STTYTS,KF6RAL-15,W6SCE-10*,WIDE2-2:`.+yl#Mj/`"";s}VX8R 146.520 T100.0Hz_ ",
                                                  @"KC6URU-7>STTYTS,KF6RAL-15,N6EX-5,W6SCE-10*:`.+yl#Mj/`"";s}VX8R 146.520 T100.0Hz_ ",
                                                  @"KC6URU-7>STTYTS,KF6RAL-15,K6ERN,W6SCE-10*:`.+yl#Mj/`"";s}VX8R 146.520 T100.0Hz_ ",
                                                  @"KG6KDB>STQQXU,W6SCE-10*:`.9Tl!j>/""6N}",
                                                  @"K6GYL>3TPXYQ,N6EX-1,W6SCE-10*:`.[>m*tj/]""5s}=",
                                                  @"W6SH-4>APWW10,CREBC-1,W6SCE-10*:@203812h3240.65NI11710.66W&PHG5460 CERO iGate ceroinc.org 160' HAAT, 6dB omni, 25w",
                                                  @"N3PV>APU25N,N6OEI-10,W6SCE-10*:}W6LFQ-1>S2SUQU,TCPIP,N3PV*:`-\*l!x#/]""31}=",
                                                  @"W6MAF-11>ST2TUU,KELLER,WIDE1,KA6PTJ-3,W6SCE-10*:'-/LnTl</]""?(}CBR-1100XX Underway",
                                                  @"KI6GVQ>SSPPUY,KF6ILA-10,WIDE1,W6SCE-10*:`-]?l! >/>""6F}=",
                                                  @"N6EX-5>APNU19,W6SCE-10*:!3348.40N111822.11W#PHG7567/W1 for South Bay/WLA",
                                                  @"WA6MHA-11>APOT01,W6SCE-10*:>Apollo XI S.F.V.Flyers RC Club,LA CA Ot1m-PB 13.9V 28C",
                                                  @"WA6MHA-11>APOT01,W6SCE-10*:!3410.50N/11828.90W_105/010g011t078P000OU2k",
                                                  @"KH2FI>APU25N,KF6ILA-10,WA6ZSN,W6SCE-10*:;TRAFFIC  *192029z3234.55N\11706.92W?64 In 10 Minutes",
                                                  @"KH2FI>APU25N,CREBC-1,W6SCE-10*:=3234.55N/11706.92W-APRS Imperial Beach {UIV32N}",
                                                  @"KH2FI>APU25N,K6ERN,W6SCE-10*:=3234.55N/11706.92W-APRS Imperial Beach {UIV32N}",
                                                  @"KH2FI>APU25N,CREBC-2,CREBC-1,W6SCE-10*:;TRAFFIC  *192039z3234.55N\11706.92W?51 In 10 Minutes",
                                                  @"WG6G>SS4WPS,W6SCE-10*,WIDE2-1:`.]=l!@>\'""3t}|#I%V(3|!wGB!|3",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_270/006g006t081P000h51b10077OTW1",
                                                  @"W6SCE-10>APN382,WIDE2-1:!3419.82N111836.06W#PHG6860/W1 on Oat Mtn./A=003747/k6ccc@amsat.org for info",
                                                  @"K6ERN>APN382,W6SCE-10*:@192034z3420.87N/11920.10W_212/002g008t075r000p000P000h58b10106.DsVP",
                                                  @"K6ERN>APN382,WA6ZSN,W6SCE-10*:@192034z3420.87N/11920.10W_212/002g008t075r000p000P000h58b10106.DsVP",
                                                  @"KQ6EA-3>S3TYQS,W6SCE-10*,WIDE2-1:`.)o 5|j/]""3z}just_out_crusin'",
                                                  @"N3PV>APU25N,N6OEI-10,W6SCE-10*:}W9IF-4>APRS,TCPIP,N3PV*:!!009F00D402C90000273A0323--------012403390000009D",
                                                  @"K6TZ>APTW01,K6ERN,W6SCE-10*:_10201346c015s000g000t071r000p000P000h59b10120tU2k",
                                                  @"KH2FI>APU25N,CREBC-2,CREBC-1,W6SCE-10*:<IGATE,MSG_CNT=0,LOC_CNT=11",
                                                  @"WA6DKS-9>S4QUPP,W6SCE-10*:`.<7oTa>/""6%}",
                                                  @"WA6ZSN>APN391,K6ERN,W6SCE-10*:@192143z3419.85N/11901.57W_000/000g001t070r000p000P000h75b10071.DsVP",
                                                  @"KI6LYZ-1>SS1RYQ,KF6ILA-10,WIDE1,W6SCE-10*:`,@(o{`*/""?k}listening on 146.52",
                                                  @"N6KUZ-9>SS4UQT-2,W6SCE-10*,WIDE2-1:`.'Fl!2v/""4""}",
                                                  @"N6KUZ-9>SS4UQT-2,KF6ILA-10,WIDE1,W6SCE-10*:`.'Fl!2v/""4""}",
                                                  @"N6JVD-3>3RTWVQ,N6OEI-10,WIDE1,W6SCE-10*:`-\cn Dn\`""5'}DODGE'S VX-8G, N6JVD ON THE ROAD _#",
                                                  @"KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC6YIR-9>STQVSW,K6ERN,W6SCE-10*:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"W6CSP-10>GPSLK,W6SCE-10*:$GPGGA,204113,3350.577,N,11755.851,W,1,07,1.2,54.8,M,-31.9,M,,*45",
                                                  @"KA4JSR-2>BEACON,KF6ILA-10,W6SCE-10*:!3415.11N/11710.93W_robert.d.swain@googlemail.com",
                                                  @"WA6DKS-9>S4QUPP,W6SCE-10*:`.=o^a>/""6!}",
                                                  @"W6NZD-9>ST1PUP,W6SCE-10*,WIDE2-1:`./PlzS>/]""5m}=",
                                                  @"N6UPG-1>RELAY,KA6PTJ-3,W6SCE-10*:@192041z3441.66N/11807.53W_180/000g002t099r000p000P000h19b10128OLIVER {UIV32N}",
                                                  @"N6VNI-14>SSUPWP,N6EX-1,W6SCE-10*:`-TPl""ok/`""4^}_""",
                                                  @"WG6G>APT314,W6SCE-10*,WIDE2-1:>To Infinity and Beyond !!! 12.21V 46C Test 12345",
                                                  @"WG6G>APT314,N6EX-1,W6SCE-10*:>To Infinity and Beyond !!! 12.21V 46C Test 12345",
                                                  @"W6NZD-9>ST1PUX,W6SCE-10*,WIDE2-1:`./?mK?>/]""5j}=",
                                                  @"W6MAF-11>ST2RYY,KELLER,WIDE1,W6SCE-10*:'-0 @a</]""?T}CBR-1100XX Underway",
                                                  @"KQ6EA-3>S3TWYS,W6SCE-10*,WIDE2-1:`.*8o]|j/]""4%}",
                                                  @" N6KUZ-9>SS4UQT-2,W6SCE-10*,WIDE2-1:`.'Fl!Kv/""3{}",
                                                  @"N3PV>APU25N,N6OEI-10,W6SCE-10*:>160546zSouthwest APRS iGate and Digipeater / n3pv@amsat.org",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_265/005g005t081P000h51b10076OTW1",
                                                  @"KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC6YIR-9>APTT4,W6SCE-10*,WIDE2-1:>TinyTrak4 Alpha 100F 13.5V",
                                                  @"KC6YIR-9>APTT4,WA6ZSN,WIDE1,W6SCE-10*:>TinyTrak4 Alpha 100F 13.5V",
                                                  @"WA6ZSN>BEACON,W6SCE-10*:SMRA ERN South Mountain APRS/WX Station",
                                                  @"WA6ZSN>BEACON,K6ERN,W6SCE-10*:SMRA ERN South Mountain APRS/WX Station",
                                                  @"K6GYL>3TPXQR,W6SCE-10*,WIDE2-1:`-Ws \vj/]""5N}=",
                                                  @"KG6KDB>STQQXU,W6SCE-10*:`.9Tl!j>/""6M}",
                                                  @"N6KUZ-9>SS4UQT-2,W6SCE-10*,WIDE2-1:`.'Fl!;v/""4$}",
                                                  @"N6KUZ-9>SS4UQT-2,KF6ILA-10,WIDE1,W6SCE-10*:`.'Fl!;v/""4$}",
                                                  @" KF6RAL>APN383,K6ERN,W6SCE-10*:!3424.71NS11910.73W#PHG5110/W2,SCAn/Sulphur Mtn/NOAA/NWS/A=002724",
                                                  @"KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC6YIR-9>STQVSW,WA6ZSN,WIDE1,W6SCE-10*:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC7ADD>APT311,VACA,WIDE1,KA6PTJ-3,W6SCE-10*:>I go on snow 2",
                                                  @"W6SH-4>APWW10,W6SCE-10*,WIDE2-1:>192043zCERO iGate Coronado ceroinc.org",
                                                  @"W6SH-4>APWW10,K6ERN,W6SCE-10*:>192043zCERO iGate Coronado ceroinc.org",
                                                  @"KQ6EA-3>S3TWYS,W6SCE-10*,WIDE2-1:`.*^n^j/]""3x}just_out_crusin'",
                                                  @"N6MON-9>APT405,W6SCE-10*,WIDE2-1:!0000.000/00000.000R000/000/TinyTrak4",
                                                  @"N6MON-9>APT405,W6SCE-10*,WIDE2-1:!3348.72N/11754.75WR000/000/A=000141",
                                                  @"N6MON-9>APT405,N6EX-1,W6SCE-10*:!3348.72N/11754.75WR000/000/A=000141",
                                                  @"KF6ILA-10>GPSHW,W6SCE-10*:@000000z3321.78N/11650.18W#PHG2800/CA.SDG,USFS High Point Fire Lookout WX Sta. Elv 6133 Ft.DsVP",
                                                  @"KC6YIR-9>APTT4,W6SCE-10*,WIDE2-1:T#726,135,100,255,111,075,01101011",
                                                  @" AF6YK>S3UVUV,KF6ILA-10,WIDE1,W6SCE-10*:`-'dl d</'"":)}|*N%N(0|!win!|3",
                                                  @"KF6ILA-10>GPSHW,W6SCE-10*:@191814z3321.78N/11650.18W_326/006g012t067r000p000P000h47b10175.DsVP",
                                                  @"K6TZ>APTW01,W6SCE-10*:!3401.75N/11947.06W_SBARC Diablo Peak Santa Cruz Island",
                                                  @"K6TZ>APTW01,WA6ZSN,W6SCE-10*:!3401.75N/11947.06W_SBARC Diablo Peak Santa Cruz Island",
                                                  @"K6GYL>3TPXQR,W6SCE-10*:`-Um!4yj/]""5X}=",
                                                  @"N6EX-1>APNU19-3,W6SCE-10*:!3411.21N111802.13W#PHG5664/LA Area W1/A=003000/sceara@ham-radio.com",
                                                  @"K6DOD-1>APT311,W6SCE-10*:>:-)-",
                                                  @" XE2BGF-9>APT311,CREBC-1,W6SCE-10*:/204550h3230.52N/11659.20WF169/000/A=000442",
                                                  @"XE2BGF-9>APT311,CREBC-1,W6SCE-10*:>Haciendo mandaditos...",
                                                  @"KC6YQM-11>APOTW1,W6SCE-10*:!3410.61N/11902.02W_229/004g010t077P000h63b10123OTW1",
                                                  @"K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_258/005g006t081P000h51b10075OTW1",
                                                  @"KC5HNN>APTT4,VEGAS,WIDE1,KA6PTJ-3,W6SCE-10*:Jerry's motorhome  jgrosman@netzero.net",
                                                  @"CREBC-1>APN382,W6SCE-10*:!3218.82NS11639.92W#PHG5430/W2, WAN, Cerro Bola DM12qh",
                                                  @"WA6DKS-9>S4QTQT,W6SCE-10*:`.>6o{k>/""5t}",
                                                  @"W6MAF-11>ST2RYY,KELLER,WIDE1,W6SCE-10*:'-2=l#,</]""@1}CBR-1100XX Underway",
                                                  @"KQ6EA-3>S3TVWW,N6EX-1,W6SCE-10*:`.*hnJ%j/]""3{}",
                                                  @"N6UPG-1>RELAY,KELLER,W6SCE-10*:@192046z3441.66N/11807.53W_000/000g003t099r000p000P000h19b10128OLIVER {UIV32N}",
                                                  @"W6MF>BEACON,WB6WLV-11,W6SCE-10*:Fallbrook,CA",
                                                  @"KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"KC6YIR-9>STQVSW,WA6ZSN,WIDE1,W6SCE-10*:`.Ipl!}R/""6o}TinyTrak4 Alpha!wmo!",
                                                  @"K6TZ-11>APTW01,K6ERN,W6SCE-10*:_10201250c325s004g005t...r...p...P...h..b10152tU2k",
                                                  @"N3PV>APU25N,KA6DAC-1,W6SCE-10*:}W6KLG-2>GPSLV,TCPIP,N3PV*:$GPRMC,204700.00,A,3302.50249,N,11651.22452,W,000.0,000.0,191012,013.2,E*48",
                                                  @"N6KUZ-9>SS4UQT-2,N6EX-1,W6SCE-10*:`.'Fl!0v/""3x}",
                                                  @"K6DOD-1>STPYRS-3,W6SCE-10*,WIDE2-1:`.*Vl!</""5z}",
                                                  @"K6DOD-1>APT311,W6SCE-10*:>:-)-",
                                                  @"K6GYL>3TPWWX,W6SCE-10*,WIDE2-1:`-TZn])j/]""5`}=",
                                                  @"KE6BB>APWW10,W6SCE-10*:=T=*k#0Q]f& sTAPRS IGate SCV CA",
                                                  @"N6KUZ-9>APT407,W6SCE-10*,WIDE2-1:T#015,255,093,079,069,252,00000000",
                                                  @"N6KUZ-9>APT407,KF6ILA-10,WIDE1,W6SCE-10*:T#015,255,093,079,069,252,00000000",
                                                  @" WG6G>SS4WPS,W6SCE-10*,WIDE2-1:`.]=l!@>\'""3t}|#M%V(7|!wGB!|3",
                                                  @"WG6G>SS4WPS,N6EX-1,W6SCE-10*:`.]=l!@>\'""3t}|#M%V(7|!wGB!|3",
                                                  @"N3PV>APU25N,N6OEI-10,W6SCE-10*:}W9IF-4>APRS,TCPIP,N3PV*:!!00BE00D602D2000027390323--------01240344000000A0",
                                                  @"K6DOD-1>STPYRS-3,W6SCE-10*,WIDE2-1:`.*Vl!</""5z}",
                                                  @"K6DOD-1>APT311,W6SCE-10*:>:-)-",
                                                  @" K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@192048z3439.50N/11813.04W_248/000g002t089r000p000P000h24b10058WX at Quartz Hill {UIV32N}",
                                                  @"W6HRO>APU25N,W6SCE-10*:=3350.69N\11756.54WhHam Radio Outlet Anaheim {UIV32N}",
                                                  @"KB6HRO>APU25N,W6SCE-10*:>130000zUI-View32 V2.03",
                                                  @"K6DOD-1>STPYRS-3,W6SCE-10*,WIDE2-1:`.*Vl!</""5z}",
                                                  @"W6SCE-10>BEACON:>WIDE2-2 is best path in SoCal"
                                              };
    }
}

//1:36:34 PM <- N3PV>APU25N,N6OEI-10,W6SCE-10*:}KD6RSQ-5>APRS,TCPIP,N3PV*:!3308.94N/11610.91W_WX
//1:36:43 PM <- WA6DKS-9>S4QUPQ,W6SCE-10*:`.:)l"e>/"6*}
//1:36:54 PM <- N6UPG-1>RELAY,KA6PTJ-3,W6SCE-10*:@192036z3441.66N/11807.53W_022/000g002t099r000p000P000h19b10132OLIVER {UIV32N}
//1:37:03 PM <- K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1
//1:37:03 PM <- K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_242/004g008t081P000h51b10076OTW1
//1:37:16 PM <- K6TZ-11>APTW01,K6ERN,W6SCE-10*:_10201240c336s001g002t...r...p...P...h..b10154tU2k
//1:37:17 PM <- K6TZ-11>APTW01,WA6ZSN,W6SCE-10*:_10201240c336s001g002t...r...p...P...h..b10154tU2k
//1:37:49 PM <- KF6ILA>GPSHW,W6SCE-10*:@191918z3303.70N/11634.43W_266/009g015t075r000p000P000h36b10158L744.DsVP
//1:37:54 PM <- KF7SPP-9>SSSPPS,KF6ILA-10,WIDE1,W6SCE-10*:`-]pl cv/`"7r}448.800MHz - SHTVan mobile_"
//1:38:04 PM <- KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:38:04 PM <- KC6YIR-9>STQVSW,K6ERN,W6SCE-10*:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:38:14 PM <- CREBC-2>APN382,CREBC-1,W6SCE-10*:!3231.41NS11659.70W#PHG5530/W2,WAN, Tijuana BC  xe2si@fmre.mx
//1:38:22 PM <- WA6YLB-5>BEACON,KA6PTJ-3,W6SCE-10*:T#008,132,100,113,092,038,00000101
//1:38:23 PM <- WA6YLB-5>BEACON,KELLER,W6SCE-10*:T#008,132,100,113,092,038,00000101
//1:38:31 PM <- KI6USZ-9>S2TWUQ,KA6DAC-1,WIDE1,W6SCE-10*:`,W'nH >/`"62}BSA Volunteer!_"
//1:38:40 PM <- K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@192038z3439.50N/11813.04W_180/001g000t089r000p000P000h25b10058WX at Quartz Hill {UIV32N}
//1:38:48 PM <- KC6URU-7>STTYTS,KF6RAL-15,W6SCE-10*,WIDE2-2:`.+yl#Mj/`";s}VX8R 146.520 T100.0Hz_ 
//1:38:49 PM <- KC6URU-7>STTYTS,KF6RAL-15,N6EX-5,W6SCE-10*:`.+yl#Mj/`";s}VX8R 146.520 T100.0Hz_ 
//1:38:52 PM <- KC6URU-7>STTYTS,KF6RAL-15,K6ERN,W6SCE-10*:`.+yl#Mj/`";s}VX8R 146.520 T100.0Hz_ 
//1:38:55 PM <- KG6KDB>STQQXU,W6SCE-10*:`.9Tl!j>/"6N}
//1:38:57 PM <- K6GYL>3TPXYQ,N6EX-1,W6SCE-10*:`.[>m*tj/]"5s}=
//1:39:03 PM <- W6SH-4>APWW10,CREBC-1,W6SCE-10*:@203812h3240.65NI11710.66W&PHG5460 CERO iGate ceroinc.org 160' HAAT, 6dB omni, 25w
//1:39:11 PM <- N3PV>APU25N,N6OEI-10,W6SCE-10*:}W6LFQ-1>S2SUQU,TCPIP,N3PV*:`-\*l!x#/]"31}=
//1:39:17 PM <- W6MAF-11>ST2TUU,KELLER,WIDE1,KA6PTJ-3,W6SCE-10*:'-/LnTl</]"?(}CBR-1100XX Underway
//1:39:26 PM <- KI6GVQ>SSPPUY,KF6ILA-10,WIDE1,W6SCE-10*:`-]?l! >/>"6F}=
//1:39:26 PM <- N6EX-5>APNU19,W6SCE-10*:!3348.40N111822.11W#PHG7567/W1 for South Bay/WLA
//1:39:32 PM <- WA6MHA-11>APOT01,W6SCE-10*:>Apollo XI S.F.V.Flyers RC Club,LA CA Ot1m-PB 13.9V 28C
//1:39:33 PM <- WA6MHA-11>APOT01,W6SCE-10*:!3410.50N/11828.90W_105/010g011t078P000OU2k
//1:39:45 PM <- KH2FI>APU25N,KF6ILA-10,WA6ZSN,W6SCE-10*:;TRAFFIC  *192029z3234.55N\11706.92W?64 In 10 Minutes
//1:39:51 PM <- KH2FI>APU25N,CREBC-1,W6SCE-10*:=3234.55N/11706.92W-APRS Imperial Beach {UIV32N}
//1:39:52 PM <- KH2FI>APU25N,K6ERN,W6SCE-10*:=3234.55N/11706.92W-APRS Imperial Beach {UIV32N}
//1:39:57 PM <- KH2FI>APU25N,CREBC-2,CREBC-1,W6SCE-10*:;TRAFFIC  *192039z3234.55N\11706.92W?51 In 10 Minutes
//1:40:04 PM <- WG6G>SS4WPS,W6SCE-10*,WIDE2-1:`.]=l!@>\'"3t}|#I%V(3|!wGB!|3
//1:40:04 PM <- K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1
//1:40:05 PM <- K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_270/006g006t081P000h51b10077OTW1
//1:40:05 PM <- W6SCE-10>APN382,WIDE2-1:!3419.82N111836.06W#PHG6860/W1 on Oat Mtn./A=003747/k6ccc@amsat.org for info
//1:40:27 PM <- K6ERN>APN382,W6SCE-10*:@192034z3420.87N/11920.10W_212/002g008t075r000p000P000h58b10106.DsVP
//1:40:27 PM <- K6ERN>APN382,WA6ZSN,W6SCE-10*:@192034z3420.87N/11920.10W_212/002g008t075r000p000P000h58b10106.DsVP
//1:40:28 PM <- KQ6EA-3>S3TYQS,W6SCE-10*,WIDE2-1:`.)o 5|j/]"3z}just_out_crusin'
//1:40:29 PM <- N3PV>APU25N,N6OEI-10,W6SCE-10*:}W9IF-4>APRS,TCPIP,N3PV*:!!009F00D402C90000273A0323--------012403390000009D
//1:40:35 PM <- K6TZ>APTW01,K6ERN,W6SCE-10*:_10201346c015s000g000t071r000p000P000h59b10120tU2k
//1:40:39 PM <- KH2FI>APU25N,CREBC-2,CREBC-1,W6SCE-10*:<IGATE,MSG_CNT=0,LOC_CNT=11
//1:40:41 PM <- WA6DKS-9>S4QUPP,W6SCE-10*:`.<7oTa>/"6%}
//1:40:49 PM <- WA6ZSN>APN391,K6ERN,W6SCE-10*:@192143z3419.85N/11901.57W_000/000g001t070r000p000P000h75b10071.DsVP
//1:40:54 PM <- KI6LYZ-1>SS1RYQ,KF6ILA-10,WIDE1,W6SCE-10*:`,@(o{`*/"?k}listening on 146.52
//1:41:01 PM <- N6KUZ-9>SS4UQT-2,W6SCE-10*,WIDE2-1:`.'Fl!2v/"4"}
//1:41:01 PM <- N6KUZ-9>SS4UQT-2,KF6ILA-10,WIDE1,W6SCE-10*:`.'Fl!2v/"4"}
//1:41:02 PM <- N6JVD-3>3RTWVQ,N6OEI-10,WIDE1,W6SCE-10*:`-\cn Dn\`"5'}DODGE'S VX-8G, N6JVD ON THE ROAD _#
//1:41:07 PM <- KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:41:07 PM <- KC6YIR-9>STQVSW,K6ERN,W6SCE-10*:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:41:14 PM <- W6CSP-10>GPSLK,W6SCE-10*:$GPGGA,204113,3350.577,N,11755.851,W,1,07,1.2,54.8,M,-31.9,M,,*45
//1:41:31 PM <- KA4JSR-2>BEACON,KF6ILA-10,W6SCE-10*:!3415.11N/11710.93W_robert.d.swain@googlemail.com
//1:41:40 PM <- WA6DKS-9>S4QUPP,W6SCE-10*:`.=o^a>/"6!}
//1:41:44 PM <- W6NZD-9>ST1PUP,W6SCE-10*,WIDE2-1:`./PlzS>/]"5m}=
//1:41:58 PM <- N6UPG-1>RELAY,KA6PTJ-3,W6SCE-10*:@192041z3441.66N/11807.53W_180/000g002t099r000p000P000h19b10128OLIVER {UIV32N}
//1:41:58 PM <- N6VNI-14>SSUPWP,N6EX-1,W6SCE-10*:`-TPl"ok/`"4^}_"
//1:42:01 PM <- WG6G>APT314,W6SCE-10*,WIDE2-1:>To Infinity and Beyond !!! 12.21V 46C Test 12345
//1:42:02 PM <- WG6G>APT314,N6EX-1,W6SCE-10*:>To Infinity and Beyond !!! 12.21V 46C Test 12345
//1:42:17 PM <- W6NZD-9>ST1PUX,W6SCE-10*,WIDE2-1:`./?mK?>/]"5j}=
//1:42:29 PM <- W6MAF-11>ST2RYY,KELLER,WIDE1,W6SCE-10*:'-0 @a</]"?T}CBR-1100XX Underway
//1:42:33 PM <- KQ6EA-3>S3TWYS,W6SCE-10*,WIDE2-1:`.*8o]|j/]"4%}
//1:42:46 PM <- N6KUZ-9>SS4UQT-2,W6SCE-10*,WIDE2-1:`.'Fl!Kv/"3{}
//1:42:54 PM <- N3PV>APU25N,N6OEI-10,W6SCE-10*:>160546zSouthwest APRS iGate and Digipeater / n3pv@amsat.org
//1:43:08 PM <- K6OUA-11>APOTW1,W6SCE-10*:>13.3V  Simi Flyers ADS- WS1
//1:43:09 PM <- K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_265/005g005t081P000h51b10076OTW1
//1:43:09 PM <- KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:43:17 PM <- KC6YIR-9>APTT4,W6SCE-10*,WIDE2-1:>TinyTrak4 Alpha 100F 13.5V
//1:43:17 PM <- KC6YIR-9>APTT4,WA6ZSN,WIDE1,W6SCE-10*:>TinyTrak4 Alpha 100F 13.5V
//1:43:23 PM <- WA6ZSN>BEACON,W6SCE-10*:SMRA ERN South Mountain APRS/WX Station
//1:43:23 PM <- WA6ZSN>BEACON,K6ERN,W6SCE-10*:SMRA ERN South Mountain APRS/WX Station
//1:43:41 PM <- K6GYL>3TPXQR,W6SCE-10*,WIDE2-1:`-Ws \vj/]"5N}=
//1:43:52 PM <- KG6KDB>STQQXU,W6SCE-10*:`.9Tl!j>/"6M}
//1:43:54 PM <- N6KUZ-9>SS4UQT-2,W6SCE-10*,WIDE2-1:`.'Fl!;v/"4$}
//1:43:55 PM <- N6KUZ-9>SS4UQT-2,KF6ILA-10,WIDE1,W6SCE-10*:`.'Fl!;v/"4$}
//1:44:01 PM <- KF6RAL>APN383,K6ERN,W6SCE-10*:!3424.71NS11910.73W#PHG5110/W2,SCAn/Sulphur Mtn/NOAA/NWS/A=002724
//1:44:09 PM <- KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:44:10 PM <- KC6YIR-9>STQVSW,WA6ZSN,WIDE1,W6SCE-10*:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:44:20 PM <- KC7ADD>APT311,VACA,WIDE1,KA6PTJ-3,W6SCE-10*:>I go on snow 2
//1:44:36 PM <- W6SH-4>APWW10,W6SCE-10*,WIDE2-1:>192043zCERO iGate Coronado ceroinc.org
//1:44:36 PM <- W6SH-4>APWW10,K6ERN,W6SCE-10*:>192043zCERO iGate Coronado ceroinc.org
//1:44:44 PM <- KQ6EA-3>S3TWYS,W6SCE-10*,WIDE2-1:`.*^n^j/]"3x}just_out_crusin'
//1:44:51 PM <- N6MON-9>APT405,W6SCE-10*,WIDE2-1:!0000.000/00000.000R000/000/TinyTrak4
//1:45:13 PM <- N6MON-9>APT405,W6SCE-10*,WIDE2-1:!3348.72N/11754.75WR000/000/A=000141
//1:45:14 PM <- N6MON-9>APT405,N6EX-1,W6SCE-10*:!3348.72N/11754.75WR000/000/A=000141
//1:45:15 PM <- KF6ILA-10>GPSHW,W6SCE-10*:@000000z3321.78N/11650.18W#PHG2800/CA.SDG,USFS High Point Fire Lookout WX Sta. Elv 6133 Ft.DsVP
//1:45:15 PM <- KC6YIR-9>APTT4,W6SCE-10*,WIDE2-1:T#726,135,100,255,111,075,01101011
//1:45:28 PM <- AF6YK>S3UVUV,KF6ILA-10,WIDE1,W6SCE-10*:`-'dl d</'":)}|*N%N(0|!win!|3
//1:45:29 PM <- KF6ILA-10>GPSHW,W6SCE-10*:@191814z3321.78N/11650.18W_326/006g012t067r000p000P000h47b10175.DsVP
//1:45:38 PM <- K6TZ>APTW01,W6SCE-10*:!3401.75N/11947.06W_SBARC Diablo Peak Santa Cruz Island
//1:45:39 PM <- K6TZ>APTW01,WA6ZSN,W6SCE-10*:!3401.75N/11947.06W_SBARC Diablo Peak Santa Cruz Island
//1:45:42 PM <- K6GYL>3TPXQR,W6SCE-10*:`-Um!4yj/]"5X}=
//1:45:46 PM <- N6EX-1>APNU19-3,W6SCE-10*:!3411.21N111802.13W#PHG5664/LA Area W1/A=003000/sceara@ham-radio.com
//1:45:48 PM <- K6DOD-1>APT311,W6SCE-10*:>:-)-
//1:45:51 PM <- XE2BGF-9>APT311,CREBC-1,W6SCE-10*:/204550h3230.52N/11659.20WF169/000/A=000442
//1:45:52 PM <- XE2BGF-9>APT311,CREBC-1,W6SCE-10*:>Haciendo mandaditos...
//1:45:58 PM <- KC6YQM-11>APOTW1,W6SCE-10*:!3410.61N/11902.02W_229/004g010t077P000h63b10123OTW1
//1:46:07 PM <- K6OUA-11>APOTW1,W6SCE-10*:!3417.39N/11849.36W_258/005g006t081P000h51b10075OTW1
//1:46:23 PM <- KC5HNN>APTT4,VEGAS,WIDE1,KA6PTJ-3,W6SCE-10*:Jerry's motorhome  jgrosman@netzero.net
//1:46:35 PM <- CREBC-1>APN382,W6SCE-10*:!3218.82NS11639.92W#PHG5430/W2, WAN, Cerro Bola DM12qh
//1:46:36 PM <- WA6DKS-9>S4QTQT,W6SCE-10*:`.>6o{k>/"5t}
//1:46:42 PM <- W6MAF-11>ST2RYY,KELLER,WIDE1,W6SCE-10*:'-2=l#,</]"@1}CBR-1100XX Underway
//1:46:49 PM <- KQ6EA-3>S3TVWW,N6EX-1,W6SCE-10*:`.*hnJ%j/]"3{}
//1:46:55 PM <- N6UPG-1>RELAY,KELLER,W6SCE-10*:@192046z3441.66N/11807.53W_000/000g003t099r000p000P000h19b10128OLIVER {UIV32N}
//1:46:58 PM <- W6MF>BEACON,WB6WLV-11,W6SCE-10*:Fallbrook,CA
//1:47:10 PM <- KC6YIR-9>STQVSW,W6SCE-10*,WIDE2-1:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:47:11 PM <- KC6YIR-9>STQVSW,WA6ZSN,WIDE1,W6SCE-10*:`.Ipl!}R/"6o}TinyTrak4 Alpha!wmo!
//1:47:16 PM <- K6TZ-11>APTW01,K6ERN,W6SCE-10*:_10201250c325s004g005t...r...p...P...h..b10152tU2k
//1:47:23 PM <- N3PV>APU25N,KA6DAC-1,W6SCE-10*:}W6KLG-2>GPSLV,TCPIP,N3PV*:$GPRMC,204700.00,A,3302.50249,N,11651.22452,W,000.0,000.0,191012,013.2,E*48
//1:47:33 PM <- N6KUZ-9>SS4UQT-2,N6EX-1,W6SCE-10*:`.'Fl!0v/"3x}
//1:47:34 PM <- K6DOD-1>STPYRS-3,W6SCE-10*,WIDE2-1:`.*Vl!</"5z}
//1:47:34 PM <- K6DOD-1>APT311,W6SCE-10*:>:-)-
//1:47:47 PM <- K6GYL>3TPWWX,W6SCE-10*,WIDE2-1:`-TZn])j/]"5`}=
//1:47:47 PM <- KE6BB>APWW10,W6SCE-10*:=T=*k#0Q]f& sTAPRS IGate SCV CA
//1:47:53 PM <- N6KUZ-9>APT407,W6SCE-10*,WIDE2-1:T#015,255,093,079,069,252,00000000
//1:47:54 PM <- N6KUZ-9>APT407,KF6ILA-10,WIDE1,W6SCE-10*:T#015,255,093,079,069,252,00000000
//1:48:08 PM <- WG6G>SS4WPS,W6SCE-10*,WIDE2-1:`.]=l!@>\'"3t}|#M%V(7|!wGB!|3
//1:48:08 PM <- WG6G>SS4WPS,N6EX-1,W6SCE-10*:`.]=l!@>\'"3t}|#M%V(7|!wGB!|3
//1:48:09 PM <- N3PV>APU25N,N6OEI-10,W6SCE-10*:}W9IF-4>APRS,TCPIP,N3PV*:!!00BE00D602D2000027390323--------01240344000000A0
//1:48:23 PM <- K6DOD-1>STPYRS-3,W6SCE-10*,WIDE2-1:`.*Vl!</"5z}
//1:48:23 PM <- K6DOD-1>APT311,W6SCE-10*:>:-)-
//1:48:38 PM <- K7JAJ-1>APU25N,KA6PTJ-3,KF6RAL-15,W6SCE-10*:@192048z3439.50N/11813.04W_248/000g002t089r000p000P000h24b10058WX at Quartz Hill {UIV32N}
//1:48:47 PM <- W6HRO>APU25N,W6SCE-10*:=3350.69N\11756.54WhHam Radio Outlet Anaheim {UIV32N}
//1:48:47 PM <- KB6HRO>APU25N,W6SCE-10*:>130000zUI-View32 V2.03
//1:48:54 PM <- K6DOD-1>STPYRS-3,W6SCE-10*,WIDE2-1:`.*Vl!</"5z}
//1:48:54 PM <- W6SCE-10>BEACON:>WIDE2-2 is best path in SoCal