﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using APRSBase;
using APRSLiveConfiguration.Model;
using APRSLiveConfiguration.Properties;
using APRSLiveFeedGatherer.Model;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExceptionHandling;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;

namespace APRSLiveFeedGatherer
{
    public class APRSStationInfo
    {
        private int timeWindowFromConfig;
        private string callSignNoSSID = string.Empty;
        public IElementPrototype ElementPrototype { get; set; }
        public SortedList<DateTime, TimestampedPosition> PositionHistory = new SortedList<DateTime, TimestampedPosition>();
        static readonly BitmapSource primarySymbols;
        static readonly BitmapSource secondarySymbols;
        //public PacketInfo lastPacket;
        //        public APRSPacketBase lastPacket;
        public string lastMessage;
        public DateTime lastMessageReceivedTime;
        //        public DateTime lastUpdatedAt;
        #region Properties

        public BitmapSource Icon { get; set; }
        public string IconKey { get; set; }
        public TimestampedPosition LastPosition
        {
            get
            {
                return PositionHistory.Count == 0 ? null : PositionHistory.Values[PositionHistory.Count - 1];
            }
        }
        public string StationCallSignNoSSID
        {
            get { return callSignNoSSID; }
            private set { callSignNoSSID = value; }
        }

        #endregion

        #region Constructor
        static APRSStationInfo()
        {
            if (primarySymbols == null)
            {
                try
                {
                    var uri = new Uri("/APRSLiveFeedGatherer;component/ResourceDictionary.xaml", UriKind.Relative);
                    var resourceDictionary = new ResourceDictionary { Source = uri };
                    primarySymbols = (BitmapImage)resourceDictionary["APRSLive.PrimarySymbols"];
                    primarySymbols.Freeze();
                    secondarySymbols = (BitmapImage)resourceDictionary["APRSLive.SecondarySymbols"];
                    secondarySymbols.Freeze();
                }
                catch
                {
                    //interesting, in a bad way
                }

            }
        }
        public APRSStationInfo(int timeWindowMinutes)
        {
            timeWindowFromConfig = timeWindowMinutes;
        }

        #endregion

        public static APRSStationInfo CreateNewStationInfoFromPacket(int timeWindowMinutes, APRSPacketBase packetBase)
        {
            if (packetBase.latitude.Equals(double.NaN) || packetBase.longitude.Equals(double.NaN)) return null;

            var prototype = ElementFactory.CreateRawPrototypeOfType("OLWorks.APRSLiveStation", false);
            if (prototype == null) return null;
            CreateOrUpdateAPRSElementProperty(prototype, "EID", "Call sign as EID", packetBase.SrcCallSignWithSSID, false);

            CreateOrUpdateAPRSElementProperty(prototype, "SourceCallSign", "Call Sign", packetBase.SrcCallSignWithSSID, true);

            prototype.PermaText.IsEnhancedPermaText = true;
            prototype.UsePropertyNameInHoverText = true;

            var stationInfo = new APRSStationInfo(timeWindowMinutes)
                                  {
                                      ElementPrototype = prototype
                                  };
            stationInfo.StationCallSignNoSSID = packetBase.SrcCallSignNoSSID;

            UpdateStation(stationInfo, packetBase, true);
            return stationInfo;
        }

        private static void UpdateStation(APRSStationInfo stationInfo, APRSPacketBase packetBase, bool resetHoverText)
        {
            var elemProto = stationInfo.ElementPrototype;
            if (elemProto == null) return;

            var useUTC = Settings.Default.UseUTCTime;
            var timeToShow = packetBase.recievedTime.ToLocalTime();
            var timeString = timeToShow.ToString("g");
            if (useUTC)
            {
                timeToShow = packetBase.recievedTime.ToUniversalTime();
                timeString = timeToShow.ToString("g") + " UTC";
            }

            CreateOrUpdateAPRSElementProperty(elemProto, "Message", "Packet message", packetBase.message, false);
            CreateOrUpdateAPRSElementProperty(elemProto, "LastUpdated", "Last recieved", timeString, true);
            
            if(packetBase.sentTime.Equals(DateTime.MinValue))
            {
                var sentToShow = packetBase.sentTime.ToLocalTime();
                var sentToShowString = sentToShow.ToString("g");
                if (useUTC)
                {
                    sentToShow = packetBase.sentTime.ToUniversalTime();
                    sentToShowString = sentToShow.ToString("g") + " UTC";
                }
                CreateOrUpdateAPRSElementProperty(elemProto, "LastSent", "Last sent", sentToShowString, false);
            }

            if (!packetBase.latitude.Equals(double.NaN) && !packetBase.longitude.Equals(double.NaN))
            {
                var currentPos = new LatitudeLongitude(packetBase.latitude, packetBase.longitude);
                stationInfo.UpdatePositionHistory(currentPos, packetBase.recievedTime);//always in utc time
                var stringifiedPosHistory = string.Empty;
                var posHistoryAsStrings = new List<string>();
                foreach (var posHis in stationInfo.PositionHistory)
                {
                    var keyTime = posHis.Key;
                    var posForTime = posHis.Value.Position;
                    posHistoryAsStrings.Add(posForTime.ToXmlSaveString());
                    stringifiedPosHistory += string.Format("[Date: {0}: Position :{1}]\n", keyTime.TimeOfDay, posForTime.ToXmlSaveString());

                }
                //Arg cant add this one because depiction 1.4.2 and lower don't know how to save these properties
                var prop = CreateOrUpdateAPRSElementProperty(elemProto, "RawPosHistory", "RawPosition History", posHistoryAsStrings, false);
                prop.VisibleToUser = false;
                CreateOrUpdateAPRSElementProperty(elemProto, "StringPosHistory", "Position History", stringifiedPosHistory, false);
                //THe zoi has to be set after its updated
                elemProto.SetInitialPositionAndZOI(currentPos, null);
            }
            //            stationInfo.lastPacket = packetBase;//not sure why this is around
            if (!packetBase.altitude.Equals(double.NaN))
            {
                var alt = new Distance(MeasurementSystem.Imperial, MeasurementScale.Normal, packetBase.altitude);
                CreateOrUpdateAPRSElementProperty(elemProto, "Altitude", "Altitude", alt, resetHoverText);
            }
            if (!packetBase.speed.Equals(double.NaN))
            {
                var speed = new Speed(MeasurementSystem.Imperial, MeasurementScale.Normal, packetBase.speed);
                CreateOrUpdateAPRSElementProperty(elemProto, "Speed", "Speed", speed, resetHoverText);
            }

            double course = 0;
            var tempShowHover = resetHoverText;
            if (!packetBase.course.Equals(double.NaN))
            {
                course = packetBase.course;
            }
            else
            {
                course = double.NaN;
                tempShowHover = false;
            }

            CreateOrUpdateAPRSElementProperty(elemProto, "Direction", "Course", course, tempShowHover);

            if (packetBase.symbol_table != ' ' && packetBase.symbol_code != ' ')
            {
                string iconId = string.Format("{0},{1}", packetBase.symbol_table, packetBase.symbol_code);
                CreateOrUpdateAPRSElementProperty(elemProto, "IconSymbol", "Icon Symbol", iconId, false);
                //Update the old id, actually this could get ugly, you can't really delete things, actually, you can't really share
                //Icons between aprs stations, well you can sometimes, but not always, therefore, each station gets its own icon
                //until the depiction API gets a better way of sharing an making sure elements know about shared icons.
                //I suppose i could be smart about picking which aprs icons get shared since not all of them get overlays, and rotation
                //but that will be next time

                stationInfo.Icon = GetAPRSIconAndOverlay(packetBase.symbol_table, packetBase.symbol_code, course, false, out iconId);
                //For now ignore what comes out of the geticon call
                iconId = packetBase.SrcCallSignWithSSID + iconId;
                if (string.IsNullOrEmpty(iconId)) iconId = Guid.NewGuid().ToString();

                if (stationInfo.Icon != null)
                {
                    //Icons do not automatically update if you change the value in the depiction story image dictionary
                    //so things have to be forced a bit.this is second time this method is called
                    stationInfo.IconKey = iconId;
                    var prop = elemProto.GetPropertyByInternalName("IconPath");
                    if (prop != null)
                    {
                        var oldIconPath = prop.Value as DepictionIconPath;
                        if (oldIconPath != null) RemoveKeyValueIconFromIconLibrary(oldIconPath.Path);
                    }
                    //NOw that house keeping is done, do the rest
                    if (BitmapSaveLoadService.AddImageWithKeyInAppResources(stationInfo.Icon, stationInfo.IconKey, true))
                    {
                        var depIconPath = new DepictionIconPath(IconPathSource.File, stationInfo.IconKey);
                        CreateOrUpdateAPRSElementProperty(elemProto, "IconPath", "Icon path", depIconPath, false);
                    }
                }
            }


            if (packetBase.weatherInfo != null)
                UpdateOrWeatherInfoForStation(stationInfo, packetBase.weatherInfo, resetHoverText);
        }

        private static void UpdateStationFromPacket(APRSStationInfo stationInfo, APRSPacketBase packetBase, bool resetHoverText)
        {
            UpdateStation(stationInfo, packetBase, resetHoverText);
        }

        public void UpdateStationFromPacket(APRSPacketBase packetBase, bool resetHoverText)
        {
            UpdateStationFromPacket(this, packetBase, resetHoverText);
        }

        private static void UpdateOrWeatherInfoForStation(APRSStationInfo stationInfo, APRSWeatherInfo wi, bool resetHoverText)
        {
            if (!wi.windDirection.Equals(double.NaN))
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "WindDirection", "Wind Direction", wi.windDirection, resetHoverText);
            if (!wi.windSpeed.Equals(double.NaN))
            {
                var speed = new Speed(MeasurementSystem.Imperial, MeasurementScale.Normal, wi.windSpeed);
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "WindSpeed", "Wind Speed", speed, resetHoverText);
            }
            if (!wi.windGust.Equals(double.NaN))
            {
                var speed = new Speed(MeasurementSystem.Imperial, MeasurementScale.Normal, wi.windGust);
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "WindGust", "Wind Gust", speed, resetHoverText);
            }
            if (!wi.temperature.Equals(double.NaN))
            {
                var temp = new Temperature(MeasurementSystem.Imperial, MeasurementScale.Normal, wi.temperature);
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "Temperature", "Outdoor Temperature", temp, resetHoverText);
            }
            if (!wi.rainLastHour.Equals(double.NaN))
            {
                var dist = new Distance(MeasurementSystem.Imperial, MeasurementScale.Small, wi.rainLastHour);
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "RainLastHour", "Rain Last Hour", dist, resetHoverText);
            }
            if (!wi.rainLast24Hours.Equals(double.NaN))
            {
                var dist = new Distance(MeasurementSystem.Imperial, MeasurementScale.Small, wi.rainLast24Hours);
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "RainLast24Hours", "Rain Last 24 Hours", dist, resetHoverText);
            }
            if (!wi.rainSinceMidnight.Equals(double.NaN))
            {
                var dist = new Distance(MeasurementSystem.Imperial, MeasurementScale.Small, wi.rainSinceMidnight);
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "RainSinceMidnight", "Rain Since Midnight", dist, resetHoverText);
            }
            if (!wi.pressure.Equals(double.NaN))
            {
                //This might notbe right
                //                CreateOrUpdateAPRSElementProperty(stationInfo.Element, "BarometricPressure", "Barometric Pressure (mb)", (wi.pressure / 10).ToString());
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "BarometricPressure", "Barometric Pressure (mb)", wi.pressure, resetHoverText);
            }
            if (!wi.humidity.Equals(double.NaN))
            {
                CreateOrUpdateAPRSElementProperty(stationInfo.ElementPrototype, "Humidity", "Humidity (%)", wi.humidity, resetHoverText);
            }
        }

        #region property creationg helper

        //Ok this is pretty bad, but it is needed to make sure element icons get updated properly and to ensure that the number
        //of icons does not get out of control. This would not be needed if we a had a better way of manipulating an
        //icon at draw time.
        private static void RemoveKeyValueIconFromIconLibrary(string keyToRemove)
        {
            var app = Application.Current as IDepictionApplication; if (app != null)
            {
                var currentDepiction = app.CurrentDepiction;
                if (currentDepiction != null)
                {
                    var currentDepictionImageResources = currentDepiction.ImageResources;

                    if (!string.IsNullOrEmpty(keyToRemove) &&
                        currentDepictionImageResources.Contains(keyToRemove))
                    {
                        currentDepictionImageResources.Remove(keyToRemove);
                    }
                }
            }
        }
        private static IElementProperty CreateOrUpdateAPRSElementProperty(IElementPrototype elemProto, string propName, string propDisplayName, object propValue, bool useAsHoverText)
        {
            var existingProp = elemProto.GetPropertyByInternalName(propName);
            if (existingProp == null)
            {
                var prop = new DepictionElementProperty(propName, propDisplayName, propValue);
                prop.Deletable = false;
                prop.Editable = false;
                if (useAsHoverText) prop.IsHoverText = useAsHoverText;
                elemProto.AddPropertyOrReplaceValueAndAttributes(prop, false);// true);//was false
                return prop;
            }
            existingProp.SetPropertyValue(propValue);
            if (useAsHoverText) existingProp.IsHoverText = useAsHoverText;
            return existingProp;
        }

        #endregion

        public void ReceivedMessageFromStation(APRSMessage message)
        {
            lastMessage = message.Message;
            lastMessageReceivedTime = message.Timestamp;
            CreateOrUpdateAPRSElementProperty(ElementPrototype, "LastMessage", "Last Message", message.Message, false);
        }

        #region history stuff
        public void UpdatePositionHistory(LatitudeLongitude position, DateTime timestamp)
        {
            var posHist = new TimestampedPosition { Position = position, Time = timestamp };
            if (PositionHistory.Count > 0)
            {
                var lastPos = PositionHistory.Values[PositionHistory.Count - 1];
                if (lastPos.Position.Equals(posHist.Position))
                {
                    lastPos.Time = timestamp;
                }
                else
                {
                    PositionHistory.Add(posHist.Time, posHist);
                }
            }
            else
            {
                PositionHistory.Add(posHist.Time, posHist);
            }
        }
        //No clue what this is supposed to show, oh wait, i think this says how much
        //of the history to actually keep
        public void UpdateHistory(DateTime time)
        {
            DateTime startOfWindow = time - TimeSpan.FromMinutes(timeWindowFromConfig);//gatherer.TimeWindowMinutes);
            int delCount = 0;
            for (var i = 0; i < PositionHistory.Count; i++)
            {
                if (PositionHistory.Keys[i] > startOfWindow)
                {
                    delCount = i;
                    break;
                }
            }
            for (var i = 0; i < delCount; i++)
            {
                PositionHistory.RemoveAt(i);
            }
        }
        #endregion
        #region icon creationg helpers
        public static BitmapSource GetAPRSIconAndOverlay(char rawsymboltable, char rawSymbolCode, double course, bool isInactive, out string iconId)
        {
            var index = rawSymbolCode - 33;
            //            Console.WriteLine(string.Format("the table symbol {0} and the index {1}", rawsymboltable, index));
            //  if (index < 0) index += 33;//Increase by 32, semi magic number, it comes from the starting char of the 
            //aprs icon codes
            if (index < 0 || index > 95)
            {
                rawSymbolCode = (char)(125);
                index = rawSymbolCode - 33;//that is the no symbol index number
            }
            iconId = string.Empty;
            BitmapSource mainImageSource = primarySymbols;
            var overlayString = string.Empty;
            if (rawsymboltable.Equals('/'))
            {
                mainImageSource = primarySymbols;
                iconId += "p" + (int)rawSymbolCode;
            }
            else if (rawsymboltable.Equals('\\'))
            {
                mainImageSource = secondarySymbols;
                iconId += "s" + (int)rawSymbolCode;
            }
            else if (Regex.IsMatch(rawsymboltable.ToString(), "^[0-9]|[A-Z]"))
            {
                mainImageSource = secondarySymbols;
                overlayString = rawsymboltable.ToString();
                iconId += "s" + rawsymboltable.ToString() + ((int)rawSymbolCode).ToString();
            }
            if (!course.Equals(double.NaN)) iconId += "A" + course.ToString("0");
            var iconImage = GetCroppedAPRSIconFromLargerPNG(mainImageSource, index);
            return CreateImageOfCharAndOverlayOnMainImage(overlayString, iconImage, course, isInactive);
        }
        private static void DrawTextOnDrawingContext(DrawingContext drawingContext, string text, Brush color, FontWeight fw, double fs, int iconWidth, int iconHeight)
        {
            FormattedText fText = null;
            if (!string.IsNullOrEmpty(text))
            {
                fText = new FormattedText(text, new CultureInfo("en-us"), FlowDirection.LeftToRight, new Typeface(new FontFamily("Verdana"),
                                     FontStyles.Normal, fw, new FontStretch()), fs, color);
            }
            if (fText != null)
            {
                var tWidth = fText.Width;
                var tHeight = fText.Height;
                var lW = iconWidth - tWidth;
                var lH = iconHeight - tHeight;
                drawingContext.DrawText(fText, new Point(lW / 2d, lH / 2d));
            }
        }

        private static void DrawArrowOnDrawingContext(DrawingContext drawingContext, double course, Brush color, int brushWidth, double width, double height)
        {
            var geo = new StreamGeometry();
            double offset = 1;
            using (var ctx = geo.Open())
            {
                //                ctx.BeginFigure(new Point(width / 2, height), false, false);
                //                ctx.LineTo(new Point(width / 2, 0), true, true);
                ctx.BeginFigure(new Point(width / 16, height / 4 + offset), false, false);
                ctx.LineTo(new Point(width / 2, 0 + offset), true, true);
                ctx.LineTo(new Point(width * 15d / 16d, height / 4 + offset), true, true);
            }
            geo.Transform = new RotateTransform(-course, 9, 9);
            var drawing = new GeometryDrawing(Brushes.Transparent, new Pen(color, brushWidth), geo);
            drawingContext.DrawDrawing(drawing);
        }
        private static BitmapFrame CreateImageOfCharAndOverlayOnMainImage(string overlayString, BitmapSource mainImage, double course, bool isInactive)
        {
            var iconWidth = 18;
            var iconHeight = 18;

            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();
            var popCount = 0;
            //            if (course%360!=0)
            //            {
            //                var modCourse = course % 360;
            //                if (modCourse <= 90 || modCourse >= 270)
            //                {
            //                    if (modCourse >= 270)
            //                    {
            //                        modCourse -= 360;
            //                    }
            //                    modCourse *= -1;
            //                }
            //                else
            //                {
            //                    modCourse -= 180;
            //
            //                    drawingContext.PushTransform(new ScaleTransform(-1, 1, iconWidth / 2d, iconHeight / 2d));
            //                    popCount++;
            //                }
            //                //so zero is north, not east which means must subtract 90 from the calced course value.
            //                modCourse -= 90;
            //                drawingContext.PushTransform(new RotateTransform(modCourse, iconWidth / 2d, iconHeight / 2d));
            //                popCount++;
            //            }
            if (mainImage != null)
            {
                drawingContext.DrawImage(mainImage, new Rect(0, 0, iconWidth, iconHeight));
            }
            //            for (int i = 0; i < popCount; i++)
            //            {
            //                drawingContext.Pop();
            //            }
            DrawTextOnDrawingContext(drawingContext, overlayString, Brushes.Black, FontWeights.Bold, 12, iconWidth, iconHeight);
            if (isInactive)
            {
                DrawTextOnDrawingContext(drawingContext, "??", Brushes.DimGray, FontWeights.ExtraBold, 15, iconWidth, iconHeight);
            }
            else
            {
                if (!course.Equals(double.NaN))
                {
                    DrawArrowOnDrawingContext(drawingContext, course, Brushes.OrangeRed, 2, iconWidth, iconHeight);
                }
            }
            drawingContext.Close();
            // Render to bitmap.
            var rtBitmap = new RenderTargetBitmap(iconWidth, iconHeight, 96.0, 96.0, PixelFormats.Pbgra32);
            rtBitmap.Render(drawingVisual);
            rtBitmap.Freeze();
            var frame = BitmapFrame.Create(rtBitmap);
            frame.Freeze();
            return frame;
        }

        private static BitmapSource GetCroppedAPRSIconFromLargerPNG(BitmapSource multiBitmap, int index)
        {
            var numIconsLengthWise = 16;
            var numIconsHeightWise = 6;
            var iconSpacing = 1;

            var iconHeight = 20;
            var iconWidth = 20;
            var col = index % numIconsLengthWise;
            int row = index / numIconsLengthWise;
            int x = iconSpacing + iconSpacing * col + col * iconWidth;
            int y = iconSpacing + iconSpacing * row + row * iconHeight;
            try
            {
                var slice = new CroppedBitmap(multiBitmap, new Int32Rect(x, y, iconWidth, iconHeight));
                slice.Freeze();
                return slice;
                //                var bmi = BitmapSourceToBitmapImage(slice);   // need to do this because stuff saved in dpn file as bitmapimage,  not bitmapsource
                //                bmi.Freeze();
                //                return bmi;
            }
            catch (Exception ex)
            {
                var message = string.Format("Width {0} Height {1} Index {2} ImageWidth and height {3} {4} ", x, y, index, multiBitmap.Width, multiBitmap.Height);
                try
                {
                    DepictionExceptionHandler.HandleException(message, ex, false, true);
                }
                catch
                {
                    Console.WriteLine(message);
                    return null;
                }
                return null;
            }
        }
        #endregion
        //        public static BitmapImage BitmapSourceToBitmapImage(BitmapSource bitmapSource)//, BitmapFrame overlayFrame)
        //        {
        //            var encoder = new PngBitmapEncoder();
        //            var memoryStream = new MemoryStream();
        //            var bImg = new BitmapImage();
        //            
        ////            if (overlayFrame != null) encoder.Frames.Add(overlayFrame);
        //            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
        //            encoder.Save(memoryStream);
        //
        //            bImg.BeginInit();
        //            bImg.StreamSource = new MemoryStream(memoryStream.ToArray());
        //            bImg.EndInit();
        //
        //            memoryStream.Close();
        //
        //            return bImg;
        //        }
    }

    public class TimestampedPosition
    {
        public DateTime Time;
        public ILatitudeLongitude Position;
    }
}
