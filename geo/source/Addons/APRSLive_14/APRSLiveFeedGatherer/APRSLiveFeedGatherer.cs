using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using APRSLive.Core;
using APRSLiveConfiguration.Model;
using APRSLiveConfiguration.View;
using APRSLiveConfiguration.ViewModel;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using SerialPortConfigModel = APRSLive.Core.SerialPortConfigModel;

//The general principle behind this is having one config file that can be modified on the fly.
//that means having 2 aprslive services running does nothing
//The config is attached to the singleton gatherer
//Announcements, Bulletins, Messages, Alerts, Weather, and of course a map of all this activity including objects, satellites, nets, meetings, Hamfests, etc.
//types of aprs stations
namespace APRSLiveFeedGatherer
{
    [DepictionNonDefaultImporterMetadata("DepictionAPRSLiveAddin", DisplayName = "APRS Live Stations",
      ImporterSources = new[] { InformationSource.Web },
      Description = "The APRS Live data feed allows your APRS-enabled radio to feed APRS position data into Depiction.",
      Author = "Depictin Inc")]
    public class APRSLiveFeedGatherer : NondefaultDepictionImporterBase
    {
        private const string name = "APRS Live Stations";

        #region Variables

        private APRSLiveBackgroundService aprsService;

        private Window configWin;
        private APRSAddinConfig config;

        #endregion

        #region Propeties

        #region Properties for IDepictionAddonBase
        override public string AddonConfigViewText
        {
            get { return "Configure APRSLive"; }
        }

        #endregion
        
        public Window ConfigWin
        {
            get
            {
                configWin = GetNewConfigWin();
                return configWin;
            }
        }

        public string DisplayName
        {
            get { return name; }
        }

        public string HoverText
        {
            get
            {
                return "The APRS Live data feed allows your APRS-enabled radio to " +
                    "feed APRS position data into Depiction.";
            }
        }
        public string ToolTipText
        {
            get { return "APRS Live "; }
        }

        override public bool Activated { get { return true; } }

        override public object AddonConfigView { get { return ConfigWin; } }

        public int TimeWindowMinutes { get { return config.TimeWindowMinutes; } }

        public APRSAddinConfig Config
        {
            get { return config; }
            set
            {
                config = value;
            }
        }

        #endregion

        #region Constructor
        public APRSLiveFeedGatherer(): this(new APRSAddinConfig()){}

        public APRSLiveFeedGatherer(APRSAddinConfig config)
        {
            try
            {
                Config = config;
            }
            catch
            {

            }
        }
        #endregion

        #region Public methods
       
        //** This is the part that is turned on from the quickstart service
         
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            if (aprsService != null && aprsService.IsRunning)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("APRS Live is already running,please cancel APRS Live before continuing"));
                return;
            }
            if (!Config.UserHasSavedConfig)
            {
                var cfgw = GetNewConfigWin();
                var res = cfgw.ShowDialog();
                if (res != true)
                    return;
                else
                {
//                    config.UserHasSavedConfig = true;
                }
            }
            try
            {
                if (aprsService == null) aprsService = new APRSLiveBackgroundService(depictionRegion, Config);//packetIO);
                DepictionAccess.BackgroundServiceManager.AddBackgroundService(aprsService);
                aprsService.UpdateStatusReport(name);
                aprsService.StartBackgroundService(null);
            }
            catch (Exception e)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Cannot start APRS Live. Make sure to run the configuration utility before trying to start APRS Live: {0}", e.Message));
                throw;
            }
        }

        #endregion
        #region Config stuff

        private Window GetNewConfigWin()
        {
            if (config == null)//this should never happen
            {
                try
                {
                    config = new APRSAddinConfig();
                }
                catch
                {
                    return null;
                }
            }

            var configViewModel = new APRSAddinConfigViewModel(config)
            {
                SerialPortTestConnectionCommand = new DelegateCommand(OpenSerialPortTestConsole),
                OpenAboutBoxCommand = new DelegateCommand(OpenAboutBox),
                OpenOptionsDialogCommand = new DelegateCommand(OpenOptionsDialog),
                StopALCommand = new DelegateCommand(StopAPRSLive, IsAPRSLiveRunning)
            };
            var view = new APRSAddinConfigView
            {
                DataContext = configViewModel,
                RunPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            return view;
        }

        private void StopAPRSLive()
        {
            if (aprsService != null && aprsService.IsRunning)
            {
               aprsService.StopBackgroundService();
            }
        }

        private bool IsAPRSLiveRunning()
        {
            if (aprsService == null) return false;
            if (aprsService.IsRunning) return true;
            return false;
        }

        #endregion

        #region Dialog methods

        public void OpenSerialPortTestConsole()
        {
            if (aprsService!= null && aprsService.IsRunning)
            {
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("APRS Live is already running,please cancel APRS Live before continuing"));
                return;
            }
            var view = new SerialPortTestConsole
            {
                Config =
                    new SerialPortConfigModel(config.serialPortModel.PortName,
                                                             config.serialPortModel.Baud,
                                                             config.serialPortModel.Handshake,
                                                             config.serialPortModel.Parity)
            };
            view.ShowDialog();
        }
        public static void OpenAboutBox()
        {
            var view = new AboutBox { VersionString = APRSLiveVersion.GetVersionStr() };
            view.ShowDialog();
        }
        public static void OpenOptionsDialog()
        {
            var view = new OptionsDialog() ;
            view.ShowDialog();
        }
        #endregion

    }
}