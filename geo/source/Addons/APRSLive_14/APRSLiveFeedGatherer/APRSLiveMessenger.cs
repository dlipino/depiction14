﻿using System;
using System.Collections.Generic;
using APRSLiveFeedGatherer.Model;
using APRSPacketIO;
using Depiction.API;
using Depiction.API.ExceptionHandling;

namespace APRSLiveFeedGatherer
{
    public class APRSLiveMessenger
    {
        public event EventHandler<APRSMessageEventArgs> MessageReceived;
        private PacketIO packetIO;
        //Conversations that associated with each call sign.
        //Seems like every aprs station has a call sign and thus each station has a conversation associated with it
        //not sure how far back the history goes.
        private readonly Dictionary<string, Conversation> conversations = new Dictionary<string, Conversation>();
        private string userCallSign;

        #region properties
        public string UserCallSign
        {
            get { return userCallSign.ToUpperInvariant(); }
        }

        #endregion
        #region constructor
        public APRSLiveMessenger(string inCallSign, PacketIO packetDealer)
        {
            userCallSign = inCallSign;
            packetIO = packetDealer;
        }
        #endregion

        //How is the send message related to recieving
        public void SendMessage(APRSMessage msg)
        {
            var conv = GetConversation(msg.DestinationCallSign);
            conv.AddMessage(msg);
            CheckPacketIO();
            try
            {
                packetIO.Send(msg.Message, msg.DestinationCallSign);
            }
            catch (Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex, true, true);
            }
        }
        private void CheckPacketIO()
        {
            if (packetIO == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString("PacketIO is null and sending is not possible");
            }
        }

        public void ProcessMessage(string inMessage, string inFromCallSign, string inMessageId)
        {
            var message = string.Empty;
            if (inMessage != null) message = inMessage;

            var fromCallSign = string.Empty;
            if (inFromCallSign != null) fromCallSign = inFromCallSign.ToUpperInvariant();

            var messageId = string.Empty;
            if (inMessageId != null) messageId = inMessageId;

            if (message == "ack") return;

            var msg = new APRSMessage
            {
                DestinationCallSign = UserCallSign,
                FromCallSign = fromCallSign,
                Message = message,
                MessageID = messageId,
                Timestamp = DateTime.Now
            };
            //MOved to backgroundservice
            //            APRSStationInfo stationInfo;
            //            //What the heck are the collected elements?
            //            if (collectedElements.TryGetValue(fromCallSign, out stationInfo))
            //            {
            //                stationInfo.ReceivedMessageFromStation(msg);
            //            }

            var conversation = GetConversation(fromCallSign);
            if (conversation == null)
            {
                var eMessage = string.Format("Unable to find conversation for user {0}", fromCallSign);
                DepictionAccess.NotificationService.DisplayMessageString(eMessage, 4);
                return;
            }

            conversation.AddMessage(msg);
            if (MessageReceived != null)
            {
                MessageReceived(this, new APRSMessageEventArgs(msg));
            }
            CheckPacketIO();
            try
            {
                packetIO.SendAck(fromCallSign, msg.MessageID);
            }
            catch (Exception ex)
            {
                DepictionExceptionHandler.HandleException(ex, true, true);
            }
        }

        public Conversation GetConversation(string destCallSign)
        {
            Conversation conv;
            var errorMessage = string.Empty;
            var upperCall = destCallSign.ToUpperInvariant();
            if (!conversations.TryGetValue(upperCall, out conv))
            {
//                errorMessage = string.Format("No conversation found for {0}", upperCall);
                DepictionAccess.NotificationService.DisplayMessageString(errorMessage);
                conv = new Conversation();
                conversations.Add(upperCall, conv);
            }
            return conv;
        }
    }
}
