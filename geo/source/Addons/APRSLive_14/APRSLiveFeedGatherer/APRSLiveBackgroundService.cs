﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using APRS;
using APRS.FAPConnection;
using APRS.Listeners;
using APRSBase;
using APRSLiveConfiguration.Model;
using APRSLiveFeedGatherer.Behaviour;
using APRSLiveFeedGatherer.Model;
using APRSLiveFeedGatherer.View;
using APRSLiveFeedGatherer.ViewModel;
using APRSPacketIO;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.ValueTypes;

[assembly: InternalsVisibleTo("APRSTest")]
namespace APRSLiveFeedGatherer
{
    /***
     * this service is basically in charge of the PacketIO, which is what connects to the HAM radio input of choice.
     * The parameters of the PacketIO class are supposed to be changable on the fly.
     * Stopping the backgroundservice should stop the packetIO. The packetIO gets started and stopped withing the backgroundservice
     * in order to reset its properties.
     */
    public class APRSLiveBackgroundService : BaseDepictionBackgroundThreadOperation
    {
         const string APRSLiveElementName = "OLWorks.APRSLiveStation";
        private EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
        #region variables

        private APRSLiveMessenger _messengerService;
        private PacketIO packetIO;
        //        private Timer collectionStopWatch;
        //        private EventWaitHandle timeWaiter;
        private readonly Dictionary<string, APRSStationInfo> collectedStations = new Dictionary<string, APRSStationInfo>();
        private APRSAddinConfig config;
        private IMapCoordinateBounds regionBounds;
        private int TimeWindowMinutes = 15;//what is this?
        private bool isConnectedToElementRepository;
        public const string serviceNameConst = "APRSLiveService";

        #endregion

        #region properties
        internal APRSLiveMessenger MessengerService { get { return _messengerService; } }
        public bool IsRunning { get; private set; }//im pretty sure im duplicating something 
        public override bool IsServiceRefreshable
        {
            get { return false; }
        }

        public override string ServiceName { get { return serviceNameConst; } }

        #endregion

        #region constructor

        public APRSLiveBackgroundService(IMapCoordinateBounds regionOfInterest, APRSAddinConfig aprsConfig)//PacketIO packetIO)
        {
            IsRunning = false;
            regionBounds = regionOfInterest;
            config = aprsConfig;
        }

        void MessengerServiceMessageReceived(object sender, APRSMessageEventArgs e)
        {
            var message = e.Message;
            APRSStationInfo stationInfo;
            var processMessage = string.Format("Recieved message from {0}", message.FromCallSign);
            DepictionAccess.NotificationService.DisplayMessageString(processMessage, 3);
            //What the heck are the collected elements?
            if (collectedStations.TryGetValue(message.FromCallSign, out stationInfo))
            {
                stationInfo.ReceivedMessageFromStation(message);
            }
        }

        #endregion

        #region Public helpers
        public PacketIO CreatePacketIOFromConfig(APRSAddinConfig aprsConfig)
        {
            PacketEncoder encoder;
            IPacketListener listener;
            if (aprsConfig.InputType == APRSInputType.Serial)
            {
                listener = new SerialPacketListener(aprsConfig.serialPortModel.PortName, aprsConfig.serialPortModel.Baud, aprsConfig.serialPortModel.Parity,
                    8, StopBits.One, aprsConfig.serialPortModel.Handshake);
                encoder = new PacketEncoder(aprsConfig.CallSign);
            }
            else if (aprsConfig.InputType == APRSInputType.TCP)
            {
                var tcpListener = new TCPPacketListener(aprsConfig.tcpConfigModel.Host, aprsConfig.tcpConfigModel.Port, aprsConfig.CallSign, aprsConfig.tcpConfigModel.Password);
                tcpListener.SoftwareName = "APRSLive-Depiction";
                tcpListener.SoftwareVersion = "0.95";
                tcpListener.UseCustomFilter = aprsConfig.tcpConfigModel.UseCustomFilter;
                tcpListener.CustomFilter = aprsConfig.tcpConfigModel.CustomFilter;
                if (regionBounds != null)
                    tcpListener.SetCenter(regionBounds.Center.Latitude, regionBounds.Center.Longitude);
                listener = tcpListener;
                encoder = new TcpPacketEncoder(aprsConfig.CallSign);
            }
            else if (aprsConfig.InputType == APRSInputType.AGWPE)
            {
                var agwpeListener = new AGWPEListener(aprsConfig.agwpeConfigModel.RadioPort, aprsConfig.agwpeConfigModel.Host, aprsConfig.agwpeConfigModel.TCPPort, aprsConfig.CallSign);
                listener = agwpeListener;
                encoder = new PacketEncoder(aprsConfig.CallSign);
            }
            else
            {
                listener = new LoopbackPacketListener();
                encoder = new PacketEncoder(aprsConfig.CallSign);
            }

            //Uses the fap parser by default
            if (aprsConfig.UseAlternateParser)
            {
                listener.SetPacketParser(new PacketParser());
            }

            packetIO = new PacketIO(listener, encoder, aprsConfig.CallSign);
            return packetIO;
        }
        #endregion

        #region Overrides
        public override void RefreshResult() { }
        #region setup
        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            waitHandle.Reset();
            //            collectionStopWatch = new Timer(TimedEmailReader, this, 0, ConvertMinutesToMilliSeconds(RefreshRateMinutes));
            //            var millisecondsToSleep = randomNumberGenerator.Next(100, 200);
            //            var refreshRateMillisecond = (int)(RefreshRateMinutes * 60 * 1000);
            //            var waitTime = refreshRateMillisecond + millisecondsToSleep;
            //            collectionStopWatch.Change(waitTime, waitTime);
            //            timeWaiter.WaitOne();
            return SetupPacketIO();
        }
        internal bool SetupPacketIO()
        {
            if (config == null) return false;

            packetIO = CreatePacketIOFromConfig(config);
            _messengerService = new APRSLiveMessenger(config.CallSign, packetIO);
            _messengerService.MessageReceived += MessengerServiceMessageReceived;
            return true;
        }
        #endregion
        #region stuff for starting
        protected override object ServiceAction(object args)
        {
            if (!StartPackeService()) return false;
            //            var startMessage = string.Format("APRSLive session started at {0}", DateTime.Now);
            //            Logger.WriteLogItem(startMessage);
            IsRunning = true;
            waitHandle.WaitOne();
            return null;
        }

        internal bool StartPackeService()
        {
            if (packetIO == null) { return false; }
            //Start the packet getter
            try
            {
                var tcpListener = packetIO.Listener as TCPPacketListener;
                if (tcpListener != null && regionBounds != null)
                {
                    tcpListener.SetCenter(regionBounds.Center.Latitude, regionBounds.Center.Longitude);
                }

                packetIO.Start();
                packetIO.PacketReceived += _PacketListener_PacketReceived;
            }
            catch (Exception ex)
            {
                DepictionAccess.NotificationService.DisplayMessageString(ex.Message, 10);
                return false;
            }
            //Try and fill the collection with things that are already in the depiction, but why?
            AttachOrDetachSendCommandToStoryElements(true);

            //Used to make a test run smoother
            //            var currDepiction = DepictionAccess.CurrentDepiction;
            //            if (currDepiction != null)
            //            {
            //                var elements =
            //                    currDepiction.CompleteElementRepository.AllElements.Where(e => e.ElementType == "Depiction.Plugin.APRSLiveFeed");
            //                if (!isConnectedToElementRepository)
            //                {
            //                    isConnectedToElementRepository = true;
            //                    currDepiction.CompleteElementRepository.ElementListChange += CompleteElementRepository_ElementListChange;
            //                }
            //                try
            //                {
            //                    lock (collectedStations)
            //                    {
            //                        foreach (var element in elements)
            //                        {
            //                            var sInfo = new APRSStationInfo(TimeWindowMinutes) { Element = element };
            //                            string callSign;
            //                            if (element.GetPropertyValue("CallSign", out callSign))
            //                            {
            //                                if (collectedElements.ContainsKey(callSign))
            //                                {
            //                                    //actually not sure what is supposed to happen here, not sure if it is supposed
            //                                    //to replace the thing or not
            //
            //                                }
            //                                else
            //                                {
            //                                    collectedElements.Add(callSign, sInfo);
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //                catch
            //                {
            //
            //                }
            //            }
            return true;
        }
        protected void SetARPSLiveElementAccordingToAPRSStatus(bool stopping)
        {
            var currDepiction = DepictionAccess.CurrentDepiction;
            if (currDepiction == null) return;

            var elements =
                currDepiction.CompleteElementRepository.AllElements.Where(e => e.ElementType.Equals(APRSLiveElementName));
            if (stopping)
            {
                foreach (var element in elements)
                {
                    var iconString = element.GetPropertyByInternalName("IconSymbol").Value as string;
                    if (iconString == null) continue;
                    var iconSub = iconString.Split(',');
                    string iconId;
                    var icon = APRSStationInfo.GetAPRSIconAndOverlay(iconSub[0][0], iconSub[1][0], 0, true, out iconId);
                    var iconProp = element.GetPropertyByInternalName("IconPath");
                    var iconVal = iconProp.Value as DepictionIconPath;
                    if (iconVal == null) continue;
                    //Needed because depiction api will not trigger an event call because the iconVal.path doesnot actually
                    //change (we are changing the value in the library.
                    var notUpdatingName = iconVal.Path + "D";
                    if (BitmapSaveLoadService.AddImageWithKeyInAppResources(icon, notUpdatingName, true))
                    {
                        var depIconPath = new DepictionIconPath(IconPathSource.File, notUpdatingName);
                        element.SetPropertyValue("IconPath", depIconPath, true);
                    }
                }
            }
        }
        protected void AttachOrDetachSendCommandToStoryElements(bool attach)
        {
            var currDepiction = DepictionAccess.CurrentDepiction;
            if (currDepiction == null) return;

            var elements =
                currDepiction.CompleteElementRepository.AllElements.Where(e => e.ElementType.Equals(APRSLiveElementName));

            if (attach)
            {
                foreach (var element in elements)
                {
                    if (config.InputType == APRSInputType.TCP || config.InputType == APRSInputType.Serial)
                    {
                        var cmd = new DelegateCommand<IDepictionElement>(OpenSendMessageDialog);
                        cmd.Text = "Send Message";
                        var elemPar = element as DepictionElementParent;
                        if (elemPar != null)
                        {
                            var commands = elemPar.ElementCommands;
                            if (!commands.Contains(cmd))
                            {
                                elemPar.AddCommandToElement(cmd);
                            }
                        }
                    }
                }
            }
            else
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                    "Cannot currently detach send message command", 2);
                //This wont work with current API because we need a delete
                //                foreach (var element in elements)
                //                {
                //                    if (config.InputType == APRSInputType.TCP || config.InputType == APRSInputType.Serial)
                //                    {
                //                        var cmd = new DelegateCommand<IDepictionElement>(OpenSendMessageDialog);
                //                        cmd.Text = "Send Message";
                //                        ((DepictionElementParent)element).AddCommandToElement(cmd);
                //                    }
                //                }
            }
        }
        #endregion

        #region Ending hep methods

        public override void StopBackgroundService()
        {
            StopService();
            ServiceStopRequested = true;
            waitHandle.Set();

            UpdateStatusReport("Cancelling");
        }

        internal void StopService()
        {
            if (packetIO != null)
            {
                //                var startMessage = string.Format("APRSLive session stopped at {0}", DateTime.Now);
                //                Logger.WriteLogItem(startMessage);
                packetIO.Stop();
                packetIO.PacketReceived -= _PacketListener_PacketReceived;
                //                if (DepictionAccess.CurrentDepiction != null)
                //                {
                //                    DepictionAccess.CurrentDepiction.CompleteElementRepository.ElementListChange -= CompleteElementRepository_ElementListChange;
                //                    isConnectedToElementRepository = false;
                //                }
                packetIO = null;
                AttachOrDetachSendCommandToStoryElements(false);
            }
            SetARPSLiveElementAccordingToAPRSStatus(true);
        }

        protected override void ServiceComplete(object args)
        {
            ServiceStopRequested = false;
            IsRunning = false;
            Debug.WriteLine("done with service");
        }

        #endregion

        #endregion
        #region message dialog stuff (what ever that is)
        //What is this supposed to do,and how is it used?
        public void OpenSendMessageDialog(object commandData)
        {
            var element = commandData as IDepictionElement;

            if (element != null)
            {
                string callSign;
                if (element.GetPropertyValue("CallSign", out callSign))
                {
                    SendMessageDialogShowModal(callSign);
                }
            }
        }
        //So confused as to what this is supposed to do and why it cares about the change in call sign
        public void SendMessageDialogShowModal(string destCallSign)
        {
            var message = new APRSMessage
            {
                DestinationCallSign = destCallSign,
                FromCallSign = config.CallSign
            };
            Conversation conv = _messengerService.GetConversation(destCallSign);
            var view = new MessageWithConversationView();

            var mwconvModel = new MessageWithConversationModel(message, conv);
            var mwconvVM = new MessageWithConversationViewModel(mwconvModel, MessengerService);//, this);
            mwconvVM.DestinationCallsignChanged += mwconv_DestinationCallsignChanged;
            view.DataContext = mwconvVM;
            view.Show();
        }
        private void mwconv_DestinationCallsignChanged(object sender, EventArgs e)
        {
            var mwconvViewModel = sender as MessageWithConversationViewModel;
            if (mwconvViewModel != null)
            {
                var conv = _messengerService.GetConversation(mwconvViewModel.MessageViewModel.DestinationCallSign);
                if (conv != mwconvViewModel.Model.Conversation)
                {
                    mwconvViewModel.Model.Conversation = conv;
                    mwconvViewModel.ConversationViewModel = new ConversationViewModel(conv, MessengerService);//, this);
                }
            }
        }
        #endregion
        #region Event connectors

        /***
        * Seems to be the general packed listener. This is not controlled by the background service at all. Instead
        * the background service needs to stay running.
        * The packet listener appears to always be on and thus seems to not need a timer to recheck the status of what it is connectect to.
        * There appear to be 2 types of packets, the message type and the non message type
        * */
        private void _PacketListener_PacketReceived(object sender, PacketInfoEventArgs e)
        {
            var fromCallSign = e.PacketInfo.SrcCallSignWithSSID;
            if (e.PacketInfo.packetType == APRSPacketType.Message)
            {
                if (e.PacketInfo.dstCallSign == config.CallSign)
                {
                    _messengerService.ProcessMessage(e.PacketInfo.message, e.PacketInfo.SrcCallSignWithSSID, e.PacketInfo.messageId);
                }
            }
            else
            {//This assumes things are go from aprs to depiction. Not from depiction to aprs, not sure if the original 
                //aprs live did that, or if it should be a planned upgrade
                lock (collectedStations)
                {
                    APRSStationInfo stationInfo;
                    if (collectedStations.TryGetValue(fromCallSign, out stationInfo))
                    {
                        stationInfo.UpdateStationFromPacket(e.PacketInfo, false);
                    }
                    else
                    {
                        //don't know what the time window does
                        stationInfo = APRSStationInfo.CreateNewStationInfoFromPacket(10, e.PacketInfo);
                        if (stationInfo == null) return;
                    }
                    var proto = stationInfo.ElementPrototype;
                    var story = DepictionAccess.CurrentDepiction;
                    if (proto == null || story == null) return;
                    var elemRepo = story.CompleteElementRepository;
                    if (elemRepo == null) return;
                    var shouldAdd = true;
                    if (config.CropToRegion)
                    {
                        shouldAdd = false;
                        LatitudeLongitude latLong;
                        if (proto.GetPropertyValue("Position", out latLong) && latLong.IsValid)
                        {
                            var regionGeom = new DepictionGeometry(regionBounds);
                            if (regionGeom.Contains(new DepictionGeometry(latLong)))
                            {
                                shouldAdd = true;
                            }
                        }
                    }
                    if (shouldAdd)
                    {
                        if (collectedStations.ContainsKey(fromCallSign))
                        {
                            collectedStations[fromCallSign] = stationInfo;
                        }
                        else
                        {
                            collectedStations.Add(fromCallSign, stationInfo);
                        }
                        var prototypes = new List<IElementPrototype> { proto };
                        List<IDepictionElement> updatedElements;
                        List<IDepictionElement> createdElements;

                        elemRepo.AddOrUpdateRepositoryFromPrototypes(prototypes, false, out createdElements, out updatedElements);
                        foreach (var createdElement in createdElements)
                        {
                            //Hack to remove the displayname as initial hovertext//doesn work very well
                            //                            var prop = new DepictionElementProperty("DisplayName", "Name", "APRS Station");
                            //                            prop.Deletable = false;
                            //                            prop.Editable = true;
                            //                            prop.IsHoverText = false;
                            //                            createdElement.AddPropertyOrReplaceValueAndAttributes(prop, true);

                            if (config.InputType == APRSInputType.TCP || config.InputType == APRSInputType.Serial)
                            {
                                var cmd = new DelegateCommand<IDepictionElement>(OpenSendMessageDialog);
                                cmd.Text = "Send Message";
                                ((DepictionElementParent)createdElement).AddCommandToElement(cmd);
                            }
                        }
                        //Hack because the zoi is only drawn if the property is set on the element and 
                        //the update elements via prototypes does not read info about the element that it is updating
                        foreach (var updatedElement in updatedElements)
                        {

                            //the the display history prop
                            var drawZOIProp = updatedElement.GetPropertyByInternalName("DrawZOIFromHistory");
                            if (drawZOIProp != null)
                            {
                                List<string> dataPointsString;
                                List<ILatitudeLongitude> dataPoints = new List<ILatitudeLongitude>();
                                if (updatedElement.GetPropertyValue("RawPosHistory", out dataPointsString))
                                {
                                    var geom = CreateZOIFromPositionHistory.CreatePositionHistoryFromPositionStringList(
                                        dataPointsString);
                                    if (geom == null) continue;
                                    updatedElement.SetZOIGeometry(geom);
                                }
                            }
                        }
                        story.AddElementListToMainMap(createdElements);
                    }

                    //                            if (config.InputType == APRSInputType.TCP || config.InputType == APRSInputType.Serial)
                    //                            {
                    //                                var cmd = new DelegateCommand<IDepictionElement>(OpenSendMessageDialog);
                    //                                cmd.Text = "Send Message";
                    //                                ((DepictionElementParent)element).AddCommandToElement(cmd);
                    //                            }
                    //
                    //                            story.AddElementToDepictionElementList(element, true);
                    //                            collectedElements.Add(fromCallSign, stationInfo);
                    //                    }
                }
            }
        }
        #endregion
    }
}
