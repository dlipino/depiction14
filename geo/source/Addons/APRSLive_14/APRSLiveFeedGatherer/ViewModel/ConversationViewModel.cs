﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Threading;
using APRSLiveFeedGatherer.Model;
using Depiction.API.MVVM;

namespace APRSLiveFeedGatherer.ViewModel
{
    public class ConversationViewModel : ViewModelBase
    {
        //TODO figure out what this really does, and why does the messenger have to be passed into it
        private Conversation conversation;
        private APRSLiveMessenger hackmessenger;//need to figure out what this really does

        private ObservableCollection<APRSMessageVM> _messages;

        public ObservableCollection<APRSMessageVM> Messages
        {
            get { return _messages; }
            set { _messages = value; }
        }
        #region constructor
        public ConversationViewModel(Conversation conversation, APRSLiveMessenger messenger)//, APRSLiveFeedGatherer gatherer)
        {
            this.conversation = conversation;
            hackmessenger = messenger;
            conversation.CollectionChanged += conversation_CollectionChanged;
            Messages = new ObservableCollection<APRSMessageVM>();
            foreach (var message in conversation.Messages)
            {
                Messages.Add(new APRSMessageVM(message, hackmessenger));
            }

        }
        #endregion 
        void conversation_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Add) return;
            if (!Application.Current.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal,
                    new Action<object, NotifyCollectionChangedEventArgs>(conversation_CollectionChanged), sender, e);
                return;
            }

            foreach (APRSMessage message in e.NewItems)
            {
                Messages.Add(new APRSMessageVM(message, hackmessenger));//, gatherer));    
            }
        }
        ~ConversationViewModel()
        {
            conversation.CollectionChanged -= conversation_CollectionChanged;
        }

    }
}
