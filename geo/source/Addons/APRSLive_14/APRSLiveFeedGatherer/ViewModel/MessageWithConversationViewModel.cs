﻿using System;
using APRSLiveFeedGatherer.Model;
using Depiction.API.MVVM;

namespace APRSLiveFeedGatherer.ViewModel
{
    public class MessageWithConversationViewModel:  ViewModelBase
    {
        public event EventHandler DestinationCallsignChanged;
        private ConversationViewModel _conversationViewModel;

        public APRSMessageVM MessageViewModel { get; set; }
        public MessageWithConversationModel Model { get; set; }

        public ConversationViewModel ConversationViewModel
        {
            get { return _conversationViewModel; }
            set
            {
                _conversationViewModel = value;
                NotifyPropertyChanged("ConversationViewModel");
            }
        }

        public MessageWithConversationViewModel(MessageWithConversationModel model,APRSLiveMessenger messenger)//, APRSLiveFeedGatherer gatherer)
        {
            Model = model;
//            Gatherer = gatherer;
            MessageViewModel = new APRSMessageVM(model.Message,messenger);//, gatherer);
            MessageViewModel.DestinationCallsignChanged += MessageViewModel_DestinationCallsignChanged;
            _conversationViewModel = new ConversationViewModel(model.Conversation,messenger);//, gatherer);
        }

        private void MessageViewModel_DestinationCallsignChanged(object sender, EventArgs e)
        {
            if (DestinationCallsignChanged != null)
                DestinationCallsignChanged(this, new EventArgs());
        }
    }
}
