using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace APRSLiveFeedGatherer.Behaviour
{
    [Export(typeof(BaseBehavior))]
    [Behavior("CreateZOIFromPositionHistory", "Create ZOI From Position History", "Uses APRS elements position history to draw ZOI")]
    public class CreateZOIFromPositionHistory : BaseBehavior
    {
        //arg don't like having to add this, but depiction geometry does not give the option of
        //creating lines with more than 2 points
        private static readonly IGeometryFactory geometryFactory = new GeometryFactory();
        private static readonly ParameterInfo[] parameters =
            new[]{
                    new ParameterInfo("DrawZOIFromHistory", typeof (bool))
                        {ParameterName = "DrawZOIFromHistory", ParameterDescription = "Display the history"},
                    new ParameterInfo("RawPosHistory", typeof (List<string>))
                        {ParameterName = "RawPosHistory", ParameterDescription = "DateTime and location string form"}
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            object drawVal;
            bool createZOI = false;
            List<string> dataPointsString = new List<string>();
            var result = new BehaviorResult { SubscriberHasChanged = false };
            if (parameterBag.TryGetValue("DrawZOIFromHistory", out drawVal) && drawVal is bool)
            {
                createZOI = (bool)drawVal;
                if(!createZOI)
                {
                    SetGeometryToPoint(subscriber);
                    result.SubscriberHasChanged = true;
                    return result;
                }
            }else
            {
                return result;
            }

            object historyVal;
            if (parameterBag.TryGetValue("RawPosHistory", out historyVal) && historyVal is List<string>)
            {
                dataPointsString = (List<string>)historyVal;
            }
            else
            {
                return result;
            }
            var geom = CreatePositionHistoryFromPositionStringList(dataPointsString);
            if (geom == null)
            {
                SetGeometryToPoint(subscriber);
                return result;
            }
            result.SubscriberHasChanged = true;
            
            subscriber.SetPropertyValue("ZOIShapeType", ZOIShapeType.Line);
            subscriber.SetZOIGeometry(geom);
            return result;
        }

        private static void SetGeometryToPoint(IDepictionElement subscriber)
        {
            subscriber.SetPropertyValue("ZOIShapeType", ZOIShapeType.Point);
            subscriber.SetZOIGeometry(new DepictionGeometry(subscriber.Position));
        }

        public static IDepictionGeometry CreatePositionHistoryFromPositionStringList(List<string> dataPointsString )
        {
            if (dataPointsString==null || dataPointsString.Count < 2)return null;
            List<ILatitudeLongitude> dataPoints = new List<ILatitudeLongitude>();

            foreach (var strval in dataPointsString)
            {
                dataPoints.Add(new LatitudeLongitude(strval));
            }
            var coordList = new List<ICoordinate>();
            foreach(var dataPoint in dataPoints)
            {
                coordList.Add(new Coordinate(dataPoint.Longitude,dataPoint.Latitude));
            }
            return new DepictionGeometry(geometryFactory.CreateLineString(coordList.ToArray()));
        }

    }
}