﻿using System;

namespace APRSLiveFeedGatherer.Model
{
    public class APRSMessage
    {
        private string fromCallSign = string.Empty;
        private string destinationCallSign = string.Empty;
        #region properties
        public string FromCallSign
        {
            get { return fromCallSign.ToUpperInvariant(); }
            set { if (value != null) fromCallSign = value; }
        }
        public string DestinationCallSign
        {
            get { return destinationCallSign.ToUpperInvariant(); }
            set { if (value != null) destinationCallSign = value; }
        }
        public string Message { get; set; }
        public string MessageID { get; set; }
        public DateTime Timestamp { get; set; }
        #endregion
    }

    public class APRSMessageEventArgs : EventArgs
    {
        public APRSMessageEventArgs(APRSMessage msg)
        {
            Message = msg;
        }

        public APRSMessage Message { get; private set; }
    }
}
