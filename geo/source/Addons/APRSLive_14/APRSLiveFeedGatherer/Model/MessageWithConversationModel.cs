﻿namespace APRSLiveFeedGatherer.Model
{
    public class MessageWithConversationModel
    {
        public MessageWithConversationModel(APRSMessage msg, Conversation conv)
        {
            Message = msg;
            Conversation = conv;
        }
        public APRSMessage Message { get; set; }
        public Conversation Conversation { get; set; }
    }
}