using System;

namespace APRS.Types
{
    public class WeatherInfo
    {
        private const double KMH_TO_MPH = .621371;

        public double WindDirection { get; private set; }
        public double WindSpeed { get; private set; }
        public double WindGust { get; private set; }
        public double Temperature { get; private set; }
        public double RainLastHour { get; private set; }
        public double RainLast24Hours { get; private set; }
        public double RainSinceMidnight { get; private set; }
        public double Humidity { get; private set; }
        public double BarometricPressure { get; private set; }

        public WeatherInfo()
        {
            WindDirection = double.NaN;
            WindSpeed = double.NaN;
            WindGust = double.NaN;
            Temperature = double.NaN;
            RainLastHour = double.NaN;
            RainLast24Hours = double.NaN;
            RainSinceMidnight = double.NaN;
            BarometricPressure = double.NaN;
            Humidity = double.NaN;
        }

        public static WeatherInfo ParseWith7ByteWindData(string wString)
        {
            var wi = new WeatherInfo();
            if (wString.Length > 6)
            {
                wi.WindDirection = Convert.ToInt32(wString.Substring(0, 3));
                wi.WindSpeed = Convert.ToInt32(wString.Substring(4, 3));
            }
            ParseWeatherString(wString.Substring(7), wi);
            return wi;
        }

        public static WeatherInfo ParsePositionless(string wString)
        {
            var wi = new WeatherInfo();
            ParseWeatherString(wString, wi);
            return wi;
        }

        private static void ParseWeatherString(string wString, WeatherInfo wi)
        {
            var strLen = wString.Length;
            var len = 3;
            for (var i = 0; i < strLen; i += len + 1)
            {
                try
                {
                    switch (wString[i])
                    {
                        case 'c':
                            wi.WindDirection = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 's':
                            wi.WindSpeed = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'g':
                            wi.WindGust = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 't':
                            wi.Temperature = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'r':
                            wi.RainLastHour = Convert.ToInt32(wString.Substring(i + 1, len))/100d;
                            break;
                        case 'p':
                            wi.RainLast24Hours = Convert.ToInt32(wString.Substring(i + 1, len)) / 100d;
                            break;
                        case 'P':
                            wi.RainSinceMidnight = Convert.ToInt32(wString.Substring(i + 1, len)) / 100d;
                            break;
                        case 'h':
                            len = 2;
                            wi.Humidity = Convert.ToInt32(wString.Substring(i + 1, len));
                            break;
                        case 'b':
                            len = 5;
                            wi.BarometricPressure = Convert.ToInt32(wString.Substring(i + 1, len))/10d;
                            break;

                    }
                }
                catch (Exception e)
                {
                    continue;
                }


            }
        }

        public static WeatherInfo ParseUltimeter(string s)
        {
            var wi = new WeatherInfo();

            var pos = 0;
            wi.WindGust = (GetUltimeterFieldValue(s, pos)/10d)*KMH_TO_MPH;// * 0.0621371192);
            pos += 4;
            wi.WindDirection = GetUltimeterFieldValue(s, pos)*360d/255d;
            pos += 4;
            wi.Temperature = GetUltimeterFieldValue(s, pos) * 0.1;
            pos += 4;
            pos += 4;
            wi.BarometricPressure = GetUltimeterFieldValue(s, pos)/10d;
            pos += 4;
            pos += 4;
            pos += 4;
            pos += 4;
            wi.Humidity = GetUltimeterFieldValue(s, pos) * 0.1;
            pos += 4;
            pos += 4;
            pos += 4;
            wi.RainSinceMidnight = GetUltimeterFieldValue(s, pos)/100d;
            pos += 4;
            wi.WindSpeed = (GetUltimeterFieldValue(s, pos) / 10d) * KMH_TO_MPH;

            return wi;

        }

        private static double GetUltimeterFieldValue(string s, int pos)
        {
            try
            {
                var num= Convert.ToInt32(s.Substring(pos, 4), 16);
                if (num >= 32768) num -= 65536;
                return num;
            }
            catch (Exception e)
            {

            }
            return double.NaN;
        }
    }
}