﻿using System;
using System.Collections.Generic;
using APRS.Listeners;
using APRSBase;
using APRSFapCSharp;

namespace APRS.FAPConnection
{
    public class PacketParserFAP : PacketParserBase
    {
        public List<string> unparsedLines = new List<string>();
        private APRSPacketFAPDetails partialPacket;
        static FAP_APRSPacketParser parser = new FAP_APRSPacketParser();

        public override string ParserName
        {
            get { return "FAP Parser"; }
        }

        override public List<APRSPacketBase> GetPackets(string data)
        {
            if (msgFragment != null)
            {
                data = msgFragment + data;
                msgFragment = null;
            }
            var hasFragment = !lastLineComplete(data);

            //var lines = data.Split('\n');
            var lines = data.Split(new[] { '\r', '\n' });
            if (lines.Length > 0)
            {
                if (hasFragment)
                {
                    var lastLine = lines[lines.Length - 1];
                    msgFragment = lastLine;
                    lines[lines.Length - 1] = string.Empty;
                }
            }

            var packets = new List<APRSPacketBase>();
            foreach (var s in lines)
            {
                var line = s.TrimEnd('\r');
                if (line == string.Empty) continue;
                FireLineRecievedEvent(this, new LineReceivedEventArgs(line));
                APRSPacketFAPDetails pkt = null;
                var goodParse = false;
                if (partialPacket != null)
                {
                    pkt = partialPacket;
                    partialPacket = null;
                    //dont really know how this is supposed to work, is it supposed
                    //to contiue where it leaves off?
                    //if ((DateTime.UtcNow - pkt.ReceivedDate).TotalSeconds < 2)  // partial packets need to disappear after 2 seconds
                    if ((DateTime.UtcNow - pkt.receivedTime.ToUniversalTime()).TotalSeconds < 2)
                    {
                        try
                        {
                            goodParse = parser.FAP_APRSParser(ref pkt, line, true);
                            //pkt.ParseFromAX25Data(line);
                        }
                        catch (Exception e)
                        {
                            //continue;
                        }
                        if (goodParse)
                        {
                            packets.Add(ConversionHelpers.ConvertFAPPacketInfoToPacketBase(pkt));
                            continue;
                        }
                    }
                }

                if (!data.StartsWith("#"))
                {
                    goodParse = parser.FAP_APRSParser(ref pkt, line, false);
                }
                if (pkt != null)
                {
                    if (goodParse)
                        packets.Add(ConversionHelpers.ConvertFAPPacketInfoToPacketBase(pkt));
                    else
                        partialPacket = pkt;
                }
                else
                {
                    //nothing is done?
                    unparsedLines.Add(s);
                }
            }
            return packets;
        }

        public override APRSPacketBase ParseRawPacket(APRSPacketRaw rawPacket)
        {
            var fapPacket = new APRSPacketFAPDetails();
            fapPacket.header = string.Empty;
            fapPacket.src_callsign = rawPacket.CallSign;
            fapPacket.dst_callsign = rawPacket.Destination;
            fapPacket.destination = rawPacket.Destination;
            fapPacket.body = rawPacket.RawMessage;
            var parser = new FAP_APRSPacketParser();
            parser.FAP_ParseBody(fapPacket, rawPacket.RawMessage, false, false);
            return ConversionHelpers.ConvertFAPPacketInfoToPacketBase(fapPacket);
        }

        private static bool lastLineComplete(string data)
        {
            return data.EndsWith("\n");
        }
    }
}