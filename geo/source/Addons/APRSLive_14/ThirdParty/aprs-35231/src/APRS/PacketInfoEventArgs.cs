﻿using System;
using APRSBase;

namespace APRS
{
    public class PacketInfoEventArgs : EventArgs
    {
        public APRSPacketBase PacketInfo { get; private set; }
        public PacketInfoEventArgs(APRSPacketBase pi)
        {
            PacketInfo = pi;
        }

//        public PacketInfoEventArgs(PacketInfo pi)
//        {
//            PacketInfo = pi;
//        }
//
//        public PacketInfo PacketInfo { get; private set; }
    }
}