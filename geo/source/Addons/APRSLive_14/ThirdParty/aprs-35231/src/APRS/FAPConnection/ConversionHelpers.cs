﻿using APRS.Types;
using APRSBase;
using APRSFapCSharp;

namespace APRS.FAPConnection
{
    public class ConversionHelpers
    {
        #region converters for the original
        static public APRSPacketBase ConvertPacketInfoToPacketBase(PacketInfo inPacket)
        {
            var basePacket = new APRSPacketBase();
            basePacket.SrcCallSignWithSSID = inPacket.Callsign;
            //Hopefully these are actually the same thing
            basePacket.dstCallSign = inPacket.MessageRecipient;
            basePacket.destination = inPacket.MessageRecipient;
            basePacket.message = inPacket.Message;
            basePacket.messageId = inPacket.MessageID;
            basePacket.recievedTime = inPacket.ReceivedDate;
            basePacket.sentTime = inPacket.SentDate;

            if(inPacket.DataType.Equals(DataType.Message))
            {
                basePacket.packetType = APRSPacketType.Message;
            }else
            {
                basePacket.packetType = APRSPacketType.Undefined;
            }

            basePacket.symbol_code = inPacket.SymbolCode;
            basePacket.symbol_table = inPacket.SymbolTable;
           
            if (inPacket.DataType.Equals(DataType.Message))
            {
                basePacket.packetType = APRSPacketType.Message;
            }

            if (inPacket.Latitude != null) basePacket.latitude = inPacket.Latitude.Value;
            if (inPacket.Longitude != null) basePacket.longitude = inPacket.Longitude.Value;
            if (inPacket.Speed != null) basePacket.speed = inPacket.Speed.KilometersPerHour / 1000;
            if (inPacket.Direction != null) basePacket.course = inPacket.Direction.ToDegrees();
            if (inPacket.Altitude != null) basePacket.altitude = inPacket.Altitude.MetersAboveSeaLevel;

            basePacket.weatherInfo = ConvertWeatherInfoToAPRSWeatherInfo(inPacket.WeatherInfo);

            return basePacket;
        }

        static public APRSWeatherInfo ConvertWeatherInfoToAPRSWeatherInfo(WeatherInfo info)
        {
            if (info == null) return null;
            var newInfo = new APRSWeatherInfo();
            newInfo.windDirection = info.WindDirection;
            newInfo.windGust = info.WindGust;
            newInfo.windSpeed = info.WindSpeed;
            newInfo.temperature = info.Temperature;
            newInfo.rainLast24Hours = info.RainLast24Hours;
            newInfo.rainLastHour = info.RainLastHour;
            newInfo.rainSinceMidnight = info.RainSinceMidnight;
            newInfo.humidity = info.Humidity;
            newInfo.pressure = info.BarometricPressure;

            return newInfo;
        }
        #endregion

        #region FAP converters
        static public APRSPacketBase ConvertFAPPacketInfoToPacketBase(APRSPacketFAPDetails inPacket)
        {
            var basePacket = new APRSPacketBase();

            basePacket.SrcCallSignWithSSID = inPacket.src_callsign;
            basePacket.dstCallSign = inPacket.dst_callsign;
            basePacket.destination = inPacket.destination;
            basePacket.message = inPacket.message;
            basePacket.messageId = inPacket.message_id;
            basePacket.recievedTime = inPacket.receivedTime;
            basePacket.sentTime = inPacket.sentTime;

            basePacket.symbol_code = inPacket.symbol_code;
            basePacket.symbol_table = inPacket.symbol_table;

            basePacket.packetType = APRSPacketType.Undefined;
            if (inPacket.fap_packet_type.Equals(PacketType.fapMESSAGE))
            {
                basePacket.packetType = APRSPacketType.Message;
            }

            basePacket.latitude = inPacket.latitude;
            basePacket.longitude = inPacket.longitude;

            basePacket.speed = inPacket.speed;
            basePacket.course = inPacket.course;
            basePacket.altitude = inPacket.altitude;

            basePacket.weatherInfo = ConvertFAPWeatherInfoToAPRSWeatherInfo(inPacket.wx_report);

            return basePacket;
        }
        static public APRSWeatherInfo ConvertFAPWeatherInfoToAPRSWeatherInfo(APRSFapWeatherInfo info)
        {
            if (info == null) return null;
            var newInfo = new APRSWeatherInfo();
            newInfo.windDirection = info.WindDirection;
            newInfo.windGust = info.WindGust;
            newInfo.windSpeed = info.WindSpeed;
            newInfo.temperature = info.Temperature;
            newInfo.indoorTemperature = info.IndoorTemperature;
            newInfo.rainLast24Hours = info.RainLast24Hours;
            newInfo.rainLastHour = info.RainLastHour;
            newInfo.rainSinceMidnight = info.RainSinceMidnight;
            newInfo.humidity = info.Humidity;
            newInfo.insideHumidity = info.InsideHumidity;
            newInfo.pressure = info.Pressure;
            newInfo.SoftwareIndicator = info.SoftwareIndicator;

            return newInfo;
        }
        #endregion

    }
}