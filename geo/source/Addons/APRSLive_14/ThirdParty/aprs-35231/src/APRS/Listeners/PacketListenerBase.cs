using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using APRS.FAPConnection;
using APRSBase;
using Depiction.API.ExceptionHandling;

namespace APRS.Listeners
{
    public interface IPacketListener
    {
        event EventHandler<PacketInfoEventArgs> PacketReceived;
        event EventHandler<LineReceivedEventArgs> LineReceived;
        bool IsListening { get; }
        string CallSign { get; set; }
        void Start();
        void Stop();
        void SendString(string str);
        void SetPacketParser(PacketParserBase parserToUse);
    }

    public class PacketListenerBase : IPacketListener
    {
        #region Events

        public event EventHandler<LineReceivedEventArgs> LineReceived;
        public event EventHandler<PacketInfoEventArgs> PacketReceived;
        #endregion

        protected const int bufSize = 256;
        protected virtual char TerminationChar { get { return '\n'; } }
        protected bool closed = true;
        protected readonly byte[] readBuffer = new byte[bufSize];
        public virtual Stream GetReadStream() { return null; }
        protected PacketParserBase packetParser;
        private bool isParserAttached = false;
        #region properties

        public string CallSign { get; set; }

        #endregion

        public virtual bool IsListening { get { return false; } }

        public virtual void Start() { BeginListening(); }

        public virtual void Stop()
        {
            if (closed)
                return;
            closed = true;
            var stream = GetReadStream();
            if (stream != null)
                stream.Close();
        }

        #region constructor

        public PacketListenerBase()
        {
            SetPacketParser(new PacketParserFAP());
        }

        #endregion
        #region public helpers

        public void SetPacketParser(PacketParserBase parserToUse)
        {
            if (parserToUse == null) return;
            if (packetParser != null)
            {
                packetParser.LineReceived -= packetParser_LineReceived;
                isParserAttached = false;
                packetParser = null;
            }
            if (!isParserAttached)
            {
                packetParser = parserToUse;
                isParserAttached = true;
                packetParser.LineReceived += packetParser_LineReceived;
            }
        }

        #endregion
        //Seems kind of hokie
        //        private void AttachEventsToPacketParser()
        //        {
        //            if(!isParserAttached)
        //            {
        //                isParserAttached = true;
        //                packetParser.LineReceived += packetParser_LineReceived;
        //            }
        //        }

        private void packetParser_LineReceived(object sender, LineReceivedEventArgs e)
        {
            if (LineReceived != null) LineReceived(this, e);
        }

        protected void BeginListening()
        {
            closed = false;
            var stream = GetReadStream();
            stream.BeginRead(readBuffer, 0, bufSize, receiveCallback, null);
        }

        public virtual void SendString(string str)
        {
            Console.WriteLine("Sending " + str);
            str += TerminationChar;
            var bytes = Encoding.ASCII.GetBytes(str);
            var stream = GetWriteStream();

            stream.Write(bytes, 0, bytes.Length);
            stream.Flush();
        }

        private void receiveCallback(IAsyncResult ar)
        {
            if (closed) return;
            var stream = GetReadStream();
            lock (this)
            {
                var bytesRead = stream.EndRead(ar);

                string data = null;
                if (bytesRead > 0)
                {
                    data = Encoding.ASCII.GetString(readBuffer, 0, bytesRead);
                }
                //copy data quickly out of buffer, then trigger asynchronous read again
                //sometimes there is a race condition in the stop and the read, hence the try/catch
                try
                {
                    if (stream.CanRead && !closed)
                    {
                        stream.BeginRead(readBuffer, 0, bufSize, receiveCallback, null);
                    }
                }
                catch(Exception ex)
                {
                    DepictionExceptionHandler.HandleException("In receivecallback", ex, true, true);
                }

                if (data != null)
                {
                    List<APRSPacketBase> packets = packetParser.GetPackets(data);
                    if (PacketReceived != null)
                    {
                        foreach (var pkt in packets)
                        {
                            PacketReceived(this, new PacketInfoEventArgs(pkt));
                        }
                    }
                }
            }
        }

        public virtual Stream GetWriteStream()
        {
            return GetReadStream();
        }
    }

    public class LineReceivedEventArgs : EventArgs
    {
        public string Line { get; set; }
        public LineReceivedEventArgs(string line)
        {
            Line = line;
        }
    }
}