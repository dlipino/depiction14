﻿namespace APRS
{
    public enum APRSInputType
    {
        Serial,
        TCP,
        Loopback,
        AGWPE
    }
}
