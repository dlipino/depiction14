﻿using System;
using System.Collections.Generic;
using APRS.Listeners;
using APRSBase;

namespace APRS.FAPConnection
{
    abstract public class PacketParserBase
    {
        public event EventHandler<LineReceivedEventArgs> LineReceived;
        public abstract string ParserName { get; }
        public string msgFragment = string.Empty;
        public abstract List<APRSPacketBase> GetPackets(string data);
        public void FireLineRecievedEvent(object sender,LineReceivedEventArgs args)
        {
            if (LineReceived != null)
                LineReceived(this, args);
        }
        public abstract  APRSPacketBase ParseRawPacket(APRSPacketRaw rawPacket);
    }
}