﻿using System;
using System.Collections.Generic;
using APRS.FAPConnection;
using APRS.Listeners;
using APRSBase;


namespace APRS
{
    public class PacketParser : PacketParserBase
    {
        public List<string> unparsedLines = new List<string>();
        private PacketInfo partialPacket;

        public override string ParserName
        {
            get { return "Codeplex parser"; }
        }

        override public List<APRSPacketBase> GetPackets(string data)
        {
            if (msgFragment != null)
            {
                data = msgFragment + data;
                msgFragment = null;
            }
            var hasFragment = !lastLineComplete(data);

            //var lines = data.Split('\n');
            var lines = data.Split(new[] { '\r', '\n' });
            if (lines.Length > 0)
            {
                if (hasFragment)
                {
                    var lastLine = lines[lines.Length - 1];
                    msgFragment = lastLine;
                    lines[lines.Length - 1] = string.Empty;
                }
            }

            var packets = new List<APRSPacketBase>();
            foreach (var s in lines)
            {
                var line = s.TrimEnd('\r');
                if (line == string.Empty) continue;
                FireLineRecievedEvent(this, new LineReceivedEventArgs(line));
                PacketInfo pkt;
                if (partialPacket != null)
                {
                    pkt = partialPacket;
                    partialPacket = null;
                    if ((DateTime.UtcNow - pkt.ReceivedDate.ToUniversalTime()).TotalSeconds < 2)  // partial packets need to disappear after 2 seconds
                    {
                        try
                        {
                            pkt.ParseFromAX25Data(line);
                        }
                        catch (Exception e)
                        {
                            //continue;
                        }
                        if (pkt.DataParsed)
                        {
                            packets.Add(ConversionHelpers.ConvertPacketInfoToPacketBase(pkt));
                            continue;
                        }
                    }
                }
                pkt = ParseLine(line);
                if (pkt != null)
                    if (pkt.DataParsed)
                        packets.Add(ConversionHelpers.ConvertPacketInfoToPacketBase(pkt));
                    else
                        partialPacket = pkt;
                else
                {
                    unparsedLines.Add(s);
                }
            }
            return packets;
        }

        public override APRSPacketBase ParseRawPacket(APRSPacketRaw rawPacket)
        {
            var packetInfo = PacketInfo.ParseFromRawPacket(rawPacket);
            return ConversionHelpers.ConvertPacketInfoToPacketBase(packetInfo);
        }

        private static bool lastLineComplete(string data)
        {
            return data.EndsWith("\n");
        }

        public PacketInfo ParseLine(string data)
        {
            if (data.StartsWith("#"))
                return null;

            try
            {
                return PacketInfo.ParseFromString(data);
            }
            catch (Exception e)
            {
                //TODO: log/show an error
                return null;
            }
        }

        //        public APRSPacketBase ParseLine(string data)
        //        {
        //            if (data.StartsWith("#"))
        //                return null;
        //
        //            try
        //            {
        //                var pkt = new PacketInfo(data);
        //                return ConversionHelpers.ConvertPacketInfoToPacketBase(pkt);
        //
        //            }
        //            catch (Exception e)
        //            {
        //                //TODO: log/show an error
        //                return null;
        //            }
        //        }
    }
}
