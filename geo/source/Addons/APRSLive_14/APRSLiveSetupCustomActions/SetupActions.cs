﻿using System;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Management.Instrumentation;
using System.Security.AccessControl;
using Microsoft.Win32;


namespace APRSLiveSetupCustomActions
{
    [RunInstaller(true)]
    public partial class SetupActions : Installer
    {
        private const string addonsInDepictionDirName = "Add-ons";
        private static string aprsLiveBaseProductName = "APRSLive";
        private string aprsLiveInstalledProductName = string.Empty;
        private const string helpDir = "Help";
        private const string elementsDir = "Elements";
        public SetupActions()
        {
            InitializeComponent();
        }
        public override void Install(System.Collections.IDictionary stateSaver)
        {
            base.Install(stateSaver);
            //            Debugger.Break();
            //            Directory.CreateDirectory(Path.Combine(depictionInstallDir, "andme"));
        }
        public override void Uninstall(System.Collections.IDictionary savedState)
        {
            base.Uninstall(savedState);
            // /APRSBaseDir=[PRODUCTNAME]  didnt seem to work
            aprsLiveInstalledProductName = Context.Parameters["ProductName"];
            var depictionDir = GetDepictionInstallDirectory();
            if (string.IsNullOrEmpty(aprsLiveInstalledProductName))
            {
                aprsLiveInstalledProductName = aprsLiveBaseProductName;
            }

            if (!string.IsNullOrEmpty(depictionDir))
            {
                DeleteAPRSLiveFromDepictionAddinsDirectory(depictionDir, aprsLiveInstalledProductName);
            }
        }
        //The default values attempt to delete the .99.01 directory
        private static void DeleteAPRSLiveFromDepictionAddinsDirectory(string depictionDir, string discoveredAPRSProductName)
        {
            if (string.IsNullOrEmpty(depictionDir))
            {
                depictionDir = GetDepictionInstallDirectory();
            }
            var aprsProductName = discoveredAPRSProductName;
            if (string.IsNullOrEmpty(aprsProductName)) aprsProductName = aprsLiveBaseProductName;
            var aprsLiveDirInDepictionDir = Path.Combine(depictionDir, addonsInDepictionDirName, aprsProductName);
            if (Directory.Exists(aprsLiveDirInDepictionDir))
            {
                Directory.Delete(aprsLiveDirInDepictionDir, true);
            }
        }

        public override void Commit(System.Collections.IDictionary savedState)
        {
            var depictionInstallDir = GetDepictionInstallDirectory();
            if(string.IsNullOrEmpty(depictionInstallDir))
            {
                throw new InstanceNotFoundException("Could not locate Depiction 1.4.");
            }
            base.Commit(savedState);
            //            GiveUserPrivilegesToDirectory(depictionInstallDir);
            //put into the custom actions: /TARGETDIR="[TARGETDIR]\"//what is needed
            string aprsBaseInstallDir = Context.Parameters["TARGETDIR"];
            //            if (string.IsNullOrEmpty(aprsBaseInstallDir))
            //            {
            //                Directory.CreateDirectory(Path.Combine(depictionInstallDir, "empty2baseinstall"));
            //            }

            // /ProductName="[ProductName]"  in the CustomActionData
            aprsLiveInstalledProductName = Context.Parameters["ProductName"];
            //            if (string.IsNullOrEmpty(aprsLiveInstalledProductName))
            //            {
            //                Directory.CreateDirectory(Path.Combine(depictionInstallDir, "emptybaseinstall"));
            //            }
            //Delete previous version which does nto seem to get uninstalled correctly
            DeleteAPRSLiveFromDepictionAddinsDirectory(depictionInstallDir, aprsLiveInstalledProductName);
            if (!string.IsNullOrEmpty(aprsBaseInstallDir) && !string.IsNullOrEmpty(aprsLiveInstalledProductName))
            {
                CopyAPRSToDepictionDirAddonDirAndUpdatePrivilages(aprsBaseInstallDir, aprsLiveInstalledProductName);
            }
        }

        public static void CopyAPRSToDepictionDirAddonDirAndUpdatePrivilages(string aprsTargetInstallDir, string aprsProductName)//string installDir,string aprsInstallDir)
        {
            if (string.IsNullOrEmpty(aprsTargetInstallDir) || string.IsNullOrEmpty(aprsProductName)) return;

            var depictionInstallDir = GetDepictionInstallDirectory();

            if (!string.IsNullOrEmpty(depictionInstallDir))
            {
                var addonDir = Path.Combine(depictionInstallDir, addonsInDepictionDirName);
                GiveUserPrivilegesToDirectory(addonDir);

                var aprsLiveAddonDir = Path.Combine(addonDir, aprsProductName);
                GiveUserPrivilegesToDirectory(aprsLiveAddonDir);
                CopyAPRSLiveInstallToDepictionAddon(Path.Combine(aprsTargetInstallDir, aprsLiveBaseProductName),
                                                    aprsLiveAddonDir);
                //                CopyDirectoryAndFiles(Path.Combine(aprsTargetInstallDir, aprsLiveBaseProductName), aprsLiveAddonDir);
                //                CopyDirectoryAndFiles(Path.Combine(aprsTargetInstallDir, helpDir), Path.Combine(aprsLiveAddonDir, helpDir));
            }
        }

        private static void GiveUserPrivilegesToDirectory(string dir)
        {
            DirectorySecurity dirSec;
            FileSystemAccessRule fsar;
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            dirSec = Directory.GetAccessControl(dir);
            fsar = new FileSystemAccessRule("Users"
                                            , FileSystemRights.FullControl
                                            , InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit
                                            , PropagationFlags.None
                                            , AccessControlType.Allow);
            dirSec.AddAccessRule(fsar);
            Directory.SetAccessControl(dir, dirSec);
        }

        private static void CopyAPRSLiveInstallToDepictionAddon(string srcDir, string dstDir)
        {
            //The APRSLive dir should have dirs of help and elements. Files of *.dll and a .rtf

            var srcDirInfo = new DirectoryInfo(srcDir);
            var dstDirInfo = new DirectoryInfo(dstDir);
            CopyAll(srcDirInfo,dstDirInfo,"*.dll");
        }

        public static void CopyAll(DirectoryInfo source, DirectoryInfo target,string searchPattern)
        {
            if (source.FullName.ToLower() == target.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it. 
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }
            var sp = "*.*";
            if (!string.IsNullOrEmpty(searchPattern)) sp = searchPattern;
            // Copy each file into it's new directory. 
            foreach (var fi in source.GetFiles(sp))
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion. 
            foreach (var diSourceSubDir in source.GetDirectories())
            {
                var nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir,null);
            }
        }

        private static string GetDepictionInstallDirectory()
        {
            var depictionInstallDir = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Depiction Inc\Depiction 1.4", "Location", "");
            if (depictionInstallDir == "")
            {
                depictionInstallDir = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Wow6432Node\Depiction Inc\Depiction 1.4", "Location", "");
                if (depictionInstallDir.Equals(string.Empty)) return string.Empty;
            }
            return depictionInstallDir;
        }

        private static void CopyDirectoryAndFiles(string srcDirectory, string destDirectory, string searchPattern)
        {
            try
            {
                if (!Directory.Exists(destDirectory))
                {
                    Directory.CreateDirectory(destDirectory);
                }
                var dirInfo = new DirectoryInfo(srcDirectory);
                var sp = "*.*";
                if (!string.IsNullOrEmpty(searchPattern)) sp = searchPattern;
                foreach (var patternMatchFiles in dirInfo.GetFiles(sp))
                {
                    var destFileName = Path.Combine(destDirectory, patternMatchFiles.Name);
                    if (File.Exists(destFileName))
                    {
                        File.Delete(destFileName);
                    }
                    File.Copy(patternMatchFiles.FullName, destFileName);
                }
                //                foreach (var originalFullFileName in Directory.GetFiles(srcDirectory))
                //                {
                //                    var originalFileName = Path.GetFileName(originalFullFileName);
                //                    var extension = Path.GetExtension(originalFileName);
                //                    var matchExtension = !string.IsNullOrEmpty(extensionToMatch) && !string.IsNullOrEmpty(extension) &&
                //                                         extension.Equals(extensionToMatch, StringComparison.InvariantCultureIgnoreCase);
                //                    if (!string.IsNullOrEmpty(originalFileName) && matchExtension)
                //                    {
                //
                //                        var destFileName = Path.Combine(destDirectory, originalFileName);
                //                        if (File.Exists(destFileName))
                //                        {
                //                            File.Delete(destFileName);
                //                        }
                //                        File.Copy(originalFullFileName, destFileName);
                //                    }
                //                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
