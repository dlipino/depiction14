using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using APRS;
using APRSLiveConfiguration.Model;
using Depiction.API.MVVM;

namespace APRSLiveConfiguration.ViewModel
{
    public class APRSAddinConfigViewModel : DialogViewModelBase
    {
        private string selectedInput;
        private static Dictionary<string, APRSInputType> configNameToType;
        private ICommand openAboutBoxCommand;
        private ICommand openOptionsDialogCommand;
        private ICommand helpCommand;
        private ICommand okCommand;
        private ICommand stopALCommand;

        #region Properties

        public APRSAddinConfig Config { get; private set; }

        public List<string> InputConfigs { get; set; }
        public TCPConfigViewModel TcpConfigViewModel { get; set; }
        public SerialPortConfigViewModel SerialPortConfigViewModel { get; set; }
        public AGWPEConfigViewModel AGWPEConfigViewModel { get; set; }

        public Visibility SerialPortConfigVisible
        {
            get
            {
                return Config.InputType == APRSInputType.Serial ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public bool CropToRegion
        {
            get { return Config.CropToRegion; }
            set { Config.CropToRegion = value; NotifyPropertyChanged("CropToRegion"); }
        }
        public bool UseAlternateParser
        {
            get { return Config.UseAlternateParser; }
            set { Config.UseAlternateParser = value; NotifyPropertyChanged("UseAlternateParser"); }
        }
        public bool UseUTCTime
        {
            get { return Config.UseUTCTime; }
            set { Config.UseUTCTime = value; NotifyPropertyChanged("UseUTCTime"); }
        }
        public Visibility TCPConfigVisible
        {
            get
            {
                return Config.InputType == APRSInputType.TCP ? Visibility.Visible : Visibility.Collapsed;
            }
        }
        public Visibility AGWPEConfigVisible
        {
            get
            {
                return Config.InputType == APRSInputType.AGWPE ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public APRSInputType InputType
        {
            get { return Config.InputType; }
        }

        public string CallSign
        {
            get { return Config.CallSign; }
            set { Config.CallSign = value; NotifyPropertyChanged("CallSign"); }
        }


        public string SelectedInput
        {
            get { return selectedInput; }
            set
            {
                selectedInput = value;
                Config.InputType = configNameToType[selectedInput];
                NotifyPropertyChanged("SelectedInput");
                NotifyPropertyChanged("InputType");
                NotifyPropertyChanged("TCPConfigVisible");
                NotifyPropertyChanged("SerialPortConfigVisible");
                NotifyPropertyChanged("AGWPEConfigVisible");
            }
        }
        #region Commands

        public ICommand StopALCommand
        {
            get { return stopALCommand; }
            set
            {
                stopALCommand = value;
                NotifyPropertyChanged("StopALCommand");
            }
        }

        public ICommand OKCommand
        {
            get
            {
                if (okCommand == null)
                    okCommand = new DelegateCommand(Config.SaveConfig);
                return okCommand;
            }
        }

        public ICommand OpenAboutBoxCommand
        {
            get { return openAboutBoxCommand; }
            set
            {
                openAboutBoxCommand = value;
                NotifyPropertyChanged("OpenAboutBoxCommand");
            }
        }
        public ICommand OpenOptionsDialogCommand
        {
            get { return openOptionsDialogCommand; }
            set
            {
                openOptionsDialogCommand = value;
                NotifyPropertyChanged("OpenOptionsDialogCommand");
            }
        }
        public ICommand SerialPortTestConnectionCommand
        {
            set
            {
                if (SerialPortConfigViewModel != null)
                {
                    SerialPortConfigViewModel.TestConnectionCommand = value;
                }
            }
        }

        public ICommand HelpCommand
        {
            get
            {
                if (helpCommand == null)
                    helpCommand = new DelegateCommand(ShowHelp);
                return helpCommand;
            }
        }
        #endregion

        #endregion

        #region Constructor
        public APRSAddinConfigViewModel()
            : this(new APRSAddinConfig())
        {

        }

        public APRSAddinConfigViewModel(APRSAddinConfig config)
        {
            Config = config;
            InputConfigs = new List<string>();
            SerialPortConfigViewModel = new SerialPortConfigViewModel(config.serialPortModel);
            TcpConfigViewModel = new TCPConfigViewModel(config.tcpConfigModel);
            AGWPEConfigViewModel = new AGWPEConfigViewModel(config.agwpeConfigModel);
            if (configNameToType == null)
            {
                configNameToType = new Dictionary<string, APRSInputType>
                                       {
                                           {SerialPortConfigViewModel.TypeName, SerialPortConfigViewModel.ConfigType},
                                           {TcpConfigViewModel.TypeName, TcpConfigViewModel.ConfigType},
                                           {AGWPEConfigViewModel.TypeName, AGWPEConfigViewModel.ConfigType}
                                       };
#if DEBUG
                configNameToType.Add("TestConfig",APRSInputType.Loopback);
#endif

            }

            foreach (var kvpair in configNameToType)
            {
                InputConfigs.Add(kvpair.Key);
            }
            foreach (var kvpair in configNameToType)
            {
                if (kvpair.Value == config.InputType)
                {
                    selectedInput = kvpair.Key;
                    break;
                }
            }
        }

        #endregion

        private static void ShowHelp()
        {
            System.Diagnostics.Process.Start("mainHelp.html");
        }
    }
}