using System.IO.Ports;
using System.Windows.Input;
using APRS;
using APRSLiveConfiguration.Model;
using Depiction.API.MVVM;

namespace APRSLiveConfiguration.ViewModel
{

    public class SerialPortConfigViewModel : ViewModelBase 
    {
        private readonly SerialPortConfigModel model;

        public SerialPortConfigViewModel(IAPRSInputConfiguration model)
        {
            this.model = model as SerialPortConfigModel;
        }

        public static int[] AvailableBaudRates
        {
            get
            {
                return new[] { 1200, 2400, 4800, 9600, 19200, 38400, 115200 };
            }
        }


         //<ComboBox Margin="5" Grid.Row="2" SelectedValue ="{Binding Path=Parity}" ItemsSource="{Binding Path=AvailableParity}" Name="parityCombo" Grid.Column="1"  />
         //   <Label Margin="5" Grid.Row="2" Name="label8" HorizontalAlignment="Left" Width="86" Height="25.96" VerticalAlignment="Top">Parity</Label>


            
         //   <ComboBox Margin="5" Grid.Row="4" SelectedValue ="{Binding Path=Handshaking}" ItemsSource="{Binding Path=AvailableHandshakes}" Name="handshakingCombo" Grid.Column="1"  />
         //   <Label Margin="5" Grid.Row="4" Name="label7" HorizontalAlignment="Left" Width="86" Height="25.96" VerticalAlignment="Top">Flow control</Label>





        public static Parity[] AvailableParity
        {
            get { return new[] {Parity.Even, Parity.Odd, Parity.None}; }
        }

        public static FlowControlType[] AvailableFlowControl
        {
            get  {return new[] { FlowControlType.Software, FlowControlType.Hardware, FlowControlType.None }; }
        }

        public string[] AvailableSerialPorts
        {
            get { return SerialPort.GetPortNames(); }
        }

        public string SerialPortName { get { return model.PortName; } set { model.PortName = value; } }
        public int BaudRate { get { return model.Baud; } set { model.Baud = value;}}
        public Parity Parity { get { return model.Parity; }  set { model.Parity = value; } }
        public FlowControlType FlowControl { get { return HandshakeToFlowControl(model.Handshake); } set { model.Handshake = FlowControlTypeToHandshake(value); } }


        private static Handshake FlowControlTypeToHandshake(FlowControlType fc)
        {
            switch (fc)
            {
                case FlowControlType.Hardware:
                    return Handshake.RequestToSend;
                case FlowControlType.Software:
                    return Handshake.XOnXOff;
                default:
                    return Handshake.None;
            }
        }

        private static FlowControlType HandshakeToFlowControl(Handshake hs)
        {
            switch (hs)
            {
                case Handshake.RequestToSend:
                    return FlowControlType.Hardware;

                case Handshake.XOnXOff:
                    return FlowControlType.Software;
                    
                default:
                    return FlowControlType.None;
                    
            }
            
        }


        private ICommand testConnectionCommand;
        public ICommand TestConnectionCommand
        {
            get { return testConnectionCommand; }
            set 
            { 
                testConnectionCommand = value;
                NotifyPropertyChanged("TestConnectionCommand");
            }
        }

        public string TypeName
        {
            get { return model.TypeName; }
        }

        public APRSInputType ConfigType
        {
            get { return APRSInputType.Serial; }
        }

        public override string ToString()
        {
            return TypeName;
        }
    }
}