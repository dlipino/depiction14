﻿using APRS;
using APRSLiveConfiguration.Model;
using Depiction.API.MVVM;

namespace APRSLiveConfiguration.ViewModel
{
    public class AGWPEConfigViewModel: ViewModelBase 
    {
        private readonly AGWPEConfigModel model;

        public AGWPEConfigViewModel(IAPRSInputConfiguration model)
        {
            this.model = model as AGWPEConfigModel;
        }

        public int TCPPort { get { return model.TCPPort; } set { model.TCPPort = value; NotifyPropertyChanged("TCPPort"); } }
        public byte RadioPort { get { return model.RadioPort; } set { model.RadioPort = value; NotifyPropertyChanged("RadioPort"); } }
        public string Host { get { return model.Host; } set { model.Host = value; NotifyPropertyChanged("Host"); } }

        public string TypeName
        {
            get { return model.TypeName; }
        }

        public APRSInputType ConfigType
        {
            get { return APRSInputType.AGWPE; }
        }

        public override string ToString()
        {
            return TypeName;
        }
    }
}
