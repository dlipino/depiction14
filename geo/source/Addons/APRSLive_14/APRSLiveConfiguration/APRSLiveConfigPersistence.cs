using System;
using System.Configuration;
using System.IO;
using System.IO.Ports;

namespace APRSLiveConfiguration
{
    public class APRSLiveConfigPersistence: ConfigurationSection
    {
        
        static APRSLiveConfigPersistence()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;
        }


        public static Configuration GetConfig()
        {
            var p = new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            return ConfigurationManager.OpenExeConfiguration(p.LocalPath);
        }

        public static string GetConfigPath()
        {
            Uri p = new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
            return p.LocalPath + ".config";
        }

        public static APRSLiveConfigPersistence GetConfigSection()
        {
            
            var config = GetConfig();
            return GetConfigSection(config);
        }

        public static APRSLiveConfigPersistence GetConfigSection(Configuration config)
        {
            var sect = config.GetSection("AprsLiveConfig");
            return sect as APRSLiveConfigPersistence;
        }

        static System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            return System.Reflection.Assembly.GetExecutingAssembly();
        }

        [ConfigurationProperty("AgwpeTCPHost", DefaultValue = "localhost", IsRequired = false)]
        public string AgwpeTCPHost
        {

            get { return (string)this["AgwpeTCPHost"]; }
            set { this["AgwpeTCPHost"] = value; }
        }

        [ConfigurationProperty("AgwpeRadioPort", DefaultValue = (byte)0, IsRequired = false)]
        public byte AgwpeRadioPort
        {

            get { return (byte)this["AgwpeRadioPort"]; }
            set { this["AgwpeRadioPort"] = value; }
        }

        [ConfigurationProperty("AgwpeTCPPort", DefaultValue = 8000, IsRequired = false)]
        public int AgwpeTCPPort
        {
            get { return (int)this["AgwpeTCPPort"]; }
            set { this["AgwpeTCPPort"] = value; }
        }

        [ConfigurationProperty("UserHasSavedConfig", DefaultValue = false, IsRequired = false)]
        public bool UserHasSavedConfig
        {
            get { return (bool)this["UserHasSavedConfig"]; }
            set { this["UserHasSavedConfig"] = value; }
        }
        
        [ConfigurationProperty("TimeWindowMinutes", DefaultValue = "60", IsRequired = false)]
        public int TimeWindowMinutes
        {
            get { return (int) this["TimeWindowMinutes"] ; }
            set { this["TimeWindowMinutes"] = value; }
        }


        [ConfigurationProperty("SerialPort", DefaultValue = "", IsRequired = false)]
        public string SerialPort
        {
            get { return this["SerialPort"] as string; }
            set { this["SerialPort"] = value; }
        }

        [ConfigurationProperty("TCPPort", DefaultValue = 14579, IsRequired = false)]
        public int TCPPort
        {
            get { return (int) this["TCPPort"]; }
            set { this["TCPPort"] = value; }
        }

        [ConfigurationProperty("TCPHost", DefaultValue = "", IsRequired = false)]
        public string TCPHost
        {
            get { return this["TCPHost"] as string; }
            set { this["TCPHost"] = value; }
        }

        [ConfigurationProperty("TCPCustomFilter", DefaultValue = "", IsRequired = false)]
        public string TCPCustomFilter
        {
            get { return this["TCPCustomFilter"] as string; }
            set { this["TCPCustomFilter"] = value; }
        }

        [ConfigurationProperty("TCPUseCustomFilter", DefaultValue = false,  IsRequired = false)]
        public bool TCPUseCustomFilter
        {
            get { return (bool) this["TCPUseCustomFilter"]; }
            set { this["TCPUseCustomFilter"] = value; }
        }


        [ConfigurationProperty("TCPPassword", DefaultValue = "", IsRequired = false)]
        public string TCPPassword
        {
            get { return this["TCPPassword"] as string; }
            set { this["TCPPassword"] = value; }
        }

        [ConfigurationProperty("CallSign", DefaultValue = "XXXXXX", IsRequired = false)]
        public string CallSign
        {
            get { return this["CallSign"] as string; }
            set { this["CallSign"] = value; }
        }

        [ConfigurationProperty("InputType", DefaultValue = "TCP", IsRequired = false)]
        public string InputType
        {
            get { return this["InputType"] as string; }
            set { this["InputType"] = value; }
        }

        [ConfigurationProperty("BaudRate", DefaultValue = "9600", IsRequired = false)]
        public int BaudRate
        {
            get { return Convert.ToInt32(this["BaudRate"]); }
            set { this["BaudRate"] = value; }
        }

        [ConfigurationProperty("SerialParity", DefaultValue = Parity.None, IsRequired = false)]
        public Parity SerialParity
        {
            get { return (Parity) this["SerialParity"] ; }
            set { this["SerialParity"] = value; }
        }

        [ConfigurationProperty("SerialHandshake", DefaultValue = Handshake.None, IsRequired = false)]
        public Handshake SerialHandshake
        {
            get { return (Handshake)this["SerialHandshake"]; }
            set { this["SerialHandshake"] = value; }
        }

        [ConfigurationProperty("CropToRegion", DefaultValue = false, IsRequired = false)]
        public bool CropToRegion
        {
            get { return (bool) this["CropToRegion"]; }
            set { this["CropToRegion"] = value; }
        }
    }
}