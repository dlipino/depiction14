﻿using System.IO;
using System.Windows;
using APRSPacketIO;
using Depiction.API;

namespace APRSLiveConfiguration.View
{
    /// <summary>
    /// Interaction logic for APRSAddinConfigView.xaml
    /// </summary>
    public partial class APRSAddinConfigView
    {
        public APRSAddinConfigView()
        {
            InitializeComponent();
        }

        public string RunPath = string.Empty;

        private void btnOK_Click_1(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            if (window != null)
            {
                if (System.Windows.Interop.ComponentDispatcher.IsThreadModal)
                    window.DialogResult = true;
                else
                    window.Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            if (window != null)
            {
                if (System.Windows.Interop.ComponentDispatcher.IsThreadModal)
                    window.DialogResult = false;
                else
                    window.Close();
            }
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            var helpPath = Path.Combine(RunPath, "help\\mainHelp.html");
            try
            {
                System.Diagnostics.Process.Start(helpPath);
            }
            catch
            {
                // do nothing if no help exists.
            }
        }

        private void btnShowLog_Click(object sender, RoutedEventArgs e)
        {
            var logpath = Logger.FullLogName;
            if (!File.Exists(logpath))
            {
                var message = string.Format("APRSLive log, {0}, has not been created yet.", Path.GetFileName(logpath));
                DepictionAccess.NotificationService.SendNotificationDialog(this, message, "No Log File");
                return;
            }
            try
            {
                System.Diagnostics.Process.Start(logpath);
            }
            catch
            {
                // do nothing if no log exists.
            }
        }
    }
}