﻿
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.HelperObjects;
using Depiction.API.Service;

namespace APRSLiveConfiguration.View
{
    /// <summary>
    /// Interaction logic for AboutBox.xaml
    /// </summary>
    public partial class OptionsDialog
    {
        const string APRSLiveElementName = "OLWorks.APRSLiveStation";
        public OptionsDialog()
        {
            InitializeComponent();
        }

        private void hideHistoryBttn_Click(object sender, RoutedEventArgs e)
        {
            SetMovementHistoryVisibility(false);
        }

        private void showHistoryBttn_Click(object sender, RoutedEventArgs e)
        {
            SetMovementHistoryVisibility(true);
        }

        private void SetMovementHistoryVisibility(bool visible)
        {
            var currDepiction = DepictionAccess.CurrentDepiction;
            if (currDepiction == null) return;

            var elements =
                currDepiction.CompleteElementRepository.AllElements.Where(e => e.ElementType.Equals(APRSLiveElementName));

            foreach (var element in elements)
            {
                var showHistory = element.GetPropertyByInternalName("DrawZOIFromHistory");
                if (showHistory == null) continue;
                element.SetPropertyValue("DrawZOIFromHistory", visible);
            }

        }
    }
}