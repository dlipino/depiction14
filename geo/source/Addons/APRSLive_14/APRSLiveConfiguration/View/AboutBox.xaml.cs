﻿
namespace APRSLiveConfiguration.View
{
    /// <summary>
    /// Interaction logic for AboutBox.xaml
    /// </summary>
    public partial class AboutBox
    {
        public AboutBox()
        {
            InitializeComponent();
        }

        public string VersionString
        {
            set { VersionStr.Content = value; }
        }
    }
}