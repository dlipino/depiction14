using System;
using APRS;
using APRSLiveConfiguration.Properties;


namespace APRSLiveConfiguration.Model
{
    public class APRSAddinConfig
    {
        private string callSign = string.Empty;
        public event EventHandler ConfigChangesApplied;
        public SerialPortConfigModel serialPortModel = new SerialPortConfigModel();
        public TCPConfigModel tcpConfigModel = new TCPConfigModel();
        public AGWPEConfigModel agwpeConfigModel = new AGWPEConfigModel();
        public APRSInputType InputType = APRSInputType.TCP;

        #region Properties

        public int TimeWindowMinutes { get; set; }
        public bool UserHasSavedConfig { get; set; }
        public bool CropToRegion { get; set; }
        public bool UseAlternateParser { get; set; }
        public bool UseUTCTime { get; set; }

     
        public string CallSign { get { return callSign.ToUpperInvariant(); } set { callSign = value; } }

        #endregion
        #region constructor
        public APRSAddinConfig()
        {
            CallSign = String.Empty;
            LoadConfig();
        }
        #endregion

        private void LoadConfig()
        {
            serialPortModel.PortName = Settings.Default.SerialPort;
            serialPortModel.Baud = Settings.Default.BaudRate;
            serialPortModel.Handshake = Settings.Default.SerialHandshake;
            serialPortModel.Parity = Settings.Default.SerialParity;
            InputType = Settings.Default.InputType;
            tcpConfigModel.Host = Settings.Default.TCPHost;
            tcpConfigModel.Port = Settings.Default.TCPPort;
            tcpConfigModel.Password = Settings.Default.TCPPassword;
            tcpConfigModel.UseCustomFilter = Settings.Default.TCPUseCustomFilter;
            tcpConfigModel.CustomFilter = Settings.Default.TCPCustomFilter;
            agwpeConfigModel.Host = Settings.Default.AgwpeTCPHost;
            agwpeConfigModel.RadioPort = Settings.Default.AgwpeRadioPort;
            agwpeConfigModel.TCPPort = Settings.Default.AgwpeTCPPort;
            CallSign = Settings.Default.CallSign;
            CropToRegion = Settings.Default.CropToRegion;
            UseAlternateParser = Settings.Default.UseAlternateParser;
            TimeWindowMinutes = Settings.Default.TimeWindowMinutes;
            UserHasSavedConfig = Settings.Default.UserHasSavedConfig;
            UseUTCTime = Settings.Default.UseUTCTime;

        }
        public void SaveConfig()
        {
            var section = Settings.Default;
            section.SerialPort = serialPortModel.PortName;
            section.BaudRate = serialPortModel.Baud;
            section.SerialHandshake = serialPortModel.Handshake;
            section.SerialParity = serialPortModel.Parity;
            section.InputType = InputType;
            section.TCPHost = tcpConfigModel.Host;
            section.TCPPort = tcpConfigModel.Port;
            section.TCPPassword = tcpConfigModel.Password;
            section.CropToRegion = CropToRegion;

            section.TCPUseCustomFilter = tcpConfigModel.UseCustomFilter;
            section.TCPCustomFilter = tcpConfigModel.CustomFilter;

            section.CallSign = CallSign;
            section.UseAlternateParser = UseAlternateParser;
            section.UseUTCTime = UseUTCTime;
            section.TimeWindowMinutes = TimeWindowMinutes;

            section.AgwpeTCPHost = agwpeConfigModel.Host;
            section.AgwpeRadioPort = agwpeConfigModel.RadioPort;
            section.AgwpeTCPPort = agwpeConfigModel.TCPPort;
            section.UserHasSavedConfig = true;
            UserHasSavedConfig = true;
            section.Save();

            if (ConfigChangesApplied != null)
                ConfigChangesApplied(this, EventArgs.Empty);
        }
    }
}