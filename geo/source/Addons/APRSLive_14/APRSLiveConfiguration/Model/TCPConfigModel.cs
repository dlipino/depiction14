


using System;

namespace APRSLiveConfiguration.Model
{
    public class TCPConfigModel: IAPRSInputConfiguration
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Password { get; set; }
        public bool UseCustomFilter  { get; set; }
        public string CustomFilter { get; set; }
        public string TypeName
        {
            get { return "TCP Connection"; }
        }

    }
}