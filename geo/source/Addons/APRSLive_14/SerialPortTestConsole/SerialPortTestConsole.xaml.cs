﻿using System;
using System.IO.Ports;
using System.Windows;
using APRSPacketIO;
using Handshake=System.IO.Ports.Handshake;

namespace APRSLive.Core
{
    /// <summary>
    /// Interaction logic for SerialPortTestConsole.xaml
    /// </summary>
    public partial class SerialPortTestConsole
    {
        public SerialPortTestConsole()
        {
            InitializeComponent();
            comport.DataReceived += serialPort_DataReceived;
            // Set the port's settings
        }

        public SerialPortConfigModel Config { get; set;}
        private SerialPort comport = new SerialPort();
        
        private void receivedData(string data)
        {
            if (!CheckAccess())
            {
                Dispatcher.BeginInvoke(new Action<string>(receivedData), data);
                return;
            }
            consoleTextBox.AppendText(data);
        }

        void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            receivedData(comport.ReadExisting());
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            comport.BaudRate = Config.Baud;
            comport.DataBits = 8;
            comport.StopBits = StopBits.One;
            comport.Parity = Config.Parity;
            comport.PortName = Config.PortName;
            comport.Handshake = Config.Handshake;

            if (comport.Handshake != Handshake.None)
                comport.DtrEnable = true;
            SerialPortFixer.Execute(comport.PortName);

            // Open the port
            comport.Open();
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            comport.Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            comport.Write(txtSendData.Text + "\r");
            txtSendData.Clear();
        }
    }

    public struct SerialPortConfigModel
    {
        public string PortName;
        public int Baud;
        public Handshake Handshake;
        public Parity Parity;
        public SerialPortConfigModel(string portName, int baud, Handshake handshake, Parity parity )
        {
            PortName = portName;
            Baud = baud;
            Handshake = handshake;
            Parity = parity;

        }
    }
}