﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace APRSBase
{
    public enum APRSPacketType //fap_packet_type_t;
    {
        Undefined,
        Message
    }
    //Mainly used in test for now
    public class APRSPacketRaw
    {
        public string CallSign { get; private set; }
        public string Destination { get; private set; }
        public ReadOnlyCollection<string> Stations { get; private set; }
        public string RawMessage { get; private set; }
        
        public APRSPacketRaw(string callsign, string stationDest, string stationRoute, string ax25Data)
        {
            CallSign = callsign;
            Destination = stationDest;
            var list = new List<string>();
            list.Add(stationDest);
            list.AddRange(stationRoute.Split(','));
            Stations = new ReadOnlyCollection<string>(list);
            RawMessage = ax25Data;
        }

    }
    public class APRSPacketBase
    {
        /// AX.25-level source callsign.
        private string srcCallSignSSID = string.Empty;
        private string srcCallSignNoSSID = string.Empty;

        public string SrcCallSignWithSSID
        {
            get { return srcCallSignSSID; }
            set
            {
                srcCallSignSSID = value;
                //Kind of assumes there is no problem with the id before hand
                var groups = Regex.Match(srcCallSignSSID, @"^([A-Z0-9]{1,6})(-\d{1,2}|)$",RegexOptions.IgnoreCase).Groups;
                if(groups.Count>1)
                {
                    srcCallSignNoSSID = groups[1].Value;
                }
            }
        }

        public string SrcCallSignNoSSID
        {
            get { return srcCallSignNoSSID; }
        }

        public APRSPacketType packetType = APRSPacketType.Undefined;
        public DateTime sentTime = DateTime.MinValue;
        public DateTime recievedTime = DateTime.MinValue;
        //Sender/reciever info
        /// AX.25-level destination callsign.
        public string dstCallSign = string.Empty;


        public string destination = string.Empty;
        public string message = string.Empty;
        public string messageId = string.Empty;

        //Location info
        public double latitude = double.NaN;
        public double longitude = double.NaN;

        public double altitude; /// Altitude in meters. don't know how it
        public double course; /// Course in degrees, zero is unknown and 360 is north.
        public double speed; /// Land speed in km/h.

        // Symbol table designator. 0x00 = undef.
        public char symbol_table;
        //        _SymbolTableMap['/'] = SymbolTable.Primary;
        //            _SymbolTableMap['\\'] = SymbolTable.Secondary;

        // Slot of symbol table 0x00 = undef.
        public char symbol_code;

        //Weather info
        public APRSWeatherInfo weatherInfo = null;
    }
}