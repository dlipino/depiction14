﻿namespace APRSBase
{
    public class APRSWeatherInfo
    {
        // Wind gust in mph.
        public double windGust = double.NaN;
        //	 Wind direction in degrees.
        public double windDirection = double.NaN;
        //	 Wind speed in mph.
        public double windSpeed = double.NaN;
        //	 Temperature in degrees F.
        public double temperature = double.NaN;
        //	 Indoor temperature in degrees F.
        public double indoorTemperature = double.NaN;
        //	 Rain from last 1 hour, in inches.
        public double rainLastHour = double.NaN;
        //	 Rain from last day, in inches.
        public double rainLast24Hours = double.NaN;
        //	 Rain since midnight, in inches.
        public double rainSinceMidnight = double.NaN;
        //	 Relative humidity percentage.
        public double humidity = double.NaN;
        //	 Relative inside humidity percentage.
        public double insideHumidity = double.NaN;
        //	 Air pressure in millibars.
        public double pressure = double.NaN;
        //	 Luminosity in watts per square meter.
        public double luminosity = double.NaN;
        //	 Show depth increasement from last day, in inches.
        public double snowLast24Hours = double.NaN;
        //	 Software type indicator.
        public string SoftwareIndicator = null;
    }
}