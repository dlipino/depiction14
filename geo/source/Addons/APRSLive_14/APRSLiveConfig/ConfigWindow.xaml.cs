﻿using System;
using System.IO;
using System.Windows;
using APRSLiveConfiguration.Model;
using APRSLiveConfiguration.View;
using APRSLiveConfiguration.ViewModel;
using Microsoft.Win32;
using MvvmFoundation.Wpf;
using SerialPortConfigModel=APRSLive.Core.SerialPortConfigModel;

namespace APRSLiveConfig
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        private string configPath;
        private string configFileName;
        private APRSAddinConfig config;
        

        public ConfigWindow()
        {
            InitializeComponent();
        }

        private string depictionInstallDir;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = ConfigView.DataContext as APRSAddinConfigViewModel;
            if (viewModel == null) return;
            config = viewModel.Config;
            viewModel.Config.ConfigChangesApplied += Config_ConfigChangesApplied;
            viewModel.SerialPortTestConnectionCommand = new RelayCommand(OpenSerialPortTestConsole);
            viewModel.OpenAboutBoxCommand = new RelayCommand(OpenAboutBox);
            
            configPath = viewModel.Config.ConfigPath;
            configFileName = Path.GetFileName(configPath);
            depictionInstallDir = (string)Registry.GetValue(@"HKEY_LOCAL_MACHINE\Software\Depiction Inc\Depiction", "Location", "");
#if DEBUG
            var depictionProductName = "Depiction_Debug";
#else 
            var depictionProductName = "Depiction";
#endif

            ConfigView.RunPath = Path.Combine(
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), depictionProductName), 
                    @"AddinPipeline\Addins\APRSLive");
        }

        private static void OpenAboutBox()
        {
            var window = new AboutBoxWindow();
            var view = new AboutBox { VersionString = APRSLive.Core.APRSLiveVersion.GetVersionStr() };
            window.MGrid.Children.Add(view);
            window.ShowDialog();
        }

        private static void OpenSerialPortConsoleForConfig(APRSAddinConfig cfg)
        {
            try
            {
                var window = new TestConsoleWindow();
                var view = new APRSLive.Core.SerialPortTestConsole
                {
                    Config =
                        new SerialPortConfigModel(cfg.serialPortModel.PortName,
                                                                 cfg.serialPortModel.Baud,
                                                                 cfg.serialPortModel.Handshake,
                                                                 cfg.serialPortModel.Parity)
                };
                window.MGrid.Children.Add(view);
                window.ShowDialog();
            }
            catch (Exception e)
            {

                MessageBox.Show(String.Format("Cannot load Serial Test Console. Error: {0}", e.Message));
            }
            
        }




        private void OpenSerialPortTestConsole()
        {
            OpenSerialPortConsoleForConfig(config);
        }
        void Config_ConfigChangesApplied(object sender, EventArgs e)
        {
            //CopyConfigFileToProductAddinPipeline("Depiction_Debug");
            CopyConfigFileToProductAddinPipeline();
        }

        private void CopyConfigFileToProductAddinPipeline()
        {
            var targetConfigFilePath =
                Path.Combine(depictionInstallDir, @"Add-ons\APRSLive\");

            if (!Directory.Exists(targetConfigFilePath))
                Directory.CreateDirectory(targetConfigFilePath);
            File.Copy(configPath, Path.Combine(targetConfigFilePath, configFileName) , true);
        }
    }
}