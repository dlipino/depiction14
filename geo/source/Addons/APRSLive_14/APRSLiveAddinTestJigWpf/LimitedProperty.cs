using System;
using Depiction.APINew.InteractionEngine;
using Depiction.APINew.Interfaces.ElementInterfaces;
using Depiction.Serialization;


namespace APRSLiveAddinTestJigWpf
{
    internal class LimitedProperty: IElementProperty
    {
        public object Value { get; set;}
        

        public object DefaultValue
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool Editable { get; set;}
        

        public bool Visible
        {
            get { return true;; }
        }

        public bool DeleteIfEditZoi
        {
            get { return false;; }
        }

        public bool Deletable
        {
            get { return true; }
        }

        //public SetWeight SetWeight
        //{
        //    get { return SetWeight.User; }
        //}

        public Type PropertyType
        {
            get { return Value.GetType(); }
        }

        public string Name { get; set;}
        
        public string DisplayName 
        {
            get { return Name; }
        }

        public int PropertyIndex { get; set;}
        

        //public ElementPropertySource PropertySource { get; set;}

        //public ValidationResult Set(object newValue, SetWeight newSetWeight)
        //{
        //    Value = newValue;
        //    return new ValidationResult();
        //}

        //public ValidationResult Set(object newValue, bool runPostEdit, SetWeight newSetWeight)
        //{
        //    throw new System.NotImplementedException();
        //}

        public IDepictionElement Element { get; set;}

        public void RestoreDefaultValue()
        {
            throw new System.NotImplementedException();
        }

        public bool CanSerializeToXml
        {
            get { return false;; }
        }

        public bool IsHoverText { get; set;}

        public IElementProperty Clone()
        {
            throw new System.NotImplementedException();
        }

        #region IElementProperty Members

        public IElementPropertyData PropertData
        {
            get { throw new NotImplementedException(); }
        }

        public Depiction.APINew.HelperObjects.DepictionValidationResult SetPropertyValue(object newValue, IDepictionElement elementForPostSetActions)
        {
            throw new NotImplementedException();
        }

        public Depiction.APINew.HelperObjects.DepictionValidationResult SetPropertyValue(object newValue)
        {
            throw new NotImplementedException();
        }

        public Depiction.APINew.HelperObjects.DepictionValidationResult SetValueAndUpdateType(object newValue, IDepictionElement elementForPostSetActions)
        {
            throw new NotImplementedException();
        }

        public event Action<object> ValueChanged;

        #endregion

        #region IElementPropertyBase Members


        bool IElementPropertyBase.Deletable
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public object InitialValue
        {
            get { throw new NotImplementedException(); }
        }

        public string InternalName
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsInUse
        {
            get { throw new NotImplementedException(); }
        }

        public DateTime LastModified
        {
            get { throw new NotImplementedException(); }
        }

        public Depiction.APINew.CoreEnumAndStructs.MeasurementScale MeasurementScale
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

       
        Depiction.APINew.CoreEnumAndStructs.PropertySource IElementPropertyBase.PropertySource
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int Rank
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void RefreshValue()
        {
            throw new NotImplementedException();
        }

        public Depiction.APINew.OldValidationRules.IValidationRule[] ValidationRules
        {
            get { throw new NotImplementedException(); }
        }

        public SerializableDictionary<string, string[]> PostSetActions
        {
            get { throw new System.NotImplementedException(); }
        }

        public bool VisibleToUser
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region IDeepCloneable<IElementProperty> Members

        public IElementProperty DeepClone()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDeepCloneable Members

        object Depiction.APINew.Interfaces.IDeepCloneable.DeepClone()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}