using System;
using System.Drawing;
using System.IO;
using System.Windows.Threading;
using Depiction.API.Interfaces.MessagingInterface;

namespace APRSLiveAddinTestJigWpf
{
    public class LimitedDepictionAccess: IDepictionAccess
    {

        public LimitedDepictionAccess()
        {
            Librarian = new LimitedLibrarian();
            PropertyFactory = new LimitedPropertyFactory();
            NotificationService = new LimitedNotificationService();
        }
        public UserConfigurableProperties UserConfigurableProperties
        {
            get { return null; }
        }

        public IDepictionNotificationService NotificationService { get; set; }
        
        public Dispatcher ForegroundThreadDispatcher
        {
            get { return null; }
        }

        public ISpatialOps SpatialOps
        {
            get { return null; }
        }

        public IDepiction Depiction
        {
            get { return null; }
        }

        public ILibrarian Librarian { get; set;}
        

        public LatitudeLongitude LastMouseUpLatLon
        {
            get { return null; }
        }

        public IZoneOfInfluenceFactory ZoneOfInfluenceFactory
        {
            get { return null; }
        }

        public IGeoConverter PixelConverter
        {
            get { return null; }
        }

        public IWfsElementFactory WfsElementFactory
        {
            get { return null; }
        }

        public IPortalElementRetriever PortalElementRetriever
        {
            get { return null; }
        }

        public IImageWarper ImageWarper
        {
            get { return null; }
        }

        public IPropertyFactory PropertyFactory { get; set;}
        

        public IFileBrowser FileBrowser
        {
            get { return null; }
        }

        public IColorMapFactory ColorMapFactory
        {
            get { return null; }
        }

        public IApplicationPathService ApplicationPathService
        {
            get { return null; }
            set { }
        }

        public string CacheFile(byte[] buffer, string fileName)
        {
            return null;
        }

        public string RetrieveCachedFilePath(string fileName)
        {
            return null;
        }

        public string CacheTile(Bitmap tileToCache, string tileKey)
        {
            return null;
        }

        public string RetrieveCachedTile(string tileKey)
        {
            return null;
        }

        public void CacheTerrainFile(string filename, string tileKey)
        {
            
        }

        public void CacheWarp(string tileKey, LatitudeLongitude topLeft, LatitudeLongitude bottomRight)
        {
            
        }

        public BoundingBox RetrieveCachedWarp(string tileKey)
        {
            return null;
        }

        public void DeleteCachedFile(string fileName)
        {
            
        }

        public IArcImsImageRetriever ArcImsImageRetriever
        {
            get { return null; }
        }

        public string CacheFile(Stream buffer, string fileName)
        {
            throw new NotImplementedException();
        }
    }
}