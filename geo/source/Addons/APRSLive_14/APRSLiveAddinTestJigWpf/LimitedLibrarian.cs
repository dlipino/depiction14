﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using APRSLiveAddinTestJig;
using Depiction.API.Interface;
using Depiction.API.ValueObject;
using Depiction.APINew.ValueTypes;


namespace APRSLiveAddinTestJigWpf
{
    public class LimitedLibrarian: ILibrarian
    {
        public IElement CreateElement(string typeName)
        {
            var elem = new LElement();
            elem.ElementType = typeName;
            return elem;
        }

        public IElement CreateImageElement(ImageElementInfo imageElementInfo)
        {
            throw new System.NotImplementedException();
        }

        public IElement GenerateRoadNetworkElement(LatitudeLongitude[][] roadSegments, string displayName)
        {
            throw new System.NotImplementedException();
        }

        public IElement GenerateRoadNetworkElement(RoadSegment[] roadSegments, string displayName)
        {
            throw new NotImplementedException();
        }

        public void CreateStartAndEndWaypointsForNewRoute(IElement newRoute)
        {
            throw new NotImplementedException();
        }

        public void ModifyRouteBasedOnElementZOI(IElement oldRoute, bool isFreeForm)
        {
            throw new NotImplementedException();
        }

        public bool AddBitmapResourceToStory(Uri iconPath)
        {
            throw new System.NotImplementedException();
        }

        public bool AddBitmapToElement(IElement element, BitmapSource bitmap, string imageKey)
        {
            if (!Application.Current.CheckAccess())
            {
                var func = new Func<IElement, BitmapSource, string, bool>(AddBitmapToElement);
                    
                return (bool) Application.Current.Dispatcher.Invoke(DispatcherPriority.Normal,
                                                                    func, element, bitmap, imageKey);
            }
            var elem = element as LElement;
            elem.GroupIconSource = bitmap;
            
            return true;
        }
    }
}