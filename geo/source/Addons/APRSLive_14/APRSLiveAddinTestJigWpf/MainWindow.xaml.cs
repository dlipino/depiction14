﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using APRSLiveAddinTestJig;
using APRSLiveConfiguration.Model;
using APRSPacketIO;
using Depiction.API;
using Depiction.API.Interface;
using Depiction.API.ValueObject;
using Depiction.APINew;
using Depiction.APINew.Interfaces.ElementInterfaces;

namespace APRSLiveAddinTestJigWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {

        private readonly APRSLiveFeedGatherer.APRSLiveFeedGatherer aprsElementSource;
        private APRSAddinConfig config;
        private readonly ObservableCollection<LElement> elements = new ObservableCollection<LElement>();
        private readonly Dictionary<string, IDepictionElement> elementDictionary = new Dictionary<string, IDepictionElement>();
        private bool cancelled;
        private bool paused;
        private readonly BackgroundWorker worker;


        public MainWindow()
        {
            InitializeComponent();
            
            DepictionAccess.Initialize(new LimitedDepictionAccess());
            listBox1.ItemsSource = elements;
            config = new APRSAddinConfig();
            aprsElementSource = new APRSLiveFeedGatherer.APRSLiveFeedGatherer(config);
            //aprsElementSource.ConfigChanged += config_ConfigChangesApplied;
            worker = new BackgroundWorker();
            worker.DoWork += bgw_DoWork;
            ((LimitedNotificationService)DepictionAccess.NotificationService).NotificationReceived += MainWindow_NotificationReceived;
            Logger.OnWriteLog += Logger_OnWriteLog;
            
            

        }

        private void Logger_OnWriteLog(string message)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(new Action<string>(Logger_OnWriteLog),  message );
                return;
            }
            txtLog.AppendText(message + "\n");
        }

        public void MainWindow_NotificationReceived(string message)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(new Action<string>(MainWindow_NotificationReceived), new[] { message });
                return;
            }
            receivedMessages.AppendText(message + "\n");
        }

        

        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!cancelled)
            {
                if (!paused)
                    CheckElements();
                Thread.Sleep(2000);
               
            }
            Console.WriteLine("Shutting down Main app background thread");
        }
        
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (!worker.IsBusy)
                worker.RunWorkerAsync();
        }

        private void CheckElements()
        {
            var gatheredElements = aprsElementSource.GatherElements(new BoundingBox(new LatitudeLongitude(47.65, -122.35), new LatitudeLongitude(47.55, -122.25)), null);

            AddElements(gatheredElements);
        }

        private void AddElements(IEnumerable<IElement> gatheredElements)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(new Action<IEnumerable<IElement>>(AddElements), new [] {gatheredElements});
                return;
            }
            foreach (LElement element in gatheredElements)
            {
                if (!elementDictionary.ContainsKey(element.ElementKey))
                {
                    elementDictionary.Add(element.ElementKey, element);
                    elements.Add(element);
                }
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            aprsElementSource.ConfigDialogShowModal();
            //var configWin = new ConfigWindow();
            //var newConfig = new APRSAddinConfig();
            //newConfig.ConfigChangesApplied += config_ConfigChangesApplied;
            //configWin.ConfigView.DataContext = new APRSAddinConfigViewModel(newConfig);
            //configWin.ShowDialog();


        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            aprsElementSource.SendMessageDialogShowModal(config.CallSign);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            cancelled = true;
            aprsElementSource.Cancel();
        }
    }
}
