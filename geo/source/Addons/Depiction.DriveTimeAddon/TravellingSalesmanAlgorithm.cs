﻿using System;
using System.Collections.Generic;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Algorithms.ShortestPath;

namespace Depiction.DriveTimeAddon
{
    
      //This encapsulates reusable functionality for resolving TSP problems on
      //Google Maps.
      //The authors of this code are James Tolley <info [at] gmaptools.com>
      //and Geir K. Engdahl <geir.engdahl (at) gmail.com>

      //For the most up-to-date version of this file, see
      //http://code.google.com/p/google-maps-tsp-solver/

      //To receive updates, subscribe to google-maps-tsp-solver@googlegroups.com

      //version 1.0; 05/07/10

      //Usage:
      //See http://code.google.com/p/google-maps-tsp-solver/

    public class TravellingSalesmanAlgorithm
    {
        private readonly bool roundTrip;

        const int maxTspDynamic = 15;     // Max size for brute force, may seem conservative, but many browsers have limitations on run-time.
        const int maxTripSentry = 2000000000; // Approx. 63 years., this long a route should not be reached...

        private double[,] dur; //edge weights
        private IList<RoadSegment>[,] paths;
        private int numActive;
        private int[] currPath;
        private int[] bestPath;
        private double bestTrip;
        private bool[] visited;

        private bool[] nextSet;
        private readonly Random random = new Random();

        public TravellingSalesmanAlgorithm(bool roundTrip)
        {
            this.roundTrip = roundTrip;
        }

        public void Calculate(IDepictionElement route, IRoadGraph graph, IList<RoadNode> roadNodes, out IList<IList<RoadSegment>> coordinates, out IList<double> driveTimes)
        {
            numActive = roadNodes.Count;
            dur = new double[numActive,numActive];
            paths = new IList<RoadSegment>[numActive, numActive];
            for (int i = 0; i < numActive; i++)
            {
                var dijkstra = new DijkstraShortestPathAlgorithm<RoadNode, RoadSegment>(graph.Graph, graph.GetDriveTimeFunc(route));

                //Attach a Vertex Predecessor Recorder Observer to give us the paths
                var predecessors = new VertexPredecessorRecorderObserver<RoadNode, RoadSegment>();
                using (predecessors.Attach(dijkstra))
                {
                    dijkstra.Compute(roadNodes[i]);
                    for (int j = 0; j < roadNodes.Count; j++)
                    {
                        if (i == j) continue;
                        IEnumerable<RoadSegment> path;
                        predecessors.TryGetPath(roadNodes[j], out path);
                        if (path == null)
                        {
                            coordinates = null;
                            driveTimes = null;
                            return;
                        }
                        paths[i, j] = path as IList<RoadSegment>;

                        double distance;
                        dijkstra.TryGetDistance(roadNodes[j], out distance);
                        dur[i, j] = distance;
                    }
                }

                //dijkstra.Compute(roadNodes[i]);
                
                //for (int j = 0; j < numActive; j++)
                //{
                //    double distance;
                //    dijkstra.TryGetDistance(roadNodes[j], out distance);
                //    dur[i, j] = distance;
                //}
            }
            var tspPath = doTsp(1);

            //Convert tspPath into something Depiction can use
            //var result = new RoadNode[numActive];
            //for (int i = 0; i < numActive; i++)
            //{
            //    result[i] = roadNodes[tspPath[i]];
            //}

            var totalCoords = new List<IList<RoadSegment>>();
            driveTimes = new List<double>();
            for (int i = 1; i < numActive; i++ )
            {
                {
                    var coords = paths[tspPath[i - 1], tspPath[i]];
                    totalCoords.Add(coords);
                    driveTimes.Add(dur[i - 1, i]);
                    //for (int j = 0; j < coords.Length; j++)
                    //{
                    //    if (i > 1 && j == 0) continue;
                    //    totalCoords.Add(coords[j]);
                    //}
                }
            }
            coordinates = totalCoords;
            //return result;
        }



        private int[] doTsp(int mode)
        {
            // Calculate shortest roundtrip:
            visited = new bool[numActive];
            currPath = new int[numActive];
            bestPath = new int[numActive];
            nextSet = new bool[numActive];
            bestTrip = maxTripSentry;
            visited[0] = true;
            currPath[0] = 0;
            //cachedDirections = true;
            int[] resultingPath;
            if (numActive <= maxTspDynamic + mode)
            {
                resultingPath = tspDynamic(mode);
            }
            else
            {
                resultingPath = tspAntColonyK2(mode);
            }

            return resultingPath;
        }




        /* Computes a near-optimal solution to the TSP problem, 
         * using Ant Colony Optimization and local optimization
         * in the form of k2-opting each candidate route.
         * Run time is O(numWaves * numAnts * numActive ^ 2) for ACO
         * and O(numWaves * numAnts * numActive ^ 3) for rewiring?
         * 
         * if mode is 1, we start at node 0 and end at node numActive-1.
         */
        private int[] tspAntColonyK2(int mode)
        {
            const double alfa = 1.0; // The importance of the previous trails
            const double beta = 1.0; // The importance of the durations
            const double rho = 0.1;  // The decay rate of the pheromone trails
            const double asymptoteFactor = 0.9; // The sharpness of the reward as the solutions approach the best solution
            var pher = new double[numActive, numActive];
            var nextPher = new double[numActive, numActive];
            var prob = new double[numActive];
            const int numAnts = 10;
            const int numWaves = 10;

            var lastNode = 0;
            const int startNode = 0;
            var numSteps = numActive - 1;
            var numValidDests = numActive;
            if (mode == 1)
            {
                lastNode = numActive - 1;
                numSteps = numActive - 2;
                numValidDests = numActive - 1;
            }
            for (var wave = 0; wave < numWaves; ++wave)
            {
                for (var ant = 0; ant < numAnts; ++ant)
                {
                    var curr = startNode;
                    var currDist = 0.0;
                    for (var i = 0; i < numActive; ++i)
                    {
                        visited[i] = false;
                    }

                    currPath[0] = curr;
                    for (var step = 0; step < numSteps; ++step)
                    {
                        visited[curr] = true;
                        var cumProb = 0.0;
                        for (var next = 1; next < numValidDests; ++next)
                        {
                            if (!visited[next])
                            {
                                prob[next] = Math.Pow(pher[curr, next], alfa) *
                                Math.Pow(dur[curr, next], 0.0 - beta);
                                cumProb += prob[next];
                            }
                        }
                        var guess = random.NextDouble() * cumProb;
                        var nextI = -1;
                        for (var next = 1; next < numValidDests; ++next)
                        {
                            if (!visited[next])
                            {
                                nextI = next;
                                guess -= prob[next];
                                if (guess < 0)
                                {
                                    nextI = next;
                                    break;
                                }
                            }
                        }
                        currDist += dur[curr, nextI];
                        currPath[step + 1] = nextI;
                        curr = nextI;
                    }
                    currPath[numSteps + 1] = lastNode;
                    currDist += dur[curr, lastNode];

                    // k2-rewire:
                    var lastStep = numActive;
                    if (mode == 1)
                    {
                        lastStep = numActive - 1;
                    }
                    var changed = true;

                    while (changed)
                    {
                        var i = 0;
                        changed = false;
                        for (; i < lastStep - 2 && !changed; ++i)
                        {
                            var cost = dur[currPath[i + 1], currPath[i + 2]];
                            var revCost = dur[currPath[i + 2], currPath[i + 1]];
                            var iCost = dur[currPath[i], currPath[i + 1]];
                            for (var j = i + 2; j < lastStep && !changed; ++j)
                            {
                                double nowCost = cost + iCost + dur[currPath[j], currPath[j + 1]];
                                double newCost = revCost + dur[currPath[i], currPath[j]]
                                                 + dur[currPath[i + 1], currPath[j + 1]];
                                if (nowCost > newCost)
                                {
                                    currDist += newCost - nowCost;
                                    // Reverse the detached road segment.
                                    for (var k = 0; k < Math.Floor(((double)j - i) / 2); ++k)
                                    {
                                        int tmp = currPath[i + 1 + k];
                                        currPath[i + 1 + k] = currPath[j - k];
                                        currPath[j - k] = tmp;
                                    }
                                    changed = true;
                                    --i;
                                }
                                cost += dur[currPath[j], currPath[j + 1]];
                                revCost += dur[currPath[j + 1], currPath[j]];
                            }
                        }
                    }

                    if (currDist < bestTrip)
                    {
                        bestPath = currPath;
                        bestTrip = currDist;
                    }
                    for (var i = 0; i <= numSteps; ++i)
                    {
                        nextPher[currPath[i], currPath[i + 1]] += (bestTrip - asymptoteFactor * bestTrip) / (numAnts * (currDist - asymptoteFactor * bestTrip));
                    }
                }
                for (var i = 0; i < numActive; ++i)
                {
                    for (var j = 0; j < numActive; ++j)
                    {
                        pher[i, j] = pher[i, j] * (1.0 - rho) + rho * nextPher[i, j];
                        nextPher[i, j] = 0.0;
                    }
                }
            }
            return bestPath;
        }

        /* Solves the TSP problem to optimality. Memory requirement is
         * O(numActive * 2^numActive)
         */
        private int[] tspDynamic(int mode)
        {
            var numCombos = 1 << numActive;
            var C = new double[numCombos, numActive];
            var parent = new int[numCombos, numActive];
            for (var k = 1; k < numActive; ++k)
            {
                var kIndex = 1 + (1 << k);
                C[kIndex, k] = dur[0, k];
            }
            for (var s = 3; s <= numActive; ++s)
            {
                for (var i = 0; i < numActive; ++i)
                {
                    nextSet[i] = false;
                }
                var sIndex = nextSetOf(s);
                while (sIndex >= 0)
                {
                    for (var k = 1; k < numActive; ++k)
                    {
                        if (nextSet[k])
                        {
                            var prevIndex = sIndex - (1 << k);
                            C[sIndex, k] = maxTripSentry;
                            for (var m = 1; m < numActive; ++m)
                            {
                                if (nextSet[m] && m != k)
                                {
                                    if (C[prevIndex, m] + dur[m, k] < C[sIndex, k])
                                    {
                                        C[sIndex, k] = C[prevIndex, m] + dur[m, k];
                                        parent[sIndex, k] = m;
                                    }
                                }
                            }
                        }
                    }
                    sIndex = nextSetOf(s);
                }
            }
            for (var i = 0; i < numActive; ++i)
            {
                bestPath[i] = 0;
            }
            int index = (1 << numActive) - 1;
            int currNode;
            if (mode == 0)
            {
                currNode = -1;
                bestPath[numActive] = 0;
                for (var i = 1; i < numActive; ++i)
                {
                    if (C[index, i] + dur[i, 0] < bestTrip)
                    {
                        bestTrip = C[index, i] + dur[i, 0];
                        currNode = i;
                    }
                }
                bestPath[numActive - 1] = currNode;
            }
            else
            {
                currNode = numActive - 1;
                bestPath[numActive - 1] = numActive - 1;
                bestTrip = C[index, numActive - 1];
            }
            for (var i = numActive - 1; i > 0; --i)
            {
                currNode = parent[index, currNode];
                index -= (1 << bestPath[i]);
                bestPath[i - 1] = currNode;
            }
            return bestPath;
        }

        //Finds the next integer that has num bits set to 1.
        private int nextSetOf(int num)
        {
            var count = 0;
            var ret = 0;
            for (var i = 0; i < numActive; ++i)
            {
                count += Convert.ToInt32(nextSet[i]);
            }
            if (count < num)
            {
                for (var i = 0; i < num; ++i)
                {
                    nextSet[i] = true;
                }
                for (var i = num; i < numActive; ++i)
                {
                    nextSet[i] = false;
                }
            }
            else
            {
                // Find first 1
                var firstOne = -1;
                for (var i = 0; i < numActive; ++i)
                {
                    if (nextSet[i])
                    {
                        firstOne = i;
                        break;
                    }
                }
                // Find first 0 greater than firstOne
                var firstZero = -1;
                for (var i = firstOne + 1; i < numActive; ++i)
                {
                    if (!nextSet[i])
                    {
                        firstZero = i;
                        break;
                    }
                }
                if (firstZero < 0)
                {
                    return -1;
                }
                // Increment the first zero with ones behind it
                nextSet[firstZero] = true;
                // Set the part behind that one to its lowest possible value
                for (var i = 0; i < firstZero - firstOne - 1; ++i)
                {
                    nextSet[i] = true;
                }
                for (var i = firstZero - firstOne - 1; i < firstZero; ++i)
                {
                    nextSet[i] = false;
                }
            }
            // Return the index for this set
            for (var i = 0; i < numActive; ++i)
            {
                ret += (Convert.ToInt32(nextSet[i]) << i);
            }
            return ret;
        }

    }
}