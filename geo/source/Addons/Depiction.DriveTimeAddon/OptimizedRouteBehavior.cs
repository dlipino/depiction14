﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.ValueTypes;
using GisSharpBlog.NetTopologySuite.Geometries;
//#if(DEBUG)
//#elif(OSINT)
//#else
////using Depiction.LicenseManager.Model;//Resharper lies, this is needed for non-debug mode. not so sure anymore
//#endif

namespace Depiction.DriveTimeAddon
{
    [Export(typeof(BaseBehavior))]
    [Behavior("OptimizedRoute", "Find best route", "Find best route")]
    public class OptimizedRouteBehavior : BaseBehavior
    {
        const string directionPropertyName = "Directions";
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("RoadNetwork", typeof (IDepictionElementBase))
                        {
                            ParameterName = "Road Network", 
                            ParameterDescription = "The road network to use when calculating the route"
                        },
                    //new ParameterInfo("RoundTrip", typeof (bool))
                    //    {
                    //        ParameterName = "Round trip", 
                    //        ParameterDescription = "Property determining whether to perform a Roundtrip or an A-Z Trip."
                    //    }
                };
        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement route, Dictionary<string, object> parameterBag)
        {
//#if(DEBUG)
//#elif(OSINT)
//#else
//            if (!LogisticsLicenseService.LicenseService().LicenseState.Equals(DepictionLicenseState.ActivatedNoExpiration))
//            {
//                DepictionAccess.NotificationService.DisplayMessageString(
//                    "Logistics Add On must be activated from 'Tools -> Add ons' before using its elements.");
//                return new BehaviorResult();
//            }
//#endif
            var roadNetwork = (IDepictionElement)parameterBag["RoadNetwork"];

            if (roadNetwork == null) return new BehaviorResult();
            IRoadGraph roadGraph = roadNetwork.GetPropertyByInternalName("RoadGraph").Value as IRoadGraph;
            if (roadGraph == null) return new BehaviorResult();
            var roadNodes = new List<RoadNode>();
            var elementWaypoints = route.Waypoints;
            if (elementWaypoints.Length <= 1) return new BehaviorResult();
            //bool attachToRoadNetworkNodes;

            for (int i = 0; i < elementWaypoints.Length; i++)
            {
                var nearestNode = roadGraph.FindNearestNode(elementWaypoints[i].Location);
                //if (attachToRoadNetworkNodes)
                {
                    elementWaypoints[i].UpdateLocationWithoutChangeNotification(nearestNode.Vertex);
                }
                roadNodes.Add(nearestNode);
            }

            TravellingSalesmanAlgorithm algorithm = new TravellingSalesmanAlgorithm(true);

            IList<IList<RoadSegment>> completePath;

            IList<double> driveTimes;
            algorithm.Calculate(route, roadGraph, roadNodes, out completePath, out driveTimes);
            if (completePath == null || driveTimes == null)
            {
                DepictionAccess.NotificationService.DisplayMessageString(
                    "Could not find optimized route because a path could not be found between waypoints.");
                return new BehaviorResult();
            }
            SetZOIAndWaypoints(route, completePath);
            SetDirections(route, completePath, driveTimes);
            return new BehaviorResult();
        }

        private static void SetDirections(IDepictionElement route, IList<IList<RoadSegment>> completePath, IList<double> completeRouteTimes)
        {
            var service = new RouteDirectionsService();
            string directions = service.GetRouteDirections(completePath, completeRouteTimes);
            var directionsProperty = new DepictionElementProperty(directionPropertyName, directions);
            directionsProperty.IsHoverText = true;
            route.AddPropertyOrReplaceValueAndAttributes(directionsProperty);
            route.UseEnhancedPermaText = true;

        }

        private static void SetZOIAndWaypoints(IDepictionElement route, IList<IList<RoadSegment>> completePath)
        {
            var geomFact = new GeometryFactory();
            var coordinates = new List<Coordinate>();
            for (int i = 0; i < completePath.Count; i++)
            {
                var roadSegments = completePath[i];
                
                for (int j = 0; j < roadSegments.Count; j++)
                {
                    if (j == 0 && i == 0)
                        coordinates.Add(new Coordinate(roadSegments[j].Source.Vertex.Longitude, roadSegments[j].Source.Vertex.Latitude));
                    coordinates.Add(new Coordinate(roadSegments[j].Target.Vertex.Longitude, roadSegments[j].Target.Vertex.Latitude));
                }

                route.Waypoints[i].UpdateLocationWithoutChangeNotification(roadSegments[0].Source.Vertex);
            }
            var geom = new DepictionGeometry(geomFact.CreateLineString(coordinates.ToArray()));
            lock (route.ZoneOfInfluence)
            {
                route.SetZOIGeometryAndUpdatePosition(geom);
            }
        }
    }
}