﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.OldValidationRules;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.OldValidationRules;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Algorithm;
using GisSharpBlog.NetTopologySuite.Geometries;
using QuickGraph.Algorithms.ShortestPath;
#if(DEBUG)
#elif(OSINT)
#else
using Depiction.API;
//using Depiction.LicenseManager.Model;
#endif

namespace Depiction.DriveTimeAddon
{
    [Export(typeof(BaseBehavior))]
    [Behavior("CalculateDriveTimeRadius", "Calculate drive time", "Calculate drive time",
        DisplayName = "Logistics Add-on", Author = "Depiction, Inc.",
        Description = "<html>The Depiction Logistics Add-on adds three new elements to Depiction: the Drive Time Radius, which draws a polygon to represent the area within a specified drive time from a location; the Route - Drive Time, which calculates the shortest route based on speed limit data; and the Route - Optimized Drive Time, which calculates the most efficient route between multiple points based on speed limit data.</html>",
        AddonPackage = "Drive time")]
    public class DriveTimeRadiusBehavior : BaseBehavior, IDepictionAddonBase
    {
        private static readonly ParameterInfo[] parameters =
            new[]
                {
                    new ParameterInfo("RoadNetwork", typeof (IDepictionElementBase))
                        {
                            ParameterName = "Road Network", 
                            ParameterDescription = "The road network to use when calculating the route"
                        },
                        new ParameterInfo("Time", typeof (double))
                        {
                            ParameterName = "Elapsed time",
                            ParameterDescription = "Elapsed time in minutes",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "Elapsed time must be a number"),
                                        new MinValueValidationRule(0)
                                    }
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
//#if(DEBUG)
//#elif(OSINT)
//#else
//            if (!LogisticsLicenseService.LicenseService().LicenseState.Equals(DepictionLicenseState.ActivatedNoExpiration))
//            {
//                DepictionAccess.NotificationService.DisplayMessageString(
//                    "Logistics Add On must be activated from 'Tools -> Add ons' before using its elements.");
//                return new BehaviorResult();
//            }
//#endif
            var elapsedTime = (double)parameterBag["Time"];
            var roadNetwork = (IDepictionElement)parameterBag["RoadNetwork"];

            var roadGraph = roadNetwork != null ? roadNetwork.GetPropertyByInternalName("RoadGraph").Value as IRoadGraph : null;

            ILatitudeLongitude startingPoint = subscriber.Position;
            GeometryFactory geomFactory = new GeometryFactory(new PrecisionModel());

            var coordinates = AttemptToFindNodesWithinDistance(subscriber, startingPoint, roadGraph, elapsedTime);
            var hull = new ConvexHull(coordinates.ToArray(), geomFactory);

            var geometry = hull.GetConvexHull() as Polygon;
            if (geometry != null)
            {
                Console.WriteLine("input is {0} points, convex hull is {1} points", coordinates.Count, geometry.Coordinates.Length);
                subscriber.SetZOIGeometry(new DepictionGeometry(geometry));
            }


            return new BehaviorResult();
        }

        private static List<ICoordinate> AttemptToFindNodesWithinDistance(IDepictionElement route, ILatitudeLongitude latitudeLongitude, IRoadGraph roadGraph, double maximumTime)
        {
            var node = roadGraph.FindNearestNode(latitudeLongitude);
            var coordinates = new List<ICoordinate>();
            var dijkstraa = new DijkstraShortestPathAlgorithm<RoadNode, RoadSegment>(roadGraph.Graph, roadGraph.GetDriveTimeFunc(route));
            dijkstraa.Compute(node);

            foreach (var target in dijkstraa.Distances)
            {
                if (target.Value < maximumTime)
                {
                    coordinates.Add(new Coordinate(target.Key.Vertex.Longitude, target.Key.Vertex.Latitude));
                }
            }

            return coordinates;

        }

        public object AddonMainView
        {
            get { return null; }
        }

        public string AddonConfigViewText
        {
            get { return null; }
        }

        public object AddonConfigView
        {
            get { return null; }
        }
        public object AddonActivationView
        {

#if(DEBUG)
           get { return null; }     
#elif(OSINT)
                get { return null; }
#else
            get
            {
                return null;
//                if (!LogisticsLicenseService.LicenseService().LicenseState.Equals(DepictionLicenseState.ActivatedNoExpiration))
//                    return new ActivationDialog { Service = LogisticsLicenseService.LicenseService() };
//                return new LicenseInformationDialog(LogisticsLicenseService.LicenseService());
            }
#endif
        }

        public ViewModelBase AddinViewModel
        {
            get { return null; }
        }
    }
}