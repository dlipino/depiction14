﻿using System;
using System.IO;
using Depiction.API;
using Depiction.LicenseManager.Model;

namespace Depiction.DriveTimeAddon
{
    internal static class LogisticsLicenseService
    {
        private static LicenseService licenseService;
        private static readonly string licenseDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction");
        private static readonly string licensePath = Path.Combine(licenseDir, "logisticsv1.lic");
            
        public static LicenseService LicenseService()
        {
            //get
            {
                if (!DepictionAccess.DepictionDispatcher.CheckAccess())
                {
                    var result = DepictionAccess.DepictionDispatcher.Invoke(new Func<LicenseService>(LicenseService));
                    return result as LicenseService;
                }
                if (licenseService == null)
                    licenseService = new LicenseService(licensePath);
                return licenseService;
            }
        }
    }
}
