﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace OSMDataImporter
{

    [DepictionNonDefaultImporterMetadata("OpenStreetMapPlaceOfWorshipImporter",
    ImporterSources = new[] { InformationSource.Web },
    Description = "This quick start data source will bring in all of the places of worship that are within the boundaries of your depiction that have been identified within OpenStreetMap. Warning: OpenStreetMap is a crowd sourced mapping project and all places of worship in your area may not be present. However, you are welcome to become an OSM contributor and add all of the places of worship in your community and then they will be available. Check out http://www.openstreetmap.org.",
    DisplayName = "Places of Worship (OpenStreetMap)")]
    public class OSMPlaceOfWorshipImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "place_of_worship"; } }
        protected override string ElementType { get { return "Depiction.Plugin.PlaceOfWorship"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapHospitalImporter", 
        ImporterSources = new[] { InformationSource.Web },
        Description = "This quick start data source will bring in all of the hospitals that are within the boundaries of your depiction that have been identified within OpenStreetMap. Warning: OpenStreetMap is a crowd sourced mapping project and all hospitals in your area may not be present. However, you are welcome to become an OSM contributor and add all of the hospitals in your community and then they will be available. Check out http://www.openstreetmap.org.",
        DisplayName = "Hospitals (OpenStreetMap)")]
    public class OSMHospitalImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "hospital"; } }
        protected override string ElementType { get { return "Depiction.Plugin.Hospital"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapPoliceStationImporter",
        ImporterSources = new[] { InformationSource.Web },
        Description = "This quick start data source will bring in all of the police stations that are within the boundaries of your depiction that have been identified within OpenStreetMap. Warning: OpenStreetMap is a crowd sourced mapping project and all police stations in your area may not be present. However, you are welcome to become an OSM contributor and add all of the police stations in your community and then they will be available. Check out http://www.openstreetmap.org.",
        DisplayName = "Police Stations (OpenStreetMap)")]
    public class OSMPoliceImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "police"; } }
        protected override string ElementType { get { return "Depiction.Plugin.PoliceStation"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapFireStationImporter",
        ImporterSources = new[] { InformationSource.Web },
        Description = "This quick start data source will bring in all of the fire stations that are within the boundaries of your depiction that have been identified within OpenStreetMap. Warning: OpenStreetMap is a crowd sourced mapping project and all fire stations in your area may not be present. However, you are welcome to become an OSM contributor and add all of the fire stations in your community and then they will be available. Check out http://www.openstreetmap.org.",
        DisplayName = "Fire Stations (OpenStreetMap)")]
    public class OSMFireStationImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "fire_station"; } }
        protected override string ElementType { get { return "Depiction.Plugin.FireStation"; } }
    }

    [DepictionNonDefaultImporterMetadata("OpenStreetMapSchoolImporter",
        ImporterSources = new[] { InformationSource.Web },
        Description = "This quick start data source will bring in all of the schools that are within the boundaries of your depiction that have been identified within OpenStreetMap. Warning: OpenStreetMap is a crowd sourced mapping project and all schools in your area may not be present. However, you are welcome to become an OSM contributor and add all of the schools in your community and then they will be available. Check out http://www.openstreetmap.org.",
        DisplayName = "Schools (OpenStreetMap)")]
    public class OSMSchoolImporter : OSMPointDataImporter
    {
        protected override string LayerName { get { return "school"; } }
        protected override string ElementType { get { return "Depiction.Plugin.School"; } }
    }

    public abstract class OSMPointDataImporter : NondefaultDepictionImporterBase
    {
        protected abstract string LayerName { get; }
        protected abstract string ElementType { get; }
        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {

            var operationParams = new Dictionary<string, object>();
            operationParams.Add("Area", depictionRegion);
            operationParams.Add("layerName", LayerName);
            operationParams.Add("elementType", ElementType);
            foreach (var param in parameters)
            {
                operationParams.Add(param.Key, param.Value);
            }
            var streetmapService = new OpenStreetMapPointDataImporterService();
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(streetmapService);
            streetmapService.StartBackgroundService(operationParams);
        }
    }
}
