using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using Depiction.API.AbstractObjects;
using Depiction.API.ValueTypes;

namespace OSMDataImporter.OSMObjects
{
    public class OSMHelperMethods
    {
        #region static xml to osm roadnetwork parts
        static public void GetNodesAndWays(string xmlPath, out OSMNode[] nodesInFile, out OSMWay[] waysInFile, BaseDepictionBackgroundThreadOperation threadConnection)
        {
            var nodes = new List<OSMNode>();
            var ways = new List<OSMWay>();

            using (var reader = new XmlTextReader(xmlPath) { XmlResolver = null })
            {
                while (reader.Read())
                {
                    if (threadConnection != null && threadConnection.ServiceStopRequested)
                    {
                        nodesInFile = new OSMNode[0];
                        waysInFile = new OSMWay[0];
                        reader.Close();
                        return;
                    }
                    if (reader.NodeType != XmlNodeType.Element) continue;

                    if (reader.Name == "node")
                    {
                        OSMNode nodeExtracted = ExtractNode(reader);
                        nodes.Add(nodeExtracted);
                    }
                    if (reader.Name == "way")
                    {
                        OSMWay wayExtracted = ExtractWay(reader);
                        ways.Add(wayExtracted);
                    }
                }
            }
            nodesInFile = nodes.ToArray();
            waysInFile = ways.ToArray();
        }
        private static OSMNode ExtractNode(XmlTextReader reader)
        {
            var nodeId = reader.GetAttribute("id");
            var latitude = reader.GetAttribute("lat");
            var longitude = reader.GetAttribute("lon");
            var tags = new Dictionary<string, string>();
            //var timestamp = reader.GetAttribute("timestamp");
            //var user = reader.GetAttribute("user");

            string name = null;
            if (!reader.IsEmptyElement)
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "node") break;
                    if (reader.Name == "tag")
                    {
                        var k = reader.GetAttribute("k");
                        var v = reader.GetAttribute("v");
                        tags.Add(k, v);
                    }
                }
            }
            return new OSMNode { LatLong = new LatitudeLongitude(latitude, longitude), NodeID = Convert.ToInt64(nodeId), Tags = tags };
        }

        private static OSMWay ExtractWay(XmlTextReader reader)
        {
            var wayId = reader.GetAttribute("id");
            var timestamp = reader.GetAttribute("timestamp");
            var user = reader.GetAttribute("user");
            var nodeIds = new List<long>();
            var tags = new Dictionary<string, string>();

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    return new OSMWay { NodeIDs = nodeIds.ToArray(), Tags = tags, TimeStamp = Convert.ToDateTime(timestamp), User = user, WayID = Convert.ToInt64(wayId) };
                }

                if (reader.NodeType != XmlNodeType.Element) continue;

                if (reader.Name == "nd")
                    nodeIds.Add(Convert.ToInt64(reader.GetAttribute("ref")));

                if (reader.Name == "tag")
                {
                    var key = reader.GetAttribute("k");
                    var value = reader.GetAttribute("v");
                    if (!tags.ContainsKey(key))
                        tags.Add(key, value);
                    else
                    {
                        if (!tags[key].Equals(value))
                            throw new Exception(
                                string.Format("Could not add ({0}, {1}), since ({0},{2}) already exists in tags.",
                                              key, value, tags[key]));
                    }
                }
            }

            return new OSMWay { NodeIDs = nodeIds.ToArray(), Tags = tags, TimeStamp = Convert.ToDateTime(timestamp), User = user, WayID = Convert.ToInt64(wayId) };
        }
        #endregion

    }
}