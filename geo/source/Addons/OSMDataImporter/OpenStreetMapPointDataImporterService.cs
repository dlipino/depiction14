﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.ValueTypes;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using OSMDataImporter.OSMObjects;
using Point = System.Windows.Point;

namespace OSMDataImporter
{
    public class OpenStreetMapPointDataImporterService : BaseDepictionBackgroundThreadOperation
    {
private const int maxNumberOfThreads = 1;
        private const double cachedTileDiameterInDegrees = .1;//was .03, but there are issues with the amount of info which stops the transfer in certain areas
        //TODO send this as a parameter from webservice
        readonly Dictionary<long, OSMNode> nodes = new Dictionary<long, OSMNode>();
        readonly Dictionary<long, OSMWay> ways = new Dictionary<long, OSMWay>();
        //private string osmURL = "http://open.mapquestapi.com/xapi/api/0.6/way[highway=motorway|motorway_link|trunk|trunk_link|primary|primary_link|secondary|tertiary|unclassified|road|residential|living_street|service|track][bbox={0},{1},{2},{3}]";
        private string osmURL = "http://www.overpass-api.de/api/xapi?*[bbox={0},{1},{2},{3}][amenity={4}]";
        private int numberOfWorkers;
        private readonly object numberOfWorkersLock = new object();
        private readonly object tilesRetrievedLock = new object();
        private double totalTiles;
        private double tilesRetrieved;
        private TileCacheService cacheService;
        private string layerName;
        private string elementType;
        private IMapCoordinateBounds area;

        public bool ReplaceCachedInformation { get { return Settings.Default.ReplaceCachedFiles; } }

        #region Constructor

        public OpenStreetMapPointDataImporterService()
        {
            cacheService=new TileCacheService("OSMPoints");
        }

        #endregion

        #region Overrides of BaseDepictionBackgroundThreadOperation

        override protected bool ServiceSetup(Dictionary<string, object> args)
        {
            UpdateStatusReport("Open Street Map Points");
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var argDictionary = args as Dictionary<string, object>;
            if (argDictionary == null) return null;
            if (!argDictionary.ContainsKey("Area")) return null;
            var area = argDictionary["Area"] as IMapCoordinateBounds;

            if (argDictionary.ContainsKey("url"))
            {
                osmURL = argDictionary["url"].ToString();
            }
            if (argDictionary.ContainsKey("layerName"))
            {
                layerName = argDictionary["layerName"].ToString();
            }

            if (argDictionary.ContainsKey("elementType"))
            {
                elementType = argDictionary["elementType"].ToString();
            }
            var elements = CreatePointElements(area, osmURL);
            return elements;
        }

        protected override void ServiceComplete(object args)
        {
            var argElement = args as IList<IDepictionElement>;
            if (argElement != null)
            {
                UpdateStatusReport("Adding elements to depiction");
                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(argElement, true);
            }
        }
        #endregion

        #region protecteed methdos

        protected IList<IDepictionElement> CreatePointElements(IMapCoordinateBounds area, string newOsmUrl)
        {
            this.area = area;
            if (!string.IsNullOrEmpty(newOsmUrl))
            {
                osmURL = newOsmUrl;
            }
            var firstCol = getColumn(area.Left);
            var lastCol = getColumn(area.Right);
            var firstRow = getRow(area.Top);
            var lastRow = getRow(area.Bottom);

            totalTiles = (lastCol - firstCol + 1) * (lastRow - firstRow + 1);

            for (int col = firstCol; col <= lastCol; col++)
            {
                for (int row = firstRow; row <= lastRow; row++)
                {
                    while (numberOfWorkers >= maxNumberOfThreads)
                    {
                        Thread.Sleep(100);
                    }
                    if (!ServiceStopRequested)
                    {
                        BackgroundWorker osmRoadNetworkTileGetter = new BackgroundWorker();
                        osmRoadNetworkTileGetter.DoWork += osmRoadNetworkTileGetter_DoWork;
                        osmRoadNetworkTileGetter.RunWorkerCompleted += osmRoadNetworkTileGetter_RunWorkerCompleted;
                        Thread.Sleep(100);
                        osmRoadNetworkTileGetter.RunWorkerAsync(new Point(col, row));

                        lock (numberOfWorkersLock)
                        {
                            numberOfWorkers++;
                        }
                    }
                }
            }

            while (numberOfWorkers > 0) Thread.Sleep(100);
            if (ServiceStopRequested) return null;

            var message = string.Format("Importing {0} nodes", nodes.Count);
            UpdateStatusReport(message);
            var elements = GeneratePointElements(nodes, ways);
            return elements;
        }

        #endregion
        #region tile thread methods

        void osmRoadNetworkTileGetter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lock (tilesRetrievedLock)
            {
                tilesRetrieved++;
                var message = string.Format("Retrieved {0} of {1} tiles", tilesRetrieved, totalTiles);
                UpdateStatusReport(message);
            }
            lock (numberOfWorkersLock)
            {
                numberOfWorkers--;
            }
        }

        void osmRoadNetworkTileGetter_DoWork(object sender, DoWorkEventArgs e)
        {
            Point p = (Point)e.Argument;

            int col = (int)p.X;
            int row = (int)p.Y;
            if (ServiceStopRequested) return;
            var fileName = "OSMPoints_" + layerName + "_row_" + row + "_col_" + col + ".xml";
            string fullxmlFileName;
            if(cacheService == null)
            {
                fullxmlFileName = DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName);
            }else
            {
                fullxmlFileName = cacheService.GetCacheFullStoragePath(fileName);
            }

            if (!File.Exists(fullxmlFileName) || ReplaceCachedInformation)
            {
                var box = GetBoundingBox(col, row);
                fullxmlFileName = GetXML(string.Format(CultureInfo.InvariantCulture, osmURL, box.Left, box.Bottom, box.Right, box.Top, layerName), fullxmlFileName, 0);
            }

            if (string.IsNullOrEmpty(fullxmlFileName)) return;

            try
            {
                OSMWay[] waysInFile;
                OSMNode[] nodesInFile;
                OSMHelperMethods.GetNodesAndWays(fullxmlFileName, out nodesInFile, out waysInFile, this);

                lock (nodes)
                {
                    foreach (var node in nodesInFile)
                    {
                        if (ServiceStopRequested) break;
                        
                        if (!nodes.ContainsKey(node.NodeID))
                            nodes.Add(node.NodeID, node);
                    }
                }

                lock (ways)
                {
                    foreach (var way in waysInFile)
                    {
                        if (ServiceStopRequested) break;

                        if (!ways.ContainsKey(way.WayID))
                            ways.Add(way.WayID, way);
                    }
                }
            }
            catch (Exception)
            {
                // Don't keep a cached file that failed to process.

                if (File.Exists(fullxmlFileName))
                    File.Delete(fullxmlFileName);
                // DepictionAccess.PathService.DeleteCachedFile(fileName);
            }
        }
        #endregion

        #region public methods

        public IList<IDepictionElement> GeneratePointElements(Dictionary<long, OSMNode> osmNodes, Dictionary<long, OSMWay> osmWays)
        {
            var elements = new List<IDepictionElement>();
            var geometryFactory = new GeometryFactory();
            foreach (var way in osmWays.Values)
            {
                if (ServiceStopRequested) return null;
                if (!way.Tags.ContainsKey("amenity") || !string.Equals(layerName, way.Tags["amenity"])) continue;
                IElementPrototype prototype;
                if (!string.IsNullOrEmpty(elementType))
                    prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(elementType);
                else
                    prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.PointOfInterest");
                var element = ElementFactory.CreateElementFromPrototype(prototype);
                if (way.Tags.ContainsKey("name") && !string.IsNullOrEmpty(way.Tags["name"]))
                    element.SetPropertyValue("displayname", way.Tags["name"]);

                List<ICoordinate> coordinates = new List<ICoordinate>(way.NodeIDs.Length);
                bool inRequestedArea = false;
                foreach (var nodeID in way.NodeIDs)
                {
                    var nodeLatLong = nodes[nodeID].LatLong;
                    if (area.Contains(nodeLatLong))
                    {
                        inRequestedArea = true;
                    }
                    coordinates.Add(new Coordinate(nodeLatLong.Longitude, nodeLatLong.Latitude));
                }

                if (!inRequestedArea)
                {
                    continue;
                }
                try
                {
                    var shell = geometryFactory.CreateLinearRing(coordinates.ToArray());
                    var polygon = geometryFactory.CreatePolygon(shell, null);
                    IDepictionGeometry geometry = new DepictionGeometry(polygon);
                    element.SetZOIGeometryAndUpdatePosition(geometry);
                }
                catch 
                {
                    if (coordinates.Count > 0)
                        element.SetZOIGeometryAndUpdatePosition(new DepictionGeometry(new LatitudeLongitude(coordinates[0].Y, coordinates[0].X)));
                }
                
                elements.Add(element);
            }

            foreach (var node in osmNodes.Values)
            {
                if (ServiceStopRequested) return null;
                if (!node.Tags.ContainsKey("amenity") || !string.Equals(layerName, node.Tags["amenity"])) continue;
                bool inRequestedArea = area.Contains(node.LatLong);
                if (!inRequestedArea)
                {
                    continue;
                }
                IElementPrototype prototype;
                if (!string.IsNullOrEmpty(elementType))
                    prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName(elementType);
                else
                    prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.PointOfInterest");

                var element = ElementFactory.CreateElementFromPrototype(prototype);
                if (node.Tags.ContainsKey("name") && !string.IsNullOrEmpty(node.Tags["name"]))
                    element.SetPropertyValue("displayname", node.Tags["name"]);
                element.Position = node.LatLong;
                elements.Add(element);
            }
            return elements;
        }

        #endregion

        #region private helpers

        #region static methods
        

        private static int getColumn(double Longitude)
        {
            return Convert.ToInt32(Math.Floor((Longitude + 180) / cachedTileDiameterInDegrees));
        }

        private static int getRow(double Latitude)
        {
            return Convert.ToInt32(Math.Floor(Math.Abs(Latitude - 80) / cachedTileDiameterInDegrees));
        }

        private static IMapCoordinateBounds GetBoundingBox(int column, int row)
        {
            var topLeft = new LatitudeLongitude(80 - (row * cachedTileDiameterInDegrees), (column * cachedTileDiameterInDegrees) - 180);
            return new MapCoordinateBounds(topLeft, new LatitudeLongitude(topLeft.Latitude - cachedTileDiameterInDegrees, topLeft.Longitude + cachedTileDiameterInDegrees));
        }
        #endregion

        private string GetXML(string url, string fileName, int exceptionCount)
        {
            //var webClient = new WebClient();
            //var buffer = new byte[0];
            if (ServiceStopRequested) return null;

            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.AutomaticDecompression = DecompressionMethods.GZip;
            HttpWebResponse webResponse = null;
            if (ServiceStopRequested) return null;
            try
            {
                webResponse = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (WebException ex)
            {
                // Retry 3 times if there is a web exception (time-out)
                if (ServiceStopRequested)
                {
                    if (webResponse != null) webResponse.Close();
                    return null;
                }

                if (exceptionCount < 3)
                {
                    return GetXML(url, fileName, exceptionCount + 1);
                }
                DepictionAccess.NotificationService.DisplayMessageString(ex.Message, 5);
            }
            if (ServiceStopRequested || webResponse == null)
            {
                if (webResponse != null) webResponse.Close();
                return null;
            }
            var buffer = webResponse.GetResponseStream();
            if (buffer == null) return string.Empty;
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                buffer.CopyTo(fileStream);
            }
            // if this file was created while we were waiting, let's not redo it, why would this occur
//            var valuetoReturn = DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName) ??
//                                DepictionAccess.PathService.CacheFile(webResponse.GetResponseStream(), fileName);
            webResponse.Close();
            return fileName;
        }



        #endregion
    }
}
