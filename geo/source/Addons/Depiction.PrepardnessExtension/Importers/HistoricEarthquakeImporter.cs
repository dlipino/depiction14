using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.Service.CoreBackgroundServices;

namespace Depiction.PreparednessExtension.Importers
{
    [DepictionNonDefaultImporterMetadata("USGSRecentEarthquakeImporter",
        Description = "Recent magnitude 1 or greater earthquakes from around the world recorded by the U.S. Geological Survey (USGS) using a variety of global data sources and partners. Additional info: <a href=\"http://earthquake.usgs.gov/eqcenter/\">http://earthquake.usgs.gov/eqcenter/</a>",
        DisplayName = "Earthquakes -- Past 7 Days (USGS)",
        ImporterSources = new[] { InformationSource.Web },AddonPackage="Preparedness")]
    public class HistoricEarthquakeImporter : NondefaultDepictionImporterBase
    {
        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            //            http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-age.kmz 
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new EarthQuakeElementProviderBackground();
            var importerName = "Earthquakes -- Past 7 Days (USGS)";
            var name = string.Format(importerName);
            var elementTypeKey = DepictionWxsToElementService.elementTypeParamString;
            var epicenterType = "Depiction.Plugin.Epicenter";
            if (parameters.ContainsKey(elementTypeKey))
            {
                parameters[elementTypeKey] = epicenterType;
            }
            else
            {
                parameters.Add(elementTypeKey, epicenterType);
            }
            var nameKey = "name";
            if (parameters.ContainsKey(nameKey))
            {
                parameters[nameKey] = name;
            }
            else
            {
                parameters.Add(nameKey, name);
            }
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);

        }
    }

    public class EarthQuakeElementProviderBackground : BaseDepictionBackgroundThreadOperation
    {
        private string name = "Earthquakes -- Past 7 Days (USGS)";
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var earthQuakeKMZLocation = new Uri("http://earthquake.usgs.gov/eqcenter/catalogs/eqs7day-age.kmz");
            var fileName = "eqs7day-age.kmz";
            UpdateStatusReport(string.Format("Downloading earthquake information"));
            var fullName = Path.Combine(DepictionAccess.PathService.DepictionCacheDirectory, fileName);
            using (WebClient wbClient = new WebClient())
            {
                wbClient.DownloadFile(earthQuakeKMZLocation, fullName);
            }

            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var elementType = parameters[DepictionWxsToElementService.elementTypeParamString].ToString();
            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;
            var tag = Tags.DefaultFileTag + Path.GetFileName(fullName);
            if (parameters.ContainsKey("Tag"))
            {
                tag = parameters["Tag"].ToString();
            }
            if (File.Exists(fullName))
            {
                UpdateStatusReport(string.Format("Reading {0}.", fileName));
                var prototypes = KMLKMZBackgroundService.ProcessKMZKMLFileIntoRawPrototypes(fullName, elementType, regionBounds);
                foreach (var prototype in prototypes)
                {
                    prototype.UseEnhancedPermaText = true;
                    prototype.PermaText.PixelHeight = 250;
                    prototype.PermaText.PixelWidth = 310;
                    var prop = prototype.GetPropertyByInternalName("displayName");
                    var eid = string.Empty;
                    if (prop != null)
                    {
                        prop.IsHoverText = true;
                        eid = prop.Value.ToString();
                    }
                    prop = prototype.GetPropertyByInternalName("description");
                    if (prop != null)
                    {
                        prop.IsHoverText = true;
                    }
                    if (!string.IsNullOrEmpty(eid))
                    {
                        var eidProp = new DepictionElementProperty("eid", eid);
                        eidProp.VisibleToUser = true;
                        eidProp.Editable = false;
                        prototype.AddPropertyOrReplaceValueAndAttributes(eidProp);
                    }

                    prototype.Tags.Add(tag);
                }
                UpdateStatusReport(string.Format("Finished reading {0} elements,preparing to display.", prototypes.Count));
                return prototypes;
            }
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            if (DepictionAccess.CurrentDepiction == null) return;
            var prototypes = args as IEnumerable<IElementPrototype>;
            if (prototypes == null || prototypes.Count() == 0)
            {
                var message =
                    string.Format(
                        "Could not add {0} The source may be unavailable or there is no data for the requested area", name);
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                return;
            }

            DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(prototypes, true, false);
        }

        #endregion
    }
}