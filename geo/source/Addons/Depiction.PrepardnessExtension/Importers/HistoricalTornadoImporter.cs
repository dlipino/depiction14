using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.Service;

namespace Depiction.PreparednessExtension.Importers
{
    [DepictionNonDefaultImporterMetadata("HistoricalTornadoGatherer1950-2006",
        Description = "These lines indicate the approximate path, date and intensity of historical tornado events; compiled by NOAA's National Weather Service (NWS) Storm Prediction Center (SPC). Additional info: <a href=\"http://www.data.gov/details/47\">http://www.data.gov/details/47</a>",
        DisplayName = "Tornado Tracks 1950-2006 (NOAA)",
        ImporterSources = new[] { InformationSource.Web }, ValidRegions = new[] { "US" }, AddonPackage = "Preparedness")]
    public class HistoricalTornadoImporter : NondefaultDepictionImporterBase
    {
        #region Overrides of AbstractDepictionDefaultImporter

        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new HistoricalTornadoImporterBackgroundService();
            var name = string.Format("Tornado Tracks 1950-2006 (NOAA)");

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }

        #endregion

    }

    public class HistoricalTornadoImporterBackgroundService : BaseDepictionBackgroundThreadOperation
    {
        private string name = "Tornado Tracks 1950-2006 (NOAA)";
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;
            var area = parameters["RegionBounds"] as IMapCoordinateBounds;
            if (area == null) return null;

            var typeKey = DepictionWxsToElementService.elementTypeParamString;
            var hurricaneType = "Depiction.Plugin.TornadoTrack";
            if (!parameters.ContainsKey(typeKey))
            {
                parameters.Add(typeKey, hurricaneType);
            }
            else
            {
                parameters[typeKey] = hurricaneType;
            }
            return GatherElements(area, parameters);
        }

        protected override void ServiceComplete(object args)
        {
            if (DepictionAccess.CurrentDepiction == null) return;
            var elements = args as IEnumerable<IElementPrototype>;
            if (elements == null || elements.Count() == 0)
            {
                var message =
                    string.Format(
                        "Could not add {0} The source may be unavailable or there is no data for the requested area", name);
                DepictionAccess.NotificationService.DisplayMessageString(message, 3);
                return;
            }

            UpdateStatusReport("Adding tornado tracks");
            DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(elements, true,false);

        }

        #endregion

        #region copied from HistoricialHurricane
        public IElementPrototype[] GatherElements(IMapCoordinateBounds area, Dictionary<string, object> parameters)
        {
            var prototypes = DepictionWxsToElementService.GetDataFromWxSUsingParameter(parameters, area, this);
            List<object> fscales = new List<object>();
            foreach (var elem in prototypes)
            {
                elem.UsePropertyNameInHoverText = true;
                var prop = elem.GetPropertyByInternalName("DATE");
                if (prop != null)
                {
                    prop.IsHoverText = true;
                }
                prop = elem.GetPropertyByInternalName("FSCALE");//F_SCALE
                if (prop != null)
                {
                    fscales.Add(prop.Value);
                    prop.IsHoverText = true;
                }
                prop = elem.GetPropertyByInternalName("YEAR");
                if (prop != null)
                {
                    prop.IsHoverText = true;
                }
            }
            //color it by fscale
            var colorPoints = new List<Color>();
            colorPoints.Add(Colors.Yellow);
            colorPoints.Add(Colors.Red);

            var domainPoints = new List<object>();
            fscales.Sort();
            if (fscales.Count > 0)
            {
                domainPoints.Add(fscales.First());
                domainPoints.Add(fscales.Last());
            }
            var colorMap = new ColorInterpolator(domainPoints, colorPoints);
            var borderName = "ZOIBorder";
            foreach (var prototype in prototypes)
            {
                var prop = prototype.GetPropertyByInternalName("FSCALE");//F_SCALE//Different from 1.2, for some reason the '_' gets removed
                if (prop != null)
                {
                    var propVal = colorMap.GetColorForValue(prop.Value);
                    if (prototype.SetPropertyValue(borderName, propVal) != true)
                    {
                        var newProp = new DepictionElementProperty(borderName, "Line color", propVal);
                        newProp.Deletable = false;
                        newProp.Rank = 10;
                        prototype.AddPropertyOrReplaceValueAndAttributes(newProp, false);
                    }
                }
            }

            return prototypes.ToArray();
        }

        #endregion
    }
}