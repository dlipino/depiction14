﻿using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Navigation;
using Depiction.PublishToWebAddon.Converters;

namespace Depiction.PublishToWebAddon
{
    /// <summary>
    /// Interaction logic for PublishToWebSucceededDialog.xaml
    /// </summary>
    public partial class PublishToWebSucceededDialog
    {
        public static readonly BooleanInverterConverter BooleanInverter = new BooleanInverterConverter();
        public static readonly BooleanToVisibilityConverter BooleanToVisibility = new BooleanToVisibilityConverter();
        public PublishToWebSucceededDialog()
        {
            InitializeComponent();
            Resizeable = true;
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
