﻿using System.Windows.Input;
using Depiction.API.MVVM;

namespace Depiction.PublishToWebAddon
{
    public class PublishToWebSucceededViewModel : DialogViewModelBase
    {
        public PublishToWebSucceededViewModel(string depictionURL)
        {
            DepictionURL = depictionURL;
            DepictionEmbedCode = string.Format("<iframe src=\"{0}\" width=\"800\" height=\"600\"></iframe>", depictionURL);
        }
        public string DepictionURL { get; set; }
        public string DepictionEmbedCode { get; set; }

        private DelegateCommand okCommand;

        public ICommand OKCommand
        {
            get
            {
                if (okCommand == null)
                    okCommand = new DelegateCommand(close);
                return okCommand;
            }
        }

        private void close()
        {
            IsDialogVisible = false;
        }
    }
}