﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.MVVM;
using Depiction.API.StaticAccessors;
using Ionic.Zip;
using Microsoft.Win32;
using Newtonsoft.Json;

namespace Depiction.PublishToWebAddon
{
    class PublishToWebViewModel : DialogViewModelBase
    {
        public string Title { get; set; }
        //public string Author { get; set; }
        public string Description { get; set; }

        public bool HideBackground { get; set; }
        private bool publishLocally;
        public bool PublishLocally
        {
            get { return publishLocally; }
            set
            {
                publishLocally = value;
                NotifyPropertyChanged("PublishLocally");
                NotifyPropertyChanged("PublishOnServer");
            }
        }

        public bool PublishOnServer
        {
            get { return !PublishLocally; }
        }

        private string localFilename;
        public string LocalFilename
        {
            get { return localFilename; }
            set
            {
                localFilename = value;
                NotifyPropertyChanged("LocalFilename");
            }
        }

        public string Username { get; set; }
        public string Filename { get; set; }
        public string Password { get; set; }
        //public LegendType LegendType { get; set; }

        //private Dictionary<LegendType, string> legendTypeList;
        //public IDictionary LegendTypeList
        //{
        //    get 
        //    { 
        //        if (legendTypeList == null)
        //        {
        //            legendTypeList = new Dictionary<LegendType, string>();
        //            legendTypeList.Add(LegendType.NoLegend, "No legend");
        //            legendTypeList.Add(LegendType.Revealers, "Revealers");
        //            legendTypeList.Add(LegendType.Elements, "Elements");
        //            legendTypeList.Add(LegendType.RevealersAndElements, "Elements and Revealers");
        //        }
        //        return legendTypeList;
        //    }
        //}



        private DelegateCommand browseCommand;

        public ICommand BrowseCommand
        {
            get
            {
                if (browseCommand == null)
                    browseCommand = new DelegateCommand(executeBrowse, canExecuteBrowse);
                return browseCommand;
            }
        }

        private bool canExecuteBrowse()
        {
            return PublishLocally;
        }

        private void executeBrowse()
        {
            var dialog = new SaveFileDialog { Filter = "Zip files (*.zip)|*.zip" };
            dialog.ShowDialog();
            LocalFilename = dialog.FileName;
            if (LocalFilename == null)
                LocalFilename = string.Empty;
        }

        private DelegateCommand okCommand;
        
        public ICommand OKCommand
        {
            get
            {
                if (okCommand == null)
                    okCommand = new DelegateCommand(submit);
                return okCommand;
            }
        }

        //string webFolder;
        //string tempPath;
        //string saveDepictionFileName;
        //private bool subscribed;
        private void submit()
        {
                
            string tempPath = Path.Combine(DepictionAccess.PathService.TempFolderRootPath, Guid.NewGuid().ToString());

            string webFolder = Path.Combine(tempPath, "webDepiction");
            Directory.CreateDirectory(webFolder);

            bool succeeded = false;

            //Serialize to xml and image files
            var depictionWebSerializer = new DepictionWebSerializer(webFolder, Title, Description, LegendType.RevealersAndElements, HideBackground);
            depictionWebSerializer.Serialize();

            //Compress
            var dpnZipFilePath = Path.Combine(webFolder, "depictionForWeb.zip");
            using (var zip = new ZipFile())
            {
                zip.AddFile(Path.Combine(webFolder, "depictionForWeb.xml"), "");
                zip.AddDirectory(Path.Combine(webFolder, "Images"), "Images");
                zip.Save(dpnZipFilePath);
            }


#if DEBUG
            foreach (var file in Directory.GetFiles(webFolder))
            {
                File.Copy(file, Path.Combine(@"D:\programs\depiction14\geo\source\Viewer\SilverlightTest\ClientBin", Path.GetFileName(file)), true);
//                File.Copy(file, Path.Combine(@"c:\depiction\svn\trunk\geo\source\Viewer\SilverlightTest\ClientBin", Path.GetFileName(file)), true);
            }
#endif
            if (PublishLocally && !string.IsNullOrEmpty(LocalFilename))
            {
                DefaultResourceCopier.CopySpecificResourcesToDirectory(new Dictionary<string, string> {
                        {"Depiction.PublishToWebAddon.WebViewer.Silverlight.js", "Silverlight.js"},
                        {"Depiction.PublishToWebAddon.WebViewer.DepictionViewer.html", "DepictionViewer.html"},
                        {"Depiction.PublishToWebAddon.WebViewer.DepictionSilverlight.xap", "DepictionSilverlight.xap"}
                    }, webFolder, Assembly.Load("Depiction.PublishToWebAddon"));

                using (var zip = new ZipFile())
                {
                    zip.AddFile(dpnZipFilePath, "");
                    zip.AddFile(Path.Combine(webFolder, "Silverlight.js"), "");
                    zip.AddFile(Path.Combine(webFolder, "DepictionViewer.html"), "");
                    zip.AddFile(Path.Combine(webFolder, "DepictionSilverlight.xap"), "");
                    zip.Save(LocalFilename);
                }
                succeeded = true;
                DepictionAccess.NotificationService.DisplayMessageString(string.Format("Successfully published the depiction titled {0} to {1}.", Title, LocalFilename));
            }
            else if (!PublishLocally)
            {
                var byteArray = File.ReadAllBytes(dpnZipFilePath);

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://publish.depiction.com/");

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                object args = new { author = "", description = Description, depictionStory = byteArray, version = "1" };
                var response = client.PostAsJsonAsync("Home/Publish", args).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var storyUrl = JsonConvert.DeserializeObject<string>(result);
                    succeeded = true;

                    var successVM = new PublishToWebSucceededViewModel(storyUrl);
                    var successDialog = new PublishToWebSucceededDialog() { DataContext = successVM};
                    successDialog.Show();
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                }
            }

            IsDialogVisible = !succeeded;

        }

    }

    public enum LegendType
    {
        NoLegend, Revealers, Elements, RevealersAndElements
    }


}
