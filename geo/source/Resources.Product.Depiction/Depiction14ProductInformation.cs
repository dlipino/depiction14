﻿namespace Resources.Product.Depiction
{
    public class Depiction14ProductInformation : DepictionDebugInformation
    {
        public override string ProductType { get { return Release; } }

        public override string DirectoryNameForCompany { get { return "Depiction_14"; } }
        public override string ProductName { get { return "Depiction"; } }
        //This needs to be fixed so multiple version can run at the same time
        public override string ProcessName { get { return "Depiction"; } }
        public override string LicenseFileName { get { return "licensev14.lic"; } }

        //I really have no idea how theses work, or what changes these cause.
        public override string MetricsServiceName { get { return "ReleaseMetricsService"; } }
        public override string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
        public override string LicensingServiceName { get { return "ReleaseLicensingService"; } }
        public override string ProductValidatorName { get { return "ReleaseDepictionProductValidator"; } }
    }
}