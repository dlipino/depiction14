﻿using System.Collections.Specialized;
using Depiction.API.AbstractObjects;

namespace Resources.Product.Depiction
{
    public class DepictionDebugInformation : ProductInformationBase
    {
        public override string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
//                public override string QuickstartServiceName { get { return "DebugQuickstartService"; } }
        public override string ResourceAssemblyName { get { return "Resources.Product.Depiction"; } }
        public override string SplashScreenPath { get { return "Images/SplashScreen.png"; } }
//        public override string AppIconResourceName { get { return "Images/Depiction.ico"; } }
        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.",ProductVersion, DepictionCopyright);
            }
        }

        override public StringCollection ProductQuickAddElements
        {
            get
            {
                return new StringCollection{"RectangleShape","CircleShape","UserDrawnLine","UserDrawnPolygon"};
            }
        }
        public override string AboutFileAssemblyLocation
        {
            get { return "Docs.About.xps"; }
        }
        public override string EulaFileAssemblyLocation
        {
            get { return "Docs.DepictionEula.xps"; }
        }
    }
}