﻿<?xml version="1.0" encoding="utf-8"?>
<depictionElement elementType="Depiction.Plugin.Sensor" displayName="Sensor">
  <properties>
    <property name="Author" displayName="Author" value="Depiction, Inc." typeName="System.String" deletable="false" />
    <property name="Description" displayName="Description" value="Show the zones of different sensors by setting their range, orientation and field of view." typeName="System.String" editable="False" deletable="False" />
    <property name="SensorType" displayName="Sensor Type (e.g. motion)" value="" typeName="System.String" />
    <property name="Radius" displayName="Nominal range" value="1000 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API">
      <postSetActions>
        <action name="CreateArc">
          <parameters>
            <parameter elementQuery=".Radius" />
            <parameter elementQuery=".Orientation" />
            <parameter elementQuery=".Width" />
          </parameters>
        </action>
      </postSetActions>
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType" />
            <parameter value="Radius must be a distance." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
            <parameter value="1000000 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Orientation" displayName="Centerline orientation in degrees" value="30 degrees" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Angle, Depiction.API">
      <postSetActions>
        <action name="CreateArc">
          <parameters>
            <parameter elementQuery=".Radius" />
            <parameter elementQuery=".Orientation" />
            <parameter elementQuery=".Width" />
          </parameters>
        </action>
      </postSetActions>
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Orientation angle must be a number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Double" />
            <parameter value="360" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Width" displayName="Field of view in degrees" value="220" deleteIfEditZoi="true" typeName="System.Double">
      <postSetActions>
        <action name="CreateArc">
          <parameters>
            <parameter elementQuery=".Radius" />
            <parameter elementQuery=".Orientation" />
            <parameter elementQuery=".Width" />
          </parameters>
        </action>
      </postSetActions>
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Stop angle must be a number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Double" />
            <parameter value="360" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Notes" displayName="Additional notes" typeName="System.String" value="" />
    <property name="ShowElementPropertiesOnCreate" value="True" typeName="System.Boolean" editable="False" visible="False" />
    <property name="Draggable" displayName="Allow dragging" value="True" typeName="System.Boolean" deletable="false" />
    <property name="PlaceableWithMouse" value="true" editable="false" typeName="System.Boolean" visible="false" />
    <property name="IconPath" displayName="Icon path" value="embeddedresource:Depiction.Sensor" typeName="System.String" deletable="False" />
    <property name="ZOIFill" displayName="Fill color" value="#FF6E00F8" typeName="Color" deletable="False" />
    <property name="ZOIBorder" displayName="Outline color" value="#FF7100FF" typeName="Color" deletable="False" />
  </properties>
  <generateZoi>
    <actions>
      <action name="CreateArc">
        <parameters>
          <parameter elementQuery=".Radius" />
          <parameter elementQuery=".Orientation" />
          <parameter elementQuery=".Width" />
        </parameters>
      </action>
    </actions>
  </generateZoi>
</depictionElement>