using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Depiction.Converters.MultiValueConverters
{
    //LEgacy but around because it looks cool and does stuff in a magical way
    [ValueConversion(typeof(Visibility), typeof(bool)), Localizability(LocalizationCategory.NeverLocalize)]
    public class MultiBooleanToVisibilityConverter : IMultiValueConverter
    {
        private readonly bool useAndLogic;

        public MultiBooleanToVisibilityConverter(bool useAndLogic)
        {
            this.useAndLogic = useAndLogic;
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length < 1)
                return Visibility.Collapsed;

            bool result = useAndLogic;
            foreach (var value in values)
            {
                var boolValue = (bool) value;
                if (boolValue != useAndLogic)
                {
                    result = !useAndLogic;
                    break;
                }
            }

            return result ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}