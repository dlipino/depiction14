﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Depiction.Converters.ValueConverters
{
    public class ShiftGeometryToOriginConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Geometry outGeometry = value as Geometry;
            if(outGeometry == null) return null;
            if(value is RectangleGeometry)
            {
                outGeometry = new RectangleGeometry(new Rect(0, 0, outGeometry.Bounds.Width, outGeometry.Bounds.Height));
            }
            if(value is EllipseGeometry)
            {
                outGeometry = new EllipseGeometry(new Rect(0, 0, outGeometry.Bounds.Width, outGeometry.Bounds.Height));
            }
            return outGeometry;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}