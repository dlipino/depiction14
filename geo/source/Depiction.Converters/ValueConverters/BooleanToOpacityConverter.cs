﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Depiction.Converters.ValueConverters
{
    [ValueConversion(typeof(bool), typeof(double)), 
     Localizability(LocalizationCategory.NeverLocalize)]
    public class BooleanToOpacityConverter : IValueConverter
    {
        public BooleanToOpacityConverter(bool trueIsTransparent)
        {
            TrueIsTransparent = trueIsTransparent;
        }

        public BooleanToOpacityConverter() : this(true) { }

        public bool TrueIsTransparent { get; set; }

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                bool boolVal = (bool) value ^ TrueIsTransparent;
                if (!boolVal) return 0D;
                return 1D;
            }
            catch
            {
                return 1D;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}