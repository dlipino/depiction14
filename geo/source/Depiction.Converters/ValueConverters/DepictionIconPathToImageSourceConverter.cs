﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Depiction.API.ValueTypeConverters;

namespace Depiction.Converters.ValueConverters
{
    public class DepictionIconPathToImageSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converter = new DepictionIconPathTypeConverter();
            var obj = converter.ConvertTo(value,typeof (ImageSource));
            return obj;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}