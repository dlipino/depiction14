﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.Converters.ValueConverters
{
    public class DepictionDisplayerTypeToBoolConverter : IValueConverter
    {
        //Kind of a hack converter
        #region Implementation of IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is DepictionDisplayerType || value is string)
            {
                var param = parameter.ToString().ToLowerInvariant();
                var displayerType = value.ToString().ToLower();
                if (displayerType.Contains(param))
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            bool useValue = (bool)value;
            string targetValue = parameter.ToString();
            if (useValue)
                return Enum.Parse(targetType, targetValue);

            return null;
        }

        #endregion
    }
}