using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.CoreModel.ValueTypes;

namespace Depiction.Converters.ValueConverters
{
    public class DepictionAngleToDoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var angle = value as Angle;
            if (angle == null) return string.Empty;
            var doubleValue = angle.Value;
            return doubleValue - 90;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value + 90;
        }
    }
}