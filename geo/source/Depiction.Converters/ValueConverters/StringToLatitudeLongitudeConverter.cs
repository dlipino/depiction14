﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;

namespace Depiction.Converters.ValueConverters
{
    public class StringToLatitudeLongitudeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return new LatitudeLongitude();
            if (string.IsNullOrEmpty(value.ToString())) return new LatitudeLongitude();
            var converter = new LatitudeLongitudeTypeConverter();
            var obj = converter.ConvertFromString(value.ToString());
            return obj;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var converter = new LatitudeLongitudeTypeConverter();
            try
            {
                var obj = converter.ConvertFromString(value.ToString());
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}