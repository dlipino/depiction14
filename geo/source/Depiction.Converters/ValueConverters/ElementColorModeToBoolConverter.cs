﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.Converters.ValueConverters
{
    public class ElementColorModeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ElementColorMode)
            {
                var requiredMode = parameter.ToString();
                var currentMode = ((ElementColorMode)value).ToString().ToLower();
                if (requiredMode.ToLower().Equals(currentMode))
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {

                var selected = (bool) value;
                if(selected) return (ElementColorMode)Enum.Parse(typeof(ElementColorMode), parameter.ToString());
            }
            return ElementColorMode.NotAValidMode;
        }
    }
}