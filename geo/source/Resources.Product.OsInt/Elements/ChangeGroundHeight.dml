﻿<?xml version="1.0" encoding="utf-8"?>
<DepictionElement elementType="Depiction.Plugin.ChangeGroundHeight" displayName="Change elevation height">
  <properties>
    <property name="Author" displayName="Author" value="ARG3X International" typeName="String" editable="false" deletable="false" />
    <property name="Description" displayName="Description" value="Raise or lower a portion of the terrain elevation. To change the shape from a basic rectangle, shift+click on the shape, then shift+click to add vertices. " typeName="String" editable="false" deletable="false" />
    <property name="Length" displayName="Length" value="1000 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API">
      <postSetActions>
        <action name="CreateRectangle">
          <parameters>
            <parameter elementQuery=".Width" />
            <parameter elementQuery=".Length" />
            <parameter elementQuery=".Rotation" />
          </parameters>
        </action>
      </postSetActions>
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType" />
            <parameter value="Length must be a distance." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Width" displayName="Width" value="500 feet" deleteIfEditZoi="true" typeName="Depiction.API.ValueObject.Distance, Depiction.API">
      <postSetActions>
        <action name="CreateRectangle">
          <parameters>
            <parameter elementQuery=".Width" />
            <parameter elementQuery=".Length" />
            <parameter elementQuery=".Rotation" />
          </parameters>
        </action>
      </postSetActions>
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType" />
            <parameter value="Width must be a distance." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 feet" type="Depiction.API.ValueObject.Distance, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Height" displayName="Height" value="-20 feet" typeName="Depiction.API.ValueObject.Distance, Depiction.API">
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType" />
            <parameter value="Height must be a distance." type="System.String" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Rotation" displayName="Orientation in degrees" value="0" deleteIfEditZoi="true" typeName="System.Double">
      <postSetActions>
        <action name="CreateRectangle">
          <parameters>
            <parameter elementQuery=".Width" />
            <parameter elementQuery=".Length" />
            <parameter elementQuery=".Rotation" />
          </parameters>
        </action>
      </postSetActions>
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Double" type="System.RuntimeType" />
            <parameter value="Rotation must be a number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.RangeValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Double" />
            <parameter value="360" type="System.Double" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="Notes" displayName="Additional notes" typeName="System.String" value="" />
    <property name="ZOIBorder" displayName="Outline color" value="#CC3300" typeName="Color" deletable="false" />
    <property name="ZOIFill" displayName="Fill color" value="#000000" typeName="Color" deletable="false" />
    <property name="Draggable" displayName="Allow dragging" value="True" typeName="Boolean" deletable="false">
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Boolean" type="System.RuntimeType" />
            <parameter value="Allow Draggable must be a boolean." type="System.String" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="ShowElementPropertiesOnCreate" value="True" typeName="Boolean" editable="False" visible="False" />
    <property name="IconPath" displayName="Icon path" value="embeddedresource:ARG3X.ChangeGroundHeight" typeName="String" deletable="False" />
    <property name="PlaceableWithMouse" value="true" editable="false" typeName="Boolean" visible="false" />
    <property name="CanEditZoi" displayName="Editable ZOI" value="True" typeName="Boolean" editable="True" visible="True" deletable="false" />
    <property name="ShowIcon" displayName="Show icon" value="true" typeName="Boolean" visible="true" deletable="false" editable="True" />
  </properties>
  <generateZoi>
    <actions>
      <action name="CreateRectangle">
        <parameters>
          <parameter elementQuery=".Width" />
          <parameter elementQuery=".Length" />
          <parameter elementQuery=".Rotation" />
        </parameters>
      </action>
    </actions>
  </generateZoi>
</DepictionElement>