﻿using System.Collections.Specialized;
using Depiction.API.AbstractObjects;
using System.IO;
using System;

namespace Resources.Product.OsInt
{
    //    OSINT-ENVIRONMENT Military/LE Edition
    public class OsIntEnvironmentInformation : ProductInformationBase
    {
        public override string ProductType { get { return OsintInformation; } }

        public override string LicenseFileName { get { return "licensev14.lic"; } }
        public override string SupportEmail { get { return "support@osint-environment.com"; } }
        public override string ProductWebpage { get { return "http://www.Osint-Environment.com"; } }
        public override string StoryName { get { return "DatumPointNode"; } }

        public override string DirectoryNameForCompany { get { return "ARG3X_International"; } }
        public override string CompanyName { get { return "ARG3X International"; } }

        public override string LicenseFileDirectory
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), DirectoryNameForCompany);
            }
        }
        public override string FileExtension { get { return ".dpn"; } }
        public override string DepictionCopyright { get { return "© 2017 ARG3X International"; } }


        public override string ProductName
        {
            get { return "C-UAS Sandbox"; }
        }
        public override string ProductNameInternalUsage { get { return "C_UAS_Sandbox"; } }
        public override string ResourceAssemblyName
        {
            get { return "Resources.Product.OsInt"; }
        }

        public override string SplashScreenPath
        {
            get { return "Images/SplashScreen.png"; }
        }
        //        public override string AppIconResourceName { get { return "Images/DepictionRealWorld.ico"; } }
        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.",
                                     ProductVersion, DepictionCopyright);
            }
        }

        override public StringCollection ProductQuickAddElements
        {
            get
            {
                return new StringCollection { "UserDrawnLine", "UserDrawnPolygon" };
            }
        }
        public override string EulaFileAssemblyLocation
        {
            get { return "Docs.OsintInformation_EULA.xps"; }
        }
    }
}
