﻿namespace Depiction.Web.Helpers
{
    public class SubmitButton : IButton
    {
        private readonly string name;
        private readonly string buttonText;

        public SubmitButton(string name, string buttonText)
        {
            this.name = name;
            this.buttonText = buttonText;
        }

        public SubmitButton(string buttonText) : this(buttonText, buttonText)
        {
            
        }

        #region IButton Members

        public string Button
        {
            get { return string.Format("<button type=\"submit\" name=\"{0}\" value=\"{0}\"><span>{1}</span></button>", name, buttonText); }
        }

        #endregion
    }
}