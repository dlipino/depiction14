﻿using System.Web.Mvc;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Helpers
{
    public class TextHtmlControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly bool isPassword;
        private readonly MaskInformation mask;
        private readonly ValidationInformation validation;
        private readonly string value;

        public TextHtmlControl(string controlName) : this(controlName, string.Empty)
        {
        }

        public TextHtmlControl(string controlName, string value) : this(controlName, value, null, null, false)
        {
        }

        public TextHtmlControl(string controlName, string value, MaskInformation mask) : this(controlName, value, mask, null, false)
        {
        }

        public TextHtmlControl(string controlName, string value, ValidationInformation validation) : this(controlName, value, null, validation, false)
        {
        }

        public TextHtmlControl(string controlName, string value, ValidationInformation validation, bool isPassword) : this(controlName, value, null, validation, isPassword)
        {
        }

        public TextHtmlControl(string controlName, string value, MaskInformation mask, ValidationInformation validation, bool isPassword)
        {
            this.controlName = controlName;
            this.value = value;
            this.mask = mask;
            this.validation = validation;
            this.isPassword = isPassword;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                string cssClass = string.Empty;
                string alt = string.Empty;

                if (validation != null)
                    cssClass += string.Format("fValidate[{0}]", validation.BuildValidationString());

                if (mask != null)
                {
                    cssClass += " iMask";
                    alt = string.Format("{{ type: 'fixed', mask: '{0}', stripMask: {1} }}", mask.Mask, mask.StripMask ? "true" : "false");
                }

                var tagBuilder = new TagBuilder("input");

                if (isPassword)
                    tagBuilder.Attributes.Add("type", "password");
                else
                    tagBuilder.Attributes.Add("type", "text");

                tagBuilder.Attributes.Add("id", controlName);
                tagBuilder.Attributes.Add("name", controlName);

                if (!string.IsNullOrEmpty(value))
                    tagBuilder.Attributes.Add("value", value);

                if (!string.IsNullOrEmpty(cssClass))
                    tagBuilder.Attributes.Add("class", cssClass.Trim());

                if (!string.IsNullOrEmpty(alt))
                    tagBuilder.Attributes.Add("alt", alt);

                return tagBuilder.ToString(TagRenderMode.SelfClosing);
            }
        }

        #endregion
    }
}