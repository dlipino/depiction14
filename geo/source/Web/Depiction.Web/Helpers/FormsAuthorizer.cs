﻿using System.Web.Security;
using Depiction.Web.Helpers.Interfaces;

namespace Depiction.Web.Helpers
{
    public class FormsAuthorizer : IAuthorizer
    {
        #region IAuthorizer Members

        public void DoAuthorize(string user, bool authorized)
        {
            FormsAuthentication.SetAuthCookie(user, authorized);
        }

        #endregion
    }
}