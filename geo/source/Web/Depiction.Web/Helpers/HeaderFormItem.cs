﻿namespace Depiction.Web.Helpers
{
    public class HeaderFormItem : IFormItem
    {
        private readonly string headerText;

        public HeaderFormItem(string headerText)
        {
            this.headerText = headerText;
        }

        #region IFormItem Members

        public string FormItem
        {
            get { return string.Format("<h3 class=\"HeaderFormItem\">{0}</h3>", headerText); }
        }

        #endregion
    }
}