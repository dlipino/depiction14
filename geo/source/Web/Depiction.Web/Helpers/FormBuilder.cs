﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Depiction.Web.Helpers
{
    public static class FormBuilder
    {
        public static string BuildForm<T>(Expression<Action<T>> action, string formName, IFormItem[] formItems) where T : Controller
        {
            return BuildForm(action, formName, formItems, EncType.ApplicationxWWWFormUrlEncoded);
        }

        public static string BuildForm<T>(Expression<Action<T>> action, string formName, IFormItem[] formItems, EncType encType) where T : Controller
        {
            var tagBuilder = new TagBuilder("form");
            var actionLink = ExpressionHelper.GetActionLink(action);
            var timeStamp = DateTime.Now;

            tagBuilder.Attributes.Add("name", formName);
            tagBuilder.Attributes.Add("id", formName);
            tagBuilder.Attributes.Add("action", actionLink);
            tagBuilder.Attributes.Add("method", "post");

            if(encType == EncType.Multipartformdata)
                tagBuilder.Attributes.Add("enctype", "multipart/form-data");

            tagBuilder.InnerHtml = string.Format("\r\n<input id=\"PostingTo\" name=\"PostingTo\" type=\"hidden\" value=\"{0}\" /><input id=\"TimeStamp\" name=\"TimeStamp\" type=\"hidden\" value=\"{1}\" /><input id=\"FormKey\" name=\"FormKey\" type=\"hidden\" value=\"{2}\" />", actionLink, timeStamp, actionLink.ComputeHash(timeStamp));
            tagBuilder.InnerHtml += BuildForm(formItems);

            return tagBuilder.ToString();
        }

        private static string BuildForm(IFormItem[] formItems)
        {
            string form = string.Empty;

            foreach(var formItem in formItems)
                if(formItem != null)
                    form += string.Format("{0}\r\n", formItem.FormItem);

            return string.Format("\r\n<div class=\"DataEntryArea\">\r\n{0}</div>\r\n", form);
        }
    }
}