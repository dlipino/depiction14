﻿using System.Collections.Generic;
using System.Text;

namespace Depiction.Web.Helpers
{
    public class AreaHtmlControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly string value;
        private readonly string selectedUnit;

        private readonly IList<string> units = new List<string>
                                                   {
                                                           "square feet",
                                                           "square meters",
                                                           "square inches",
                                                           "square centimeters",
                                                   };

        public AreaHtmlControl(string controlName, string value, string selectedUnit)
        {
            this.controlName = controlName;
            this.value = value;
            this.selectedUnit = selectedUnit;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                //"<input value=\"{0}\" id=\"{1}\" name=\"{1}\" style=\"width: 100px;\" class=\"fValidate['required','real']\" />&nbsp;"
                var builder =
                        new StringBuilder(
                                string.Format(
                                        "<input value=\"{0}\" id=\"{1}\" name=\"{1}\" style=\"width: 100px;\" />&nbsp;"
                                        + "<select id=\"{1}::Unit\" name=\"{1}::Unit\">",
                                        value,
                                        controlName));
                foreach (var u in units)
                {
                    builder.Append(string.Format(
                            "<option value=\"{0}\" label=\"{0}\"{1}>{0}</option>", u, u == selectedUnit ? " selected" : string.Empty));
                }
                builder.Append("</select>");

                return builder.ToString();
            }
        }

        #endregion
    }
}