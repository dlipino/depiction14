﻿namespace Depiction.Web.Helpers.Validation
{
    public class MatchesValidation : IValidation
    {
        private readonly string matchesFieldName;

        public MatchesValidation(string matchesFieldName)
        {
            this.matchesFieldName = matchesFieldName;
        }

        #region IValidation Members

        public string ValidationText
        {
            get { return string.Format("={0}", matchesFieldName); }
        }

        #endregion
    }
}