﻿namespace Depiction.Web.Helpers.Validation
{
    public class RequiredValidation : IValidation
    {
        #region IValidation Members

        public string ValidationText
        {
            get { return "required"; }
        }

        #endregion
    }
}