﻿namespace Depiction.Web.Helpers.Validation
{
    public class AlphaNumericValidation : IValidation
    {
        #region IValidation Members

        public string ValidationText
        {
            get { return "alphanum"; }
        }

        #endregion
    }
}