﻿namespace Depiction.Web.Helpers.Validation
{
    public class EmailValidation : IValidation
    {
        #region IValidation Members

        public string ValidationText
        {
            get { return "email"; }
        }

        #endregion
    }
}