﻿using System.Web.Mvc;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Helpers
{
    public class DropDownListHtmlControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly SelectOption[] options;
        private readonly ValidationInformation validation;

        public DropDownListHtmlControl(string controlName, SelectOption[] options) : this(controlName, options, null)
        {
        }

        public DropDownListHtmlControl(string controlName, SelectOption[] options, ValidationInformation validation)
        {
            this.controlName = controlName;
            this.options = options;
            this.validation = validation;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                string cssClass = string.Empty;

                if (validation != null)
                    cssClass += string.Format("fValidate[{0}]", validation.BuildValidationString());

                var tagBuilder = new TagBuilder("select");
                tagBuilder.Attributes.Add("id", controlName);
                tagBuilder.Attributes.Add("name", controlName);

                if (!string.IsNullOrEmpty(cssClass))
                    tagBuilder.Attributes.Add("class", cssClass.Trim());

                tagBuilder.InnerHtml = BuildOptions();

                return tagBuilder.ToString();
            }
        }

        #endregion

        private string BuildOptions()
        {
            if (options == null) return string.Empty;
            string returnValue = string.Empty;

            foreach (SelectOption option in options)
            {
                if (option.Value == null)
                    option.Value = option.Label;

                var tagBuilder = new TagBuilder("option");

                if (option.Disabled)
                    tagBuilder.Attributes.Add("disabled", "disabled");

                if (option.Selected)
                    tagBuilder.Attributes.Add("selected", "selected");

                tagBuilder.Attributes.Add("value", option.Value);
                tagBuilder.Attributes.Add("label", option.Label);
                tagBuilder.InnerHtml = option.Label;

                returnValue += tagBuilder.ToString();
            }

            return returnValue;
        }
    }
}