﻿namespace Depiction.Web.Helpers
{
    public class SpeedHTMLControl : IHtmlControl
    {
        private readonly string controlName;
        private readonly string value;
        private readonly string unit;

        public SpeedHTMLControl(string controlName, string value, string unit)
        {
            this.controlName = controlName;
            this.value = value;
            this.unit = unit;
        }

        #region IHtmlControl Members

        public string HtmlControl
        {
            get
            {
                return string.Format(
                    "<input value=\"{0}\" id=\"{1}\" name=\"{1}\" style=\"width: 100px;\" class=\"fValidate['required','real']\" />&nbsp;" +
                    "<select id=\"{1}::Unit\" name=\"{1}::Unit\">" +
                    "<option value=\"mph\" label=\"mph\"" + (unit == "mph" ? " selected" : string.Empty) + "\">mph</option>" +
                    "<option value=\"kph\" label=\"kph\"" + (unit == "kph" ? " selected" : string.Empty) + "\">kph</option>" +
                    "<option value=\"ft/s\" label=\"ft/s\"" + (unit == "ft/s" ? " selected" : string.Empty) + "\">ft/s</option>" +
                    "<option value=\"m/s\" label=\"m/s\"" + (unit == "m/s" ? " selected" : string.Empty) + "\">m/s</option>" +
                    "</select>", value, controlName);
            }
        }

        #endregion
    }
}