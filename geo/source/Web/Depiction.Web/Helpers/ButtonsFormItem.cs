﻿using System.Web.Mvc;

namespace Depiction.Web.Helpers
{
    public class ButtonsFormItem : IFormItem
    {
        private readonly IButton[] buttons;

        public ButtonsFormItem(IButton[] buttons)
        {
            this.buttons = buttons;
        }

        #region IFormItem Members

        public string FormItem
        {
            get
            {
                var tagBuilder = new TagBuilder("p");
                tagBuilder.Attributes.Add("class", "ButtonsFormItem");
                tagBuilder.InnerHtml = buildButtons(buttons);
                return tagBuilder.ToString();
            }
        }

        #endregion

        private static string buildButtons(IButton[] buttons)
        {
            string returnValue = string.Empty;

            foreach (IButton button in buttons)
                if (button != null)
                    returnValue += button.Button;

            return returnValue;
        }
    }
}