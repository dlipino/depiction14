﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace Depiction.Web.Helpers
{
    public static class ExtensionMethods
    {
        public static string ClickHereButton(this HtmlHelper helper, string href, string buttonText)
        {
            return string.Format("<a href=\"{0}\" class=\"clickHereButton\"><span class=\"action\">{1}</span><span class=\"callOut\">CLICK HERE</span></a>", href, buttonText);
        }

        public static void Validate(this NameValueCollection form)
        {
            if(form["FormKey"] != ComputeHash(form["PostingTo"], DateTime.Parse(form["TimeStamp"])))
                throw new Exceptions.FormTamperingException();
        }

        public static string ComputeHash(this string key, DateTime timeStamp)
        {
            if (timeStamp.Second == 0)
                timeStamp.AddSeconds(1);
            int offSet = Convert.ToInt32(((double)timeStamp.Day/timeStamp.Month/timeStamp.Second) * 100);

            var salt = Encoding.UTF8.GetBytes(secretSalt).ToList();

            while (offSet > salt.Count)
                offSet -= 13;

            salt.InsertRange(offSet, Encoding.UTF8.GetBytes(key));

            var hash = new SHA512Managed();
            return Convert.ToBase64String(hash.ComputeHash(salt.ToArray()));
        }

        private const string secretSalt = "fjk34()4832)_0nmfjkl290$()*%(#KJKJ45KFLSKDJ8*FEdfdsbf";
    }
}