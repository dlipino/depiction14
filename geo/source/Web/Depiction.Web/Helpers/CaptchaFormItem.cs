﻿namespace Depiction.Web.Helpers
{
    public class CaptchaFormItem : IFormItem
    {
        // This is the key for Depiction.com ...
        //private const string PublicKey = "6Ler1gYAAAAAAKHKf1Kw9GqgkOcitqwOLMiz9CNm";

        // Use this key for local testing
        private const string PublicKey = "6Lfe1gYAAAAAAPCx_m8wOqp2saxZUynDiMx14LKY";

        #region IFormItem Members

        public string FormItem
        {
            get { return
                "<script type=\"text/javascript\" src=\"http://api.recaptcha.net/challenge?k=" + PublicKey + "\"></script>" +
                "<noscript>" +
                "  <iframe src=\"http://api.recaptcha.net/noscript?k=" + PublicKey + "\"" + 
                "      height=\"300\" width=\"500\" frameborder=\"0\"></iframe><br>" + 
                "  <textarea name=\"recaptcha_challenge_field\" rows=\"3\" cols=\"40\">" + 
                "  </textarea>" + "  <input type=\"hidden\" name=\"recaptcha_response_field\"" + 
                "      value=\"manual_challenge\">" + "</noscript>"; }
        }

        #endregion
    }
}