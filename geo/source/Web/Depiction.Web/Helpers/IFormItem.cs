﻿namespace Depiction.Web.Helpers
{
    public interface IFormItem
    {
        string FormItem { get; }
    }
}