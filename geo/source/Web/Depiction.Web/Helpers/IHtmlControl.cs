﻿namespace Depiction.Web.Helpers
{
    public interface IHtmlControl
    {
        string HtmlControl { get; }
    }
}