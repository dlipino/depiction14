﻿using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Depiction.Web.Helpers
{
    public static class ExcelExport
    {
        public static void Export(HttpResponse response, object model, string fileName)
        {
            string dg;
            //clean up the response.object
            response.Clear();
            response.Charset = "";
            //set the response mime type for excel
            response.ContentType = "application/vnd.ms-excel";
            response.AddHeader("content-disposition", "attachment; filename=" + fileName);

            var test = new DataGrid {
                DataSource = model
            };

            test.GridLines = GridLines.None;
            test.HeaderStyle.Font.Bold = true;

            using (var writer = new StringWriter())
            {
                test.DataBind();

                var htmlTextWriter = new HtmlTextWriter(writer);
                test.RenderControl(htmlTextWriter);
                dg = writer.ToString();
            }

            response.Write(dg);
            response.End();
        }
    }
}