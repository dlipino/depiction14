using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using NHibernate;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class RegionsController : Controller
    {
        private readonly IRepository repository;

        public RegionsController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.GetAll<RegionDefinition>());
        }

        public void Save(int id)
        {
            RegionDefinition region = repository.GetOne<RegionDefinition>(id) ?? new RegionDefinition();

            region.RegionCode = Request.Form["RegionCode"];
            region.RegionName = Request.Form["RegionName"];

            using (ITransaction transaction = repository.BeginTransaction())
            {
                repository.Save(region);
                transaction.Commit();
            }

            Response.Redirect("~/Regions");
        }

        public ActionResult Edit(int id)
        {
            return View(repository.GetOne<RegionDefinition>(id));
        }

        public ActionResult New()
        {
            return View("Edit", new RegionDefinition());
        }
    }
}