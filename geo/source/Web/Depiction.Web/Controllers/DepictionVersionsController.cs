using System;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;

namespace Depiction.Web.Controllers
{
    [Authorize(Roles = "admin")]
    public class DepictionVersionsController : Controller
    {
        private readonly IRepository repository;

        public DepictionVersionsController(IRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return View(repository.GetAll<DepictionVersion>());
        }

        public ActionResult New()
        {
            return View("Edit", new DepictionVersion());
        }

        public ActionResult Edit(int id)
        {
            return View("Edit", repository.GetOne<DepictionVersion>(id));
        }

        public void Save(int id)
        {
            using(var transaction = repository.BeginTransaction())
            {
                var versionToSave = repository.GetOne<DepictionVersion>(id) ?? new DepictionVersion();

                versionToSave.FriendlyName = Request.Form["FriendlyName"];
                versionToSave.VersionNumber = Request.Form["VersionNumber"];

                repository.Save(versionToSave);

                transaction.Commit();
            }

            Response.Redirect("~/DepictionVersions");
        }

        public void Delete(int id)
        {
            using (var transaction = repository.BeginTransaction())
            {
                var version = repository.GetOne<DepictionVersion>(id) ?? new DepictionVersion();
                repository.Delete(version);

                transaction.Commit();
            }

            Response.Redirect("~/DepictionVersions");
        }
    }
}