﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.ExtensionMethods;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Interfaces;
using Depiction.Web.Views.User;

namespace Depiction.Web.Controllers
{
    [HandleError(ExceptionType = typeof (UserControllerException))]
    public class UserController : Controller
    {
        private readonly IRepository repository;
        private readonly IAuthorizer authorizer;

        public UserController(IRepository repository, IAuthorizer authorizer)
        {
            this.repository = repository;
            this.authorizer = authorizer;
        }

        public ActionResult Index()
        {
            return View("Login");
        }

        public ActionResult Edit(int id)
        {
            return View(new EditUserData {
                User = repository.GetOne<User>(id),
                AllRoles = repository.GetAll<Role>()
            });
        }

        public ActionResult Login()
        {
            return View();
        }

        public void DoLogin()
        {
            User[] user = repository.Search<User>(u => u.Person.Email == Request.Form["Email"] && u.Password == Request.Form["Password"].PasswordEncode());

            if (user.Length != 1) throw new UserControllerException("No account exists for that email and password combination.");

            authorizer.DoAuthorize(user[0].Id.ToString(), false);

            Response.Redirect("~/Home/Dashboard");
        }

        public ActionResult Register()
        {
            return View();
        }

        public void SaveUser(int id)
        {
            Request.Form.Validate();

            if (Request.Form["Password"] != Request.Form["ConfirmPassword"])
                throw new UserControllerException("The passwords to not match!");

            var userToSave = repository.GetOne<User>(id);

            bool registering = userToSave == null;

            var userWithEmailAddress = repository.Search<User>(u => u.Person.Email == Request.Form["Email"]);

            if (userWithEmailAddress.Length > 0 && ((!registering && userToSave.Id != userWithEmailAddress[0].Id) || registering))
                throw new UserControllerException("An account already exists for that email address! Please reset your password and log in.");

            if (registering)
            {
                if (Request.Form["Password"] == string.Empty)
                    throw new UserControllerException("You must specify a password to create an account.");

                userToSave = new User {
                    Person = new Person(),
                    Roles = new List<Role> { repository.Queryable<Role>().Single(r => r.RoleName == "livereports") }
                };
            }

            UpdateModel(userToSave.Person, new[] {"FirstName", "LastName", "Email"});

            if (Request.Form["Password"] != string.Empty)
                userToSave.Password = Request.Form["Password"].PasswordEncode();

            if (!registering && Request.Form["Role"] != null)
            {
                userToSave.Roles = new List<Role>();

                string[] roles = Request.Form["Role"].Split(',');

                foreach (string role in roles)
                    userToSave.Roles.Add(repository.GetOne<Role>(Convert.ToInt32(role)));
            }

            using (var transaction = repository.BeginTransaction())
            {
                repository.Save(userToSave);
                transaction.Commit();
            }

            if (registering)
                Response.Redirect("~/User/Registered");
            else
                Response.Redirect("~/User/Saved");
        }

        public ViewResult Registered()
        {
            return View();
        }

        public ViewResult Saved()
        {
            return View();
        }
    }

    public class UserControllerException : Exception
    {
        public UserControllerException(string errorMessage) : base(errorMessage) {}
    }
}