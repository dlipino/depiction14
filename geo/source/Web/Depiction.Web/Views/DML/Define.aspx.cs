﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;

namespace Depiction.Web.Views.DML
{
    public class Define : ViewPage
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem("Upload a DML File: "),
                new DataFormItem("To", new FileUploadHTMLControl("DMLFile")),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Add DML Definition")})
            };

            form = FormBuilder.BuildForm<DMLController>(d => d.SubmitDML(), "SubmitDML", formItems.ToArray(), EncType.Multipartformdata);

            base.OnLoad(e);
        }
    }
}