﻿using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.Depictions
{
    public class Show : ViewPage<WebDepiction>
    {
        protected string webDepictionLocation;
        protected string form { get; private set; }

        protected override void OnLoad(System.EventArgs e)
        {
            webDepictionLocation = Request.Url.Authority.IndexOf("localhost") == 0 ? "http://request.depiction.com/WebDepictions/" : "http://request.depiction.com/WebDepictions/";
            if (User.Identity.IsAuthenticated)
            {
                IFormItem[] formItems = new IFormItem[]
                                {
                                    new HeaderFormItem("Comment on this depiction"),
                                    new DataFormItem(" ",
                                                     new TextHtmlControl("Post Comment", string.Empty,
                                                                         new ValidationInformation
                                                                             {
                                                                                 Validations =
                                                                                     new IValidation[]
                                                                                         {new RequiredValidation()}
                                                                             })),
                                    new ButtonsFormItem(new IButton[] {new SubmitButton("Post Comment")})
                                };

                form = FormBuilder.BuildForm<DepictionsController>(u => u.DoComment(Model.Id), "LoginForm", formItems);
            }
            else
            {
                IFormItem[] formItems = new IFormItem[]
                                {
                                    new HeaderFormItem("Login to post a comment"),
                                    //new DataFormItem(" ",
                                    //                 new TextHtmlControl("Post Comment", string.Empty,
                                    //                                     new ValidationInformation
                                    //                                         {
                                    //                                             Validations =
                                    //                                                 new IValidation[]
                                    //                                                     {new RequiredValidation()}
                                    //                                         })),
                                    new ButtonsFormItem(new IButton[] {new SubmitButton("Login")})
                                };
                form = FormBuilder.BuildForm<UserController>(u => u.Login(), "LoginForm", formItems);
            }
            base.OnLoad(e);
        }

    }
}