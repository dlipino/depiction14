﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="GeneratedCodes.aspx.cs" Inherits="Depiction.Web.Views.Admin.GeneratedCodes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Generated codes</h1>
<h2>The following trial activation codes have been generated:</h2>
<%
    foreach (var activationCode in Model)
    {
        %>
<h3><%= activationCode %></h3>
<%
    } %>
</asp:Content>