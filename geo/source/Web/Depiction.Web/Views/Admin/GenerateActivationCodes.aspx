﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="GenerateActivationCodes.aspx.cs" Inherits="Depiction.Web.Views.Admin.GenerateActivationCodes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="../../Content/mootools-1.1.js" type="text/javascript"></script>
<script src="../../Content/iMask-full.js" type="text/javascript"></script>
<script src="../../Content/fValidator-full.js" type="text/javascript"></script>
<script type="text/javascript">

window.addEvent("domready", function() {
    var exValidatorA = new fValidator("generateForm");
});

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Generate activation codes</h1>
    <%= form %>
</asp:Content>