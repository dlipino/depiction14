﻿using System;
using System.Web.Mvc;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.Admin
{
    public class IndicatePurchases : ViewPage
    {
        protected string form;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new IFormItem[] {
                new HeaderFormItem("Indicate which trial users have purchased Depiction"),
                new DataFormItem("Comma-delimited Email Addresses", new MultiLineTextHtmlControl("emailAddresses", string.Empty, new ValidationInformation {
                Validations = new IValidation[] {new NumericValidation(), new RequiredValidation()}
            })), new ButtonsFormItem(new[] {new SubmitButton("Save")})};

            form = FormBuilder.BuildForm<AdminController>(a => a.PurchasesIndicated(), "purchasersForm", formItems);

            base.OnLoad(e);
        }
    }
}