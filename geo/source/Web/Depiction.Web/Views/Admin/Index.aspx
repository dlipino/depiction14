﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.Admin.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table>
    <thead>
        <td>General</td>
        <td>Quickstart Data</td>
    </thead>
    <tr>
        <td><%= (new LinkButtonHtmlControl<DBController>(d => d.UpdateDB(), "Update Database")).Button %></td>
        <td><%= (new LinkButtonHtmlControl<AddinController>(a => a.Index(), "Manage Addins")).Button %></td>
    </tr>
    <tr>
        <td><%= (new LinkButtonHtmlControl<AdminController>(a => a.Users(), "Manage Users")).Button %></td>
        <td><%= (new LinkButtonHtmlControl<DepictionVersionsController>(d => d.Index(), "Manage Depiction Versions")).Button %></td>
    </tr>
    <tr>
        <td><%= (new LinkButtonHtmlControl<AdminController>(a => a.MarketSegments(), "Market Segments")).Button %></td>
        <td><%= (new LinkButtonHtmlControl<RegionsController>(r => r.Index(), "Manage Quickstart Regions")).Button %></td>
    </tr>
    <tr>
        <td><%= (new LinkButtonHtmlControl<AdminController>(a => a.GenerateActivationCodes(), "Generate Activation Codes")).Button %></td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><%= (new LinkButtonHtmlControl<AdminController>(a => a.IndicatePurchases(), "Indicate Purchases")).Button %></td>
        <td>&nbsp;</td>
    </tr>
</table>
</asp:Content>