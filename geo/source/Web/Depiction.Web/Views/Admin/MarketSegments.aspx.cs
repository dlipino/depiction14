﻿using System.Web.Mvc;

namespace Depiction.Web.Views.Admin
{
    public class MarketSegments : ViewPage<MarketSegmentInfo[]> { }

    public class MarketSegmentInfo
    {
        public int Id { get; set; }
        public string MarketSegment { get; set; }
        public int UsageCount { get; set; }
        public int Status { get; set; }
    }
}