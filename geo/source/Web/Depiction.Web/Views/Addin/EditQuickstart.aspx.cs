﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;

namespace Depiction.Web.Views.Addin
{
    public class EditQuickstart : ViewPage<QuickstartData>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var parameter in Model.Parameters)
            {
                stringBuilder.Append(string.Format("[{0},{1}]", parameter.ParameterName, parameter.ParameterValue));
            }
            var formItems = new List<IFormItem> {
                new HeaderFormItem("Editing Quickstart source"),
                new DataFormItem("Quickstart name", new TextHtmlControl("QuickstartName", Model.QuickstartName)),
                new DataFormItem("Description", new MultiLineTextHtmlControl("Description", Model.Description)),
                //new DataFormItem("URL", new TextHtmlControl("URL", Model.URL)),
                new DataFormItem("Parameters<br/>[name,value][name,value]...", new MultiLineTextHtmlControl("Parameters", stringBuilder.ToString())),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Edit Quickstart source")})
            };

            form = FormBuilder.BuildForm<AddinController>(a => a.SaveQuickstartEdit(Model.Id), "EditQuickstartForm", formItems.ToArray());
            base.OnLoad(e);
        }
    }
}
