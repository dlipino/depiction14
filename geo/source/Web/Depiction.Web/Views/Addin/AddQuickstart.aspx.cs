﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;

namespace Depiction.Web.Views.Addin
{
    public class AddQuickstart : ViewPage<AddinInformation>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem("Add a quickstart source for " + Model.FriendlyName),
                new DataFormItem("Quickstart name", new TextHtmlControl("QuickstartName")),
                new DataFormItem("Description", new MultiLineTextHtmlControl("Description")),
                new DataFormItem("URL", new TextHtmlControl("URL")),
                new DataFormItem("Parameters<br/>[name,value][name,value]...", new MultiLineTextHtmlControl("Parameters")),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Add Association")})
            };

            form = FormBuilder.BuildForm<AddinController>(a => a.SaveQuickstartAddition(Model.Id), "AddQuickstartForm", formItems.ToArray());
            base.OnLoad(e);
        }
    }
}