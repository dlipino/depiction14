﻿using System.Collections.Generic;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.DepictionVersions
{
    public class Edit : ViewPage<DepictionVersion>
    {
        protected string form = string.Empty;

        protected override void OnLoad(System.EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem(Model.Id > 0 ? "Edit version mapping: " : "Create a version mapping:"),
                new DataFormItem("Friendly name", new TextHtmlControl("FriendlyName", Model.FriendlyName, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DataFormItem("Hard-coded version", new TextHtmlControl("VersionNumber", Model.VersionNumber, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation(), new NumericValidation()}
                })),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Save")})
            };

            form = FormBuilder.BuildForm<DepictionVersionsController>(d => d.Save(Model.Id), "EditVersionForm", formItems.ToArray());
            base.OnLoad(e);
        }
    }
}