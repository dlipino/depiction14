﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Depiction.Web.Views.Home.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveClientUrl("~/Content/piechart.css")%>" rel="stylesheet" type="text/css" />
    <script src="<%= ResolveClientUrl("~/Content/mootools-beta-1.2b2.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Content/moocanvas.js")%>" type="text/javascript"></script>
    <script src="<%= ResolveClientUrl("~/Content/piechart.js")%>" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Dashboard</h1>
    <h2>Trial Registrations</h2>
    <table class="pieChart">
        <tr>
            <th>Registrations</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Not Activated</td>
            <td><%= Model.NotActivatedUserCount %></td>
        </tr>
        <tr>
            <td>Activated</td>
            <td><%= Model.ActivatedUserCount %></td>
        </tr>
    </table>
    <h2>Generated Keys for Conferences</h2>
    <table class="pieChart">
        <tr>
            <th>Generated Keys</th>
            <th>Value</th>
        </tr>
        <tr>
            <td>Not Activated</td>
            <td><%= Model.GeneratedKeysNotActivated %></td>
        </tr>
        <tr>
            <td>Activated</td>
            <td><%= Model.GeneratedKeysActivated %></td>
        </tr>
    </table>
    <table>
        <tr>
            <th>By the numbers</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td>Trial registrations</td>
            <td><%= Model.TrialRegistrations %></td>
        </tr>
        <tr>
            <td>Generated keys</td>
            <td><%= Model.GeneratedKeyCount %></td>
        </tr>
        <tr>
            <td>Currently active trials</td>
            <td><%= Model.CurrentlyActiveTrials %></td>
        </tr>
        <tr>
            <td>Average time between sign up and activation</td>
            <td><%= Model.AverageTimeBetweenSignUpAndActivation %></td>
        </tr>
    </table>
</asp:Content>