﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.Regions.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Regions Depiction knows about</h1>
    <%
        foreach (var regionDefinition in Model)
        {
            %>
        <p><%= regionDefinition.RegionName + " " + regionDefinition.RegionCode %></p>
            <%
        } %>
    <%= (new LinkButtonHtmlControl<RegionsController>(r => r.New(), "Create a new region definition")).Button %>
</asp:Content>