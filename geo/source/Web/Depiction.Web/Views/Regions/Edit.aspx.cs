﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.Regions
{
    public class Edit : ViewPage<RegionDefinition>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem(Model.Id > 0 ? "Edit region definition: " : "Create a new region definition: "),
                new DataFormItem("Region code", new TextHtmlControl("RegionCode", Model.RegionCode, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DataFormItem("Region name", new TextHtmlControl("RegionName", Model.RegionName, new ValidationInformation {
                    Validations = new[] {new RequiredValidation()}
                })),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Save")})
            };

            form = FormBuilder.BuildForm<RegionsController>(r => r.Save(Model.Id), "EditRegionDefinition", formItems.ToArray());
            base.OnLoad(e);
        }
    }
}