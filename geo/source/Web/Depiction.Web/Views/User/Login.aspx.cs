﻿using System.Web.Mvc;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.User
{
    public class Login : ViewPage
    {
        protected string form { get; private set; }

        protected override void OnLoad(System.EventArgs e)
        {
            var formItems = new IFormItem[]
                {
                    new HeaderFormItem("Log In to the Trial Site"),
                    new DataFormItem("Email", new TextHtmlControl("Email", string.Empty, new ValidationInformation { Validations = new IValidation[] {new EmailValidation(), new RequiredValidation()}})),
                    new DataFormItem("Password", new TextHtmlControl("Password", string.Empty, new ValidationInformation { Validations = new [] {new RequiredValidation()}}, true)),
                    new ButtonsFormItem(new IButton[] { new SubmitButton("Log In"), new LinkButtonHtmlControl<UserController>(u => u.Register(), "Register")})
                };

            form = FormBuilder.BuildForm<UserController>(u => u.DoLogin(), "LoginForm", formItems);
            base.OnLoad(e);
        }
    }
}