﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.User
{
    public class Edit : ViewPage<EditUserData>
    {
        protected string form { get; private set; }

        protected override void OnLoad(System.EventArgs e)
        {
            var formItems = new List<IFormItem>
                {
                    new HeaderFormItem("Edit user: " + Model.User.Person.Email), 
                    new DataFormItem("Email", new TextHtmlControl("Email", Model.User.Person.Email, new ValidationInformation { Validations = new IValidation[] {new EmailValidation(), new RequiredValidation()} })), 
                    new DataFormItem("First Name", new TextHtmlControl("FirstName", Model.User.Person.FirstName, new ValidationInformation { Validations = new[] {new RequiredValidation()} })), 
                    new DataFormItem("Last Name", new TextHtmlControl("LastName", Model.User.Person.LastName, new ValidationInformation { Validations = new[] {new RequiredValidation()} })), 
                    new DataFormItem("Password", new TextHtmlControl("Password", string.Empty, null, true)),
                    new DataFormItem("Confirm Password", new TextHtmlControl("ConfirmPassword", string.Empty, new ValidationInformation { Validations = new IValidation[] {new MatchesValidation("Password")} }, true)),
                    new ButtonsFormItem(new IButton[] {new SubmitButton("Save changes")})
                };

            if(Context.User.IsInRole("admin"))
            {
                string displayText = "Roles";

                foreach (var role in Model.AllRoles)
                {
                    formItems.Insert(formItems.Count - 1, new DataFormItem(displayText, new CheckBoxHtmlControl("Role", role.Id.ToString(), role.RoleName, Model.User.Roles.Any(r => r.RoleName == role.RoleName))));
                    displayText = "&nbsp;";
                }
            }

            form = FormBuilder.BuildForm<UserController>(u => u.SaveUser(Model.User.Id), "EditUserForm", formItems.ToArray());
            base.OnLoad(e);
        }
    }

    public class EditUserData
    {
        public Marketing.DomainModel.Model.User User { get; set; }
        public Role[] AllRoles { get; set; }
    }
}