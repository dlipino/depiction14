﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Depiction.Web.Views.User.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="<%= ResolveClientUrl("~/Content/mootools-1.1.js")%>" type="text/javascript"></script>
<script src="<%= ResolveClientUrl("~/Content/iMask-full.js")%>" type="text/javascript"></script>
<script src="<%= ResolveClientUrl("~/Content/fValidator-full.js")%>" type="text/javascript"></script>
<script type="text/javascript">

    window.addEvent("domready", function() {
        var exValidatorA = new fValidator("LoginForm");
    });

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%= form %>
</asp:Content>