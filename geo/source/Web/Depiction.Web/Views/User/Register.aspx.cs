﻿using System.Web.Mvc;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.User
{
    public class Register : ViewPage
    {
        protected string form { get; private set; }

        protected override void OnLoad(System.EventArgs e)
        {
            var formItems = new IFormItem[] 
                {
                    new HeaderFormItem("Register for the Trial Site"), 
                    new DataFormItem("Email", new TextHtmlControl("Email", string.Empty, new ValidationInformation { Validations = new IValidation[] {new EmailValidation(), new RequiredValidation()} })), 
                    new DataFormItem("First Name", new TextHtmlControl("FirstName", string.Empty, new ValidationInformation { Validations = new[] {new RequiredValidation()} })), 
                    new DataFormItem("Last Name", new TextHtmlControl("LastName", string.Empty, new ValidationInformation { Validations = new[] {new RequiredValidation()} })), 
                    new DataFormItem("Password", new TextHtmlControl("Password", string.Empty, new ValidationInformation { Validations = new[] {new RequiredValidation()} }, true)),
                    new DataFormItem("Confirm Password", new TextHtmlControl("ConfirmPassword", string.Empty, new ValidationInformation { Validations = new IValidation[] {new MatchesValidation("Password")} }, true)),
                    new ButtonsFormItem(new IButton[] {new SubmitButton("Register")})
                };

            form = FormBuilder.BuildForm<UserController>(u => u.SaveUser(0), "RegisterForm", formItems);
            base.OnLoad(e);
        }
    }
}