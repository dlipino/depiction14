﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Registered.aspx.cs" Inherits="Depiction.Web.Views.User.Registered" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Thank you for registering!</h1>
    <p>We will send you an email when your account has been approved.</p>
</asp:Content>
