﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.SignUp.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">

    window.addEvent("domready", function() {
        var exValidatorA = new fValidator("RegistrationForm");
    });

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<h1>Please register for download</h1>
<p>Please register so we can notify you of free webinars, tutorials and other information that will make your trial experience better. An email will be sent to you shortly with an activation code and download information. We will not give or sell your information to anyone else.</p>
<%= form %>
</asp:Content>