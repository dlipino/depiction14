﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.Reports.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<h1>Available reports</h1>
<%= (new LinkButtonHtmlControl<ReportsController>(r => r.TrialRegistrations(), "Trial Registrations")).Button %>
<%= (new LinkButtonHtmlControl<ReportsController>(r => r.UsageStatistics(), "Usage Statistics")).Button %>
</asp:Content>