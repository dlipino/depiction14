﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DMLPropertyOptions.ascx.cs"
    Inherits="Depiction.Web.Views.LiveReports.DMLPropertyOptions" %>
<%@ Import Namespace="Depiction.Web.Helpers.Validation"%>
<%  if (Model != null)
 {
     string[] value;
     foreach (var sessionProperty in Model)
     {
         // Strip off the assembly portion (if it exists) of the type name.
         var typeName = sessionProperty.Property.ValueType.Split(new[] { ',' }).First();

         switch (typeName)
         {
             case "System.String":
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new TextHtmlControl("Property::" + sessionProperty.Property.PropertyName, sessionProperty.Property.DefaultValue)).FormItem%>
<%
                 break;
                 
             case "System.Boolean":
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new DropDownListHtmlControl("Property::" + sessionProperty.Property.PropertyName, new[] { new SelectOption { Label = "Yes", Value = "True" }, new SelectOption { Label = "No", Value = "False" } })).FormItem%>
<%
                 break;

             case "System.Int32":
             case "System.Double":
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new TextHtmlControl("Property::" + sessionProperty.Property.PropertyName, sessionProperty.Property.DefaultValue, new ValidationInformation { Validations = new IValidation[] { new NumericValidation() }})).FormItem%>
<%
                 break;
                 
             case "Depiction.API.ValueObject.Distance":
                 value = sessionProperty.Property.DefaultValue.Split(' ');
                 if(value.Length != 2) value = new [] { "0", "feet"};
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new DistanceHTMLControl("Property::" + sessionProperty.Property.PropertyName, value[0], value[1])).FormItem%>
<%
                 break;
                 
             case "Depiction.API.ValueObject.Speed":
                 value = sessionProperty.Property.DefaultValue.Split(' ');
                 if(value.Length != 2) value = new [] { "0", "mph"};
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new SpeedHTMLControl("Property::" + sessionProperty.Property.PropertyName, value[0], value[1])).FormItem%>
<%
                 break;

             case "Depiction.API.ValueObject.Area":
                 value = sessionProperty.Property.DefaultValue.Split(' ');
                 if(value.Length != 2) value = new [] { "0", "square feet"};
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new AreaHtmlControl("Property::" + sessionProperty.Property.PropertyName, value[0], value[1])).FormItem%>
<%
                 break;

             case "System.Windows.Media.Color":
                 value = new[] { sessionProperty.Property.DefaultValue, "Brown" };
                 var controlName = "Property::" + sessionProperty.Property.PropertyName;
                 var defaultValue = sessionProperty.Property.DefaultValue;
                 //var validation = new ValidationInformation {Validations = new IValidation[] {new ColorValidation()}};
%>
        <%= new DataFormItem(sessionProperty.Property.DisplayName, new TextHtmlControl(controlName, defaultValue)).FormItem%>
<%
                 break;
         }
     }
 } %>
 
 