﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DMLPropertySelector.ascx.cs" Inherits="Depiction.Web.Views.LiveReports.DMLPropertySelector" %>
<%
    foreach (var dmlProperty in Model)
    {
        %>
        <%= new DataFormItem("&nbsp", new CheckBoxHtmlControl("dmlProperties", dmlProperty.Id.ToString(), dmlProperty.DisplayName, false)).FormItem %>
        <%
    }%>