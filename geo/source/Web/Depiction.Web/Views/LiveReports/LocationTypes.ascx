﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationTypes.ascx.cs" Inherits="Depiction.Web.Views.LiveReports.LocationTypes" %>
<%@ Import Namespace="Depiction.Web.Helpers.Validation"%>
<%
    switch(Model.LocationType)
    {
        case "LatLon":
%>
        <%= new DataFormItem("Latitude", new TextHtmlControl("Location::Latitude", string.Empty, new ValidationInformation { Validations = new IValidation[] { new RequiredValidation() } })).FormItem%>
        <%= new DataFormItem("Longitude", new TextHtmlControl("Location::Longitude", string.Empty, new ValidationInformation { Validations = new IValidation[] { new RequiredValidation() } })).FormItem%>
<%
            break;
        case "Address":
%>
        <%= new DataFormItem("Address", new TextHtmlControl("Location::Address", string.Empty, new ValidationInformation { Validations = new IValidation[] { new RequiredValidation(), new BindValidation("Property::Address") } })).FormItem%>
        <%= new DataFormItem("City", new TextHtmlControl("Location::City", string.Empty, new ValidationInformation { Validations = new IValidation[] { new RequiredValidation(), new BindValidation("Property::City") } })).FormItem%>
        <%= new DataFormItem("State", new TextHtmlControl("Location::State", string.Empty, new ValidationInformation { Validations = new IValidation[] { new RequiredValidation(), new BindValidation("Property::State") } })).FormItem%>
        <%= new DataFormItem("Zip code", new TextHtmlControl("Location::ZipCode", string.Empty, new ValidationInformation { Validations = new IValidation[] { new BindValidation("Property::ZipCode") } })).FormItem%>
<%
            break;
    }
     %>