﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.LiveReports
{
    public class AddElement : ViewPage<SelectOption[]>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem("Add an element to the session"),
                new DataFormItem("Element type", new DropDownListHtmlControl("ElementType", Model, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DivFormItem("ElementProperties", string.Empty),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Add element")})
            };

            form = FormBuilder.BuildForm<LiveReportsController>(lr => lr.DoAddElement(), "AddElementForm", formItems.ToArray());

            base.OnLoad(e);
        }
    }
}