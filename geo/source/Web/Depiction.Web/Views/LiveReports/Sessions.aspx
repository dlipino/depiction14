﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Sessions.aspx.cs" Inherits="Depiction.Web.Views.LiveReports.Sessions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">

    window.addEvent("domready", function() {
        var exValidatorA = new fValidator("CreateLiveReportSession");
    });

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h1>Live Report Sessions</h1>
<%
    foreach (var liveReportSession in Model)
    {
        %>
        <p>
        <% if (User.Identity.IsAuthenticated)
           {
               if (User.IsInRole("admin"))
               {%> 
            <a href="<%= ResolveUrl("~/LiveReports/Delete/" + liveReportSession.Id) %>">delete</a>
            <a href="<%= ResolveUrl("~/LiveReports/Edit/" + liveReportSession.Id) %>">edit</a>
        <%     }
           } %>
            <a href="<%= ResolveUrl("~/LiveReports/Submit/" + liveReportSession.Id) %>"><%= liveReportSession.SendReportsToEmail%></a>
        </p>
        <%
    } %>

</asp:Content>
