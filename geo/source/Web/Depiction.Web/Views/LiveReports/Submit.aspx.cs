﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Validation;

namespace Depiction.Web.Views.LiveReports
{
    public class Submit : ViewPage<LiveReportSession>
    {
        protected string form = string.Empty;

        protected override void OnLoad(EventArgs e)
        {
            var formItems = new List<IFormItem> {
                new HeaderFormItem("Submit a Live Report to " + Model.SendReportsToEmail.Replace("@", " at ")),
                new DataFormItem("From", new TextHtmlControl("From", "your email address", new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation(), new EmailValidation()}
                })),
                new DataFormItem("Element type", new DropDownListHtmlControl("ElementType", BuildElementTypes(), new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}
                })),
                new DivFormItem("ElementProperties", string.Empty),
                new DataFormItem("Location type", new DropDownListHtmlControl("LocationType", new[] {
                    new SelectOption { Label = "Choose location mode ...", Value = string.Empty},
                    new SelectOption {
                    Label = "Latitude/Longitude",
                    Value = "LatLon"
                }, new SelectOption {
                    Label = "Address",
                    Value = "Address"
                },}, new ValidationInformation {
                    Validations = new IValidation[] {new RequiredValidation()}})),
                new DivFormItem("LocationSpec", string.Empty),
                new CaptchaFormItem(),
                new ButtonsFormItem(new IButton[] {new SubmitButton("Submit Live Report")})
            };

            form = FormBuilder.BuildForm<LiveReportsController>(lr => lr.DoSubmit(Model.Id), "SubmitLiveReport", formItems.ToArray());

            base.OnLoad(e);
        }

        private SelectOption[] BuildElementTypes()
        {
            var dmlNames = Model.Properties.Select(p => new {
                displayName = p.Property.DMLDefinition.DMLDisplayName,
                definitionId = p.Property.DMLDefinition.Id.ToString()
            }).Distinct().ToList();

            List<SelectOption> options = dmlNames.Select(n => new SelectOption {
                Label = n.displayName,
                Value = Model.Id + "/" + n.definitionId
            }).ToList();

            options.Insert(0, new SelectOption {
                Label = "choose one ...",
                Value = string.Empty
            });

            return options.ToArray();
        }
    }
}