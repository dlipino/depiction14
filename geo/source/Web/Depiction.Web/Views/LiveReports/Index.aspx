﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Stats.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Depiction.Web.Views.LiveReports.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table>
    <thead>
        <td>General</td>
    </thead>
    <tr>
        <td><%= (new LinkButtonHtmlControl<DMLController>(d => d.Index(), "Manage DML Files")).Button %></td>
    </tr>

    <tr>
        <td><%= (new LinkButtonHtmlControl<LiveReportsController>(l => l.Sessions(), "View sessions")).Button %></td>
    </tr>    
    <tr>
        <td><%= (new LinkButtonHtmlControl<LiveReportsController>(l => l.CreateSession(), "Create a live reports session")).Button %></td>
    </tr>
</table>
</asp:Content>