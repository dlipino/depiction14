﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace PublishingService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [ValidateInput(false)]
        public ActionResult Publish(string author, string description, string version, byte[] depictionStory)
        {
            var story = new DepictionStory();
            using (var db = new PublishingServiceContext())
            {
                story.Author = author;
                story.Description = description;
                story.StoryFile = depictionStory;
                story.Version = version;
                story.RandomizedURL = GenerateRandomURL(6);
                db.DepictionStories.Add(story);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {

                }
            }
            return new JsonResult { Data = string.Format("http://{0}/Home/Depiction/{1}", Request.Url.Authority, story.RandomizedURL), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GenerateRandomURL(int length)
        {
            
            StringBuilder sb = new StringBuilder();
            Random random = new Random();
            string url = "";
            bool completed = false;
            while (!completed)
            {
                sb.Clear();
                for (int i = 0; i < length; i++)
                {
                    int next = random.Next(10 + 26 + 26);
                    if (next < 10)
                        sb.Append(next);
                    else if (next < 36)
                        sb.Append(Char.ConvertFromUtf32(next + 65 - 10));
                    else
                    {
                        sb.Append(Char.ConvertFromUtf32(next + 97 - 36));
                    }
                }
                url = sb.ToString();
                using (var db = new PublishingServiceContext())
                {
                    DepictionStory story = db.DepictionStories.FirstOrDefault(t => t.RandomizedURL == url);
                    completed = (story == null);
                }
            }
            return url;
        }

        public ActionResult Depiction(string id)
        {
            using (var db = new PublishingServiceContext())
            {
                DepictionStory story = db.DepictionStories.First(t => t.RandomizedURL.Equals(id));
                return View("Depiction", story);
            }
        }

        public ActionResult DepictionZIP(string id)
        {
            using (var db = new PublishingServiceContext())
            {
                DepictionStory story = db.DepictionStories.First(t => t.RandomizedURL == id);
                return new FileContentResult(story.StoryFile, System.Net.Mime.MediaTypeNames.Application.Octet);
            }
        }




    }
}
