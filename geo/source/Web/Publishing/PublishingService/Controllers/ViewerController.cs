﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PublishingService.Controllers
{
    public class ViewerController : Controller
    {

        public ActionResult Get(string version)
        {
            using (var db = new PublishingServiceContext())
            {

                DepictionViewer viewer = db.DepictionViewers.First(t => t.Version == version && !t.Deleted);
                return new FileContentResult(viewer.ViewerXAP, "System.Net.Mime.MediaTypeNames.Application.Octet");
            }
        }

        public ActionResult Upload()
        {
            return View("Upload");
        }
        public ActionResult DoUpload(string version, HttpPostedFileBase file)
        {
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the filename
                var fileName = Path.GetFileName(file.FileName);

                using (var db = new PublishingServiceContext())
                {

                    DepictionViewer viewer = db.DepictionViewers.FirstOrDefault(t => t.Version == version && !t.Deleted);
                    if (viewer != null)
                        return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "This version of the viewer already exists.  To replace it, delete it first, then upload.");

                    viewer = new DepictionViewer();
                    viewer.Version = version;
                    var memoryStream = new MemoryStream();
                    file.InputStream.CopyTo(memoryStream);
                    viewer.ViewerXAP = memoryStream.ToArray();
                    db.DepictionViewers.Add(viewer);
                    db.SaveChanges();
                    return new ContentResult { Content = string.Format("Successfully uploaded version {0}", version) };
                }


                //// store the file inside ~/App_Data/uploads folder
                //var path = Path.Combine(Server.MapPath("~/App_Data/uploads"), fileName);
                //file.SaveAs(path);
            }
            // redirect back to the index action to show the form once again
            //return RedirectToAction("Index");
            return null;
        }
        //{
        //    using (var db = new PublishingServiceContext())
        //    {

        //        DepictionViewer viewer = db.DepictionViewers.First(t => t.Version == version && !t.Deleted);
        //        if (viewer != null)
        //            return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "This version of the viewer already exists.  To replace it, delete it first, then upload.");

        //        viewer = new DepictionViewer();
        //        viewer.Version = version;
        //        viewer.ViewerXAP = viewerXAP;
        //        db.DepictionViewers.Add(viewer);
        //        db.SaveChanges();
        //        return new ContentResult { Content = string.Format("Successfully uploaded version {0}",version) };
        //    }
        //}

        public ActionResult Delete(string version)
        {
            using (var db = new PublishingServiceContext())
            {

                DepictionViewer viewer = db.DepictionViewers.First(t => t.Version == version && !t.Deleted);
                viewer.Deleted = true;
                try
                {
                    db.SaveChanges();
                    return new ContentResult { Content = string.Format("Successfully deleted version {0}", version) };
                }
                catch (Exception e)
                {
                    return new ContentResult { Content = string.Format("Failed to delete version {0}", version) };
                }
            }
        }
    }
}