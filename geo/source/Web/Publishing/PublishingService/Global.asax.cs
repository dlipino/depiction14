﻿using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace PublishingService
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //using (var db = new PublishingServiceContext())
            //{
            //    var bytes = File.ReadAllBytes(@"C:\depiction\svn\trunk\geo\source\Viewer\SilverlightTest\ClientBin\depictionForWeb.zip");
            //    db.DepictionStories.Add(new DepictionStory() { Author = "jason", Description = "qwerty", StoryFile = bytes, Version = "1" });
            //    try
            //    {
            //        db.SaveChanges();
            //    }
            //    catch (Exception e)
            //    {

            //    }
            //}
            //Remove and JsonValueProviderFactory and add JsonDotNetValueProviderFactory
            ValueProviderFactories.Factories.Remove(ValueProviderFactories.Factories.OfType<JsonValueProviderFactory>().FirstOrDefault());
            ValueProviderFactories.Factories.Add(new JsonDotNetValueProviderFactory());
        }
    }

    public class DepictionStory
    {

        public int Id { get; set; }
        public string Author { get; set; }
        public string Version { get; set; }
        public string RandomizedURL { get; set; }
        public string Description { get; set; }
        //public string XML { get; set; }
        public byte[] StoryFile { get; set; }
        public bool Deleted { get; set; }

    }

    public class DepictionViewer
    {

        public int Id { get; set; }
        public string Version { get; set; }
        public byte[] ViewerXAP { get; set; }
        public bool Deleted { get; set; }
    }

    public class PublishingServiceContext : DbContext
    {
        public DbSet<DepictionStory> DepictionStories { get; set; }
        public DbSet<DepictionViewer> DepictionViewers { get; set; } 
    }

}