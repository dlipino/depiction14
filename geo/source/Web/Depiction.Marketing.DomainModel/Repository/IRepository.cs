﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NHibernate;

namespace Depiction.Marketing.DomainModel.Repository
{
    public interface IRepository
    {
        void Delete<T>(T objectToDelete);
        void Save<T>(T objectToSave);
        T GetOne<T>(int id);
        T[] GetAll<T>();
        T[] Search<T>(Expression<Func<T, bool>> expression);
        IQueryable<T> Queryable<T>();
        ITransaction BeginTransaction();
        ISession RepositorySession { get; }
    }
}