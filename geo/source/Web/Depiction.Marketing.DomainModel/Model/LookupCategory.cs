﻿namespace Depiction.Marketing.DomainModel.Model
{
    public class LookupCategory
    {
        public virtual int Id { get; set; }
        public virtual string Category { get; set; }
    }
}