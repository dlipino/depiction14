﻿using System.Collections.Generic;

namespace Depiction.Marketing.DomainModel.Model
{
    public class User
    {
        public virtual int Id { get; set; }
        //public virtual WebDepiction WebDepictions { get; set; }
        public virtual Person Person { get; set; }
        public virtual IList<Role> Roles { get; set; }
        public virtual string Password { get; set; }
    }
}