﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class SessionProperty
    {
        public virtual Int32 Id { get; private set; }
        public virtual DMLProperty Property { get; set; }
        public virtual bool Required { get; set; }
    }
}