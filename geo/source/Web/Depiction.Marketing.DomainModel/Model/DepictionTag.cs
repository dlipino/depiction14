﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class DepictionTag
    {
        public virtual Int32 Id { get; set; }
        public virtual String Tag { get; set; }
    }
}