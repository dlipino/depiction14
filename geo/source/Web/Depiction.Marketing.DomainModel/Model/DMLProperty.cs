﻿namespace Depiction.Marketing.DomainModel.Model
{
    public class DMLProperty
    {
        public virtual int Id { get; private set; }
        public virtual string PropertyName { get; set; }
        public virtual string DisplayName { get; set; }
        public virtual string DefaultValue { get; set; }
        public virtual string ValueType { get; set; }
        public virtual DMLDefinition DMLDefinition { get; set; }
    }
}