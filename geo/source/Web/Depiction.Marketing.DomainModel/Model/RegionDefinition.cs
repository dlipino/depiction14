﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class RegionDefinition
    {
        public virtual Int32 Id { get; set; }
        public virtual string RegionCode { get; set; }
        public virtual string RegionName { get; set; }
    }
}