﻿using System;
using System.Collections.Generic;

namespace Depiction.Marketing.DomainModel.Model
{
    public class QuickstartData
    {
        public virtual Int32 Id { get; set; }
        public virtual string QuickstartName { get; set; }
        public virtual IList<QuickstartParameter> Parameters { get; set; }
        public virtual string Description { get; set; }
        //public virtual string URL { get; set; }
    }
}