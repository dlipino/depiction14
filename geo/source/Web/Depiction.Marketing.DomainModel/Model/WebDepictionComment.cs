﻿using System;

namespace Depiction.Marketing.DomainModel.Model
{
    public class WebDepictionComment
    {
        public virtual int Id { get; private set; }
        public virtual string Comment { get; set; }
        public virtual WebDepiction WebDepiction { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual Person Person { get; set; }
    }
}