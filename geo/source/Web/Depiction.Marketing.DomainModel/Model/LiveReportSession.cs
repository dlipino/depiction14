﻿using System.Collections.Generic;

namespace Depiction.Marketing.DomainModel.Model
{
    public class LiveReportSession
    {
        public LiveReportSession()
        {
            Properties = new List<SessionProperty>();
        }

        public virtual int Id { get; private set; }
        public virtual string SendReportsToEmail { get; set; }
        public virtual IList<SessionProperty> Properties { get; set; }
    }
}