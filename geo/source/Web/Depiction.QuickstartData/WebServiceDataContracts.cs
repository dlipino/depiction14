﻿using System.Runtime.Serialization;

namespace Depiction.WebServices
{
    [DataContract(Namespace = "http://request.depiction.info/QuickstartDataService")]
    public class LatitudeLongitude
    {
        [DataMember]
        public double Latitude { get; set; }

        [DataMember]
        public double Longitude { get; set; }
    }

    [DataContract(Namespace = "http://request.depiction.info/QuickstartDataService")]
    public class BoundingBox
    {
        [DataMember]
        public double NorthLatitude { get; set; }

        [DataMember]
        public double SouthLatitude { get; set; }

        [DataMember]
        public double EastLongitude { get; set; }

        [DataMember]
        public double WestLongitude { get; set; }
    }
}