﻿using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace Depiction.WebServices
{
    public class DepictionValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (userName != "DepiCtion*f54KJfdi@090" || password != "87$389FdjkJfio234{}fdsfdshGjew4*&Fd")
                throw new SecurityTokenException();
        }
    }
}