﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Authentication;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Enum;
using Depiction.Marketing.DomainModel.ExtensionMethods;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using ICSharpCode.SharpZipLib.Zip;
using NHibernate;
using NHibernate.Linq;

namespace Depiction.WebServices
{
    // NOTE: If you change the class name "DepictionWebServiceMVVM" here, you must also update the reference to "DepictionWebServiceMVVM" in Web.config.
    public class DepictionWebServiceMVVM : IDepictionWebServiceMVVM
    {
        public string[] GetRegionCodesForRegion(BoundingBox boundingBox)
        {
            var regionCodes = new List<string>();

            try
            {
                var corners = new[] {new {
                    lat = boundingBox.NorthLatitude,
                    lon = boundingBox.WestLongitude
                }, new {
                    lat = boundingBox.SouthLatitude,
                    lon = boundingBox.WestLongitude,
                }, new {
                    lat = boundingBox.NorthLatitude,
                    lon = boundingBox.EastLongitude
                }, new {
                    lat = boundingBox.SouthLatitude,
                    lon = boundingBox.EastLongitude
                }};

                foreach (var corner in corners)
                {
                    WebRequest countryRequest = WebRequest.Create(string.Format("http://ws.geonames.net/countrySubdivision?lat={0}&lng={1}&radius=1&maxRows=10&username=depiction", corner.lat, corner.lon));
                    WebResponse response = countryRequest.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        var xml = new XmlDocument();
                        XmlReader reader = XmlReader.Create(stream);
                        xml.Load(reader);

                        XmlNode countryCodeNode = xml.SelectSingleNode("/geonames/countrySubdivision/countryCode");

                        if (countryCodeNode != null && !regionCodes.Contains(countryCodeNode.InnerText))
                            regionCodes.Add(countryCodeNode.InnerText);
                    }
                }
            }
            catch
            {
                return regionCodes.ToArray();
            }

            return regionCodes.ToArray();
        }

        public QuickstartData[] GetQuickstartSourcesByRegionCodes(string depictionVersion, string[] regionCodes)
        {

            var quickStartData = new List<QuickstartData>();

            using (var session = DBFactory.GetSession())
            {
                var repository = new BaseRepository(session);

                var version = repository.Queryable<DepictionVersion>().First(v => v.VersionNumber == depictionVersion);
                IQueryable<Marketing.DomainModel.Model.AddinInformation> addinForRegion = repository.Queryable<Marketing.DomainModel.Model.AddinInformation>().Where(a => a.ValidWithVersions.Contains(version));

                foreach (Marketing.DomainModel.Model.AddinInformation addinInformation in addinForRegion)
                {
                    if (addinInformation.Regions.Count != 0)
                    {
                        bool inRegion = false;

                        foreach (var region in addinInformation.Regions)
                        {
                            if (!regionCodes.Contains(region.RegionCode)) continue;
                            inRegion = true;
                            break;
                        }

                        if (!inRegion) continue;
                    }

                    foreach (Marketing.DomainModel.Model.QuickstartData quickstartItem in addinInformation.RelatedQuickstartItems)
                    {
                        var quickstartItemParameters = new Dictionary<string, string>();

                        foreach (QuickstartParameter parameter in quickstartItem.Parameters)
                        {
                            quickstartItemParameters.Add(parameter.ParameterName, parameter.ParameterValue);
                        }

                        quickStartData.Add(new QuickstartData
                        {
                            DataFacade = addinInformation.Name,
                            ElementType = Enum.GetName(typeof(AddinType), addinInformation.AddinType),
                            Name = quickstartItem.QuickstartName,
                            Parameters = quickstartItemParameters,
                            Description = quickstartItem.Description,
                            //URL = quickstartItem.URL,
                        });
                    }
                }
            }

            return quickStartData.ToArray();
        }

        public bool IsProductSNValid(string serialNumber, int productId)
        {
            throw new NotImplementedException();
        }

        public int BeginPublishToWeb(string depictionVersion, DepictionWebServiceParameter[] parameters)
        {
            //Expect the following parameters: String userName, String password, String depictionTitle, String description, string[] tags
            var userName = (String)parameters.Single(p => p.Key == "UserName").Value;
            var password = (String)parameters.Single(p => p.Key == "Password").Value;
            var depictionTitle = (String)parameters.Single(p => p.Key == "depictionTitle").Value;
            var description = (String)parameters.Single(p => p.Key == "Description").Value;
            var filename = (String)parameters.Single(p => p.Key == "Filename").Value;

            var webDepiction = new WebDepiction
            {
                DepictionTitle = depictionTitle,
                Description = description,
                Filename = filename,
                CreationDate = DateTime.Now
            };

            using (ISession session = DBFactory.GetSession())
            {
                var user = session.Linq<User>().Where(u => u.Person.Email == userName && u.Password == password.PasswordEncode()).ToArray();

                if (user.Length != 1) throw new FaultException<AuthenticationException>(new AuthenticationException("No account exists for that email and password combination."), new FaultReason("No account exists for that email and password combination."));

                if (!user[0].Roles.Any(r => r.RoleName == "publishtoweb"))
                    throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException("That user is not authorized to publish to web. Please email info@depiction.com to request access."), new FaultReason("That user is not authorized to publish to web. Please email info@depiction.com to request access."));

                webDepiction.User = user[0];

                foreach (var tag in (String[])parameters.Single(p => p.Key == "Tags").Value)
                {
                    var depictionTag = session.Linq<DepictionTag>().FirstOrDefault(dt => dt.Tag == tag.Trim().ToLower());

                    if (depictionTag != null)
                    {
                        webDepiction.Tags.Add(depictionTag);
                    }
                    else
                    {
                        webDepiction.Tags.Add(new DepictionTag { Tag = tag.Trim().ToLower() });
                    }
                }

                using (ITransaction transaction = session.BeginTransaction())
                {
                    foreach (var tag in webDepiction.Tags)
                    {
                        session.SaveOrUpdate(tag);
                    }

                    session.Save(webDepiction);
                    transaction.Commit();
                }
            }

            return webDepiction.Id;
        }

        public bool FinalizePublishToWeb(int webDepictionId)
        {
            try
            {
                var webDepictionPath = Path.Combine(GetWebDepictionPath(webDepictionId), "webDepiction.zip");
                var zip = new FastZip();
                zip.ExtractZip(webDepictionPath, GetWebDepictionPath(webDepictionId), "");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void PublishWebDepictionChunk(int webDepictionId, byte[] fileChunk)
        {
            var webDepictionPath = Path.Combine(GetWebDepictionPath(webDepictionId), "webDepiction.zip");

            using (var fileStream = File.Exists(webDepictionPath) ? new FileStream(webDepictionPath, FileMode.Append) : new FileStream(webDepictionPath, FileMode.Create))
            {
                fileStream.Write(fileChunk, 0, fileChunk.Length);
            }
        }

        public void PublishDPNChunk(int webDepictionId, byte[] fileChunk)
        {
            string filename;
            using (ISession session = DBFactory.GetSession())
            {
                var webDepiction = session.Linq<WebDepiction>().First(u => u.Id == webDepictionId);
                filename = webDepiction.Filename;
            }
            var webDepictionPath = Path.Combine(GetWebDepictionPath(webDepictionId), filename);

            using (var fileStream = File.Exists(webDepictionPath) ? new FileStream(webDepictionPath, FileMode.Append) : new FileStream(webDepictionPath, FileMode.Create))
            {
                fileStream.Write(fileChunk, 0, fileChunk.Length);
            }
        }

        private string GetWebDepictionPath(Int32 webDepictionId)
        {
            var webDepictionPath = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WebDepictions"), webDepictionId.ToString());

            if (!Directory.Exists(webDepictionPath))
                Directory.CreateDirectory(webDepictionPath);

            return webDepictionPath;
        }
    }
}
