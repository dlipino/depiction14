﻿using System;
using System.Security.Authentication;
using System.ServiceModel;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    // NOTE: If you change the interface name "IDepictionWebService" here, you must also update the reference to "IDepictionWebService" in Web.config.
    [ServiceContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public interface IDepictionWebService
    {
        [OperationContract(Name = "GetRegionCodesForRegion")]
        string[] GetRegionCodesForRegion(string depictionVersion, DepictionWebServiceParameter[] parameters);

        [OperationContract(Name = "GetQuickstartSourcesByRegionCodes")]
        QuickstartData[] GetQuickstartSourcesByRegionCodes(string depictionVersion, DepictionWebServiceParameter[] parameters);

        [OperationContract(Name = "GeocodeLocation")]
        LatitudeLongitude GeocodeLocation(string depictionVersion, DepictionWebServiceParameter[] parameters);

        [OperationContract(Name = "SearchRemoteRepositoryForAddinNotFoundLocally")]
        AddinInformation SearchRemoteRepositoryForAddinNotFoundLocally(string depictionVersion, DepictionWebServiceParameter[] parameters);

        [OperationContract(Name = "GetTrialLicense")]
        TrialLicenseResult GetTrialLicense(string activationCode);

        [OperationContract]
        bool IsProductSNValid(string serialNumber, int productId);

        [OperationContract]
        void LogMetrics(MetricType metricType, string logKey, string details);

        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(UnauthorizedAccessException))]
        Int32 BeginPublishToWeb(string depictionVersion, DepictionWebServiceParameter[] parameters);

        [OperationContract]
        bool FinalizePublishToWeb(Int32 webDepictionId);

        [OperationContract]
        void PublishWebDepictionChunk(Int32 webDepictionId, byte[] fileChunk);

        [OperationContract]
        void PublishDPNChunk(Int32 webDepictionId, byte[] fileChunk);
    }
}