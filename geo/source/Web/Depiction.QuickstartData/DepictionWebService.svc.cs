﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Authentication;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Xml;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Enum;
using Depiction.Marketing.DomainModel.ExtensionMethods;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.WebServices.CheckpointService;
using ICSharpCode.SharpZipLib.Zip;
using NHibernate;
using NHibernate.Linq;

namespace Depiction.WebServices
{
    // NOTE: If you change the class name "DepictionWebService" here, you must also update the reference to "DepictionWebService" in Web.config.
    public class DepictionWebService : IDepictionWebService
    {
        #region fields

        private const string YahooAppId = "hoMgIV_V34HYGoRQxugEpr_8LNl1Mg36zC0NPyJ0BsrejYb_kC5WGZLc1AwGj9d9BFcQ";
        private const string YahooUri = "http://local.yahooapis.com/MapsService/V1/geocode";

        #endregion

        #region IDepictionWebService Members

        public string[] GetRegionCodesForRegion(string depictionVersion, DepictionWebServiceParameter[] parameters)
        {
            var regionCodes = new List<string>();

            try
            {
                var corners = new[] {new {
                    lat = (double)parameters.Single(p => p.Key == "Top").Value,
                    lon = (double)parameters.Single(p => p.Key == "Right").Value
                }, new {
                    lat = (double)parameters.Single(p => p.Key == "Bottom").Value,
                    lon = (double)parameters.Single(p => p.Key == "Right").Value
                }, new {
                    lat = (double)parameters.Single(p => p.Key == "Top").Value,
                    lon = (double)parameters.Single(p => p.Key == "Left").Value
                }, new {
                    lat = (double)parameters.Single(p => p.Key == "Bottom").Value,
                    lon = (double)parameters.Single(p => p.Key == "Left").Value
                }};

                foreach (var corner in corners)
                {
                    WebRequest countryRequest = WebRequest.Create(string.Format("http://ws.geonames.net/countrySubdivision?lat={0}&lng={1}&radius=1&maxRows=10&username=depiction", corner.lat, corner.lon));
                    WebResponse response = countryRequest.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        var xml = new XmlDocument();
                        XmlReader reader = XmlReader.Create(stream);
                        xml.Load(reader);

                        XmlNode countryCodeNode = xml.SelectSingleNode("/geonames/countrySubdivision/countryCode");

                        if (countryCodeNode != null && !regionCodes.Contains(countryCodeNode.InnerText))
                            regionCodes.Add(countryCodeNode.InnerText);
                    }
                }
            }
            catch
            {
                return regionCodes.ToArray();
            }

            return regionCodes.ToArray();
        }

        public QuickstartData[] GetQuickstartSourcesByRegionCodes(string depictionVersion, DepictionWebServiceParameter[] parameters)
        {
            var regionCodes = (String[]) parameters.Single(p => p.Key == "RegionCodes").Value;

            var quickStartData = new List<QuickstartData>();

            using (var session = DBFactory.GetSession())
            {
                var repository = new BaseRepository(session);

                var version = repository.Queryable<DepictionVersion>().First(v => v.VersionNumber == depictionVersion);
                IQueryable<Marketing.DomainModel.Model.AddinInformation> addinForRegion = repository.Queryable<Marketing.DomainModel.Model.AddinInformation>().Where(a => a.ValidWithVersions.Contains(version));

                foreach (Marketing.DomainModel.Model.AddinInformation addinInformation in addinForRegion)
                {
                    if(addinInformation.Regions.Count != 0)
                    {
                        bool inRegion = false;

                        foreach (var region in addinInformation.Regions)
                        {
                            if (!regionCodes.Contains(region.RegionCode)) continue;
                            inRegion = true;
                            break;
                        }

                        if (!inRegion) continue;
                    }

                    foreach (Marketing.DomainModel.Model.QuickstartData quickstartItem in addinInformation.RelatedQuickstartItems)
                    {
                        var quickstartItemParameters = new Dictionary<string, string>();

                        foreach (QuickstartParameter parameter in quickstartItem.Parameters)
                        {
                            quickstartItemParameters.Add(parameter.ParameterName, parameter.ParameterValue);
                        }

                        quickStartData.Add(new QuickstartData {
                            DataFacade = addinInformation.Name,
                            ElementType = Enum.GetName(typeof (AddinType), addinInformation.AddinType),
                            Name = quickstartItem.QuickstartName,
                            Parameters = quickstartItemParameters,
                            Description = quickstartItem.Description,
                            //URL = quickstartItem.URL,
                        });
                    }
                }
            }

            return quickStartData.ToArray();
        }

        public LatitudeLongitude GeocodeLocation(string depictionVersion, DepictionWebServiceParameter[] parameters)
        {
            var locationToGeocode = (string) parameters.Single(p => p.Key == "Location").Value;
            return DoGeocoding(locationToGeocode);
        }

        public AddinInformation SearchRemoteRepositoryForAddinNotFoundLocally(string depictionVersion, DepictionWebServiceParameter[] parameters)
        {
            var addinName = (string)parameters.Single(p => p.Key == "AddinName").Value;
            var addinType = (AddinType)parameters.Single(p => p.Key == "AddinType").Value;

            Marketing.DomainModel.Model.AddinInformation foundAddin;

            using (ISession session = DBFactory.GetSession())
            {
                foundAddin = session.Linq<Marketing.DomainModel.Model.AddinInformation>().SingleOrDefault(a => a.Name == addinName && a.AddinType == addinType);
            }

            return foundAddin != null ? new AddinInformation {
                AddinDownloadLocation = foundAddin.AddinDownloadLocation,
                AddinType = foundAddin.AddinType,
                FriendlyName = foundAddin.FriendlyName,
                Name = foundAddin.Name
            } : null;
        }

        public TrialLicenseResult GetTrialLicense(string activationCode)
        {
            var licenseResult = new TrialLicenseResult {
                Valid = false,
                ActivationCode = activationCode
            };

            try
            {
                TrialRegistration registration;

                using (ISession session = DBFactory.GetSession())
                {
                    registration = Enumerable.SingleOrDefault(session.Linq<TrialRegistration>(), r => r.ActivationCode == activationCode.ToUpper());

                    if (registration == null)
                        throw new Exception("There were no registrations found matching that activation code.");
                    if (registration.ActivationCount > 2)
                        throw new Exception("That activation code has already been used to activate the trial version three times. Please register for the trial to obtain a new registration code.");

                    licenseResult.LicenseKey = GetHash(registration.ActivationCode);
                    licenseResult.Valid = true;

                    registration.Activated = true;
                    registration.LastActivationDate = DateTime.Now;

                    if (registration.ActivationCount == 0)
                        registration.FirstActivationDate = DateTime.Now;

                    registration.ActivationCount++;

                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Update(registration);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                licenseResult.Valid = false;
                licenseResult.ErrorMessage = ex.Message;
            }

            return licenseResult;
        }

        public bool IsProductSNValid(string serialNumber, int productId)
        {
            var checkpointService = new CheckPointServiceClient();
            var record = new TCheckPointSNSerialRecord();
            TRecordResponse response = checkpointService.GetSerialNoRecordBySN(serialNumber, productId, ref record);
            return response.Success;
        }

        public void LogMetrics(MetricType metricType, string logKey, string details)
        {
            string metricName = LookupStrings.GetMetricTypeString(metricType);

            using (ISession session = DBFactory.GetSession())
            {
                Lookup metric = session.Linq<Lookup>().SingleOrDefault(l => l.LookupCategory.Category == "Usage Metric" && l.Value == metricName);
                TrialRegistration registration = session.Linq<TrialRegistration>().SingleOrDefault(r => r.ActivationCode == logKey);

                var usageMetric = new UsageMetric {
                    DateLogged = DateTime.Now,
                    Details = details,
                    Person = registration == null ? null : registration.Registrant,
                    Metric = metric
                };

                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(usageMetric);
                    transaction.Commit();
                }
            }
        }

        public int BeginPublishToWeb(string depictionVersion, DepictionWebServiceParameter[] parameters)
        {
            //Expect the following parameters: String userName, String password, String depictionTitle, String description, string[] tags
            var userName = (String) parameters.Single(p => p.Key == "UserName").Value;
            var password = (String)parameters.Single(p => p.Key == "Password").Value;
            var depictionTitle = (String)parameters.Single(p => p.Key == "depictionTitle").Value;
            var description = (String)parameters.Single(p => p.Key == "Description").Value;
            var filename = (String) parameters.Single(p => p.Key == "Filename").Value;

            var webDepiction = new WebDepiction {
                DepictionTitle = depictionTitle,
                Description = description,
                Filename = filename,
                CreationDate = DateTime.Now
            };

            using (ISession session = DBFactory.GetSession())
            {
                var user = session.Linq<User>().Where(u => u.Person.Email == userName && u.Password == password.PasswordEncode()).ToArray();

                if (user.Length != 1) throw new FaultException<AuthenticationException>(new AuthenticationException("No account exists for that email and password combination."), new FaultReason("No account exists for that email and password combination."));

                if (!user[0].Roles.Any(r => r.RoleName == "publishtoweb"))
                    throw new FaultException<UnauthorizedAccessException>(new UnauthorizedAccessException("That user is not authorized to publish to web. Please email info@depiction.com to request access."), new FaultReason("That user is not authorized to publish to web. Please email info@depiction.com to request access."));

                webDepiction.User = user[0];

                foreach (var tag in (String[])parameters.Single(p => p.Key == "Tags").Value)
                {
                    var depictionTag = session.Linq<DepictionTag>().FirstOrDefault(dt => dt.Tag == tag.Trim().ToLower());

                    if (depictionTag != null)
                    {
                        webDepiction.Tags.Add(depictionTag);
                    }
                    else
                    {
                        webDepiction.Tags.Add(new DepictionTag { Tag = tag.Trim().ToLower()});
                    }
                }

                using (ITransaction transaction = session.BeginTransaction())
                {
                    foreach (var tag in webDepiction.Tags)
                    {
                        session.SaveOrUpdate(tag);
                    }

                    session.Save(webDepiction);
                    transaction.Commit();
                }
            }

            return webDepiction.Id;
        }

        public bool FinalizePublishToWeb(int webDepictionId)
        {
            try
            {
                var webDepictionPath = Path.Combine(GetWebDepictionPath(webDepictionId), "webDepiction.zip");
                var zip = new FastZip();
                zip.ExtractZip(webDepictionPath, GetWebDepictionPath(webDepictionId), "");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void PublishWebDepictionChunk(int webDepictionId, byte[] fileChunk)
        {
            var webDepictionPath = Path.Combine(GetWebDepictionPath(webDepictionId), "webDepiction.zip");

            using (var fileStream = File.Exists(webDepictionPath) ? new FileStream(webDepictionPath, FileMode.Append) : new FileStream(webDepictionPath, FileMode.Create))
            {
                fileStream.Write(fileChunk, 0, fileChunk.Length);
            }
        }

        public void PublishDPNChunk(int webDepictionId, byte[] fileChunk)
        {
            string filename;
            using (ISession session = DBFactory.GetSession())
            {
                var webDepiction = session.Linq<WebDepiction>().First(u => u.Id == webDepictionId);
                filename = webDepiction.Filename;
            }
            var webDepictionPath = Path.Combine(GetWebDepictionPath(webDepictionId), filename);

            using (var fileStream = File.Exists(webDepictionPath) ? new FileStream(webDepictionPath, FileMode.Append) : new FileStream(webDepictionPath, FileMode.Create))
            {
                fileStream.Write(fileChunk, 0, fileChunk.Length);
            }
        }

        #endregion

        #region helper methods

        /// <summary>
        /// This method needs to be the same as the web service
        /// </summary>
        private static byte[] GetHash(string trialKey)
        {
            var encoding = new ASCIIEncoding();
            byte[] keyBits = encoding.GetBytes(trialKey);

            SHA512 sha512 = new SHA512Managed();
            return sha512.ComputeHash(keyBits);
        }

        private static Dictionary<string, string> GetCatalogParameter(string catalogid)
        {
            var dictionary = new Dictionary<string, string> {
                {"catalogid", catalogid}
            };
            return dictionary;
        }

        private static LatitudeLongitude DoGeocoding(string addressString)
        {
            string url = string.Format("{0}?appid={1}&location={2}", YahooUri, YahooAppId, addressString);
            var request = (HttpWebRequest) WebRequest.Create(url);

            using (var response = (HttpWebResponse) request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    return ParseResponse(responseStream);
                }
            }
        }

        private static LatitudeLongitude ParseResponse(Stream responseStream)
        {
            if (responseStream != null)
            {
                string lattitude = string.Empty;
                string longitude = string.Empty;

                using (var reader = new XmlTextReader(responseStream))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                if (reader.Name != "Succeeded" && reader.Name != "ResultSet")
                                {
                                    if (reader.Name == "Latitude" || reader.Name == "Longitude")
                                    {
                                        string text = reader.ReadString();
                                        if (reader.Name == "Latitude")
                                        {
                                            lattitude = text;
                                        }
                                        if (reader.Name == "Longitude")
                                        {
                                            longitude = text;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        return null;
                    }
                }

                return new LatitudeLongitude {
                    Latitude = Convert.ToDouble(lattitude),
                    Longitude = Convert.ToDouble(longitude)
                };
            }

            return null;
        }

        private string GetWebDepictionPath(Int32 webDepictionId)
        {
            var webDepictionPath = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WebDepictions"), webDepictionId.ToString());

            if (!Directory.Exists(webDepictionPath))
                Directory.CreateDirectory(webDepictionPath);

            return webDepictionPath;
        }

        #endregion
    }
}