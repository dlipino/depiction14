﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    // NOTE: If you change the class name "DepictionWebServiceV1M1" here, you must also update the reference to "DepictionWebServiceV1M1" in Web.config.
    public class DepictionWebServiceV1M3 : IDepictionWebServiceV1M3
    {
        private const string yahooAppId = "hoMgIV_V34HYGoRQxugEpr_8LNl1Mg36zC0NPyJ0BsrejYb_kC5WGZLc1AwGj9d9BFcQ";
        protected string yahooUri = "http://local.yahooapis.com/MapsService/V1/geocode";

        #region IDepictionWebServiceV1M1 Members

        public string[] GetRegionCodesForRegion(BoundingBox region)
        {
            var regionCodes = new List<string>();

            try
            {
                var corners = new[] { new { lat = region.NorthLatitude, lon = region.EastLongitude }, new { lat = region.SouthLatitude, lon = region.EastLongitude }, new { lat = region.NorthLatitude, lon = region.WestLongitude }, new { lat = region.SouthLatitude, lon = region.WestLongitude } };

                foreach (var corner in corners)
                {
                    WebRequest countryRequest = WebRequest.Create(string.Format("http://ws.geonames.net/countrySubdivision?lat={0}&lng={1}&radius=1&maxRows=10&username=depiction", corner.lat, corner.lon));
                    WebResponse response = countryRequest.GetResponse();

                    using (Stream stream = response.GetResponseStream())
                    {
                        var xml = new XmlDocument();
                        XmlReader reader = XmlReader.Create(stream);
                        xml.Load(reader);

                        XmlNode countryCodeNode = xml.SelectSingleNode("/geonames/countrySubdivision/countryCode");

                        if (countryCodeNode != null && !regionCodes.Contains(countryCodeNode.InnerText))
                            regionCodes.Add(countryCodeNode.InnerText);
                    }
                }
            }
            catch
            {
                return regionCodes.ToArray();
            }

            return regionCodes.ToArray();
        }

        public QuickstartDataV1M0[] GetQuickstartSourcesByRegionCodes(string[] regionCodes, string[] gatherersInstalled)
        {
            var quickStartData = new List<QuickstartDataV1M0>();
            var dataDefinition = new Dictionary<string, QuickstartDataV1M0[]>();

            // US Data
            var usElevation = new QuickstartDataV1M0 { DataFacade = "Portal", ElementType = "Elevation", Name = "Elevation (USGS 30m)", Parameters = getCatalogParameter("355928") };
            var usWaterBodies = new QuickstartDataV1M0 { ElementType = "WaterBody", DataFacade = "Portal", Name = "Water bodies (US Census)", Parameters = getCatalogParameter("355926") };
            var terraServerDOQ = new QuickstartDataV1M0 { DataFacade = "TerraServerSatelliteGatherer", ElementType = "ElementGatherer", Name = "Imagery (TerraServer)" };
            var terraServerUrban = new QuickstartDataV1M0 { DataFacade = "TerraServerUrbanGatherer", ElementType = "ElementGatherer", Name = "Urban High-Res Imagery (TerraServer)" };
            var terraServerTopo = new QuickstartDataV1M0 { DataFacade = "TerraServerTopoGatherer", ElementType = "ElementGatherer", Name = "Topographic Map (TerraServer)" };
            var weatherService = new QuickstartDataV1M0 { DataFacade = "UnitedStatesNationalWeather24HourGathererV2", ElementType = "ElementGatherer", Name = "Weather Forecast (NOAA 24-hour)" };
            var dopplerData = new QuickstartDataV1M0 { DataFacade = "Portal", ElementType = "Image", Name = "Current Doppler Weather Data", Parameters = getCatalogParameter("355939") };
            
            // Canadian data
            var caElevation = new QuickstartDataV1M0 { Name = "Elevation (Canada CDED)", DataFacade = "Portal", ElementType = "Elevation", Parameters = getCatalogParameter("355931") };

            //Afghanistan Data
            var afElevation = new QuickstartDataV1M0 { Name = "Elevation (Afghanistan NED 30m)", DataFacade = "Portal", ElementType = "Elevation", Parameters = getCatalogParameter("355934") };


            dataDefinition.Add("US", new[] { usElevation, usWaterBodies, terraServerDOQ, terraServerUrban, terraServerTopo, weatherService, dopplerData });
            dataDefinition.Add("CA", new[] { caElevation }); 
            dataDefinition.Add("AF", new[] { afElevation });

            foreach (string regionCode in regionCodes.Distinct())
                if (dataDefinition.ContainsKey(regionCode))
                    quickStartData.AddRange(dataDefinition[regionCode]);

            // Just add this in for everybody for now
            quickStartData.Add(new QuickstartDataV1M0 { DataFacade = "NASAJPLGatherer", ElementType = "ElementGatherer", Name = "Imagery (NASA Landsat 7)" });
            quickStartData.Add(new QuickstartDataV1M0 { DataFacade = "OpenStreetMapsGatherer", ElementType = "ElementGatherer", Name = "Street Map (OpenStreetMap)" });
            quickStartData.Add(new QuickstartDataV1M0 { DataFacade = "OpenStreetMapRoadNetworkGatherer.6", ElementType = "ElementGatherer", Name = "Road Network (OpenStreetMap)" });
            //World Elevation Data
            var yahooParameter = new Dictionary<string, string>();
            yahooParameter.Add("URI", "http://api.local.yahoo.com/MapsService/V1/geocode?appid=Tango");

            quickStartData.Add(new QuickstartDataV1M0 { DataFacade = "ElementGeocoder", ElementType = "Geocoder", Name = "Element geocoder", Parameters = yahooParameter });

            return quickStartData.ToArray();
        }

        public LatitudeLongitude GeocodeLocation(string locationToGeocode)
        {
            return DoGeocoding(locationToGeocode);
        }

        public AddinInformationV1M0 SearchRemoteRepositoryForAddinNotFoundLocally(string addinName, AddinType addinType)
        {
            return availableAddins.SingleOrDefault(a => a.Name == addinName && a.AddinType == addinType);
        }

        private static readonly List<AddinInformationV1M0> availableAddins = new[]
            {
                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/TerraServerSatelliteGatherer.zip",
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0],
                    FriendlyName = "Imagery (TerraServer)", Name = "TerraServerSatelliteGatherer", Version = "1"},
                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/TerraServerUrbanGatherer.zip",
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0],
                    FriendlyName = "Urban High-Res Imagery (TerraServer)", Name = "TerraServerUrbanGatherer", Version = "1"},
                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/TerraServerTopoGatherer.zip", 
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0], 
                    FriendlyName = "Topographic Map (TerraServer)", Name = "TerraServerTopoGatherer", Version = "1"},
                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/NASAJplGatherer.zip",
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0], 
                    FriendlyName = "Imagery (NASA Landsat 7)", Name = "NASAJPLGatherer", Version = "1"},
                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/OpenStreetMapsGatherer.zip",
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0],
                    FriendlyName = "Street Map (OpenStreetMap)", Name = "OpenStreetMapsGatherer", Version = "1"},

                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/OpenStreetMapRoadNetworkGatherer.6.zip",
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0],
                    FriendlyName = "Road Network (OpenStreetMap)", Name = "OpenStreetMapRoadNetworkGatherer.6", Version = "1"},

                new AddinInformationV1M0 {AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/ElementGeocoder.zip",
                    AddinType = AddinType.Geocoder, DependencyLocations = new string[0],
                    FriendlyName = "Element geocoder", Name = "ElementGeocoder", Version = "1"},
                new AddinInformationV1M0 { AddinDownloadLocation = "https://request.depiction.com/Addins/V1M1/UnitedStatesNationalWeatherGatherer.zip",
                    AddinType = AddinType.ElementGatherer, DependencyLocations = new string[0],
                    FriendlyName = "Weather Forecast (NOAA 24-hour)", Name = "UnitedStatesNationalWeather24HourGathererV2", Version = "2"}
            }.ToList();

        #endregion

        private Dictionary<string, string> getCatalogParameter(string catalogid)
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("catalogid", catalogid);
            return dictionary;
        }

        public LatitudeLongitude DoGeocoding(string addressString)
        {
            string url = string.Format("{0}?appid={1}&location={2}", yahooUri, yahooAppId, addressString);
            var request = (HttpWebRequest)WebRequest.Create(url);

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    return ParseResponse(responseStream);
                }
            }
        }

        private LatitudeLongitude ParseResponse(Stream responseStream)
        {
            if (responseStream != null)
            {
                string lattitude = string.Empty;
                string longitude = string.Empty;

                using (var reader = new XmlTextReader(responseStream))
                {
                    try
                    {
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                if (reader.Name != "Succeeded" && reader.Name != "ResultSet")
                                {
                                    if (reader.Name == "Latitude" || reader.Name == "Longitude")
                                    {
                                        string text = reader.ReadString();
                                        if (reader.Name == "Latitude")
                                        {
                                            lattitude = text;
                                        }
                                        if (reader.Name == "Longitude")
                                        {
                                            longitude = text;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        return null;
                    }
                }

                return new LatitudeLongitude { Latitude = Convert.ToDouble(lattitude), Longitude = Convert.ToDouble(longitude) };
            }

            return null;
        }
    }
}
