﻿using System.Runtime.Serialization;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    [DataContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public class AddinInformation
    {
        [DataMember]
        public string FriendlyName { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string AddinDownloadLocation { get; set; }

        [DataMember]
        public AddinType AddinType { get; set; }
    }
}