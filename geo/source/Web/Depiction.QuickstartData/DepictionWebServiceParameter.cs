﻿using System.Runtime.Serialization;

namespace Depiction.WebServices
{
    [DataContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    [KnownType(typeof(string[]))]
    public class DepictionWebServiceParameter
    {
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public object Value { get; set; }
    }
}