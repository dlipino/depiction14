﻿using System.ServiceModel;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    // NOTE: If you change the interface name "IDepictionWebServiceV1M1" here, you must also update the reference to "IDepictionWebServiceV1M1" in Web.config.
    [ServiceContract(Namespace = "http://request.depiction.com/DepictionWebService")]
    public interface IDepictionWebServiceV1M1
    {
        [OperationContract(Name = "GetRegionCodesForRegion")]
        string[] GetRegionCodesForRegion(BoundingBox region);

        [OperationContract(Name = "GetQuickstartDataByRegionCodes")]
        QuickstartDataV1M0[] GetQuickstartDataByRegionCodes(string[] regionCodes);

        [OperationContract(Name = "GeocodeLocation")]
        LatitudeLongitude GeocodeLocation(string locationToGeocode);

        [OperationContract(Name = "SearchAddinRepository")]
        AddinInformationV1M0 SearchAddinRepository(string addinName, AddinType addinType);
    }
}