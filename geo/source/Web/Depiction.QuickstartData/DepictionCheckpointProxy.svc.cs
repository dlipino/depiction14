﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Depiction.WebServices.CheckpointService;

namespace Depiction.WebServices
{
    // NOTE: If you change the class name "DepictionCheckpointProxy" here, you must also update the reference to "DepictionCheckpointProxy" in Web.config.
    public class DepictionCheckpointProxy : IDepictionCheckpointProxy
    {
        public bool IsProductSNValid(string serialNumber, int productID)
        {
            var checkpointService = new CheckPointServiceClient();
            var record = new TCheckPointSNSerialRecord();
            var response = checkpointService.GetSerialNoRecordBySN(serialNumber, productID, ref record);
            return response.Success;
        }

    }
}
