﻿using System.ServiceModel;
using Depiction.Marketing.DomainModel.Enum;

namespace Depiction.WebServices
{
    [ServiceContract]
    public interface IDepictionMetricsServiceV2
    {
        [OperationContract]
        void LogMetrics(MetricType metricType, string logKey, string details);
    }
}