using System.Runtime.Serialization;

namespace Depiction.WebServices
{
    [DataContract(Namespace = "http://request.depiction.info/DepictionLicensingService")]
    public class TrialLicenseResult
    {
        [DataMember]
        public string ActivationCode { get; set; }

        [DataMember]
        public byte[] LicenseKey { get; set; }

        [DataMember]
        public bool Valid { get; set; }

        [DataMember]
        public string ErrorMessage { get; set; }
    }
}