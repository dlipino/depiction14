﻿using System;
using System.Linq;
using Depiction.Marketing.DomainModel;
using Depiction.Marketing.DomainModel.Enum;
using Depiction.Marketing.DomainModel.Model;
using NHibernate;
using NHibernate.Linq;

namespace Depiction.WebServices
{
    public class DepictionMetricsServiceV2 : IDepictionMetricsServiceV2
    {
        #region IDepictionMetricsService Members

        public void LogMetrics(MetricType metricType, string logKey, string details)
        {
            string metricName = LookupStrings.GetMetricTypeString(metricType);

            using (ISession session = DBFactory.GetSession())
            {
                Lookup metric = session.Linq<Lookup>().SingleOrDefault(l => l.LookupCategory.Category == "Usage Metric" && l.Value == metricName);
                var registration = session.Linq<TrialRegistration>().SingleOrDefault(r => r.ActivationCode == logKey);

                var usageMetric = new UsageMetric
                {
                    DateLogged = DateTime.Now,
                    Details = details,
                    Person = registration == null ? null : registration.Registrant,
                    Metric = metric
                };

                using (ITransaction transaction = session.BeginTransaction())
                {
                    session.Save(usageMetric);
                    transaction.Commit();
                }
            }
        }

        #endregion
    }
}