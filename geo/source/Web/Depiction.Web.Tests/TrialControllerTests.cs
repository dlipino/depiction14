﻿using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Controllers;
using Depiction.Web.Views.SignUp;
using NHibernate;
using NUnit.Framework;

namespace Depiction.Web.Tests
{
    [TestFixture]
    public class TrialControllerTests : BaseWebTest
    {
        [Test]
        public void IndexWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new SignUpController(repository);
            var result = trialController.Index();
            Assert.IsNotNull(result);
            Assert.AreEqual(typeof(RegisterViewData), ((ViewResult)result).ViewData.Model.GetType());

            var registerViewData = (RegisterViewData) ((ViewResult) result).ViewData.Model;

            Assert.Greater(registerViewData.MarketSegments.Length, 0);
            Assert.Greater(registerViewData.Titles.Length, 0);
        }

        [Test]
        public void ConfirmationWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new SignUpController(repository);
            var result = trialController.Confirmation();
            Assert.IsNotNull(result);
        }

        [Test]
        public void RegisterForTrialWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new SignUpController(repository);

            const string email = "unittest@depiction.com";

            var formData = new NameValueCollection {
                {"AdditionalInformation", "Additional information"},
                {"Organization", "organization"},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email},
                {"Title", "title"},
                {"MarketSegment", "1"}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.Register();

            TrialRegistration registration = repository.Queryable<TrialRegistration>().FirstOrDefault();

            Assert.IsNotNull(registration);
            Assert.AreEqual(registration.Registrant.Email, email);
            Assert.AreEqual(((MockResponse) context.HttpContext.Response).RedirectedTo, "/SignUp/Confirmation");
        }
    }
}