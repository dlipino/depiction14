﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using Depiction.Marketing.DomainModel.ExtensionMethods;
using Depiction.Marketing.DomainModel.Model;
using Depiction.Marketing.DomainModel.Repository;
using Depiction.Web.Controllers;
using Depiction.Web.Helpers;
using Depiction.Web.Helpers.Exceptions;
using Depiction.Web.Helpers.Interfaces;
using Depiction.Web.Views.User;
using NHibernate;
using NUnit.Framework;

namespace Depiction.Web.Tests
{
    [TestFixture]
    public class UserControllerTests : BaseWebTest
    {
        [Test]
        public void IndexWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var userController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());
            ActionResult result = userController.Index();
            Assert.IsNotNull(result);
        }

        [Test]
        public void LoginWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var userController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());
            ActionResult result = userController.Login();
            Assert.IsNotNull(result);
        }

        [Test]
        public void RegisterWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var userController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());
            ActionResult result = userController.Register();
            Assert.IsNotNull(result);
        }

        [Test]
        public void RegisteredWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var userController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());
            ActionResult result = userController.Registered();
            Assert.IsNotNull(result);
        }

        [Test]
        public void SavedWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var userController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());
            ActionResult result = userController.Saved();
            Assert.IsNotNull(result);
        }

        [Test]
        [ExpectedException(typeof(UserControllerException))]
        public void SaveUserWithoutMatchingPasswordFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", postingTo.ComputeHash(timeStamp)},
                {"Password", "Additional information"},
                {"ConfirmPassword", "organization"},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(0);
        }

        [Test]
        [ExpectedException(typeof(UserControllerException))]
        public void SaveUserRegisterWithNoPasswordFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", postingTo.ComputeHash(timeStamp)},
                {"Password", string.Empty},
                {"ConfirmPassword", string.Empty},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(0);
        }

        [Test]
        [ExpectedException(typeof(FormTamperingException))]
        public void SaveUserWithTamperedFormFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", "invalid key"},
                {"Password", "Additional information"},
                {"ConfirmPassword", "organization"},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(0);
        }

        [Test]
        [ExpectedException(typeof(UserControllerException))]
        public void SaveUserRegisteringWithNoPasswordFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", postingTo.ComputeHash(timeStamp)},
                {"Password", ""},
                {"ConfirmPassword", ""},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(0);
        }

        [Test]
        [ExpectedException(typeof(UserControllerException))]
        public void SaveUserRegisteringWithExistingEmailAddressFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", postingTo.ComputeHash(timeStamp)},
                {"Password", "password"},
                {"ConfirmPassword", "password"},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email},
                {"Role", "1"}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(0);

            User user = Enumerable.FirstOrDefault(repository.Queryable<User>());

            Assert.IsNotNull(user);
            Assert.AreEqual(user.Person.Email, email);
            trialController.SaveUser(0);
        }

        [Test]
        [ExpectedException(typeof(UserControllerException))]
        public void SaveUserWithExistingEmailAddressFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            repository.Save(new User
            {
                Password = "blah",
                Person = new Person
                {
                    FirstName = "Test",
                    LastName = "Test",
                    Email = email
                }
            });

            repository.Save(new User
            {
                Password = "blah",
                Person = new Person
                {
                    FirstName = "Test",
                    LastName = "Test",
                    Email = "new@email.com"
                }
            });

            User user = Enumerable.LastOrDefault(repository.Queryable<User>());

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", postingTo.ComputeHash(timeStamp)},
                {"Password", "password"},
                {"ConfirmPassword", "password"},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email},
                {"Role", "1"}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(user.Id);
        }

        [Test]
        public void SaveUserWorksForRegisterAndAdmin()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var postingTo = "/User/SaveUser/0";
            var timeStamp = DateTime.Now;

            var formData = new NameValueCollection {
                {"PostingTo", postingTo},
                {"TimeStamp", timeStamp.ToString()},
                {"FormKey", postingTo.ComputeHash(timeStamp)},
                {"Password", "password"},
                {"ConfirmPassword", "password"},
                {"FirstName", "first name"},
                {"LastName", "last name"},
                {"Email", email},
                {"Role", "1"}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(0);

            User user = Enumerable.FirstOrDefault(repository.Queryable<User>());

            Assert.IsNotNull(user);
            Assert.AreEqual(user.Person.Email, email);
            Assert.IsNull(user.Roles);
            Assert.AreEqual(((MockResponse)context.HttpContext.Response).RedirectedTo, "~/User/Registered");

            string password = user.Password;

            formData["Password"] = string.Empty;
            formData["ConfirmPassword"] = string.Empty;

            context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(user.Id);

            user = Enumerable.FirstOrDefault(repository.Queryable<User>());

            Assert.AreEqual(password, user.Password);
            Assert.AreEqual(1, user.Roles.Count);
            Assert.AreEqual(((MockResponse)context.HttpContext.Response).RedirectedTo, "~/User/Saved");

            formData["Password"] = "new password";
            formData["ConfirmPassword"] = "new password";

            context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.SaveUser(user.Id);

            user = Enumerable.FirstOrDefault(repository.Queryable<User>());

            Assert.AreNotEqual(password, user.Password);
        }

        [Test]
        public void EditWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            repository.Save(new User
            {
                Password = "blah",
                Person = new Person
                {
                    FirstName = "Test",
                    LastName = "Test",
                    Email = "test@test.com"
                }
            });

            User user = Enumerable.FirstOrDefault(repository.Queryable<User>());

            ControllerContext context = getMockContext(null);

            trialController.ControllerContext = context;
            var result = trialController.Edit(user.Id);

            Assert.IsNotNull(result);

            var editViewData = (EditUserData)((ViewResult)result).ViewData.Model;

            Assert.AreEqual(user.Id, editViewData.User.Id);
            Assert.Greater(editViewData.AllRoles.Length, 0);
        }

        [Test]
        [ExpectedException(typeof(UserControllerException))]
        public void DoLoginForNonExistantAccountFails()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            var formData = new NameValueCollection {
                {"Email", email},
                {"Password", "password"}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.DoLogin();
        }

        [Test]
        public void DoLoginWorks()
        {
            var repository = new BaseRepository(containerProvider.ApplicationContainer.Resolve<ISession>());
            var trialController = new UserController(repository, containerProvider.ApplicationContainer.Resolve<IAuthorizer>());

            const string email = "unittest@depiction.com";

            repository.Save(new User
            {
                Password = "password".PasswordEncode(),
                Person = new Person
                {
                    FirstName = "Test",
                    LastName = "Test",
                    Email = email
                }
            });

            var formData = new NameValueCollection {
                {"Email", email},
                {"Password", "password"}
            };

            ControllerContext context = getMockContext(formData);

            trialController.ControllerContext = context;
            trialController.DoLogin();

            Assert.AreEqual(((MockResponse)context.HttpContext.Response).RedirectedTo, "~/Home/Dashboard");
        }
    }
}