using System.IO;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.Depiction12Deserializers.Subtrees
{
    public abstract class ElementSubtreeLoaderBase : BaseSubtreeLoader
    {

        protected override void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            var elementRepository = Deserializers12.DeserializeFromFolder<IElementRepository12>("ElementRepository", stream, subtreeInfo.DataPath);
            SetElementRepository(memento.Story, elementRepository);
        }

        protected abstract IElementRepository12 GetElementRepository(IStory12 story);
        protected abstract void SetElementRepository(IStory12 story, IElementRepository12 repository);

    }
}