using System.IO;
using Depiction.DPNPorting.SerializationObjects12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.Depiction12Deserializers.Subtrees
{
    public class ElementSubtreeLoader : BaseSubtreeLoader
    {

        protected override void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            memento.Story.ElementRepository = Deserializers12.DeserializeFromFolder<ElementRepository>("ElementRepository", stream, subtreeInfo.DataPath);
        }
    }
}