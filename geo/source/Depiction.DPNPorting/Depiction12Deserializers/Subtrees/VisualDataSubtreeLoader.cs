using System;
using System.IO;
using Depiction.DPNPorting.SerializationObjects12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.Depiction12Deserializers.Subtrees
{
    public class VisualDataSubtreeLoader: BaseSubtreeLoader
    {
        protected override void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            memento.VisualData = Deserializers12.DeserializeFromFolder<VisualData>("VisualData", stream, subtreeInfo.DataPath);
        }
    }
}