using System.IO;
using Depiction.DPNPorting.SerializationObjects12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.Depiction12Deserializers.Subtrees
{
    public class StorySubtreeLoader : BaseSubtreeLoader
    {
        #region ISubtreeLoader Members


        protected override void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            memento.Story = Deserializers12.DeserializeFromFolder<Story>("Story", stream, subtreeInfo.DataPath);
        }

        #endregion
    }
}