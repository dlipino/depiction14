using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Depiction.DPNPorting.Depiction12Deserializers.Subtrees;
using Depiction.DPNPorting.SerializationObjects12;
using Depiction.Serialization;
using Ionic.Zip;
using UnregisteredSubtreeLoader=Depiction.DPNPorting.Depiction12Deserializers.Subtrees.UnregisteredSubtreeLoader;


namespace Depiction.DPNPorting.Depiction12Deserializers
{
    public class StoryPersistence12
    {
        #region Story persistence

        public const string TempSaveDirectory = "tempSaveDir";
        public const string StoryFilename = "storyMemento.xml";
        public const string DepictionInfoFilename = "depictionMetadataMemento.xml";
        public const string OldDepictionName = "Depiction 1.2";
        #endregion Story persistence

        // THESE TWO VARIABLES MUST BE INCREMENTED TOGETHER! basically this is the 8th file format change.
        public const int CurrentDepictionFileVersion = 10;
        // The Application version in which this DepictionFileVersion was introduced.
        private const string MinimumAppVersionForDepictionFileVersion = "1.2.2";
        //Variables that should set road network/elevation changes to the state they were before the
        //save since versions less than 5 did not save the change.
        public static int OriginalVersionOfCurrentLoadedDepiction { get; private set; }
        public const int VersionsThatMustRunInteractionsAfterLoading = 5;//Don't change this one
        public const int VersionThatGavePositionToShapes = 9;//8, there was a bug somewhere which made this not work;//Don't change this one
        public const int DepictionRoadgraphWithOneWay = 9; //Or this one
        public const int DepictionAreaAndPerimeter = 10; //Or this one

        static public int PercentOfOperationCompleted { get; set; }
        #region Human readable names for subtrees
//        private static List<StorySubtreeInfo> GetStorySubtrees()
//        {
//            var subtrees = new List<StorySubtreeInfo>
//                               {
//                                   new StorySubtreeInfo("Story", "Depiction.VisualModel.Loaders.StorySubtreeLoader", "",
//                                                        "Story.xml"),
//                                   new StorySubtreeInfo("VisualData",
//                                                        "Depiction.VisualModel.Loaders.VisualDataSubtreeLoader", "",
//                                                        "VisualData.xml"),
//                                   new StorySubtreeInfo("InteractionRules",
//                                                        "Depiction.VisualModel.Loaders.InteractionRulesSubtreeLoader",
//                                                        "", "InteractionRules.xml"),
//                                   new StorySubtreeInfo("Images",
//                                                        "Depiction.VisualModel.Loaders.RasterImagesSubtreeLoader",
//                                                        "Depiction.DomainModel.Resources.RasterImagesMemento.xsd",
//                                                        "Images.xml"),
//                                   new StorySubtreeInfo("ElementTypes",
//                                                        "Depiction.VisualModel.Loaders.ElementTypesSubtreeLoader", "",
//                                                        ""),
//                                   new StorySubtreeInfo("Elements", "Depiction.VisualModel.Loaders.ElementSubtreeLoader",
//                                                        "", "Elements.xml"),
//                                   new StorySubtreeInfo("UnregisteredElements",
//                                                        "Depiction.VisualModel.Loaders.UnregisteredElementSubtreeLoader",
//                                                        "", "UnregisteredElements.xml"),
//                                   new StorySubtreeInfo("InteractionGraphElements",
//                                                        "Depiction.VisualModel.Loaders.InteractionGraphElementsSubtreeLoader",
//                                                        "", "InteractionGraphElements.xml")
//
//                               };
//
//            return subtrees;
//        }
        #endregion

        private static Helper12DeserializationClasses.ISubtreeLoader FindSubtreeLoader(string typeName)
        {
            Type type = null;// Type.GetType(typeName);
            if(typeName.Contains("StorySubtreeLoader"))
            {
                type = typeof (StorySubtreeLoader);
            }
            else if (typeName.Contains("UnregisteredElementSubtreeLoader"))
            {
                type = typeof(UnregisteredSubtreeLoader);
            }
            else if (typeName.Contains("ElementSubtreeLoader"))
            {
                type = typeof(ElementSubtreeLoader);
            }
            else if (typeName.Contains("VisualDataSubtreeLoader"))
            {
                type = typeof(VisualDataSubtreeLoader);
            }
            else if (typeName.Contains("RasterImagesSubtreeLoader"))
            {
                type = typeof(RasterImagesSubtreeLoader);
            }
            
            
            if (type != null)
            {
                var types = new Type[0];
                ConstructorInfo constructorInfo = type.GetConstructor(types);
                if (constructorInfo != null)
                    return constructorInfo.Invoke(null) as Helper12DeserializationClasses.ISubtreeLoader;
            }
            return null;
        }

        /// <summary>
        /// Returns story loaded from file, and sets DomainAccess.VisualData.
        /// 
        /// If fails, returns null;
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="worker"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static Helper12DeserializationClasses.PersistenceMemento12 LoadUnzipped122StoryFromFolder(string folderLocation, string fileName, Dictionary<string, Type> stringToTypeDictionary)
        {
            
            var archiveDir = folderLocation;// Path.Combine(folderLocation, TempSaveDirectory);
            //On every load, assume the loaded depictions comes from the current version (in case of errors)
            OriginalVersionOfCurrentLoadedDepiction = CurrentDepictionFileVersion;

            var persistenceMemento = new Helper12DeserializationClasses.PersistenceMemento12();
            var storyPersistenceInfo = GetStoryPersistenceInfo(archiveDir);
            if (storyPersistenceInfo == null)
            {
                throw new Exception(String.Format(
                                        "File \"{0}\" is not a valid {1} file.\n\nPlease select a valid {1} file.",
                                        fileName, "Depiction 1.2"));
            }

            //Remember the version of the depiction that is currently getting loaded
            OriginalVersionOfCurrentLoadedDepiction = storyPersistenceInfo.DepictionFileVersion;


//            // See if we need to import from one file version to the current
//            if (CurrentDepictionFileVersion < OriginalVersionOfCurrentLoadedDepiction)
//            {
//                // Need newer version of Depiction.
//                var message =
//                    string.Format(
//                        "Cannot open {2} file {0}\n\nThis file was created by a more recent version of {3}. To open it, please upgrade your {3} to at least version {1}. See Help for how to upgrade.",
//                        fileName,
//                        storyPersistenceInfo.MinimumAppVersionForDepictionFileVersion, UIAccess._productInformation.StoryName,
//                        UIAccess._productInformation.ProductName);
//                if (DomainAccess.IsTrialMode)
//                {
//                    message =
//                        string.Format(
//                            "Cannot open {1} file {0}\n\nThis file was created by a more recent version of {2}. To open it, please get the latest version of the Depiction Reader.",
//                            fileName, UIAccess._productInformation.StoryName,
//                            UIAccess._productInformation.ProductName);
//                }
//                throw new Exception(message);
//            }
//            if (CurrentDepictionFileVersion > OriginalVersionOfCurrentLoadedDepiction)
//            {
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 3, string.Format("Updating {0} to latest version...", UIAccess._productInformation.StoryName), e)) return null;
//                // Update the DPN file.
//                try
//                {
//                    archiveDir = ImportToLatestDpnVersion(archiveDir, OriginalVersionOfCurrentLoadedDepiction);
//                    var message =
//                        string.Format(
//                            "This {0} file was created by an older version of {1}. When saved, it will not load on older versions.",
//                            UIAccess._productInformation.StoryName, UIAccess._productInformation.ProductName);
//                    if (DomainAccess.IsTrialMode)
//                    {
//                        message = string.Format("The {0} file was created by an older Depiction version",
//                                                UIAccess._productInformation.StoryName);
//                    }
//                    DepictionAccess.NotificationService.SendNotification(message, 20);
//                }
//                catch (Exception ex)
//                {
//                    throw new Exception(string.Format("Could not convert {2} file \"{0}\" to current file format.\n\nInternal error: {1}",
//                                                      fileName, ex.Message, UIAccess._productInformation.StoryName));
//                }
//            }

            try
            {
                LoadStorySubtrees(persistenceMemento, storyPersistenceInfo.Subtrees, archiveDir, stringToTypeDictionary);//, worker, e);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Error opening {2} file \"{0}\"\n\nInternal error: {1}",
                                                  fileName, ex.Message, OldDepictionName));
            }
//            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Retrieving visual data...", e)) return null;
            var oldVisualData = persistenceMemento.VisualData;
//            DomainAccess.VisualData = persistenceMemento.VisualData;
//            DomainAccess.Router = new Router(persistenceMemento.Story.ElementRepository, persistenceMemento.Story.InteractionRuleRepository);
//            DomainAccess.Router.BeginAsync();
//            DomainAccess.Router.ElementRulePairs = persistenceMemento.ElementRulePairs;
            if (oldVisualData != null)
            {

                var elements = persistenceMemento.Story.ElementRepository.AllElements;
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Setting backdrop...", e)) return null;
                //if (oldVisualData.PrimaryDisplayer != null)
                 //   oldVisualData.PrimaryDisplayer.RestoreElements(elements);
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Setting revealers...", e)) return null;
                //foreach (var displayer in oldVisualData.RevealerCollection)
               // {
                //    displayer.RestoreElements(elements);
               // }
            }
//            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted += 1, "Setting up visuals...", e)) return null;
            Directory.Delete(archiveDir, true);
            return persistenceMemento;
        }

//        private static string ImportToLatestDpnVersion(string archiveDir, int depictionFileVersion)
//        {
//            var importFilter =
//                DepictionImportFilter.CreateImportFilterFromVersionXToCurrentVersion(depictionFileVersion, CurrentDepictionFileVersion);
//            string homeDir = archiveDir;
//
//            //Only do filter actions if there is more than one thing to do.  Sometimes we just want to give
//            //a good message when a user tries to open a newer version in an older version.
//            if (importFilter != null)
//            {
//                //Do nothing if there are no actions
//                if (importFilter.actions.Count == 0) return homeDir;
//
//                var importedArchiveDir = Path.Combine(DepictionAccess.ApplicationPathService.TempFolderPath, "importDirectory");
//                //Copy stuff over
//                FileSystemHelper.CopyDirectory(homeDir, importedArchiveDir, true);
//                //Do the import stuff, usually implies taking stuff from the homeDir, changing them then passing them to
//                //the importedArchiveDir (this might explain duplicate elements sometimes)
//                importFilter.DoImport(homeDir, importedArchiveDir);
//                return importedArchiveDir;
//            }
//
//            throw new Exception("Import filter not found");
//        }

        /// <summary>
        /// Returns the story persistent info.
        /// Returns null if "StoryInfo" directory does not exist.
        /// </summary>
        /// <param name="archiveDir"></param>
        /// <returns></returns>
        private static StoryPersistenceInfo GetStoryPersistenceInfo(string archiveDir)
        {
            string filePath = Path.Combine(archiveDir, StoryFilename);
            if (!File.Exists(filePath)) return null;
            return Deserializers12.LoadFromXmlFile<StoryPersistenceInfo>(filePath, "StoryInfo");
        }

        public static void UnzipFileToDirectory(string fileName, string archiveDir)
        {
            if (Directory.Exists(archiveDir))
                Directory.Delete(archiveDir, true);
            if (!File.Exists(fileName)) return;
//            string tempName = fileName;
            bool deleteTemp = false;
//            var fileExtension = Path.GetExtension(fileName);
//            var needToUnEncrypt = true;
//            if (fileExtension.Equals(new DepictionProductInformation().FileExtension) || string.IsNullOrEmpty(UIAccess._productInformation.FilePassword))
//                needToUnEncrypt = false;
//            if (needToUnEncrypt)
//            {
//                tempName = GetZipFromInAZip(fileName, UIAccess._productInformation.FilePassword);
//                //Since i don't know exactly what happens if the GetZipFromInAZip goes bad, and we need
//                //to delete the temp file. We must make sure that the temp file is not the original.
//                if (!string.IsNullOrEmpty(tempName) && !tempName.Equals(fileName))
//                {
//                    deleteTemp = true;
//                }
//                else
//                {
//                    throw new Exception(String.Format(
//                                            "File \"{0}\" is not a valid {1} file.\n\nPlease select a valid {1} file.",
//                                            fileName, UIAccess._productInformation.StoryName));
//                }
//            }
//            var decompressor = new FastZip { CreateEmptyDirectories = true, RestoreDateTimeOnExtract = true, RestoreAttributesOnExtract = true };
////            if (needToUnEncrypt)
////            {
////                decompressor.Password = UIAccess._productInformation.FilePassword;
////            }
//            decompressor.ExtractZip(tempName, archiveDir, "");

            using (var zip = new ZipFile(fileName))
            {
                zip.ExtractAll(archiveDir);
            }
            if (deleteTemp && File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }
//        #region Stuff for removing images taht are not used
//
//        private static ResourceDictionary RemoveUnusedImagesFromResources()
//        {
//            var imageNames = new List<string>();
//            foreach (var elem in CommonData.Story.UnregisteredElementRepository.AllElements)
//            {
//                var name = elem.ImagePath;
//                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
//                {
//                    imageNames.Add(name);
//                }
//            }
//            foreach (var elem in CommonData.Story.ElementRepository.AllElements)
//            {
//                var name = elem.ImagePath;
//                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
//                {
//                    imageNames.Add(name);
//                }
//            }
//            foreach (var elem in CommonData.Story.PrototypeRepository.AvailableElementPrototypes)
//            {
//                var name = elem.ImagePath;
//                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
//                {
//                    imageNames.Add(name);
//                }
//            }
//
//            var matchingRD = new RasterImageResourceDictionary();
//            foreach (object keyObject in CommonData.Story.BitmapResources.Keys)
//            {
//                var keyString = keyObject as string;
//
//                if (!string.IsNullOrEmpty(keyString) && imageNames.Contains(keyString))
//                {
//                    matchingRD.Add(keyObject, CommonData.Story.BitmapResources[keyObject]);
//                }
//            }
//
//            //            var usedImages = CommonData.Story.BitmapResources.Where(images => (imageNames.Contains(images.Key)));
//            return matchingRD;
//
//        }
//        #endregion

//        static public bool ReportProgressCheckForCancel(BackgroundWorker worker, int progress, string report, DoWorkEventArgs e)
//        {
//            if (worker != null)
//            {
//                if (e != null)
//                {
//                    if (worker.CancellationPending)
//                    {
//                        e.Cancel = true;
//                        return true;
//                    }
//                }
//                if (report != null)
//                    worker.ReportProgress(progress, report);
//                else
//                    worker.ReportProgress(progress);
//
//            }
//            return false;
//        }

//        public void SaveDepictionToFile(IVisualData visualData, IStory story, ElementTupleList elementRulePairs, string fileName, BackgroundWorker worker, DoWorkEventArgs e)
//        {
//            SaveStoryToFile(visualData, story, elementRulePairs, fileName, worker, e);
//        }

//        private static void SaveStoryToFile(IVisualData visualData, IStory story, ElementTupleList elementRulePairs, string fileName, BackgroundWorker worker, DoWorkEventArgs e)
//        {
//            string archiveDir = CreateArchiveDirectory();
//            var persistenceMemento = new PersistenceMemento12 { Story = story, ElementRulePairs = elementRulePairs };
//
//            PercentOfOperationCompleted = 1;
//            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Removing unused images...", e)) return;
//
//            //Remove all unused images before saving (davidl)
//            ((Story)persistenceMemento.Story).BitmapResources = RemoveUnusedImagesFromResources();
//            persistenceMemento.VisualData = visualData;
//            PercentOfOperationCompleted = 5;
//            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Starting save...", e)) return;
//
//            SaveStoryToArchive(persistenceMemento, archiveDir, worker, e);
//
//            story.NeedSaving = false;
//
//            if (File.Exists(fileName))
//            {
//                File.Delete(fileName);
//                PercentOfOperationCompleted += 1;
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Deleting old file...", e)) return;
//            }
//
//            PercentOfOperationCompleted += 1;
//            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Compressing file...", e)) return;
//
//
//            var fastZip = new FastZip
//                              {
//                                  CreateEmptyDirectories = true,
//                                  RestoreDateTimeOnExtract = true,
//                                  RestoreAttributesOnExtract = true
//                              };
//            if (!string.IsNullOrEmpty(UIAccess._productInformation.FilePassword))
//            {
//                fastZip.Password = UIAccess._productInformation.FilePassword;
//            }
//            fastZip.CreateZip(fileName, archiveDir, true, "");
//            if (!string.IsNullOrEmpty(UIAccess._productInformation.FilePassword))
//            {
//                PutZipIntoAZip(fileName, UIAccess._productInformation.FilePassword);
//            }
//
//            WebServiceFacade.LogUsageMetric(MetricType.Saved, fileName.Substring(fileName.LastIndexOf('\\') + 1).MD5Hash());
//
//            if (ReportProgressCheckForCancel(worker, 100, "Save complete.", e)) return;
//        }

//        //Only designed for a single file!!!! not for whole directories. (davidl)
//        static private string GetZipFromInAZip(string fileName, string password)
//        {
//            var inputStream = new ZipInputStream(File.OpenRead(fileName));
//            var targetDir = Path.GetDirectoryName(Path.GetFullPath(fileName));
//            var tempFullName = fileName;
//            var tempName = Path.GetFileName(Path.GetTempFileName());
//            #region Silly entry counting thing
//            try
//            {
//                if (!string.IsNullOrEmpty(password))
//                {
//                    inputStream.Password = password;
//                }
//                //Make sure that this zip file only has one entry, the stupid way
//                var count = 0;
//                while ((inputStream.GetNextEntry()) != null && count < 3)
//                {
//                    count++;
//                }
//                if (count != 1) return fileName;
//            }
//            finally
//            {
//                inputStream.Close();
//            }
//            #endregion
//
//            //            var decompressor = new FastZip { CreateEmptyDirectories = true, RestoreDateTimeOnExtract = true, RestoreAttributesOnExtract = true };
//            //            tempFullName = Path.Combine(targetDir, tempName);
//            //            if (!string.IsNullOrEmpty(UIAccess._productInformation.FilePassword))
//            //            {
//            //                decompressor.Password = UIAccess._productInformation.FilePassword;
//            //            }
//            //            decompressor.ExtractZip(tempName, targetDir, "");
//
//            inputStream = new ZipInputStream(File.OpenRead(fileName));
//            try
//            {
//                if (!string.IsNullOrEmpty(password))
//                {
//                    inputStream.Password = password;
//                }
//
//                while (inputStream.GetNextEntry() != null)
//                {
//                    tempFullName = Path.Combine(targetDir, tempName);
//                    FileStream streamWriter = File.Create(tempFullName);
//                    try
//                    {
//                        var buffer = new byte[4096];
//                        int size;
//                        do
//                        {
//                            size = inputStream.Read(buffer, 0, buffer.Length);
//                            streamWriter.Write(buffer, 0, size);
//                        } while (size > 0);
//                    }
//                    finally
//                    {
//                        streamWriter.Close();
//                    }
//                }
//            }
//            finally
//            {
//                inputStream.Close();
//            }
//            return tempFullName;
//        }

//        //Only designed for a single file!!!! not for whole directories. (davidl)
//        static private void PutZipIntoAZip(string fileName, string password)
//        {
//            // Zip up the files - From SharpZipLib Demo Code (more or less)
//            var tempFileName = Path.GetTempFileName();
//            using (ZipOutputStream s = new ZipOutputStream(File.Create(tempFileName)) { Password = password })
//            {
//                s.SetLevel(9); // 0-9, 9 being the highest compression
//                byte[] buffer = new byte[4096];
//                ZipEntry entry = new ZipEntry(Path.GetFileName(fileName));
//                entry.DateTime = DateTime.Now;
//                s.PutNextEntry(entry);
//
//                using (FileStream fs = File.OpenRead(fileName))
//                {
//                    int sourceBytes;
//                    do
//                    {
//                        sourceBytes = fs.Read(buffer, 0, buffer.Length);
//                        s.Write(buffer, 0, sourceBytes);
//                    }
//                    while (sourceBytes > 0);
//                }
//                s.Finish();
//                s.Close();
//            }
//            try
//            {
//                File.Copy(tempFileName, fileName, true);
//            }
//            catch (Exception)
//            {
//                File.Delete(tempFileName);
//                return;
//            }
//            File.Delete(tempFileName);
//        }

        #region Save Helper Methods

//        private static void SaveStoryToArchive(PersistenceMemento dataToSave, string archiveDir, BackgroundWorker worker, DoWorkEventArgs e)
//        {
//            SaveDepictionPersistenceMemento(dataToSave, archiveDir, worker, e);
//            var storyPersistenceInfo = new StoryPersistenceInfo
//                                           {
//                                               Author = Settings.Default.Author,
//                                               SaveDateTime = DateTime.Now,
//                                               DepictionFileVersion = CurrentDepictionFileVersion,
//                                               MinimumAppVersionForDepictionFileVersion = MinimumAppVersionForDepictionFileVersion,
//                                               AppVersion = VersionInfo.AppVersion,
//                                               Subtrees = GetStorySubtrees()
//                                           };
//
//            string storyInfoFilename = Path.Combine(archiveDir, StringConstants.StoryFilename);
//            PercentOfOperationCompleted += 1;
//            if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, "Saving version info...", e)) return;
//            SerializationService.SaveToXmlFile(storyPersistenceInfo, storyInfoFilename, "StoryInfo");
//        }

        /// <summary>
        /// Provide a user-friendly name for the subtree, instead of the internal name we use in the .dpn file.
        /// </summary>
        /// <param name="subtreeName"></param>
        /// <returns></returns>
        private static string FriendlyNameForSubtree(string subtreeName)
        {
            var friendlyNamesForSubtrees =
                new Dictionary<string, string>
                    {
                        {"ElementTypes", "element definitions"},
                        {"VisualData", "revealers"},
                        {"InteractionRules", "interactions"},
                        {"Images", "images"},
                        {"Elements", "elements"},
                        {"UnregisteredElements", "un-geo-aligned elements"},
                        {"Story", "depiction"},//UIAccess._productInformation.StoryName},
                        {"InteractionGraphElements", "modified elements"},
                    };
#if (DEBUG)
            if (!friendlyNamesForSubtrees.ContainsKey(subtreeName))
                throw new Exception(string.Format("No friendly name for \"{0}\"", subtreeName));
#endif
            return friendlyNamesForSubtrees[subtreeName] ?? subtreeName;
        }

//        private static void SaveDepictionPersistenceMemento(PersistenceMemento12 dataToSave, string archiveDir, BackgroundWorker worker, DoWorkEventArgs e)
//        {
//            var subTrees = GetStorySubtrees();
//            const int totalSaveToComplete = 85;
//            int subTreeCount = subTrees.Count;
//            if (subTreeCount == 0) subTreeCount = 1;
//            int increment = totalSaveToComplete / subTreeCount;
//
//            foreach (var subtree in subTrees)
//            {
//                var report = string.Format("Saving {0}...", FriendlyNameForSubtree(subtree.DataPath));
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, report, e)) return;
//
//                ISubtreeLoader loader = FindSubtreeLoader(subtree.LoaderTypeName);
//                if (loader != null)
//                {
//                    string fullDataPath = Path.Combine(archiveDir, subtree.DataPath);
//                    if (!Directory.Exists(fullDataPath))
//                        Directory.CreateDirectory(fullDataPath);
//                    loader.SaveSubtree(dataToSave,
//                                       new SubtreeInfo12
//                                           {
//                                               DataPath = fullDataPath,
//                                               XMLFileName = subtree.XmlFileName,
//                                               SchemaResourcePath = subtree.SchemaResourcePath
//                                           });
//                }
//                PercentOfOperationCompleted += increment;
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, null, e)) return;
//            }
//
//            string depictionMetadataFilename = Path.Combine(archiveDir, StringConstants.DepictionInfoFilename);
//            SerializationService.SaveToXmlFile(dataToSave.Story.Metadata, depictionMetadataFilename, "DepictionInfo");
//        }
//
        #endregion



        #region Load Helper Methods

        /// <summary>
        /// Loads all of the subtrees / folders in this .dpn file.
        /// Caller must catch any exceptions.
        /// </summary>
        /// <param name="dataToLoad"></param>
        /// <param name="subtreeList"></param>
        /// <param name="archiveDir"></param>
        /// <param name="worker"></param>
        /// <param name="e"></param>
        private static void LoadStorySubtrees(Helper12DeserializationClasses.PersistenceMemento12 dataToLoad, ICollection<StorySubtreeInfo> subtreeList, string archiveDir,
                                              Dictionary<string, Type> allowedTypeDictionary)
        {
            Deserializers12.stringToTypeDictionary = allowedTypeDictionary;//This is the hacked needed for converting from 1.2 to 1.3W
//            const int totalLoadComplete = 85;
//            int subTreeCount = subtreeList.Count;
//            if (subTreeCount == 0) subTreeCount = 1;
//            int increment = totalLoadComplete / subTreeCount;
            foreach (var subtree in subtreeList)
            {
//                var report = string.Format("Loading {0}...", FriendlyNameForSubtree(subtree.DataPath));
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, report, e)) return;
                var loader = FindSubtreeLoader(subtree.LoaderTypeName);
                if (loader != null)
                {
                    string fullDataPath = Path.Combine(archiveDir, subtree.DataPath);
                    var subtreeInfo = new Helper12DeserializationClasses.SubtreeInfo12
                                          {
                                              DataPath = fullDataPath,
                                              XMLFileName = subtree.XmlFileName,
                                              SchemaResourcePath = subtree.SchemaResourcePath
                                          };

                    loader.LoadSubtree(dataToLoad, subtreeInfo);
                }
//                PercentOfOperationCompleted += increment;
//                if (ReportProgressCheckForCancel(worker, PercentOfOperationCompleted, null, e)) return;
            }
            string filePath = Path.Combine(archiveDir, DepictionInfoFilename);

            if (File.Exists(filePath))
            {
                dataToLoad.Story.Metadata = Deserializers12.LoadFromXmlFile<DepictionMetadata>(filePath, "DepictionInfo");
            }
        }

        #endregion
    }
}