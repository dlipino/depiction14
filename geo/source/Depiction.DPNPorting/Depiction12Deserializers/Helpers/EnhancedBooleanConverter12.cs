﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace Depiction.DPNPorting.Depiction12Deserializers.Helpers
{
    /// <summary>
    /// Extend normal BooleanConverter to handle "Yes", "No", "Y", "N", "T", "F".
    /// </summary>
    public class EnhancedBooleanConverter12 : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(bool));
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            if (value is string)
            {
                var result = Parse((string)value);
                if (result == null)
                {
                    throw new Exception(string.Format("\"{0}\" is not a valid boolean value.", value));
                }
                return result;
            }
            return base.ConvertFrom(context, info, value);
        }

        public static object Parse(string s)
        {
            var valueStr = s.Trim();
            switch (valueStr.ToLower())
            {
                case "yes":
                case "y":
                case "t":
                    return true;
                case "no":
                case "n":
                case "f":
                    return false;
            }
            bool boolValue;
            if (bool.TryParse(valueStr, out boolValue))
                return boolValue;
            return null;
        }
    }
}