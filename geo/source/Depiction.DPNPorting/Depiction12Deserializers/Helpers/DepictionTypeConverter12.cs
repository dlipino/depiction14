using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Media;
using Depiction.API.ValueTypes;

namespace Depiction.DPNPorting.Depiction12Deserializers.Helpers
{
    /// <summary>
    /// Converts one type to another
    /// </summary>
    public static class DepictionTypeConverter12
    {
        /// <summary>
        /// Changes the type.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="conversionType">Type of the conversion.</param>
        /// <returns></returns>
        public static object ChangeType(object value, Type conversionType)
        {
            if (conversionType == null)
                return null;
            if (value.GetType().Equals(conversionType))
                return value;
            if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                var nullableConverter = new NullableConverter(conversionType);
                conversionType = nullableConverter.UnderlyingType;
            }
            if (value is string && conversionType == typeof(LatitudeLongitude))
            {
                string[] latLonString = ((string)value).Split(',');

                if (latLonString.Length == 2)
                {
                    double latD;
                    double longD;
                    if (double.TryParse(latLonString[0], out latD) && double.TryParse(latLonString[1], out longD))
                    {
                        return new LatitudeLongitude(latD, longD);
                    }
                    throw new Exception(string.Format("Cannot convert string: {0} to a Latitude Longitude", value));
                    //return new LatitudeLongitude(LatitudeLongitude.INVALID_VALUE, LatitudeLongitude.INVALID_VALUE);
                }
                return null;
            }
            if (value is string && conversionType == typeof(Color))
            {
                try
                {
                    return (Color)ColorConverter.ConvertFromString((string)value);
                }
                catch
                {
                    throw new Exception(string.Format("Value \"{0}\" is not a valid color.", value));
                }
            }
            if (value is string && conversionType.IsEnum)
            {
                try
                {
                    return Enum.Parse(conversionType, (string)value);
                }
                catch (ArgumentException)
                {
//                    throw new InvalidElementFileEnumException(string.Format(
//                                                                  "Value \"{0}\" is not defined for enum \"{1}\"", value,
//                                                                  conversionType.FullName));
                }
            }
            if (value is string && conversionType == typeof(bool))
            {
                try
                {
                    return EnhancedBooleanConverter12.Parse((string)value);
                }
                catch
                {
                    throw new Exception(string.Format("Value \"{0}\" is not a valid boolean.", value));
                }
            }

            //.NET's Int32Converter doesn't allow thousands separators, so we special case and use int.Parse
            if (value is string && conversionType == typeof(int))
            {
                return int.Parse((string)value, NumberStyles.AllowThousands, CultureInfo.CurrentCulture);
            }
            //find destination type converter
            var converter = TypeDescriptor.GetConverter(conversionType);

            if (converter != null && value is string && converter.CanConvertFrom(typeof(string)))
            {

                return converter.ConvertFromInvariantString((string)value);
            }
            if (converter != null && converter.CanConvertFrom(value.GetType()))
            {
                return converter.ConvertFrom(value);
            }

            //find source type converter
            var sourceTypeConverter = TypeDescriptor.GetConverter(value.GetType());
            if (sourceTypeConverter != null && conversionType.Equals(typeof(string)) && sourceTypeConverter.CanConvertTo(typeof(string)))
            {
                return sourceTypeConverter.ConvertToInvariantString(value);
            }
            if (sourceTypeConverter != null && sourceTypeConverter.CanConvertTo(conversionType))
            {
                return sourceTypeConverter.ConvertTo(value, conversionType);
            }
            return Convert.ChangeType(value, conversionType);
        }

        /// <summary>
        /// Extracts the value and measurement system from the given value, which must be a string.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="typeString"></param>
        /// <param name="theSystem"></param>
        /// <param name="defaultSystem"></param>
        /// <returns></returns>
        public static string GetTheValue(object value, string typeString, out string theSystem, string defaultSystem)
        {
            var valString = value as string;
            if (valString == null)
            {
                throw new Exception(string.Format("Cannot have null value for {0}", typeString));
            }
            valString = valString.Trim();
            var options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            var pattern = @"^(?<value>[^ \t]+)[ \t]+(?<system>.+)$";
            if (!Regex.IsMatch(valString, pattern, options))
            {
                theSystem = null;
                return null;
            }
            var matches = Regex.Matches(valString, pattern, options);
            var match = matches[0];
            var theValue = match.Groups["value"].Value;
            theSystem = match.Groups["system"].Value;
            return theValue;
        }
    }
}