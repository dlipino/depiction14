using System;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class RasterImageResourceDictionary : ResourceDictionary, IXmlSerializable
    {
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            var rasterFilenames = Deserializers12.DeserializeItemList<string>("RasterFilenames", typeof(string), reader);
            var rasterKeys = Deserializers12.DeserializeItemList<string>("RasterKeys", typeof(string), reader);
            var bitmapLoader = new BitmapSaveToFileHelper();
            for (int i = 0; i < rasterFilenames.Count; i++)
            {
                var filename = rasterFilenames[i];
                var key = rasterKeys[i];
                if (Deserializers12.ReadBinaryFile(filename, bitmapLoader.LoadFromFile))
                {
                    Add(key, bitmapLoader.Bitmap);
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
//            var rasterFilenames = new List<string>();
//            var rasterKeys = new List<string>();
//
//            foreach (string key in Keys)
//            {
//                var extension = Path.GetExtension(key);
//                // since the original filename is used as the key, grab the extension from that for enhanced readability
//                var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
//                var bitmapSaver = new BitmapSaveToFileHelper((BitmapImage)this[key]);
//                SerializationService.SaveBinaryFile(filename, bitmapSaver.SaveToFile);
//                rasterFilenames.Add(filename);
//                rasterKeys.Add(key);
//            }
//            SerializationService.SerializeItemList("RasterFilenames", typeof(string), rasterFilenames, writer);
//            Deserializers12.SerializeItemList("RasterKeys", typeof(string), rasterKeys, writer);

        }
    }

    public class BitmapSaveToFileHelper
    {
        private BitmapImage bitmap;

        public BitmapSaveToFileHelper(BitmapImage bitmap)
        {
            this.bitmap = bitmap;
        }

        public BitmapSaveToFileHelper() {}

        public BitmapImage Bitmap
        {
            get { return bitmap; }
        }

        public bool LoadFromFile(string fileName)
        {
            bitmap = new BitmapImage();
            Bitmap.BeginInit();
            Bitmap.CacheOption = BitmapCacheOption.OnLoad;
            Bitmap.UriSource = new Uri(fileName);
            Bitmap.EndInit();
            Bitmap.Freeze();
            return true;
        }

        public bool SaveToFile(string fileName)
        {
            return SaveBitmap(Bitmap, fileName);
        }

        public static bool SaveBitmap(BitmapSource bitmap, string fileName)
        {
            if (bitmap != null)
            {
                BitmapEncoder encoder = new JpegBitmapEncoder();
                ((JpegBitmapEncoder) encoder).QualityLevel = 80;

                if (Path.HasExtension(fileName))
                {
                    switch (Path.GetExtension(fileName).ToLower())
                    {
                        case ".gif":
                        case ".giff":
                            encoder = new GifBitmapEncoder();
                            break;
                        case ".tiff":
                        case ".tif":
                            encoder = new TiffBitmapEncoder();
                            break;
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                }

                using (var stream = new FileStream(fileName, FileMode.Create))
                {
                    encoder.Frames.Add(BitmapFrame.Create(bitmap));
                    encoder.Save(stream);
                }

                return true;
            }

            return false;
        }
    }
}