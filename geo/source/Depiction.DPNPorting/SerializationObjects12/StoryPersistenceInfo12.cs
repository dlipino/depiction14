using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class StoryPersistenceInfo : IXmlSerializable
    {
        public int DepictionFileVersion { get; set; }
        public string AppVersion { get; set; }
        public string Author { get; set; }
        public DateTime SaveDateTime { get; set; }
        public List<StorySubtreeInfo> Subtrees { get; set; }
        public string MinimumAppVersionForDepictionFileVersion { get; set; }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        private static int GetDpnNumberForFileVersion(string fileVersion)
        {
            if (fileVersion.Equals("1.0.0"))
                return 1;
            return int.Parse(fileVersion);
        }

        public void ReadXml(XmlReader reader)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            DepictionFileVersion = GetDpnNumberForFileVersion(reader.ReadElementContentAsString("DepictionFileVersion", ns));
            if (reader.Name.Equals("MinimumAppVersionForDepictionFileVersion"))
                MinimumAppVersionForDepictionFileVersion = reader.ReadElementContentAsString("MinimumAppVersionForDepictionFileVersion", ns);
            else
                MinimumAppVersionForDepictionFileVersion = "unknown";
            AppVersion = reader.ReadElementContentAsString("AppVersion", ns);
            if (reader.Name.Equals("BuildNumber"))
                reader.ReadElementContentAsString("BuildNumber", ns);

            if (reader.Name.Equals("Author"))
                Author = reader.ReadElementContentAsString("Author", ns);
            else
                Author = string.Empty;
            SaveDateTime = reader.ReadElementContentAsDateTime("SaveDateTime", ns);

            Subtrees = new List<StorySubtreeInfo>(Deserializers12.DeserializeItemList<StorySubtreeInfo>("subtrees", typeof(StorySubtreeInfo), reader));
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("nl-NL");
//            var ns = Deserializers12.DepictionXmlNameSpace;
//            writer.WriteElementString("DepictionFileVersion", ns, DepictionFileVersion.ToString());
//            writer.WriteElementString("MinimumAppVersionForDepictionFileVersion", ns, MinimumAppVersionForDepictionFileVersion);
//            writer.WriteElementString("AppVersion", ns, AppVersion);
//            writer.WriteElementString("BuildNumber", ns, VersionInfo.BuildNumber);
//            writer.WriteElementString("Author", ns, Author);
//            writer.WriteElementString("SaveDateTime", ns, SaveDateTime.ToString("s"));
//            SerializationService.SerializeItemList("subtrees", typeof(StorySubtreeInfo), Subtrees, writer);
        }
    }
}