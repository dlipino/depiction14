using System;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class LabelElement :  ILabelElement12, IXmlSerializable
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public Point CanvasLocation { get; set; }
        public string LabelContent { get; set; }
        public string UserText { get; set; }
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            if (reader.Name.Equals("ParentElementKey"))
            {
//                parentElementKey = reader.ReadElementContentAsString("ParentElementKey", ns);
                reader.ReadElementContentAsString("ParentElementKey", ns);
            }
            if (reader.Name.Equals("CanvasLocation")) CanvasLocation = Deserializers12.Deserialize<Point>("CanvasLocation", reader);
            if (reader.Name.Equals("Width")) Width = Double.Parse(reader.ReadElementContentAsString("Width", ns));
            if (reader.Name.Equals("Height")) Height = Double.Parse(reader.ReadElementContentAsString("Height", ns));
            if (reader.Name.Equals("UserText")) UserText = reader.ReadElementContentAsString("UserText", ns);
            if (reader.Name.Equals("LabelContent")) LabelContent = reader.ReadElementContentAsString("LabelContent", ns);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}