using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class DepictionMetadata : IDepictionMetadata12,IXmlSerializable
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public string Tags { get; set; }
        public DateTime SaveDateTime { get; set; }

        public DepictionMetadata()
        {
            Title = "Depiction";
            Author = "Generic";
            Description = "No description was given.";
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            Title = string.Empty;
            Author = string.Empty;
            SaveDateTime = DateTime.Now;
            Description = "No description was given.";
            reader.ReadStartElement();
            if (reader.Name.Equals("Title"))
                Title = reader.ReadElementContentAsString("Title", ns);
            if (reader.Name.Equals("Author"))
                Author = reader.ReadElementContentAsString("Author", ns);

            if (reader.Name.Equals("SaveDateTime"))
            {
                SaveDateTime = reader.ReadElementContentAsDateTime("SaveDateTime", ns);
            }
            if (reader.Name.Equals("Description"))
                Description = reader.ReadElementContentAsString("Description", ns);
            if (reader.Name.Equals("Tags"))
                Tags = reader.ReadElementContentAsString("Tags", ns);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;

            writer.WriteElementString("Title", ns, Title);

            writer.WriteElementString("Author", ns, Author);
            writer.WriteElementString("SaveDateTime", ns, SaveDateTime.ToString("s"));

            writer.WriteElementString("Description", ns, Description);
            writer.WriteElementString("Tags", ns, Tags);

        }
    }
}