using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.DPNPorting.Interfaces122;
using Depiction.DPNPorting.SerializationObjects12.DataTypes12;
using Depiction.DPNPorting.SerializationObjects12.Displayers12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class VisualData :Helper12DeserializationClasses.IVisualData,IXmlSerializable
    {
        public double ScaleX { get; set; }
        public double ScaleY { get; set; }
        public double TransX { get; set; }
        public double TransY { get; set; }

        public List<IRevealer12> RevealerCollection { get; set; }

        public IElementDisplayer12 PrimaryDisplayer { get; set; }

        public Helper12DeserializationClasses.IGeoExtent ViewportExtent { get; set; }
        
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            if (reader.Name.Equals("TransX")) TransX = reader.ReadElementContentAsDouble("TransX", ns);
            if (reader.Name.Equals("TransY")) TransY = reader.ReadElementContentAsDouble("TransY", ns);
            if (reader.Name.Equals("ScaleX")) ScaleX = reader.ReadElementContentAsDouble("ScaleX", ns);
            if (reader.Name.Equals("ScaleY")) ScaleY = reader.ReadElementContentAsDouble("ScaleY", ns);
            if (reader.Name.Equals("ViewportExtent")) ViewportExtent = Deserializers12.Deserialize<ViewportExtent>("ViewportExtent", reader);
            PrimaryDisplayer = Deserializers12.Deserialize<DepictionCanvas>("primaryDisplayer", reader);
            var revealers = Deserializers12.DeserializeItemList<IRevealer12>("revealers", typeof(DepictionRevealerCanvas), reader);
            RevealerCollection = new List<IRevealer12>(revealers);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
//            var ns = Deserializers12.DepictionXmlNameSpace;
//            writer.WriteElementString("TransX", ns, TransX.ToString("R"));
//            writer.WriteElementString("TransY", ns, TransY.ToString("R"));
//            writer.WriteElementString("ScaleX", ns, ScaleX.ToString("R"));
//            writer.WriteElementString("ScaleY", ns, ScaleY.ToString("R"));
//
//            //HAAAACCCKKKK since the saved instance is not really used (only used because of legacy)
//            var world = VisualAccess.WorldCanvas;
//            if (world != null)
//            {
//                var worldCanvasHolderRect = new Rect(0, 0, ((FrameworkElement) world.Parent).ActualWidth,
//                                                     ((FrameworkElement) world.Parent).ActualHeight);
//                ViewportExtent = new ViewportExtent(world.VisibleViewportLatLongSize, worldCanvasHolderRect.Width,
//                                                    worldCanvasHolderRect.Height);
//            }
//            SerializationService.Serialize("ViewportExtent", viewportExtent, writer);//This should be killed off sometime
//            SerializationService.Serialize("primaryDisplayer", PrimaryDisplayer as DepictionCanvas, writer);
//            SerializationService.SerializeItemList("revealers", typeof(DepictionRevealerCanvas),  RevealerCollection, writer);
            
        }
    }
}