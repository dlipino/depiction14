using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypeConverters;
using Depiction.DPNPorting.Interfaces122;
using Depiction.DPNPorting.SerializationObjects12.DataTypes12;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class BaseElement : IElementInternal12
    {
        private readonly Dictionary<string, IElementProperty12> customProperties = new Dictionary<string, IElementProperty12>();
        public ILatitudeLongitude Position
        {
            get
            {
                if(customProperties.ContainsKey("Position"))
                {
                    var val = customProperties["Position"].Value.ToString();
                    return LatitudeLongitudeTypeConverter.ConvertStringToLatLong(val);
                }
                return LatitudeLongitudeTypeConverter.ConvertStringToLatLong("");
            }
        }

        public string ElementKey { get; private set; }
        public string TypeDisplayName { get; set; }
        public string ElementType { get; set; }
        public IElement12[] Children { get; private set; }
        public IElement12 Parent { get; set; }
        public string LabelData { get; set; }
        public IZoneOfInfluence12 ZoneOfInfluence { get; private set; }
        public IElementProperty12[] Properties { get { return customProperties.Values.ToArray(); } }
        public IImageObjectMetadata12 ImageMetaData { get; set; }
        public string ImagePath { get; private set; }
        public SerializableDictionary<string, string[]> ClickActions { get; set; }
        public SerializableDictionary<string, string[]> CreateActions { get; set; }
        public SerializableDictionary<string, string[]> DeleteActions { get; set; }
        public SerializableDictionary<string, string[]> GenerateZoiActions { get; set; }

        public double PermaTextX { get; set; }
        public double PermaTextY { get; set; }
        public bool PermaHoverTextOn { get; set; }
        public ILabelElement12 Label { get; set; }

        static public string ValidInternalPropertyName( string propertyName)
        {
            return new string(propertyName.ToCharArray().Where(Char.IsLetterOrDigit).ToArray());
        }
        public bool AddProperty(IElementProperty12 propertyToAdd)
        {
            lock (customProperties)
            {
                if (!HasProperty(propertyToAdd.Name))
                {
                    customProperties.Add(propertyToAdd.Name, propertyToAdd);
//                    propertyToAdd.PropertyIndex = RunningElementPropertyCount;
//                    RunningElementPropertyCount++;//Like a data base
                }

//                GetProperty(propertyToAdd.Name).Element = this;
//                NotifyPropertyChanged(propertyToAdd.Name);
            }
//            if (propertyDescriptorCollection != null)
//                propertyDescriptorCollection.Add(new DictionaryPropertyDescriptor<string, IProperty>(customProperties, propertyToAdd.Name));
            return true;
        }
        public bool HasProperty(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                return false;
            var internalPropertyName = ValidInternalPropertyName(propertyName);
            foreach (var key in customProperties.Keys)
            {
                if (key.Equals(internalPropertyName, StringComparison.CurrentCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            reader.ReadElementContentAsString("ElementDefinitionVersion", ns);
            ElementKey = reader.ReadElementContentAsString("elementKey", ns);
            //HoverTextField was removed from BaseElement and replaced by a property on Property.  Checking for it here goes against the system set up
            //with import actions to upgrade depictions but it's a quick and dirty fix.
            string hoverTextField = null;
            // Handle older formats.
            if (reader.Name.Equals("hoverTextField"))
                hoverTextField = reader.ReadElementContentAsString("hoverTextField", ns);
            // Handle older formats.
            if (reader.Name.Equals("iconSize"))
            {
                var iconSize = reader.ReadElementContentAsDouble("iconSize", ns);
                AddProperty(new Property("IconSize", "IconSize", iconSize, null, null, false) { Visible = false });
            }

            TypeDisplayName = reader.ReadElementContentAsString("typeDisplayName", ns);
            ElementType = reader.ReadElementContentAsString("elementType", ns);
            // Handle older formats.
            if (reader.Name.Equals("addPropertiesOnImport"))
            {
                var AddPropertiesOnImport = reader.ReadElementContentAsBoolean("addPropertiesOnImport", ns);
            }
            else
            {
                var AddPropertiesOnImport = true;
            }

            ClickActions = SerializationService.Deserialize<SerializableDictionary<string, string[]>>("ClickActions", reader);
            CreateActions = SerializationService.Deserialize<SerializableDictionary<string, string[]>>("CreateActions", reader);
            GenerateZoiActions = SerializationService.Deserialize<SerializableDictionary<string, string[]>>("GenerateZoiActions", reader);
            DeleteActions = SerializationService.Deserialize<SerializableDictionary<string, string[]>>("DeleteActions", reader);
//            var data = new ImageObjectMetadata();
//            data.ReadXml(reader);
//            ImageMetaData = data;
            ImageMetaData = Deserializers12.Deserialize<ImageObjectMetadata>("ImageMetaData", reader);
            ZoneOfInfluence = Deserializers12.Deserialize<ZoneOfInfluence>("zoneOfInfluence", reader);
            Debug.WriteLine(string.Format("Getting properties for {0}",TypeDisplayName));
            var propList = Deserializers12.DeserializeItemList<Property>("customProperties", typeof(Property), reader);
            foreach (var property in propList)
                AddProperty(property);
            //race condition with properties and the zone of influence, hence setting ZOI goes after getting properties
//            SetZoneOfInfluence(zoi, false);
//            if (!String.IsNullOrEmpty(hoverTextField))
//            {
//                var prop = GetProperty(hoverTextField);
//                if (prop != null)
//                    prop.IsHoverText = true;
//            }

            var childrenList = Deserializers12.DeserializeItemList<IElement12>("childrenList", typeof(BaseElement), reader);
            Children = childrenList.ToArray();
//            foreach (IElementInternal12 element in childrenList)
//            {
//                AddChild(element);
//            }
            if (reader.Name.Equals("LabelElement"))
            {
                Label = Deserializers12.Deserialize<LabelElement>("LabelElement", reader);
                //Attach teh events 
//                LabelElement.AttachLabelToParentEvents(this);
            }

            if (reader.Name.Equals("PermaHoverTextOn"))
            {
                PermaHoverTextOn = bool.Parse(reader.ReadElementContentAsString("PermaHoverTextOn", ns));
            }
            if (reader.Name.Equals("PermaTextX"))
            {
                PermaTextX = double.Parse(reader.ReadElementContentAsString("PermaTextX", ns));
            }
            if (reader.Name.Equals("PermaTextY"))
            {
                PermaTextY = double.Parse(reader.ReadElementContentAsString("PermaTextY", ns));
            }

            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
           writer.WriteStartElement("BaseElement");
            writer.WriteEndElement();
        }
    }
}