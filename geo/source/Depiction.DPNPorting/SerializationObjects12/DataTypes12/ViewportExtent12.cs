using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12.DataTypes12
{
    public class ViewportExtent : Helper12DeserializationClasses.IGeoExtent,IXmlSerializable
    {       
        private BoundingBox viewPortBoundingBox12;
        private double heightInPixels;
        private double widthInPixels;

//        public ILatitudeLongitude NorthWest { get{return viewPortBoundingBox12.} private set; }
//        public ILatitudeLongitude SouthWest { get; private set; }
//        public ILatitudeLongitude SouthEast { get; private set; }
//        public ILatitudeLongitude NorthEast { get; private set; }
        public IMapCoordinateBounds GeoBounds
        {
            get
            {
                var b = viewPortBoundingBox12;

                return new MapCoordinateBounds(new LatitudeLongitudeBase(b.Top,b.Left),new LatitudeLongitudeBase(b.Bottom,b.Right));
            }
        }

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            heightInPixels = reader.ReadElementContentAsDouble("heightInPixels", ns);
            widthInPixels = reader.ReadElementContentAsDouble("widthInPixels", ns);
            viewPortBoundingBox12 = Deserializers12.Deserialize<BoundingBox>("viewPortBoundingBox", reader);
            //in case there is extra junk, just keep reading (davidl)
            while (!reader.NodeType.Equals(XmlNodeType.EndElement))
            {
                reader.Read();
            }
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}