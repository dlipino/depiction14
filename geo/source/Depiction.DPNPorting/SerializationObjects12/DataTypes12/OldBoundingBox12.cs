using System;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

//namespace Depiction.API.ValueObject

namespace Depiction.DPNPorting.SerializationObjects12.DataTypes12
{
    public class BoundingBox :IBoundingBox12, IXmlSerializable
    {
        #region Variables

        private Size mapImageSize;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BoundingBox"/> class.
        /// </summary>
        public BoundingBox()
        {
        }

        ///<summary>
        /// Initializes a new instance of the <see cref="BoundingBox"/> class.
        ///</summary>
        ///<param name="topLeft">The top left coordinate of the bounding box.</param>
        ///<param name="bottomRight">The bottom right coordinate of the bounding box.</param>
//        public OldBoundingBox12(LatitudeLongitude topLeft, LatitudeLongitude bottomRight)
//        {
//            Top = topLeft.Latitude;
//            Left = topLeft.Longitude;
//            Bottom = bottomRight.Latitude;
//            Right = bottomRight.Longitude;
//        }

        #endregion

        #region Properties

        /// <summary>
        /// Size of map to return when requesting a map.
        /// </summary>
        public Size MapImageSize
        {
            get { return mapImageSize; }
            set { mapImageSize = value; }
        }

        public double Top { get; set; }

        public double Bottom { get; set; }

        public double Right { get; set; }

        public double Left { get; set; }

        /// <summary>
        /// Top left (northwest) corner of the box
        /// </summary>
//        public LatitudeLongitude TopLeft
//        {
//            get { return new LatitudeLongitude(Top, Left); }
//        }
//
//        /// <summary>
//        /// Bottom right (southeast) corner of the box
//        /// </summary>
//        public LatitudeLongitude BottomRight
//        {
//            get { return new LatitudeLongitude(Bottom, Right); }
//        }
//
//        /// <summary>
//        /// Top right (northeast) corner of the box
//        /// </summary>
//        public LatitudeLongitude TopRight
//        {
//            get { return new LatitudeLongitude(Top, Right); }
//        }
//
//        /// <summary>
//        /// Bottom left (southwest) corner of the box
//        /// </summary>
//        public LatitudeLongitude BottomLeft
//        {
//            get { return new LatitudeLongitude(Bottom, Left); }
//        }
//
//        /// <summary>
//        /// Center of the bounding box; calculated with Lat = (Top + Bottom / 2), Lon = (Right + Left) /2
//        /// </summary>
//        public LatitudeLongitude Center
//        {
//            get { return new LatitudeLongitude(((Bottom + Top) / 2), ((Right + Left) / 2)); }
//        }

        ///<summary>
        /// Returns the Width of the bounding box.
        ///</summary>
        public double Width
        {
            get { return Math.Abs(Right - Left); }
        }

        /// <summary>
        /// Returns the Height of the bounding box.
        /// </summary>
        public double Height
        {
            get { return Math.Abs(Top - Bottom); }
        }

        /// <summary>
        /// The number of meters per pixel at the current zoom level at the center of the depiction's area.
        /// </summary>
//        public double MetersPerPixel
//        {
//            get { return CalculateMetersPerPixel(MapImageSize); }
//            set
//            {
//                double horizontalMeters = TopLeft.DistanceTo(TopRight, MeasurementSystemAndScale.Metric);
//                double verticalMeters = TopLeft.DistanceTo(BottomLeft, MeasurementSystemAndScale.Metric);
//                mapImageSize.Width = horizontalMeters;
//                mapImageSize.Height = verticalMeters;
//            }
//        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the bounding box extents
        /// </summary>
        /// <param name="northern">Northern border of the box</param>
        /// <param name="southern">Southern border of the box</param>
        /// <param name="eastern">Eastern border of the box</param>
        /// <param name="western">Western border of the box</param>
        /// <param name="mapSize">Size of the map to request</param>
        /// <returns></returns>
        public bool SetBoundingBoxExtents(double northern, double southern, double eastern, double western, Size mapSize)
        {
            try
            {
                Top = northern;
                Bottom = southern;
                Right = eastern;
                Left = western;

//                double metersPP = CalculateMetersPerPixel(mapSize);

                mapImageSize = mapSize;
                return true;
            }
            catch
            {
                return false;
            }
        }

//        private double CalculateMetersPerPixel(Size mapSize)
//        {
//            double DiagonalMeters = TopLeft.DistanceTo(BottomRight, MeasurementSystemAndScale.Metric);
//            double DiagonalPixels =
//                Math.Pow(Math.Pow(mapSize.Width, 2) + Math.Pow(mapSize.Height, 2), .5);
//
//            return DiagonalMeters / DiagonalPixels;
//        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            Top = reader.ReadElementContentAsDouble("Top", ns);
            Left = reader.ReadElementContentAsDouble("Left", ns);
            Bottom = reader.ReadElementContentAsDouble("Bottom", ns);
            Right = reader.ReadElementContentAsDouble("Right", ns);
            mapImageSize = Size.Parse(reader.ReadElementContentAsString("mapImageSize", ns));
            reader.ReadEndElement();

        }
//        public override bool Equals(object obj)
//        {
//            var bb = obj as BoundingBox;
//            if (bb == null) return false;
//            return ((Top == bb.Top) && (Left == bb.Left) && (Bottom == bb.Bottom) && (Right == bb.Right) &&
//                    (MapImageSize == bb.MapImageSize));
//        }

        public void WriteXml(XmlWriter writer)
        {
//            var ns = Constants.DepictionXmlNameSpace;
//            writer.WriteElementString("Top", ns, Top.ToString());
//            writer.WriteElementString("Left", ns, Left.ToString());
//            writer.WriteElementString("Bottom", ns, Bottom.ToString());
//            writer.WriteElementString("Right", ns, Right.ToString());
//            writer.WriteElementString("mapImageSize", ns, mapImageSize.ToString());
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", Top, Left, Bottom, Right);
        }
    }
}