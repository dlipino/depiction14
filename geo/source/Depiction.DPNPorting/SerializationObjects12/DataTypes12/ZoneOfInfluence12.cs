using System;
using System.Windows.Media;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.OldInterfaces;
using Depiction.DPNPorting.Interfaces122;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12.DataTypes12
{
    public class ZoneOfInfluence : IZoneOfInfluence12, IXmlSerializable
    {
        public string WtkString { get; private set; }
        public bool IsEmpty { get; private set; }

        public override string ToString()
        {
            return WtkString;
        }
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            string ns = Deserializers12.DepictionXmlNameSpace;
            reader.ReadStartElement();
            WtkString = reader.ReadElementContentAsString("geometryWkt", ns);

            var fillColorString = reader.ReadElementContentAsString("fillColor", ns);
//            if (fillColorString != string.Empty)
//                fillColor = fillColorString;
            var strokeColorString = reader.ReadElementContentAsString("strokeColor", ns);
            string strokeDashCollectionString = reader.ReadElementContentAsString("strokeDashCollection", ns);
            if (strokeDashCollectionString != string.Empty)
            {
                var converter = new DoubleCollectionConverter();
                //StrokeDashCollection = (DoubleCollection)converter.ConvertFromInvariantString(strokeDashCollectionString);
            }

            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}