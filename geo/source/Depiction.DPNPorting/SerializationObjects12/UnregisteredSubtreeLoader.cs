using System.IO;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.DPNPorting.Depiction12Deserializers.Subtrees;
using Depiction.Serialization;

namespace Depiction.DPNPorting.SerializationObjects12
{
    public class UnregisteredSubtreeLoader : BaseSubtreeLoader
    {
        protected override void Load(Helper12DeserializationClasses.PersistenceMemento12 memento, Stream stream, Helper12DeserializationClasses.SubtreeInfo12 subtreeInfo)
        {
            memento.Story.UnregisteredElementRepository = Deserializers12.DeserializeFromFolder<UnregisteredElementRepository>("ElementRepository", stream, subtreeInfo.DataPath);
        }
    }
}