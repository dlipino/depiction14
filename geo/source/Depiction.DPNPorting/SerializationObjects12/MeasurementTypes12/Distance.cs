namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public class Distance : BaseMeasurement
    {
        public override string GetUnits(MeasurementSystemAndScale systemAndScale)
        {
            systemAndScale = GetSystem(systemAndScale);
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.Imperial:
                    return "feet";
                case MeasurementSystemAndScale.Metric:
                    return "meters";
                case MeasurementSystemAndScale.ImperialSmall:
                    return "inches";
                case MeasurementSystemAndScale.MetricSmall:
                    return "centimeters";
                case MeasurementSystemAndScale.ImperialLarge:
                    return "miles";
                case MeasurementSystemAndScale.MetricLarge:
                    return "km";
                default:
                    return "";
            }
        }
    }
}