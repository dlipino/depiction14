namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public class Temperature : BaseMeasurement
    {
        public override string GetUnits(MeasurementSystemAndScale systemAndScale)
        {
            systemAndScale = GetSystem(systemAndScale);
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.Imperial:
                case MeasurementSystemAndScale.ImperialLarge:
                case MeasurementSystemAndScale.ImperialSmall:
                    return "�F";
                case MeasurementSystemAndScale.Metric:
                case MeasurementSystemAndScale.MetricLarge:
                case MeasurementSystemAndScale.MetricSmall:
                    return "�C";
                default:
                    return "";
            }
        }
        
    }
}