namespace Depiction.DPNPorting.SerializationObjects12.MeasurementTypes12
{
    public class Area : BaseMeasurement
    {
        public override string GetUnits(MeasurementSystemAndScale systemAndScale)
        {
            systemAndScale = GetSystem(systemAndScale);
            switch (systemAndScale)
            {
                case MeasurementSystemAndScale.ImperialLarge:
                    return "square miles";
                case MeasurementSystemAndScale.MetricLarge:
                    return "square km";
                case MeasurementSystemAndScale.Imperial:
                    return "square feet";
                case MeasurementSystemAndScale.Metric:
                    return "square meters";
                case MeasurementSystemAndScale.ImperialSmall:
                    return "square inches";
                case MeasurementSystemAndScale.MetricSmall:
                    return "square centimeters";
                default:
                    return "";
            }
        }
    }
}