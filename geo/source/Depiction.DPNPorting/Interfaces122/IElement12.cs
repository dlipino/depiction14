using System;
using System.Xml.Serialization;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface IElement12 : IXmlSerializable
    {
        ILatitudeLongitude Position { get; }
        string ElementKey { get; }
        string TypeDisplayName { get; set; }
        string ElementType { get; set; }
        IElement12[] Children { get; }
        IElement12 Parent { get; set; }

        string LabelData { set; get; }
//        void CreateLabel(string labelData, double labelWidth, double labelHeight);

        IZoneOfInfluence12 ZoneOfInfluence { get; }
        /// <summary>
        /// Gets or sets the type of this element.
        /// </summary>



        IElementProperty12[] Properties { get; }
        /// <summary>
        /// If this object is represented by an image, where should we place it?
        /// </summary>
        IImageObjectMetadata12 ImageMetaData { get; set; }

        /// <summary>
        /// The icon displayed in the menu bar
        /// </summary>
        //ImageSource IconSource { get; }
    }
}