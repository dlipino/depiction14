using System.Windows;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface IAnnotationElement12
    {
        //            double Padding { get; }
        //            double BorderThickness { get; }
        ILatitudeLongitude MapLocation { get; }
        double ContentLocationX { get; set; }
        double ContentLocationY { get; set; }
        string BackgroundColor { get; }
        string ForegroundColor { get; }
        double Width { get; }
        double Height { get; }
        string AnnotationText { get; }
        //            ImageSource IconSource { get; }
        //            double IconSize { get; }
        double PixelWidthAtCreation { get; }
        Point PixelLocation { get; }

        bool IsCollapsed { get; }
        //            double CurrentOnePixelInWorldCanvasCoordinates { get; set; }
        //            void AdjustWidth(double WorldCanvasPixelsToMove);
        //            void AdjustHeight(double WorldCanvasPixelsToMove);
        //            Visibility Visibility { get; }
        //            Visibility Collapsed { get; set; }
        //            event Action DialogRequested;
        //            bool AnnotationDialogVisible { get; set; }
        //            int ZIndex { get; }
        //            IAnnotationElement Copy(double newCreationPixeWidth);

    }
}