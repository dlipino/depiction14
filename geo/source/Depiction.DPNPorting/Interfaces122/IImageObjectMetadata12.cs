namespace Depiction.DPNPorting.Interfaces122
{
    public interface IImageObjectMetadata12
    {
        string ImageFilename { get; set; }
        bool IsGeoReferenced { get; set; }
        double Rotation { get; set; }
        string RetrievedFrom { get; set; }

        double TopLeftLatitude { get; set; }
        double TopLeftLongitude { get; set; }

        double BottomRightLatitude { get; set; }
        double BottomRightLongitude { get; set; }
    }
}