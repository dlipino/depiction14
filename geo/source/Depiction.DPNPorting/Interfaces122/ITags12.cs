using System.Collections.Generic;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface ITags12
    {
        HashSet<string> GetTagsForElementID(string id);
//        void RestoreElements(IEnumerable<IElementInternal12> elements);
    }
}