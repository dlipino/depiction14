using Depiction.Serialization;

namespace Depiction.DPNPorting.Interfaces122
{
    /// <summary>
    /// The core contract for all types of story elements.
    /// </summary>
    public interface IElementInternal12 : IElement12
    {
        #region Properties

        //   Image ElementImage { get; }
        //Not sure if the should go here or in IElement
        string ImagePath { get; }
        ILabelElement12 Label { get; set; }
        double PermaTextX { get; set; }
        double PermaTextY { get; set; }
        bool PermaHoverTextOn { get; set; }
        /// <summary>
        /// Gets or sets the time tick.
        /// The number of times a rule has been fired on this object for one pass
        /// over the graph.  Used by InteractionGraph.
        /// </summary>
        //int NumberIncomingEdgesFired { get; set; }

        /// <summary>
        /// Gets or sets the create actions.
        /// </summary>
        /// <value>The create actions.</value>
        SerializableDictionary<string, string[]> ClickActions { get; set; }

        /// <summary>
        /// Gets or sets the create actions.
        /// </summary>
        /// <value>The create actions.</value>
        SerializableDictionary<string, string[]> CreateActions { get; set; }

        /// <summary>
        /// Gets or sets the delete actions.
        /// </summary>
        /// <value>The delete actions.</value>
        SerializableDictionary<string, string[]> DeleteActions { get; set; }

        SerializableDictionary<string, string[]> GenerateZoiActions { get; set; }

        //bool AddPropertiesOnImport { get; set; }
        //IEnumerable<IDelegateActionCommand> ContextMenuCommands { get; }

        #endregion

        #region Methods

        //void SetZoneOfInfluence(IZoneOfInfluence zoi, bool triggerChangedEvent);

        //        void SetPixelLocationWithoutChangingZOI(Point newPixelLocation);

        /// <summary>
        /// Restores this object to its pristine state.
        /// </summary>
        //void Restore();

        #endregion

        #region Events

        /// <summary>
        /// Occurs when [do interaction].
        /// </summary>
        // event Action<IElementInternal> DoInteraction;
        //event Action<IElementView, string> OnHideElement;

        #endregion

        /// <summary>
        /// Notifies the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        // void NotifyPropertyChanged(string propertyName);


        // IElementInternal Clone();

        /// <summary>
        /// Set this property to this value.
        /// If the property does not exist, it will be created and added to this element.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        /// <param name="visible"></param>
        // void SetRequiredProperty(string propertyName, object value, bool visible);

        // void RunCreateActions(bool b);
        //void GenerateZoiIfApplicable();
        // void DeletePropertiesForCustomZoi();
        // string GetUniqueKey();

        /// <summary>
        /// Sets an elements IconPath, if iconPath is null, the elements icon does not change.
        /// </summary>
        /// <param name="iconPath"></param>
        //void SetElementIconPath(IconPath iconPath);
    }
}