using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface IElementRepository12
    {
        /// <summary>
        /// A readonly collection of the Objects in the repository.
        /// This is the preferred method of getting the objects for two reasons:  
        ///     It is more performant than using the ToArray implementation ( the ElementsInRepository property)
        ///     It also is dynamic, meaning you can save a reference to this collection and as new objects are added later, the new objects are available;
        /// </summary>
        ReadOnlyCollection<IElementInternal12> AllElementsInternal { get; }
        ReadOnlyCollection<IElement12> AllElements { get; }
//
//        void AddElementList(List<IElement> elements, string sourceName, SourceType sourceType, bool autoAdd);
//        void Add(IElement element, string sourceName, SourceType sourceType);
//        void Add(IElement element, string sourceName, SourceType sourceType, bool autoAdd);
//        void Clear();
//        void FireCollectionChangedEvent(NotifyCollectionChangedEventArgs args);
//
//        /// <summary>
//        /// The set of tags used for elements in this repository.
//        /// </summary>
        ITags12 Tags { get; }
//
//        event DisplayNewElementDelegate DisplayNewElementEvent;
//
//        List<IElement> GetAllElementsAndChildren();
//        void DisableNotifications();
//        void EnableNotifications();
    }
}