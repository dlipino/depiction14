using System;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface IDepictionMetadata12
    {
        string Title { get;  }
        string Description { get;  }
        string Author { get;  }
        string Tags { get;  }
        DateTime SaveDateTime { get;  } 
    }
}