using System.Collections.Generic;
using System.Windows;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface IStory12 
    {
        string[] RegionCodes { get; }
        string CurrentStoryName { get; set; }
        IDepictionMetadata12 Metadata { get; set; }
        /// <summary>
        /// Gets the element repository.
        /// </summary>
        IElementRepository12 ElementRepository { get; set; }

        /// <summary>
        /// Gets the unregistered element repository.
        /// </summary>
        IElementRepository12 UnregisteredElementRepository { get; set; }

        List<IAnnotationElement12> Annotations { get; }

        /// <summary>
        /// Gets or sets the region extent.
        /// </summary>
        /// <value>The region extent.</value>
        IBoundingBox12 RegionExtent { get; set; }
//        bool HasRegion { get; }

        ResourceDictionary BitmapResources { get; }
//        ResourceDictionary VectorResources { get; }
//        bool NeedSaving { get; set; }
//        void LoadVectorResources();
//        void ClearStory();

        /// <summary>
        /// Gets the tags object
        /// </summary>
        ITags12 Tags { get; }

        /// <summary>
        /// Provides access to the repository of interactions.
        /// </summary>
//        IInteractionRuleRepository InteractionRuleRepository { get; set; }

        /// <summary>
        /// Provides access to the repository of element prototypes.
        /// </summary>
        /// <value>The PrototypeRepository.</value>
//        IPrototypeRepository PrototypeRepository { get; set; }

        //void AddElementsToRepositories(ImportedElements importedElements, string sourceName, SourceType sourceType, bool autoAdd);
//        void AddAndMergeElementsToRepositories(ImportedElements importedElements, string sourceName, SourceType sourceType, bool autoAdd);
    }
}