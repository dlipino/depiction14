namespace Depiction.DPNPorting.Interfaces122
{
    public interface IBoundingBox12
    {

        double Top { get;  }

        double Bottom { get;  }

        double Right { get;  }

        double Left { get;  }
    }
}