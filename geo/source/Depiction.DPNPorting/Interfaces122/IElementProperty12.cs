using System;
using System.Collections.Generic;
using Depiction.API.OldValidationRules;
using Depiction.Serialization;

namespace Depiction.DPNPorting.Interfaces122
{
    /// <summary>
    /// An element property.
    /// </summary>
    public interface IElementProperty12
    {
        /// <summary>
        /// The property's value.
        /// </summary>
        object Value { get; }
        IValidationRule[] ValidationRules { get;  }
        string StringValue { get; }
        /// <summary>
        /// Gets the post set actions.
        /// </summary>
        /// <value>The post set actions.</value>
         SerializableDictionary<string, string[]> PostSetActions { get;  }
        /// <summary>
        /// Return the default value for this property, usually specified in the DML file.
        /// </summary>
        object DefaultValue { get; }
        bool Deletable { get; }
        /// <summary>
        /// May the user edit this property via the Element Information control?
        /// </summary>
        bool Editable { get; set; }

        /// <summary>
        /// May the user see this property in the Element Information control?
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// The type for this property's value.
        /// </summary>
        Type PropertyType { get; }

        /// <summary>
        /// The internal name for this property. It's format must conform to that for a C# variable.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// What the UI will display as this variable's name.
        /// </summary>
        string DisplayName { get; }

        /// <summary>
        /// The order in which to display this property in the Element Information control.
        /// This should be done via the View Model.
        /// </summary>
        int PropertyIndex { get; set; }

        /// <summary>
        /// The source for the value of this property.
        /// </summary>
        //  ElementPropertySource PropertySource { get; set; }



        /// <summary>
        /// The element to which this property belongs.
        /// </summary>
        IElement12 Element { get; set; }


        /// <summary>
        /// Is this property part of this element's hover text?
        /// </summary>
        bool IsHoverText { get; set; }


    }
}