using System;
using System.Windows;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface ILabelElement12 //: IEquatable<ILabelElement12>
    {
//        event Func<ILabelElement12, string, bool> OnHideLabel;
//        Point ParentPixelLocation { get; }
//        string ParentElementKey { get; }
        double Width { get; set; }
        double Height { get; set; }

        Point CanvasLocation { get; set; }
        string LabelContent { get; set; }
        string UserText { get; set; }
//        PointCollection LinePoints { get; set; }

//        void AttachLabelToParentEvents(IElementView parent);
    }
}