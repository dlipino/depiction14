using System.Windows;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.DPNPorting.Interfaces122
{
    public interface IRevealer12 : IElementDisplayer12
    {
        IMapCoordinateBounds RevealerGeoBounds { get; }
        bool RectShape { get; set; }

        bool Moveable { get; set; }
        bool RevealerVisible { get; set; }
        bool BorderVisible { get; set; }
        bool ButtonsAndInfoVisible { get; set; }
//        bool BorderHighlighted { get; set; }
        double Opacity { get; set; }


        Point Center { get; }
        double Width { get; set; }
        double Height { get; set; }
        string Units { get; }
//        void FireSizeAndPlaceChangeProperties();

        double MinContentLocationX { get; set; }
        double MinContentLocationY { get; set; }
        bool PermaTextVisible { get; set; }

//        int? ElementCount { get; }
//        void CalculateShownElementCount();

        //Don't like these

        //        void OpenProperties();
//        void Show();
//        void Hide();
//        void Maximize();

//        void ShowInDisplayContent();
    }
}