﻿using System;
using System.Collections.Generic;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.APIUnmanaged;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.Serialization;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

[SetUpFixture]
public class AssemblySetup
{
    [SetUp]
    public void Setup()
    {
        Console.WriteLine("Performing one time global setup");
        GdalEnvironment.SetupEnvironment();
        DepictionAccess.InteractionsLibrary.RefreshInteractionLibrary();
    }
} 

namespace Depiction.UnitTests
{
    
    public class UnitTestHelperMethods
    {
        public static IPolygon GetPolygonFromCoordinates(Coordinate[] coordinates)
        {
            GeometryFactory geoFact = new GeometryFactory();
            return geoFact.CreatePolygon(geoFact.CreateLinearRing(coordinates), null);
        }

        static public void PropertyTester(string propName, object expected, IElementPropertyHolder propHolder)
        {
            Assert.IsNotNull(propHolder);
            object val;
            var hasVal = propHolder.GetPropertyValue(propName, out val);
            Assert.IsTrue(hasVal);
            Assert.AreEqual(expected, val);
            //It would be nice to do a property equality
        }
        
        static public void SetupElementLibraryWithDMLFiles(List<string> dmlFiles, TempFolderService tempFolderService, bool useTestAssembly)
        {
            SetupElementLibraryWithDMLFiles(dmlFiles, tempFolderService, useTestAssembly, false);
        }
        static public void SetupElementLibraryWithDMLFiles(List<string> dmlFiles, TempFolderService tempFolderService, bool useTestAssembly,bool loadAutodetect)
        {
            Assert.IsNotNull(tempFolderService);
            Assert.IsNotNull(dmlFiles);
            Assert.IsNotNull(DepictionAccess.ElementLibrary);
         
            if(loadAutodetect)
            {
                var autoAssembly = "Depiction.CoreModel";
                var autoPath = "Depiction.CoreModel.ElementLibrary.";
                LoadElementsDMLsToLibraryFromAssembly(new List<string>{"AutoDetect.dml"}, autoAssembly, autoPath, tempFolderService);
            }
            string assemblyName = "Resources.Product.Depiction";
            string dmlStart = "Resources.Product.Depiction.Elements.";
            if(useTestAssembly)
            {
                assemblyName = "Depiction.UnitTests";
                dmlStart = "Depiction.UnitTests.CoreModel.ElementLibrary.";
            }

            LoadElementsDMLsToLibraryFromAssembly(dmlFiles, assemblyName, dmlStart, tempFolderService);

        }
        static public void LoadElementsDMLsToLibraryFromAssembly(List<string> dmlFiles, string assemblyName, string elementAssemblyPath,TempFolderService tempFolderService)
        {
            Assert.IsNotNull(tempFolderService);
            Assert.IsNotNull(dmlFiles);
            Assert.IsNotNull(DepictionAccess.ElementLibrary);
            int count = 0;
            var tempFileName = tempFolderService.GetATempFilenameInDir();
            foreach (var name in dmlFiles)
            {
                DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, elementAssemblyPath + name, tempFileName);
                var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
                if (loadedPrototype != null)
                {
                    if (DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype))
                    {
                        count++;
                    }
                }
            }

            Assert.AreEqual(dmlFiles.Count, count);
        }

        static public IElementPrototype CreateRawPrototypeWithProps(string type, Dictionary<string,object> props)
        {
            var prototypeWithEid = ElementFactory.CreateRawPrototype();
            prototypeWithEid.UpdateTypeIfRaw(type);
            foreach(var keyPair in props)
            {
                var eidProperty = new DepictionElementProperty(keyPair.Key,keyPair.Value);
                prototypeWithEid.AddPropertyOrReplaceValueAndAttributes(eidProperty);
            }
            return prototypeWithEid;
        }
    }
}
