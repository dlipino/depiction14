using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.DepictionConverters;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.CoreModel.DpnPorting;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.DPNPorting.Depiction12Deserializers;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.Serialization
{
    [TestFixture]
    public class DpnPortingTests
    {//It would be nice to have a roadnetwork inporting test
        private TempFolderService temp;
        private string unzipDir = "";
        string assemblyName = "Depiction.UnitTests";
        string dmlassemblyName = "Resources.Product.Depiction";
        string dmlStart = "Resources.Product.Depiction.Elements.";
        [SetUp]
        public void Setup()
        {
            temp = new TempFolderService(true);
            unzipDir = temp.GetATempFilenameInDir();
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(assemblyName, "dpn", true, temp.FolderName);
            DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(dmlassemblyName, "xml", true, temp.FolderName);
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            //Not sure how needed this is
            DepictionTypeInformationSerialization.UpdateSerializationServiceTypeDictionaryWithFile(Path.Combine(temp.FolderName, "Depiction.DefaultResources.DepictionDataTypes.xml"));
            //  DepictionCoreTypes.DoFullTypeUpdateFromFileAndDefaults;//This one might be the one we are actually trying to use

        }
        [TearDown]
        public void TearDown()
        {
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            DepictionAccess.ElementLibrary = null;
            temp.Close();
        }
        protected void DMLLoadingHelper(List<string> dmlTypesToLoad)
        {
            var tempFileName = temp.GetATempFilenameInDir();
            int count = 0;
            foreach (var name in dmlTypesToLoad)
            {
                DefaultResourceCopier.CopyResourceStreamToFile(dmlassemblyName, dmlStart + name, tempFileName);
                var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
                if (loadedPrototype != null)
                {
                    DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);
                    count++;
                }
            }
            Assert.AreEqual(dmlTypesToLoad.Count, count);
        }

        //         <Top>47.6236360627514</Top>
        //        <Left>-122.379439</Left>
        //        <Bottom>47.5831557397061</Bottom>
        //        <Right>-122.278962809524</Right>
        //        <mapImageSize>1000,598</mapImageSize>
        [Test]
        public void CanEmpty122DpnTurnIntoEmpty13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.Empty122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldProtoLibrary = new ElementPrototypeLibrary();
            oldProtoLibrary.SetUserPrototypesFromPath(Path.Combine(unzipDir, "ElementTypes"), false);

            Assert.AreEqual(59, oldProtoLibrary.UserPrototypes.Count);

            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(0, oldStory.ElementRepository.AllElements.Count);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);

            Console.WriteLine(newDepiction.DepictionsFileName);
        }
        [Test]
        public void Can122DpnWithAnnotationTurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.SingleAnnotation122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.AreEqual(1, oldStory.Annotations.Count);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(1, newDepiction.DepictionAnnotations.Count);
        }
        [Test]
        public void Can122DpnWithSingleElementTurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.SingleElement122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
           
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(1, oldStory.ElementRepository.AllElements.Count);
            var oldElement = oldStory.ElementRepository.AllElements[0];
            var oldElementTags = oldStory.Tags.GetTagsForElementID(oldElement.ElementKey);
            //Setup for element copying, need prior knowledge of dpn and know what element is in it
            var prototypeNames = new List<string> { "Car.dml" };
            DMLLoadingHelper(prototypeNames);

            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(1, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            Assert.AreEqual(1, newDepiction.MainMap.ElementIdsInDisplayer.Count);

            var newElement = newDepiction.CompleteElementRepository.AllElements[0];
            var newElementTags = newElement.Tags;
            Assert.AreEqual(oldElementTags.Count,newElementTags.Count);
            foreach(var tag in oldElementTags)
            {
                if(!newElementTags.Contains(tag))
                {
                    var notGood = false;
                    Assert.IsTrue(notGood);
                }
            }

        }
        [Test]
        public void Can122DpnWithSingleElementWithChildrenProtToADepiction13Dpn()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.SingleRouteWith2Waypoints122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;

            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(1, oldStory.ElementRepository.AllElements.Count);
            var elementWithChildre = oldStory.ElementRepository.AllElements[0];
            Assert.AreEqual(4, elementWithChildre.Children.Length);
            //Setup for element copying, need prior knowledge of dpn and know what element is in it
            var prototypeNames = new List<string> { "RouteUserDrawn.dml" };
            DMLLoadingHelper(prototypeNames);

            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(1, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            Assert.AreEqual(1, newDepiction.MainMap.ElementIdsInDisplayer.Count);
            var elementWithWayPoints = newDepiction.CompleteElementRepository.ElementsGeoLocated[0];
            Assert.AreEqual(4, elementWithWayPoints.Waypoints.Length);
            foreach (var waypoint in elementWithWayPoints.Waypoints)
            {
                Assert.IsNotNull(waypoint.Location);
                Assert.IsTrue(waypoint.Location.IsValid);
            }
        }

        [Test]
        public void Can122DpnWithSingleElementZOITurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.SingleElementZOI122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(1, oldStory.ElementRepository.AllElements.Count);

            var expectedZOI =
                "POLYGON ((-122.32616198706666 47.605071231178975,-122.32135874632591 47.610965545152645,-122.33762176683706 47.612211034244446,-122.33339722980001 47.60701751543597,-122.32616198706666 47.605071231178975))";

            var element = oldStory.ElementRepository.AllElements[0];
            Assert.IsNotNull(element);
            Assert.IsNotNull(element.ZoneOfInfluence);
           
            var oldZOI = element.ZoneOfInfluence.ToString();
            var type122 = element.ElementType;
            var type13 = DepictionStoryTranfer122To13.ConvertTypeNameFrom122To133(type122);
            //Arg since the type has changed, some preprocessing is needed, this is done in code but for the test it will be handled as a one off
            //Setup for element copying, need prior knowledge of dpn and know what element is in it
            var prototypeNames = new List<string> { "UserDrawnPolygon.dml" };//Total hack, i have no clue how get get the file name with the dml without changing dml format
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(1, DepictionAccess.ElementLibrary.PrototypeCount);

            Assert.AreEqual(expectedZOI, oldZOI);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(1, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            var newElement = newDepiction.CompleteElementRepository.ElementsGeoLocated[0];
            var loadedZOI = newElement.ZoneOfInfluence.ToString();
            //Drat due to some format changes, the new polygon doesn't have the last point
            var zoiNew = new ZoneOfInfluence(GeometryToOgrConverter.WktToZoiGeometry(expectedZOI));

            Assert.AreEqual(zoiNew.ToString(), loadedZOI);
            Console.WriteLine(newDepiction.DepictionsFileName);
        }
        [Test]
        public void Does122DpnWithUnknownElementGetLoadedCorrectlyIn13()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.SingleElementZOI122.dpn");
            Assert.IsTrue(File.Exists(fileName));
            
            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldProtoLibrary = new ElementPrototypeLibrary();
            try
            {
                oldProtoLibrary.SetLoadedPrototypesFromPath(Path.Combine(unzipDir, "ElementTypes"), false);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something will go wrong on user created interactions and types");
                Console.WriteLine(ex.Message);
            }
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            oldPersistenceMemento.InherentPrototypes = oldProtoLibrary;
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(1, oldStory.ElementRepository.AllElements.Count);

            var element = oldStory.ElementRepository.AllElements[0];
            Assert.IsNotNull(element);
            Assert.IsNotNull(element.ZoneOfInfluence);
            var oldProps = element.Properties;
            var oldFillColor = Colors.Black;
            foreach (var prop in oldProps)
            {
                if (prop.Name.Equals("FillColor", StringComparison.InvariantCultureIgnoreCase))
                {
                    var val = prop.Value;
                    oldFillColor = (Color)ColorConverter.ConvertFromString(val.ToString());
                    break;
                }
            }
            Assert.AreNotEqual(Colors.Black, oldFillColor);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(1, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
            var newElement = newDepiction.CompleteElementRepository.ElementsGeoLocated[0];

            Color newColor;
            Assert.IsTrue(newElement.GetPropertyValue("ZOIFill", out newColor));

            Assert.AreEqual(oldFillColor, newColor);
        }

        [Test]
        public void Can122DpnWithSingleImageElementTurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.Image122Geoaligned.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(1, oldStory.ElementRepository.AllElements.Count);

            var element = oldStory.ElementRepository.AllElements[0];
            Assert.IsNotNull(element);
            var type122 = element.ElementType;

            var type13 = DepictionStoryTranfer122To13.ConvertTypeNameFrom122To133(type122);
            //            //Arg since the type has changed, some preprocessing is needed, this is done in code but for the test it will be handled as a one off
            //            //Setup for element copying, need prior knowledge of dpn and know what element is in it
            var prototypeNames = new List<string> { "Image.dml" };//Total hack, i have no clue how get get the file name with the dml without changing dml format
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(1, DepictionAccess.ElementLibrary.PrototypeCount);
            //
            //
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(newDepiction.DepictionGeographyInfo.DepictionRegionBounds.Bottom, 47.5831557397061);
            Assert.AreEqual(newDepiction.DepictionGeographyInfo.DepictionRegionBounds.Left, -122.379439);
            Assert.AreEqual(1, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
        }

        [Test]
        public void Can122DpnWithSingleUnAlignedImageElementTurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.Image122Ungeoaligned.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.UnregisteredElementRepository);
            Assert.AreEqual(1, oldStory.UnregisteredElementRepository.AllElements.Count);

            var element = oldStory.UnregisteredElementRepository.AllElements[0];
            Assert.IsNotNull(element);
            var type122 = element.ElementType;
            //Ug i guess images in 1.2.2 had a bug that caused their type to get changes when saved
            //            Assert.IsTrue(type122.ToLowerInvariant().Contains("image"));
            var type13 = DepictionStoryTranfer122To13.ConvertTypeNameFrom122To133(type122);
            //            //Arg since the type has changed, some preprocessing is needed, this is done in code but for the test it will be handled as a one off
            //            //Setup for element copying, need prior knowledge of dpn and know what element is in it
            var prototypeNames = new List<string> { "Image.dml" };//Total hack, i have no clue how get get the file name with the dml without changing dml format
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(1, DepictionAccess.ElementLibrary.PrototypeCount);
            //
            //
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(newDepiction.DepictionGeographyInfo.DepictionRegionBounds.Bottom, 47.5831557397061);
            Assert.AreEqual(newDepiction.DepictionGeographyInfo.DepictionRegionBounds.Left, -122.379439);
            Assert.AreEqual(1, newDepiction.CompleteElementRepository.ElementsUngeoLocated.Count);
            var newElement = newDepiction.CompleteElementRepository.ElementsUngeoLocated[0];
            Assert.IsFalse(newElement.IsGeolocated);
        }


        [Test]
        public void Can122DpnWithRevealerTurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.SingleElementInRevealer122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);

            Assert.AreEqual(oldStory.RegionExtent.Bottom, 47.5831557397061);
            Assert.AreEqual(oldStory.RegionExtent.Left, -122.379439);
            Assert.IsNotNull(oldStory.ElementRepository);
            Assert.AreEqual(1, oldStory.ElementRepository.AllElements.Count);
            Assert.AreEqual(1, oldVisualData.RevealerCollection.Count);

            //Setup for element copying, need prior knowledge of dpn and know what element is in it
            var prototypeNames = new List<string> { "Building.dml" };
            DMLLoadingHelper(prototypeNames);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(1, newDepiction.Revealers.Count);
            Assert.AreEqual(1, newDepiction.Revealers[0].ElementIdsInDisplayer.Count);
            Assert.AreEqual(oldVisualData.RevealerCollection[0].FriendlyName, newDepiction.Revealers[0].DisplayerName);
        }

        [Test]
        public void Can122DpnWithChangedRoadNetworkTurnIntoA13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.RoadNetworkSmall122.dpn");
            Assert.IsTrue(File.Exists(fileName));
            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);
            var elemCount = oldStory.ElementRepository.AllElements.Count;

            //DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(temp.FolderName, "dml", true, assemblyName);
            var prototypeNames = new List<string> { "RoadNetwork.dml", "Explosion.dml" };
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(prototypeNames.Count, DepictionAccess.ElementLibrary.PrototypeCount);

            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);
            Assert.AreEqual(elemCount, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
        }
        [Test]
        public void Can122DpnWithKitchenSinkTurnInto13DepictionStory()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.ElevationPlusElements122.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);
            var elemCount = oldStory.ElementRepository.AllElements.Count;
          
            //DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(temp.FolderName, "dml", true, assemblyName);
            var prototypeNames = new List<string> { "Elevation.dml","Antenna.dml","ChangeGroundHeight.dml",
                "Flood.dml","LineOfSight.dml","Sensor.dml","FluidFlow.dml","WaterBarrier.dml"};//Total hack, i have no clue how get get the file name with the dml without changing dml format
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(prototypeNames.Count, DepictionAccess.ElementLibrary.PrototypeCount);
            //            //
            //            //
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);

            Assert.AreEqual(elemCount, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
        }
        [Test]
        public void Can122DpnWithWeatherLoadInto13()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.Dml122WithBadWeather.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);
            var elemCount = oldStory.ElementRepository.AllElements.Count;

            var prototypeNames = new List<string> { "USNationalWeatherService24HourForecast.dml" };//Total hack, i have no clue how get get the file name with the dml without changing dml format
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(prototypeNames.Count, DepictionAccess.ElementLibrary.PrototypeCount);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);

            Assert.AreEqual(elemCount, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
        }
        [Test]
        public void Can122DpnWithAntennaLoadInto13()
        {
            var fileName = Path.Combine(temp.FolderName, "Depiction.UnitTests.Serialization.Dml122WithBadAntenna.dpn");
            Assert.IsTrue(File.Exists(fileName));

            DepictionPersistenceHelper.UnzipFileToDirectory(fileName, unzipDir);
            var oldPersistenceMemento = StoryPersistence12.LoadUnzipped122StoryFromFolder(unzipDir, fileName, DepictionCoreTypes.StringToTypeDictionaryFor122To13Conversion);
            var oldStory = oldPersistenceMemento.Story;
            var oldVisualData = oldPersistenceMemento.VisualData;
            Assert.IsNotNull(oldStory);
            var elemCount = oldStory.ElementRepository.AllElements.Count;

            var prototypeNames = new List<string> { "Antenna.dml", "StagingArea.dml" };//Total hack, i have no clue how get get the file name with the dml without changing dml format
            DMLLoadingHelper(prototypeNames);
            Assert.AreEqual(prototypeNames.Count, DepictionAccess.ElementLibrary.PrototypeCount);
            var newDepiction = DepictionStoryTranfer122To13.TranformStoryTo13Story(oldPersistenceMemento, fileName);

            Assert.AreEqual(elemCount, newDepiction.CompleteElementRepository.ElementsGeoLocated.Count);
        }
    }
}