﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.ValueTypes;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.Serialization
{
    [TestFixture]
    public class SerializationServiceTests
    {
        private TempFolderService temp;

        [SetUp]
        public void CreateTempDir()
        {
            temp = new TempFolderService(true);
        }
        [TearDown]
        public void DeleteTempDir()
        {
            temp.Close();
        }

        [Ignore]
        public void TypeAndClassTest()
        {
            var save = new SimpleSaveObject();
            var localName = "SimpleSave";
            var fileName = temp.GetATempFilenameInDir();
//            object o2 = Convert.ChangeType(o1, Type.GetType(sType)); 

            Console.WriteLine(save.GetType() + " the type");
            SerializationService.SaveToXmlFile < IXmlSerializable>(save, fileName, localName);
            var loaded = SerializationService.LoadFromXmlFile<IXmlSerializable>(fileName, localName);
            Console.WriteLine(loaded.GetType());
        }
        [Test]
        public void TypeDictionarySaveLoadTest()
        {
            var fileName = temp.GetATempFilenameInDir();
            var expectedValues = new Dictionary<string, string>();

            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            var objectList = new List<object>();
            objectList.Add(1);
            objectList.Add(new LatitudeLongitude());
            objectList.Add(2);
            foreach(var val in objectList)
            {
                var shortType = DepictionTypeInformationSerialization.AddTypeToDictionaryGetSimpleName(val.GetType());
                if(!expectedValues.ContainsKey(shortType))
                {
                    expectedValues.Add(shortType,val.GetType().AssemblyQualifiedName);
                }
            }
            DepictionTypeInformationSerialization.SaveSerializationServiceTypeDictionaryToFile(fileName);
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            Assert.AreEqual(0, DepictionTypeInformationSerialization.DictionaryCountForTests);
            DepictionTypeInformationSerialization.UpdateSerializationServiceTypeDictionaryWithFile(fileName);
            Assert.AreEqual(2, DepictionTypeInformationSerialization.DictionaryCountForTests);
            foreach(var key in expectedValues.Keys)
            {
                Assert.AreEqual(expectedValues[key], DepictionTypeInformationSerialization.GetFullTypeStringFromSimpleTypeString(key));
            }
        }

        [Test]
        public void SaveLoadFromXMLFileTest()
        {
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            var save = new SimpleSaveObject();
            var fileName = temp.GetATempFilenameInDir();
            var localName = "SimpleSave";
            SerializationService.SaveToXmlFile(save, fileName, localName);

            var loaded =  SerializationService.LoadFromXmlFile<SimpleSaveObject>(fileName, localName);
            Assert.AreEqual(save.NumToSave, loaded.NumToSave);
            Assert.AreEqual(save.StringToSave,loaded.StringToSave);
            save.NumToSave = 2;
            save.StringToSave = "Not saving";
            Assert.AreNotEqual(save.NumToSave, loaded.NumToSave);
            Assert.AreNotEqual(save.StringToSave, loaded.StringToSave);

            var fileName1 = temp.GetATempFilenameInDir();
            SerializationService.SaveToXmlFile(save, fileName1, localName);

            var loaded1 = SerializationService.LoadFromXmlFile<SimpleSaveObject>(fileName1, localName);

            Assert.AreEqual(save.NumToSave, loaded1.NumToSave);
            Assert.AreEqual(save.StringToSave, loaded1.StringToSave);
            
        }

        [Test(Description="this test sucks, and doesn't really prove the that the serializable dictionary works, especially not with more complicated classes")]
        public void SaveLoadSerializableDictonaryTests()
        {
            SerializableDictionary<string,double> dict = new SerializableDictionary<string, double>();
            string key1 = "first";
            string key2 = "second";
            double val1 = 1;
            double val2 = 2;
            dict.Add(key1,val1);
            dict.Add(key2,val2);

            Assert.AreEqual(2,dict.Count);
            var fileName = temp.GetATempFilenameInDir();
            var localName = "Dictionary";
            SerializationService.SaveToXmlFile(dict, fileName, localName);

            var loaded = SerializationService.LoadFromXmlFile<SerializableDictionary<string, double>>(fileName, localName);
            Assert.AreEqual(2,loaded.Count);
            Assert.AreEqual(loaded[key1],val1);
            Assert.AreEqual(loaded[key2], val2);

        }

        #region Simple things to save

        public class SimpleSaveObject : IXmlSerializable
        {
            public double NumToSave { get; set; }
            public string StringToSave { get; set; }

            public SimpleSaveObject()
            {
                NumToSave = 1d;
                StringToSave = "save me";
            }
            public XmlSchema GetSchema()
            {
                throw new NotImplementedException();
            }

            public void ReadXml(XmlReader reader)
            {
                var ns = SerializationConstants.DepictionXmlNameSpace;
                NumToSave = 0d;
                StringToSave = "default me";
                reader.ReadStartElement();
                if (reader.Name.Equals("NumToSave"))
                    NumToSave = reader.ReadElementContentAsDouble("NumToSave", ns);
                if (reader.Name.Equals("StringToSave"))
                    StringToSave = reader.ReadElementContentAsString("StringToSave", ns);//A good test would be to read the incorrect name
                //in case there is extra junk, just keep reading (davidl)
                while (!reader.NodeType.Equals(XmlNodeType.EndElement))
                {
                    reader.Read();
                }
                reader.ReadEndElement();
            }

            public void WriteXml(XmlWriter writer)
            {
                var ns = SerializationConstants.DepictionXmlNameSpace;
                writer.WriteStartElement("SimpleSaveElement");
                //New stuff
                writer.WriteElementString("NumToSave", ns, NumToSave.ToString());
                //end
                writer.WriteElementString("StringToSave", ns, StringToSave);
                writer.WriteEndElement();
            }
        }

        #endregion

    }
}
