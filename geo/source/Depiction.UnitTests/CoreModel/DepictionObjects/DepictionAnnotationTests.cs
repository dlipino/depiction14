﻿using System.IO;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionAnnotationTests
    {
        [Test]
        public void DoesDepictionAnnotationSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var annotation = new DepictionAnnotation();
            annotation.MapLocation = new LatitudeLongitude(35,24);
            annotation.AnnotationText = "Test annotation";
            var file = temp.GetATempFilenameInDir();
            var localName = "SingleAnnotation";
            SerializationService.SaveToXmlFile(annotation, file, localName);

            var loadedAnnotation = SerializationService.LoadFromXmlFile<DepictionAnnotation>(file, localName);

            Assert.IsTrue(annotation.Equals(loadedAnnotation));

            if (File.Exists(file))
            {
                File.Delete(file);
            }
            //This should delete everything in the temp directory
            temp.Close();
        }
    }
}
