﻿using System.IO;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.MEFRepository;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.Serialization;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionElementTests
    {
        [Test]
        public void DoesDepictionElementParentSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);
            DepictionTypeInformationSerialization.ResetTypeDictionaryForTests();
            var elemParent = new DepictionElementParent();
            elemParent.Tags.Add("Fist tag");
            elemParent.UsePermaText = true;
            elemParent.AddWaypoint(new DepictionElementWaypoint(){Name = "Name"});
            Assert.IsFalse(elemParent.Equals(null));//A quick null check, should probably be a different test
            Assert.IsFalse(elemParent.IsGeolocated);
            Assert.AreEqual(1, elemParent.Waypoints.Length);
            var file = temp.GetATempFilenameInDir();
            var localName = "DepictionElement";

            SerializationService.SaveToXmlFile(elemParent, file, localName);
            try
            {
                var loadedElemParent = SerializationService.LoadFromXmlFile<DepictionElementParent>(file, localName);
                Assert.IsNotNull(loadedElemParent);
                Assert.IsTrue(Equals(elemParent, loadedElemParent));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
        [Test]
        public void DoesInvalidElementPropertyReturnNull()
        {
            var element = new DepictionElementParent();
            Assert.IsNull(element.GetPropertyByInternalName("None", true));
        }
        [Test]
        public void DoesInvalidLatLongResetElementZOI()
        {
            var latLong = new LatitudeLongitude(1, 3);
            var geom = new DepictionGeometry(latLong);
            var element = new DepictionElementParent();
            element.Position = latLong;
            Assert.AreEqual(latLong,element.Position);
            Assert.AreEqual(geom,element.ZoneOfInfluence.Geometry);
            element.Position = null;
            Assert.IsFalse(element.Position.IsValid);
            var emptyGeom = new DepictionGeometry();
            Assert.AreEqual(emptyGeom,element.ZoneOfInfluence.Geometry);
        }

        [Test]
        public void CreateShapePostSetActionTests()
        {//OK i have no idea where this test should actually go since it is current testing 2 things, the actual action
            //and the method of getting the action. Current duplicates the explosion
            AddinRepository.Compose();
            var elemParent = new DepictionElementParent();
            elemParent.SetInitialPositionAndZOI(new LatitudeLongitude(0, 0), null);
            //            elemParent.Position = new LatitudeLongitude(0, 0);
            var postSetAction = new SerializableDictionary<string, string[]> { { "CreateShape", new[] { ".Radius", ".Shape" } } };
            var propWithPostSetAction = new DepictionElementProperty("Radius", "Radius", new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 10), null, postSetAction);
            elemParent.AddPropertyOrReplaceValueAndAttributes(propWithPostSetAction);
            var otherProp = new DepictionElementProperty("Shape", "Circle");
            elemParent.AddPropertyOrReplaceValueAndAttributes(otherProp);
            elemParent.SetPropertyValue("Radius", new Distance(MeasurementSystem.Metric, MeasurementScale.Normal, 3), true);
        }
        [Test]
        public void ModifyingElementZOIUpdatesPositionWhenRequested()
        {
            var defaultPosition = new LatitudeLongitude(1, 2);
            var element = new DepictionElementParent(); //Create default
            var linearRing = UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
                                              {
                                                  new Coordinate(0, 0), new Coordinate(0, 10), new Coordinate(10, 10),
                                                  new Coordinate(10, 0),
                                                  new Coordinate(0, 0)
                                              });
            var centroid = linearRing.Centroid;
            var centroidLatLong = new LatitudeLongitude(centroid.Y, centroid.X);

            Assert.IsNotNull(element);

            element.SetInitialPositionAndZOI(null, new ZoneOfInfluence(new DepictionGeometry(linearRing)));
            Assert.AreEqual(centroidLatLong, element.Position);

            element.SetInitialPositionAndZOI(defaultPosition, null);
            Assert.AreEqual(defaultPosition, element.Position);

            element.SetZOIGeometryAndUpdatePosition(new DepictionGeometry(linearRing));
            Assert.AreEqual(centroidLatLong, element.Position);

            element.SetInitialPositionAndZOI(defaultPosition, null);
            Assert.AreEqual(defaultPosition, element.Position);
            element.SetZOIGeometry(new DepictionGeometry(linearRing));
            Assert.AreEqual(defaultPosition, element.Position);
        }
        [Test]
        public void ShiftingPositionOfElementWithZOIShiftsZOI()
        {
            var element = new DepictionElementParent(); //Create default
            //X = Long Y = Lat
            double lCoordX = 0;
            double rCoordX = 8;
            double tCoordY = 10;
            double bCoordY = 0;

            double startLat = tCoordY/2;
            double startLong = rCoordX/2;
            var defaultPosition = new LatitudeLongitude(startLat, startLong);

            var linearRing =
            UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
				         	{
				         		new Coordinate(lCoordX, bCoordY), new Coordinate(lCoordX, tCoordY), new Coordinate(rCoordX, tCoordY), new Coordinate(rCoordX, bCoordY),
				         		new Coordinate(lCoordX, bCoordY)
				         	});
            var centroid = linearRing.Centroid;
            var centroidLatLong = new LatitudeLongitude(centroid.Y, centroid.X);
            Assert.AreEqual(defaultPosition,centroidLatLong);
            Assert.IsNotNull(element);

            element.SetInitialPositionAndZOI(defaultPosition,new ZoneOfInfluence(new DepictionGeometry(linearRing)));

            Assert.AreEqual(defaultPosition, element.Position);
            var newLat = 1d;
            var newLong = 2d;
            var newPosition = new LatitudeLongitude(newLat, newLong);
            element.Position = newPosition;

            Assert.AreEqual( newPosition,element.Position);
            var difLat = newLat - startLat;
            var difLong = newLong - startLong;

            var shiftedlinearRing =
                UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
				         	{
				         		new Coordinate(lCoordX+difLong, bCoordY+difLat), new Coordinate(lCoordX+difLong, tCoordY+difLat), 
                                new Coordinate(rCoordX+difLong, tCoordY+difLat), new Coordinate(rCoordX+difLong, bCoordY+difLat),
				         		new Coordinate(lCoordX+difLong, bCoordY+difLat)
				         	});
            ZoneOfInfluence zone2 = new ZoneOfInfluence(new DepictionGeometry(shiftedlinearRing));
            centroid = shiftedlinearRing.Centroid;
            centroidLatLong = new LatitudeLongitude(centroid.Y, centroid.X);
            Assert.AreEqual(zone2, element.ZoneOfInfluence);
            Assert.AreEqual(centroidLatLong,element.Position);
        }

        [Test]
        public void ElementPositionShiftUpdatesClosedZOI()
        {
            var element = new DepictionElementParent(); //Create default
            //X = Long Y = Lat
            double lCoordX = 0;
            double rCoordX = 8;
            double tCoordY = 10;
            double bCoordY = 0;

            double startLat = tCoordY / 2;
            double startLong = rCoordX / 2;
            var defaultPosition = new LatitudeLongitude(startLat, startLong);

            var linearRing =
            UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
				         	{
				         		new Coordinate(lCoordX, bCoordY), new Coordinate(lCoordX, tCoordY), new Coordinate(rCoordX, tCoordY), new Coordinate(rCoordX, bCoordY),
				         		new Coordinate(lCoordX, bCoordY)
				         	});
            var centroid = linearRing.Centroid;
            var centroidLatLong = new LatitudeLongitude(centroid.Y, centroid.X);
            Assert.AreEqual(defaultPosition, centroidLatLong);
            Assert.IsNotNull(element);

            element.SetInitialPositionAndZOI(defaultPosition, new ZoneOfInfluence(new DepictionGeometry(linearRing)));

            double shiftLat = 4;
            double shiftLong = -7;

            var newLatLong = new LatitudeLongitude(startLat + shiftLat, startLong + shiftLong);
            element.Position = newLatLong;
            var shiftZOI = new ZoneOfInfluence(new DepictionGeometry(linearRing));
            shiftZOI.ShiftZoneOfInfluence(new LatitudeLongitude(shiftLat,shiftLong));
            Assert.AreEqual(shiftZOI, element.ZoneOfInfluence);
        }
    }
}
