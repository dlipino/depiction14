﻿using System.IO;
using Depiction.CoreModel.DepictionObjects;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionObjects
{
    [TestFixture]
    public class DepictionStoryMetadataTests
    {
        [Test]
        public void DoesDepictionStoryMetadataSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var metaData = new DepictionStoryMetadata();
            metaData.SetMetadata("test depiction","no authoer","brief description");

            var file = temp.GetATempFilenameInDir();
            var localName = "Metadata";
            SerializationService.SaveToXmlFile(metaData,file,localName);

            var loadedMetadata = SerializationService.LoadFromXmlFile<DepictionStoryMetadata>(file, localName);

            Assert.IsTrue(metaData.Equals(loadedMetadata));
            if(File.Exists(file))
            {
                File.Delete(file);
            }


            temp.Close();
        }
    }
}
