﻿using System.IO;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.DepictionPersistence
{
    [TestFixture]
    public class DepictionSubtreeInfoTests
    {
        [Test]
        public void DoesDepictionDefaultSubtreeInfoSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionSubtreeInfoBase();
            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionSubtreeInfoBase>(file, localName);

                Assert.IsTrue(Equals(toSave,loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
        [Test]
        public void DoesDepictionSubtreeInfoSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionSubtreeInfoBase("", "", "DepictionMetadata.xml", "DepictionInformation", typeof(double));
            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionSubtreeInfoBase>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
