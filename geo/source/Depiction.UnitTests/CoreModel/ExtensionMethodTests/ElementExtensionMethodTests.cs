﻿using System;
using System.IO;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.ExtensionMethodTests
{
    [TestFixture]
    public class ElementExtensionMethodTests
    {
        [Test]
        public void DoesTheSameCommandGetAddedTwice()
        {
            var elemPar = new DepictionElementParent();
            var cmd = new DelegateCommand<IDepictionElement>(RandomCommand);
            cmd.Text = "Send Message";

            var commands = elemPar.ElementCommands;
            if (!commands.Contains(cmd))
            {
                elemPar.AddCommandToElement(cmd);
            }
        }

        private void RandomCommand(IDepictionElement obj)
        {
            Console.WriteLine(obj.ElementKey);
        }
    }
}
