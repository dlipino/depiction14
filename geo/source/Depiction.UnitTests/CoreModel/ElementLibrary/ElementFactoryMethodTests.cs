﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Depiction.API;
using Depiction.API.DepictionComparers;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.ElementLibrary
{
    [TestFixture]
    public class ElementFactoryMethodTests
    {
        private TempFolderService tempFolder;

        [SetUp]
        public void CreateElementDMLFiles()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder = new TempFolderService(true);
           DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
//           DefaultResourceCopier.CopyAssemblyEmbeddedResourcesToDirectory(temp.FolderName, "dml", true, assemblyName);
        }
        [TearDown]
        public void TestCleanup()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();
        }
        [Test]
        public void DoNonCustomPropertiesOfCopiedElementsGetTransfered()
        {
            var changeVal = true;
            var prototypeToTest = new ElementPrototype("None", "Thing");
            prototypeToTest.UsePermaText = changeVal;
            prototypeToTest.UsePropertyNameInHoverText = changeVal;
            prototypeToTest.UseEnhancedPermaText = changeVal;
            var element = ElementFactory.CreateDesiredTypeFromElementBase<DepictionElementParent>(prototypeToTest);
            Assert.IsTrue(element.UsePermaText);
            Assert.IsTrue(element.UsePropertyNameInHoverText);
            Assert.IsTrue(element.UseEnhancedPermaText);
        }
        [Test]
        public void ArbitraryDMLFileLoadingTest()
        {
            var elementFiles = new List<string> { "NonWorkingAddonDML.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, true);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("FuncionalDML");
            Assert.IsNotNull(createdPrototype);
           
        }
        [Test]
        public void CreateSimpleElementFromSimplePrototypeGivesElementAccessiblePropertiesTests()
        {
            var elementFiles = new List<string> { "ValidTestElement.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, true);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("ValidTestElement");
            //Interesting, why does this one work?
            Assert.AreEqual(6, createdPrototype.OrderedCustomProperties.Length);
            var defaultElem = new DepictionElementParent();
            var compare = new PropertyComparers.InternalPropertyNameComparer<IElementProperty>();
            var matches = createdPrototype.OrderedCustomProperties.Union(defaultElem.OrderedCustomProperties,compare);
            var count = matches.Count();
            
            var elementFromProto = ElementFactory.CreateElementFromPrototype(createdPrototype);

            Assert.AreEqual(count, elementFromProto.OrderedCustomProperties.Length);
        }
        [Test]
        public void DoPropertyValuesFromDMLStayWhenNewElementIsCreatedTest()
        {
            var elementFiles = new List<string> { "RouteUserDrawn.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, false);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("RouteUserDrawn");
            //Interesting, why does this one work?
            Assert.IsNotNull(createdPrototype);
            bool showIcon = true;
            Assert.IsTrue(createdPrototype.GetPropertyValue("ShowIcon", out showIcon));
            Assert.IsFalse(showIcon);

            var element = ElementFactory.CreateElementFromPrototype(createdPrototype);
            Assert.IsNotNull(element);
            showIcon = true;
            Assert.IsTrue(element.GetPropertyValue("ShowIcon", out showIcon));
            Assert.IsFalse(showIcon);

            //Another way to create an elemetn
            var otherElement = ElementFactory.CreateElementFromTypeString("RouteUserDrawn");
            Assert.IsNotNull(otherElement);
            showIcon = true;
            Assert.IsTrue(otherElement.GetPropertyValue("ShowIcon", out showIcon));
            Assert.IsFalse(showIcon);
        }
        [Test]
        public void DoPropertyAttributesFromDMLStayWhenNewElementIsCreatedTest()
        {
            var elementFiles = new List<string> { "RouteUserDrawn.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, false);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("RouteUserDrawn");
            //Interesting, why does this one work?
            Assert.IsNotNull(createdPrototype);
            var prop = createdPrototype.GetPropertyByInternalName("ShowIcon");
            Assert.IsNotNull(prop);
            Assert.IsFalse(prop.VisibleToUser);

            var element = ElementFactory.CreateElementFromPrototype(createdPrototype);
            Assert.IsNotNull(element);
            prop = element.GetPropertyByInternalName("ShowIcon");
            Assert.IsNotNull(prop);
            Assert.IsFalse(prop.VisibleToUser);

            //Another way to create an elemetn
            var otherElement = ElementFactory.CreateElementFromTypeString("RouteUserDrawn");
            Assert.IsNotNull(otherElement);
            prop = otherElement.GetPropertyByInternalName("ShowIcon");
            Assert.IsNotNull(prop);
            Assert.IsFalse(prop.VisibleToUser);
        }

        [Test]
        public void DoesCreateElementFromPrototypeWork()
        {
            var elementFiles = new List<string> { "UserDrawnPolygon.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, true);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("UserDrawnPolygon");
            Assert.IsNotNull(createdPrototype);
            var createdElement = ElementFactory.CreateElementFromPrototype(createdPrototype);
            Assert.IsNotNull(createdElement);
        }

        [Test]
        public void DoesOlderDMLWithOlderTypesLoad()
        {
            var expedtedCount = 9 ;
            var elementFiles = new List<string> { "ValidOldDML.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, true);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("ValidOldDMl");
            Assert.IsNotNull(createdPrototype);
            object res;
            createdPrototype.GetPropertyValue("IconPath", out res);
            Assert.AreEqual(typeof(DepictionIconPath), res.GetType());
            Assert.AreEqual(expedtedCount, createdPrototype.OrderedCustomProperties.Length);
        }
        [Test]
        public void DoesOlderDMLWithOlderTypesLoadAndSaveThenReload()
        {
            var expedtedCount = 9 ;
            var elementFiles = new List<string> { "ValidOldDML.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, true);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("ValidOldDMl");
            Assert.IsNotNull(createdPrototype);
            Assert.AreEqual(expedtedCount, createdPrototype.OrderedCustomProperties.Length);
            var newFileName = Path.Combine(tempFolder.FolderName, "Depiction.UnitTests.CoreModel.ElementLibrary.ValidOldSavedDML.dml");
            UnifiedDepictionElementWriter.WriteIDepictionElementBaseToFile(createdPrototype, newFileName);

            var newPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(newFileName);
            Assert.IsNotNull(newPrototype);
            Assert.AreEqual(expedtedCount, newPrototype.OrderedCustomProperties.Length);
        }

        [Test]
        public void DoesOlderDMLWithOlderTypesAndTypeValidationLoad()
        {
            var expedtedCount = 4;
            var elementFiles = new List<string> { "ValidOldDMLWithValidation.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(elementFiles, tempFolder, true);
            var createdPrototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("ValidOldDMLWithValidation");
            Assert.IsNotNull(createdPrototype);
            Assert.AreEqual(expedtedCount, createdPrototype.OrderedCustomProperties.Length);
            var propertiesDeepClone = createdPrototype.GetPropertyListClone();
            foreach (var prop in propertiesDeepClone)
            {
                var rules = prop.ValidationRules;
                if(rules != null)
                {
                    foreach(var rule in rules)
                    {
                        Assert.IsTrue(rule.IsRuleValid);
                    }
                }
            }
        }

        #region updating elemetns tests
        
        [Test]
        public void UpdateChangesElementProperties()
        {
            var propName = "test";
            var value = 1;
            var firstElement = new DepictionElementParent();
            firstElement.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty(propName, value));

            UnitTestHelperMethods.PropertyTester(propName, value, firstElement);

            var value1 = 4;
            var secondElement = ElementFactory.CreateRawPrototype();
            secondElement.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty(propName, value1));

            ElementFactory.UpdateElementWithPrototypeProperties(firstElement, secondElement, false,true);

            UnitTestHelperMethods.PropertyTester(propName, value1, firstElement);
        }

        [Test]
        public void UpdateElementDoesNotNeedToGeocode()
        {
            var propName = "test";
            var propVal = 1;
            
            var posValue = new LatitudeLongitude(10,20);
            var firstElement = new DepictionElementParent();
            firstElement.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty(propName, propVal));
            firstElement.SetInitialPositionAndZOI(posValue,null);
            UnitTestHelperMethods.PropertyTester(propName, propVal, firstElement);

            var value1 = 4;
            var secondElement = ElementFactory.CreateRawPrototype();
            secondElement.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty(propName, value1));

            ElementFactory.UpdateElementWithPrototypeProperties(firstElement, secondElement, false,true);

            UnitTestHelperMethods.PropertyTester(propName, value1, firstElement);
            Assert.AreEqual(posValue,firstElement.Position);
        }


        #endregion



        [Test]
        [Ignore("Nice test to have, but doesn't really test any of the app, just the test element load process")]
        public void DoesReadFromDirectoryGetCorrectNumberOfPrototypes()
        {
            var path = tempFolder.FolderName;
            Assert.IsTrue(Directory.Exists(path));
            var list = ElementFactory.GetAllPrototypesFromDirectory(path,null);
            Assert.AreEqual(4,list.Count);
        }
    }
}
