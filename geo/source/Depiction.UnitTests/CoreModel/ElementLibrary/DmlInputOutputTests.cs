using System.Collections.Generic;
using Depiction.API;
using Depiction.CoreModel.ElementLibrary;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.CoreModel.ElementLibrary
{
    [TestFixture]
    public class DmlInputOutputTests
    {
        private TempFolderService tempFolder;
        private string DefaultPrototypeOrigin = "Default";
        [SetUp]
        public void TestSetup()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder = new TempFolderService(true);
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
        }
        [TearDown]
        public void TestCleanUp()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();
        }

        [Test]
        public void AreLoadedDMLsGivenTheCorrectPrototypeOrigin()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var prototype = DepictionAccess.ElementLibrary.GuessPrototypeFromString("Person");
            Assert.IsNotNull(prototype);
            Assert.AreEqual(DefaultPrototypeOrigin,prototype.PrototypeOrigin);

        }
    }
}