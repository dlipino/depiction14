﻿using System.IO;
using Depiction.API.HelperObjects;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.ValueTypes
{
    [TestFixture]
    public class IconPathTests
    {
        [Test]
        public void DoesDepictionSpecificsSaveLoad()
        {
            //This is more of a sanity check than an actual unit test.
            var temp = new TempFolderService(true);

            var toSave = new DepictionIconPath();
            toSave.Path = "random.png";
            Assert.IsFalse(Equals(toSave, null));
            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<DepictionIconPath>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
