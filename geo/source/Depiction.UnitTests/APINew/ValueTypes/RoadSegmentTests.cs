﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Depiction.API.ValueTypes;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.ValueTypes
{
    [TestFixture]
    public class RoadSegmentTests
    {
        [Test]
        public void DoesRoadSegmentSaveLoad()
        {
            var temp = new TempFolderService(true);

            
            var node = new RoadNode();
            node.NodeID = 1;
            node.Vertex = new LatitudeLongitude(1, 2);

            var node2 = new RoadNode();
            node2.NodeID = 2;
            node2.Vertex = new LatitudeLongitude(3, 4);

            var toSave = new RoadSegment(node, node2);

            var file = temp.GetATempFilenameInDir();
            var localName = "SaveElement";

            SerializationService.SaveToXmlFile(toSave, file, localName);
            try
            {
                var loaded = SerializationService.LoadFromXmlFile<RoadSegment>(file, localName);

                Assert.IsTrue(Equals(toSave, loaded));
            }
            finally
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
                temp.Close();
            }
        }
    }
}
