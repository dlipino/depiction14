﻿using System.Globalization;
using System.Windows.Documents;
using Depiction.API.DialogBases.TextConverters;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.ConverterTests
{
    [TestFixture]
    public class DocumentConversionTests
    {
        [Test]
        public void DoesStringNormalStringWithHTMLTagsKeepNewLines()
        {
            var converter = new InputStringToFlowDocumentConverter();
            var workingNormal = "displayname: APRS Station\r\nauthor: Depiction, Inc";
            var converted = converter.Convert(workingNormal, typeof(FlowDocument), null, CultureInfo.CurrentCulture) as FlowDocument;
            Assert.IsNotNull(converted);
            Assert.AreEqual(2,converted.Blocks.Count);


            var workingHTML =
                "<!DOCTYPE html><html><body><p>This is a paragraph.</p><p>This is a paragraph.</p><p>This is a paragraph.</p></body></html>";
            converted = converter.Convert(workingHTML, typeof(FlowDocument), null, CultureInfo.CurrentCulture) as FlowDocument;
            Assert.IsNotNull(converted);
            Assert.AreEqual(3, converted.Blocks.Count);
            var count = converted.Blocks.Count;
            var baseTestSTring = "displayname: APRS Station<br/>Speed : 0KTS<br/>";
            converted = converter.Convert(baseTestSTring, typeof(FlowDocument), null, CultureInfo.CurrentCulture) as FlowDocument;
            Assert.IsNotNull(converted);
            var paragraph = converted.Blocks.FirstBlock as Paragraph;
            Assert.IsNotNull(paragraph);
            //Lines plus line breaks
            Assert.AreEqual(4, paragraph.Inlines.Count);

            var mixedString = "displayname: APRS Station\r\nSpeed : 0KTS<br/>";
            converted = converter.Convert(mixedString, typeof(FlowDocument), null, CultureInfo.CurrentCulture) as FlowDocument;
            Assert.IsNotNull(converted);
            paragraph = converted.Blocks.FirstBlock as Paragraph;
            Assert.IsNotNull(paragraph);
            //Lines plus line breaks
            Assert.AreEqual(4, paragraph.Inlines.Count);
         

        }
    }
}