using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.ExtensionMethods;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew.InteractionEngine
{
    [TestFixture]
    public class InteractionGraphTests
    {
        const string behaviorName = "TestBehavior";
        private const string selfChangePropName = "Changed";
        private const string objectElemName = "object";
        private const string redElemName = "red";
        private const string greenElemName = "green";
        #region helper methods

        private static ElementRepository AddStockElements()
        {
            var elementRepository = new ElementRepository();
            var elementList = new List<IDepictionElement> { CreateSimpleElement(objectElemName), 
                CreateSimpleElement("notPartOfGraph"), 
                CreateSimpleElement(redElemName), 
                CreateSimpleElement(greenElemName), 
                CreateSimpleElement(greenElemName) };
            foreach (var element in elementList)
            {
                elementRepository.AddElement(element);
            }
            return elementRepository;
        }

        private static IDepictionElement CreateSimpleElement(string elementType)
        {
            var element = new DepictionElementParent { ElementType = elementType };
            var prop = new DepictionElementProperty("DisplayName", string.Empty, "Mock","Mock", null, null);
            element.RemovePropertyIfNameAndTypeMatch(prop);
            element.AddPropertyOrReplaceValueAndAttributes(prop);
            element.SetInitialPositionAndZOI(new LatitudeLongitude(1, 2), null);
            return element;
        }

        private static IDepictionElement CreateElementWithTestPostSetAction(string elementType)
        {
            var element = new DepictionElementParent { ElementType = elementType };
            
            var postSetAction = new SerializableDictionary<string, string[]>();
            postSetAction.Add(behaviorName, new[] { selfChangePropName + ":false" });

            var prop = new DepictionElementProperty("DisplayName", string.Empty, "Mock", "Mock", null, postSetAction);
            element.RemovePropertyIfNameAndTypeMatch(prop);
            element.AddPropertyOrReplaceValueAndAttributes(prop);
            prop = new DepictionElementProperty(selfChangePropName, selfChangePropName, true, true, null, null);
            element.AddPropertyOrReplaceValueAndAttributes(prop);

            element.SetInitialPositionAndZOI(new LatitudeLongitude(1, 2), null);
            var elementProp = element.GetPropertyByInternalName("DisplayName");
            Assert.IsNotNull(elementProp);
            Assert.IsNotNull(elementProp.PostSetActions);
            return element;
        }

        private static List<DepictionInteractionMessage> GetDirtyList(IElementRepository elementRepository)
        {
            var dirtyList = new List<DepictionInteractionMessage>();
            foreach (var obj in elementRepository.AllElements) dirtyList.Add(new DepictionInteractionMessage(obj));
            return dirtyList;
        }

        private static IDepictionElement FindFirstElementByInternalTypeName(IElementRepository elements, string elementType)
        {
            try
            {
                var element =
                    elements.AllElements.FirstOrDefault(
                        t => t.ElementType.Equals(elementType, StringComparison.InvariantCultureIgnoreCase));

                return element;
            }
            catch
            {
                return null;
            }
        }
        private static IEnumerable<IDepictionElement> FindElementsByInternalTypeName(IElementRepository elements, string elementType)
        {
            try
            {
                var element =
                    elements.AllElements.Where(
                        t => t.ElementType.Equals(elementType, StringComparison.InvariantCultureIgnoreCase));

                return element;
            }
            catch
            {
                return null;
            }
        }
        #region Excess interacion rule creation methods from 1.2
        private static InteractionRule CreateNewInteractionRule(string triggerElementType, string affectedElementType)
        {
            return CreateNewInteractionRule(Guid.NewGuid().ToString(), triggerElementType, affectedElementType);
        }

        private static InteractionRule CreateNewInteractionRule(string ruleName, string triggerElementType, string affectedElementType)
        {
            var rule = new InteractionRule { Publisher = triggerElementType, Name = ruleName };
            rule.Subscribers.Add(affectedElementType);
            return rule;
        }

        private static InteractionRule CreateNewInteractionRule(string ruleName, string triggerElementType, string affectedElementType, string propertyName, string newValue)
        {
            var rule = new InteractionRule { Publisher = triggerElementType, Name = ruleName };
            rule.Subscribers.Add(affectedElementType);
            rule.Behaviors.Add(behaviorName, new[] { new ElementFileParameter { Type = ElementFileParameterType.Subscriber, ElementQuery = propertyName }, 
                new ElementFileParameter { Type = ElementFileParameterType.Value, ElementQuery = newValue } });
            return rule;
        }
        #endregion
        #endregion

        #region Setup/Teardown
        [SetUp]
        public void Setup()
        {
            var addinRepo = AddinRepository.Instance;
            var behaviours = addinRepo.GetBehaviors();
            Assert.IsNotNull(behaviours);
            behaviours.Clear();
            var key = new BehaviorAttribute(behaviorName, "tester", "unittest");
            var behavior = new TestBehavior();
            behaviours.Add(key, behavior);
        }

        [TearDown]
        public void TearDown()
        {
            AddinRepository.Decompose();
        }

        #endregion

        #region 1.3 tests

        [Test]
        public void SelfChangingElementTest()
        {
            var repo = new ElementRepository();
            var element = CreateElementWithTestPostSetAction("SelfTester");
            repo.AddElement(element);
            Assert.AreEqual(true, element.Query("."+selfChangePropName));
            element.SetPropertyValue("displayName", "other");
            Assert.AreEqual(false, element.Query("." + selfChangePropName));
        }
        [Test]
        public void DoesForegroundThreadRouterWithNoInputEndAfterItStarts()
        {
            var repo = new ElementRepository();
            var ruleRepo = new InteractionRuleRepository();
            var router = new ForegroundThreadRouter(repo, ruleRepo);

            var element = CreateElementWithTestPostSetAction("SelfTester");
            repo.AddElement(element);
            router.Begin();
        }

        [Test]
        public void DoesForegroundThreadRouterFinishWithSingleInput()
        {
            var repo = new ElementRepository();

            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var router = new ForegroundThreadRouter(repo, ruleRepo);

            repo.AddElement(CreateSimpleElement(objectElemName));
            repo.AddElement(CreateSimpleElement(redElemName));

            router.Begin();
        }
        [Test]
        public void DoesForegroundThreadRouterFinishWithMultipleInput()
        {
            var repo = new ElementRepository();

            var ruleRepo = new InteractionRuleRepository();
//            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            var router = new ForegroundThreadRouter(repo, ruleRepo);

            repo.AddElement(CreateSimpleElement(objectElemName));
            repo.AddElement(CreateSimpleElement(redElemName));
            repo.AddElement(CreateSimpleElement(redElemName));
            repo.AddElement(CreateSimpleElement(redElemName));
            repo.AddElement(CreateSimpleElement(redElemName));

            router.Begin();
        }

        #endregion


        #region tests From 1.2
        [Test]
        public void AreAllRulesAppliedAfterExecuting()
        {
            ElementRepository elementRepository = AddStockElements();
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));
           
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);

            Assert.AreEqual("Mock", FindFirstElementByInternalTypeName(elementRepository, greenElemName).Query(".DisplayName"));
            graph.ExecuteInteractions(GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
            var greens = FindElementsByInternalTypeName(elementRepository, greenElemName);
            Assert.AreEqual(2, greens.Count());
            foreach (var elem in greens)
            {
                Assert.AreEqual("greenie", elem.Query(".DisplayName"));
            }
            Assert.AreEqual("reddie", FindFirstElementByInternalTypeName(elementRepository, redElemName).Query(".DisplayName"));
            Assert.AreEqual("Mock", FindFirstElementByInternalTypeName(elementRepository, objectElemName).Query(".DisplayName"));
            Assert.AreEqual("Mock", FindFirstElementByInternalTypeName(elementRepository, "notPartOfGraph").Query(".DisplayName"));
           
        }

        [Test]
        public void AreOnlyDirtyElementsAffected()
        {
            var ruleRepo = new InteractionRuleRepository(); 
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            ElementRepository elementRepository = AddStockElements();
            var dirtyList = new List<DepictionInteractionMessage>();

            var greenElements = FindElementsByInternalTypeName(elementRepository, greenElemName);
            foreach(var element in greenElements)
            {
                var interactionMessage = new DepictionInteractionMessage(element);
                dirtyList.Add(interactionMessage);
            }
           
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(dirtyList);
            Assert.AreEqual(3, graph.VertexCount);
            graph.ExecuteInteractions(dirtyList);
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void DoesChangingATriggeredElementExecuteRule()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));

            ElementRepository elementRepository = AddStockElements();
            // all objects not dirty
            var interactionMessage =
                new DepictionInteractionMessage(FindFirstElementByInternalTypeName(elementRepository, "green"));
            var dirtyList = new List<DepictionInteractionMessage>{ interactionMessage };
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(dirtyList);
            Assert.AreEqual(2, graph.VertexCount);
            graph.ExecuteInteractions(dirtyList);
        }

        [Test]
        public void DoesExecuteInteractionsFinish()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule1", objectElemName, greenElemName, "DisplayName", "greenie"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("rule2", objectElemName, redElemName, "DisplayName", "reddie"));

            ElementRepository elementRepository = AddStockElements();
            //Order is important, i think
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(GetDirtyList(elementRepository));
            Assert.AreEqual(4, graph.VertexCount);
            graph.ExecuteInteractions(GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void DoesGraphBuilderReturnGraph()
        {

            // first rule object->green
            var rule1 = new InteractionRule { Publisher = "object", Name = "rule1" };
            rule1.Subscribers.Add("green");
            // rule2 object->red
            var rule2 = new InteractionRule { Publisher = "object", Name = "rule2" };
            rule2.Subscribers.Add("red");

            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(rule1);
            ruleRepo.AddInteractionRule(rule2);

            ElementRepository elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(GetDirtyList(elementRepository));
            Assert.AreEqual(4, graph.VertexCount);
            Assert.AreEqual(3, graph.EdgeCount);
        }

        [Test]
        public void DoesRebuildExcludeEdges()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "red"));

            ElementRepository elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(GetDirtyList(elementRepository));
            graph.ExcludeEdge(FindFirstElementByInternalTypeName(elementRepository, "object"), 
                              FindFirstElementByInternalTypeName(elementRepository, "green"), 
                              ruleRepo.InteractionRules[0]);
            graph.RebuildInterElementInteractionGraph();
            Assert.AreEqual(2, graph.EdgeCount);
        }

        [Test]
        public void DoesRebuildExcludeVertices()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "red"));

            ElementRepository elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(GetDirtyList(elementRepository));
            graph.ExcludeVertex(FindFirstElementByInternalTypeName(elementRepository, "red"));
            graph.RebuildInterElementInteractionGraph();
            Assert.AreEqual(3, graph.VertexCount);
            graph.ExcludeVertex(FindFirstElementByInternalTypeName(elementRepository, "green"));
            graph.RebuildInterElementInteractionGraph();
            Assert.AreEqual(2, graph.VertexCount);
            graph.ExcludeVertex(FindFirstElementByInternalTypeName(elementRepository, "object"));
            graph.RebuildInterElementInteractionGraph();
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void DoesTraversalTraverseEveryVertex()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "red"));

            ElementRepository elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.BuildChangeGraph(GetDirtyList(elementRepository));

            graph.ExecuteCurrentInteractionGraph();
            foreach (var vertex in graph.Vertices)
            {
                Assert.AreEqual(1, vertex.NumberIncomingEdgesFired);
            }
        }

        [Test]
        public void DoWeHandleCyclesAndCompleteAnyway()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("green", "red"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("red", "blue"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("blue", "green"));

            ElementRepository elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);
            graph.ExecuteInteractions(GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
        }

        [Test]
        public void IsReadonlyCollectionDynamicallyUpdated()
        {
            var ruleRepo = new InteractionRuleRepository();
            ReadOnlyCollection<IInteractionRule> rules = ruleRepo.InteractionRules;
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            Assert.AreEqual(1, rules.Count);
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("green", "red"));
            Assert.AreEqual(2, rules.Count);
        }

        [Test]
        public void SecondCycleTest()
        {
            var ruleRepo = new InteractionRuleRepository();
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("object", "green"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("green", "red"));
            ruleRepo.AddInteractionRule(CreateNewInteractionRule("red", "green"));

            ElementRepository elementRepository = AddStockElements();
            var graph = new InteractionGraph(elementRepository, ruleRepo.InteractionRules);

            graph.ExecuteInteractions(GetDirtyList(elementRepository));
            Assert.AreEqual(0, graph.VertexCount);
        }

        #endregion
    }
}