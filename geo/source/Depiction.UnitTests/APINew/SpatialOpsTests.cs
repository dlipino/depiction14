﻿using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ValueTypes;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

namespace Depiction.UnitTests.APINew
{
    [TestFixture(Description = "These tests come from original Depiction and im not sure how good they are.")]
    public class SpatialOpsTests
    {
        [Test]
        public void PointDepictionGeometryIntersectsGridTest()
        {
            var grid = new GridSpatialData(3, 3, new LatitudeLongitude(3, 0), new LatitudeLongitude(0, 3));

            grid.SetZ(1, 1, 1);

            Assert.IsTrue(SpatialOps.Intersects(grid, GetPointGeometry(1.1, 1.1)));
            Assert.IsTrue(SpatialOps.Intersects(grid, GetPointGeometry(1, 1)));
            Assert.IsTrue(SpatialOps.Intersects(grid, GetPointGeometry(1.9, 1.1)));
            Assert.IsTrue(SpatialOps.Intersects(grid, GetPointGeometry(1.99999, 1.999999)));

            Assert.IsFalse(SpatialOps.Intersects(grid, GetPointGeometry(0.9, 1.1)));
            Assert.IsFalse(SpatialOps.Intersects(grid, GetPointGeometry(1.1, 0.9)));
            Assert.IsFalse(SpatialOps.Intersects(grid, GetPointGeometry(0, 2.9)));
            Assert.IsFalse(SpatialOps.Intersects(grid, GetPointGeometry(2, 2)));

        }
        #region Helper methods
        private DepictionGeometry GetPointGeometry(double lat, double lon)
        {
            var geomFactory = new GeometryFactory();
            return new DepictionGeometry(geomFactory.CreatePoint(new Coordinate(lon, lat)));
        }
        #endregion
    }
}
