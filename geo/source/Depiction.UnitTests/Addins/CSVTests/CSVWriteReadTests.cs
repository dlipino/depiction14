using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes;
using Depiction.CSVExtension.Models;
using Depiction.CSVExtension.Models.Exporters;
using Depiction.CSVExtension.ViewModels;
using Depiction.Serialization;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;

namespace Depiction.UnitTests.Addins.CSVTests
{
    [TestFixture]
    public class CSVWriteReadTests
    {
        private TempFolderService tempFolder;
        //These tests should be in the addin that creates them. For now mash everything into the main depiction app.
        #region Setup/Teardown

        [SetUp]
        protected void Setup()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);
            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;

        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            DepictionAccess.ProductInformation = null;
            tempFolder.Close();
        }

        #endregion

        #region Round trip tests
        [Test]
        public void SimpleElementToCSVFilesAreRoundtrippable()
        {
            var geoCoder = new DepictionGeocodingService();
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var latLong = new LatitudeLongitude(1, 2);
            var file = tempFolder.GetATempFilenameInDir() + ".csv";
            var writer = new DepictionCSVExporter();
            var elemParent = ElementFactory.CreateElementFromTypeString("Person");
            Assert.IsNotNull(elemParent);
            elemParent.Position = latLong;
            writer.WriteElements(file, new[] { elemParent }, false);


            var csvReader = new CSVFileReadingService();
            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            locationPropHolder.LatitudeLongitude = "Position";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSingleField;
            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(file, string.Empty, null, false, locationPropHolder, locationModeType, geoCoder);
            Assert.AreEqual(1, prototypes.Count());
            var elements = new List<IDepictionElement>();
            foreach (var prototype in prototypes)
            {
                //                var createdElement = ElementFactory.CreateDesiredTypeFromElementBase<DepictionElementParent>(prototype as DepictionElementBase);
                var createdElement = ElementFactory.CreateElementFromPrototype(prototype);
                if (createdElement != null)
                {
                    elements.Add(createdElement);
                }
            }
            Assert.AreEqual(1, elements.Count());
            foreach (var element in elements)
            {
                Assert.AreEqual(elemParent.ElementType, element.ElementType);
                foreach (var prop in elemParent.OrderedCustomProperties)
                {
                    UnitTestHelperMethods.PropertyTester(prop.InternalName, prop.Value, element);
                }
                Assert.AreEqual(elemParent.ZoneOfInfluence, element.ZoneOfInfluence);
            }
        }

        [Test]
        public void DoesRoundTripCSVElementWithNoEIDUpdateNotChangeHoverTextOfElement()
        {
            ElementRepository tempRepo = new ElementRepository();
            var geoCoder = new DepictionGeocodingService();
            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            var locationModeType = CSVLocationModeType.None;

            var element = ElementFactory.CreateElementFromTypeString("PointOfInterest");
            Assert.IsNotNull(element);
//            var eid = "123";
//            var idProp = new DepictionElementProperty("eid", eid);
//            element.AddPropertyIfItDoesNotExist(idProp, true, false);
            //Remove all hovertext props and get all the props
            foreach (var oprop in element.OrderedCustomProperties)
            {
                oprop.IsHoverText = false;
            }
            Assert.AreEqual(0, CSVReadingTests.CountHoverTextProps(element));

            var propName = "Random";
            var prop = new DepictionElementProperty(propName, "something");
            var isHoverTextValue = true;
            prop.IsHoverText = isHoverTextValue;
            element.AddPropertyIfItDoesNotExist(prop, true, false);
            tempRepo.AddElement(element);

            var file = tempFolder.GetATempFilenameInDir() + ".csv";
           
            var writer = new DepictionCSVWriterService();
            Assert.IsFalse(element.Position.IsValid);
            string modChar = "a";
            writer.DoWriteElements(file, new[] { element },false,modChar);
            var csvReader = new CSVFileReadingService();
            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(file, string.Empty, null, false, locationPropHolder, locationModeType,geoCoder ).ToList();
            Assert.AreEqual(1, prototypes.Count);

            List<IDepictionElement> created;
            List<IDepictionElement> updated;
            tempRepo.AddOrUpdateRepositoryFromPrototypes(prototypes, true, out created, out updated);
            Assert.AreEqual(1,tempRepo.AllElements.Count);
            var outElement = tempRepo.AllElements.First();
            //Ensure the added prop is still there
            var outProp = outElement.GetPropertyByInternalName(propName);
            Assert.IsNotNull(outProp);
            //Ensure all the string props have changed
            foreach (var oprop in element.OrderedCustomProperties)
            {
                var val = oprop.Value as string;
                if(val != null && !oprop.InternalName.Equals("eid",StringComparison.InvariantCultureIgnoreCase))
                {
                    Assert.IsTrue(val.EndsWith(modChar));
                }
            }
            //Makes sure the one prop with hovertext is still hovertext
            Assert.AreEqual(1, CSVReadingTests.CountHoverTextProps(element));
        }

        [Test]
        public void SimpleElementWithNoPositionToCSVFilesAreRoundtrippable()
        {
            var geoCoder = new DepictionGeocodingService();
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var file = tempFolder.GetATempFilenameInDir() + ".csv";
            var writer = new DepictionCSVExporter();
            var elemParent = ElementFactory.CreateElementFromTypeString("Person");
            Assert.IsNotNull(elemParent);
            Assert.IsFalse(elemParent.Position.IsValid);
            writer.WriteElements(file, new[] { elemParent }, false);

            var csvReader = new CSVFileReadingService();

            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            locationPropHolder.LatitudeLongitude = "Position";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSingleField;

            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(file, string.Empty, null, false, locationPropHolder, locationModeType, geoCoder).ToList();
            Assert.AreEqual(1, prototypes.Count());
            var elements = new List<IDepictionElement>();
            foreach (var prototype in prototypes)
            {
                //                var createdElement = ElementFactory.CreateDesiredTypeFromElementBase<DepictionElementParent>(prototype as DepictionElementBase);
                var createdElement = ElementFactory.CreateElementFromPrototype(prototype);
                if (createdElement != null)
                {
                    elements.Add(createdElement);
                }
            }
            Assert.AreEqual(1, elements.Count());
            foreach (var element in elements)
            {
                Assert.AreEqual(elemParent.ElementType, element.ElementType);
                foreach (var prop in elemParent.OrderedCustomProperties)
                {
                    UnitTestHelperMethods.PropertyTester(prop.InternalName, prop.Value, element);
                }
                Assert.AreEqual(elemParent.ZoneOfInfluence, element.ZoneOfInfluence);
            }
        }
        [Test]
        public void SimpleElementWithEIDToCSVFilesAreRoundtrippable()
        {
            var geoCoder = new DepictionGeocodingService();
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var latLong = new LatitudeLongitude(1, 2);
            var file = tempFolder.GetATempFilenameInDir() + ".csv";
            var writer = new DepictionCSVExporter();
            var elemParent = ElementFactory.CreateElementFromTypeString("Person");
            Assert.IsNotNull(elemParent);
            elemParent.Position = latLong;
            var eidPropIntVal = 123456789983213423;
            var eidProp = new DepictionElementProperty("eid", eidPropIntVal);

            elemParent.AddPropertyOrReplaceValueAndAttributes(eidProp, false);
            object eidType;
            elemParent.GetPropertyValue("eid", out eidType);
            Assert.AreEqual(typeof(string),eidType.GetType());
            writer.WriteElements(file, new[] { elemParent }, false);

            var csvReader = new CSVFileReadingService();
            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            locationPropHolder.LatitudeLongitude = "Position";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSingleField;
            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(file, string.Empty, null, false, locationPropHolder, locationModeType, geoCoder);
            Assert.AreEqual(1, prototypes.Count());
            UnitTestHelperMethods.PropertyTester("eid", eidPropIntVal.ToString(), prototypes.First());
        }

        [Test]
        public void PolygonElementToCSVFilesAreRoundtrippable()
        {
            var geoCoder = new DepictionGeocodingService();
            var dmlsToLoad = new List<string> { "LoadedShapes.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var file = tempFolder.GetATempFilenameInDir() + ".csv";
            var writer = new DepictionCSVExporter();
            var elemParent = ElementFactory.CreateElementFromTypeString("Polygon");
            Assert.IsNotNull(elemParent);
            var linearRing = UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
				         	{
				         		new Coordinate(0, 0), new Coordinate(0, 10), new Coordinate(10, 10), new Coordinate(10, 0),
				         		new Coordinate(0, 0)
				         	});

            ZoneOfInfluence zone = new ZoneOfInfluence(new DepictionGeometry(linearRing));
            elemParent.SetInitialPositionAndZOI(null, zone);
            writer.WriteElements(file, new[] { elemParent }, false);

            var csvReader = new CSVFileReadingService();
            var locationPropHolder = new GeoLocationInfoToPropertyHolder();
            locationPropHolder.LatitudeLongitude = "Position";
            var locationModeType = CSVLocationModeType.LatitudeLongitudeSingleField;
            //            var elements = csvReader.ReadCSVFileAndCreatePrototypes(file, string.Empty, null, false, locationPropHolder, locationModeType, geoCoder);
            var prototypes = csvReader.ReadCSVFileAndCreatePrototypes(file, string.Empty, null, false, locationPropHolder, locationModeType, geoCoder);
            Assert.AreEqual(1, prototypes.Count());
            var elements = new List<IDepictionElement>();
            foreach (var prototype in prototypes)
            {
                //                var createdElement = ElementFactory.CreateDesiredTypeFromElementBase<DepictionElementParent>(prototype as DepictionElementBase);
                var createdElement = ElementFactory.CreateElementFromPrototype(prototype);
                if (createdElement != null)
                {
                    elements.Add(createdElement);
                }
            }
            Assert.AreEqual(1, elements.Count());
            foreach (var element in elements)
            {
                Assert.AreEqual(elemParent.ElementType, element.ElementType);
                foreach (var prop in elemParent.OrderedCustomProperties)
                {
                    UnitTestHelperMethods.PropertyTester(prop.InternalName, prop.Value, element);
                }
                Assert.AreEqual(elemParent.ZoneOfInfluence, element.ZoneOfInfluence);
            }

        }
        #endregion
        #region not used
        protected void CreateSimpleCSVFile(string fileName, List<string> headers, List<List<object>> valuesList)
        {
            using (var writer = File.CreateText(fileName))
            {
                var builder = new StringBuilder();
                var properties = headers;
                //write the header
                bool first = true;
                foreach (var property in properties)
                {
                    if (!first)
                    {
                        builder.Append(",");
                    }
                    else
                    {
                        first = false;
                    }
                    builder.Append(property);
                }


                foreach (var values in valuesList)
                {
                    builder.AppendLine();
                    first = true;
                    foreach (var val in values)
                    {
                        if (!first)
                        {
                            builder.Append(",");
                        }
                        else
                        {
                            first = false;
                        }
                        builder.Append(val.ToString());
                    }
                }

                writer.Write(builder.ToString());
                writer.Flush();
            }
        }
        #endregion
    }
}