﻿using System;
using System.IO;
using Depiction.API.ValueTypes;
using Depiction.Geocoders.GeocodeFarmGeocoder;
using NUnit.Framework;

namespace Depiction.UnitTests.Addins.GeocoderTests
{
    [TestFixture]
    public class GeocodeFarmGeocoderTests
    {
        [Test]
        [Ignore("Just testing the esxapedatastring method")]
        public void URIStringReplacementTest()
        {
            var addressString = "the end is net";
            var URI = "http://me.go";
            string url = string.Format("{0}&location={1}", URI, Uri.EscapeDataString(addressString));
        }
        [Test]
        public void GeocodeFarmXMLReadTest()
        {
            var dir = Environment.CurrentDirectory;
            var fullFileName = Path.Combine(dir, "Addins","GeocoderTests","GeocodeFarmRequestResult.xml");
            var expected = new LatitudeLongitude("47.7941209782830", "-122.356649576903");
            using (var fileStream = new FileStream(fullFileName, FileMode.Open))
            {
                var res = GeocodeFarmGeocoder.ParseXMLStreamResponse(fileStream);
                Assert.AreEqual(expected, res);
            }

        }
    }
}