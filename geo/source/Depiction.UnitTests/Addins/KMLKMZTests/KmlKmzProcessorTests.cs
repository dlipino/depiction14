using System.IO;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.Service.FileReading;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.Addins.KMLKMZTests
{
    [TestFixture]
    public class KmlKmzProcessorTests
    {
        string assemblyName = "Depiction.UnitTests";
        string fileStart = "Depiction.UnitTests.Addins.KMLKMZTests.";
        private TempFolderService temp;
        [SetUp]
        public void SetupTest()
        {
            temp = new TempFolderService(true);
        }
        [TearDown]
        public void Teardown()
        {
            temp.Close();
        }


        [Test]
        public void CanKMLImageBeLoaded()
        {
            
            var tempDir = temp.FolderName;

            var baseName = "AI8P_85";
            var imageName = baseName + ".png";
            var fileName = baseName + ".kml";
            var tempImageName = Path.Combine(tempDir, imageName);
            var tempFileName = Path.Combine(tempDir, fileName);
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + imageName, tempImageName);
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, fileStart + fileName, tempFileName);
            Assert.IsTrue(File.Exists(tempImageName));
            Assert.IsTrue(File.Exists(tempFileName));

            var kmlProcessor = new KmlKmzProcessor(tempFileName);
            var result = kmlProcessor.ProcessKmlKmzAndGetPrototypes(null, "", false);

        }
    }
}