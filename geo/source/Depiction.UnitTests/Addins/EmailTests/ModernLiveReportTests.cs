﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.StaticAccessors;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.LiveReports.Models;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.Addins.EmailTests
{
    //Not exactly the best thing to do, but the other tests were getting too big, and also the new
    //stuff doesnt really fit with the older tests, and most of the older tests 
    [TestFixture]
    public class ModernLiveReportTests
    {
        //        private TempFolderService tempFolder;
        //        string assemblyName = "Depiction.UnitTests";
        //        private string dmlStart = "Depiction.UnitTests.Addins.";
        //        private string tempFileName = string.Empty;
        //
        //        [SetUp]
        //        protected void Setup()
        //        {
        //            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
        //            tempFolder = new TempFolderService(true);
        //
        //            var prototypeNames = new List<string> { "Person.dml", "PointOfInterest.dml" };
        //            tempFileName = tempFolder.GetATempFilenameInDir();
        //            int count = 0;
        //            foreach (var name in prototypeNames)
        //            {
        //                DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, dmlStart + name, tempFileName);
        //                var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
        //                DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);
        //                count++;
        //            }
        //
        //            Assert.AreEqual(count, DepictionAccess.ElementLibrary.UserPrototypes.Count);
        //
        //            var testProduct = new TestProductInformation();
        //            DepictionAccess.ProductInformation = testProduct;
        //        }
        //        [TearDown]
        //        public void TearDown()
        //        {
        //            DepictionAccess.ProductInformation = null;
        //            DepictionAccess.ElementLibrary = null;
        //            tempFolder.Close();
        //        }

        [Test]
        public void TestDepictionEmailTagGetter()
        {
            var tags = "the tags";
            var origText = "non imporatn text: other things";
            string subject = string.Format(" {0} {1} {2} {3}", origText, EmailParser.depictionTagOpener,
                                           tags, EmailParser.depictionTagCloser);

            string cleanedSubject;
            var readTags = EmailParser.GetDepictionEmailTagsFromSubjectAndCleanSubject(subject, out cleanedSubject);
            Assert.AreEqual(tags, readTags);
            Assert.AreEqual(origText, cleanedSubject);
        }

        [Test]
        public void CanEmailParserMatchTags()
        {

            var tagRequests = new[] { "tag1", "tag2", "tag3" };
            var nonRequestTag = "tag4";
            //kitchen sink test for sanity
            var subjectTags = string.Format("{0},{1}  ,{2}", tagRequests[0], tagRequests[1], tagRequests[2]);
            string subject = string.Format(" nothing {0} {1} {2}", EmailParser.depictionTagOpener,
                                         subjectTags, EmailParser.depictionTagCloser);

            Assert.IsTrue(EmailParser.IsSubjectUsable(subject, tagRequests[0]));
            Assert.IsTrue(EmailParser.IsSubjectUsable(subject, tagRequests[1]));
            Assert.IsTrue(EmailParser.IsSubjectUsable(subject, tagRequests[2]));
            Assert.IsFalse(EmailParser.IsSubjectUsable(subject, nonRequestTag));
            Assert.IsTrue(EmailParser.IsSubjectUsable(subject, tagRequests[2] + "," + tagRequests[0]));

            //A simpler version of the test
            var subjectTagSimple = string.Format("{0}", tagRequests[0]);
            string simpleSubject = string.Format(" nothing {0} {1} {2}", EmailParser.depictionTagOpener,
                                        subjectTagSimple, EmailParser.depictionTagCloser);
            Assert.IsTrue(EmailParser.IsSubjectUsable(simpleSubject, tagRequests[0]));
            Assert.IsTrue(EmailParser.IsSubjectUsable(simpleSubject, string.Empty));
            Assert.IsFalse(EmailParser.IsSubjectUsable(simpleSubject, tagRequests[1]));

            //ensure that emails with no tags only get read if no tag is requested, not sure
            //if that is what we really want
            simpleSubject = "nothing";
            Assert.IsFalse(EmailParser.IsSubjectUsable(simpleSubject, tagRequests[0]));
            Assert.IsTrue(EmailParser.IsSubjectUsable(simpleSubject, string.Empty));
            Assert.IsFalse(EmailParser.IsSubjectUsable(simpleSubject, tagRequests[1]));
        }

        [Test]
        public void DoPrototypesGetEmailSubjectTag()
        {
            var tagRequests = new[] {"tag1", "tag2", "tag3"};
            var nonRequestTag = "tag4";
            //kitchen sink test for sanity
            var subjectTags = string.Format("{0},{1}  ,{2}", tagRequests[0], tagRequests[1], tagRequests[2]);
            string subject = string.Format(" nothing {0} {1} {2}", EmailParser.depictionTagOpener,
                                         subjectTags, EmailParser.depictionTagCloser);

            var proto = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subject, string.Empty);
            Assert.IsNotNull(proto);
            var protoTags = proto.Tags;
            Assert.AreEqual(3,protoTags.Count);
            foreach(var tag in tagRequests)
            {
                Assert.IsTrue(protoTags.Contains(tag));
            }
        }

        [Test]
        public void DoEmailsWithNoTagsInSubjectParseCorrectly()
        {
            string subject = string.Format(" nothing");

            //Don't forget this automatically adds one tag
            var proto = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subject, string.Empty);
            Assert.IsNotNull(proto);
            var protoTags = proto.Tags;
            Assert.AreEqual(1, protoTags.Count);
        }
    }
}