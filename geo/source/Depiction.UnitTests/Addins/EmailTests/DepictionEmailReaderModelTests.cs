﻿using System;
using System.Collections.Generic;
using Depiction.API;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.Service;
using Depiction.CoreModel.ValueTypes;
using Depiction.LiveReports.Models;
using Depiction.Serialization;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.MapConverters;
using Depiction.ViewModels.ViewModelHelpers;
using GisSharpBlog.NetTopologySuite.Geometries;
using NUnit.Framework;
using Point=System.Windows.Point;

namespace Depiction.UnitTests.Addins.EmailTests
{
    [TestFixture]
    public class DepictionEmailReaderModelTests
    {
        private TempFolderService tempFolder;

        [SetUp]
        protected void Setup()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);

            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;
        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();
            DepictionAccess.ProductInformation = null;
            DepictionAccess.GeoCodingService = null;
        }


        [Test]
        public void LatLonInSubjectTakesPriorityOverBody()
        {
            var dmlsToLoad = new List<string> { "Person.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var record = DepictionEmailReaderBackgroundService.GetElementFromEmail("Person: me, 10 11", "latitude: 20\nlongitude: 21", "me", "you",
                                                     "now", "id");
            Assert.AreEqual(new LatitudeLongitude(10, 11), record.Position);
        }
        [Test]
        public void EMailWTKTextSetsElementZOI()
        {
            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);

            var linearRing =
            UnitTestHelperMethods.GetPolygonFromCoordinates(new[]
				         	{
				         		new Coordinate(0, 0), new Coordinate(0, 10), new Coordinate(10, 10), new Coordinate(10, 0),
				         		new Coordinate(0, 0)
				         	});

            ZoneOfInfluence zone = new ZoneOfInfluence(new DepictionGeometry(linearRing));

            var subject = "thing";
            var body = "ZoneOfInfluence: " + zone;

            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            Assert.IsNotNull(prototype);
            Assert.AreEqual(zone, prototype.ZoneOfInfluence);
            var element = ElementFactory.CreateElementFromPrototype(prototype);
            Assert.IsNotNull(element);
            Assert.AreEqual(zone, element.ZoneOfInfluence);
        }

        [Test]
        public void DoElementsUpdatedByEmailKeepHoverTextProperties()
        {
            var dmlsToLoad = new List<string> { "PointOfInterest.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var elementBase = ElementFactory.CreateElementFromTypeString("PointOfInterest");
            Assert.IsNotNull(elementBase);
            var propNameList = new List<string>();
            foreach (var prop in elementBase.OrderedCustomProperties)
            {
                prop.IsHoverText = true;
                propNameList.Add(prop.InternalName);
            }
            var eidVal = "myelemen";
            var eidProp = new DepictionElementProperty("eid", eidVal);
            elementBase.AddPropertyOrReplaceValueAndAttributes(eidProp);


            var repo = new ElementRepository();
            repo.AddElement(elementBase);

            //the email that gets sent
            var subject = "";
            var body = "EID:" + eidVal + "\n" +
                       "DisplayName:Something\n" +
                       "Notes:Something special";
            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            IDepictionElement createdElement;
            var updatedId = repo.AddOrUpdateRepositoryFromPrototype(prototype, out createdElement, false);
            Assert.IsFalse(string.IsNullOrEmpty(updatedId));
            Assert.AreEqual(1, repo.AllElements.Count);
            var element = repo.AllElements[0];
            foreach (var propName in propNameList)
            {
                Assert.IsTrue(element.GetPropertyByInternalName(propName).IsHoverText);
            }

        }

        private bool propChanged = false;
        private bool vmPropChanged = false;
        [Test]
        public void DoesPositionUpdateWithEmailGeoLocationNotifyChangeInModelAndViewModel()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var pixCenter = new Point();
            var viewExtent = new WindowViewportExtent(DepictionGeographicInfo.WorldLatLongBoundingBox, ViewModelConstants.MapSize, ViewModelConstants.MapSize);
            DepictionAccess.GeoCanvasToPixelCanvasConverter = new MercatorCoordinateConverter(viewExtent, pixCenter.X, pixCenter.Y);
            var geocoder = new MockGeocoder();

            var eidVal = "testing123";
            //the email that gets sent
            var subject = "Car: Test, " + MockGeocoder.FakeAddress1;
            var body = "EID:" + eidVal + "\n";
            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                        string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });

            var repo = new ElementRepository();

            IDepictionElement createdElement;
            var updatedId = repo.AddOrUpdateRepositoryFromPrototype(prototype, out createdElement, false);
            Assert.IsTrue(string.IsNullOrEmpty(updatedId));
            Assert.IsNotNull(createdElement);
            Assert.AreEqual(1, repo.AllElements.Count);
            Assert.AreEqual(MockGeocoder.FakeResult1, createdElement.Position);

            createdElement.PropertyChanged += createdElement_PropertyChanged;
            var elementVM = new MapElementViewModel(createdElement);
            elementVM.PropertyChanged += elementVM_PropertyChanged;
            subject = "Car: Test, " + MockGeocoder.FakeAddress2;
            body = "EID:" + eidVal + "\n";
            prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                       string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });

            updatedId = repo.AddOrUpdateRepositoryFromPrototype(prototype, out createdElement, true);
            Assert.IsFalse(string.IsNullOrEmpty(updatedId));
            Assert.IsNull(createdElement);
            Assert.AreEqual(1, repo.AllElements.Count);

            createdElement = repo.AllElements[0];
            Assert.AreEqual(MockGeocoder.FakeResult2, createdElement.Position);
            
            Assert.IsTrue(propChanged);
            Assert.IsTrue(vmPropChanged);
            Assert.IsTrue(elementVM.ElementUpdated);

            DepictionAccess.GeoCodingService = null;
            DepictionAccess.GeoCanvasToPixelCanvasConverter = null;
        }
        #region prop change watchers for DoesPositionUpdateWithEmailGeoLocationNotifyChangeInModelAndViewModel

        void elementVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("MapPixelLocation", StringComparison.InvariantCultureIgnoreCase))
            {
                vmPropChanged = true;
            }
        }

        void createdElement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
           if(e.PropertyName.Equals("position",StringComparison.InvariantCultureIgnoreCase))
           {
               propChanged = true;
           }
        }
        #endregion

        [Test]
        public void DoPointElementsFromLiveReportsGetZOI()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var geocoder = new MockGeocoder();
            var eidVal = "testing123";
            //the email that gets sent
            var subject = "Car: Test, " + MockGeocoder.FakeAddress1;
            var body = "EID:" + eidVal + "\n";
            var prototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                        string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });

            var repo = new ElementRepository();

            IDepictionElement createdElement;
            var updatedId = repo.AddOrUpdateRepositoryFromPrototype(prototype, out createdElement, false);
            Assert.IsNotNull(createdElement);
            Assert.IsFalse(createdElement.ZoneOfInfluence.IsEmpty);
        }

        [Test]
        public void DoesListOfElementsWithSameEIDGetCreatedProperly()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var geocoder = new MockGeocoder();
            var repo = new ElementRepository();
            var eidVal = "testing123";
            //the email that gets sent
            var subject = "Car: Test, " + MockGeocoder.FakeAddress1;
            var body = "EID:" + eidVal + "\n";
            var firstRead = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                        string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });
            Assert.IsTrue(firstRead.ZoneOfInfluence.IsEmpty);
            subject = "Car: Test, " + MockGeocoder.FakeAddress2;
            body = "EID:" + eidVal + "\n";
            var secondRead = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                       string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });

            var protoList = new List<IElementPrototype>();
            protoList.Add(firstRead);
            protoList.Add(secondRead);
            List<IDepictionElement> createdElements;
            var updatedId = repo.AddOrUpdateRepositoryFromPrototypes(protoList, out createdElements, false);
            Assert.AreEqual(1,repo.AllElements.Count);
            var element = repo.AllElements[0];
            Assert.AreEqual(MockGeocoder.FakeResult2, element.Position);
        }

        [Test]
        public void DoesListOfElementsWithSameEIDOverwriteSingleElementProperly()
        {
            var dmlsToLoad = new List<string> { "Car.dml" };
            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            DepictionAccess.GeoCodingService = new DepictionGeocodingService();
            var geocoder = new MockGeocoder();
            var repo = new ElementRepository();
            var eidVal = "testing123";

            var elementBase = ElementFactory.CreateElementFromTypeString("Car");
            Assert.IsNotNull(elementBase);
            var propNameList = new List<string>();
            foreach (var prop in elementBase.OrderedCustomProperties)
            {
                prop.IsHoverText = true;
                propNameList.Add(prop.InternalName);
            }
            var eidProp = new DepictionElementProperty("eid", eidVal);
            elementBase.AddPropertyOrReplaceValueAndAttributes(eidProp);
            repo.AddElement(elementBase);

            //the email that gets sent
            var subject = "Car: Test, " + MockGeocoder.FakeAddress1;
            var body = "EID:" + eidVal + "\n";
            var firstRead = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                        string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });
            subject = "Car: Test, " + MockGeocoder.FakeAddress2;
            body = "EID:" + eidVal + "\n";
            var secondRead = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body,
                       string.Empty, string.Empty, string.Empty, string.Empty, new[] { geocoder });

            var protoList = new List<IElementPrototype>();
            protoList.Add(firstRead);
            protoList.Add(secondRead);
            List<IDepictionElement> createdElements;
            var updatedId = repo.AddOrUpdateRepositoryFromPrototypes(protoList, out createdElements, false);
            Assert.AreEqual(1, repo.AllElements.Count);
            var element = repo.AllElements[0];
            Assert.AreEqual(MockGeocoder.FakeResult2, element.Position);
        }

    }
}
