using System;
using System.Collections.Generic;
using System.Linq;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.StaticAccessors;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.ValueTypes.Measurements;
using Depiction.LiveReports.Models;
using Depiction.Serialization;
using NUnit.Framework;

namespace Depiction.UnitTests.Addins.EmailTests
{
    //Some of these tests are duplicates of Emailparsing tests
    [TestFixture]
    public class LiveReportTests
    {
        private TempFolderService tempFolder;
        string assemblyName = "Depiction.UnitTests";
        private string dmlStart = "Depiction.UnitTests.Addins.";
        private string tempFileName = string.Empty;

        [SetUp]
        protected void Setup()
        {
            DepictionAccess.ElementLibrary = new ElementPrototypeLibrary();
            tempFolder = new TempFolderService(true);

            var prototypeNames = new List<string> { "Person.dml", "PointOfInterest.dml" };
            tempFileName = tempFolder.GetATempFilenameInDir();
            int count = 0;
            foreach (var name in prototypeNames)
            {
                DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, dmlStart + name, tempFileName);
                var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
                DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);
                count++;
            }

            Assert.AreEqual(count, DepictionAccess.ElementLibrary.UserPrototypes.Count);

            var testProduct = new TestProductInformation();
            DepictionAccess.ProductInformation = testProduct;
        }
        [TearDown]
        public void TearDown()
        {
            DepictionAccess.ProductInformation = null;
            DepictionAccess.ElementLibrary = null;
            tempFolder.Close();
        }


        [Test]
        public void ExpectedPropertiesCreated()
        {
            var subject = "Person: me, 10 11";
            var body = "p1 : v1";
            var from = "me";
            var to = "you";
            var datetime = "now";
            var messageId = "id";

            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, from, to, datetime, messageId);

            UnitTestHelperMethods.PropertyTester("Email subject", subject, element);
            UnitTestHelperMethods.PropertyTester("Email body", body, element);
            UnitTestHelperMethods.PropertyTester("Email from", from, element);
            UnitTestHelperMethods.PropertyTester("Email to", to, element);
            UnitTestHelperMethods.PropertyTester("EID", messageId, element);

            Assert.AreEqual(new LatitudeLongitude(10, 11), element.Position);
        }

        [Test]
        public void ElementTypeNamesAreCaseInsensitive()
        {
            var subject = "depiction.PLUGIN.pErsOn:label,__notAnAddress__";
            var body = "Age: 15\nHeight: 20\nLatitude: 10\nLongitude: 20\nEID: a";

            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            Assert.AreEqual("Depiction.Plugin.Person", element.ElementType);
            UnitTestHelperMethods.PropertyTester("age", 15, element);
            UnitTestHelperMethods.PropertyTester("height", 20, element);
            UnitTestHelperMethods.PropertyTester("position", new LatitudeLongitude(10,20), element);
        }
        [Test]
        public void ElementsFromEmailAreNotDraggable()
        {
            var subject = string.Empty;
            var body = "this is the body";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty, string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester("draggable", false, element);
        }

        [Test]
        public void ElementsFromEmailAreNotDraggableEvenIfTheirDraggableIsTrue()
        {
            var subject = string.Empty;
            var body = "Draggable: true";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                                        string.Empty, string.Empty);

            UnitTestHelperMethods.PropertyTester("draggable", false, element);
        }

        [Test]
        public void CreatesElementWhenCannotGeocodeSubject()
        {
            string[] subjects ={
                    "label,__notAnAddress__",
                    "notAnAddress",
                    "RE:",
                    "FW:",
                    "RE: FW:",
                    "FW: RE:" };
            string[] bodies = {
                                  "this is the 1st body",
                                  "this is the second body",
                                  "this is the 3rd body",
                                  "this is the fourth body",
                                  "this is the 5th body",
                                  "this is the sixth body"
                              };
            var elements = new List<IDepictionElement>();
            for (int i = 0; i < subjects.Length; i++)
            {
                var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subjects[i], bodies[i], string.Empty, string.Empty,
                                                                            string.Empty, string.Empty);
                Assert.IsNotNull(element);
                elements.Add(element);

            }
            Assert.AreEqual(subjects.Length, elements.Count);
            foreach(var element in elements)
            {
                Assert.IsFalse(element.Position.IsValid);
                Assert.IsFalse(!string.IsNullOrEmpty(element.ElementUserID));
            }
        }

        [Test]
        [Ignore("Not ready for the full geocoding tests yet")]
        public void CreatesElementWhenCanGeocodeSubject()
        {
            //            var geocoderCache = new MockGeocoderCache();
            //            geocoderCache.ClearCache();
            //            var address = "100 Pine Street, Seattle, WA";
            //            mockGeocoder.SetPosition(address, new LatitudeLongitude(1, 2));
            //            var subject = "label," + address;
            //            var body = "this is the body";
            //            ImportedElements importedElements = GetImportedElement(subject, body, "Depiction.Plugin.PointOfInterest");
            //
            //            Assert.AreEqual(0, importedElements.UngeocodedElements.Count);
            //            Assert.AreEqual(1, importedElements.GeocodedElementCount);
        }

        [Test]
        public void GeocodesElementWhenLatLonInBody()
        {
            string[] subjects ={
                                  "one, two",
                                  "Seattle",
                                  "Himalayas",
                                  "Bottom",
                                  "Bottom America",
                                  "Zero, Zero",
                    };
            string[] bodies = {
                    "Latitude: 1\n Longitude: 2",
                    "Latitude: 45.7N\n Longitude: 122.3W",
                    "Latitude: 35N\n Longitude:83E",
                    "Latitude: 23S\n Longitude:0E",
                    "Latitude: -9.5\n Longitude: -79",
                    "Latitude: 0\n Longitude:0",
                              };

            var elements = new List<IDepictionElement>();
            for (int i = 0; i < subjects.Length; i++)
            {
                var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subjects[i], bodies[i], string.Empty, string.Empty,
                                                                            string.Empty, string.Empty);
                Assert.IsNotNull(element);
                Assert.IsTrue(element.Position.IsValid);
                elements.Add(element);
            }
            Assert.AreEqual(subjects.Length, elements.Count);
        }

        [Test]
        public void InvalidLatLongInBodyFailsToGeocode()
        {
            string[] subjects ={
                                  "91 Top",
                                  "180.5 Right",
                                  "99 Top, 999 Right",
                                  "England?"
                    };
            string[] bodies = {
                    "Latitude: 85.01\n Longitude:123", // abs(Latitude) must be <= 85
                    "Latitude: 83\n Longitude:180.01",  // abs(Longitude) must be <= 180
                    "Latitude: 85.01\n Longitude:180.01",  // abs(Latitude) must be <= 85 and abs(Longitude) must be <= 180
                    "Latitude: 38,75N\n Longitude: 0,33E"  // Comma as decimal place - this may or may not work depending on local culture. 
                              };

            var elements = new List<IDepictionElement>();
            for (int i = 0; i < subjects.Length; i++)
            {
                var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subjects[i], bodies[i], string.Empty, string.Empty,
                                                                            string.Empty, string.Empty);
                Assert.IsNotNull(element);
                Assert.IsFalse(element.Position.IsValid);
                elements.Add(element);
            }
            Assert.AreEqual(subjects.Length, elements.Count);
        }

        [Test(Description = "What the heck is this really testing,ported from 1.2")]
        public void UsingNonNumericValueDoesNotWorkForNumericProperty()
        {//This test should be in the prototype to element tests
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, dmlStart + "Car.dml", tempFileName);
            var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
            DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);
            //            var subject = "label,__notAnAddress__";
            var subject = "Car,__notAnAddress__";
            var body = "Capacity: none\nWeight: empty"; //was origianlly Capacity:no which fails because no gets converted to a bool
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                            string.Empty, string.Empty);
            Assert.AreEqual("Depiction.Plugin.Car", element.ElementType);

            //            Assert.AreEqual(2, DepictionAccess.NotificationService.Messages.Count);
            UnitTestHelperMethods.PropertyTester("Capacity", 0, element);
            UnitTestHelperMethods.PropertyTester("Weight", new Weight(MeasurementSystem.Imperial, MeasurementScale.Small, 0), element);
        }


        [Test]
        public void CreatesElementWhenBodyIsEmpty()
        {
            var subject = "this is the subject";
            var body = string.Empty;
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                           string.Empty, string.Empty);
            Assert.IsNotNull(element);

        }
        [Test]
        public void AddExpectedPropertiesFromBody()
        {
            var subject = "label,__notAnAddress__";
            var body = "Age: 15\nDate:10/20/2007\nPhone number: (425)297-1950";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                           string.Empty, string.Empty);
            Assert.IsNotNull(element);
            UnitTestHelperMethods.PropertyTester("Age", 15d, element);
            UnitTestHelperMethods.PropertyTester("date", "10/20/2007", element);
            UnitTestHelperMethods.PropertyTester("phone number", "(425)297-1950", element);
        }

        [Test]
        public void AddUnexpectedPropertyValueFromBody()
        {
            var subject = "label,__notAnAddress__";
            var propName = "Cowabunga";
            var propValue = "?/~`||!@#$%^&*()_+=-\\}]{[";
            var body = propName + ": " + propValue;
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                            string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester(propName.ToLower(), propValue, element);
        }

        [Test]
        public void AddUnexpectedPropertyValuesFromBody()
        {
            var subject = "label,__notAnAddress__";
            var propName1 = "Cowabunga";
            var propName2 = "'";
            var propValue1 = "?/~`||!@#$%^&*()_+=-\\}]{[";
            var propValue2 = ";>.<,";
            var body = string.Format("{0}: {1}\n{2}: {3}", propName1, propValue1, propName2, propValue2);
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                           string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester(propName1.ToLower(), propValue1, element);
            object val;
            var hasVal = element.GetPropertyValue(propName2, out val);
            Assert.IsFalse(hasVal);
        }

        [Test]
        public void PropertyNameMustContainAlphaNumeric()
        {
            string[] propNames = { "3", "'", "_" };
            string[] propValues = { "for 3", "for '", "for _" };
            var subject = "label,__notAnAddress__";
            var body = string.Empty;
            for (int i = 0; i < propNames.Length; i++)
                body += string.Format("{0}:{1}\n", propNames[i], propValues[i]);

            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                          string.Empty, string.Empty);
            Assert.IsNotNull(element);

            for (int i = 0; i < propNames.Length; i++)
            {
                object val;
                Assert.IsFalse(element.GetPropertyValue(propNames[i], out val));
            }
        }


        [Test(Description = "THis seems liek a weak test")]
        public void IgnoreHttpInSubject()
        {
            var subject = "http://newsletters.sdmediagroup.com/cgi-bin4/DM/y/hBLsR0KBxuv0TKJ0FPJO0E=\r\nM";
            var body = "Ordinary text body";

            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                         string.Empty, string.Empty);
            Assert.IsNotNull(element);
        }


        [Test(Description = "This might change for 1.3.x")]
        public void IgnoreHttpInBody()
        {
            var subject = "label,__notAnAddress__";
            var body = "";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                        string.Empty, string.Empty);

            var minimumNumberOfProperties = element.OrderedCustomProperties.Length;

            subject = "label,__notAnAddress__";
            body = "  <http://newsletters.sdmediagroup.com/cgi-bin4/DM/y/hBLsR0KBxuv0TKJ0FPJO0E=\r\nM><http://newsletters.sdmediagroup.com/cgi-bin4/DM/y/hBLsR0KBxuv0TKJ0ElZq0E=\r\nd><http://newsletters.sdmediagroup.com/cgi-bin4/DM/y/hBLsR0KBxuv0TKJ0FPJP0E=\r\nN><http://newsletters.sdmediagroup.com/cgi-bin4/DM/y/hBLsR0KBxuv0TKJ0EZzf0E=\r\ng><http://newsletters.sdmediagroup.com/cgi-bin4/DM/y/hBLsR0KBxuv0TKJ0ETNu0E=\r\n6>\r\n\r\n\r\nEditor's Note\r\n\r\n\r\nDigital Fusion as Art, or How Did They Do That?\r\n\r\n\r\nFor me, \"fusion\" used to be all about an arc welder, a pile of scrap iron,\r\nand a couple of hours of fun. Then the term was expropriated in the jazz\r\nworld by the likes of the Brecker\r\nBrothers";
            element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                        string.Empty, string.Empty);

            Assert.AreEqual(minimumNumberOfProperties, element.OrderedCustomProperties.Length);
        }

        [Test(Description = "THis seems like another weak test")]
        public void CreatesElementWhenSubjectMaxLength()
        {
            // Two ways to get a max length subject:
            // * 998 characters in a row, or
            // * 12 lines of 78 characters each, and a 13th line of 62 characters.
            var seventyEightchars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 1234567890 `~!@#$%^&*()_";
            var sixtyTwoChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 12345678";
            var oneLineSubject = seventyEightchars + seventyEightchars + seventyEightchars +
                                 seventyEightchars + seventyEightchars + seventyEightchars + seventyEightchars +
                                 seventyEightchars + seventyEightchars + seventyEightchars + seventyEightchars +
                                 seventyEightchars + sixtyTwoChars;
            var multiLineSubject = seventyEightchars + "\n" + seventyEightchars + "\n" + seventyEightchars + "\n" +
                                   seventyEightchars + "\n" + seventyEightchars + "\n" + seventyEightchars + "\n" +
                                   seventyEightchars + "\n" + seventyEightchars + "\n" + seventyEightchars + "\n" +
                                   seventyEightchars + "\n" + seventyEightchars + "\n" + seventyEightchars + "\n" + sixtyTwoChars;
            var body = "Ordinary text body";

            // One character less than max subject length

            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(oneLineSubject.Substring(0, oneLineSubject.Length - 1), body, string.Empty, string.Empty,
                                                       string.Empty, string.Empty);
            Assert.IsNotNull(element);

            // Max subject length
            //            importedElements = GetImportedElement(oneLineSubject, body, "Depiction.Plugin.PointOfInterest");
            //            Assert.AreEqual(1, importedElements.UngeocodedElements.Count);
            element = DepictionEmailReaderBackgroundService.GetElementFromEmail(oneLineSubject, body, string.Empty, string.Empty,
                                                       string.Empty, string.Empty);
            Assert.IsNotNull(element);

            // One character more than max subject length; what happens now?
            element = DepictionEmailReaderBackgroundService.GetElementFromEmail(oneLineSubject + "X", body, string.Empty, string.Empty,
                                                       string.Empty, string.Empty);
            Assert.IsNotNull(element);

            // One character less than max subject length
            element = DepictionEmailReaderBackgroundService.GetElementFromEmail(multiLineSubject.Substring(0, multiLineSubject.Length - 1), body, string.Empty, string.Empty,
                                                       string.Empty, string.Empty);
            Assert.IsNotNull(element);

            // Max subject length
            element = DepictionEmailReaderBackgroundService.GetElementFromEmail(multiLineSubject, body, string.Empty, string.Empty,
                                                       string.Empty, string.Empty);
            Assert.IsNotNull(element);

            // One character more than max subject length; what happens now?
            element = DepictionEmailReaderBackgroundService.GetElementFromEmail(multiLineSubject + "X", body, string.Empty, string.Empty,
                                                       string.Empty, string.Empty);
            Assert.IsNotNull(element);
        }

        #region subject vs body tests.
        [Test]
        public void CanGetElementTypeFromSubject()
        {
            var subject = "Person: bubble and squeak";
            var body = "nothin";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                           string.Empty, string.Empty);

            Assert.IsNotNull(element);
            Assert.AreEqual("Depiction.Plugin.Person", element.ElementType);
        }

        [Test]
        public void CanGetElementTypeFromBody()
        {
            var subject = "bubble and squeak";
            var body = "Element type: PErson";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                         string.Empty, string.Empty);

            Assert.IsNotNull(element);
            Assert.AreEqual("Depiction.Plugin.Person", element.ElementType);
        }

        [Test]
        public void ElementTypeFromSubjectSupersedesFromBody()
        {
            DefaultResourceCopier.CopyResourceStreamToFile(assemblyName, dmlStart + "Car.dml", tempFileName);
            var loadedPrototype = UnifiedDepictionElementReader.CreateElementPrototypeFromDMLFile(tempFileName);
            Assert.IsNotNull(loadedPrototype);
            DepictionAccess.ElementLibrary.AddElementPrototypeToLibrary(loadedPrototype);

            var subject = "Person: colcannon";
            var body = "Element type: Car";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                     string.Empty, string.Empty);

            Assert.IsNotNull(element);
            Assert.AreEqual("Depiction.Plugin.Person", element.ElementType);
        }
        #endregion
        [Test]
        public void UpdateWithNewLatLongInSubjectLineMovesElement()
        {
//            var dmlsToLoad = new List<string> { "LoadedShapes.dml", "AutoDetect.dml" };
//            UnitTestHelperMethods.SetupElementLibraryWithDMLFiles(dmlsToLoad, tempFolder, false);
            var address1 = "1,2";
            var location1 = new LatitudeLongitude(1, 2);

            var address2 = "3,4";
            var location2 = new LatitudeLongitude(3, 4);

            var subject = "Person:label," + address1;
            var body = "Age: 15\nHeight: 20\nEID: a";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                     string.Empty, string.Empty);
            UnitTestHelperMethods.PropertyTester("EID", "a", element);
            UnitTestHelperMethods.PropertyTester("Age", 15d, element);
            UnitTestHelperMethods.PropertyTester("Height", 20d, element);
            UnitTestHelperMethods.PropertyTester("Position", location1, element);
            var propCount = element.OrderedCustomProperties.Length;
            // Can move by entering new address in the subject line

            subject = "label," + address2;
            body = "EID: a";

            var rawPrototype = DepictionEmailReaderBackgroundService.GetRawPrototypeFromEmailWithUniqueID(subject, body, string.Empty, string.Empty,
                                                    string.Empty, string.Empty);
            int numberOfDefaultPropsAdded = 6;
            int numberOfPropsAdded = 3;
            Assert.AreEqual(numberOfPropsAdded + numberOfDefaultPropsAdded, rawPrototype.OrderedCustomProperties.Length);
            UnitTestHelperMethods.PropertyTester("EID", "a", rawPrototype);
            UnitTestHelperMethods.PropertyTester("Position", location2, rawPrototype);

            ElementFactory.UpdateElementWithPrototypeProperties(element,rawPrototype,false,true);

            UnitTestHelperMethods.PropertyTester("EID", "a", element);
            UnitTestHelperMethods.PropertyTester("Age", 15d, element);
            UnitTestHelperMethods.PropertyTester("Height", 20d, element);
            UnitTestHelperMethods.PropertyTester("Position", location2, element);

            Assert.AreEqual(propCount,element.OrderedCustomProperties.Length);
        }
        [Test]
        public void LocationIsUsedAsForPosition()
        {

            var subject = "graffiti report";

            var expectedLatLongString = "47.99058,-122.20441";
            var body = "Location: "+ expectedLatLongString +"\n" +
                       "remarks:  still testing";
            var element = DepictionEmailReaderBackgroundService.GetElementFromEmail(subject, body, string.Empty, string.Empty,
                                                  string.Empty, string.Empty);

            var expectedLatLong = new LatitudeLongitude(47.99058, -122.20441);
            UnitTestHelperMethods.PropertyTester("Location", expectedLatLongString, element);
            UnitTestHelperMethods.PropertyTester("Position", expectedLatLong, element);
        }
        [Test]
        public void DoElementPropertiesOnlyUpdateIfNewer()
        {
            var numberOfEntries = 3;
            var address1 = "1,2";
            var address2 = "3,4";

            var age1 = "Age: 15";
            var age2 = "Age: 25";

            var eid = "EID: a";
            
            var addresses = new[] { address1, address2, address1 };
            var ages = new[] {age1, age1, age2};
            var addressesLatLong = new LatitudeLongitude[numberOfEntries];
            for(int i =0;i<addresses.Length;i++)
            {
                addressesLatLong[i]= new LatitudeLongitude(addresses[i]);
            }
            var times = new[]
                            {
                                new DateTime(2, 1, 1, 1, 1, 2), new DateTime(2, 1, 1, 1, 1, 3),
                                new DateTime(2, 1, 1, 1, 1, 1)
                            };

            var prototypes = new List<IElementPrototype>();
            for(int i =0;i<numberOfEntries;i++)
            {
                var subject = "Person:label," + addresses[i];
                var body = ages[i] + "\nHeight: 20\n" + eid;
                var proto = EmailParser.GetRawPrototypeFromEmailSubjectAndBody(subject, body, times[i], null);
                foreach(var prop in proto.OrderedCustomProperties)
                {
                    Assert.AreEqual(times[i],prop.LastModified);
                }
                prototypes.Add(proto);
            }

            var elementRepo = new ElementRepository();
            var createdElements = new List<IDepictionElement>();
            var updateElements = new List<IDepictionElement>();
            elementRepo.AddOrUpdateRepositoryFromPrototypes(prototypes, true,out createdElements, out updateElements);

            Assert.AreEqual(1,elementRepo.AllElements.Count);
            var element = elementRepo.AllElements.First();

            Assert.AreEqual(addressesLatLong[1], element.Position);
            UnitTestHelperMethods.PropertyTester("Age", 15, element);

        }
    }
}