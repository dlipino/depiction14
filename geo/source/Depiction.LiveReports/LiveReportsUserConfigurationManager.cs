using System;
using System.Configuration;
using System.IO;
using Depiction.APINew;

namespace Depiction.LiveReports
{
    public class LiveReportsUserConfigurationManager
    {
//        private const string LiveReportSettings = "LiveReportSettings";
        private static Configuration depictionConfiguration;
        private static string configFile = "";
        private static string configFileName = "";

        protected static bool SetupConfigPath()
        {
            configFile = string.Concat("LiveReportUser", ".config");
            if (DepictionAccess.PathService == null) return false;
            configFileName = Path.Combine(DepictionAccess.PathService.AppDataDirectoryPath, configFile);
            return true;
        }
        public static void CreateLiveReportUserConfigFile()
        {
            SetupConfigPath();
            if (!File.Exists(configFileName))
            {
                var configExe = new ExeConfigurationFileMap();// ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
                configExe.ExeConfigFilename = configFile;
                configExe.LocalUserConfigFilename = configFile;
                configExe.RoamingUserConfigFilename = configFile;

                var config = ConfigurationManager.OpenMappedExeConfiguration(configExe, ConfigurationUserLevel.None);

                config.SectionGroups.Clear();
                config.Sections.Clear();
                config.SaveAs(configFileName, ConfigurationSaveMode.Minimal, true);
                config.Save(ConfigurationSaveMode.Modified);
            }
        }

        public static Configuration LiveReportUserConfiguration()
        {
            CreateLiveReportUserConfigFile();
            //Not really sure what mapped does yet, but im sure it is better than what im current doing
            if (!File.Exists(configFileName)) return null;
            if (depictionConfiguration == null)
            {
                var configExe = new ExeConfigurationFileMap();
                configExe.ExeConfigFilename = configFileName;
                depictionConfiguration = ConfigurationManager.OpenMappedExeConfiguration(configExe, ConfigurationUserLevel.None);

            }
                return depictionConfiguration;
        }
//        public static EmailSettingsConfigurationSection GetSectionDefaultDepictionConfigurationSection()
//        {
//            Configuration config = LiveReportUserConfiguration();
//            var section = config.Sections[DefaultSettings];
//
//            if (section == null)
//            {
//                config.Sections.Add(DefaultSettings, new EmailSettingsConfigurationSection());
//                config.Save(ConfigurationSaveMode.Modified);
//            }
//            return ((EmailSettingsConfigurationSection)config.GetSection(DefaultSettings));
//        }

        public static void MapExeConfiguration()
        {
            if (DepictionAccess.PathService == null) return;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);

            var folder = DepictionAccess.PathService.AppDataDirectoryPath;
            var fullFileName = Path.Combine(folder, configFile);
            config.SaveAs(fullFileName, ConfigurationSaveMode.Full);
        }
    }
}