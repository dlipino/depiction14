﻿using System.Windows;
using System.Windows.Controls;
using Depiction.LiveReports.EmailConfiguration;
using Depiction.LiveReports.ViewModels;

namespace Depiction.LiveReports.Views
{
    /// <summary>
    /// Interaction logic for LiveReportsFullView.xaml
    /// </summary>
    public partial class LiveReportsFullView
    {
        public LiveReportsFullView()
        {
            InitializeComponent();
        }

        private void cboExistingEmailAddresses_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var dc = DataContext as LiveReportSettupVM;
            if (dc == null) return;
            var elem = sender as ComboBox;
            if (elem == null) return;
            dc.SelectedEmailAccount = elem.SelectedItem as IncomingEmailSettings;
            txtPassword.Password = "";
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as LiveReportSettupVM;
            if (dc == null) return;
            var elem = sender as PasswordBox;
            if (elem == null) return;
            dc.EmailPassword = elem.Password;
        }
    }
}