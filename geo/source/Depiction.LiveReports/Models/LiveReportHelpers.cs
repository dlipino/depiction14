using System.Collections.Generic;

namespace Depiction.LiveReports.Models
{
    public class LiveReportHelpers
    {
        public static readonly List<string> LatitudePropertyNames = new List<string>
                                                                        {
                                                                            "latitude", 
                                                                            "lat"
                                                                        };

        public static readonly List<string> LongitudePropertyNames = new List<string>
                                                                         {
                                                                             "longitude",
                                                                             "long",
                                                                             "lon"
                                                                         };

        public static readonly List<List<string>> LegalGeocodeFieldCombinations = new List<List<string>>
                                                                                      {
                                                                                          new List<string> {"street", "city", "state", "zipcode", "country"}, 
                                                                                          new List<string> {"street", "city", "state", "zip code", "country"}, 
                                                                                          new List<string> {"street", "city", "state", "postcode", "country"}, 
                                                                                          new List<string> {"street address", "city", "state", "zipcode", "country"},
                                                                                          new List<string> {"street address", "city", "state", "zip code", "country"},
                                                                                          new List<string> {"street address", "city", "state", "postcode", "country"},
                                                                                          new List<string> {"street", "city", "province", "zipcode", "country"}, 
                                                                                          new List<string> {"street", "city", "province", "zip code", "country"}, 
                                                                                          new List<string> {"street", "city", "province", "postcode", "country"}, 
                                                                                          new List<string> {"street address", "city", "province", "zipcode", "country"},
                                                                                          new List<string> {"street address", "city", "province", "zip code", "country"},
                                                                                          new List<string> {"street address", "city", "province", "postcode", "country"},
                                                                              
                                                                                          new List<string> {"street", "city", "state", "zipcode"}, 
                                                                                          new List<string> {"street", "city", "state", "zip code"}, 
                                                                                          new List<string> {"street", "city", "state", "postcode"}, 
                                                                                          new List<string> {"street address", "city", "state", "zipcode"},
                                                                                          new List<string> {"street address", "city", "state", "zip code"},
                                                                                          new List<string> {"street address", "city", "state", "postcode"},
                                                                                          new List<string> {"street", "city", "province", "zipcode"}, 
                                                                                          new List<string> {"street", "city", "province", "zip code"}, 
                                                                                          new List<string> {"street", "city", "province", "postcode"}, 
                                                                                          new List<string> {"street address", "city", "province", "zipcode"},
                                                                                          new List<string> {"street address", "city", "province", "zip code"},
                                                                                          new List<string> {"street address", "city", "province", "postcode"},

                                                                                          //new List<string> {"street", "city", "state", "postcode"}, 
                                                                                          //new List<string> {"street address", "city", "state", "postcode"},

                                                                                          //new List<string> {"street", "city", "province", "postal code"}, 
                                                                                          //new List<string> {"street address", "city", "province", "postal code"},

                                                                                          //new List<string> {"street", "city", "territory", "postcode"}, 
                                                                                          //new List<string> {"street address", "city", "territory", "postcode"},

                                                                                          new List<string> {"street", "city", "zipcode"}, 
                                                                                          new List<string> {"street", "city", "zip code"}, 
                                                                                          new List<string> {"street", "city", "postcode"}, 
                                                                                          new List<string> {"street address", "city", "zipcode"},
                                                                                          new List<string> {"street address", "city", "zip code"},
                                                                                          new List<string> {"street address", "city", "postcode"},
                                                                              
                                                                                          new List<string> {"street", "city", "state", "country"}, 
                                                                                          new List<string> {"street address", "city", "state", "country"}, 
                                                                                          new List<string> {"street", "city", "province", "country"}, 
                                                                                          new List<string> {"street address", "city", "province", "country"}, 
                                                                              
                                                                                          new List<string> {"street", "city", "state"}, 
                                                                                          new List<string> {"street address", "city", "state"}, 
                                                                                          new List<string> {"street", "city", "province"}, 
                                                                                          new List<string> {"street address", "city", "province"}, 

                                                                                          new List<string> {"location"}, 

                                                                                          new List<string> {"address"}, 
                                                                                      };
    }
}