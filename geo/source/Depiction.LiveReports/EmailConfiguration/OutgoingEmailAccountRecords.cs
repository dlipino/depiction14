using System;
using System.Configuration;
using Depiction.LiveReports.EmailConfiguration;

namespace Depiction.LiveReports.EmailConfiguration
{
    public class OutgoingEmailAccountRecords : ConfigurationElementCollection
    {
        public OutgoingEmailSettings this[int index]
        {
            get { return BaseGet(index) as OutgoingEmailSettings; }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new OutgoingEmailSettings();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var emailElement = (OutgoingEmailSettings)element;
            if (string.IsNullOrEmpty(emailElement.Identifier))
            {
                emailElement.Identifier = Guid.NewGuid().ToString();
            }
            return emailElement.Identifier;
        }

        public void AddOrReplaceExisting(OutgoingEmailSettings emailAccount)
        {
            try
            {
                var index = BaseIndexOf(emailAccount);
                if (index != -1)
                {
                    this[index] = emailAccount;
                }
                else
                {
                    BaseAdd(emailAccount, true);
                }
            }
            catch
            {//Hack for sure
                var index = BaseIndexOf(emailAccount);
                this[index] = emailAccount;
            }
        }
//        public void Add(OutgoingEmailSettings emailAccount)
//        {
//            BaseAdd(emailAccount);
//        }
        public void Remove(OutgoingEmailSettings emailAccount)
        {
            var index = BaseIndexOf(emailAccount);
            if (index >= 0)
            {
              //  BaseRemove(emailAccount);//you would think this would do something, but it doesnt, im sure it has to do with getelementkey
                BaseRemove(emailAccount.Identifier);
            }
        }

        public void Insert(OutgoingEmailSettings emailAccount, int index)
        {
            BaseAdd(index, emailAccount);
        }
    }
}