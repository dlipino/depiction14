﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;

namespace Depiction.Geocoders.USCGeocoder
{
    [Geocoder("USCElementGeocoder", DisplayName = "USC geocoder")]
    public class USCElementGeocoder : IDepictionGeocoder
    {
        #region Variables

        const string noResponseFromGeocoder = "No response received from GeoCoder.";
        const string parseResponseFailedForGeoCoder = "Unable to process response from the geocoder.";
        public const string InvalidGeoLocationText = "Could not find this location, please be more specific: {0}";
        private string URI;
        private Dictionary<string, string> geocoderParameters;
        #endregion

        #region Properties

        public bool StoreResultsInDepiction
        {
            get { return true; }
        }

        public string GeocoderName
        {
            get { return "USC Element geocoder"; }
        }

        public bool IsAddressGeocoder
        {
            get { return true; }
        }

        public bool IsLatLongGeocoder
        {
            get { return false; }
        }
        #endregion
        #region Methods
        public void ReceiveParameters(Dictionary<string, string> parameters)
        {
            if (parameters != null && parameters.ContainsKey("URI"))
                URI = parameters["URI"];
            if (parameters != null)
            {
                geocoderParameters = new Dictionary<string, string>();
                foreach (var entry in parameters)
                {
                    geocoderParameters.Add(entry.Key, entry.Value);
                }
            }
        }

        public GeocodeResults GeocodeAddress(string addressString, bool isLatLong)
        {
            return GeocodeAddress(addressString);
        }

        public GeocodeResults GeocodeAddress(string street, string city, string state, string zipcode, string country)
        {
            if (!DepictionInternetConnectivityService.IsInternetAvailable || string.IsNullOrEmpty(URI))
                return null;
            if (!String.IsNullOrEmpty(zipcode))
            {
                if (String.IsNullOrEmpty(city) || String.IsNullOrEmpty(state))
                {
                    //get it from geonames
                    AddressParser.GetCityFromGeoNames(zipcode, out city, out state);
                }
            }

            var outString = String.Format("streetAddress={0}&city={1}&state={2}&zip={3}",
                                          string.IsNullOrEmpty(street) ? "" : Uri.EscapeDataString(street),
                                          string.IsNullOrEmpty(city) ? "" : Uri.EscapeDataString(city),
                                          string.IsNullOrEmpty(state) ? "" : Uri.EscapeDataString(state),
                                          string.IsNullOrEmpty(zipcode) ? "" : Uri.EscapeDataString(zipcode));


            string url = string.Format("{0}&{1}", URI, outString);

            WebRequest geocoderRequest = WebRequest.Create(url);
            try
            {
                using (WebResponse response = geocoderRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        return ParseResponse(responseStream);
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    return new GeocodeResults(string.Format(InvalidGeoLocationText, outString));
                //                throw;//why the throw?
            }
            catch (Exception ex)
            {

            }
            return new GeocodeResults(string.Format(InvalidGeoLocationText, outString));
        }

        public GeocodeResults GeocodeAddress(string addressString)
        {
            if (!DepictionInternetConnectivityService.IsInternetAvailable || string.IsNullOrEmpty(URI))
                return null;
            //
            //unparsed addresses come either from ADD ELEMENT via address OR LIVE REPORTS
            //
            //geocode this ONLY if this geocoder is PRIMARY for Live Reports
            if (geocoderParameters.ContainsKey("LiveReports"))
            {
                if (!geocoderParameters["LiveReports"].Equals("Primary"))
                    return null;
            }
            string city, state, zip;
            addressString = AddressParser.ParseCSZ(addressString, out city, out state, out zip);
            string outString;
            if (!state.Equals(""))
                outString = String.Format("streetAddress={0}&state={1}&zip={2}", addressString, state, zip);
            else
                outString = String.Format("streetAddress={0}&zip={1}", addressString, zip);


            string url = string.Format("{0}&{1}", URI, outString);

            WebRequest geocoderRequest = WebRequest.Create(url);
            try
            {
                using (WebResponse response = geocoderRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        return ParseResponse(responseStream);
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    return new GeocodeResults(string.Format(InvalidGeoLocationText, addressString));
                //throw;
            }
            catch (Exception ex)
            {

            }
            return new GeocodeResults(string.Format(InvalidGeoLocationText, addressString));
        }

        protected GeocodeResults GetGeocodeResults(string url, string initialRequest)
        {
            WebRequest geocoderRequest = WebRequest.Create(url);
            try
            {
                using (WebResponse response = geocoderRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        return ParseResponse(responseStream);
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    return new GeocodeResults(string.Format(InvalidGeoLocationText, initialRequest));
                //                throw;
            }
            catch (Exception ex)
            {

            }
            return new GeocodeResults(string.Format(InvalidGeoLocationText, initialRequest));
        }

        private static GeocodeResults ParseResponse(Stream responseStream)
        {
            LatitudeLongitude position;
            bool accurate = false;
            if (responseStream != null)
            {
                string latitude = string.Empty;
                string longitude = string.Empty;
                var reader = new XmlTextReader(responseStream);
                try
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name == "FeatureMatchingGeographyType")
                            {
                                string text = reader.ReadString();

                                if (text.Equals("StreetSegment") || text.Equals("StreetIntersection") || text.Equals("StreetCentroid") || text.Equals("BuildingDoor") || text.Equals("BuildingCentroid") || text.Equals("Parcel"))
                                    accurate = true;
                            }
                            if (reader.Name != "WebServiceGeocodeResult")
                            {
                                if (reader.Name == "Latitude" || reader.Name == "Longitude")
                                {
                                    string text = reader.ReadString();
                                    if (reader.Name == "Latitude")
                                    {
                                        latitude = text;
                                    }
                                    if (reader.Name == "Longitude")
                                    {
                                        longitude = text;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (XmlException ex)
                {
                    throw new Exception(parseResponseFailedForGeoCoder, ex);
                }
                finally
                {
                    reader.Close();
                }

                position = new LatitudeLongitude(latitude, longitude);
                if (position.Equals(new LatitudeLongitude(0, 0)))
                    throw new Exception(parseResponseFailedForGeoCoder);
            }
            else
            {
                throw new Exception(noResponseFromGeocoder);
            }

            return new GeocodeResults(position, accurate);
        }

        #endregion
    }
}