﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;

namespace Depiction.Geocoders.GeocodeFarmGeocoder
{
    [Geocoder("GeocodeFarm", DisplayName = "Geocode.Farm Geocoder", Description = "Used for initial location geocoding. Limited to 250 geocodes per day.")]
    public class GeocodeFarmGeocoder : IDepictionGeocoder
    {
        #region Variables

        const string noResponseFromGeocoder = "No response received from GeoCoder.";
        const string parseResponseFailedForGeoCoder = "Unable to process response from the geocoder.";
        public const string InvalidGeoLocationText = "Could not find this location, please be more specific: {0}";
        private string URI;
        private Dictionary<string, string> geocoderParameters;

        #endregion

        #region properties
        public bool StoreResultsInDepiction
        {
            get { return true; }
        }


        public string GeocoderName
        {
            get { return "Geocode.Farm geocoder"; }
        }
        #endregion
        #region GeocoderAddinView Members

        public GeocodeResults GeocodeAddress(string addressString, bool isLatLong)
        {
            throw new System.NotImplementedException();
        }

        public GeocodeResults GeocodeAddress(string street, string city, string state, string zipcode, string country)
        {
            string fullAddress = string.Format("{0} {1} {2} {3} {4}", street, city, state, zipcode, country);
            return GeocodeAddress(fullAddress);
        }



        public bool IsAddressGeocoder
        {
            get { return true; }
        }

        public bool IsLatLongGeocoder
        {
            get { return false; }
        }

        public void ReceiveParameters(Dictionary<string, string> parameters)
        {
            if (parameters != null && parameters.ContainsKey("URI"))
                URI = parameters["URI"];

            if (parameters != null)
            {
                geocoderParameters = new Dictionary<string, string>();
                foreach (var entry in parameters)
                {
                    geocoderParameters.Add(entry.Key, entry.Value);
                }
            }
        }
        //  https://www.geocode.farm/v3/xml/forward/?addr=22518%2092nd%20Ave%20W%20Edmonds%20WA%2098020&lang=en&count=1
        public GeocodeResults GeocodeAddress(string addressString)
        {
            if (!DepictionInternetConnectivityService.IsInternetAvailable || string.IsNullOrEmpty(URI))
                return new GeocodeResults("Internet not available for geocoding");
            //
            //unparsed addresses come either from ADD ELEMENT via address OR LIVE REPORTS
            //
            //geocode this ONLY if this geocoder is PRIMARY for Live Reports
            if (geocoderParameters.ContainsKey("LiveReports"))
            {
                if (!geocoderParameters["LiveReports"].Equals("Primary")) return new GeocodeResults("This geocoder cannot be used for live reports");
            }
            //            string url = string.Format("{0}&location={1}", URI, Uri.EscapeDataString(addressString));
            string urlUsableAddress = Uri.EscapeDataString(addressString);
            var uri = "https://www.geocode.farm/v3/xml/forward/?addr={0}&lang=en&count=1";
            var uriKey = "URI";
            if (geocoderParameters.ContainsKey(uriKey))
            {
                var founduri = geocoderParameters[uriKey];
                if (!string.IsNullOrEmpty(founduri))
                {
                    uri = founduri;
                }
            }

            var requestUrl = string.Format(uri, urlUsableAddress);

            WebRequest geocoderRequest = WebRequest.Create(requestUrl);
            try
            {
                using (WebResponse response = geocoderRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        return new GeocodeResults(ParseXMLStreamResponse(responseStream), true);
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    return new GeocodeResults(string.Format(InvalidGeoLocationText, addressString));
                throw;
            }

        }

        public static LatitudeLongitude ParseXMLStreamResponse(Stream responseStream)
        {
            LatitudeLongitude position;

            if (responseStream != null)
            {
                string latitude = string.Empty;
                string longitude = string.Empty;
                var reader = new XmlTextReader(responseStream);
                try
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name != "Succeeded" && reader.Name != "ResultSet")
                            {
                                if (reader.Name.Equals("Latitude", StringComparison.InvariantCultureIgnoreCase)
                                    || reader.Name.Equals("Longitude", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    string text = reader.ReadString();
                                    if (reader.Name.Equals("Latitude", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        latitude = text;
                                    }
                                    if (reader.Name.Equals("Longitude", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        longitude = text;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (XmlException ex)
                {
                    throw new Exception(parseResponseFailedForGeoCoder, ex);
                }
                finally
                {
                    reader.Close();
                }

                position = new LatitudeLongitude(latitude, longitude);
            }
            else
            {
                throw new Exception(noResponseFromGeocoder);
            }

            return position;
        }

        #endregion
    }
}

/*

 <geocoding_results>
<LEGAL_COPYRIGHT>
<copyright_notice>
Copyright (c) 2015 Geocode.Farm - All Rights Reserved.
</copyright_notice>
<copyright_logo>https://www.geocode.farm/images/logo.png</copyright_logo>
<terms_of_service>
https://www.geocode.farm/policies/terms-of-service/
</terms_of_service>
<privacy_policy>https://www.geocode.farm/policies/privacy-policy/</privacy_policy>
</LEGAL_COPYRIGHT>
<STATUS>
<access>FREE_USER, ACCESS_GRANTED</access>
<status>SUCCESS</status>
<address_provided>22518 92nd Ave W Edmonds WA 98020</address_provided>
<result_count>1</result_count>
</STATUS>
<ACCOUNT>
<ip_address/>
<distribution_license>NONE, UNLICENSED</distribution_license>
<usage_limit>250</usage_limit>
<used_today>12</used_today>
<used_total>12</used_total>
<first_used>09 Oct 2015</first_used>
</ACCOUNT>
<RESULTS>
<result>
<result_number>1</result_number>
<formatted_address>22518 92nd Ave W, Edmonds, WA 98020, USA</formatted_address>
<accuracy>EXACT_MATCH</accuracy>
<ADDRESS>
<street_number>22518</street_number>
<street_name>92nd Avenue West</street_name>
<neighborhood>Westgate</neighborhood>
<locality>Edmonds</locality>
<admin_2>Snohomish County</admin_2>
<admin_1>Washington</admin_1>
<postal_code>98020</postal_code>
<country>United States</country>
</ADDRESS>
<LOCATION_DETAILS>
<elevation>UNAVAILABLE</elevation>
<timezone_long>UNAVAILABLE</timezone_long>
<timezone_short>America/Boise</timezone_short>
</LOCATION_DETAILS>
<COORDINATES>
<latitude>47.7941209782830</latitude>
<longitude>-122.356649576903</longitude>
</COORDINATES>
<BOUNDARIES>
<northeast_latitude>47.7941209740785</northeast_latitude>
<northeast_longitude>-122.356649685833</northeast_longitude>
<southwest_latitude>47.7927719197085</southwest_latitude>
<southwest_longitude>-122.357997980293</southwest_longitude>
</BOUNDARIES>
</result>
</RESULTS>
<STATISTICS>
<https_ssl>ENABLED, SECURE</https_ssl>
</STATISTICS>
</geocoding_results>
 
*/