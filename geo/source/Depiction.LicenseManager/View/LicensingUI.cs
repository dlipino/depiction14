using Depiction.API.Interfaces;

namespace Depiction.LicenseManager.View
{
    public class LicensingUI : ILicensingUI
    {
        private ActivateDepictionWindow activateDialog;

        public bool LaunchPurchaseActivationUI(ILicenseService licenseService)
        {
            activateDialog = new ActivateDepictionWindow();
            activateDialog.DataContext = licenseService;
            
            //showdialog always returns true or false, returning bool? here just seems like bad design from .net.  i'd like to hide this in an extension method or something eventually.
// ReSharper disable PossibleInvalidOperationException
            return (bool)activateDialog.ShowDialog();
// ReSharper restore PossibleInvalidOperationException
        }
    }
}