using System;
using System.IO;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.DialogBases;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;
using Depiction.LicenseManager.Model;
using Depiction.LicenseManager.View;

namespace Depiction.LicenseManager
{
    public class LicensingManager
    {
        private ILicenseService licenseService;
        private string fullLicenseToUse = string.Empty;
        private string existingLegacyFullFile = string.Empty;
        private ILicensingUI licensingUI;

        public ILicenseService LicenseService
        {
            // should not really expose this.  Only needed once in DepictionwindowActions.  TODO: Find a better way to do that.
            get { return licenseService; }

        }

        public LicensingManager()
        {

            var expectedLicenseFile = Path.Combine(DepictionAccess.ProductInformation.LicenseFileDirectory, DepictionAccess.ProductInformation.LicenseFileName);

            //Legacy lic files, simple version. A more complete version would check to see if the file is authorized then set it to new value if it
            //is not
            var legacyFullFile = string.Empty;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Release:
                    var legacyPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction");
                    legacyFullFile = Path.Combine(legacyPath, "licensev1.lic");
                    break;
            }
            //Check for legacy and take note
            if (File.Exists(legacyFullFile))
            {
                existingLegacyFullFile = legacyFullFile;
            }
            fullLicenseToUse = expectedLicenseFile;
            licensingUI = new LicensingUI();
        }

        public bool CheckLicense()
        {

            var productName = DepictionAccess.ProductInformation.ProductName;
            var companyName = DepictionAccess.ProductInformation.CompanyName;
            var productType = DepictionAccess.ProductInformation.ProductType;
            try
            {
                if (licenseService == null)
                {
                    switch (productType)
                    {
                        case ProductInformationBase.OsintInformation:
//                            licenseService = new LicenseService(fullLicenseToUse,"frilling___sojoUrned_8_Worn",
//                                "variantS_WISPIER_4_setoff",7);//last int determines what ss checks for in system harddrive etc
   licenseService = new LicenseService(fullLicenseToUse,"austrian_6_abode_._epitome", "featuring_CAMPANILE_2_newscast",7);//last int determines what ss checks for in system harddrive etc
                                                  
break;
                        default: 
                            licenseService = new LicenseService(fullLicenseToUse);
                            //This should only happen if there is an unactivated version of depiction on the comp
                            //hopefully that is a very small percentage of users.
                            //there was also a check for legacy, but itook it out
                            Console.WriteLine(licenseService.LicenseState);
                            if (!string.IsNullOrEmpty(existingLegacyFullFile) && licenseService.LicenseState.Equals(DepictionLicenseState.UnActivated))//if legacy
                            {
                                licenseService = new LicenseService(existingLegacyFullFile);
                                fullLicenseToUse = existingLegacyFullFile;
                            }
                            break;
                    }
                  
                }
                switch (licenseService.LicenseState)
                {
                    case DepictionLicenseState.LicenseFileMissing:
                        var msg = string.Format("You must have a valid license to run {0}. Please contact {1}. Shutting down.",
                                               productName, companyName);
                        DepictionMessageBox.ShowDialog(msg, "Missing license file", MessageBoxButton.OK);
                        //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog(msg);
                        return false;
                    case DepictionLicenseState.InvalidLicense:

                        var msg2 = string.Format("{0} could not run because:\n\n{1} ", productName,
                                                 licenseService.ErrorMessage);
                        DepictionMessageBox.ShowDialog(msg2, "Invalid License", MessageBoxButton.OK);
                        //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog(msg2);
                        return false;
                    default:
                        //                        if (licenseService.IsTrialMode)
                        //                            return CheckTrialModeLicense();
                        return CheckPurchasedLicense();
                }
            }
            catch (Exception e)
            {
                DepictionAccess.NotificationService.DisplayInquiryDialog(DepictionExceptionHandler.MessageFromExceptionAndInnerException(e), "Depiction start error", MessageBoxButton.OK);
                DepictionExceptionHandler.HandleException(e, false, true);
                //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Exception in CheckLicense", e);
                //                CommonData.depictionAccessInternalHandle.NotificationService.SendNotificationDialog(
                //                    ExceptionHandler.MessageFromExceptionAndInnerException(e));
            }
            return false;
        }


        private bool CheckPurchasedLicense()
        {
            switch (licenseService.LicenseState)
            {
                case DepictionLicenseState.UnActivated:
                    if (licensingUI.LaunchPurchaseActivationUI(licenseService))
                        return true;
                    // return CheckPurchasedLicense();
                    //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Purchased License Unactivated");
                    return false;
                case DepictionLicenseState.ActivatedExpired:
                    //                    if (DepictionAccess._productInformation.RenewableLicense && licensingUI.LaunchRenewalUI(licenseService))
                    //                        return CheckPurchasedLicense();
                    // CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("Purchased License Expired");
                    return true;

                case DepictionLicenseState.ActivatedUnexpired:
                case DepictionLicenseState.ActivatedNoExpiration:
                    return true;
                case DepictionLicenseState.UnActivatedInTrialPeriod:
                    if (licensingUI.LaunchPurchaseActivationUI(licenseService))
                        return true;
                    return false;

                default:
                    //  CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("License Check unsuccessful: Unexpected value for LicenseState ");
                    return false;
            }
        }

        public bool DeactivateLicense()
        {
            licenseService.DeactivateLicense();
            return true;// licenseService.IsDeactivated;
        }
    }
}