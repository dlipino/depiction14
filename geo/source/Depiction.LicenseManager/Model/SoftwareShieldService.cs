using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.API.Service;
using SSCProt;

namespace Depiction.LicenseManager.Model
{
    /// <summary>
    /// Provides access to the internals of Software Shield.
    /// </summary>
    [Guid("3B10E58D-381E-4D4F-AD2C-6900F2FC6CC8")]
    public class SoftwareShieldService 
    {
        #region Fields

        private readonly int FingerPrintOptionsCode;
        private readonly string GlobalAuthorizationCodePassword = "";
        private readonly ILicenseService _currentDateProvider;
        private readonly string licenseFilename = "";
        private readonly string licenseFilePassword = "";



        #endregion

        #region SoftwareShield_ClientProtectorDefinitions

        // the ClientProtector server
        private SSCProtectorClass SSCP;

        // ======= Authorization Definition IDs ==============
        // this is the ID of the recovery definition in the license.
        // this license is configured to make the user input an Activation Code to recover

        #endregion

        #region SoftwareShield_FPUControlWord_WorkAround

        /* /////////////////////////////////////////////////////////////
		 * This functionality may be required in some .NET applications 
		 * because the .NET framework expects the floating point control 
		 * word to mask all exceptions internally. However the 
		 * SoftwareShieldService COM servers expect a standard FPU exception mask. 
		 * 
		 * If you are having strange exceptions that disappear when you 
		 * comment out the creation of your SoftwareShieldService COM servers - 
		 * use the workaround shown in this module to get and reset the 
		 * default control word.  This should only have to be done 
		 * imemdiately after the COM servers constructtion call.
		 * 
		 * See the help on "Linking to your Application in C#" for more info.
		 * /////////////////////////////////////////////////////////////
		*/

        private readonly int DefaultCW;

        [DllImport("msvcrt.dll")]
        public static extern int _controlfp(int IN_New, int IN_Mask);

        #endregion

        #region properties

        protected bool LicenseFileMissing { get; private set; }

        public string ErrorMessage { get; private set; }
        public DepictionLicenseState LicenseState { get; set; }


        public DateTime ExpirationDate { get; private set; }
        public bool IsExpired
        {
            get
            {
                if (ExpirationDate != DateTime.MinValue)
                {
                    if (_currentDateProvider.CurrentDate > ExpirationDate)
                        return true;
                }
                return false;
            }
        }
        #endregion

        #region Constructors

        public SoftwareShieldService(ILicenseService currentDateProvider, string licenseFileName, string licenseFilePassword, string authCodePassword, int fingerPrintOptionsCode)
        {

            _currentDateProvider = currentDateProvider;
            licenseFilename = licenseFileName;
            this.licenseFilePassword = licenseFilePassword;
            GlobalAuthorizationCodePassword = authCodePassword;
            FingerPrintOptionsCode = fingerPrintOptionsCode;
            //
            // Required for 
            // lets get the default control word
            //
            DefaultCW = _controlfp(0, 0);


            Initialize();
            if (HasValidLicenseFile)
            {
                InitLicense();
            }
            UpdateLicenseInfo();
        }
#endregion

        public void UpdateLicenseInfo()
        {
            Console.WriteLine(string.Format("Getting data for {0}",licenseFilename));
            SerialNumber = GetSerialNumber();
            Console.WriteLine(SerialNumber);
            ExpirationDate = GetExpirationDate();
            Console.WriteLine(ExpirationDate);
            IsValidLicense = HasValidLicenseFile && !IsExpired;
            LicenseState = DepictionLicenseState.InvalidLicense;
            if (LicenseFileMissing)
                LicenseState = DepictionLicenseState.LicenseFileMissing;
            else if (HasValidLicenseFile)
            {
                if (IsActivated)
                {
                    if (IsExpired)
                        LicenseState = DepictionLicenseState.ActivatedExpired;
                    else if (ExpirationDate == DateTime.MinValue)
                        LicenseState = DepictionLicenseState.ActivatedNoExpiration;
                    else
                        LicenseState = DepictionLicenseState.ActivatedUnexpired;
                }
                else
                {
                    if(ExpirationDate == DateTime.MinValue)
                    {
                        LicenseState = DepictionLicenseState.UnActivated;
                    }else if(DateTime.Today>ExpirationDate)
                    {
                        LicenseState = DepictionLicenseState.UnActivated;
                    }else
                    {
                        LicenseState = DepictionLicenseState.UnActivatedInTrialPeriod;  
                    }
                }
            }
        }

        private DateTime GetExpirationDate()
        {
            // return the lesser of hard or period expiration date if both exist, otherwise the relevant one, else DateTime.MinValue;
            int year, month, day, hour, minute;
            bool isHardExpireType;
//            SSCP.GetHardExpireDate(out year, out month, out day, out hour, out minute, out isHardExpireType);
            SSCP.GetExpireDate(out year, out month, out day, out hour, out minute, out isHardExpireType);
            
            DateTime hardExpirationDate = DateTime.MinValue;

            if (isHardExpireType)
                hardExpirationDate = new DateTime(year, month, day, hour, minute, 0);

            bool isPeriodExpireType;
            DateTime periodExpirationDate = DateTime.MinValue;
            SSCP.GetExpirePeriodDate(out year, out month, out day, out hour, out minute, out isPeriodExpireType);
            if (isPeriodExpireType)
                periodExpirationDate = new DateTime(year, month, day, hour, minute, 0);

            if (isHardExpireType && isPeriodExpireType)
            {
                if (hardExpirationDate < periodExpirationDate)
                    return hardExpirationDate;
                return periodExpirationDate;
            }
            if (isHardExpireType)
                return hardExpirationDate;
            if (isPeriodExpireType)
                return periodExpirationDate;
            return DateTime.MinValue;
        }


        #region Methods

        public bool ActivateLicense(string authCode, bool checkProductId, out string message)
        {
            int AutoActivateReturn;
            // Attempts to Activate the Serial Number
            SSCP.AutoActivateSerialNumber(authCode, out AutoActivateReturn);
            if (!HandleAutoActivate(AutoActivateReturn, out message))
                return false;

            SerialNumber = "Activated";
            IsValidLicense = true;
            IsActivated = true;
            message = "Activated";
            UpdateLicenseInfo();
            return true;
        }

        public bool ActivateManually(string manualActivationCode, out string message)
        {
            int manualActivationStatusCode;

            SSCP.InputActivationCode(manualActivationCode, out manualActivationStatusCode);

            if (!HandleActivationReturnCode(manualActivationStatusCode, out message))
                return false;

            SerialNumber = "Activated";
            IsValidLicense = true;
            IsActivated = true;
            message = "Activated";
            UpdateLicenseInfo();
            return true;
        }

        public string GetSerialNumber()
        {
            string snList;
            SSCP.GetAllSerialNumbers(out snList);
            //hopefully, this will only contain ONE serial number
            //
            //the format is ID=xxxx-xxxx-xxxx-xxxx, where ID is the auth def id, which is 5 in our case
            //
            string sn = null;
            if (snList != null) sn = snList.Substring(snList.LastIndexOf("=", StringComparison.InvariantCulture) + 1, 19);
            return sn;
        }
        public void EndSSCP()
        {
            SSCP = null;
        }
        /// <summary>
        /// Initialize the license by starting up the ClientProtector.
        /// </summary>
        public void Initialize( /*object sender, System.EventArgs e*/)
        {
            int return_code;
            const int debugFlags = 0;

            try
            {
                SSCP = new SSCProtectorClass(); //This causes a stack over flow

                // restore the FPU control word .NET expects
                _controlfp(DefaultCW, 0xfffff);

                SSCP.OnAttemptDeactivate += SSCP_OnAttemptDeactivate;
                SSCP.OnAttemptReleaseCP += SSCP_OnAttemptReleaseCP;
            }
            catch (Exception)
            {
               // CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("problem with Initialize", e);
                IsValidLicense = false;
                throw;
            }

            if (!File.Exists(licenseFilename))
            {
                HasValidLicenseFile = false;
                LicenseFileMissing = true;
                return;
            }
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog(
            //    string.Format("before SSCP.StartUp:\n licenseFilename = {0}\n licenseFilePassword = {1}\n GlobalAuthorizationCodePassword = {2}\n FingerPrintOptionsCode = {3}\n debugFlags = {4}",
            //licenseFilename, licenseFilePassword, GlobalAuthorizationCodePassword, FingerPrintOptionsCode, debugFlags));
            SSCP.StartUp(licenseFilename, licenseFilePassword, GlobalAuthorizationCodePassword, FingerPrintOptionsCode, debugFlags, out return_code);
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog(string.Format("after SSCP.StartUp; return_code = {0}", return_code));

            IsVirginInstall = true;
            TrialPeriodExpired = false;
            HasValidLicenseFile = true;
            string message;
            if (!HandleStartUpReturnCode(return_code, out message))
            {
                ErrorMessage = message;
                IsValidLicense = false;
                HasValidLicenseFile = false;
                return;
            }
            IsDeactivated = false;

            CheckPointAttemptsFailed = 0;
        }


        private bool IsAuthorized()
        {
            bool isAuthorized;

            // find out if the copy protection has been released for this system
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("before SSCP.IsSystemAuthorized");
            SSCP.IsSystemAuthorized(out isAuthorized);
            if (isAuthorized)
                return IsExpired;
            return false;
        }


        /// <summary>
        /// Justs sets up the UI with the appropriate Licensing data.
        /// (which features are enbaled etc.)
        /// </summary>
        private void InitLicense()
        {
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("enter InitLicense");
            bool isAuthorized;

            // find out if the copy protection has been released for this system
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("before SSCP.IsSystemAuthorized");
            SSCP.IsSystemAuthorized(out isAuthorized);

            if (isAuthorized)
            {
                // in this case - because of the design of our license we know that
                // once copy protection is released at least some of our controlled
                // features may be owned.  Now we just have to find out which ones.
                IsValidLicense = true;
                IsActivated = true;
                SerialNumber = GetSerialNumber();
                if (string.IsNullOrEmpty(SerialNumber))
                {
                    SerialNumber = ReadSNFromFile();
                    //license file is not loaded with serial number...load it now
                    int returnCode, paramValue = 0;
                    //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("before SSCP.SetAuthorizationDetailsEx");
                    SSCP.SetAuthorizationDetailsEx(DepictionAccess.ProductInformation.LicenseActivationID, GlobalAuthorizationCodePassword, true, paramValue,
                                                   SerialNumber, out returnCode);
                }
            }
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("exit InitLicense");

        }

        private string ReadSNFromFile()
        {
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("enter ReadSNFromFile");
            string sn1 = null;
            string fileName = licenseFilename + @".serialnumber.txt";
            if (File.Exists(fileName))
            {
                StreamReader reader = new StreamReader(fileName);
                sn1 = reader.ReadLine();
                reader.Close();
            }
            //CommonData.depictionAccessInternalHandle.NotificationService.WriteToLog("exit ReadSNFromFile");
            return sn1;
        }

        #endregion

        #region Properties
        private string serialNumber;
        public string SerialNumber
        {
            get { return serialNumber; }
            set
            {
                serialNumber = value;
                //VersionForUi.Instance.SerialNumber = value;
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// EVENT HANDLER
        /// The ClientProtector will fire this event if a request to deactivate is made
        /// giving the user a last chance to abort deactivation.
        /// </summary>
        /// <param name="AllowDeactivate">Return set to true if the ClientProtector should allow the deactivation.</param>
        private void SSCP_OnAttemptDeactivate(out bool AllowDeactivate)
        {
            var productName = DepictionAccess.ProductInformation.ProductName;
            MessageBoxResult res = DepictionAccess.NotificationService.DisplayInquiryDialog(string.Format("This action will permanently deactivate {0} on this machine.\n\nAre you sure you want to do this?", productName),
                                                            "Deactivate", MessageBoxButton.YesNo);
            AllowDeactivate = false;
            if (res.Equals(MessageBoxResult.Yes))
            {
                AllowDeactivate = true;
            }
            //            AllowDeactivate = (DialogResult.Yes == DepictionMessageBox.Show("This action will permanently deactivate the license on this machine. Are you sure you want to do this?", "Deactivate", MessageBoxButtons.YesNo));
        }

        /// <summary>
        /// EVENT HANDLER
        /// The ClientProtector will fire this event if the Copy Protection needs to
        /// be released from the scope of a StartUp call.
        /// </summary>
        /// <param name="ReleaseCPAuthRequestCode">The Release Copy Protection Request Code</param>
        /// <param name="InputActivationCode">returns with the user input Activation Code.</param>
        private void SSCP_OnAttemptReleaseCP(string ReleaseCPAuthRequestCode, out string InputActivationCode)
        {
            // This particular license design will not respond to this event.
            // We will be releasing the Copy Protection ourselves along with
            // enabling any featuers the user buys from outside the scope of the
            // StartUp call with a Run-Time Composite Authorization

            // Just for illustration - if we needed to respond -
            // here is where we would do it.

            InputActivationCode = "";

            //
            //find how many more days of trial are left
            //
        }

        #endregion

        #region Active stuff

        //Hack
        public bool ExitActivation;

        public string UserInputSerialNumber = "";

        public int CheckPointAttemptsFailed { get; private set; }
        public bool IsValidLicense { get; private set; }
        public bool HasValidLicenseFile { get; private set; }
        public bool IsActivated { get; private set; }
        public bool IsDeactivated { get; private set; }
        public bool TrialPeriodExpired { get; private set; }
        public bool IsVirginInstall { get; private set; }

        #region Deactivate
        public void DeactivateLicense()
        {
            string deactConfirmCode;
            int return_code;
            var productName = DepictionAccess.ProductInformation.ProductName;
            var supportEmail = DepictionAccess.ProductInformation.SupportEmail;
            if (IsDeactivated)
            {
                DepictionAccess.NotificationService.SendNotificationDialog("License has already been deactivated",
                                         string.Format("{0} Deactivation", productName));
                return;
            }

            MessageBoxResult result =
                 DepictionAccess.NotificationService.DisplayInquiryDialog(
                    string.Format(
                        "Please only deactivate if you do not plan to run Depiction on this computer again.  You do not need to deactivate to re-install {0}.\n\nNote: if after deactivation, you'd like to install {0} again on this computer, you will have to contact {1} for instructions..\n\nAre you sure you want to continue?",
                        productName,supportEmail),
                    string.Format("{0} Deactivating Warning", productName), MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel) return;

            result = DepictionAccess.NotificationService.DisplayInquiryDialog(
                string.Format(
                    "{0} will exit at the end of this process.\n\nIf you need to save any changes to your current {1}, please cancel and do that before proceeding with deactivating {0}.",
                    productName, DepictionAccess.ProductInformation.StoryName),
                string.Format("{0} Exit Warning", productName), MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.Cancel) return;

            SSCP.RequestDeactivation(out deactConfirmCode, out return_code);
            if ((RETURNCODES)return_code == RETURNCODES.FALSE_PRECONDITION_NOT_MET)
            {
                DepictionAccess.NotificationService.SendNotificationDialog("You cannot deactivate this license since it has never been activated.",
                                         string.Format("{0} Deactivation", productName));
            }
            else if ((RETURNCODES)return_code == RETURNCODES.FALSE_REFUSE_TO_DEACTIVATE)
            {
                DepictionAccess.NotificationService.SendNotificationDialog("Deactivation aborted.",
                                         string.Format("{0} Deactivation", productName));
            }
            else if ((RETURNCODES)return_code == RETURNCODES.FALSE_LICENSE_DEACTIVATED)
            {
                string message = "Deactivation confirmed.\n\n";

                string serialNumberInput = GetSerialNumber();
                message += "--------------------------------------------------------\n";
                message +=
                    string.Format(
                        "If you want to move your license to a new computer, SEND MAIL to {0} with your name, email address used to purchase the {2}, and the following information: \n\nDeactivation code for SN: {1}\n\n",
                        supportEmail, serialNumberInput,productName);
                message += "Deactivation confirmation code: " + deactConfirmCode + ".\n";
                message += "--------------------------------------------------------\n";

                message += "\n\n";
                message +=
                    "Depiction support will then add a new activation to your license. Thanks.\n\n";

                string logFileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "DEACTIVATION_LOG.TXT");

                message += "The following log file has been created for your reference:\n" + logFileName;
                DepictionAccess.NotificationService.SendNotificationDialog(message, "Deactivation -- PLEASE READ INSTRUCTIONS CAREFULLY");
                IsDeactivated = true;
                SerialNumber = "Deactivated";

                //Save the deactivation log and other info in the deactivation_log.txt file
                File.AppendAllText(logFileName, message);
            }
        }

        #endregion

        public bool RequestManualActivation(string serialNumberToTry, int requestAuthCodeID)
        {
            string authReqCode;
            int returnCode;
            //first off, call the RequestActivation function returns an Authorization Request Code 
            //corresponding to an existing Authorization Definition in the currently loaded license. 
            SSCP.RequestActivation(requestAuthCodeID, out authReqCode, out returnCode);
            CheckPointAttemptsFailed++;
            //try to see if they entered a serial number
            SerialNumber = serialNumberToTry;
            //write the serial number out to a txt file
            //write the projString string to fileName
            string fileName = licenseFilename + @".serialnumber.txt";
            StreamWriter sr = File.CreateText(fileName);
            sr.Write(serialNumberToTry);
            sr.Close();

            //call up a browser with auth req id and serial number as parameter
            //the url will give the user option to enter serial number and email id so an activation
            //code specific to their machine can be sent by depiction support
            //
            switch ((RETURNCODES)returnCode)
            {
                case RETURNCODES.FALSE_WAITING_FOR_CODE:
                    string url = "http://www.depiction.com/manual_activation_request?app=depiction&serialnumber=" +
                                 SerialNumber + "&activationrequestid=" + authReqCode;
                    DepictionInternetConnectivityService.OpenBrowserInOrderTo(url, string.Format("manually activate {0}", DepictionAccess.ProductInformation.ProductName));
                    return false;
                default:
                    //notiz why no DepictionAccess.NotificationService.SendNotificationDialog(
                    string message = string.Format("{0} was unable to obtain an authorization request code for your computer. Please send mail to {1}.",
                        DepictionAccess.ProductInformation.ProductName, DepictionAccess.ProductInformation.SupportEmail);
                    DepictionAccess.NotificationService.SendNotificationDialog(message, "Authorization Request Code Error");
                    return false;
            }
        }

        #endregion

        #region SShield specific methods

        /// <summary>
        /// Construct a message to display, given the return code associated with Auto Activation.
        /// </summary>
        /// <param name="return_code">The returncode that was recived when using Auto Activation</param>
        /// <param name="message">The message to display.</param>
        /// <returns>True if the the serial number is successfully activated otherwise fails</returns>
        private static bool HandleAutoActivate(int return_code, out string message)
        {
            message = String.Empty;
            switch ((RETURNCODES)return_code)
            {
                case RETURNCODES.FALSE_NO_SN:
                    message = "No Serial Number was entered";
                    break;

                case RETURNCODES.FALSE_NO_CHECKPOINT_URL:
                    message = "This Application does not have have a CheckPoint URL associated to it";
                    break;

                case RETURNCODES.FALSE_INVALID_SN:
                    message = "Your Serial Number is invalid.";
                    break;

                case RETURNCODES.FALSE_MISSING_MAIN_LF:
                    message = "The Main license project can not be found on the server";
                    break;

                case RETURNCODES.FALSE_MAIN_LF_CORRUPT:
                    message = "The license project on the server is corrupted";
                    break;

                case RETURNCODES.FALSE_INTERNAL_FAILURE:
                    message = "An internal failure occured on the server";
                    break;

                case RETURNCODES.FALSE_NO_ACTIVATIONS:
                    message = "All the activations associated with your Serial Number have been used";
                    break;

                case RETURNCODES.FALSE_CONNECTION_FAILURE:
                    message = "Could not connect to the CheckPoint Server (FALSE_CONNECTION_FAILURE).  If you have a firewall try disabling it.  If activation continues to fail, request a manual activation.";
                    break;

                case RETURNCODES.TRUE_EXPIRY:
                case RETURNCODES.TRUE_NON_EXPIRY:
                case RETURNCODES.FALSE_LICENSE_EXPIRED:
                    //they own the software
                    return true;

                default:
                    message = string.Format("Unrecognized return code: {0}", return_code);
                    return false;
            }


            return false;
        }

        /// <summary>
        /// Constructs a message to the user indicating what is happening and why.
        /// </summary>
        /// <param name="returnCode">The return code from the client protectors StartUp call to interpret.</param>
        /// <param name="msg">The message to display.</param>
        /// <returns>Returns true if the application should be allowed to run, false otherwise.</returns>
        private bool HandleStartUpReturnCode(int returnCode, out string msg)
        {
            var productName = DepictionAccess.ProductInformation.ProductName;
            var emailInfo = DepictionAccess.ProductInformation.SupportEmail;
            var productWebpage = DepictionAccess.ProductInformation.ProductWebpage;
            msg = "";
            switch ((RETURNCODES)returnCode)
            {
                case RETURNCODES.TRUE_NON_EXPIRY:
                    // this means that the copy protection has been released -
                    // run in full mode. They own the software.
                    return true;

                case RETURNCODES.TRUE_EXPIRY:
                    //This return code indicates that while the license 
                    //is an expiring type of license (any of the three types) 
                    //it has not yet expired. No copy protection was found, 
                    //or it was already released, all alias files corroborate and 
                    //are consistent.
                    return true;

                case RETURNCODES.FALSE_LICENSE_DEACTIVATED:
                    // this means that the user has deactivated thier license.
                    msg = string.Format("Your license for this application has been permanently deactivated. {0} will exit.", productName);
                    return false;

                case RETURNCODES.FALSE_INSTALL_DATE_INVALID:
                    // this means that the users clock is wrong and its the first time
                    // we have run - tell the user to correct it.
                    if (!IsAuthorized())
                    {
                        msg = "Your system clock is not set correctly. Please change it to the correct date and time.";
                        return false;
                        //                        return Activate();
                    }
                    return true;

                case RETURNCODES.FALSE_LICENSE_NEEDS_RECOVERY:
                    // this means the user has refused to recover when it was necessary
                    msg = "Your license must be recovered before you can use this application.";
                    return false;

                case RETURNCODES.FALSE_CODE_INVALID_BAD_ID:
                case RETURNCODES.FALSE_CODE_INVALID_BAD_FP:
                case RETURNCODES.FALSE_CODE_INVALID_EXPIRED_SL:
                case RETURNCODES.FALSE_CODE_INVALID_SINGLE_USE:
                case RETURNCODES.FALSE_CODE_INVALID_PASSWORD:
                case RETURNCODES.FALSE_CODE_INVALID_FORMAT:
                    // this group means that the user tried to recover with a bad code...
                    HandleActivationReturnCode(returnCode, out msg);
                    return false;

                case RETURNCODES.FALSE_NOT_AUTH_NOT_EXPIRED:
                case RETURNCODES.FALSE_SYSTEM_NOT_AUTHORIZED:
                    // this means that the copy protection has not been released and
                    // the expiration has not yet lapsed.
                    // this will be our trial mode.
                    return true;

                case RETURNCODES.FALSE_CLOCK_TURNED_BACK:
                    // this means that the user is turning the clock back.
                    if (!IsAuthorized())
                    {
                        msg = string.Format("Your system clock is not set correctly. Please change it to the correct date and time and try again. {0} will exit.", productName);
                        return false;
                        //                        return Activate();
                    }
                    return true;

                case RETURNCODES.FALSE_MISSING_MAIN_LF:
                    // this means that the license file is missing AND they cant find
                    // it from our event handler
                    msg = string.Format("You must have a valid license to run this program. Please contact vendor. {0} will exit.", productName);
                    return false;
                case RETURNCODES.FALSE_MAIN_LF_CORRUPT:
                    msg = "Main license file has been corrupted. ";
                    msg += string.Format(" Send mail to {0} with the following in the Subject: line -- ", emailInfo);
                    msg += " Requesting reactivation of serial number: " + GetSerialNumber();
                    msg += "...\n";
                    return false;
                case RETURNCODES.FALSE_NOT_VIRGIN_INSTALL:
                    IsVirginInstall = false;
                    msg = "A trial copy was already installed on this machine. ";
                    msg += string.Format("You can activate your copy by purchasing at {0}/purchase \n\n",productWebpage);
                    msg += string.Format("Thanks for trying out {0}.", productName);

                    return false;

                case RETURNCODES.FALSE_NOT_AUTH_CLOCK_TURNED_BACK:
                    //if the license has been activated, they're free to move the clock around
                    if (!IsAuthorized())
                    {
                        msg = "System clock on this computer was turned back. Trial copy will not work anymore. ";
                        msg += string.Format("You can activate your copy by purchasing at {0}/purchase \n\n",productWebpage);
                        msg += string.Format("Thanks for trying out {0}.", productName);
                        return false;

                    }
                    return true;
                case RETURNCODES.FALSE_NOT_AUTH_EXPIRED:
                    return true;

                    //                    return Activate();

                case RETURNCODES.FALSE_LICENSE_EXPIRED:
                    //even if copy protection was released, if license expires (15 day or hard date)
                    //you get here
                    //if (!IsAuthorized())
                    //{
                    //    message = "Your trial period has expired. \n\n";
                    //    message += "You can purchase a copy of Depiction at http://www.depiction.com/purchase \n\n";
                    //    message += "Thanks for trying out Depiction.";
                    //    MessageBox.Show(message, "Trial Period Expired", MessageBoxButton.OK);
                    //    return false;
                    //    //                        return Activate();
                    //}
                    return true;
                default:
                    msg = string.Format("ERROR: Unknown return_code: {0} Please send email to {1}. {2} will exit.", ((RETURNCODES)returnCode), emailInfo, productName);
                    return false;
            }
        }

        /// <summary>
        /// This function should only be called when have recieved a return_code as a 
        /// direct result of inputting an activation code. This could occurr from the scope of the StartUp call
        /// </summary>
        /// <param name="return_code">The return_code from the ClientProtector to interpret</param>
        /// <param name="message">Error message if result is false</param>
        /// <returns>Returns true if the application should be allowed to run, false otherwise.</returns>
        private bool HandleActivationReturnCode(int return_code, out string message)
        {
            var supportEmail = DepictionAccess.ProductInformation.SupportEmail;
            message = string.Empty;
            switch ((RETURNCODES)return_code)
            {
                    // first all the good ones:
                case RETURNCODES.TRUE_NON_EXPIRY:
                    // this means that the copy protection and the trial period have
                    // been released - run in full mode. They own the software.
                    return true;

                    // now all the code specific error ones.  We could just handle these
                    // all as a group - but for illustration - we shopw them all separately

                case RETURNCODES.FALSE_CODE_INVALID_BAD_ID:
                    message = string.Format("The code you entered did not have a valid ID. Please send email to {0}.",
                                            supportEmail);
                    break;

                case RETURNCODES.FALSE_CODE_INVALID_BAD_FP:
                    message =
                        string.Format("The code you entered does not match this machine. Please send email to {0}.",
                                      supportEmail);
                    break;

                case RETURNCODES.FALSE_CODE_INVALID_EXPIRED_SL:
                    message = string.Format("The code you entered has expired. Please send email to {0}.",
                                            supportEmail);
                    break;

                case RETURNCODES.FALSE_CODE_INVALID_SINGLE_USE:
                    message =
                        string.Format(
                            "The code you entered has already been used once. It can not be used again. Please send email to {0}.",
                            supportEmail);
                    break;

                case RETURNCODES.FALSE_CODE_INVALID_FORMAT:
                    message = string.Format("The code you entered was in an invalid format. Please send email to {0}.",
                                           supportEmail);
                    break;

                case RETURNCODES.FALSE_CODE_INVALID_PASSWORD:
                    message = string.Format("The code password was incorrect. Please send email to {0}.",
                                            supportEmail);
                    break;

                default:
                    // it may be a different code -
                    return HandleStartUpReturnCode(return_code, out message);
            }
            return false;
        }

        #endregion
    }
}