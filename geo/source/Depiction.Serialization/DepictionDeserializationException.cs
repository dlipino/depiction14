using System;

namespace Depiction.Serialization
{
    //????????? why is this here?
    /// <summary>
    /// Use to advertise problems encountered when deserializing a depiction object.
    /// The message should be a sentence fragment, which will be wrapped
    /// by the catching code.
    /// 
    /// When loading a story, throwing an exception of this type will abort the load
    /// with a friendly message.
    /// </summary>
    public class DepictionDeserializationException : Exception
    {
        public DepictionDeserializationException(string sentenceFragment): base(sentenceFragment)
        {}
    }
}