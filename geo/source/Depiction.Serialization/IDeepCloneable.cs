﻿namespace Depiction.Serialization
{
    public interface IDeepCloneable<T>
    {
        T DeepClone();
    }
}