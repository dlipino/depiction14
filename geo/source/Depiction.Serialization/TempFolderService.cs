﻿using System;
using System.IO;

namespace Depiction.Serialization
{
    public class TempFolderService
    {
        //Not really sure why this class is in here, but it needed to start somewhere
        public TempFolderService(bool isTest)
        {
            if (isTest) FolderName = CreateTempTestDirectory();
            else
            {
                FolderName = CreateTempDepictionDirectory(); 
            }
        }

        ~TempFolderService()
        {
            Close();
            //if (folderName != "") throw new Exception("Ya gotta close me");
        }

        public string FolderName { get; set;}
         public string GetATempFilenameInDir()
         {
             return GetATempFilenameInDir(null);
         }
        public string GetATempFilenameInDir(string dotLessExtension)
        {
            if(!string.IsNullOrEmpty(dotLessExtension))
                return Path.Combine(FolderName, "TF_" + Guid.NewGuid()+"."+dotLessExtension);
            return Path.Combine(FolderName, "TF_" + Guid.NewGuid());
        }

        private string CreateTempTestDirectory()
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "TEST_" + Guid.NewGuid());
            Directory.CreateDirectory(tempDir);
            return tempDir;
        }
        private static string CreateTempDepictionDirectory()
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "ST_" + Guid.NewGuid());
            Directory.CreateDirectory(tempDir);
            return tempDir;
        }

        static private void EmptyFolder(DirectoryInfo directoryInfo)
        {
            foreach (var file in directoryInfo.GetFiles())
            {

                file.Delete();
            }
            foreach (var subfolder in directoryInfo.GetDirectories())
            {
                EmptyFolder(subfolder);
            }
        }
        private static void DeleteTempDirectory(string dirName)
        {
            try
            {



                if (Directory.Exists(dirName))
                {
                    EmptyFolder(new DirectoryInfo(dirName));
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        /// <summary>
        /// This method should delete everything that was created in the temp directory, in addition to removing
        /// the directory itself.
        /// </summary>
        public void Close()
        {
            DeleteTempDirectory(FolderName);
            FolderName = String.Empty;
        }
    }
}