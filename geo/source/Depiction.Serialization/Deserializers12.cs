using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace Depiction.Serialization
{
    public delegate bool FileOperation(string filename);

    public class Deserializers12
    {       
        #region Constants that deal with map size and zooming and stuff

        public const double MapSize = DefaultMapSize*MapSizeMulti;
        private const double DefaultMapSize = 300000;
        public const double MapSizeMulti = 100;
        public const double MaxZoomFactor = 4500.0 / MapSizeMulti;//4500 is the max, there are actually some miss hits when editing zoi at that zoom
        public const double MinZoomFactor = .001 / MapSizeMulti;
        
        
        #endregion

        #region static cheating
        public static Dictionary<string, Type> stringToTypeDictionary = new Dictionary<string, Type>();
        public static Type FindTypeByString(string fullTypeName)
        {
            foreach (var type in stringToTypeDictionary.Keys)
            {
                if (fullTypeName.ToLowerInvariant().Contains(type.ToLowerInvariant()))
                {
                    return stringToTypeDictionary[type];
                }
            }
            return null;
        }

        #endregion

        public const string DepictionXmlNameSpace = "http://depiction.com";
        private static readonly Dictionary<Type, DataContractSerializer> serializers = new Dictionary<Type, DataContractSerializer>();
        private static string rootPath;
        public static XmlReader GetXmlReader(Stream stream)
        {
            var settings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true, CheckCharacters = false };
            return XmlReader.Create(stream, settings);
        }
        protected static DataContractSerializer GetSerializer(Type type)
        {
            DataContractSerializer serializer;

            if (!serializers.TryGetValue(type, out serializer))
            {
                var xmlRootAttribute = type.GetCustomAttributes(typeof(XmlRootAttribute), false);
                if (xmlRootAttribute.Length > 0)
                    serializer = new DataContractSerializer(type, ((XmlRootAttribute)xmlRootAttribute[0]).ElementName, DepictionXmlNameSpace);
                else if (type.IsGenericType || type.IsArray)
                    serializer = new DataContractSerializer(type);
                else
                    serializer = new DataContractSerializer(type, type.Name, DepictionXmlNameSpace);

                serializers.Add(type, serializer);
            }

            return serializer;
        }
        public static string BinaryFileFolder
        {
            get { return Path.Combine(rootPath, "binaryFiles"); }
        }
        public static bool ReadBinaryFile(string fileName, FileOperation action)
        {
            AssertReadyToDoBinaryFileOperation();
            return DoBinaryFileSaveLoadOperation(fileName, action);
        }

        private static void AssertReadyToDoBinaryFileOperation()
        {
            if (rootPath == String.Empty)
                throw new Exception("rootPath must be set in SerializationService before doing a binary save or load");
        }
        private static bool DoBinaryFileSaveLoadOperation(string fileName, FileOperation action)
        {
            var fullFilename = Path.Combine(BinaryFileFolder, fileName);
            return action(fullFilename);
        }

        public static T LoadFromXmlFile<T>(string filePath, string localName)
        {
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = GetXmlReader(stream))
                    {
                        return Deserialize<T>(localName, reader);
                    }
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }
        public static object DeserializeObject(string localName, Type type, XmlReader reader)
        {
            var ns = DepictionXmlNameSpace;
            reader.ReadStartElement(localName, ns);
            var ret = GetSerializer(type).ReadObject(reader);
            reader.ReadEndElement();
            return ret;
        }
        public static T Deserialize<T>(XmlReader reader)
        {
            var ret = (T)GetSerializer(typeof(T)).ReadObject(reader);
            reader.ReadEndElement();
            return ret;
        }
        /// Deserializes an object, that has not been surrounded with additional tags
        public static object DeserializeObject(Type type, XmlReader reader)
        {
            if (type is IXmlSerializable)
            {
                var ret = Activator.CreateInstance(type);
                ((IXmlSerializable)ret).ReadXml(reader);
                return ret;
            }
            return GetSerializer(type).ReadObject(reader);
        }

        public static T Deserialize<T>(string localName, XmlReader reader)
        {
            var ns = DepictionXmlNameSpace;
            reader.ReadStartElement(localName, ns);
            var type = typeof (T);
            var ret = default(T);
            if(type is IXmlSerializable)
            {
                ((IXmlSerializable)ret).ReadXml(reader);
            }else
            {
                ret = (T)GetSerializer(typeof(T)).ReadObject(reader);
            }
            reader.ReadEndElement();
            return ret;
        }
        public static void Serialize<T>(string localName, T obj, XmlWriter writer)
        {
            var ns = DepictionXmlNameSpace;
            writer.WriteStartElement(localName, ns);
            GetSerializer(typeof(T)).WriteObject(writer, obj);
            writer.WriteEndElement();
        }

        public static IList<T> DeserializeItemList<T>(string localName, Type type, XmlReader reader)
        {
            var ns = DepictionXmlNameSpace;
            var elementList = new List<T>();

            //Return empty list if the node type is empty (davidl). would it be better to return a null?
            if (reader.NodeType.Equals(XmlNodeType.EndElement)) return elementList;
            try
            {
                bool listIsEmpty = reader.IsEmptyElement;
                reader.ReadStartElement(localName);//, ns);
                if (!listIsEmpty)
                {
                    while (reader.NodeType != XmlNodeType.EndElement)
                    {
                        var interType = type;
                        if (interType == null)
                        {
                            var typeName = reader.Name;
                            interType = Type.GetType(typeName);
                        }
                        try
                        {//If a single object gives a problem keep going
                            var element = DeserializeObject(interType, reader);
                            elementList.Add((T)element);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                            if (reader.NodeType.Equals(XmlNodeType.EndElement) && reader.Name.Equals("Property"))
                            {
                                reader.Read();
                            }
                        }

//                        reader.MoveToContent();
                    }
                    reader.ReadEndElement();
                }
            }
            catch (Exception ex) { return new List<T>(); }

            return elementList;
        }
        public static IList<T> DeserializeHeterogeneousItemList<T>(string localName, string itemName, XmlReader reader)
        {
            var ns = DepictionXmlNameSpace;
            var elementList = new List<T>();

            bool listIsEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(localName, ns);
            if (!listIsEmpty)
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    var typeName = reader.ReadElementContentAsString(itemName + "_typeName", ns);
                    var type = Type.GetType(typeName);
                    if (type == null)
                    {
                        type = FindTypeByString(typeName);
                    }

                    var element = (T)DeserializeObject(itemName, type, reader);

                    elementList.Add(element);
                    reader.MoveToContent();
                }
                reader.ReadEndElement();
            }
            return elementList;
        }
        public static T DeserializeFromFolder<T>(string localName, Stream xmlStream, string folderName)
        {
            rootPath = folderName;
            T retVal;
            using (var reader = GetXmlReader(xmlStream))
            {
                retVal = Deserialize<T>(localName, reader);
            }
            rootPath = "";
            return retVal;
        }
    }
}