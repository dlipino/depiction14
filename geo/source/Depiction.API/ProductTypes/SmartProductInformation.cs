﻿namespace Depiction.API.ProductTypes
{
    public class SmartProductInformation : ProductInformationBase
    {
        #region Properties that may be different between Release and Debug

        public override string DirectoryNameForCompany { get { return "Dfm"; } }
        public override string LicenseFileDirectory { get { return "SMART"; } }
        public override string MetricsServiceName { get { return "ReleaseMetricsService"; } }
        public override string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
        public override string LicensingServiceName { get { return "ReleaseLicensingService"; } }
        public override string ProductValidatorName { get { return "ReleaseDepictionProductValidator"; } }
        public override string ProcessName { get { return "SMART"; } }

        #endregion

        #region Properties that vary only on _productInformation "brand"

        public override string UiConfigXamlUri { get { return "Depiction.DefaultResources;Component/ProductResources/UiConfigDFM.xaml"; } }
        public override string FilePassword { get { return "Wh1t3lAb3L"; } }
        public override string FileExtension { get { return ".dfm"; } }
        public override string ProductName { get { return "SMA/RT"; } }
        public override string CompanyName { get { return "Dinn Focused Marketing, Inc."; } }
        public override string SupportEmail { get { return "michael@dinnfm.com"; } }

        public override string StoryName { get { return "SMA/RT depiction"; } }

        public override string SplashScreenPath { get { return "DefaultImages/SplashScreens/SMARTSplash.png"; } }
        public override string LicenseFileName { get { return "SMART-V1.lic"; } }
        public override int LicenseActivationID { get { return 0; } }
        public override int LicenseRenewalID { get { return 1; } }
        public override bool RenewableLicense
        {
            get { return true; }
        }

        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} Portions © 2009 Dinn Focused Marketing, Inc. All rights reserved. \"SMA/RT\" is a trademark of Dinn Focused Marketing, Inc. All other trademarks are the property of their respective owners.",
                                     ProductVersion, DepictionCopyright);
            }
        }
        public override int ProductID { get { return 14; } }
        #endregion
    }
}