﻿namespace Depiction.API.ProductTypes
{
    public class DepictionPrepProductInformation : DepictionProductInformation
    {
        override public string DirectoryNameForCompany { get { return "DepictionPrep"; } }
        public override string ProcessName { get { return "DepictionPrep"; } }
        public override string ProductName { get { return "DepictionPrep beta"; } }
        public override string SplashScreenPath { get { return "PrepAppImages/PrepSplashScreen.png"; } }
        public override string LicenseFileDirectory { get { return "DepictionPrep"; } }
        override public string LicenseFileName
        {
            get { return "depictionprep.lic"; }
        }
        public override int ProductID { get { return 19; } }
    }
}