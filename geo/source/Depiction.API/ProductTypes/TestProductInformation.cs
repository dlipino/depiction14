﻿using Depiction.API.ProductTypes;

namespace Depiction.API.ProductTypes
{
    public class TestProductInformation : DepictionProductInformation
    {
        override public string DirectoryNameForCompany { get { return "Depiction_Testing"; } }
    }
}