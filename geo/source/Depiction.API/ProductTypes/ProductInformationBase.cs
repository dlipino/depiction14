﻿using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API;

namespace Depiction.API.ProductTypes
{
    public abstract class ProductInformationBase : IProductInformation
    {
        /// <summary>
        /// Name for directory under Environment.SpecialFolders.CommonProgramFiles.
        /// </summary>
        public abstract string DirectoryNameForCompany { get; }

        public virtual bool BypassLicenseValidation {get { return false; }}
        
        public virtual string LicenseFileDirectory { get { return "Depiction"; } }

        /// <summary>
        /// Name of web service for collection usage statistics.
        /// </summary>
        public abstract string MetricsServiceName { get; }
        /// <summary>
        /// Name of web service for providing list of available quickstart items for the region.
        /// </summary>
        public abstract string QuickstartServiceName { get; }
        /// <summary>
        /// Name of web service for validating the application's license.
        /// </summary>
        public abstract string LicensingServiceName { get; }

        public abstract string ProductValidatorName { get; }

        /// <summary>
        /// Name of process used by main application.
        /// </summary>
        public abstract string ProcessName { get; }
        /// <summary>
        /// Package path to the xaml file containing _productInformation-specific UI aspects.
        /// </summary>
        public abstract string UiConfigXamlUri { get; }
        /// <summary>
        /// The password to use for encrypting the depiction file.
        /// Return null to not use encryption.
        /// </summary>
        public abstract string FilePassword { get; }
        /// <summary>
        /// File extension, including leading dot, for saved depiction file.
        /// </summary>
        public abstract string FileExtension { get; }
        /// <summary>
        /// Name of _productInformation to show user in the UI.
        /// </summary>
        public abstract string ProductName { get; }
        /// <summary>
        /// Name of company creating this _productInformation.
        /// </summary>
        public abstract string CompanyName { get; }

        public abstract string SupportEmail { get; }

        /// <summary>
        /// Phrase to use when referring to a depiction file.
        /// </summary>
        virtual public string StoryName { get { return string.Empty; } }
        /// <summary>
        /// Package path to the splash screen image.
        /// </summary>
        public abstract string SplashScreenPath { get; }
        /// <summary>
        /// Name of the .lic license file.
        /// </summary>
        public abstract string LicenseFileName { get; }
        public abstract int LicenseActivationID { get; }
        public abstract int LicenseRenewalID { get; }
        /// <summary>
        /// Whether the license for this _productInformation is renewable.
        /// </summary>
        public abstract bool RenewableLicense { get; }
        /// <summary>
        /// _productInformation ID in Checkpoint license server
        /// </summary>
        public abstract int ProductID { get; }
        /// <summary>
        /// _productInformation-specific text for the About box.
        /// </summary>
        public abstract string AboutText { get; }
        public string DepictionCopyright { get { return "© 2008-2010 Depiction, Inc. All rights reserved. \"Depiction\" and \"More than Mapping\" are either registered trademarks or trademarks of Depiction, Inc. All rights reserved."; } }
        public string ProductVersion { get { return string.Format("{0}™ {1}", ProductName, VersionInfo.FullAppVersion); } }
       

    }
}