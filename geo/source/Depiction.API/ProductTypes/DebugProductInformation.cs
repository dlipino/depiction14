﻿namespace Depiction.API.ProductTypes
{
    public class DebugProductInformation : DepictionProductInformation
    {
        #region Properties that may be different between Release and Debug

        override public string DirectoryNameForCompany { get { return "Depiction_Debug"; } }
        override public bool BypassLicenseValidation { get { return true; } }
        override public string MetricsServiceName { get { return "DebugMetricsService"; } }
        //override public string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
        override public string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
        override public string LicensingServiceName { get { return "DebugLicensingService"; } }
        override public string ProductValidatorName { get { return "DebugDepictionProductValidator"; } }
        override public string ProcessName { get { return "Depiction.vshost"; } }
        //public override string LicenseFileName { get { return "SMART-V1.lic"; } }
        public override int ProductID { get { return 8; } }
        public override bool RenewableLicense { get { return true; } }
        
        #endregion
    }
}