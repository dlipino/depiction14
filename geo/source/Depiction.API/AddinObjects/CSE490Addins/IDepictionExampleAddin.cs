namespace Depiction.APINew.AddinObjects.CSE490Addins
{
    public interface IDepictionExampleAddin
    {
        object AddinSimpleConfigView { get; }
    }
}