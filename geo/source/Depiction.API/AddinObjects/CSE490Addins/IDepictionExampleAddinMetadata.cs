namespace Depiction.APINew.AddinObjects.CSE490Addins
{
    public interface IDepictionExampleAddinMetadata
    {
        string Name { get;  }
        string DisplayName { get;  }
        string Description { get;  }
        string Author { get;  }
    }
}