namespace Depiction.API.AddinObjects.Interfaces
{
    public interface IDepictionNonDefaultImporter : IDepictionImporterBase
    {
        //Does the importer have permission to function ie the license check (hopefully)
        bool Activated { get; }
    }

}