namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IBaseMetadata
    {
        string AddonPackage { get; }

        string Name { get; }
        string Author { get; }
        string Company { get; }
        string DisplayName { get; }
        string Description { get; }
        bool IsDefault { get; }

        AddonUsage[] AddonUsages { get; }
    }
}