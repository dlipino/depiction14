namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IDepictionExtensionIOMetadata : IBaseMetadata
    {
        string FileTypeInformation { get; }
        string[] ExtensionsSupported { get; }
        string[] ElementTypesSupported { get; }
        string[] ElementTypesNotSupported { get; }
    }
}