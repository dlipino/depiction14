using Depiction.API.CoreEnumAndStructs;

namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IDepictionDefaultImporterMetadata : IDepictionExtensionIOMetadata
    {
        InformationSource[] ImporterSources { get; }
    }
}