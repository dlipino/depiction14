﻿namespace Depiction.API.AddinObjects.Interfaces.Metadata
{
    public interface IConditionMetaData : IBaseMetadata
    {
        string ConditionName { get; }
        string ConditionDescription { get; }
    }
}