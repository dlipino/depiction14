using System.Collections.Generic;
using Depiction.API.EnhancedTypes;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.AddinObjects.Interfaces
{
    public interface IDepictionImporterBase:IDepictionAddonBase
    {
        event ElementImportCompleteEventHandler ElementImportingComplete;
        //This method is total legacy
        void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters);

    }

    public delegate void ElementImportCompleteEventHandler(object sender, ElementImportCompleteEventHandlerArgs args);
}