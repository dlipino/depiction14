using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.AddinObjects.Interfaces
{
    /// <summary>
    ///  A very simplistic Icondition, 1.3 won't change this much, 1.4 will go to town on this. Simple because it only compairs 2 things.
    /// THis could be made much much cooler
    /// </summary>
    public interface ICondition
    {
        string Name { get; }
        HashSet<string> ConditionTriggerProperties { get; }
        /// <summary>
        /// Executes this instance.
        /// </summary>
        bool Evaluate(IDepictionElementBase triggerElement, IDepictionElementBase affectedElement, object[] inputParameters);
    }
}