﻿using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.API.ExtensionMethods;

namespace Depiction.API.AddinObjects.AbstractObjects
{
    /// <summary>
    /// BaseBehavior is the base class for Behaviors.
    /// A behavior is a way to encapsulate a set of logic to modify an element.
    /// Behaviors are invoked either by an interaction, 
    /// or by a postSetActions after an element's property changes value.
    /// </summary>
    public abstract class BaseBehavior
    {
        public abstract ParameterInfo[] Parameters { get; }
        protected abstract BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag);

        public virtual BehaviorResult DoBehavior(IDepictionElement subscriber, object[] inputParameters)
        {
            if (Parameters == null && inputParameters.Length != 0)
            {
                return new BehaviorResult();
            }
            if (subscriber == null) return new BehaviorResult();
            if (Parameters != null && Parameters.Length != 0 && inputParameters.Length != Parameters.Length)
            {
                return new BehaviorResult();
            }

            var parameterBag = new Dictionary<string, object>();

            for (int i = 0; i < inputParameters.Length; i++)
            {

                var inputParam = inputParameters[i];
                //Two different methods that this gets called, thrue interactions and from a post set actions
                if(inputParam is string && inputParam.ToString().StartsWith("."))
                {//Coming from post set action, dml file
                    var propName = inputParam.ToString().Remove(0, 1);
                    parameterBag.Add(propName, subscriber.Query(inputParam.ToString()));
                }else
                {//Coming from an interaction
                    ParameterInfo userParameter = Parameters[i];
                    var userKey = userParameter.ParameterKey;
                    parameterBag.Add(userKey, inputParameters[i]);
                }

            }

            return InternalDoBehavior(subscriber, parameterBag);
        }
    }
}