﻿using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.Interfaces;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class GeocoderAttribute : DepictionAddonBaseMetadata, IGeocoderMetadata
    {
        public GeocoderAttribute(string name)
            : base(typeof(IDepictionGeocoder))
        {
            Name = name;
            DisplayName = name;
        }
    }
}