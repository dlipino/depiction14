using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DepictionNonDefaultImporterMetadata : DepictionAddonBaseMetadata, IDepictionNonDefaultImporterMetadata
    {
        #region variables

        protected string[] extensionsSupported = new string[0];
        protected string[] typesSupported = new string[0];
        protected string[] typesNotSupported = new string[0];
        protected string[] validRegionCodes = new string[0];
        protected InformationSource[] importerSources = new[] {InformationSource.Web,InformationSource.File};

        #endregion
        #region Properties

        public string FileTypeInformation { get; set; }

        public string[] ExtensionsSupported
        {
            get { return extensionsSupported; }
        }

        public string[] ElementTypesSupported
        {
            get { return typesSupported; }
            set{ typesSupported = value;}
        }
        public string[] ElementTypesNotSupported
        {
            get { return typesNotSupported; }
            set{ typesNotSupported = value;}
        }

        public InformationSource[] ImporterSources
        {
            get { return importerSources; }
            set { importerSources = value; }
        }

        public string[] ValidRegions
        {
            get { return validRegionCodes; }
            set { validRegionCodes = value; }
        }

        #endregion

        #region constructors

        public DepictionNonDefaultImporterMetadata(Type metadataForType) : base(metadataForType)
        {
            Name = "Importer";
            DisplayName = "Importer";
        }

        public DepictionNonDefaultImporterMetadata(string name)
            : base(typeof(IDepictionNonDefaultImporter))
        {
            Name = name;
        }
        public DepictionNonDefaultImporterMetadata(string name, string[] extensions)
            : this(name)
        {
            extensionsSupported = extensions;
        }
        public DepictionNonDefaultImporterMetadata(string name, string[] extensions, string[] types)
            : this(name)
        {
            extensionsSupported = extensions;
            typesSupported = types;
        }
        #endregion
    }
}