using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class DepictionElementExporterMetadata : DepictionAddonBaseMetadata, IDepictionExporterMetadata
    {
        #region variables

        protected string[] extensionsSupported = new string[0];
        protected string[] typesSupported = new string[0];
        protected string[] typesNotSupported = new string[0];

        #endregion
        #region Properties

        public string FileTypeInformation { get; set; }

        public string[] ExtensionsSupported
        {
            get { return extensionsSupported; }
        }

        public string[] ElementTypesSupported
        {
            get { return typesSupported; }
        }

        public string[] ElementTypesNotSupported
        {
            get { return typesNotSupported; }
        }

        //        virtual public string Company
        //        {
        //            get { return "Depiction"; }
        //        }

        #endregion

        #region constructors

        public DepictionElementExporterMetadata(Type metadataForType)
            : base(metadataForType)
        {
            name = "Exporter";
            displayName = "Exporter";
        }

        public DepictionElementExporterMetadata(string name)
            : base(typeof(IDepictionElementExporter))
        {
            Name = name;
        }
        public DepictionElementExporterMetadata(string name, string[] extensions)
            : this(name)
        {
            extensionsSupported = extensions;
        }
        public DepictionElementExporterMetadata(string name, string[] extensions, string[] types)
            : this(name)
        {
            extensionsSupported = extensions;
            typesSupported = types;
        }
        #endregion



    }
}