﻿using System;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.AddinObjects.MEFMetadata
{
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public sealed class ConditionAttribute : DepictionAddonBaseMetadata, IConditionMetaData
    {
        public ConditionAttribute(string ConditionName, string ConditionDisplayName, string ConditionDescription)
            : base(typeof(ICondition))
        {
            this.ConditionName = ConditionName;
            Name = DisplayName = ConditionDisplayName;

            this.ConditionDescription = ConditionDescription;
        }

        public string ConditionName
        {
            get;
            set;
        }

        public string ConditionDescription
        {
            get;
            set;
        }
    }
}