using System.Collections;
using System.Collections.Generic;
//Came from 1.2, not sure of the original source, possible Socha
namespace Depiction.API.EnhancedTypes
{
    public class ThreadSafeDictionary<K, V> : IDictionary<K, V>
    {
        private readonly IDictionary<K, V> dictionary;

        public ThreadSafeDictionary()
        {
            dictionary = new Dictionary<K, V>();
        }

        public ThreadSafeDictionary(IEqualityComparer<K> comparer)
        {
            dictionary = new Dictionary<K, V>(comparer);
        }

        public ThreadSafeDictionary(IDictionary<K, V> dictionary)
        {
            this.dictionary = new Dictionary<K, V>(dictionary);
        }

        public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
        {
            Dictionary<K, V> newCollection;
            lock (this)
            {
                newCollection = new Dictionary<K, V>(dictionary);
            }
            return newCollection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(KeyValuePair<K, V> item)
        {
            lock (this)
            {
                dictionary.Add(item.Key, item.Value);
            }
        }

        public void Clear()
        {
            lock (this)
            {
                dictionary.Clear();
            }
        }

        public bool Contains(KeyValuePair<K, V> item)
        {
            lock (this)
            {
                return dictionary.Contains(item);
            }
        }

        public void CopyTo(KeyValuePair<K, V>[] array, int arrayIndex)
        {
            lock (this)
            {
                dictionary.CopyTo(array, arrayIndex);
            }
        }

        public bool Remove(KeyValuePair<K, V> item)
        {
            lock (this)
            {
                return dictionary.Remove(item);
            }
        }

        public int Count
        {
            get
            {
                lock (this)
                {
                    return dictionary.Count;
                }
            }
        }

        public bool IsReadOnly
        {
            get
            {
                lock (this)
                {
                    return dictionary.IsReadOnly;
                }
            }
        }

        public bool ContainsKey(K key)
        {
            lock (this)
            {
                return dictionary.ContainsKey(key);
            }
        }

        public void Add(K key, V value)
        {
            lock (this)
            {
                dictionary.Add(key, value);
            }
        }

        public bool Remove(K key)
        {
            lock (this)
            {
                return dictionary.Remove(key);
            }
        }

        public bool TryGetValue(K key, out V value)
        {
            lock (this)
            {
                return dictionary.TryGetValue(key, out value);
            }
        }

        public V this[K key]
        {
            get
            {
                lock (this)
                {
                    return dictionary[key];
                }
            }
            set
            {
                lock (this)
                {
                    dictionary[key] = value;
                }
            }
        }

        public ICollection<K> Keys
        {
            get
            {
                ICollection<K> collection;
                lock (this)
                {
                    collection = new List<K>(dictionary.Keys);
                }
                return collection;
            }
        }

        public ICollection<V> Values
        {
            get
            {
                ICollection<V> collection;
                lock (this)
                {
                    collection = new List<V>(dictionary.Values);
                }
                return collection;
            }
        }
    }
}