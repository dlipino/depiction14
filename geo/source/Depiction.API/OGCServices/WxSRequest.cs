﻿using System;
using System.Collections.Generic;
using System.Xml;
using CarbonTools;
using CarbonTools.Content.Features;
using CarbonTools.Content.OGC;
using CarbonTools.Content.OGC.Capabilities;
using CarbonTools.Content.OGC.GML;
using CarbonTools.Content.OGC.WFS;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Interfaces.WebDataInterfaces;
using IRepeatableRequest=Depiction.API.Interfaces.WebDataInterfaces.IRepeatableRequest;

namespace Depiction.API.OGCServices
{
    internal class WxsRequest : Interfaces.WebDataInterfaces.IRepeatableRequest
    {
        private readonly string connectionString;
        private readonly string layerName;
        private readonly IMapCoordinateBounds regionExtent;
        private readonly Response response;
        private string lastErrorMessage = "";
        private LayerItemWFS layerItem;


        public WxsRequest(string layerName, string serverType, string outputFilename, string ConnectionString, IMapCoordinateBounds regionExtent)
        {
            if (serverType != "WFS")
                throw new Exception("Only Support WFS");
            this.layerName = layerName;
            connectionString = ConnectionString;
            this.regionExtent = regionExtent;
            response = new Response { ResponseFile = outputFilename };
        }

        #region IRepeatableRequest Members

        public string Requestor { get; set; }

        public Response Response
        {
            get { return response; }
        }

        public void Execute()
        {
            try
            {
                var ogcCapSource = new SourceOGCCapabilities { Address = new Uri(connectionString), ServiceType = OGCServiceTypes.WFS };

                var ogcHandler = new HandlerOGCCapabilities(ogcCapSource);
                ogcHandler.GetCapabilities();
                if (ogcHandler.Data != null && ((DataOGCCapabilities)ogcHandler.Data).LayerItems != null && ((DataOGCCapabilities)ogcHandler.Data).LayerItems.Count == 0 && ogcCapSource.ServiceType == OGCServiceTypes.WFS)
                {
                    //try a different version of WFS
                    ogcCapSource.Version = "1.0.0";
                    ogcHandler = new HandlerOGCCapabilities(ogcCapSource);
                    ogcHandler.GetCapabilities();
                }

                //Get all layer names
                var ogcData = ogcHandler.Data as DataOGCCapabilities;
                var source = new SourceWFS();
                layerItem = GetLayerItemFromOGCData(layerName, ogcData);
                if (layerItem == null)
                {
                    //DepictionAccess.NotificationService.SendNotification(string.Format("Layer name: {0} not found at WFS server {1}", layerName, connectionString));
                    response.IsRequestSuccessful = false;
                    return;
                }
                // Set additional query parameters

                List<BoundingBox> bboxes = layerItem.GetBoundingBoxes();
                if (bboxes != null)
                    source.BBox = bboxes[0].Clone() as BoundingBox;
                else
                    source.BBox = layerItem.LLBoundingBox.Clone() as BoundingBox;
                if (source.BBox == null)
                {
                    response.IsRequestSuccessful = false;
                    return;
                }
                source.BBox.Set(regionExtent.Left, regionExtent.Bottom, regionExtent.Right, regionExtent.Top);

                source.Format = GmlVersions.GML2;

                DataFeatures data;
                // this is a hack to work around some problems with CarbonTools api
                // GetDepictionWFSData does much less load on the geoserver and returns much more quickly
                bool isDepictionPortal = connectionString.Contains("portal.depiction.com") || connectionString.Contains("geoserver.depiction") ||
                                         connectionString.Contains("depictioninc");

                if (isDepictionPortal)
                {
                    data = GetDepictionWFSData(ogcData, source);
                }
                else
                {
                    data = GetStandardWFSData(ogcData, source);
                }
                XmlDocument doc = FeaturesToGML.GetGML(data, null);
                if (doc != null)
                {
                    doc.Save(response.ResponseFile);
                    response.IsRequestSuccessful = true;
                    //response.DataFeatures = data;
                    return;
                }

                if (data.Features == null)
                {
                    response.ResponseFile = null;
                    response.IsRequestSuccessful = false;
                    return;
                }
            }
            catch (CarbonToolsException e)
            {
                lastErrorMessage = e.Message;
            }

            response.IsRequestSuccessful = false;
        }

        public string LastErrorMessage
        {
            get { return lastErrorMessage; }
        }

        public void Cancel()
        {
            if (response != null)
                response.IsRequestCanceled = true;
        }

        #endregion

        private DataFeatures GetDepictionWFSData(DataOGCCapabilities ogcData, SourceWFS source)
        {
            source.UseBoundingBoxFilter = true;

            SetWfsAddressAndLayer(ogcData, source);

            var handler = new HandlerWFS(source);
            handler.UseSchema = false; // Use non-validating parser

            handler.GetFeature();

            return handler.Data as DataFeatures;
        }

        private DataFeatures GetStandardWFSData(DataOGCCapabilities ogcData, SourceWFS source)
        {
            //set the Uri and layer name for the Wfs source
            SetWfsAddressAndLayer(ogcData, source);

            HandlerWFS handler = SetWfsGeometryFilter(source);
            DataFeatures data;
            handler.GetFeature();

            data = handler.Data as DataFeatures;
            return data;
        }

        private HandlerWFS SetWfsGeometryFilter(SourceWFS source)
        {
            source.UseBoundingBoxFilter = true;
            source.MaxFeatures = 1;
            var handler = new HandlerWFS(source);
            handler.UseSchema = false; // Use non-validating parser

            handler.GetFeature();

            var data = handler.Data as DataFeatures;
            if (data != null && data.Features == null)
            {
                //try GML3
                source.Format = GmlVersions.GML3;
                handler.GetFeature();
                data = handler.Data as DataFeatures;
            }

            if (data != null && data.Features != null && data.HasData())
            {
                ItemGeometry ig = FeatureAnalysis.GetGeometry(data.Features[0] as ItemMember);
                if (ig != null)
                {
                    if (String.IsNullOrEmpty(ig.Parent.Prefix)) source.FilterProperty = ig.Parent.Name;
                    else source.FilterProperty = ig.Parent.Prefix + ":" + ig.Parent.Name;
                    source.UseBoundingBoxFilter = true;
                    source.MaxFeatures = -1;
                }
            }
            return handler;
        }

        private void SetWfsAddressAndLayer(DataOGCCapabilities ogcData, SourceWFS source)
        {
            if (ogcData != null)
            {
                RequestItem requestItem = ogcData.RequestItems.Find("GetFeature");

                //Set the WFS Address
                string address = null;
                if (requestItem != null) address = requestItem.GetDCPOnlineResource("HTTP", "POST");
                if (address == null && requestItem != null) address = requestItem.GetDCPOnlineResource("HTTP", "GET");
                if (address != null) source.Address = new Uri(address);

                source.Layers.Add(new WFSLayerType(layerItem.Name));
            }
        }


        private static LayerItemWFS GetLayerItemFromOGCData(string layerName, DataOGCCapabilities ogcData)
        {
            if (ogcData.LayerItems != null)
            {
                foreach (LayerItem item in ogcData.LayerItems)
                {
                    if (item.Title == layerName)
                        return item as LayerItemWFS;
                }
                //didn't find the layer by title
                //see if you can find it by the layer's name
                foreach (LayerItem item in ogcData.LayerItems)
                {
                    if (item.Name == layerName)
                        return item as LayerItemWFS;
                }
            }
            return null;
        }
    }
}
