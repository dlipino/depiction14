﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Drawing;
using CarbonTools.Content;
using CarbonTools.Content.OGC;
using CarbonTools.Content.OGC.Capabilities;
using CarbonTools.Content.OGC.WMS;
using Depiction.APINew.Interfaces.GeoTypeInterfaces;
using Depiction.APINew.Interfaces.WebDataInterfaces;
using Depiction.APINew.StaticAccessors;
using Depiction.APINew.ValueTypes;

namespace Depiction.APIUnmanaged.Service
{
    public class WmsDataProvider : IDataRetriever
    {
        private readonly string serverUrl;
        private readonly string layerName;
        private readonly IMapCoordinateBounds boundingBox;
        private readonly string requestedFormat;
        private readonly string layerTitle;
        private readonly string mapStyle;
        private readonly string requestedElement;
        private readonly Image legendImage;

        private DataOGCCapabilities ogcData;
        private EventWaitHandle waitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);

        public WmsDataProvider(string serverUrl, string layerName, IMapCoordinateBounds boundingBox, string format)
            : this(serverUrl, layerName, boundingBox, "", format, "", "Depiction.Plugin.Image", null)
        { }

        public WmsDataProvider(string serverUrl, string layerName, IMapCoordinateBounds boundingBox, string style, string format, string layerTitle, string elementType, Image legend)
        {
            this.serverUrl = serverUrl;
            this.layerName = layerName;
            this.boundingBox = boundingBox;
            requestedFormat = format;
            this.layerTitle = layerTitle;
            mapStyle = style;
            requestedElement = elementType;
            legendImage = legend;
        }

        public string LayerTitle
        {
            get { return layerTitle; }
        }

        public Response GetData()
        {
            var dpResponse = new Response();
            //
            //Upon Carbontools support's advice, we are using the handlers in asynch mode
            //
            //URL used to verify this: http://www.pdc.org/wms/wmservlet/PDC_Active_Hazards?
            //Feb 27 2010
            GetOGCCapabilities();

            //Find the service address using the request items in capabilities
            if (ogcData == null)
            {
                //DepictionAccess.NotificationService.SendNotification(string.Format("GetCapabilities failed for WMS request {0}.", serverUrl));
                return dpResponse;
            }

            RequestItem requestItem = ogcData.RequestItems.Find("GetMap");
            var found = false;
            if (requestItem != null)
            {
                foreach (var supportedFormat in requestItem.Formats)
                {
                    if (supportedFormat == requestedFormat)
                    {
                        found = true;
                        break;
                    }
                }
            }
            if (!found)
            {
                //DepictionAccess.NotificationService.SendNotification(
                //    "Requested format not supported from WMS data provider.");
                return dpResponse;
            }

            //Set the WFS Address
            string address = requestItem.GetDCPOnlineResource("HTTP", "GET");


            List<LayerItemWMS> liWms = GetLayerItemFromOGCData(layerName, ogcData);
            if (liWms == null || liWms.Count == 0)
            {
                //DepictionAccess.NotificationService.SendNotification(
                //    string.Format("Layer name: {0} not found at WMS server {1}", layerName, serverUrl));
                return dpResponse;
            }


            var source = new SourceWMS();

            //
            //now, if the layer itself has a coordinate system try using that first
            //if not, look for SRS for the whole WMS service
            //
            if (liWms[0].LLBoundingBox != null)
                source.BBox = liWms[0].LLBoundingBox.Clone() as CarbonTools.BoundingBox;
            else
            {
                List<CarbonTools.BoundingBox> bboxes = liWms[0].GetBoundingBoxes();
                if (bboxes != null) source.BBox = bboxes[0].Clone() as CarbonTools.BoundingBox;
            }
            if (source.BBox == null)
            {
                dpResponse.IsRequestSuccessful = false;
                return dpResponse;
            }
            source.BBox.Set(boundingBox.Left, boundingBox.Bottom, boundingBox.Right, boundingBox.Top);

            source.Address = new Uri(address);
            foreach (LayerItemWMS item in liWms)
                source.Layers.Add(new WMSLayerType(item.Name, mapStyle ?? ""));

            source.Format = requestedFormat;
            source.Version = ogcData.Version;
            source.Width = 1024;// (int)boundingBox.MapImageSize.Width;
            source.Height = 1024;// (int)boundingBox.MapImageSize.Height;
            //TODO: get image size from bbox or say, from parameters of this QS item
            var handler = new HandlerWMS(source);
            string extension;
            if (requestedFormat.Equals("image/png"))
                extension = "png";
            else if (requestedFormat.Equals("image/jpg"))
                extension = "jpg";
            else
                extension = "jpg";
            var filename = Path.Combine(ProductAndFolderService.PathService.TempFolderPath, string.Format("WMSRequest-{0}.{1}", Guid.NewGuid().ToString("N"), extension));

            handler.GetMap(filename);
            if (!TestState.IsError(handler.State))
            {
                if (requestedElement != "Depiction.Plugin.Elevation")
                {
                    //garden-variety images coming in need to be warped from Geo to Mercator
                    //elevation data gets warped/transformed by the Terrain classes and so we don't
                    //need to use Warp routines at this stage
                    string warpedImageFilename = WarpFromGeoToMercator(source, filename);
                    var coverageInfo = new DepictionImageMetadata(warpedImageFilename,
                                                                  new LatitudeLongitude(boundingBox.Top, boundingBox.Left),
                                                                  new LatitudeLongitude(boundingBox.Bottom,
                                                                                        boundingBox.Right),
                                                                  "WMS:" + serverUrl);
                    var transparentColorString = "#FFFFFF";
                    MakeTransparent(warpedImageFilename, transparentColorString);
                    string legendImageFilename = "";
                    if (legendImage != null)
                        legendImageFilename = AddLegendImage(legendImage, warpedImageFilename);

                    if (!String.IsNullOrEmpty(legendImageFilename))
                    {
                        coverageInfo.ImageFilename = legendImageFilename;
                    }
                    //var data = new DataProviderData
                    //{
                    //    CoverageInfos = new[] { coverageInfo },
                    //    DataClass = DataClass.Coverage,
                    //    DataFilename = String.IsNullOrEmpty(legendImageFilename) ? warpedImageFilename : legendImageFilename,
                    //    RetrievedFromPath = serverUrl
                    //};
                    dpResponse.ResponseFile = warpedImageFilename;
                    dpResponse.IsRequestSuccessful = true;
                }
                else if (requestedElement == "Depiction.Plugin.Elevation")
                {
                    //the WMS data, in whatever format, is elevation or some other sort of coverage data
                    //if it is in geotiff format, no worries
                    //but if it's in png or jpg formats, we need to write out a jpgw or pngw world file
                    //to be used by the coveragedata converter

                    //dpResponse.Data = new DataProviderData
                    //{
                    //    CoverageInfos = null,
                    //    DataClass = DataClass.Coverage,
                    //    DataFilename = filename,
                    //    RetrievedFromPath = serverUrl
                    //};
                    dpResponse.IsRequestSuccessful = true;

                }
            }
            else dpResponse.IsRequestSuccessful = false;

            return dpResponse;
        }

        private static string AddLegendImage(Image image, string warpedImageFilename)
        {
            var imageBitmap = new Bitmap(warpedImageFilename);
            var gr = Graphics.FromImage(imageBitmap);
            int width = image.Width;
            int height = image.Height;
            int x = imageBitmap.Width - width;
            int y = imageBitmap.Height - height;

            gr.DrawImage(image, x > 0 ? x : 0, y > 0 ? y : 0, width, height);

            var filename = Path.Combine(ProductAndFolderService.PathService.TempFolderPath, string.Format("WMSRequest-{0}.png", Guid.NewGuid().ToString("N")));

            imageBitmap.Save(filename, ImageFormat.Png);
            imageBitmap.Dispose();
            return filename;
        }


        private string WarpFromGeoToMercator(SourceWMS source, string filename)
        {
            //the wms request was saved as JPG
            //it is in Geographic coordinate system
            //project it to Mercator
            double errorThreshold = 0;
            ILatitudeLongitude warpedTopLeft, warpedBottomRight;
            double mpp;
            mpp = GetMetersPerPixel(boundingBox, source.Width, source.Height);
            var inputBitmap = Image.FromFile(filename);
            var svc = new ImageWarperService();
            Bitmap warpedImageBitmap = svc.WarpImageGeoToMercator(
                inputBitmap, new LatitudeLongitude(boundingBox.Top, boundingBox.Left),
                mpp, mpp /*mpp is same for X and Y*/, out warpedTopLeft, out warpedBottomRight, errorThreshold);
            var warpedImageFilename = Path.Combine(ProductAndFolderService.PathService.TempFolderPath, string.Format("WMSRequest-{0}.png", Guid.NewGuid().ToString("N")));
            warpedImageBitmap.Save(warpedImageFilename, ImageFormat.Png);

            return warpedImageFilename;
        }

        private static void MakeTransparent(string warpedImageFilename, string transparentColorString)
        {
            var imageBitmap = new Bitmap(warpedImageFilename);
            imageBitmap.MakeTransparent(Color.FromArgb(0, ColorTranslator.FromHtml(transparentColorString)));

            imageBitmap.Save(warpedImageFilename, ImageFormat.Png);
            imageBitmap.Dispose();

        }
        private static List<LayerItemWMS> GetLayerItemFromOGCData(string layerName, DataOGCCapabilities ogcData)
        {
            //see if the layer by its whole name is present in ogcData
            //
            var layerItems = new List<LayerItemWMS>();
            if (ogcData.LayerItems != null)
            {
                foreach (LayerItem item in ogcData.LayerItems)
                {
                    if (String.Equals(item.Title, layerName))
                    {
                        layerItems.Add(item as LayerItemWMS);
                    }
                }
                //didn't find the layer by title
                //see if you can find it by the layer's name
                foreach (LayerItem item in ogcData.LayerItems)
                {
                    if (String.Equals(item.Name, layerName))
                        layerItems.Add(item as LayerItemWMS);
                }
            }

            if (layerItems.Count > 0) return layerItems;

            //layerName, apparently, could be composed of multiple layers separated by commas
            //so, see if each one of those layer names are present on the server
            string[] layernames = layerName.Split(new[] { ',' });

            foreach (string layer in layernames)
                if (ogcData.LayerItems != null)
                {
                    foreach (LayerItem item in ogcData.LayerItems)
                    {
                        if (item.Title == layer)
                            layerItems.Add(item as LayerItemWMS);
                    }
                    //didn't find the layer by title
                    //see if you can find it by the layer's name
                    foreach (LayerItem item in ogcData.LayerItems)
                    {
                        if (item.Name == layer)
                            layerItems.Add(item as LayerItemWMS);
                    }
                }
            return layerItems;
        }

        private void GetOGCCapabilities()
        {
            var ogcCapSource = new SourceOGCCapabilities
                                   {
                                       Address = new Uri(serverUrl),
                                       ServiceType = OGCServiceTypes.WMS
                                   };
            var ogcHandler = new HandlerOGCCapabilities(ogcCapSource) { Synchronous = false };
            ogcHandler.OperationDone += ogcHandler_OperationDone;
            ogcHandler.GetCapabilities();
            waitHandle.WaitOne();
            ogcData = ogcHandler.Data as DataOGCCapabilities;
        }

        void ogcHandler_OperationDone(object sender, EventArgs e)
        {
            try
            {

            }
            finally
            {
                waitHandle.Set();
            }
        }

        public double GetMetersPerPixel(IMapCoordinateBounds box, int width, int height)
        {
            var topleft = new LatitudeLongitude(box.Top, box.Left);
            var bottomRight = new LatitudeLongitude(box.Bottom, box.Right);

            double diagDistanceInPixels = Math.Sqrt(Math.Pow(width, 2) + Math.Pow(height, 2));
            double diagDistanceInMeters = Math.Sqrt(Math.Pow(topleft.Latitude - bottomRight.Latitude, 2) + Math.Pow(topleft.Longitude - bottomRight.Longitude, 2));//topleft.DistanceTo(bottomRight, MeasurementSystemAndScale.Metric);
            return (diagDistanceInMeters / diagDistanceInPixels);
        }

        public string RequestFileData(string filePath, out Response responseOut)
        {
            responseOut = GetData();
            if (responseOut.IsRequestSuccessful)
                return responseOut.ResponseFile;
            return null;

        }

        public void Cancel()
        {

        }
    }
}