﻿using System;
using System.Collections;
using System.Text;

namespace Depiction.API.OGCServices
{

    public class WMSRequestBuilder
    {
        private Hashtable clientInfo;
        protected Hashtable queryParameters;
        private UriBuilder uri;

        public WMSRequestBuilder()
        {
            initializeParams();
            clientInfo = new Hashtable();
        }

        public WMSRequestBuilder(Uri uri)
        {
            this.uri = new UriBuilder(uri.Scheme, uri.Host, uri.Port, uri.AbsolutePath);
            initializeParams();
            clientInfo = new Hashtable();
        }

        public Hashtable ClientInfo
        {
            get { return clientInfo; }
        }

        public string Service
        {
            set { SetParam("SERVICE", value); }
            get { return GetParam("SERVICE"); }
        }

        public string Version
        {
            set { SetParam("VERSION", value); }
            get { return GetParam("VERSION"); }
        }

        public string Exceptions
        {
            set { SetParam("EXCEPTIONS", value); }
            get { return GetParam("EXCEPTIONS"); }
        }

        public string Request
        {
            set { SetParam("REQUEST", value); }
            get { return GetParam("REQUEST"); }
        }

        public Uri Uri
        {
            set { uri = new UriBuilder(value.Scheme, value.Host, value.Port, value.AbsolutePath); }
            get { return new Uri(ToString()); }
        }

        private void initializeParams()
        {
            queryParameters = new Hashtable();
            queryParameters["VERSION"] = "1.1.1";
            queryParameters["SERVICE"] = "WMS";
            queryParameters["EXCEPTIONS"] = "application/vnd.ogc.se_xml";
        }

        public void SetParam(string name, string param)
        {
            if (name != null && !name.Equals(string.Empty))
            {
                queryParameters[name] = param;
            }
        }

        public string GetParam(string name)
        {
            if (name == null)
                return null;
            else
                return queryParameters[name] as string;
        }

        private string getQueryString()
        {
            // Assemble a query string from all the individual parameters.
            StringBuilder queryString = new StringBuilder();
            IDictionaryEnumerator iter = queryParameters.GetEnumerator();

            while (iter.MoveNext())
            {
                queryString.Append(iter.Key as string);
                queryString.Append("=");
                queryString.Append(iter.Value as string);
                queryString.Append("&");
            }

            return queryString.ToString();
        }

        public override string ToString()
        {
            uri.Query = getQueryString();
            return uri.Uri.ToString();
        }
    }
    public class MapRequestBuilder : WMSRequestBuilder
    {
        public MapRequestBuilder()
        {
            initializeParams();
        }

        public MapRequestBuilder(Uri getMapUri)
            : base(getMapUri)
        {
            initializeParams();
        }

        public string Layers
        {
            set { SetParam("LAYERS", value); }
            get { return GetParam("LAYERS"); }
        }
        //
        //TODO: needs to be refactored to a WCSRequestBuilder class
        //
        public string Coverage
        {
            set { SetParam("COVERAGE", value); }
            get { return GetParam("COVERAGE"); }
        }

        public string Styles
        {
            set { SetParam("STYLES", value); }
            get { return GetParam("STYLES"); }
        }

        public string Srs
        {
            set { SetParam("SRS", value); }
            get { return GetParam("SRS"); }
        }

        //
        //TODO: needs to be refactored to a WCSRequestBuilder class
        //
        public string Crs
        {
            set { SetParam("CRS", value); }
            get { return GetParam("CRS"); }
        }
        public string BoundingBox
        {
            set { SetParam("BBOX", value); }
            get { return GetParam("BBOX"); }
        }

        public int Width
        {
            set { SetParam("WIDTH", value.ToString()); }
            get { return Int32.Parse(GetParam("WIDTH")); }
        }

        public int Height
        {
            set { SetParam("HEIGHT", value.ToString()); }
            get { return Int32.Parse(GetParam("HEIGHT")); }
        }

        public string Format
        {
            set { SetParam("FORMAT", value); }
            get { return GetParam("FORMAT"); }
        }

        public bool Transparent
        {
            set { SetParam("TRANSPARENT", value ? "TRUE" : "FALSE"); }
            get { return GetParam("TRANSPARENT").Equals("TRUE") ? true : false; }
        }

        public string BackgroundColor
        {
            set { SetParam("BGCOLOR", value); }
            get { return GetParam("BGCOLOR"); }
        }

        public string Time
        {
            set { SetParam("TIME", value); }
            get { return GetParam("TIME"); }
        }

        public string Elevation
        {
            set { SetParam("ELEVATION", value); }
            get { return GetParam("ELEVATION"); }
        }

        public string Sld
        {
            set { SetParam("SLD", value); }
            get { return GetParam("SLD"); }
        }

        public string Wfs
        {
            set { SetParam("WFS", value); }
            get { return GetParam("WFS"); }
        }

        private void initializeParams()
        {
            queryParameters["REQUEST"] = "GetMap";
            queryParameters["SRS"] = "EPSG:4326";
            queryParameters["BBOX"] = "-122.5074, 47.6286, -121.4165, 48.4468";
            queryParameters["WIDTH"] = "300";
            queryParameters["HEIGHT"] = "150";
            queryParameters["TRANSPARENT"] = "TRUE";
            queryParameters["FORMAT"] = "image/gif";
        }

        // This function adds the default extents to the GetMap URI as explicit
        // parameters, rather than implicit ones. This is necessary, for example,
        // to identify URIs for cached maps. If the URI did not contain explicit
        // extents, then each URI for the same layer would appear the same even
        // if the default changed on the WMS server. Using explicit default
        // extents eliminates that condition.
        public void IncludeDefaultExtents(WMSLayer layer)
        {
            foreach (WMSLayer.ExtentType extent in layer.Extents)
            {
                if (extent.Default != null && !extent.Default.Equals(string.Empty))
                {
                    if (extent.Name.Equals("elevation") || extent.Name.Equals("Elevation") ||
                        extent.Name.Equals("ELEVATION"))
                    {
                        Elevation = extent.Default;
                    }
                    else if (extent.Name.Equals("time") || extent.Name.Equals("Time") || extent.Name.Equals("TIME"))
                    {
                        Time = extent.Default;
                    }
                    else
                    {
                        SetParam(extent.Name, extent.Default);
                    }
                }
            }
        }
    }
}
