﻿using System.IO;
using System.Net;
using System.Xml.XPath;

namespace Depiction.API.OGCServices
{
    //NO clue what this does
    public class WMSCapabilities
    {
        private XPathDocument doc; // The root of the XML document.
        private WMSServer server; // The Wms.Client.Server object associated with these capabilities.
        bool Errors = false;

        public bool HasErrors
        {
            get
            {
                return Errors;
            }
        }

        public WMSCapabilities(string capabilityUrl, WMSServer server)
        {
            // Constructor requires a path to an XML capabilities file,
            // and a reference to a Wms.Client.Server object.
            this.server = server;
            //filePath is actually an URI string
            string tempFileName = Path.Combine(DepictionAccess.PathService.FolderInTempFolderRootPath, System.Guid.NewGuid().ToString("N") + ".xml");
            using (WebClient wbClient = new WebClient())
            {
                wbClient.DownloadFile(capabilityUrl, tempFileName);
            }

            doc = new XPathDocument(tempFileName);
        }

        // The next several "Get" functions are utilities for parsing the capabilities
        // document.

        static public string ExpandPattern(string pattern)
        {
            // The 'Url' vs. 'URL' case is so prevalent that we account for it here rather
            // than in all the invoking instances.
            if ((!pattern.Equals(string.Empty)) && (pattern.IndexOf("Url") >= 0))
            {
                pattern = pattern + "|" + pattern.Replace("Url", "URL");
            }
            return pattern + "|" + pattern.ToLower() + "|" + pattern.ToUpper();
        }

        internal static string GetStringInstance(XPathNavigator node, string selectPattern)
        {
            string retVal = string.Empty;
            XPathNodeIterator iter = node.Select(ExpandPattern(selectPattern));
            if (iter.MoveNext())
            {
                retVal = iter.Current.Value;
            }
            return retVal;
        }

        internal static string GetStringInstance(XPathDocument doc, string selectPattern)
        {
            return GetStringInstance(doc.CreateNavigator(), selectPattern);
        }

        internal static string[] GetStringsInstance(XPathNavigator node, string selectPattern)
        {
            XPathNodeIterator iter = node.Select(ExpandPattern(selectPattern));
            string[] retVal = new string[iter.Count];
            while (iter.MoveNext())
            {
                retVal[iter.CurrentPosition - 1] = iter.Current.Value;
            }
            return retVal;
        }

        internal static string[] GetStringsInstance(XPathDocument doc, string selectPattern)
        {
            return GetStringsInstance(doc.CreateNavigator(), selectPattern);
        }

        internal static string GetOnlineResourceInstance(XPathNavigator node, string selectPattern)
        {
            string retVal = string.Empty;
            XPathNodeIterator iter = node.Select(ExpandPattern(selectPattern));
            if (iter.MoveNext())
            {
                retVal = iter.Current.GetAttribute("href", "http://www.w3.org/1999/xlink");
            }
            return retVal;
        }

        internal static string GetOnlineResourceInstance(XPathDocument doc, string selectPattern)
        {
            return GetOnlineResourceInstance(doc.CreateNavigator(), selectPattern);
        }

        internal static bool GetBooleanInstance(XPathNavigator node, string selectPattern)
        {
            string q = GetStringInstance(node, selectPattern);
            return q.Trim().Equals("1");
        }

        // These following functions use an XPath string to identify the elements of
        // the XML capabilities to find and return.

        public string ServiceOnlineResource
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Service/OnlineResource"); }
        }

        public string Version
        {
            get { return GetStringInstance(doc, @"/*/@Version"); }
        }

        public string UpdateSequence
        {
            get { return GetStringInstance(doc, @"/*/@updateSequence|./*/UpdateSequence"); }
        }

        public string ServiceName
        {
            get { return GetStringInstance(doc, @"/*/Service/Name"); }
        }

        public string ServiceTitle
        {
            get { return GetStringInstance(doc, @"/*/Service/Title"); }
        }

        public string ServiceAbstract
        {
            get { return GetStringInstance(doc, @"/*/Service/Abstract"); }
        }

        public string[] ServiceKeywordList
        {
            get { return GetStringsInstance(doc, @"/*/Service/KeywordList/Keyword"); }
        }

        public string ServiceFees
        {
            get { return GetStringInstance(doc, @"/*/Service/Fees"); }
        }

        public string ServiceAccessConstraints
        {
            get { return GetStringInstance(doc, @"/*/Service/AccessConstraints"); }
        }

        public string ServiceContactPerson
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactPersonPrimary/ContactPerson"); }
        }

        public string ServiceContactOrganization
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactPersonPrimary/ContactOrganization"); }
        }

        public string ServiceContactPosition
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactPosition"); }
        }

        public string ServiceContactVoiceTelephone
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactVoiceTelephone"); }
        }

        public string ServiceContactFacsimileTelephone
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactFacsimileTelephone"); }
        }

        public string ServiceContactElectronicMailAddress
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactElectronicMailAddress"); }
        }

        public string ServiceContactAddressType
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactAddress/AddressType"); }
        }

        public string ServiceContactAddress
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactAddress/Address"); }
        }

        public string ServiceContactAddressCity
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactAddress/City"); }
        }

        public string ServiceContactAddressStateOrProvince
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactAddress/StateOrProvince"); }
        }

        public string ServiceContactAddressPostCode
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactAddress/PostCode"); }
        }

        public string ServiceContactAddressCountry
        {
            get { return GetStringInstance(doc, @"/*/Service/ContactInformation/ContactAddress/Country"); }
        }

        public string GetCapabilitiesRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/GetCapabilities/DCPType/HTTP/Get/OnlineResource"); }
        }

        public string GetMapRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/GetMap/DCPType/HTTP/Get/OnlineResource"); }
        }
        public string GetCoverageRequestUri
        {
            get
            {
                string uri = GetOnlineResourceInstance(doc, @"/*/Capability/Request/GetCoverage/DCPType/HTTP/Get/OnlineResource");
                return uri;
            }
        }

        public string GetFeatureInfoRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/GetFeatureInfo/DCPType/HTTP/Get/OnlineResource"); }
        }

        public string DescribeLayerRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/DescribeLayer/DCPType/HTTP/Get/OnlineResource"); }
        }

        public string GetLegendGraphicRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/GetLegendGraphic/DCPType/HTTP/Get/OnlineResource"); }
        }

        public string GetStylesRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/GetStyles/DCPType/HTTP/Get/OnlineResource"); }
        }

        public string PutStylesRequestUri
        {
            get { return GetOnlineResourceInstance(doc, @"/*/Capability/Request/PutStyles/DCPType/HTTP/Get/OnlineResource"); }
        }

        public string[] GetCapabilitiesRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/GetCapabilities/Format"); }
        }

        public string[] GetMapRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/GetMap/Format"); }
        }

        public string[] GetFeatureInfoRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/GetFeatureInfo/Format"); }
        }

        public string[] DescribeLayerRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/DescribeLayer/Format"); }
        }

        public string[] GetLegendGraphicRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/GetLegendGraphic/Format"); }
        }

        public string[] GetStylesRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/GetStyles/Format"); }
        }

        public string[] PutStylesRequestFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Request/PutStyles/Format"); }
        }

        public string[] ExceptionFormats
        {
            get { return GetStringsInstance(doc, @"/*/Capability/Exception/Format"); }
        }

        public WMSLayer[] Layers
        {
            get
            {
                if (!Errors)
                {
                    XPathNodeIterator iter
                        = doc.CreateNavigator().Select(ExpandPattern(@"/*/Capability"));
                    if (iter.MoveNext())
                    {
                        return WMSLayer.GetLayers(iter.Current, server);
                    }
                    return new WMSLayer[0];
                }
                return new WMSLayer[0];
            }
        }

        public class UserDefinedSymbolizationType
        {
            public bool SupportSld;
            public bool UserLayer;
            public bool UserStyle;
            public bool RemoteWfs;
        }

        public UserDefinedSymbolizationType UserDefinedSymbolization
        {
            get
            {
                XPathNodeIterator iter
                    = doc.CreateNavigator().Select(ExpandPattern(@"./UserDefinedSymbolization"));
                UserDefinedSymbolizationType retVal = new UserDefinedSymbolizationType();
                if (iter.MoveNext())
                {
                    retVal.SupportSld = GetBooleanInstance(iter.Current, @"./@SupportSld|./@SupportSLD");
                    retVal.UserLayer = GetBooleanInstance(iter.Current, @"./@UserLayer");
                    retVal.UserStyle = GetBooleanInstance(iter.Current, @"./@UserStyle");
                    retVal.RemoteWfs = GetBooleanInstance(iter.Current, @"./@RemoteWfs|./@RemoteWFS");
                }
                return retVal;
            }
        }
    }
}
