﻿using System;
using System.Threading;
using Depiction.API.Interfaces.WebDataInterfaces;

namespace Depiction.API.OGCServices
{
    //This is legacy tech
    /// <summary>
    /// Retries an <see cref="IRepeatableRequest"/> until canceled by user or successful.
    /// </summary>
    public class RepeaterService
    {
        private readonly IRepeatableRequest request;
        private DateTime lastRequestTime = DateTime.MinValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="RepeaterService"/> class.
        /// </summary>
        public RepeaterService(IRepeatableRequest request, int tryCount)
        {
            this.request = request;
            TryCount = tryCount;
            RetryDelaySeconds = 10;
            if (tryCount <= 0)
                TryCount = int.MaxValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RepeaterService"/> class.
        /// </summary>
        /// <param name="request">The repeatable request.</param>
        public RepeaterService(IRepeatableRequest request)
            : this(request, 0)
        {
        }

        /// <summary>
        /// Gets or sets the retry delay seconds.
        /// </summary>
        /// <value>The delay between retrys.</value>
        public int RetryDelaySeconds { get; set; }

        /// <summary>
        /// Gets or sets the try count.
        /// </summary>
        /// <value>The number of times to try.</value>
        public int TryCount { get; set; }

        /// <summary>
        /// Executes the request, blocking until is canceled, succeeds, or exceptions out
        /// </summary>
        public void Execute()
        {
            int tries = 0;

            while (!(request.Response.IsRequestCanceled || request.Response.IsRequestSuccessful) && tries < TryCount)
            {
                if (IsCanceled)
                    return;
                DateTime curTime = DateTime.Now;
                if (RetryDelaySeconds == 0 || (curTime - lastRequestTime).Seconds > RetryDelaySeconds)
                {
                    lastRequestTime = curTime;
                    tries++;
                    ExecuteRequest();
                }
                else
                    Thread.Sleep(1000);
            }
        }

        private void ExecuteRequest()
        {
            request.Execute();
        }

        public void Cancel()
        {
            IsCanceled = true;
            if (request != null)
                request.Cancel();
        }

        private bool IsCanceled { get; set; }
    }
}
