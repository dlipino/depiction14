﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Depiction.API.ValueTypeConverters
{
    /// <summary>
    /// ugh
    /// </summary>
    public class DepictionImageResourceToSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //var converter = new DepictionIconPathTypeConverter();
            //var obj = converter.ConvertTo(value, typeof(ImageSource));
            //return obj;



            string url = value as string;
            //Uri uri;
            if (url == null) return null;
            var resource = Application.Current.Resources[url] as ImageSource;
            if (resource != null)
                return resource;
            else
            {
                return value;
            }
            //if (resource != null && resource.UriSource != null)
            //    uri = resource.UriSource;
            //else if (resource != null && (resource.StreamSource is System.IO.FileStream))
            //    sourceUri = new Uri((resource.StreamSource as System.IO.FileStream).Name);
            //else
            //    sourceUri = new Uri(sourceUrl);
            
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
