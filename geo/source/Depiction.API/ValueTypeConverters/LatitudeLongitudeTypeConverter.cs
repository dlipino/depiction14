﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Depiction.API.Service;
using Depiction.API.ValueTypes;

namespace Depiction.API.ValueTypeConverters
{
    public class LatitudeLongitudeTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string));
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
        {
            return (destType == typeof(string));
        }
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                var latLong = (LatitudeLongitude)value;
                return latLong.ToXmlSaveString();// string.Format(culture, "{0},{1}", latLong.Latitude, latLong.Longitude);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo info, object value)
        {
            var dividers = new List<char> { ',', ' ' };
            string message = "";
            if (value is string)
            {
                var valString = value as string;
                var result = ConvertStringToLatLong(valString);
                if (result == null) throw new Exception(string.Format("Cannot convert string: {0} to a Latitude Longitude", value));
                return result;
            }
            return base.ConvertFrom(context, info, value);
        }
        static public LatitudeLongitude ConvertStringToLatLong(string valString)
        {
            //This will fail for decimal values that use (commas) 1,2 == 1.2
            var dividers = new List<char> { ',', ' ' };
            string message = "";
            if (valString == null) return new LatitudeLongitude();
            valString = valString.Trim();
            if (string.IsNullOrEmpty(valString)) return new LatitudeLongitude();

            //first try latlong
            double latD;
            double longD;
            var result = LatLongParserUtility.ParseForLatLong(valString, out latD, out longD,
                                                              out message);
            if (result == true)
            {
                return new LatitudeLongitude(latD, longD);
            }
            //Was an understandable format for a lat/long string, but was not a valid world location
            if (result == false && valString.Split(' ').Length != 3)
            {
                return new LatitudeLongitude();
            }
            //Alright so it was not a valid lat long, try utm
            result = UtmParserUtility.ParseForUTMLocationToLatLong(valString, out latD, out longD,
                                                                       out message);
            if (result == true)
            {
                return new LatitudeLongitude(latD, longD);
            }

            return null;
        }
    }
}