﻿using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.APINew.HelperObjects;
using Depiction.APINew.OldValidationRules;

namespace Depiction.APINew.OldValidationRules
{
    ///<summary>
    /// A validation rule to check for the existence of elevation in this depiction.
    ///</summary>
    public class TerrainExistsValidationRule : IValidationRule
    {
        #region IValidationRule Members

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
        }

        public DepictionValidationResult Validate(object valueToValidate)
        {
            var requiredElement = "Elevation";
            var terrainExists = DepictionAccess.Depiction != null && DepictionAccess.Depiction.GetFirstElementByTag(requiredElement) != null;
            if (terrainExists)
                return new ValidationResult {IsValid = true};

            var message = string.Format("Must add \"{0}\" data before changing flood height. (see \"Elevation data\" in help).",
                                        requiredElement);
            return new DepictionValidationResult { IsValid = false, Message = message };
        }

        public ValidationRuleParameter[] Parameters
        {
            get { return new ValidationRuleParameter[0];}
        }

        #endregion
    }
}