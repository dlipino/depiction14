﻿using System.Xml.Serialization;
using Depiction.API.HelperObjects;

namespace Depiction.API.OldValidationRules
{
    /// <summary>
    /// Contract for a validation rule
    /// </summary>
    public interface IValidationRule : IXmlSerializable
    {
        /// <summary>
        /// Is this given value valid, according to this validation rule?
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        DepictionValidationResult Validate(object valueToValidate);
        bool IsRuleValid { get; }
        /// <summary>
        /// The parameters passed to this validation rule.
        /// </summary>
        /// <value>The parameters.</value>
        ValidationRuleParameter[] Parameters { get; }
    }
}