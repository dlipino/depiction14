using System;
using System.Xml;
using System.Xml.Schema;
using Depiction.APINew.HelperObjects;
using Depiction.APINew.OldValidationRules;
using Depiction.APINew.ValueTypeInterfaces;
using Depiction.Serialization;

namespace Depiction.APINew.OldValidationRules
{
    /// <summary>
    /// Validates whether a variable is within a range
    /// </summary>
    public class RangeValidationRule : IValidationRule
    {
        #region Variables

        public object max;
        public object min;

        #endregion

        #region Constructor
        
        ///<summary>
        /// Create a new instance of this class.
        ///</summary>
        public RangeValidationRule()
        {}

        /// <summary>
        /// Initializes a new instance of the <see cref="RangeValidationRule"/> class.
        /// </summary>
        /// <param name="minValue">The min value.</param>
        /// <param name="maxValue">The max value.</param>
        public RangeValidationRule(object minValue, object maxValue)
        {
            min = minValue;
            max = maxValue;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the parameters.
        /// </summary>
        /// <value>The parameters.</value>
        public ValidationRuleParameter[] Parameters
        {
            get { return new[] {new ValidationRuleParameter(min.ToString(), min.GetType()), new ValidationRuleParameter(max.ToString(), max.GetType())}; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates the specified value to validate.
        /// If one of the parameters is an IMeasurement, then all must be.
        /// </summary>
        /// <param name="valueToValidate">The value to validate.</param>
        /// <returns></returns>
        public DepictionValidationResult Validate(object valueToValidate)
        {
            var result = new DepictionValidationResult();
            result.IsValid = false;
            double minVal, maxVal, valVal;
            if (min is IMeasurement || max is IMeasurement || valueToValidate is IMeasurement)
            {
                if (!(min is IMeasurement))
                {
                    result.Message = string.Format(
                        "The minimum value {0} cannot be validated because its units have not been specified.", min);
                    return result;
                }
                if (!(max is IMeasurement))
                {
                    result.Message = string.Format(
                        "The maximum value {0} cannot be validated because its units have not been specified.", max);
                    return result;
                }
                if (!(valueToValidate is IMeasurement))
                {
                    result.Message = string.Format(
                        "The value \"{0}\" cannot be validated because its units have not been specified.", valueToValidate);
                    return result;
                }
                var useSystem = ((IMeasurement)min).DefaultSystem;
                var useScale = ((IMeasurement)min).DefaultScale;
                valVal = ((IMeasurement)valueToValidate).GetValue(useSystem,useScale);
                minVal = ((IMeasurement)min).GetValue(useSystem, useScale);
                maxVal = ((IMeasurement)max).GetValue(useSystem, useScale);
            }
            else
            {
                try
                {
                    minVal = Convert.ToDouble(min);
                }
                catch (Exception)
                {
                    result.Message = string.Format("Unable to convert minimum value \"{0}\" to numeric for validation.", min);
                    return result;
                }
                try
                {
                    maxVal = Convert.ToDouble(max);
                }
                catch (Exception)
                {
                    result.Message = string.Format("Unable to convert maximum value \"{0}\" to numeric for validation.", max);
                    return result;
                }
                try
                {
                    valVal = Convert.ToDouble(valueToValidate);
                }
                catch (Exception)
                {
                    result.Message = string.Format("Unable to convert \"{0}\" to numeric for validation.", valueToValidate);
                    return result;
                }
            }

            result.IsValid = (valVal >= minVal && valVal <= maxVal);
            result.Message = string.Format("Value must be between {0} and {1}", min, max);
            return result;
        }

        #endregion

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            var minTypeString = reader.ReadElementContentAsString("minType", ns);
            var minValue = reader.ReadElementContentAsString("minValue", ns);

            var minType = Type.GetType(minTypeString);
            min = DepictionTypeConverter.ChangeType(minValue, minType);

            var maxTypeString = reader.ReadElementContentAsString("maxType", ns);
            var maxValue = reader.ReadElementContentAsString("maxValue", ns);

            var maxType = Type.GetType(maxTypeString);
            max = DepictionTypeConverter.ChangeType(maxValue, maxType);

            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteElementString("minType", ns, min.GetType().FullName);
            writer.WriteElementString("minValue", ns, min.ToString());
            writer.WriteElementString("maxType", ns, max.GetType().FullName);
            writer.WriteElementString("maxValue", ns, max.ToString());
        }
    }
}