﻿using System;
using Depiction.API.OldValidationRules;

namespace Depiction.API.ValueTypes
{
    public class ParameterInfo
    {
        public string ParameterKey { get; private set; }
        public string ParameterName { get; set; }
        public string ParameterDescription { get; set; }
        public Type ParameterType { get; private set; }
        public string[] PossibleValues { get; set; }
        public IValidationRule[] ValidationRules { get; set; }

        public ParameterInfo(string parameterKey, Type parameterType)
        {
            ParameterKey = parameterKey;
            ParameterType = parameterType;
            ParameterDescription = "";
            ParameterName = "";
        }
    }
}
