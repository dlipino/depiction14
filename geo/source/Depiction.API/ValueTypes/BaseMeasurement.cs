using System;
using System.Globalization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionTypeInterfaces;
using Depiction.API.Properties;
using Depiction.Serialization;

namespace Depiction.API.ValueTypes
{
    /// <summary>
    /// The base class from which to create new types for Depiction element properties.
    /// Each type represents a measurement system (e.g. Imperial or Metric), and a size for
    /// the range of values, and a unit (e.g. mph or kph).
    /// </summary>
    public abstract class BaseMeasurement : IMeasurement, IFormattable
    {
        public const int MetricSmall = (int)(MeasurementSystem.Metric) + (int)(MeasurementScale.Small);
        public const int MetricNormal = (int)(MeasurementSystem.Metric) + (int)(MeasurementScale.Normal);
        public const int MetricLarge = (int)(MeasurementSystem.Metric) + (int)(MeasurementScale.Large);
        public const int ImperialSmall = (int)(MeasurementSystem.Imperial) + (int)(MeasurementScale.Small);
        public const int ImperialNormal = (int)(MeasurementSystem.Imperial) + (int)(MeasurementScale.Normal);
        public const int ImperialLarge = (int)(MeasurementSystem.Imperial) + (int)(MeasurementScale.Large);

        //For now the order of the conversion factors will be (there should be some sort of and/or operater for this
        //MS =0 (Metric = 0 Small = 0)
        //MN =1(Metric = 0 Medium = 1)
        //ML =2(Metric = 0 Large = 2)
        //IS = 3 (Imperial = 3 Small = 0)
        //IN = 4(Imperial = 3 Medium = 1)
        //IL = 5(Imperial = 3 Large = 2)
        protected abstract double[,] ConversionFactors { get; }
        protected abstract double[,] OffsetFactors { get; }
        protected MeasurementScale initialScale;
        protected MeasurementSystem initialSystem;
        protected double numericValue;

        #region IMeasurement Members

        public bool UseDefaultScale { get; set; }

        public MeasurementScale InitialScale
        {
            get { return initialScale; }
        }

        public MeasurementSystem InitialSystem
        {
            get { return initialSystem; }
        }

        public string Units
        {
            get
            {
                if (UseDefaultScale) return GetCurrentSystemDefaultScaleUnits();
                return GetUnits(Settings.Default.MeasurementSystem, Settings.Default.MeasurementScale);
            }
        }

        public double NumericValue
        {
            get
            {
                if (UseDefaultScale) return GetCurrentSystemDefaultScaleValue();
                return GetValue(Settings.Default.MeasurementSystem, Settings.Default.MeasurementScale);
            }
            set//Used in property viewer mostly, i think
            {
                if (UseDefaultScale)
                {//This might not work all the time, because it would depend in what scale the in value is
                    SetValue(value, Settings.Default.MeasurementSystem, initialScale); 
                    return;
                }
                SetValue(value, Settings.Default.MeasurementSystem, Settings.Default.MeasurementScale);
                if (value.Equals(double.NaN))//hmmm what the heck?
                    throw new Exception();

            }
        }

        public string GetCurrentSystemDefaultScaleUnits()
        {
            return GetUnits(Settings.Default.MeasurementSystem, initialScale);
        }
        public string GetInitialUnits()
        {
            return GetUnits(initialSystem, initialScale);
        }
        public string GetUnits(MeasurementSystem system, MeasurementScale scale)
        {
            return GetUnits(system, scale, false);
        }
        public abstract string GetUnits(MeasurementSystem system, MeasurementScale scale,bool singular);

        public void SetValue(double newValue, MeasurementSystem fromSystem, MeasurementScale fromScale)
        {
            int fromSystemAndScale = (int)fromSystem + (int)fromScale;
            int toSystemAndScale = (int)initialSystem + (int)initialScale;
            numericValue = newValue * ConversionFactors[fromSystemAndScale, toSystemAndScale] + OffsetFactors[fromSystemAndScale, toSystemAndScale];
        }

        public double GetValue(MeasurementSystem toSystem, MeasurementScale toScale)
        {
            int fromSystemAndScale = (int)initialSystem + (int)initialScale;
            int toSystemAndScale = (int)toSystem + (int)toScale;
            return numericValue * ConversionFactors[fromSystemAndScale, toSystemAndScale] + OffsetFactors[fromSystemAndScale, toSystemAndScale];
        }

        public double GetCurrentSystemDefaultScaleValue()
        {
            return GetValue(Settings.Default.MeasurementSystem,initialScale);
        }
        public double GetInitialValue()
        {
            return GetValue(initialSystem, initialScale);
        }
        #endregion
        protected bool ToleranceEquals(double start, double other, double tolerance)
        {
            if (double.IsNaN(start) && double.IsNaN(other)) return true;
            if (double.IsNaN(start) && !double.IsNaN(other)) return false;
            if (!double.IsNaN(start) && double.IsNaN(other)) return false;
            double difference = Math.Abs(start * tolerance);
            if (Math.Abs(start - other) <= difference) return true;
            return false;
        }
        public override bool Equals(object obj)
        {
            var other = obj as BaseMeasurement;
            if (other == null) return false;
            var targetVal = GetInitialValue(); 
            if (!ToleranceEquals(targetVal, other.GetValue(initialSystem, initialScale),targetVal*.000001))
            return false;
            if (!Equals(GetInitialUnits(),other.GetUnits(initialSystem, initialScale)))
                return false;
            //if (!other.ToString().Equals(ToString())) return false;

            return true;
            //While this might be more accurate of an equality, it is a PITA when it comes to reading from an email
            //            return (initialSystem == other.initialSystem) && (initialScale == other.initialScale) && (numericValue == other.numericValue);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        string IFormattable.ToString(string format, IFormatProvider provider)
        {
            return ToString(provider, Settings.Default.MeasurementSystem, Settings.Default.MeasurementScale);
        }

        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        public abstract IMeasurement DeepClone();


        public override string ToString()
        {
            if (UseDefaultScale)
            {
                return ToString(CultureInfo.CurrentCulture, Settings.Default.MeasurementSystem, InitialScale);
//                return ToString(CultureInfo.CurrentCulture, InitialSystem, InitialScale);
            }
            return ToString(CultureInfo.CurrentCulture, Settings.Default.MeasurementSystem, Settings.Default.MeasurementScale);
        }

        public string ToString(IFormatProvider provider, MeasurementSystem system, MeasurementScale scale)
        {
            var val = ((IMeasurement) this).GetValue(system, scale);
            var isSingle = false;
            if (ToleranceEquals(val, 1, .0005))
            {
                isSingle = true;
            }
            return string.Format(provider, string.Format(provider, "{{0:N{0}}} {{1}}", Settings.Default.Precision), 
                val, GetUnits(system, scale,isSingle));
        }

        #region Serialization
        XmlSchema IXmlSerializable.GetSchema()
        {
            throw new NotImplementedException();
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            if (reader.Name.Equals("defaultSystem"))
            {
                initialSystem = (MeasurementSystem)Enum.Parse(typeof(MeasurementSystem), reader.ReadElementContentAsString("defaultSystem", ns));
            }
            if (reader.Name.Equals("defaultScale"))
            {
                initialScale = (MeasurementScale)Enum.Parse(typeof(MeasurementScale), reader.ReadElementContentAsString("defaultScale", ns));
            }
            numericValue = reader.ReadElementContentAsDouble("value", ns);
            reader.ReadEndElement();
        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteElementString("defaultSystem", ns, initialSystem.ToString());
            writer.WriteElementString("defaultScale", ns, initialScale.ToString());
            writer.WriteElementString("value", ns, numericValue.ToString());
        }
        #endregion
    }
}