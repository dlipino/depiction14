﻿namespace Depiction.API.ValueTypes
{
    /// <summary>
    /// The result of running a behavior.
    /// </summary>
    public class BehaviorResult
    {
        /// <summary>
        /// Did the subscriber element (the element being affected) get changed by this behavior?
        /// </summary>
        public bool SubscriberHasChanged { get; set; }
    }
}