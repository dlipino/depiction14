using System;
using System.ComponentModel;
using System.Windows.Input;
using Depiction.APINew.Interfaces.MessagingInterface;
using Depiction.APINew.MVVM;

namespace Depiction.APINew.MessagingObjects
{
    public class DefaultBackgroundServiceModel :  DepictionModelBase,IDepricatedBackgroundService
    {
        public event Action<IDepricatedBackgroundService> OperationFinished;

        private DelegateCommand haltServiceCommand;

        private string backgroundServiceName = "NOne";
        private BackgroundWorker associatedThread;
        private int percentComplete = 0;

        public DefaultBackgroundServiceModel(string name,BackgroundWorker thread)
        {
            backgroundServiceName = name;
            if (thread != null)
            {
                associatedThread = thread;
                associatedThread.RunWorkerCompleted += associatedThread_RunWorkerCompleted;
                if(associatedThread.WorkerReportsProgress)
                {
                    associatedThread.ProgressChanged += associatedThread_ProgressChanged;
                }
            }
        }
        #region pass throughs
        //Completed pass through
        void associatedThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(OperationFinished != null) OperationFinished.Invoke(this);
        }


        void associatedThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            PercentageComplete = e.ProgressPercentage;
        }

        #endregion
        #region Implementation of IDepricatedBackgroundService

        public string OperationName
        {
            get { return backgroundServiceName; }
        }

        public int PercentageComplete
        {
            get
            {
                return percentComplete;
            }
            private set { percentComplete = value; NotifyModelPropertyChangedInstant("PercentageComplete"); }
        }

        public ICommand HaltServiceCommand
        {
            get { if(haltServiceCommand == null)
            {
                haltServiceCommand = new DelegateCommand(CancelOperation);
            }
                return haltServiceCommand;
            }
        }

        public void CancelOperation()
        {
            if(associatedThread != null && associatedThread.WorkerSupportsCancellation)
            {
                associatedThread.CancelAsync();
            }else
            {//For now do this, in the future all operations should have a backgroundworker thread
                if (OperationFinished != null) OperationFinished.Invoke(this);
            }
        }

        #endregion
    }
}