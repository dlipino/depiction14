﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using Depiction.API.Service;

namespace Depiction.API.TileServiceHelpers
{

    public static class RequestHelper
    {
        public static byte[] FetchImage(string filePath)
        {
            try
            {
                using (var stream = File.OpenRead(filePath))
                {
                    return ReadFully(stream);
                }
            }
            catch (IOException e)
            {
                throw new Exception("Error reading cached tile file", e);
            }
        }

        public static byte[] FetchImage(Uri uri)
        {

            if (!DepictionInternetConnectivityService.IsInternetAvailable)
            {
                return null;
            }
            var webClient = (HttpWebRequest)WebRequest.Create(uri);

            //it seems Silverlight has explicit exceptions built in for assigning user-agent and referer. 
            //I seems there is no way around this. PDD.
            //Todo: remove this overload from SL or throw exception.
            //!!!if (!String.IsNullOrEmpty(userAgent)) webClient.Headers["user-agent"] = userAgent;
            //!!!if (!String.IsNullOrEmpty(referer)) webClient.Headers["Referer"] = referer;

            //we use a waithandle to fake a synchronous call
            var waitHandle = new AutoResetEvent(false);
            IAsyncResult result = webClient.BeginGetResponse(WebClientOpenReadCompleted, waitHandle);
            
            //This trick works because the this is called on a worker thread. In SL it wont work if you call
            //it from the main thread because the main thead dispatches the worker threads and it starts waiting 
            //before it dispatches the worker thread.
            waitHandle.WaitOne();

            var response = (HttpWebResponse)webClient.EndGetResponse(result);

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception("An error occurred while fetching the tile");
            }
            if (!response.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
            {
                string message = CreateErrorMessage(response, uri.AbsoluteUri);
                throw (new Exception(message, null));
            }
            using (Stream responseStream = response.GetResponseStream())
            {
                return ReadFully(responseStream);
            }
        }

        private static void WebClientOpenReadCompleted(IAsyncResult e)
        {
            //Call Set() so that WaitOne can proceed.
            ((AutoResetEvent)e.AsyncState).Set();
        }

        private static string CreateErrorMessage(WebResponse webResponse, string uri)
        {
            string message = String.Format(
                CultureInfo.InvariantCulture,
                "Failed to retrieve tile from this uri:\n{0}\n.An image was expected but the received type was '{1}'.",
                uri,
                webResponse.ContentType
            );

            if (webResponse.ContentType.StartsWith("text", StringComparison.OrdinalIgnoreCase))
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    message += String.Format(CultureInfo.InvariantCulture,
                      "\nThis was returned:\n{0}", ReadAllText(stream));
                }
            }
            return message;
        }

        private static string ReadAllText(Stream responseStream)
        {
            using (var streamReader = new StreamReader(responseStream, true))
            {
                using (var stringWriter = new StringWriter(CultureInfo.InvariantCulture))
                {
                    stringWriter.Write(streamReader.ReadToEnd());
                    return stringWriter.ToString();
                }
            }
        }

        /// <summary>
        ///   Reads data from a stream until the end is reached. The
        ///   data is returned as a byte array. An IOException is
        ///   thrown if any of the underlying IO calls fail.
        /// </summary>
        /// <param name = "stream">The stream to read data from</param>
        private static byte[] ReadFully(Stream stream)
        {
            //thanks to: http://www.yoda.arachsys.com/csharp/readbinary.html
            var buffer = new byte[32768];
            using (var ms = new MemoryStream())
            {
                while (true)
                {
                    var read = stream.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                    {
                        return ms.ToArray();
                    }
                    ms.Write(buffer, 0, read);
                }
            }
        }

    }
}
