﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Depiction.API.TileServiceHelpers
{
    /// <summary>
    /// Adapted from BruTile
    /// </summary>
    public struct TileIndex : IComparable
    {
        private readonly int col;
        private readonly int row;
        private readonly int tileZoomLevel;

        public int Col
        {
            get { return col; }
        }

        public int Row
        {
            get { return row; }
        }

        public int TileZoomLevel
        {
            get { return tileZoomLevel; }
        }
        
        public TileIndex(int col, int row, int tileZoomLevel)
        {
            this.col = col;
            this.row = row;
            this.tileZoomLevel = tileZoomLevel;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is TileIndex))
            {
                throw new ArgumentException("object of type TileIndex was expected");
            }
            return CompareTo((TileIndex)obj);
        }

        public int CompareTo(TileIndex index)
        {
            if (col < index.col) return -1;
            if (col > index.col) return 1;
            if (row < index.row) return -1;
            if (row > index.row) return 1;
            return tileZoomLevel.CompareTo(index.tileZoomLevel);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TileIndex))
                return false;

            return Equals((TileIndex)obj);
        }

        public bool Equals(TileIndex index)
        {
            return col == index.col && row == index.row && tileZoomLevel == index.tileZoomLevel;
        }

        public override int GetHashCode()
        {
            return col ^ row ^ tileZoomLevel.GetHashCode();
        }

        public static bool operator ==(TileIndex key1, TileIndex key2)
        {
            return Equals(key1, key2);
        }

        public static bool operator !=(TileIndex key1, TileIndex key2)
        {
            return !Equals(key1, key2);
        }

        public static bool operator <(TileIndex key1, TileIndex key2)
        {
            return (key1.CompareTo(key2) < 0);
        }

        public static bool operator >(TileIndex key1, TileIndex key2)
        {
            return (key1.CompareTo(key2) > 0);
        }
    }
}
