﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Media.Imaging;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.API.ValueTypes;

namespace Depiction.API.TileServiceHelpers
{
    public class TileCacheService
    {
        protected string cacheRoot = @"c:\depictionCache";
        protected string fullCachPath = @"c:\depictionCache";

        public TileCacheService(string cachePathName)
        {
            if(DepictionAccess.PathService != null && Directory.Exists(DepictionAccess.PathService.DepictionCacheDirectory))
            {
                cacheRoot = DepictionAccess.PathService.DepictionCacheDirectory;
            }
            if(!string.IsNullOrEmpty(cachePathName))
            {
                fullCachPath = Path.Combine(cacheRoot,cachePathName);
                if (!Directory.Exists(fullCachPath))
                {
                    Directory.CreateDirectory(fullCachPath);
                }
            }
            
        }

        public string GetCacheFullStoragePath(string tileKey)
        {
            return Path.Combine(fullCachPath, tileKey);
        }

        public void SaveTileImage(byte[] image, string imageName, bool overwrite)
        {
            if (image == null)
                return;
            if (!Directory.Exists(fullCachPath))
            {
                Directory.CreateDirectory(fullCachPath);
            }
            var fullName = Path.Combine(fullCachPath, imageName);
            try
            {
                File.WriteAllBytes(fullName, image);
            }catch{}
        }

        public bool SaveTileImage(BitmapSource image, string imageName,bool overwrite)
        {
            if (!Directory.Exists(fullCachPath))
            {
                Directory.CreateDirectory(fullCachPath);
            }
            var fullName = Path.Combine(fullCachPath, imageName);
            if (File.Exists(fullName) && !overwrite) return false;
            return BitmapSaveLoadService.SaveBitmap(image, fullName);
        }

        public IMapCoordinateBounds RetrieveCachedWarpBounds(string tileKey)
        {
            string filePath = GetCacheFullStoragePath(tileKey + ".wrp");
            if (!File.Exists(filePath)) return null;
            byte[] fileStream = File.ReadAllBytes(filePath);

            using (var memoryStream = new MemoryStream(fileStream))
            {
                var binaryFormatter = new BinaryFormatter();
                return (IMapCoordinateBounds)binaryFormatter.Deserialize(memoryStream);
            }
        }
        public void CacheWarpBounds(string tileKey, ILatitudeLongitude topLeft, ILatitudeLongitude bottomRight)
        {
            if (!Directory.Exists(fullCachPath))
            {
                Directory.CreateDirectory(fullCachPath);
            }

            using (FileStream fileStream = File.Create(fullCachPath + "\\" + tileKey + ".wrp"))
            {
                var boundingBox = new MapCoordinateBounds(topLeft, bottomRight);
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(fileStream, boundingBox);
            }
        }
    }
}
