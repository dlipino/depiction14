using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Depiction.API.Interfaces.MessagingInterface;

namespace Depiction.API.Service
{
    public class BackgroundServiceManager : IBackgroundServiceManager
    {
       
        private ObservableCollection<IDepictionBackgroundService> allBackgroundServices = new ObservableCollection<IDepictionBackgroundService>();

        public ReadOnlyCollection<IDepictionBackgroundService> CurrentServices
        {
            get
            {
                return allBackgroundServices.ToList().AsReadOnly();
            }
        }
        public event NotifyCollectionChangedEventHandler BackgroundServicesModified
        {
            add { allBackgroundServices.CollectionChanged += value; }
            remove { allBackgroundServices.CollectionChanged -= value; }
        }

        public void AddBackgroundService(IDepictionBackgroundService service)
        {
            service.BackgroundServiceFinished += service_BackgroundServiceFinished;
            allBackgroundServices.Add(service);
        }

        void service_BackgroundServiceFinished(IDepictionBackgroundService service)
        {
            allBackgroundServices.Remove(service);
        }

      
        public void CancelAllServices()
        {
            foreach (var service in allBackgroundServices)
            {
                service.StopBackgroundService();
            }
        }
        /// <summary>
        /// This cleans out everything. It tries its best to stop all services, but if they don't
        /// stop, too bad. The visual for what is running gets reset here too. And disconnects the
        /// current running services from the visual
        /// </summary>
        public void ResetServiceManager()
        {
            foreach (var service in allBackgroundServices)
            {
                service.BackgroundServiceFinished -= service_BackgroundServiceFinished;
                service.StopBackgroundService();
            }
            allBackgroundServices.Clear();
        }
    }
}