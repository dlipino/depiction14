using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;

namespace Depiction.API.Service
{
    public class BitmapSaveLoadService
    {
        //Hack central.
        public static bool IsWorking;//For some reason the IsBusy would never stop, so created this instead.
        public static void BackgroundDownloadImage(BackgroundWorker imageLoader, DepictionIconPath iconPath, double imageHeight, double imageWidth)
        {

            imageLoader.DoWork += backgroundImageLoad_DoWork;
            imageLoader.RunWorkerCompleted += worker_RunWorkerCompleted;
            IsWorking = true;
            imageLoader.RunWorkerAsync(iconPath.Path);
        }
        static public ImageSource GetImageAndStoreInDepictionImageResources(string fullImagePath, out string appDictionaryKey)
        {
            ImageSource loadedImage = null;
            appDictionaryKey = null;
            try
            {

                loadedImage = LoadImageFromURLAndCreateThumb(fullImagePath, 100, 100);
                appDictionaryKey = Path.GetFileName(fullImagePath);
                var result = new KeyValuePair<string, ImageSource>(appDictionaryKey, loadedImage);

                var app = Application.Current as IDepictionApplication;
                if (app != null)
                {
                    var currentDepiction = app.CurrentDepiction;
                    if (currentDepiction != null)
                    {
                        var dict = currentDepiction.ImageResources;
                        if (!dict.Contains(result.Key))
                        {
                            dict.Add(result.Key, result.Value);
                        }
                    }
                    else
                    {
                        var appDict = app.ImageResources;
                        if (appDict != null)
                        {
                            if (!appDict.Contains(result.Key))
                            {
                                appDict.Add(result.Key, result.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                appDictionaryKey = null;
                return null;
            }
            return loadedImage;
        }

        static public bool AddImageWithKeyInAppResources(ImageSource imageSource, string imageDictionaryKey, bool replaceIfKeyExists)
        {
            if (string.IsNullOrEmpty(imageDictionaryKey))
            {
                imageDictionaryKey = Guid.NewGuid().ToString();
            }
            try
            {
                var result = new KeyValuePair<string, ImageSource>(imageDictionaryKey, imageSource);

                var app = Application.Current as IDepictionApplication;
                if (app != null)
                {
                    var currentDepiction = app.CurrentDepiction;
                    if (currentDepiction != null)
                    {
                        var dict = currentDepiction.ImageResources;

                        if (replaceIfKeyExists && !string.IsNullOrEmpty(imageDictionaryKey) && dict.Contains(imageDictionaryKey))
                        {
                            dict.Remove(imageDictionaryKey);
                        }
                        if (!dict.Contains(result.Key))
                        {
                            dict.Add(result.Key, result.Value);
                        }
                    }
                    else
                    {
                        var appDict = app.ImageResources;
                        if (appDict != null)
                        {
                            if (replaceIfKeyExists && !string.IsNullOrEmpty(imageDictionaryKey) && appDict.Contains(imageDictionaryKey))
                            {
                                appDict.Remove(imageDictionaryKey);
                            }
                            if (!appDict.Contains(result.Key))
                            {
                                appDict.Add(result.Key, result.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        //Ok this is a strange method, its initial intent was for clearing old Elevation images since we know the old image name and it should
        //never be the same as the new elevation image name.
        static public bool LoadImageStoreInDepictionImageResourcesAndRemoveOldKey(string fullImagePath, string oldAppDictionaryKey, out string appDictionaryKey)
        {
            ImageSource loadedImage = null;
            try
            {
                loadedImage = LoadImageFromURLAndCreateThumb(fullImagePath, 100, 100);
                appDictionaryKey = Path.GetFileName(fullImagePath);
                loadedImage.Freeze();
                var result = new KeyValuePair<string, ImageSource>(appDictionaryKey, loadedImage);

                var app = Application.Current as IDepictionApplication;
                if (app != null)
                {
                    var currentDepiction = app.CurrentDepiction;
                    if (currentDepiction != null)
                    {
                        var currentDepictionImageResources = currentDepiction.ImageResources;

                        if (!string.IsNullOrEmpty(oldAppDictionaryKey) && currentDepictionImageResources.Contains(oldAppDictionaryKey))
                        {
                            currentDepictionImageResources.Remove(oldAppDictionaryKey);
                        }
                        if (!currentDepictionImageResources.Contains(result.Key))
                        {
                            currentDepictionImageResources.Add(result.Key, result.Value);

                        }
                    }
                    else
                    {
                        var appImageResources = app.ImageResources;
                        if (appImageResources != null)
                        {

                            if (!string.IsNullOrEmpty(oldAppDictionaryKey) && appImageResources.Contains(oldAppDictionaryKey))
                            {
                                appImageResources.Remove(oldAppDictionaryKey);
                            }
                            if (!appImageResources.Contains(result.Key))
                            {
                                appImageResources.Add(result.Key, result.Value);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                appDictionaryKey = null;
                return false;
            }
            return true;
        }

        static public bool LoadImageAndStoreInDepictionImageResources(string fullImagePath, out string appDictionaryKey)
        {
            return LoadImageStoreInDepictionImageResourcesAndRemoveOldKey(fullImagePath, "", out appDictionaryKey);
        }
        static void backgroundImageLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            var fullPath = e.Argument.ToString();
            string imageKey;
            LoadImageAndStoreInDepictionImageResources(fullPath, out imageKey);
            IsWorking = false;
        }
        static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //            var app = Application.Current as IDepictionApplication;
            //            if (!(e.Result is KeyValuePair<string, ImageSource>)) return;
            //            var result = (KeyValuePair<string, ImageSource>)(e.Result);
            //            if (app != null)
            //            {
            //                var currentDepiction = app.CurrentDepiction;
            //                var dict = currentDepiction.ImageResources;
            //                if (!dict.Contains(result.Key))
            //                {
            //                    dict.Add(result.Key, result.Value);
            //                }
            //            }
            IsWorking = false;

        }
        //From 122, but seems to work all the time
        private static BitmapSource GetBitmapFromFile(string iconPath)
        {
            try
            {
                BitmapSource source = null;
                using (Stream stream = new FileStream(iconPath, FileMode.Open, FileAccess.Read))
                {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.StreamSource = stream;
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    source = bitmapImage;
                    //                    switch (Path.GetExtension(iconPath).ToLower())
                    //                    {
                    //                        case ".png":
                    //                            var decoder = new PngBitmapDecoder(stream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    //                            BitmapFrame frame = decoder.Frames[0];
                    ////                            frame.Freeze();
                    ////                            source = frame;
                    //                            var writable = new WriteableBitmap(frame);
                    //                            writable.Freeze();
                    //                            source = writable;
                    //                            break;
                    //                        default:
                    //
                    //                            var bitmapImage = new BitmapImage();
                    //                            bitmapImage.BeginInit();
                    //                            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    //                            bitmapImage.StreamSource = stream;
                    //                            bitmapImage.EndInit();
                    //                            bitmapImage.Freeze();
                    //                            source = bitmapImage;
                    //                            break;
                    //                    }


                }

                return source;
            }
            catch (NotSupportedException)
            {
                return null;
            }
        }

        public static BitmapSource LoadImageFromByteAndCreateThumb(Stream mem, string fullName, int imageHeight, int imageWidth)
        {
            return GetBitmapFromFile(fullName);
            #region Stuff that should work but doesn't maybe user error
            var extension = Path.GetExtension(fullName).ToLower();
            using (mem)
            {
                BitmapDecoder decoder;
                if (!string.IsNullOrEmpty(extension))
                {
                    try
                    {
                        switch (Path.GetExtension(extension).ToLower())
                        {
                            case ".gif":
                            case ".giff"://Getting a strange exception with gif when trying to create a WriteableBitmap
                                var bitmapImage = new BitmapImage();
                                bitmapImage.BeginInit();
                                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                                bitmapImage.UriSource = new Uri(fullName);
                                bitmapImage.EndInit();
                                bitmapImage.Freeze();
                                return bitmapImage;
                            //                            decoder = new GifBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.None);
                            //                            BitmapFrame gifFrame = decoder.Frames[0];
                            //                            gifFrame.Freeze();
                            //                            return gifFrame;
                            //                            break;
                            case ".tiff":
                            case ".tif":
                                decoder = new TiffBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                                break;
                            case ".png":
                                decoder = new PngBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                                break;
                            case ".bmp":
                                decoder = new BmpBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                                break;
                            case ".jpg":
                                decoder = new JpegBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                                break;
                            default:
                                decoder = BitmapDecoder.Create(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {//Kind of a random guess because we either use jpg or png
                        decoder = new PngBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    }
                }
                else
                {
                    decoder = BitmapDecoder.Create(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                }
                BitmapFrame frame = decoder.Frames[0];

                //                double scale = GetTransormScale(imageHeight, imageWidth, frame.PixelWidth, frame.PixelHeight);
                //                BitmapSource thumbnail = ScaleBitmap(frame, scale);
                // this will disconnect the stream from the image completely ...
                var writable = new WriteableBitmap(frame);
                writable.Freeze();
                return writable;
            }
            #endregion
        }
        public static BitmapSource DownLoadImageFromURI(string uriLocation, int width, int height)
        {
            return DownLoadImageFromURI(uriLocation, width, height, "");
        }
        //This will probably get the axe because it does basically the same thing as GetBitmapFromFile but more buggy.
        public static BitmapSource DownLoadImageFromURI(string uriLocation, int width, int height, string desiredExtension)
        {
            //return GetBitmapFromUri(new Uri(uriLocation));
            BitmapDecoder decoder = null;
            var uri = new Uri(uriLocation);
            var extension = desiredExtension.ToLowerInvariant();
            if (string.IsNullOrEmpty(desiredExtension))
            {
                if (Path.HasExtension(uriLocation))
                {
                    extension = Path.GetExtension(uriLocation).ToLower();
                }
                else extension = "";
            }
            switch (extension)
            {
                case ".gif":
                case ".giff":
                    decoder = new GifBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    break;
                case ".tiff":
                case ".tif":
                    decoder = new TiffBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    break;
                case ".png":
                    decoder = new PngBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    var defaultImage = decoder.Frames[0];
                    BitmapFrame frozen = (BitmapFrame)defaultImage.GetAsFrozen();
                    defaultImage = frozen;
                    defaultImage.Freeze();
                    return defaultImage;
                case ".jpg":
                case ".jpeg":
                    decoder = new JpegBitmapDecoder(uri, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    break;
                default:
                    //This actually doesn't work so well, can't read jpegs if they make it through
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.DecodePixelHeight = 200;
                    bitmapImage.DecodePixelWidth = 200;
                    bitmapImage.CacheOption = BitmapCacheOption.Default;
                    bitmapImage.UriSource = new Uri(uriLocation);
                    bitmapImage.EndInit();
                    bitmapImage.Freeze();
                    return bitmapImage;
                //Bad we don't want to do this becasue it messes up the help 
                //                    var writable = new WriteableBitmap(bitmapImage);
                //                    writable.Freeze();
                //                    return writable;
            }

            if (decoder == null) return null;


            //                double scale = GetTransormScale(imageHeight, imageWidth, frame.PixelWidth, frame.PixelHeight);
            //                BitmapSource thumbnail = ScaleBitmap(frame, scale);

            // writablebitmap would disconnect the image from all threads, but it sometimes doesn't 
            //write the
            try
            {
                var defaultImage = decoder.Frames[0];
                BitmapFrame frozen = (BitmapFrame)defaultImage.GetAsFrozen();
                defaultImage = frozen;
                defaultImage.Freeze();
                return defaultImage;
            }
            catch (Exception ex)
            {
                //DepictionExceptionHandler.HandleException("DownLoadImageFromURI error", ex, false, false);
                var defaultImage = decoder.Frames[0];
                BitmapFrame frozen = (BitmapFrame)defaultImage.GetAsFrozen();
                defaultImage = frozen;
                defaultImage.Freeze();
                return defaultImage;
            }

            //  var defaultImage = decoder.Frames[0];
            // BitmapFrame frozen = (BitmapFrame)defaultImage.GetAsFrozen();
            //defaultImage = frozen;
            //defaultImage.Freeze();

            //            return defaultImage;
        }
        public static BitmapSource LoadImageFromURLAndCreateThumb(string url, int imageHeight, int imageWidth)
        {//ImageHeight and width are not used, yet 

            if (File.Exists(url))
            {
                byte[] buffer = File.ReadAllBytes(url);
                var stream = new MemoryStream(buffer);
                return LoadImageFromByteAndCreateThumb(stream, url, imageHeight, imageWidth);
            }
            //If it is not a file, try the web
            return DownLoadImageFromURI(url, 100, 100);
        }
        public static bool HasValidImageExtension(string fileNamePath)
        {
            if (Path.HasExtension(fileNamePath))
            {
                switch (Path.GetExtension(fileNamePath).ToLower())
                {
                    case ".gif":
                    case ".giff":
                    case ".tiff":
                    case ".tif":
                    case ".png":
                    case ".jpg":
                    case ".jpeg":
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }
        public static bool SaveBitmap(BitmapSource bitmap, string fileNamePath)
        {
            var path = Path.GetDirectoryName(fileNamePath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            if (bitmap != null)
            {
                //BitmapEncoder encoder = new BmpBitmapEncoder();
                BitmapEncoder encoder = new PngBitmapEncoder();//Default save type
                if (Path.HasExtension(fileNamePath))
                {
                    switch (Path.GetExtension(fileNamePath).ToLower())
                    {
                        case ".gif":
                        case ".giff":
                            encoder = new GifBitmapEncoder();
                            break;
                        case ".tiff":
                        case ".tif":
                            encoder = new TiffBitmapEncoder();
                            break;
                        case ".png":
                            encoder = new PngBitmapEncoder();
                            break;
                        case ".jpg":
                        case ".jpeg":
                            encoder = new JpegBitmapEncoder();
                            ((JpegBitmapEncoder)encoder).QualityLevel = 80;
                            break;
                        case ".bmp":
                            encoder = new BmpBitmapEncoder();
                            break;
                        default:
                            encoder = new PngBitmapEncoder();
                            break;
                    }
                }
                try
                {
                    using (var stream = new FileStream(fileNamePath, FileMode.Create))
                    {
                        var frame = bitmap as BitmapFrame;

                        if (frame == null)
                        {
                            encoder.Frames.Add(BitmapFrame.Create(bitmap));
                        }
                        else
                        {
                            encoder.Frames.Add(frame);
                        }
                        encoder.Save(stream);
                    }
                }
                catch
                {
                    return false;
                }

                return true;
            }

            return false;
        }
    }
}