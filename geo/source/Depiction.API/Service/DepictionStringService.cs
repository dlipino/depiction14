using System;
using System.Linq;

namespace Depiction.API.Service
{
    public class DepictionStringService
    {
//#if PREP
//        public const string AutoDetectElementString = "DepictionPrep.Plugin.AutoDetect";
//#else
        public const string AutoDetectElementString = "Depiction.Plugin.AutoDetect";
//#endif
        //There is a better version of this floating around (better as it is more complete and 
        //uses RegEx
        static public string ConvertStringToValidInternalName(string inString)
        {
            var spaceLess = inString.Replace(" ", String.Empty);
            var periodSplit = spaceLess.Split('.');

            var internalName = String.Empty;
            foreach(var split in periodSplit)
            {
                var cleanSplit = new string(split.ToCharArray().Where(Char.IsLetterOrDigit).ToArray());
                internalName += (cleanSplit + ".");
            }
            internalName = internalName.Trim('.');
            
            return internalName;
        }
    }
}