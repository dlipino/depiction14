﻿using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.Interfaces
{
    /// <summary>
    /// Interface for InteractionRule.
    /// </summary>
    public interface IInteractionRule:IDeepCloneable<IInteractionRule>
    {
        string UniqueTempSessionId { get; }
        bool IsDefaultRule { get; }
        /// <summary>
        /// The unique name of the rule.
        /// </summary>
        /// <value>The name.</value>
        string Name { get; set; }

        /// <summary>
        /// Description of the rule.
        /// </summary>
        /// <value>The name.</value>
        string Description { get; set; }

        /// <summary>
        /// The author of the rule.
        /// </summary>
        /// <value>The name.</value>
        string Author { get; set; }
        bool IsDisabled { get; set; }
        /// <summary>
        /// Gets or sets the type of the publisher, e.g. Depiction.Plugin.Person.
        /// </summary>
        /// <value>The type of the publisher.</value>
        string Publisher { get; set; }
        HashSet<string> PublisherTriggerPropertyNames { get; }

        /// <summary>
        /// Gets the subscriber types.
        /// </summary>
        /// <value>The subscriber types.</value>
        List<string> Subscribers { get; }
        HashSet<string> SubscriberTriggerPropertyNames { get; }

        /// <summary>
        /// Gets the behaviors.
        /// </summary>
        /// <value>The behaviors.</value>
        Dictionary<string, IElementFileParameter[]> Behaviors { get; }

        /// <summary>
        /// Gets the conditions.
        /// </summary>
        /// <value>The conditions.</value>
        Dictionary<string, HashSet<string>> Conditions { get; }

        IInteractionRule MakeFunctionalCopyOfInteractionRule();

        /// <summary>
        /// Executes the rule.
        /// </summary>
        /// <param name="publisher">The publisher.</param>
        /// <param name="subscriber">The subscriber.</param>
        void ExecuteRule(IDepictionElement publisher, IDepictionElement subscriber);

        /// <summary>
        /// Compares the key of an interaction with another interaction.
        /// </summary>
        /// <param name="other">Second interaction whose key is being compared.</param>
        bool KeyEquals(IInteractionRule other);

        /// <summary>
        /// Compares the functional value of an interaction with another interaction.
        /// </summary>
        /// <param name="other">Second interaction whose value is being compared.</param>
        bool FunctionallyEquals(IInteractionRule other);

        bool RuleEquals(IInteractionRule other);
    }
}
