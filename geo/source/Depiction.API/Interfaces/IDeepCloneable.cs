﻿namespace Depiction.API.Interfaces
{
    public interface IDeepCloneable<T> : IDeepCloneable
    {
        new T DeepClone();
    }
    public interface IDeepCloneable
    {
        object DeepClone();
    }
}
