﻿using System.Collections.Generic;
using Depiction.API.ValueTypes;

namespace Depiction.API.Interfaces
{
    public interface IRouteDirectionsService
    {
        string GetRouteDirections(IList<IList<RoadSegment>> list, IList<double> completeRouteTimes);
    }
}
