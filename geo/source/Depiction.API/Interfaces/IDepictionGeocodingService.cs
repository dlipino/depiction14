using System.Collections.Generic;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces
{
    public interface IDepictionGeocodingService
    {
        //Pretty sure this is not used, not sure when it would be used
        List<string> StartLocationCacheKeys { get; }

        IDepictionGeocoder[] FindAGeocodersForRegion(string region);
        
        void AddLocationPairToStartCache(string location, ILatitudeLongitude position);
        void AddLocationPairToGeneralCache(string location, ILatitudeLongitude position);

        void SaveGeneralGeoCodeCache();
        void LoadGeneralGeoCodeCache();

        GeocodeResults GeoCodeRawStringAddress(string rawAddress, IDepictionGeocoder[] geocodersToUse,bool useWebForGeocode);
        GeocodeResults GeoCodeRawStringAddress(string rawAddress, IDepictionGeocoder[] geocodersToUse);//Primary test usage, but could be used to speed up geocoding lots of thigns
        GeocodeResults GeoCodeRawStringAddress(string rawAddress);
        GeocodeResults GeoCodeRawStringAddress(string streetAddress, string city, string state, string zipCode, string country, IDepictionGeocoder[] geocodersToUse,bool useWebForGeocode);
        GeocodeResults GeoCodeRawStringAddress(string streetAddress, string city, string state, string zipCode, string country, IDepictionGeocoder[] geocodersToUse);
        GeocodeResults GeoCodeRawStringAddress(string streetAddress, string city, string state, string zipCode, string country);
    }
    public class GeocodeResults
    {
        /// <summary>
        /// The position if the geocoding succeeded.
        /// </summary>
        public ILatitudeLongitude Position { get; private set; }

        /// <summary>
        /// Whether this result is accurate (i.e. based on street level accuracy rather than state centroid accuracy)
        /// </summary>
        public bool Accurate { get; set; }

        /// <summary>
        /// An error message explaining any problems with the geocoding.
        /// </summary>
        public string ErrorMessage { get; private set; }
        /// <summary>
        /// Was this geocoding request successful?
        /// </summary>
        public bool IsValid { get
        {
            if (Position == null) return false;
            return Position.IsValid;
        } }
        public bool GeocodedByWeb { get; set; }

        /// <summary>
        /// Create an object with the position, for a successful geocoding.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="accurate"> </param>
        public GeocodeResults(ILatitudeLongitude position, bool accurate)
        {
            Position = position;
            Accurate = accurate;
            GeocodedByWeb = false;
        }
        /// <summary>
        /// Create an object with an error message, to report a failed geocoding.
        /// </summary>
        /// <param name="message"></param>
        public GeocodeResults(string message)
        {
            ErrorMessage = message;
            Position = null;
        }
    }
}