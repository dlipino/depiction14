﻿using System.Collections.Generic;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;

namespace Depiction.API.Interfaces
{
    public interface IAddinRepository
    {
        List<object> GetAllAddinMetadata();
        List<object> Get3rdPartyAddinMetadata();
        List<object> GetDefaultAddinMetadata();
        Dictionary<IDepictionDefaultImporterMetadata, IDepictionDefaultImporter> GetDefaultImporters();
        Dictionary<IDepictionNonDefaultImporterMetadata, IDepictionNonDefaultImporter>  GetNonDefaultImporters();
        Dictionary<IDepictionExporterMetadata, IDepictionElementExporter> GetExporters();
        Dictionary<IBehaviorMetaData, BaseBehavior> GetBehaviors();
        Dictionary<IConditionMetaData, ICondition> GetConditions();
    }
}
