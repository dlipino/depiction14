using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.Interfaces.DepictionStoryInterfaces
{
    public interface IElementRepository
    {
        event Action TagsChanged;
        event Action<IDepictionElement> ElementGeoLocationUpdated;
        event NotifyCollectionChangedEventHandler ElementListChange;

        List<IDepictionElement> GetElementsFromIds(IEnumerable<string> elementIds);

        ReadOnlyCollection<IDepictionElement> AllElements { get; }
        ReadOnlyCollection<IDepictionElement> ElementsGeoLocated { get; }
        ReadOnlyCollection<IDepictionElement> ElementsUngeoLocated { get; }

        List<string> AddOrUpdateRepositoryFromPrototypes(IEnumerable<IElementPrototype> elementPrototypes, out List<IDepictionElement> createdElements, bool markAdUpdated);
        void AddOrUpdateRepositoryFromPrototypes(IEnumerable<IElementPrototype> elementPrototypes, bool markAdUpdated,
            out List<IDepictionElement> createdElements, out List<IDepictionElement> updatedElements);
        bool AddElements(List<IDepictionElement> elements);

        string AddOrUpdateRepositoryFromPrototype(IElementPrototype elementPrototype, out IDepictionElement createdElement,bool markAsUpdated);
        bool AddElement(IDepictionElement element);

        bool RemoveElement(IDepictionElement element);
        List<IDepictionElement> DeleteElementsWithIdsAndReturnElements(List<string> elementIds);

        void TagElement(IDepictionElement element, string tag);
        void AddTagsToElement(IDepictionElement element, List<string> tags);
        void UntagElement(IDepictionElement element, string tag);
        void RemoveTagsFromElement(IDepictionElement element, List<string> tags);
        IEnumerable<IDepictionElement> GetElementsByTag(string tag);
        IDepictionElement GetFirstElementByTag(string tag);
        
    }
}