using System;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces.DepictionStoryInterfaces
{
    public interface IDepictionGeographicInfo
    {
        event Action DepictionSpecificsChange;
        IMapCoordinateBounds DepictionRegionBounds { get; set; }
        IMapCoordinateBounds DepictionMapWindow { get; set; }
        ILatitudeLongitude DepictionStartLocation { get; set; }
    }
}