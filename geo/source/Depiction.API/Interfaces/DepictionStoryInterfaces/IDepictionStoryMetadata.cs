using System;

namespace Depiction.API.Interfaces.DepictionStoryInterfaces
{
    public interface IDepictionStoryMetadata
    {
        event Action DepictionStoryMetadataChange;
        string Title { get; set; }
        string Description { get; set; }
        string Author { get; set; }
    }
}