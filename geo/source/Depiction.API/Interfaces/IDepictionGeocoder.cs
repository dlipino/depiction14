using System.Collections.Generic;

namespace Depiction.API.Interfaces
{
    public interface IDepictionGeocoder
    {
        bool StoreResultsInDepiction { get; }
        ///<summary>
        /// The name of this geocoder as shown in the uesr interface.
        ///</summary>
        string GeocoderName { get; }
        /// <summary>
        /// Does this geocoder understand how to parse addresses?
        /// </summary>
        bool IsAddressGeocoder { get; }
        /// <summary>
        /// 
        /// Does this geocoder understand how to parse latitude,longitude values?
        /// </summary>
        bool IsLatLongGeocoder { get; }

        /// <summary>
        /// Specify the parameters to append to the geocoding request.
        /// </summary>
        /// <param name="parameters"></param>
        void ReceiveParameters(Dictionary<string, string> parameters);
        ///<summary>
        /// Return the latitude,longitude for this address.
        /// The return value is enclosed in a results object that can be queried
        /// to learn more about whether the address was found, and if not why.
        ///</summary>
        ///<param name="addressString"></param>
        ///<returns></returns>
        GeocodeResults GeocodeAddress(string addressString);

        GeocodeResults GeocodeAddress(string addressString, bool isLatLong);
        ///
        /// 
        GeocodeResults GeocodeAddress(string street, string city, string state, string zipcode, string country);

    }


}