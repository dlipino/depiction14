﻿namespace Depiction.API.Interfaces
{
    /// <summary>
    /// Interface for ElementFileParameter.
    /// </summary>
    public interface IElementFileParameter
    {
        /// <summary>
        /// Gets or sets the sim object query.
        /// </summary>
        /// <value>The sim object query.</value>
        string ElementQuery { get; set; }

        /// <summary>
        /// Gets or sets the type of the query.
        /// </summary>
        /// <value>The type of the query.</value>
        ElementFileParameterType Type { get; set; }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        bool Equals(object obj);

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        int GetHashCode();
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ElementFileParameterType
    {
        /// <summary>
        /// 
        /// </summary>
        Subscriber,
        /// <summary>
        /// 
        /// </summary>
        Publisher,
        /// <summary>
        /// 
        /// </summary>
        Value
    }
}
