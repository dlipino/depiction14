﻿using Depiction.API.OldInterfaces;
using GeoAPI.Geometries;

namespace Depiction.API.Interfaces.GeoTypeInterfaces
{
    public interface IDepictionGeometry : IDepictionSpatialData
    {
        IGeometry Geometry { get; }
        ILatitudeLongitude Centroid { get; }
        IDepictionGeometry TranslateGeometry(ILatitudeLongitude translateLatLong);
    }
}