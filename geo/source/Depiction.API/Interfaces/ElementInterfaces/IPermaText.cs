﻿using System;
using System.Xml.Serialization;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IPermaText : IXmlSerializable, IDeepCloneable<IPermaText>
    {
        bool IsDefault { get; } //Used to save space while saving
        double PermaTextX { get; set; }
        double PermaTextY { get; set; }
        double PixelWidth { get; set; }
        double PixelHeight { get; set; }
        double MinPixelHeight { get; }
        double MinPixelWidth { get; }
        bool IsEnhancedPermaText { get; set; }
    }
}