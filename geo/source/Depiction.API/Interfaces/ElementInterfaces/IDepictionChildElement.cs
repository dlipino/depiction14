﻿using Depiction.APINew.Interfaces.ElementInterfaces;

namespace Depiction.APINew.Interfaces.ElementInterfaces
{
    public interface IDepictionChildElement : IDepictionElementBase
    {
        IDepictionElement Parent { get; }
    }
}