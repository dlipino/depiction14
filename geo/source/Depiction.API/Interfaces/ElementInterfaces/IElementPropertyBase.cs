using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Depiction.API.HelperObjects;
using Depiction.API.OldValidationRules;
using Depiction.Serialization;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IElementPropertyBase : IXmlSerializable, INotifyPropertyChanged
    {
        #region Attributes
        string InternalName { get; }

        string DisplayName { get; set; }
        bool VisibleToUser { get; set; }
        bool Editable { get; set; }
        bool Deletable { get; set; }
        #endregion

        //Not sure where these should go
        IValidationRule[] ValidationRules { get; }

        SerializableDictionary<string, string[]> PostSetActions { get; }
        #region Value related

        object Value { get; }
        object RestoreValue { set; get; }
        Type ValueType { get; }
        bool CanCloneValue { get; }

        object GetValueClone();

        DepictionValidationResult SetValueAndUpdateType(object newValue, IDepictionElement elementForPostSetActions);
        DepictionValidationResult SetPropertyValue(object newValue);
        DepictionValidationResult SetPropertyValue(object newValue, IDepictionElement elementForPostSetActions);

        #endregion
        //Not sure if this is used with the 1.4 changes
        void RefreshValue();

    }
}