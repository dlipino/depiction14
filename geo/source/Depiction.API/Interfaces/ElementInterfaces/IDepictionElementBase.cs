﻿using System.Collections.Generic;
using System.Xml.Serialization;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IDepictionElementBase : IElementPropertyHolder,  IBaseDepictionMapType, IXmlSerializable
    {
        string DefinitionGroup{ get; set; }
        bool UsePermaText { get; set; }

        IDepictionElementWaypoint[] Waypoints { get; }
        IZoneOfInfluence ZoneOfInfluence { get; }
        IPermaText PermaText { get; }
        IDepictionImageMetadata ImageMetadata { get; }
        //This needs to get removed, it should be a readonly list and then a method to add a tag
        HashSet<string> Tags { get; }

        void SetInitialPositionAndZOI(ILatitudeLongitude initialPosition, IZoneOfInfluence initialZOI);
        void ReplaceWaypointsWithoutNotification(IDepictionElementWaypoint[] newWayPoints);
        void SetImageMetadataWithoutNotification(IDepictionImageMetadata imageMetadata);

    }
}