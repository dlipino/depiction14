﻿using System;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    /// <summary>
    /// An element property.
    /// </summary>
    public interface IElementProperty : IElementPropertyBase, IDeepCloneable<IElementProperty>
    {
        #region attributes
        bool IsHoverText { get; set; }
        #endregion

        int Rank { get; set; }
       
        DateTime LastModified { get; }

        PropertySource PropertySource { get; set; }
        bool SaveProperty { get; set; }//Thought that not all properties needed to be saved, but now not so sure.
    }
}