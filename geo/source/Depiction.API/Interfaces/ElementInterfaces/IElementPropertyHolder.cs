﻿using System.Collections.Generic;
using System.ComponentModel;
using Depiction.Serialization;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IElementPropertyHolder : INotifyPropertyChanged
    {
        bool UseEnhancedPermaText { get; set; }
        bool UsePropertyNameInHoverText { get; set; }
        IElementProperty[] OrderedCustomProperties { get; }//Or a readonlyList? or i enumerable?
        List<IElementProperty> GetPropertyListClone();

        bool RemovePropertyIfNameAndTypeMatch(IElementProperty elementPropertyToRemove);
        bool RemovePropertyWithInternalName(string name, bool alsoMatchDisplayName);

        bool GetPropertyValue<TPt>(string internalName, out TPt value);
        bool? SetPropertyValue<TPt>(string internalName, TPt value, bool notifyChange);
        bool? SetPropertyValue<TPt>(string internalName, TPt value);


        bool HasPropertyByInternalOrDisplayName(string name);
        bool HasPropertyByInternalName(string propertyName);
        IElementProperty GetPropertyByInternalName(string propertyName);
        IElementProperty GetPropertyByInternalName(string internamName, bool tryDisplayName);

        string GetToolTipText(string separator, bool usePropName, bool useSimple);
        
        int NumberOfPropertiesWithDisplayName(string propertyDisplayName);
        

        #region Actions that can come from Element XML definitions

        SerializableDictionary<string, string[]> ClickActions { get; set; }
        SerializableDictionary<string, string[]> CreateActions { get; set; }
        SerializableDictionary<string, string[]> DeleteActions { get; set; }
        SerializableDictionary<string, string[]> GenerateZoiActions { get; set; }

        #endregion

        #region IProperties

        IElementProperty AddPropertyIfItDoesNotExist(IElementProperty elementPropertyToAdd, bool useAllNames, bool notifyChangeListeners);

        bool? AddPropertyOrReplaceValueNotAttributes(IElementProperty elementPropertyToAdd, bool useAllNames, bool notifyChangeListeners);
        bool? AddPropertyOrReplaceValueNotAttributes(IElementProperty elementPropertyToAdd, bool notifyChangeListeners);
        bool? AddPropertyOrReplaceValueNotAttributes(IElementProperty elementPropertyToAdd);
        //This method cannot replace values for ElementPropertyData :(
        bool? AddPropertyOrReplaceValueAndAttributes(IElementProperty elementPropertyToAdd, bool useAllNames, bool notifyChangeListeners);
        bool? AddPropertyOrReplaceValueAndAttributes(IElementProperty elementPropertyToAdd, bool notifyChangeListeners);
        bool? AddPropertyOrReplaceValueAndAttributes(IElementProperty elementPropertyToAdd);

       
        

        #endregion
    }
}