using System.ComponentModel;
using System.Xml.Serialization;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IDepictionElementWaypoint : IXmlSerializable, INotifyPropertyChanged, IDeepCloneable<IDepictionElementWaypoint>,IDeepCloneable
    {
        string Key { get; }
        string Name { get; }
        bool IsDeletable { get; }
        double IconSize { get; set; }
        ILatitudeLongitude Location { get; set; }
        DepictionIconPath IconPath { get; }
        void UpdateLocationWithoutChangeNotification(ILatitudeLongitude newLocation);
    }
}