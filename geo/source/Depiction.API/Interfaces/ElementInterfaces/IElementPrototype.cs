﻿namespace Depiction.API.Interfaces.ElementInterfaces
{
    //To use IElementPropertyData or just normal IElementProperty?
    public interface IElementPrototype : IDepictionElementBase, IDeepCloneable<IElementPrototype>
    {
        string ElementUserID { get; }
        bool IsRawPrototype { get;  }
        bool IsFileAddable { get; }
        bool IsMouseAddable { get; }
        string PrototypeOrigin { get; set; }
        string DMLFileName { get; set; }
        void UpdateTypeIfRaw(string type);
    }
}