﻿using System;
using System.Xml.Serialization;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IRevealerProperty : IXmlSerializable
    {
        object Value { get; }
        string PropertyName { get; set; }
        Type PropertyType { get; }
        
        bool SetValue(object value,bool overwriteType);
    }
}
