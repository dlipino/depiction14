﻿using Depiction.API.HelperObjects;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IBaseDepictionMapType
    {
        DepictionIconPath IconPath { get; }
        string ElementType { get; }
        string TypeDisplayName { get; }
    }
}