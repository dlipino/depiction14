﻿using System.Collections.Generic;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.Interfaces.ElementInterfaces
{
    public interface IDepictionElement : IDepictionElementBase 
    {
        string ElementKey { get; }
        bool ElementUpdated { get; set; }
        void SetZOIGeometry(IDepictionGeometry geometry);
        void SetZOIGeometryAndUpdatePosition(IDepictionGeometry geometry);
        void SetImageMetadata(IDepictionImageMetadata newImageMetadata);
        void UpdateImageMetadata(IMapCoordinateBounds bounds, double rotation);
        void UpdateToolTip();

        #region Waypoints

        List<string> GetAllWaypointKeys();
        IDepictionElementWaypoint GetWaypointWithName(string name);
        bool AddWaypoint(IDepictionElementWaypoint child);

        bool RemoveWaypoint(IDepictionElementWaypoint child);
        bool InsertWaypointBeforeLast(IDepictionElementWaypoint child);
        bool InsertWaypoint(int index,IDepictionElementWaypoint child);
        #endregion

        #region ElementProperties connected to IProperties
        //Althougt, it might make more sense create these in the viewmodel...
        ILatitudeLongitude Position { get; set; }
        string ElementUserID { get; }
        bool IsGeolocated { get; }

        /// <summary>
        /// Gets or sets the time tick.
        /// The number of times a rule has been fired on this object for one pass
        /// over the graph.  Used by InteractionGraph.
        /// </summary>
        int NumberIncomingEdgesFired { get; set; }
        void Restore(bool notifyPropertyChange);

        #endregion
    }
}