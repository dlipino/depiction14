﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Depiction.API.Interfaces.WebDataInterfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRepeatableRequest
    {
        /// <summary>
        /// Gets the requestor.
        /// </summary>
        /// <value>The requestor.</value>
        string Requestor { get; }

        Response Response { get; }

        /// <summary>
        /// Executes this instance.
        /// </summary>
        void Execute();

        string LastErrorMessage { get; }

        /// <summary>
        /// Cancel this request.
        /// </summary>
        void Cancel();
    }
}
