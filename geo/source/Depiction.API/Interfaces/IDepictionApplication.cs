using Depiction.API.HelperObjects;

namespace Depiction.API.Interfaces
{
    public interface IDepictionApplication
    {
        IDepictionStory CurrentDepiction { get; }
        RasterImageResourceDictionary ImageResources { get; }

        IAddinRepository Repository { get;}

    }
}