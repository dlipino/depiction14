﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Depiction.API.Interfaces
{

    /// <summary>
    /// A repository of InteractionRules
    /// </summary>
    public interface IInteractionRuleRepository
    {
        bool DoesRuleWithElementTypeAndTriggerNameExist(string elementType, string triggerProperty);
        /// <summary>
        /// Adds the interaction rule and runs interactions.
        /// </summary>
        /// <param name="rule">The rule.</param>
        bool AddInteractionRule(IInteractionRule rule);

        /// <summary>
        /// Adds the interaction rule and if runInteractions is true, runs interactions.
        /// </summary>
        /// <param name="rule"></param>
        /// <param name="runInteractions"></param>
        /// <returns></returns>
        bool AddInteractionRule(IInteractionRule rule, bool runInteractions);

        /// <summary>
        /// Removes an interaction rule
        /// </summary>
        /// <param name="rule">The rule to remove</param>
        void RemoveInteractionRule(IInteractionRule rule);

        void LoadRules(IEnumerable<IInteractionRule> interactionRulesToAdd);

        /// <summary>
        /// Gets all interaction rules for which the specified element type is either the 
        /// publisher or a subscriber.
        /// </summary>
        /// <param name="elementType">Type of element to search for.</param>
        /// <returns></returns>
        IInteractionRule[] GetInteractionRules(string elementType);

        /// <summary>
        /// Gets the interaction rules.
        /// </summary>
        /// <returns></returns>
        ReadOnlyCollection<IInteractionRule> InteractionRules { get; }

        /// <summary>
        /// Gets the interaction rules count.
        /// </summary>
        /// <returns></returns>
        int Count { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="elementType"></param>
        void CopyInteractionRules(string type, string elementType);

        void Clear();



    }
}
