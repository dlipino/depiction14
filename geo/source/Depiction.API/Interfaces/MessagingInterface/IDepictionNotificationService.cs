using System;
using System.Windows;

namespace Depiction.API.Interfaces.MessagingInterface
{
    public interface IDepictionNotificationService
    {
        event Action MessageAdded;
        bool DisplayMessage(IDepictionMessage message);
        bool DisplayMessageString(string simpleMessage);
        bool DisplayMessageString(string simpleMessage, int timeSeconds);
        void RemoveMessage(IDepictionMessage message);

        void SendNotificationDialog(string message);
        void SendNotificationDialog(string message, string title);
        void SendNotificationDialog(Window owner, string message, string title);
        MessageBoxResult DisplayInquiryDialog(Window owner, string message, string title, MessageBoxButton buttonOptions);
        MessageBoxResult DisplayInquiryDialog(string message, string title, MessageBoxButton buttonOptions);
    }
    public enum DepictionMessageType
    {
        Error,
        Warning,
        Information
    }
}