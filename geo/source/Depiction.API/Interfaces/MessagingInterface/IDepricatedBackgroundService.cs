using System;
using System.ComponentModel;
using System.Windows.Input;

namespace Depiction.APINew.Interfaces.MessagingInterface
{
    public interface IDepricatedBackgroundService : INotifyPropertyChanged
    {
        event Action<IDepricatedBackgroundService> OperationFinished;
        string OperationName { get; }
        int PercentageComplete { get; }
        ICommand HaltServiceCommand { get; }
        void CancelOperation();
    }
}