using System;

namespace Depiction.API.Interfaces.MessagingInterface
{
    public interface IDepictionBackgroundService
    {
        event Action<IDepictionBackgroundService> BackgroundServiceFinished;
        event Action<string> BackgroundServiceStatusReportUpdate;

        Guid ServiceID { get; }
        string ServiceName { get; }
        string ServiceStatusReport { get; }
        bool ServiceStopRequested { get; }
        bool IsServiceRefreshable { get; }

        void StartBackgroundService(object arg);
        void RefreshResult();
        void StopBackgroundService();
        void UpdateStatusReport(string update);
      
    }
}