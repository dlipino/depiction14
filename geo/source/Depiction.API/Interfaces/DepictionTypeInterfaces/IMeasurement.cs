using System;
using System.Xml.Serialization;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.API.Interfaces.DepictionTypeInterfaces
{
    /// <summary>
    /// Provides a system for creating and converting measurements between various measurement systems.
    /// </summary>
    public interface IMeasurement : IXmlSerializable,IDeepCloneable<IMeasurement>,IDeepCloneable
    {
        bool UseDefaultScale { get; set; }
        /// <summary>
        /// Returns a double value of the measurement in the user's current measurement system.
        /// </summary>
        double NumericValue { get;}// set; }
        
        /// <summary>
        /// Returns the units of the measurement in the user's current measurement system.
        /// </summary>
        string Units { get; }

        /// <summary>
        /// Returns the system currently used to represent this measurement.
        /// </summary>
        MeasurementSystem InitialSystem { get; }
        MeasurementScale InitialScale { get; }
        /// <summary>
        /// Returns the units of the measurement using its internal system.
        /// </summary>
        /// <returns></returns>
        string GetCurrentSystemDefaultScaleUnits();

        /// <summary>
        /// Returns the units of the measurement in the provided measurement systemAndScale.
        /// </summary>
        /// <returns></returns>
        string GetUnits(MeasurementSystem system, MeasurementScale scale);

        /// <summary>
        /// Sets the value of the measurement to the given value using the given systemAndScale.
        /// </summary>
        /// <param name="newValue"></param>
        /// <param name="system"></param>
        /// <param name="scale"></param>
        void SetValue(double newValue, MeasurementSystem system, MeasurementScale scale);

        /// <summary>
        /// Gets the value of this measurement in the specified measurement systemAndScale.
        /// </summary>
        /// <returns></returns>
        double GetValue(MeasurementSystem system, MeasurementScale scale);

        /// <summary>
        /// Gets the value of this measurement using its default system.
        /// </summary>
        /// <returns></returns>
        double GetCurrentSystemDefaultScaleValue();

        /// <summary>
        /// Converts this measurement to a string format
        /// </summary>
        /// <param name="culture"></param>
        /// <param name="system"></param>
        /// <param name="scale"></param>
        /// <returns></returns>
        string ToString(IFormatProvider culture, MeasurementSystem system, MeasurementScale scale);
    }
}