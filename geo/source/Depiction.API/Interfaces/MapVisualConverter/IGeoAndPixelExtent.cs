using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
//TODO move this to viewmodel or at least move the part related to pixelwidth 
//GRRRR don't really like having this in core model since the core model never really nows what is going on with the pixel

namespace Depiction.API.Interfaces.MapVisualConverter
{
    /// <summary>
    /// Contract for a Geographic extent.
    /// </summary>
    public interface IGeoAndPixelExtent
    {
        /// <summary>
        /// Gets the north west.
        /// </summary>
        /// <value>The north west.</value>
        ILatitudeLongitude NorthWest { get;} 
        /// <summary>
        /// Gets the south west.
        /// </summary>
        /// <value>The south west.</value>
        ILatitudeLongitude SouthWest { get;}
        /// <summary>
        /// Gets the south east.
        /// </summary>
        /// <value>The south east.</value>
        ILatitudeLongitude SouthEast { get;}
        /// <summary>
        /// Gets the north east.
        /// </summary>
        /// <value>The north east.</value>
        ILatitudeLongitude NorthEast { get;}
        /// <summary>
        /// Gets the center.
        /// </summary>
        /// <value>The center.</value>
        ILatitudeLongitude Center { get;}
        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <value>The width.</value>
        double Width { get; }
        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <value>The height.</value>
        double Height { get; }

        /// <summary>
        /// Gets or sets the width in pixels.
        /// </summary>
        /// <value>The width in pixels.</value>
        double WidthInPixels { get; set; }
        /// <summary>
        /// Gets or sets the height in pixels.
        /// </summary>
        /// <value>The height in pixels.</value>
        double HeightInPixels { get; set; }
    }
}