using System;
using System.Configuration;
using System.IO;

namespace Depiction.API.DepictionConfiguration
{
    public class DepictionUserConfigurationManager
    {
        private const string DefaultSettings = "DefaultSettings";
        private static Configuration depictionConfiguration;
        private static string configFile = "";
        private static string configFileName = "";

        protected static bool SetupConfigPath()
        {
            configFile = string.Concat("DepictionUser", ".config");
            if (DepictionAccess.PathService == null) return false;
            configFileName = Path.Combine(DepictionAccess.PathService.AppDataDirectoryPath, configFile);
            return true;
        }
        public static void CreateDepictionUserConfigFile()
        {
            SetupConfigPath();
            if (!File.Exists(configFileName))
            {
                var configExe = new ExeConfigurationFileMap();// ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
                configExe.ExeConfigFilename = configFile;
                configExe.LocalUserConfigFilename = configFile;
                configExe.RoamingUserConfigFilename = configFile;

                var config = ConfigurationManager.OpenMappedExeConfiguration(configExe, ConfigurationUserLevel.None);

                config.SectionGroups.Clear();
                config.Sections.Clear();
                config.SaveAs(configFileName, ConfigurationSaveMode.Minimal, true);
                config.Sections.Add(DefaultSettings, new DefaultDepictionConfigurationSection());
                config.Save(ConfigurationSaveMode.Modified);
            }
        }

        public static Configuration DepictionUserConfiguration()
        {
            SetupConfigPath();
            //Not really sure what mapped does yet, but im sure it is better than what im current doing
            if (!File.Exists(configFileName)) return null;
            //Always get a new one because sometime people use multiple depictions.
            //if (depictionConfiguration == null)
            //{
            var configExe = new ExeConfigurationFileMap();
            // ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            configExe.ExeConfigFilename = configFileName;
            depictionConfiguration = ConfigurationManager.OpenMappedExeConfiguration(configExe, ConfigurationUserLevel.None);

            //}
            return depictionConfiguration;
        }
        public static DefaultDepictionConfigurationSection GetSectionDefaultDepictionConfigurationSection(Configuration depictionConfig)
        {
            var config = depictionConfig;

            ConfigurationSection section = null;
            try
            {
                section = depictionConfig.Sections[DefaultSettings];
            }catch{}

            if (section == null)
            {
                try
                {
                    config.Sections.Add(DefaultSettings, new DefaultDepictionConfigurationSection());
                }catch
                {
                    config.Sections.Remove(DefaultSettings);
                    config.Sections.Add(DefaultSettings, new DefaultDepictionConfigurationSection());
                }
                config.Save(ConfigurationSaveMode.Modified);
            }
            return ((DefaultDepictionConfigurationSection)config.GetSection(DefaultSettings));
        }

        public static void MapExeConfiguration()
        {
            if (DepictionAccess.PathService == null) return;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);

            var folder = DepictionAccess.PathService.AppDataDirectoryPath;
            var fullFileName = Path.Combine(folder, configFile);
            config.SaveAs(fullFileName, ConfigurationSaveMode.Full);
        }
    }

    public class DefaultDepictionConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("RecentStartLocation")]
        public string RecentStartLocation
        {
            get { return (string)this["RecentStartLocation"] ?? ""; }
            set { this["RecentStartLocation"] = value; }
        }
        [ConfigurationProperty("SelectStartGeolocations")]
        public GeolocatedLocationRecords SelectStartGeolocations
        {
            get { return (GeolocatedLocationRecords)this["SelectStartGeolocations"] ?? new GeolocatedLocationRecords(); }
        }
    }
}