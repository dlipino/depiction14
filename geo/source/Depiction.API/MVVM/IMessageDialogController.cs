using System.Windows;

namespace Depiction.API.MVVM
{
    //Kind of a hack, but from what i heard this is needed to show dialogs in MVVM.
    public interface IMessageDialogController
    {
        void  ShowNonModalDialog(string message);

        void ShowNonModalDialog(string message, string title);

        void ShowNonModalDialog(Window owner, string message, string title);

        MessageBoxResult ShowModalDialog(string message);

        MessageBoxResult ShowModalDialog(string message, string title);

        MessageBoxResult ShowModalDialog(Window owner, string message, string title);

        MessageBoxResult ShowModalDialog(string message, string title, MessageBoxButton buttonOptions);
        MessageBoxResult ShowModalDialog(Window owner, string message, string title, MessageBoxButton buttonOptions);
    }
}