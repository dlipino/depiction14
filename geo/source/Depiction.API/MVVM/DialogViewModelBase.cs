﻿using System.Windows.Input;

namespace Depiction.API.MVVM
{
    abstract public class DialogViewModelBase : ViewModelBase, IDepictionDialogVM
    {//some of these are not needed (like temp dialog and is model)
        private DelegateCommand<bool?> toggleDialogVisiblityCommand;
        private bool isDialogVisible;
        private string dialogDisplayName = "Default Dialog";

        public ICommand ToggleDialogVisibilityCommand
        {
            get
            {
                if (toggleDialogVisiblityCommand == null)
                {
                    toggleDialogVisiblityCommand = new DelegateCommand<bool?>(ToggleDialogVisibility);
                }
                return toggleDialogVisiblityCommand;
            }
        }

        public bool IsDialogVisible
        {
            get { return isDialogVisible; }
            set
            {
                ToggleDialogVisibility(value);
            }
        }
        public bool IsTemporaryDialog { get; protected set; }
        public bool IsModel { get; set; }
        public string DialogDisplayName
        {
            get { return dialogDisplayName; }
            set
            {
                dialogDisplayName = value;
                ModifyCommandText();
            }
        }


        #region Protected methods
        virtual protected void ToggleDialogVisibility(bool? obj)
        {
            if (obj == null) isDialogVisible = !isDialogVisible;
            else
            {
                //Kind of a hack fix for the tipcontrol appearring when starting a new story because it is supposed
                //to open after the quickstart dialog closes.
                if(!obj.Equals(isDialogVisible)) isDialogVisible = (bool)obj;
            }
            if (toggleDialogVisiblityCommand == null)
            {
                toggleDialogVisiblityCommand = new DelegateCommand<bool?>(ToggleDialogVisibility);
            }
            ModifyCommandText();
            NotifyPropertyChanged("IsDialogVisible");
        }
        protected void ModifyCommandText()
        {
            if (toggleDialogVisiblityCommand == null)
            {
                toggleDialogVisiblityCommand = new DelegateCommand<bool?>(ToggleDialogVisibility);
            }
            if (isDialogVisible)
            {
                toggleDialogVisiblityCommand.Text = string.Format("Hide {0}", dialogDisplayName);
            }
            else
            {
                toggleDialogVisiblityCommand.Text = string.Format("Show {0}", dialogDisplayName);
            }
        }
        #endregion
    }
}