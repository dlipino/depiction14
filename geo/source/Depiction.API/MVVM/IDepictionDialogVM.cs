﻿using System.ComponentModel;
using System.Windows.Input;

namespace Depiction.API.MVVM
{
    public interface IDepictionDialogVM : INotifyPropertyChanged
    {
        ICommand ToggleDialogVisibilityCommand { get; }
        bool IsDialogVisible { get; set; }
    }
}