﻿namespace Depiction.API.MVVM
{
    public interface IDepictionMainWindow
    {
        IMessageDialogController MessageDialogs { get; }
//        void AddContentControlToMapCanvas(CommonControlFrameBase controlToAdd);
        void OpenDepictionFileWithFullView(string fileName,bool isSample);
    }
}