﻿using System;
using System.Collections.Generic;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.DepictionComparers
{
    public static class PropertyComparers
    {
        public class InternalPropertyNameComparer<T>: IEqualityComparer<T> where T : IElementProperty
        {
            public bool Equals(T x, T y)
            {
                return string.Equals(x.InternalName, y.InternalName, StringComparison.CurrentCultureIgnoreCase);
            }

            public int GetHashCode(T obj)
            {
                return obj.InternalName.ToLower().GetHashCode();
            }
        }

        public class PropertyNameAndTypeComparer<T> : IEqualityComparer<T> where T : IElementProperty
        {
            public bool IsNumber(object val)
            {
                if (val == null) return false;
                if (val is double || val is int || val is float)
                {
                    return true;
                }
                return false;
            }

            public bool Equals(T x, T y)
            {
                bool typeIsComparable = false;
                if (x.ValueType == y.ValueType)
                {
                    typeIsComparable = true;
                }
                if (IsNumber(x.Value) && IsNumber(y.Value))
                {
                    typeIsComparable = true;
                }

                return string.Equals(x.InternalName, y.InternalName, StringComparison.CurrentCultureIgnoreCase) && typeIsComparable;
            }

            public int GetHashCode(T obj)
            {
                return obj.InternalName.ToLower().GetHashCode();
            }

        }
    }
}