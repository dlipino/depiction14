namespace Depiction.API.CoreEnumAndStructs
{
    public enum TileImageTypes
    {
        Unknown,
        None,
        Street,
        Satellite,
        Topographic
    }
    //Simplified zoi types used to determine how to add a polygon, default is Point
    public enum ZOIShapeType
    {
        Unknown,Point,Line,UserLine,Polygon,UserPolygon
    }
    /// <summary>
    /// The type of geometry that a Zone of Influence can be. Shoult match
    /// types for wtk elements
    /// </summary>
    public enum DepictionGeometryType
    {
        /// <summary>
        /// A point.
        /// </summary>
        Point,

        /// <summary>
        /// A single line.
        /// </summary>
        LineString,

        /// <summary>
        /// A polygon.
        /// </summary>
        Polygon,

        /// <summary>
        /// An ordered list of points.
        /// </summary>
        MultiPoint,

        /// <summary>
        /// An ordered list of lines.
        /// </summary>
        MultiLineString,

        /// <summary>
        /// An ordered list of polygons.
        /// </summary>
        MultiPolygon,

        /// <summary>
        /// A regular grid of data covering a given geographical area.
        /// </summary>
        Coverage,
       GeometryNotSet
    } ;
    //This might be very short lived
    public enum DepictionReadableFileTypes
    {
        Unknown,
        Shapefile,
        GML,
        GPX,
        KML,
        KMZ
    }

}