﻿using System;
using GeoFramework;

namespace Depiction.API.CoreEnumAndStructs
{
    public class UTMLocation
    {
        public double NorthingMeters { get; set; }//I guess the meters is redundent
        public double EastingMeters { get; set; }
        public int UTMZoneNumber { get; set; }
        public ZoneLetter UTMZoneLetter { get; set; }
        public string UTMZoneLetterString
        {
            get { return UTMZoneLetter.ToString(); }
            set
            {
                var zoneLetter = ZoneLetter.Unknown;
                try
                {
                    zoneLetter = (ZoneLetter)Enum.Parse(typeof (ZoneLetter), value, true);
                }catch(Exception ex)
                {
                    zoneLetter = ZoneLetter.Unknown;
                }
                UTMZoneLetter = zoneLetter;
            }
        }
//        public ILatitudeLongitude ToLatLong()
//        {
//            var position = new Position(UTMZoneLetter, UTMZoneNumber, new Distance(EastingMeters, DistanceUnit.Meters),
//                                        new Distance(NorthingMeters, DistanceUnit.Meters));
//            var x = position.ToCartesianPoint().X.Value;
//            var y = position.ToCartesianPoint().Y.Value;
//
//            return new LatitudeLongitudeBase(y, x);
//        }
        public override string ToString()
        {
            //Produces a very nice lat/long string
            //            var position = new Position(UTMZoneLetter, UTMZoneNumber, new Distance(EastingMeters, DistanceUnit.Meters),
            //                                        new Distance(NorthingMeters, DistanceUnit.Meters));
            var outString = string.Format("{0}{1} {2:0} {3:0}", UTMZoneNumber, UTMZoneLetterString, EastingMeters,
                                          NorthingMeters);
            return outString;
        }
    }
}