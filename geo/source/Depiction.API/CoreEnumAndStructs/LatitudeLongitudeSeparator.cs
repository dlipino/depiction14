namespace Depiction.API.CoreEnumAndStructs
{
    public enum LatitudeLongitudeSeparator
    {
        CommaAndSpace,
        Space,
        Comma,
    }
}