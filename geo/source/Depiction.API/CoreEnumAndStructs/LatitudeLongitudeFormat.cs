namespace Depiction.API.CoreEnumAndStructs
{
    public enum LatitudeLongitudeFormat
    {
        UTM,
        ColonFractionalSeconds,
        ColonIntegerSeconds,
        Decimal,
        DegreesFractionalMinutes,
        DegreesMinutesSeconds,
        DegreesWithDCharMinutesSeconds,
        SignedDecimal,
        SignedDegreesFractionalMinutes,
    }
}