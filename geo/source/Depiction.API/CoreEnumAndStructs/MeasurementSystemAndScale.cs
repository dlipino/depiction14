﻿namespace Depiction.API.CoreEnumAndStructs
{
    /// <summary>
    /// A set of buckets representing the scale and measurement system (imperial or metric) that 
    /// Depiction can use for the values of an element's property.
    /// </summary>
    public enum MeasurementSystem
    {
        /// <summary>
        /// Medium-sized values in the Imperial measurement system.
        /// </summary>
        Metric = 0,
        Imperial = 3

    }

    public enum MeasurementScale
    {
        Small=0,
        Normal=1,
        Large=2
    }
}