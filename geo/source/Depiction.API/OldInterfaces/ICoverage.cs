﻿using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.API.OldInterfaces
{
    public interface ICoverage
    {
        int GetGridWidthInPixels();
        int GetGridHeightInPixels();
        int GetRow(double lat);
        int GetColumn(double lon);
        float GetValueAtGridCoordinate(int col, int row);
        void SetValueAtGridCoordinate(int col, int row, float value);
        float GetInterpolatedElevationValue(ILatitudeLongitude latitudeLongitude);
        float GetConvolvedValue(int col, int row, int kernel);
        ILatitudeLongitude GetTopLeftPosition();
        ILatitudeLongitude GetBottomRightPosition();

        string GetProjectionInWkt();
        bool IsFloatMode();
        double GetGridResolution();
        double GetLatitude(int row);
        double GetLongitude(int col);
        IDepictionImageMetadata Visual { get; }
        void ClearVisual();
        string GetValueForDisplay(ILatitudeLongitude position);
    }
}