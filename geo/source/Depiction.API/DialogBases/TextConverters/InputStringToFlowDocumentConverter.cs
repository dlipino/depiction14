﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace Depiction.API.DialogBases.TextConverters
{
    public class InputStringToFlowDocumentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                var stringValue = value.ToString();
                //Check to see if it is html
                if (stringValue.Contains("<"))
                {
                    try
                    {
                        //Check to make sure normal text is not mixed in in \n\r characters
                        //This seems like an arbitrary location for this since the error that 
                        //was found dealt with <br/> so i suppose replacing that char
                        //with a new line would also work, sort of.
                        var normalLineBreaks = Regex.Split(stringValue, "\r\n").Length;
                        if (normalLineBreaks == 0)
                        {
                            return HtmlToFlowDocumentConverter.StringToFlowDocument(stringValue);
                        }else
                        {
                            var tempText = Regex.Replace(stringValue, Environment.NewLine, "<br/>");
                            return HtmlToFlowDocumentConverter.StringToFlowDocument(tempText);
                        }
                    }catch(Exception ex)
                    {
                        //Something went wrong with the conversion, display the string value with an error message
                        stringValue = "An error occured with the html in the property. To view the full html the error needs to be fixed.\n" + stringValue;
                    }

                }
                return PlainTextToFlowDocumentConverter.PlainTextToFlowDocument(stringValue);
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}