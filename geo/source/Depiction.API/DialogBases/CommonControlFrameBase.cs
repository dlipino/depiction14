﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Depiction.APINew.DialogBases
{
    /// <summary>
    /// </summary>
    /// this is so legacy, it hurts
    [TemplatePart(Name = "PART_DragThumb", Type = typeof(Thumb))]
    public class CommonControlFrameBase : ContentControl
    {
        public event EventHandler Closed;
        static public RoutedUICommand CommonControlAcceptCommand = new RoutedUICommand("Accept", "AcceptCommand", typeof(CommonControlFrameBase));
        static public RoutedUICommand CommonControlCancelCommand = new RoutedUICommand("Cancel", "CancelCommand", typeof(CommonControlFrameBase));
        static public RoutedUICommand CommonControlApplyCommand = new RoutedUICommand("Apply", "ApplyCommand", typeof(CommonControlFrameBase));
        protected ContentPresenter contentPresenter;
//        static CommonControlFrameBase()
//        {
//            DefaultStyleKeyProperty.OverrideMetadata(typeof(CommonControlFrameBase), new FrameworkPropertyMetadata(typeof(CommonControlFrameBase)));
//        }

        public MessageBoxResult DialogResult = MessageBoxResult.None;
        public bool disableShow;
        private Storyboard burpStoryboard;

        #region Dep Propts

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(CommonControlFrameBase), new UIPropertyMetadata(typeof(CommonControlFrameBase).Name));

        public double TitleSize
        {
            get { return (double)GetValue(TitleSizeProperty); }
            set { SetValue(TitleSizeProperty, value); }
        }
        public static readonly DependencyProperty TitleSizeProperty =
            DependencyProperty.Register("TitleSize", typeof(double), typeof(CommonControlFrameBase), new UIPropertyMetadata(18.0));



        public ImageSource DialogIconSource
        {
            get { return (ImageSource)GetValue(DialogIconSourceProperty); }
            set { SetValue(DialogIconSourceProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DialogIconSource.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DialogIconSourceProperty =
            DependencyProperty.Register("DialogIconSource", typeof(ImageSource), typeof(CommonControlFrameBase), new UIPropertyMetadata(null));



        public Point StartPosition
        {
            get { return (Point)GetValue(StartPositionProperty); }
            set { SetValue(StartPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for StartPosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty StartPositionProperty =
            DependencyProperty.Register("StartPosition", typeof(Point), typeof(CommonControlFrameBase), new UIPropertyMetadata(new Point(),StarPositionSet));

        private static void StarPositionSet(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            if (e.NewValue.Equals(e.OldValue))
                return;
            var start = (Point)e.NewValue;
            var ctrl = (CommonControlFrameBase)d;
            ctrl.CurrentLocation=new Point(start.X,start.Y);
        }
        
        public bool ShowInfoButton
        {
            get { return (bool)GetValue(ShowInfoButtonProperty); }
            set { SetValue(ShowInfoButtonProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ShowInfoButton.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ShowInfoButtonProperty =
            DependencyProperty.Register("ShowInfoButton", typeof(bool), typeof(CommonControlFrameBase), new UIPropertyMetadata(false));

        public Collection<UIElement> ActionControls
        {
            get { return (Collection<UIElement>)GetValue(ActionControlsProperty); }
            set { SetValue(ActionControlsProperty, value); }
        }
        public static readonly DependencyProperty ActionControlsProperty =
            DependencyProperty.Register("ActionControls", typeof(Collection<UIElement>), typeof(CommonControlFrameBase), new UIPropertyMetadata(null));


        public bool? IsShown
        {
            get { return (bool?)GetValue(IsShownProperty); }
            set { SetValue(IsShownProperty, value); }
        }
        public static readonly DependencyProperty IsShownProperty =
            DependencyProperty.Register("IsShown", typeof(bool?), typeof(CommonControlFrameBase), new UIPropertyMetadata(null, IsShownChanged));

        private static void IsShownChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null) return;
            if (e.NewValue.Equals(e.OldValue))
                return;

            var ctrl = (CommonControlFrameBase)d;
            if (ctrl.disableShow) return;
            var newVal = (bool?)e.NewValue;
            switch (newVal)
            {
                case true:
                    ctrl.Show();
                    break;
                case false:
                    ctrl.Hide();
                    break;
                default:
                    break;
            }
        }

        #endregion
        #region Props

        public Point CurrentLocation { get; set; }
        public string AssociatedHelpFile { get; set; }

        #endregion
        #region Constructors

        public CommonControlFrameBase()
        {
            ActionControls = new Collection<UIElement>();

            // This transform group is required by the drag envent handler.
            var transformGroup = new TransformGroup();
            transformGroup.Children.Add(new ScaleTransform { ScaleX = 1, ScaleY = 1 });
            transformGroup.Children.Add(new SkewTransform { AngleX = 0, AngleY = 0 });
            transformGroup.Children.Add(new RotateTransform { Angle = 0 });
            transformGroup.Children.Add(new TranslateTransform { X = 0, Y = 0 });
            RenderTransform = transformGroup;
            FocusVisualStyle = null;
            AssociatedHelpFile = "welcome.html";
            Name = "CommonControlBase";
            CurrentLocation = new Point(StartPosition.X, StartPosition.Y);
            BindCommands();
            Loaded += CommonControlFrame2_Loaded;

        }
        public CommonControlFrameBase(Point start)
            : this()
        {
            StartPosition = start;
        }
        #endregion

        #region Constructor helpers

        void CommonControlFrame2_Loaded(object sender, RoutedEventArgs e)
        {
            Canvas.SetLeft(this, CurrentLocation.X);
            Canvas.SetTop(this, CurrentLocation.Y);
        }

        private void BindCommands()
        {
            var bindings = CommandBindings;
            var commandBinding = new CommandBinding(ApplicationCommands.Stop, Close_Executed);
            bindings.Add(commandBinding);
            commandBinding = new CommandBinding(CommonControlAcceptCommand, Accept_Executed);
            bindings.Add(commandBinding);
            commandBinding = new CommandBinding(CommonControlCancelCommand, Close_Executed);
            bindings.Add(commandBinding);
            commandBinding = new CommandBinding(CommonControlApplyCommand, Apply_Executed);
            bindings.Add(commandBinding);
        }
        
        #endregion

        #region Command handlers

        private void Close_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var control = sender as CommonControlFrameBase;
            if (control == null) return;
            control.DialogResult = MessageBoxResult.Cancel;//order is important (davidl)
            DefaultCloseCancelOrders();
        }
        private void Apply_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = MessageBoxResult.None;
            DefaultApplyOrders();
        }

        private void Accept_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = MessageBoxResult.OK;
            DefaultAcceptOrders();
        }

        void dragThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            var transgroup = RenderTransform as TransformGroup;
            if (transgroup == null) return;

            if (transgroup.Children.Count >= 4 && transgroup.Children[3] is TranslateTransform)
            {
                var translate = (TranslateTransform)transgroup.Children[3];
                if (Parent is Panel)
                {
                    var p = Parent as Panel;
                    if (p != null)
                    {
                        var s = p.RenderSize;
                        var point = Mouse.GetPosition(p);
                        if (point.X < 5 || point.Y < 5) return;
                        double modW = s.Width;
                        if ((s.Width - 5) > 0)
                        {
                            modW = s.Width - 5;
                        }
                        double modH = s.Height;
                        if ((s.Height - 5) > 0)
                        {
                            modH = s.Height - 5;
                        }
                        if (point.X > modW || point.Y > modH) return;
                    }
                }
                translate.X += e.HorizontalChange;
                translate.Y += e.VerticalChange;
                CurrentLocation = new Point(translate.X, translate.Y);
            }
        }

        #endregion

        #region Dialog locatio methods

        public void ResetTransform()
        {
            var transformGroup = new TransformGroup();
            transformGroup.Children.Add(new ScaleTransform { ScaleX = 1, ScaleY = 1 });
            transformGroup.Children.Add(new SkewTransform { AngleX = 0, AngleY = 0 });
            transformGroup.Children.Add(new RotateTransform { Angle = 0 });
            transformGroup.Children.Add(new TranslateTransform { X = 0, Y = 0 });
            RenderTransform = transformGroup;
            Canvas.SetLeft(this, StartPosition.X);
            Canvas.SetRight(this, StartPosition.Y);
        }

        public virtual void Center()
        {
            if (Parent == null) return;
            var parent = (Panel)Parent;
            ResetTransform();
            double aw = Width;
            if (aw.Equals(double.NaN)) aw = ActualWidth;
            if (aw == 0) aw = MinWidth;

            double ah = Height;
            if (ah.Equals(double.NaN)) ah = ActualHeight;
            if (ah == 0) ah = MinHeight;

            double w = parent.ActualWidth / 2 - aw / 2;
            double h = parent.ActualHeight / 2 - ah / 2;
            Canvas.SetLeft(this, w);
            Canvas.SetTop(this, h);
        }
        public virtual void CenterWidthAndGivenTop(int top)
        {
            if (Parent == null) return;
            var parent = (Panel)Parent;
            ResetTransform();
            double aw = Width;
            if (aw.Equals(double.NaN)) aw = ActualWidth;
            if (aw == 0) aw = MinWidth;

            double w = parent.ActualWidth / 2 - aw / 2;

            Canvas.SetLeft(this, w);
            Canvas.SetTop(this, top);
        }

        public void DoShowStoryBoard()
        {
            BringToFront();
            if (burpStoryboard == null)
            {
                RegisterName(Name, this);
                var duration = new Duration(TimeSpan.FromMilliseconds(75));
                var myDoubleAnimationX = new DoubleAnimation
                                             {
                                                 From = 1.0,
                                                 To = 1.05,
                                                 Duration = duration,
                                                 AutoReverse = true
                                             };
                var myDoubleAnimationY = new DoubleAnimation
                                             {
                                                 From = 1.0,
                                                 To = 1.05,
                                                 Duration = duration,
                                                 AutoReverse = true
                                             };

                // Create the storyboard.
                burpStoryboard = new Storyboard();
                burpStoryboard.Children.Add(myDoubleAnimationX);
                burpStoryboard.Children.Add(myDoubleAnimationY);
                Storyboard.SetTargetName(myDoubleAnimationX, Name);
                Storyboard.SetTargetName(myDoubleAnimationY, Name);

                var myPropertyPathX =
                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleX)");
                Storyboard.SetTargetProperty(myDoubleAnimationX, myPropertyPathX);
                var myPropertyPathY =
                    new PropertyPath("(UIElement.RenderTransform).(TransformGroup.Children)[0].(ScaleTransform.ScaleY)");
                Storyboard.SetTargetProperty(myDoubleAnimationY, myPropertyPathY);
            }
            burpStoryboard.Begin(this, false);


        }
        #endregion

        #region SHow hide plus some helpers
        public void PermaClose()
        {
            var parent = Parent as Panel;
            
            if (parent != null)
            {
                parent.Children.Remove(this);
            }
        }

        public void BringToFront()
        {
            // Bring the control to the foreground.
            var parent = Parent as Panel;
            // this stops the endless loop that was happening on the constent 
            //removing and readding.
            disableShow = true;
            if (parent != null)
            {
                var index = parent.Children.IndexOf(this);
                if (index < parent.Children.Count - 1)
                {
                    parent.Children.Remove(this);
                    try
                    {
                        parent.Children.Add(this);
                        Focus();
                    }
                    catch{}
                }
            }

            disableShow = false;
        }
        //TODO i think thi is old and needs to be updated 
        protected virtual void Show()
        {
            BringToFront();
            Visibility = Visibility.Visible;
            Focus();
            if (Parent is Canvas)
            {
                var parentRect = new RectangleGeometry
                                     {
                                         Rect = LayoutInformation.GetLayoutSlot(((Canvas)Parent))
                                     };

                var myRect = new RectangleGeometry(new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), ActualWidth, ActualHeight), 0, 0, RenderTransform);
                if (!parentRect.Bounds.Contains(myRect.Bounds)) ResetTransform();
            }
        }

        protected virtual void Hide()
        {
            Visibility = Visibility.Collapsed;
            RaiseClosed();
        }

        protected void RaiseClosed()
        {
            if (Closed != null)
                Closed(this, new EventArgs());
        }
        protected virtual void DefaultCloseCancelOrders()
        {
            //This will remove binds, hence the need for overriding.
            IsShown = false;
        }
        protected virtual void DefaultAcceptOrders()
        {
            IsShown = false;
        }
        
        protected virtual void DefaultApplyOrders()
        {
            //Apply is only used in special situations so an override is needed
        }
        #endregion

        #region overrides

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            contentPresenter = Template.FindName("Part_ContentPresenter",this) as ContentPresenter;
            var thumb = Template.FindName("PART_DragThumb", this) as Thumb;
            if (thumb != null)
            {
                thumb.DragDelta += dragThumb_DragDelta;
            }

            var actionControlPanel = Template.FindName("PART_ActionPanel", this) as Panel;
            if (actionControlPanel != null)
            {
                foreach (UIElement element in ActionControls)
                {
                    actionControlPanel.Children.Add(element);
                }
            }
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            BringToFront();
            e.Handled = false;
            base.OnPreviewMouseDown(e);
        }

        #endregion


    }
}