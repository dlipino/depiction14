﻿using Depiction.API.DialogBases.TextConverters;
using Depiction.API.ValueTypeConverters;

namespace Depiction.API.DialogBases
{
    public class DepictionAPIConverters
    {
        public static readonly PlainTextToFlowDocumentConverter PlainTextToFlowDocument = new PlainTextToFlowDocumentConverter();
        public static readonly InputStringToFlowDocumentConverter InputStringToFlowDocument = new InputStringToFlowDocumentConverter();
        public static readonly HtmlToFlowDocumentConverter HtmlToFlowDocument = new HtmlToFlowDocumentConverter();
        public static readonly DepictionImageResourceToSourceConverter IconPathToSource =
            new DepictionImageResourceToSourceConverter();
    }
}
