﻿using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.Serialization;

namespace Depiction.API.HelperObjects
{
    [TypeConverter("Depiction.API.ValueTypeConverters.DepictionIconPathTypeConverter")]
    public class DepictionIconPath : IXmlSerializable
    {//This class is somewhat legacy, either way i dont' really know how it works or how it is supposed to work.
        public IconPathSource Source { get; set; }
        public string Path { get; set; }//This is the depiction resource path
        public bool IsValid { get; set; }
        public DepictionIconPath()
        {
            IsValid = true;
            Source = IconPathSource.EmbeddedResource;
            Path = "Icon.Default";
        }
        public DepictionIconPath(IconPathSource source, string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                IsValid = false;
                Source = source;
                Path = path;
            }
            else
            {
                IsValid = true;
                Source = source;
                Path = path;
            }
        }
        public DepictionIconPath(string fullDepictionUri)
        {
            var parsed = Parse(fullDepictionUri);
            IsValid = parsed.IsValid;
            Source = parsed.Source;
            Path = parsed.Path;
        }
        public override string ToString()
        {
            string srcString = null;
            switch (Source)
            {
                case IconPathSource.EmbeddedResource:
                    srcString = "embeddedresource";
                    break;
                case IconPathSource.File:
                    srcString = "file";
                    break;
            }
            if (srcString == null || ! IsValid)
                throw new Exception("Invalid DepictionIconPath source");

            return string.Format("{0}:{1}", srcString, Path);
        }
        //This doesn't make any sense to me
        public static DepictionIconPath Parse(string iconUri)
        {
            var iconPath = new DepictionIconPath {IsValid = false};
            if (iconUri != null)
            {
                var parts = iconUri.Split(new[] { ':' }, 2);
                if (parts.Length > 1)
                {
                    var sourceString = parts[0].ToLower();
                    iconPath.Path = parts[1];
                    
                    switch (sourceString.ToLower())
                    {
                        case "xaml": // Deprecated.
                        case "embeddedresource":
                            iconPath.Source = IconPathSource.EmbeddedResource;
                            iconPath.IsValid = true;
                            break;
                        case "embeddedbitmap": // Deprecated.
                        case "file":
                            iconPath.Source = IconPathSource.File;
                            iconPath.IsValid = true;
                            break;
                        case "http":
                            iconPath.Source = IconPathSource.File;
                            iconPath.IsValid = true;
                            iconPath.Path = iconUri;
                            break;
                        default:
                            //Uri that is a file path, such as C:\blah\bleh\am.gif?
                            iconPath.Source = IconPathSource.File;
                            iconPath.IsValid = true;
                            iconPath.Path = iconUri;
                            break;
                    }
                }
            }
            return iconPath;
        }

        #region Equals overrides

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionIconPath;
            if (other == null) return false;
            if (!Path.Equals(other.Path)) return false;
            if (!IsValid.Equals(other.IsValid)) return false;
            if (!Source.Equals(other.Source)) return false;
            return true;
        }
        public override int GetHashCode()
        {
            int hashCode = Path.GetHashCode() >> 3;
            hashCode ^= Source.GetHashCode();
            return hashCode;
        }

        #endregion

        #region xmlserialization
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var nameSpace = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            if(reader.Name.Equals("FullIconPath"))
            {
                var stringVal = reader.ReadElementContentAsString("FullIconPath", nameSpace);
                var parsed = Parse(stringVal);
                IsValid = parsed.IsValid;
                Source = parsed.Source;
                Path = parsed.Path;
            }
            
            reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var nameSpace = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("DepictionIconPath");
            writer.WriteElementString("FullIconPath", nameSpace, ToString());
            writer.WriteEndElement();
        }
        #endregion
    }

    /// <summary>
    /// The original source for the icon definition.
    /// </summary>
    public enum IconPathSource
    {
        /// <summary>
        /// The icon came from a image file.
        /// </summary>
        File,

        /// <summary>
        /// The icon came from a resource embedded in a Depiction dll.
        /// </summary>
        EmbeddedResource
    } ;
}