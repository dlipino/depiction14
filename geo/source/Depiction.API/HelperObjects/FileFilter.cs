﻿using Depiction.API.AddinObjects.Interfaces;

namespace Depiction.API.HelperObjects
{
    /// <summary>
    /// A container for the information needed to describe how to filter the list of files shown in a file open dialog,
    /// for instance.
    /// </summary>
    public class FileFilter
    {

        public static FileFilter AllTypesFilter
        {
            get
            {
                return new FileFilter { Description = "All Files (*.*)", Wildcard = "*.*", Selected = true };
            }
        }

        public static FileFilter DMLTypeFilter
        {
            get
            {
                return CreateFileFilter(".dml", "Depiction dml", true);
            }
        }
        public static FileFilter InteractionTypeFilter
        {
            get
            {
                return CreateFileFilter(".xml", "Depiction interactions", true);
            }
        }

        /// <summary>
        /// The description to show for the type of files being listed.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The list of wildcards to use for filtering the files shown.
        /// </summary>
        public string Wildcard { get; set; }

        /// <summary>
        /// Automatically select the first file in the list.
        /// </summary>
        public bool Selected { get; set; }
        public IDepictionAddonBase FileIOType { get; set; }

        public static FileFilter CreateFileFilter(string extension, string fileTypeName, bool selected)
        {
            var filter = new FileFilter
            {
                Description = string.Format("{0} Files ({1})", fileTypeName, extension),
                Wildcard = string.Format("*{0}", extension),
                Selected = selected
            };
            return filter;
        }
    }
}