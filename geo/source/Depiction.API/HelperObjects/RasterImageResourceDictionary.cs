﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Service;
using Depiction.API.StaticAccessors;
using Depiction.Serialization;

namespace Depiction.API.HelperObjects
{
    public class RasterImageResourceDictionary : ResourceDictionary, IXmlSerializable
    {
        #region Stuff for removing images taht are not used
        public static ResourceDictionary GetIconsForUsedElements()
        {
            var usedImages = new RasterImageResourceDictionary();
            var elementIcons = ProductAndFolderService.DefaultIconDictionary;

            var imageNames = new HashSet<string>();
            if (DepictionAccess.CurrentDepiction != null)
            {
                foreach (var elem in DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements)
                {
                    var iconPath = elem.IconPath;
                    if (iconPath.Source.Equals(IconPathSource.EmbeddedResource))
                    {
                        var path = iconPath.Path;
                        if (!string.IsNullOrEmpty(path))
                        {
                            imageNames.Add(path);
                        }
                    }

                    foreach (var waypoint in elem.Waypoints)
                    {
                        iconPath = waypoint.IconPath;
                        if (iconPath.Source.Equals(IconPathSource.EmbeddedResource))
                        {
                            var path = iconPath.Path;
                            if (!string.IsNullOrEmpty(path))
                            {
                                imageNames.Add(path);
                            }
                        }
                    }
                }
            }
            foreach (var imageKey in imageNames)
            {
                var key = new DepictionIconPath(IconPathSource.EmbeddedResource, imageKey);
                if (elementIcons.ContainsKey(key))
                {
                    usedImages.Add(key.Path, elementIcons[key]);
                }
            }
            return usedImages;
        }

        public static ResourceDictionary RemoveUnusedImagesFromResources()
        {
            var matchingRD = new RasterImageResourceDictionary();

            if (DepictionAccess.CurrentDepiction == null) return matchingRD;
            var imageNames = new List<string>();
            foreach (var elem in DepictionAccess.CurrentDepiction.CompleteElementRepository.AllElements)
            {
                var name = elem.IconPath.Path;
                if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
                {
                    imageNames.Add(name);
                }
                foreach (var waypoint in elem.Waypoints)
                {
                    name = waypoint.IconPath.Path;
                    if (!string.IsNullOrEmpty(name) && !imageNames.Contains(name))
                    {
                        imageNames.Add(name);
                    }
                }
                var fileName = elem.ImageMetadata.ImageFilename;
                if (!string.IsNullOrEmpty(fileName) && !imageNames.Contains(fileName))
                {
                    imageNames.Add(fileName);
                }
            }
            foreach (object keyObject in DepictionAccess.CurrentDepiction.ImageResources.Keys)
            {
                var keyString = keyObject as string;

                if (!string.IsNullOrEmpty(keyString) && imageNames.Contains(keyString))
                {
                    matchingRD.Add(keyObject, DepictionAccess.CurrentDepiction.ImageResources[keyObject]);
                }
            }

            //            var usedImages = CommonData.Story.BitmapResources.Where(images => (imageNames.Contains(images.Key)));
            return matchingRD;

        }

        #endregion

        #region Equals override
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (GetType() != obj.GetType()) return false;

            // safe because of the GetType check
            var other = (RasterImageResourceDictionary)obj;
            if (Keys.Count != other.Keys.Count) return false;
            foreach (var key in Keys)
            {
                if (!other.Contains(key)) return false;
                //                if(!Equals(this[key].GetType(),other[key].GetType())) return false;
                //                if (!Equals(this[key].GetType(), other[key].GetType())) return false;
            }

            return true;
        }
        #endregion

        #region IXmlSerializable Implementation
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            var ns = SerializationConstants.DepictionXmlNameSpace;
            if (!reader.Name.Equals("RasterImageDictionary")) return;
            reader.ReadStartElement();
            var rasterFilenames = SerializationService.DeserializeItemList<string>("RasterFilenames", typeof(string), reader);
            var rasterKeys = SerializationService.DeserializeItemList<string>("RasterKeys", typeof(string), reader);

            for (int i = 0; i < rasterFilenames.Count; i++)
            {
                var filename = rasterFilenames[i];
                var key = rasterKeys[i];
                var imageSaveDir = SerializationService.SerializedImageFileFolder;

                string fullSavePath = Path.Combine(imageSaveDir, filename);
                var image = BitmapSaveLoadService.LoadImageFromURLAndCreateThumb(fullSavePath, 30, 30);
                if (image != null)
                {
                    Add(key, image);
                }
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            if (SerializationService.IsSaveLoadCancelled() == true) return;
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteStartElement("RasterImageDictionary");
            var rasterFilenames = new List<string>();
            var rasterKeys = new List<string>();
            foreach (string key in Keys)
            {
                var extension = Path.GetExtension(key);
                // since the original filename is used as the key, grab the extension from that for enhanced readability
                //The Guid id was needed in 1.2 because the full path was used which didn't save well
                //                var filename = string.Format("{0}{1}", Guid.NewGuid(), extension);
                var filename = string.Format("{0}{1}", Path.GetFileNameWithoutExtension(key), extension);
                var imageSaveDir = SerializationService.SerializedImageFileFolder;

                string fullSavePath = Path.Combine(imageSaveDir, filename);
                var saved = false;

                if (BitmapSaveLoadService.SaveBitmap(this[key] as BitmapSource, fullSavePath))
                {
                    saved = true;
                    rasterFilenames.Add(filename);
                    rasterKeys.Add(key);
                }

                if (!saved)
                {
                    Debug.WriteLine(string.Format("Could not save {0}", key));
                }
            }
            SerializationService.SerializeItemList("RasterFilenames", rasterFilenames, writer);
            SerializationService.SerializeItemList("RasterKeys", rasterKeys, writer);
            writer.WriteEndElement();

        }
        #endregion
    }
}