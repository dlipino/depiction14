﻿namespace Depiction.API.HelperObjects
{
    public class DepictionValidationResult
    {
        public ValidationResultValues ValidationOutput = ValidationResultValues.Valid;
        public string Message = string.Empty;

        #region Constructor

        public DepictionValidationResult()
        {
            ValidationOutput = ValidationResultValues.Valid;
        }
        public DepictionValidationResult(ValidationResultValues result, string message)
        {
            ValidationOutput = result;
            Message = message;
        }

        #endregion
    }
    public enum ValidationResultValues
    {
        Valid,
        InValid,
        NoChange
    }
}