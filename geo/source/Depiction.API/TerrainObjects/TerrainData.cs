﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Media3D;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ExtensionMethods;
using Depiction.Serialization;
using DepictionTerrainLayer;

namespace Depiction.API.TerrainObjects
{
    public class TerrainData : IXmlSerializable
    {
        #region Variables

        private TerrainLayer terrainLayer;
        private bool hasGoodData;

        #endregion

        #region Properties

        public bool HasGoodData
        {
            get { return hasGoodData; }
            set { hasGoodData = value; }
        }

        #endregion

        #region Constructor

        public TerrainData()
        {
            terrainLayer = new TerrainLayer();
        }

        #endregion

        #region Methods

        private bool LoadElevationFile(string filename)
        {
            bool downSampled;
            return Add(filename, "", out downSampled, "m", false, false);
        }

        public bool Add(string filename, string coordinateSystem, out bool downSampled, string elevationUnit, bool useExistingGridSpacing, bool cropGridBeforeProjection)
        {
            //get the file extension and pass it on too
            string extension = GetExtension(filename);

            if (extension == null) extension = "";

            //if elevation data already exists, it is in METERS by default
            //now, if we're trying to add new elevation from a file that is in FEET,
            //we need to convert the existing data to FEET also
            if (elevationUnit.Equals("ft")) terrainLayer.Scale(3.28f);

            hasGoodData = terrainLayer.ImportFromFile(filename, coordinateSystem, extension, useExistingGridSpacing, cropGridBeforeProjection);

            //scale to METERS if data in new file was in FEET
            if (elevationUnit.Equals("ft")) terrainLayer.Scale(1 / 3.28f);

            downSampled = terrainLayer.HasBeenDownSampled();

            return hasGoodData;
        }

        public bool AddFromFileWithoutExtents(string filename, double north, double south, double east, double west)
        {
            //convert to geotiff first
            //lock (terrainLayer)
            //{
            return terrainLayer.ImportFromFileWithoutExtents(filename, north, south, east, west);
            //}
        }

        private static string GetExtension(string s)
        {
            int startIndex = s.IndexOf(".");
            if (startIndex < 0) return null;

            int numChars = s.Length - startIndex - 1;
            return s.Substring(startIndex + 1, numChars);
        }

        /// <summary>
        /// Crops coverage data to a supplied bounding box extent
        /// </summary>
        /// <param name="extent">bounding box extent in geographic coordinates</param>
        public void CropElevationData(IMapCoordinateBounds extent)
        {
            lock (terrainLayer)
            {
                terrainLayer.CropElevationGrid(extent.Left, extent.Top, extent.Right, extent.Bottom);
            }
        }

        /// <summary>
        /// Returns true if coverage data is valid at a given latlong point
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public bool HasElevation(ILatitudeLongitude location)
        {
            if (terrainLayer == null) return false;
            lock (terrainLayer)
            {
                return terrainLayer.HasElevation(location.Longitude, location.Latitude);
            }
        }

        public bool HasNoData(ILatitudeLongitude location)
        {
            lock (terrainLayer)
            {
                return terrainLayer.HasNoData(location.Longitude, location.Latitude);
            }
        }

        /// <summary>
        /// Returns elevation value of a given location using interpolation
        /// </summary>
        public float GetInterpolatedElevationValue(ILatitudeLongitude location)
        {
            return terrainLayer.GetInterpolatedElevationValue(location.Longitude, location.Latitude);
        }

        public double GetGridWidthInMeters()
        {
            ILatitudeLongitude topleft = GetTopLeftPos();
            ILatitudeLongitude bottomRight = GetBottomRightPos();

            lock (terrainLayer)
            {
                int widthInPixels = terrainLayer.GetWidthInPixels();
                int heightInPixels = terrainLayer.GetHeightInPixels();

                double diagDistanceInPixels = Math.Sqrt(Math.Pow(widthInPixels, 2) + Math.Pow(heightInPixels, 2));
                double diagDistanceInMeters = topleft.DistanceTo(bottomRight, MeasurementSystem.Metric,MeasurementScale.Normal);
                return (diagDistanceInMeters / diagDistanceInPixels);
            }
        }

        public int GetGridWidthInPixels()
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetWidthInPixels();
            }
        }

        public int GetGridHeightInPixels()
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetHeightInPixels();
            }
        }

        public double GetLatitude(int row)
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetLatitude(row);
            }
        }

        public double GetLongitude(int col)
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetLongitude(col);
            }
        }

        public int GetRow(double lat)
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetRow(lat);
            }
        }

        public int GetColumn(double lon)
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetColumn(lon);
            }
        }

        public ILatitudeLongitude GetTopLeftPos()
        {
            var corners = new ArrayList();

            terrainLayer.GetCorners(corners, true);

            //left, top, right, and bottom in that order
            var topleft = new LatitudeLongitudeBase((double)corners[1], (double)corners[0]);
            return topleft;
        }

        public ILatitudeLongitude GetBottomRightPos()
        {
            var corners = new ArrayList();

            terrainLayer.GetCorners(corners, true);

            //left, top, right, and bottom in that order
            var bottomRight = new LatitudeLongitudeBase((double)corners[3], (double)corners[2]);
            return bottomRight;
        }

        ///<summary>
        /// Get projection system of this grid data in WKT format
        /// </summary>
        public string GetProjectionInWkt()
        {
            lock (terrainLayer)
            {
                return terrainLayer.GetProjectionWkt();
            }
        }

        /// <summary>
        /// Create a grid from scattered data points
        /// Save colormapped raster image equivalent to a jpeg format
        /// </summary>
        /// <param name="filename">file to save the raster colormapped image to</param>
        /// <param name="quality">jpeg image quality -- 0 to 100</param>
        /// <param name="colormapFileName"></param>
        /// <returns></returns>
        public bool SaveTo24BitRGB(string filename, int quality, string colormapFileName)
        {
            return terrainLayer.SaveTo24BitRGB(filename, quality, colormapFileName);
        }

        public bool SaveToBT(string filename)
        {
            return terrainLayer.SaveToBT(filename);
        }

        /// <summary>
        /// Returns coverage value given grid row and column coordinates
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public float GetElevationValueFromGridCoordinate(int col, int row)
        {
            lock (terrainLayer)
            {
                return UnlockedGetElevationValueFromGridCoordinate(col, row);
            }
        }

        public List<Point3D> FindDifferenceWith(TerrainData terrainData)
        {
            //I suppose this mehtod could be
            var differences = new List<Point3D>();
            int width = terrainData.GetGridWidthInPixels();
            int height = terrainData.GetGridHeightInPixels();

            if (GetGridWidthInPixels() != width || GetGridHeightInPixels() != height) return differences;
            //fill in the grid with values from terrainData
            //lock (terrainLayer)
            //{
            for (int col = 0; col < width; col++)
                for (int row = 0; row < height; row++)
                {
                    float baseVal = terrainData.UnlockedGetElevationValueFromGridCoordinate(col, row);
                    float val = UnlockedGetElevationValueFromGridCoordinate(col, row);
                    if (val != baseVal)
                        differences.Add(new Point3D(col, row, val));
                }
            return differences;
        }

        ///<summary>
        /// 
        /// </summary>
        public bool CreateFromExisting(TerrainData terrainData)
        {
            if (terrainData == null || !terrainData.HasGoodData) return false;

            double south, north, east, west;
            ILatitudeLongitude topLeft = terrainData.GetTopLeftPos();
            ILatitudeLongitude botRight = terrainData.GetBottomRightPos();
            south = botRight.Latitude;
            north = topLeft.Latitude;
            east = botRight.Longitude;
            west = topLeft.Longitude;

            //existing grid is always in Mercator since Depiction's internal proj system is Mercator
            bool success = CreateFromExisting(south, north, east, west, terrainData.GetGridWidthInPixels(), terrainData.GetGridHeightInPixels(), terrainData.IsFloatMode(), -32768, "", 0);
            if (success)
            {
                int width = terrainData.GetGridWidthInPixels();
                int height = terrainData.GetGridHeightInPixels();
                //fill in the grid with values from terrainData
                //lock (terrainLayer)
                //{
                for (int col = 0; col < width; col++)
                    for (int row = 0; row < height; row++)
                    {
                        float val = terrainData.UnlockedGetElevationValueFromGridCoordinate(col, row);
                        UnlockedSetElevationValue(col, row, val);
                    }
                //}
                hasGoodData = true;
            }
            else hasGoodData = false;
            return hasGoodData;
        }

        /// <summary>
        /// Returns coverage value given grid row and column coordinates.
        /// Does NOT lock terrainLayer.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private float UnlockedGetElevationValueFromGridCoordinate(int col, int row)
        {
            return terrainLayer.GetElevationFromGridCoordinates(col, row);
        }

        /// <summary>
        /// Set elevation value for a given row,col of the elevation grid.
        /// Does NOT lock terrainLayer.
        /// 
        /// row, col values are zero-based
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="elevValue"></param>
        /// <returns></returns>
        private bool UnlockedSetElevationValue(int col, int row, float elevValue)
        {
            return terrainLayer.SetElevationValue(col, row, elevValue);
        }

        /// <summary>
        /// creates a new terrainlayer with the geographic corners specified
        /// grid width (in heixels) and height are also specified
        /// </summary>
        /// <param name="south">bottom of grid extent</param>
        /// <param name="north">top of grid extent</param>
        /// <param name="east">right of grid extent</param>
        /// <param name="west">left of grid extent</param>
        /// <param name="width">width in pixels</param>
        /// <param name="height">height in pixels</param>
        /// <param name="floatgrid">whether to create floating point values for coverage data</param>
        /// <param name="defaultValue">default coverage value for the grid</param>
        /// <param name="projFilename">projection system of the grid in WKT</param>
        /// <param name="wktLength">wkt of projection in excess of 256 characters</param>
        /// <returns></returns>
        public bool CreateFromExisting(double south, double north, double east, double west, int width, int height, bool floatgrid, float defaultValue, string projFilename, int wktLength)
        {
            //creates a new terrainlayer with the geographic corners specified
            //grid width (in pixels) and height are also specified
            //
            //floatgrid specifies if the elevation grid will consist of floating point values
            //
            lock (terrainLayer)
            {
                hasGoodData = terrainLayer.Create(south, north, east, west, width, height, floatgrid, defaultValue,
                                                  projFilename, wktLength);
                return hasGoodData;
            }
        }
        ///
        /// <summary>
        /// returns true if the grid has float data
        /// </summary>
        public bool IsFloatMode()
        {
            return terrainLayer.IsFloatMode();
        }

        /// <summary>
        /// Save coverage data grid to a GeoTiff file
        /// </summary>
        /// <param name="fileName">file to save to</param>
        /// <returns></returns>
        public bool SaveToGeoTiff(string fileName)
        {
            lock (terrainLayer)
            {
                return terrainLayer.SaveToGeoTIFF(fileName);
            }
        }

        /// <summary>
        /// Returns elevation value for a given location using nearest neighbor method
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        public float GetClosestElevationValue(ILatitudeLongitude location)
        {
            return terrainLayer.GetClosestElevationValue(location.Longitude, location.Latitude);
        }

        /// <summary>
        /// Return average value of a pixel as defined by the kernel
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="kernel">dimension of a square kernel</param>
        /// <returns></returns>
        public float GetConvolvedValue(int col, int row, int kernel)
        {
            return terrainLayer.GetConvolvedValue(col, row, kernel);
        }
        /// <summary>
        /// Set elevation value for a given latlong location
        /// </summary>
        /// <param name="elevValue"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        public bool SetElevationValue(float elevValue, ILatitudeLongitude location)
        {
            lock (terrainLayer)
            {
                return terrainLayer.SetElevationValue(location.Longitude, location.Latitude, elevValue);
            }
        }

        /// <summary>
        /// Set elevation value for a given row,col of the elevation grid.
        /// 
        /// row, col values are zero-based
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="elevValue"></param>
        /// <returns></returns>
        public bool SetElevationValue(int col, int row, float elevValue)
        {
            lock (terrainLayer)
            {
                return UnlockedSetElevationValue(col, row, elevValue);
            }
        }

        /// <summary>
        /// Returns the width of a coverage grid heixel in meters
        /// </summary>
        /// <returns></returns>
        public double GetGridResolution()
        {
            lock (terrainLayer)
            {
                var cornerPts = new ArrayList();
                terrainLayer.GetCorners(cornerPts, true);
                var tlX = (double)cornerPts[0];
                var tlY = (double)cornerPts[1];
                var brX = (double)cornerPts[2];
                var brY = (double)cornerPts[3];

                var topLeft = new LatitudeLongitudeBase(tlY, tlX);//GRRRR
                var bottomRight = new LatitudeLongitudeBase(brY, brX);

                double distance = topLeft.DistanceTo(bottomRight, MeasurementSystem.Metric,MeasurementScale.Normal);
                int width = terrainLayer.GetWidthInPixels();
                int height = terrainLayer.GetHeightInPixels();
                double diagDistance = Math.Sqrt(width * width + height * height);

                double gridResolution = distance / diagDistance;

                return gridResolution;
            }
        }

        public bool LineOfSight(double x1, double y1, double x2, double y2, ArrayList intersection)
        {
            lock (terrainLayer)
            {
                return terrainLayer.IntersectTwoPoints(intersection, x1, y1, x2, y2);
            }
        }

        #endregion
        public void SetElevationValues(List<Point3D> coordPlusElevation)
        {
            foreach (var val in coordPlusElevation)
            {
                SetElevationValue((int)val.X, (int)val.Y, (float)val.Z);
            }
        }

        #region xm serializaiton

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            var imageSaveDir = SerializationService.SerializedImageFileFolder;
            if(reader.Name.Equals("TerrainData"))//1.2.2 legacy
            {
                reader.ReadStartElement();
                hasGoodData = reader.ReadElementContentAsBoolean("hasData", ns);
                if (!hasGoodData)
                {
                    terrainLayer = new TerrainLayer();
                    return;
                }
                string oldFileName;
                if (reader.Name.Equals("elevationGeoTIFF")) // The name prior to 1.01.c.
                    oldFileName = reader.ReadElementContentAsString("elevationGeoTIFF", ns);
                else if (reader.Name.Equals("elevationFileName"))
                    oldFileName = reader.ReadElementContentAsString("elevationFileName", ns);
                else
                    throw new DepictionDeserializationException("Unexpected name for elevation data in the .dpn file.");
                
                if (!Directory.Exists(imageSaveDir))
                    Directory.CreateDirectory(imageSaveDir);
                imageSaveDir = Deserializers12.BinaryFileFolder;
                string fullSavePathOld = Path.Combine(imageSaveDir, oldFileName);
                LoadElevationFile(fullSavePathOld);
//                if (!Deserializers12.ReadBinaryFile(oldFileName, LoadElevationFile))
//                {
//                    throw new Exception(string.Format("Error in xml deserialization: Could not load terrainData file: \"{0}\"", oldFileName));
//                }
                reader.ReadEndElement();
                return;
            }
            if (reader.Name.Equals("hasData"))
            {
                hasGoodData = reader.ReadElementContentAsBoolean("hasData", ns);
                if (!hasGoodData)
                {
                    terrainLayer = new TerrainLayer();
                    return;
                }
            }
            string fileName;
            if (reader.Name.Equals("elevationGeoTIFF")) // The name prior to 1.01.c.
                fileName = reader.ReadElementContentAsString("elevationGeoTIFF", ns);
            else if (reader.Name.Equals("elevationFileName"))
                fileName = reader.ReadElementContentAsString("elevationFileName", ns);
            else
                throw new DepictionDeserializationException("Unexpected name for elevation data in the .dpn file.");
            imageSaveDir = SerializationService.SerializedImageFileFolder;
            if (!Directory.Exists(imageSaveDir))
                Directory.CreateDirectory(imageSaveDir);
            string fullSavePath = Path.Combine(imageSaveDir, fileName);
            bool loadGood = LoadElevationFile(fullSavePath);
            //reader.ReadEndElement();
        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            if (!hasGoodData)
            {
                writer.WriteElementString("hasData", ns, "false");
                return;
            }
            string fileName = Guid.NewGuid() + ".tiff";
            var imageSaveDir = SerializationService.SerializedImageFileFolder;
            if (!Directory.Exists(imageSaveDir))
                Directory.CreateDirectory(imageSaveDir);
            string fullSavePath = Path.Combine(imageSaveDir, fileName);
            bool goodSave = terrainLayer.SaveToGeoTIFF(fullSavePath);

            writer.WriteElementString("hasData", ns, "true");
            writer.WriteElementString("elevationFileName", ns, fileName);
        }

        #endregion
    }
}