﻿using System;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.OldInterfaces;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.API.StaticAccessors
{
    static public class SpatialOps
    {

        static public bool Intersects(IDepictionGeometry sp1, IDepictionSpatialData sp2)
        {
            if (sp2 is IGridSpatialData)
                return Intersects(sp2 as IGridSpatialData, sp1);
            if (sp2 is IDepictionGeometry)
                return Intersects(sp1, sp2 as IDepictionGeometry);
            throw new Exception("unsupported IDepictionSpatialData type for Intersects");
        }

        static public bool Intersects(IGridSpatialData sp1, IDepictionSpatialData sp2)
        {
            if (sp2 is IGridSpatialData)
                return Intersects(sp2 as IGridSpatialData, sp1);
            if (sp2 is IDepictionGeometry)
                return Intersects(sp1, sp2 as IDepictionGeometry);
            throw new Exception("unsupported IDepictionSpatialData type for Intersects");
        }

        #region public, but not ISpatialOps

        static public bool Intersects(IDepictionGeometry sp1, IDepictionGeometry g)
        {
            bool result = false;
            try
            {
                if (sp1.Geometry is MultiLineString)
                {
                    foreach (var lineString in ((MultiLineString)sp1.Geometry).Geometries)
                    {
                        if (lineString.Intersects(g.Geometry))
                        {
                            result = true;
                            break;
                        }
                    }
                }
                else if (g.Geometry is MultiLineString)
                {
                    return Intersects(g, sp1);
                }
                else
                    result = sp1.Geometry.Intersects(g.Geometry);
            }
            catch (TopologyException ex)
            {
                //DepictionAccess.NotificationService.SendNotification(String.Format("Depiction encountered a geometry topology error. It is possible that the shape under consideration has too many vertices."));
            }
            return result;
        }

        static public bool Intersects(IGridSpatialData sp1, IGridSpatialData sp2)
        {
            return true;
        }

        static private bool Intersects(IGridSpatialData sp1, IDepictionGeometry sp2)
        {
            if (sp2.IsEmpty) return false;
            if (!sp1.BoundingBox.Intersects(sp2)) return false;
            if (sp2.Geometry is IPoint)
            {
                return PointIntersectsGrid(sp1, sp2.Geometry as IPoint);
            }
            if (sp2.Geometry is ILineString)
            {
                return LineStringIntersectsGrid(sp1, sp2.Geometry as ILineString);
            }
            if (sp2.Geometry is IMultiLineString)
            {
                return MultiLineStringIntersectsGrid(sp1, sp2.Geometry as IMultiLineString);
            }

            throw new Exception(string.Format("unsupported geometry type {0} for Intersects", sp2.Geometry.GeometryType));
        }
        #endregion

        private static bool PointIntersectsGrid(IGridSpatialData grid, IPoint point)
        {
            var valueAtPoint = grid.GetValue(point.X,point.Y);//new LatitudeLongitude(point.Y, point.X));
            if (valueAtPoint > 0) return true;
            return false;
        }

        private static bool LineStringIntersectsGrid(IGridSpatialData sp1, ILineString lineString)
        {
            throw new NotImplementedException();
        }

        private static bool MultiLineStringIntersectsGrid(IGridSpatialData sp1, IMultiLineString multiLineString)
        {
            throw new NotImplementedException();
        }
    }
}