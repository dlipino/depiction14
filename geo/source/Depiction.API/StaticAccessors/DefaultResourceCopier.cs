﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Depiction.API.StaticAccessors
{
    public static class DefaultResourceCopier
    {
        /// <summary>
        /// Helper class for loading dml resources from an assembly
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="fileExtension"></param>
        /// <param name="replace"></param>
        /// <param name="directoryPathToCreateIn"></param>
        public static void CopyAssemblyEmbeddedResourcesToDirectory(string assemblyName, string fileExtension, bool replace, string directoryPathToCreateIn)
        {
            if (!Directory.Exists(directoryPathToCreateIn))
            {
                Directory.CreateDirectory(directoryPathToCreateIn);
            }

            var existingFiles = new List<string>(Directory.GetFiles(directoryPathToCreateIn, "*." + fileExtension));
            var elementAndIconAssembly = Assembly.Load(new AssemblyName(assemblyName));
            foreach (string manifestResourceName in elementAndIconAssembly.GetManifestResourceNames())
            {
                if (!manifestResourceName.Contains("." + fileExtension)) continue;
                if (!replace && existingFiles.Contains(manifestResourceName)) continue;

                int bytesRead;
                byte[] bits;

                using (var fileResource = elementAndIconAssembly.GetManifestResourceStream(manifestResourceName))
                {
                    if (fileResource == null) continue;
                    bits = new byte[fileResource.Length];
                    bytesRead = fileResource.Read(bits, 0, (int)fileResource.Length);
                }

                using (var stream = new FileStream(Path.Combine(directoryPathToCreateIn, manifestResourceName), FileMode.Create))
                {
                    stream.Write(bits, 0, bytesRead);
                }
            }
        }

        public static void CopySpecificResourcesToDirectory(Dictionary<string, string> resourceNames, string directoryPath, Assembly assembly)
        {
            foreach (var resourceName in resourceNames)
            {
                using (var fileResource = assembly.GetManifestResourceStream(resourceName.Key))
                {
                    if (fileResource == null) continue;
                    var bits = new byte[fileResource.Length];
                    int bytesRead = fileResource.Read(bits, 0, (int)fileResource.Length);

                    using (var stream = new FileStream(Path.Combine(directoryPath, resourceName.Value), FileMode.Create))
                    {
                        stream.Write(bits, 0, bytesRead);
                    }
                }
            }
        }
        //WOrks within assemblies
        public static void CopyResourceStreamToFile(string assemblyName, string resString, string filename)
        {
            var assemblyLocation = Assembly.Load(new AssemblyName(assemblyName));
            using (Stream resStream = assemblyLocation.GetManifestResourceStream(resString))
            {
                if (resStream != null)
                {
                    var fileStream = new FileStream(filename, FileMode.Create);
                    const int size = 4096;
                    var bytes = new byte[4096];
                    int numBytes;
                    while ((numBytes = resStream.Read(bytes, 0, size)) > 0)
                        fileStream.Write(bytes, 0, numBytes);
                    fileStream.Close();
                }
            }
        }
    }
}