﻿using System;
using System.IO;

namespace Depiction.API.ExtensionMethods
{
    /// <summary>
    /// Extension methods for System.IO.Stream
    /// </summary>
    public static class StreamExtensionMethods
    {
        /// <summary>
        /// Copies one stream to another.
        /// </summary>
        /// <param name="input">The source stream.</param>
        /// <param name="output">The destination stream.</param>
        public static void CopyTo(this Stream input, Stream output)
        {
            byte[] buffer = new byte[32768];
            while (true)
            {
                //This is only here because every so often it would throw an exception and say
                //the the connect was closed by the remote host.
                try
                {
                    int read = input.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                        return;
                    output.Write(buffer, 0, read);
                }
                catch (Exception ex)
                {
                    return;
                }

            }
        }
    }
}
