﻿using System;

namespace Depiction.API.ExtensionMethods
{
    public static class GeneralExtensionMethods
    {
        public static bool ToBool(this object o, bool DefaultValue)
        {
            if (o is bool) return (bool)o;

            try
            {
                if (o == null) return DefaultValue;
                return Convert.ToBoolean(o);
            }
            catch
            {
                return DefaultValue;
            }
        }
    }
}
