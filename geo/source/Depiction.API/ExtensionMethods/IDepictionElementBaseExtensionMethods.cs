using System;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ElementPrototype;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.OldInterfaces;

namespace Depiction.API.ExtensionMethods
{
    public static class IDepictionElementBaseExtensionMethods
    {
        public static string GetDataForPosition(this IDepictionElementBase element, string propertyName, ILatitudeLongitude position)
        {
            object propValue;
            if (element.GetPropertyValue(propertyName, out propValue))
            {
                if (propValue != null && propValue is ICoverage)
                {

                    string dataAtLocation = ((ICoverage)propValue).GetValueForDisplay(position);
                    return dataAtLocation;
                }
            }
            return string.Empty;
        }

        public static DepictionGeometryType[] GetElementZOIDepictionGeometryType(this IDepictionElementBase baseElement)
        {
            var element = baseElement as DepictionPropertyHolderBase;
            if (element == null) return new[] { DepictionGeometryType.Point, DepictionGeometryType.MultiPoint };

            ZOIShapeType zoiType;
            if (element.GetPropertyValue("ZOIShapeType", out zoiType))
            {
                switch(zoiType)
                {
                    case ZOIShapeType.Polygon:
                    case ZOIShapeType.UserPolygon:
                        return new[]{DepictionGeometryType.Polygon,DepictionGeometryType.MultiPolygon};
                    case ZOIShapeType.Line:
                    case ZOIShapeType.UserLine:
                        return new[]{DepictionGeometryType.LineString,DepictionGeometryType.MultiLineString};
                }
            }
            return new[] { DepictionGeometryType.Point, DepictionGeometryType.MultiPoint };
        }


        //Copied and slightly modified from 1.2, used for interaction rule runner (mostly)
        //mostly a hack method
        public static object Query(this IDepictionElementBase elementToQuery, string queryString)
        {
            if (elementToQuery == null)
                return null;

            if (queryString.IndexOf(".") == 0)
            {
                queryString = queryString.Substring(1);

                while (queryString.Contains("."))
                {
                    queryString = queryString.Substring(queryString.IndexOf('.') + 1);
                }

                if (queryString.Trim() == string.Empty)
                    return elementToQuery;
                if (queryString.Trim().Equals("ZOI", StringComparison.InvariantCultureIgnoreCase) ||
                    queryString.Trim().Equals("ZoneOfInfluence", StringComparison.InvariantCultureIgnoreCase))
                {
                    return elementToQuery.ZoneOfInfluence;
                }

                return elementToQuery.GetPropertyByInternalName(queryString).Value;
            }

            return null;
        }
    }
}