﻿using System;
using System.Collections.Specialized;
using System.IO;

namespace Depiction.API.AbstractObjects
{
    //Not a proper abastract class, set up for debug mode be default
    public abstract class ProductInformationBase
    {
        //Semi hack for dealing with product type, for now. Hopefully and internal library will resolve this
        public const string Release = "Depiction14";
        public const string Debug = "DepictionDebug";
        public const string Reader = "DepictionReader";
        public const string Prep = "DepictionPrep";
        public const string DepictionRW = "DepictionRW";
        public const string OsintInformation = "OsintInformation";

        public virtual string ProductType { get { return Debug; } }

        //             private string _productType = Debug;
        //        public string ProductType { get { return _productType; } internal set { _productType = value; } }
        //End hack

        public const string DefaultStoryExtension = ".dpn";
        public virtual string DepictionCopyright { get { return "© 2008-2014 Depiction, Inc. All rights reserved. \"Depiction\" and \"More than Mapping\" are either registered trademarks or trademarks of Depiction, Inc. All rights reserved."; } }
        public string ProductVersion { get { return string.Format("{0}™ {1}", ProductName, VersionInfo.FullAppVersion); } }

        #region Properties that may be different between Release and Debug

        virtual public string DirectoryNameForCompany { get { return "Depiction_Debug"; } }
        virtual public string MetricsServiceName { get { return "DebugMetricsService"; } }//no clue what metricservice name i

        virtual public string QuickstartServiceName { get { return "ReleaseQuickstartService"; } }
        virtual public string LicensingServiceName { get { return "DebugLicensingService"; } }//No clue what this is for
        virtual public string ProductValidatorName { get { return "DebugDepictionProductValidator"; } }//or this
        virtual public string ProcessName { get { return "Depiction.vshost"; } }//TODO there might be issues with this in dev environment

        #endregion
        virtual public StringCollection ProductQuickAddElements
        {
            get { return new StringCollection(); }
        }  

        #region Properties that vary only on _productInformation "brand"

        public virtual string ResourceAssemblyName { get { return string.Empty; } }//Case sensitive
        public virtual string SplashScreenPath { get { return string.Empty; } }
//        public virtual string AppIconResourceName { get { return string.Empty; } }

        public virtual string FilePassword { get { return null; } }
        public virtual string FileExtension { get { return DefaultStoryExtension; } }
        public virtual string ProductName { get { return "Depiction Debug"; } }
        public virtual string ProductNameInternalUsage { get { return "Depiction"; } }
        public virtual string CompanyName { get { return "Depiction, Inc."; } }
        public virtual string SupportEmail { get { return "support@depiction.com"; } }
        public virtual string ProductWebpage { get { return "http://depiction.com"; } }

        public virtual string StoryName { get { return "depiction"; } }
        public virtual string LicenseFileDirectory { get
        {
            var orig =Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction_Inc");
            var mod= Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Depiction Inc");
            if (Directory.Exists(orig)) return orig;
            else return mod;
        } }
        public virtual string LicenseFileName { get { return string.Empty; } }
        public virtual int LicenseActivationID { get { return 5; } }
        public virtual int LicenseRenewalID { get { return 0; } }
        public virtual int ProductID { get { return 1; } }
        public virtual bool RenewableLicense { get { return true; } }
        public virtual string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.", ProductVersion, DepictionCopyright);
            }
        }

        public virtual string AboutFileAssemblyLocation
        {
            get { return "Docs.About.xps"; }
        }
        public virtual string EulaFileAssemblyLocation
        {
            get { return "Docs.DepictionEula.xps"; }
        }
        #endregion
    }

    //Hack
    internal class ProductInformationDefault : ProductInformationBase
    {

    }
}