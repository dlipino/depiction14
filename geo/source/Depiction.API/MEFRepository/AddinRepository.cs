﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;

namespace Depiction.API.MEFRepository
{
    public class AddinRepository : IAddinRepository
    {
        private static AddinRepository instance;
        static public bool IsActive
        {
            get
            {
                if (instance == null) return false;
                return true;
            }
        }
        private AddinRepository()
        {
            //Why was the constructor empty?

            Behaviors = new Dictionary<IBehaviorMetaData, BaseBehavior>();
        }

        public static AddinRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AddinRepository();
                }
                return instance;
            }
        }
        /**
         * This is a general note that should be addressed soon, hopefully. Basically the addin system is still
         * the mish mash that came from doigna port from system.addin. This is a bad thing, a very bad thing. 
         * At this point we syold be able to unify te addins by just splitting them up into importer,exporter,condition
         * and behavior. I think the simplified system should still work with user created addons even if they
         * have some sort of drm. In addition the system will force us to create something more unified and clean 
         * up the addon system for future devs.
         * Although now that i think a little bit more about the system the user created addons and depiction addons,
         * ug nevermind, we need to sit and think about the system.
         * **/
        #region Revised addins

        [ImportMany(typeof(IDepictionAddonBase))]
        private Lazy<IDepictionAddonBase, IBaseMetadata>[] LazyAddons { get; set; }
        public Dictionary<IBaseMetadata, IDepictionAddonBase> Addons { get; private set; }


        [ImportMany(typeof(IDepictionNonDefaultImporter))]
        private Lazy<IDepictionNonDefaultImporter, IDepictionNonDefaultImporterMetadata>[] NativeNonDefaultImporters { get; set; }
        public Dictionary<IDepictionNonDefaultImporterMetadata, IDepictionNonDefaultImporter> NonDefaultImporters { get; private set; }

        [ImportMany(typeof(IDepictionDefaultImporter))]
        private Lazy<IDepictionDefaultImporter, IDepictionDefaultImporterMetadata>[] NativeDefaultImporters { get; set; }
        public Dictionary<IDepictionDefaultImporterMetadata, IDepictionDefaultImporter> DefaultImporters { get; private set; }
        //TODO need to figure some way around this without using #if
#if !PREP
        [ImportMany(typeof(IDepictionElementExporter))]
#endif
        private Lazy<IDepictionElementExporter, IDepictionExporterMetadata>[] NativeElementExporters { get; set; }
        public Dictionary<IDepictionExporterMetadata, IDepictionElementExporter> ElementExporters { get; private set; }

        #endregion

        [ImportMany(typeof(BaseBehavior))]
        private Lazy<BaseBehavior, IBehaviorMetaData>[] NativeBehaviors { get; set; }
        public Dictionary<IBehaviorMetaData, BaseBehavior> Behaviors { get; private set; }

        [ImportMany(typeof(ICondition))]
        private Lazy<ICondition, IConditionMetaData>[] NativeConditions { get; set; }
        public Dictionary<IConditionMetaData, ICondition> Conditions { get; private set; }

        [ImportMany(typeof(IDepictionGeocoder))]
        public Lazy<IDepictionGeocoder, IGeocoderMetadata>[] Geocoders { get; set; }

        //[ImportMany(typeof(IQuickstartItem))]
        public IQuickstartItem[] FileQuickstartItems { get; set; }

        [ImportMany(typeof(ITiler))]
        public ITiler[] Tilers { get; private set; }
        
        public static void Decompose()
        {
            instance = null;
        }
        public static void Compose()
        {
            Instance.Addons = new Dictionary<IBaseMetadata, IDepictionAddonBase>();
            Instance.NonDefaultImporters = new Dictionary<IDepictionNonDefaultImporterMetadata, IDepictionNonDefaultImporter>();
            Instance.DefaultImporters = new Dictionary<IDepictionDefaultImporterMetadata, IDepictionDefaultImporter>();
            Instance.ElementExporters = new Dictionary<IDepictionExporterMetadata, IDepictionElementExporter>();
            Instance.Behaviors = new Dictionary<IBehaviorMetaData, BaseBehavior>();
            Instance.Conditions = new Dictionary<IConditionMetaData, ICondition>();
            Instance.FileQuickstartItems = new List<IQuickstartItem>().ToArray();
            try
            {

                //Get non default addins
                var addinDirectoryThirdParty = string.Empty;
                if(DepictionAccess.PathService != null)
                {
                    addinDirectoryThirdParty = DepictionAccess.PathService.UserInstalledAddinsDirectoryPath;
                }
                var catalog = new AggregateCatalog();
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        addinDirectoryThirdParty = Path.Combine(addinDirectoryThirdParty, "Preparedness Add-on");
                        break;
                }

                if (Directory.Exists(addinDirectoryThirdParty))
                {
                    catalog.Catalogs.Add(new DirectoryCatalog(addinDirectoryThirdParty));
                    var allSubDirs = Directory.GetDirectories(addinDirectoryThirdParty, "*", SearchOption.AllDirectories);
                    foreach (var folder in allSubDirs)
                        catalog.Catalogs.Add(new DirectoryCatalog(folder));
                }
                

                //Get the default
                //There must be a way to make sure the dlls that are getting imported have the correct
                //dll version, ie the addons were created by the version of the API dll that is running
                //the app.
                var addinDirectoryBase = string.Empty;
                if(DepictionAccess.PathService != null)
                {
                    addinDirectoryBase = DepictionAccess.PathService.DepictionAddinPath;
                    catalog.Catalogs.Add(new DirectoryCatalog(addinDirectoryBase));
                }

                var container = new CompositionContainer(catalog);

                container.ComposeParts(Instance);

                if (Instance.LazyAddons == null)
                    Instance.LazyAddons = new Lazy<IDepictionAddonBase, IBaseMetadata>[0];
                if (Instance.NativeNonDefaultImporters == null)
                    Instance.NativeNonDefaultImporters = new Lazy<IDepictionNonDefaultImporter, IDepictionNonDefaultImporterMetadata>[0];
                if (Instance.NativeDefaultImporters == null)
                    Instance.NativeDefaultImporters = new Lazy<IDepictionDefaultImporter, IDepictionDefaultImporterMetadata>[0];
                if (Instance.NativeElementExporters == null)
                    Instance.NativeElementExporters = new Lazy<IDepictionElementExporter, IDepictionExporterMetadata>[0];
                if (Instance.NativeBehaviors == null)
                    Instance.NativeBehaviors = new Lazy<BaseBehavior, IBehaviorMetaData>[0];
                if (Instance.NativeConditions == null)
                    Instance.NativeConditions = new Lazy<ICondition, IConditionMetaData>[0];
                //Get rid of some tile sources to make things easier        
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        var newTileSources = new List<ITiler>();
                        foreach (var tile in Instance.Tilers)
                        {
                            if (!tile.DisplayName.Contains("USGS"))
                            {
                                newTileSources.Add(tile);
                            }
                            else
                            {
                                if (tile.DisplayName.Contains("Topographic"))
                                {
                                    newTileSources.Add(tile);
                                }
                            }
                        }
                        Instance.Tilers = newTileSources.ToArray();
                        break;
                }

                #region Revised addins

                foreach (var addon in Instance.LazyAddons)
                {
                    Instance.Addons.Add(addon.Metadata, addon.Value);
                }

                foreach (var importer in Instance.NativeNonDefaultImporters)
                {
                    if (importer.Value.Activated)
                    {
                        Instance.NonDefaultImporters.Add(importer.Metadata, importer.Value);
                    }
                }

                foreach (var importer in Instance.NativeDefaultImporters)
                {
                    Instance.DefaultImporters.Add(importer.Metadata, importer.Value);
                }

                foreach (var exporter in Instance.NativeElementExporters)
                {
                    Instance.ElementExporters.Add(exporter.Metadata, exporter.Value);
                }

                #endregion

                foreach (var behavior in Instance.NativeBehaviors)
                {
                    Instance.Behaviors.Add(behavior.Metadata, behavior.Value);
                }

                foreach (var condition in Instance.NativeConditions)
                {
                    Instance.Conditions.Add(condition.Metadata, condition.Value);
                }

                //read quickstart items from xml file
                ResetGetAllHardCodedQSSources();
            }
            catch (ReflectionTypeLoadException ex)
            {
                StringBuilder stringBuilder = new StringBuilder("Encountered a ReflectionTypeLoadException while loading Add-ons.  One or more Add-ons may not be available.");
                if (ex.LoaderExceptions != null)
                {
                    foreach (var loaderException in ex.LoaderExceptions)
                    {
                        stringBuilder.Append(string.Format("Loader Exception: {0}",
                                                           DepictionExceptionHandler.
                                                               MessageFromExceptionAndInnerException(loaderException)));
                    }
                }
                DepictionExceptionHandler.HandleException(stringBuilder.ToString(), ex, true, true);
            }

        }
        #region quickstart stuff
        static public void ResetGetAllHardCodedQSSources()
        {
            var fileItemList = ReadQuickStartXmlFile("quickstartitems.xml");
            //var hardCodedList = GetQSItemsFromNonDefaultImporters(Instance.NonDefaultImporters);

            //var allItems = fileItemList.Union(hardCodedList);
            Instance.FileQuickstartItems = fileItemList.ToArray();// allItems.ToArray();
        }
        public static List<IQuickstartItem> ReadQuickStartXmlFile(string xmlPath)
        {
            if (!File.Exists(xmlPath))
            {
                //Instance.FileQuickstartItems = new List<IQuickstartItem>().ToArray();
                return new List<IQuickstartItem>();
            }
            var items = new List<IQuickstartItem>();
            using (var reader = new XmlTextReader(xmlPath))
            {
                while (reader.Read())
                {
                    if (reader.NodeType != XmlNodeType.Element) continue;
                    switch (reader.Name)
                    {
                        case "item":
                            var item = GetQSItem(reader);
                            items.Add(item);
                            break;
                    }
                }
            }
            //            if (items.Count > 0)
            //                Instance.FileQuickstartItems = items.ToArray();
            return items;

        }

        //I think this is the way all xml read should be done. It seems to be slight more robust
        //than the way most depiction types do their reading. 
        //(most guess at the order of elements and hope for the best) 
        private static IQuickstartItem GetQSItem(XmlTextReader reader)
        {
            IQuickstartItem item = new QuickstartItem();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "item")
                {
                    return item;
                }
                if (reader.NodeType == XmlNodeType.EndElement) continue;

                switch (reader.Name)
                {
                    case "name":
                        item.Name = reader.ReadString();
                        break;
                    case "description":
                        item.Description = reader.ReadString();
                        break;
                    case "addinname":
                        item.AddinName = reader.ReadString();
                        break;
                    case "parameters":
                        var parameters = GetParameters(reader);
                        item.Parameters = parameters;
                        break;
                }
            }

            return null;
        }
        #endregion
        private static Dictionary<string, string> GetParameters(XmlTextReader reader)
        {
            var parameters = new Dictionary<string, string>();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "parameters")
                {
                    return parameters;
                }
                if (reader.NodeType == XmlNodeType.EndElement) continue;
                if (reader.Name == "parameter")
                {
                    var attrib = reader.GetAttribute("key");
                    var value = reader.GetAttribute("value");
                    parameters.Add(attrib, value);
                }
            }
            return null;
        }

        public List<object> GetAllAddinMetadata()
        {
            var defaultMetadata = GetDefaultAddinMetadata();
            var thirdPartyMetadata = Get3rdPartyAddinMetadata();

            var allMetadatas = thirdPartyMetadata.Concat(defaultMetadata);
            return allMetadatas.ToList();
        }
        public List<object> Get3rdPartyAddinMetadata()
        {
            var metadatas = new List<object>();
            foreach (var addin in NonDefaultImporters)
            {
                metadatas.Add(addin.Key);
            }
            return metadatas;
        }
        public List<object> GetDefaultAddinMetadata()
        {
            var metadatas = new List<object>();
            if (Geocoders != null)
            {
                foreach (var addin in Geocoders)
                {
                    metadatas.Add(addin.Metadata);
                }
            }
            foreach (var addin in DefaultImporters)
            {
                metadatas.Add(addin.Key);
            }
            foreach (var addin in ElementExporters)
            {
                metadatas.Add(addin.Key);
            }
            foreach (var addin in Behaviors)
            {
                metadatas.Add(addin.Key);
            }
            foreach (var addin in Conditions)
            {
                metadatas.Add(addin.Key);
            }
            if (Tilers != null)
            {
                foreach (var addin in Tilers)
                {
                    metadatas.Add(addin);
                }
            }

            return metadatas;
        }


        public Dictionary<IDepictionDefaultImporterMetadata, IDepictionDefaultImporter> GetDefaultImporters()
        {
            return DefaultImporters;
        }
        public Dictionary<IDepictionNonDefaultImporterMetadata, IDepictionNonDefaultImporter> GetNonDefaultImporters()
        {
            return NonDefaultImporters;
        }
        public Dictionary<IDepictionExporterMetadata, IDepictionElementExporter> GetExporters()
        {
            return ElementExporters;
        }

        public Dictionary<IBehaviorMetaData, BaseBehavior> GetBehaviors()
        {
            return Behaviors;
        }

        public Dictionary<IConditionMetaData, ICondition> GetConditions()
        {
            return Conditions;
        }

        public IDepictionImporterBase GetElementImporterByName(string elementImporter)
        {
            foreach (var importer in DefaultImporters)
            {
                if (importer.Key.Name.ToLower().Equals(elementImporter.ToLower()))
                    return importer.Value;
            }

            foreach (var importer in NonDefaultImporters)
            {
                if (importer.Key.Name.ToLower().Equals(elementImporter.ToLower()))
                    return importer.Value;
            }
            return null;
        }

        public IDepictionGeocoder GetGeocoder(string geocoderName)
        {
            if (Geocoders == null) return null;
            foreach (var geocoder in Geocoders)
            {
                if (geocoder.Metadata.Name.ToLower().Equals(geocoderName.ToLower()))
                    return geocoder.Value;
            }
            return null;
        }
    }


}