using System;
using System.Collections.Generic;
using Depiction.API.Interfaces;

namespace Depiction.API.MEFRepository
{
    public class QuickstartItem : IQuickstartItem
    {
        public string Name
        {
            get;
            set;
        }

        public string AddinName
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Dictionary<string, string> Parameters
        {
            get;
            set;
        }

        public string ElementType
        {
            get;
            set;
        }
    }
    public class QuickStartItemNameTypeComparer<T> : IEqualityComparer<T> where T : IQuickstartItem
    {
        public bool Equals(T x, T y)
        {
            if(!string.Equals(x.Name, y.Name, StringComparison.CurrentCultureIgnoreCase)) return false;
            if (!string.Equals(x.AddinName, y.AddinName, StringComparison.CurrentCultureIgnoreCase)) return false;
            return true;
        }

        public int GetHashCode(T obj)
        {
            int hashCode = obj.Name.GetHashCode() >> 3;
            hashCode ^= obj.AddinName.GetHashCode();
            return hashCode;
        }
    }
}