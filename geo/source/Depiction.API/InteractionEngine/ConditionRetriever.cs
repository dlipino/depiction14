﻿using System.Windows;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.Interfaces;

namespace Depiction.API.InteractionEngine
{
    public static class ConditionRetriever
    {
        public static ICondition RetrieveCondition(string conditionName)
        {

            var app = Application.Current as IDepictionApplication;
            if (app == null) return null;

            var conditions = app.Repository.GetConditions();

            foreach (var condition in conditions)
            {
                if (condition.Key.ConditionName.Equals(conditionName))
                    return condition.Value;
            }
            return null;
           
        }
    }
}
