﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.Interfaces;
using Depiction.Serialization;

namespace Depiction.API.InteractionEngine
{

    [XmlRoot(ElementName = "Condition")]
    public class ConditionSerializer : IXmlSerializable
    {
        public string Name { get; private set; }
        public HashSet<string> ConditionTriggerProperties { get; private set; }


        public ConditionSerializer(string name, HashSet<string> triggerProps)
        {
            ConditionTriggerProperties = triggerProps;
            Name = name;
        }
        public ConditionSerializer(ICondition toSaveCondition)
        {
            ConditionTriggerProperties = toSaveCondition.ConditionTriggerProperties;
            Name = toSaveCondition.Name;
        }
        //
        public ConditionSerializer()
        { }

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            if (!reader.Name.Equals("Condition")) return;
            reader.Read();
            if(reader.Name.Equals("Name"))
            {
                Name = reader.ReadElementContentAsString();
            }
            ConditionTriggerProperties = new HashSet<string>();
            if(reader.Name.Equals("ConditionTriggerProperties"))
            {
                reader.Read();
                while(reader.Name.Equals("Property"))
                {
                    ConditionTriggerProperties.Add(reader.GetAttribute("name").Trim());
                    reader.Read();
                }
                reader.Read();
            }
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
            //            var ns = SerializationConstants.DepictionXmlNameSpace;//why is this used?
            writer.WriteStartElement("Condition");
            writer.WriteElementString("Name", Name);
            if (ConditionTriggerProperties.Count > 0)
            {
                writer.WriteStartElement("ConditionTriggerProperties");
                foreach (var triggerProp in ConditionTriggerProperties)
                {
                    writer.WriteStartElement("Property");
                    writer.WriteAttributeString("name", triggerProp);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
    }
}
