﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.ValueTypes;
using Depiction.API.ExtensionMethods;
using Depiction.Serialization;

namespace Depiction.API.InteractionEngine
{
    public class InteractionRule : IInteractionRule, IXmlSerializable, IEqualityComparer<IInteractionRule>
    {
        string sessionIdentifier = Guid.NewGuid().ToString();
        #region constructors

        public InteractionRule()
        {
            Subscribers = new List<string>();
            Description = string.Empty;
            Author = string.Empty;
            Name = string.Empty;
            IsDisabled = false;
        }

        #endregion

        #region fields

        private readonly Dictionary<string, IElementFileParameter[]> behaviors = new Dictionary<string, IElementFileParameter[]>();
        private readonly Dictionary<string, HashSet<string>> conditions = new Dictionary<string, HashSet<string>>();
        private readonly HashSet<string> publisherTriggerPropertyNames = new HashSet<string>();
        private readonly HashSet<string> subscriberTriggerPropertyNames = new HashSet<string>();

        public Dictionary<string, HashSet<string>> elementAndPropertyTriggerDictionary = new Dictionary<string, HashSet<string>>();

        #endregion

        #region properties

        public string UniqueTempSessionId
        {
            get { return sessionIdentifier; }
            private set { sessionIdentifier = value; }//Used for cloning
        }

        public bool IsDefaultRule { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public bool IsDisabled { get; set; }

        public string Publisher { get; set; }
        public HashSet<string> PublisherTriggerPropertyNames
        {
            get { return publisherTriggerPropertyNames; }
        }
        public Dictionary<string, HashSet<string>> Conditions
        {
            get { return conditions; }
        }

        public List<string> Subscribers { get; private set; }
        public HashSet<string> SubscriberTriggerPropertyNames
        {
            get { return subscriberTriggerPropertyNames; }
        }

        public Dictionary<string, IElementFileParameter[]> Behaviors
        {
            get { return behaviors; }
        }

        #endregion

        #region methods

        protected void SetupElementAndTriggerDictionary()
        {
            elementAndPropertyTriggerDictionary.Clear();
            if (Publisher == null) return;
            elementAndPropertyTriggerDictionary.Add(Publisher, PublisherTriggerPropertyNames);
            foreach (var subscriber in Subscribers)
            {
                var allProps = new HashSet<string>();
                foreach (var prop in subscriberTriggerPropertyNames)
                {
                    allProps.Add(prop);
                }
                foreach (var list in Conditions.Values)
                {//COmbine all the condition triggers

                    foreach (var prop in list)
                    {
                        allProps.Add(prop);
                    }
                }
                if (elementAndPropertyTriggerDictionary.ContainsKey(subscriber))
                {
                    foreach (var prop in allProps) elementAndPropertyTriggerDictionary[subscriber].Add(prop);
                }
                else
                {
                    elementAndPropertyTriggerDictionary.Add(subscriber, allProps);
                }
            }
        }

        public IInteractionRule MakeFunctionalCopyOfInteractionRule()
        {
            var stream = new MemoryStream();
            SerializationService.SerializeObject("InteractionRule", this, typeof(InteractionRule), stream);
            stream.Position = 0;
            return (InteractionRule)SerializationService.DeserializeObject("InteractionRule", typeof(InteractionRule), stream);
        }

        public void ExecuteRule(IDepictionElement publisher, IDepictionElement subscriber)
        {
            if (Behaviors != null)
                foreach (var behavior in Behaviors)
                {

                    BaseBehavior behaviorToDo;
                    try
                    {
                        behaviorToDo = BehaviorRetriever.RetrieveBehavior(behavior.Key);
                    }
                    catch (Exception ex)
                    {
                        //Silently swallow deprecated behaviors
                        continue;
                    }

                    Debug.Assert(behaviorToDo != null, string.Format("Could not find expected behavior: {0}", behavior.Key));
                    if (behaviorToDo == null)
                        continue;
                    var parameters = BuildParameters((behaviorToDo.Parameters != null) ? behaviorToDo.Parameters.Select(p => p.ParameterType) : null, behavior.Value, publisher, subscriber);
                    BehaviorResult result;
                    try
                    {
                        result = behaviorToDo.DoBehavior(subscriber, parameters);
                    }
                    catch (Exception ex)
                    {
                        DepictionAccess.NotificationService.DisplayMessageString(
                            string.Format("The \"{0}\" interaction could not run, because: {1}.", Name, ex.Message));
                        break;
                    }
                    //                    if (result.SubscriberHasChanged)//TODO may have bit of more than i can chew
                    //                        subscriber.UpdateInteractions();
                }
        }

        public bool EvaluateConditions(IDepictionElement publisher, IDepictionElement subscriber)
        {
            if (conditions != null)
                foreach (var conditionPair in conditions)
                {
                    ICondition condition = ConditionRetriever.RetrieveCondition(conditionPair.Key);
#if DEBUG
                    Debug.Assert(condition != null, string.Format("Could not find expected condition: {0}", conditionPair.Key));
#endif
                    if (condition == null) continue;
                    //var parameters =  BuildParameters(condition.Parameters.Select(c => c.ParameterType), conditionPair.Value, publisher, subscriber);
                    if (!condition.Evaluate(publisher, subscriber, null))
                        return false;
                }
            return true;
        }
        public static object[] BuildParametersForPostSetActions(IEnumerable<string> inParameters)
        {
            var outParameters = new List<object>();
            if (inParameters == null) return outParameters.ToArray();
            foreach (var param in inParameters)
            {
                var split = param.Split(':');
                if (split.Length == 2)
                {
                    outParameters.Add(split[0]);
                    outParameters.Add(split[1]);
                }
                else
                {
                    outParameters.Add(param);
                }
            }

            return outParameters.ToArray();
        }
        public static object[] BuildParameters(IEnumerable<Type> parameterTypes, IEnumerable<IElementFileParameter> parameters, IDepictionElement publisher, IDepictionElement subscriber)
        {
            var outParameters = new List<object>();

            if (parameters != null)
                for (int i = 0; i < parameters.Count(); i++)
                {
                    IElementFileParameter property = parameters.ElementAt(i);

                    var query = property.ElementQuery;

                    switch (property.Type)
                    {
                        //SO much hacking
                        case ElementFileParameterType.Publisher:
                            if (parameterTypes.ElementAt(i).Equals(typeof(string)))
                            {
                                outParameters.Add(query);
                            }
                            else
                            {
                                outParameters.Add(publisher.Query(query));
                            }
                            break;
                        case ElementFileParameterType.Subscriber:
                            if (parameterTypes.ElementAt(i).Equals(typeof(string)))
                            {
                                outParameters.Add(query);
                            }
                            else
                            {
                                outParameters.Add(subscriber.Query(query));
                            }

                            break;
                        case ElementFileParameterType.Value:
                            try
                            {
                                outParameters.Add(query);
                            }
                            catch
                            {
                                throw new Exception("Cannot convert behavior value to appropriate type!");
                            }
                            break;
                    }
                }

            return outParameters.ToArray();
        }

        #endregion

        #region deep clone
        public IInteractionRule DeepClone()
        {
            var cloneWithMatchingId = (InteractionRule)MakeFunctionalCopyOfInteractionRule();
            cloneWithMatchingId.UniqueTempSessionId = UniqueTempSessionId;
            cloneWithMatchingId.IsDefaultRule = IsDefaultRule;
            return cloneWithMatchingId;
        }
        object IDeepCloneable.DeepClone()
        {
            return DeepClone();
        }
        #endregion
        #region A bunch of equals and equals helpers
        bool IEqualityComparer<IInteractionRule>.Equals(IInteractionRule x, IInteractionRule y)
        {
            return Equals(x, y);
        }

        public static bool KeyEquals(IInteractionRule one, IInteractionRule other)
        {
            string oneId = null;
            string otherId = null;
            if (one != null) oneId = one.UniqueTempSessionId;
            if (other != null) otherId = other.UniqueTempSessionId;
            if (string.IsNullOrEmpty(oneId) || string.IsNullOrEmpty(otherId)) return false;
            return Equals(oneId, otherId);
        }

        public bool KeyEquals(IInteractionRule other)
        {
            return KeyEquals(this, other);
        }
        public bool RuleEquals(IInteractionRule other)
        {
            if (!(other != null
                   && Equals(Name, other.Name)
                   && Equals(Description, other.Description)
                   && Equals(Author, other.Author)))
            {
                return false;
            }
            if (!FunctionallyEquals(other)) return false;
            return true;
        }
        public bool FunctionallyEquals(IInteractionRule other)
        {
            if (other == null || !Equals(Publisher, other.Publisher))
                return false;

            //                || !Subscribers.Equals(other.Subscribers)
            if (Subscribers.Count != other.Subscribers.Count) return false;
            for (int i = 0; i < Subscribers.Count; i++)
            {
                if (!Subscribers[i].Equals(other.Subscribers[i])) return false;
            }
            if (!IsParamDictionaryEqual(Behaviors, other.Behaviors)) return false;

            if (Conditions.Count != other.Conditions.Count) return false;
            foreach (var key in Conditions.Keys)
            {
                if (!other.Conditions.ContainsKey(key)) return false;
                var val1 = Conditions[key];
                var val2 = other.Conditions[key];
                if (!val1.SetEquals(val2)) return false;
            }
            return true;
        }

        private static bool IsParamDictionaryEqual(Dictionary<string, IElementFileParameter[]> paramDictionary, IDictionary<string, IElementFileParameter[]> paramDictionary2)
        {
            if (paramDictionary.Count != paramDictionary2.Count) return false;
            foreach (var key in paramDictionary.Keys)
            {
                if (!paramDictionary2.ContainsKey(key)) return false;
                var val1 = paramDictionary[key];
                var val2 = paramDictionary2[key];
                if (val1.Length != val2.Length) return false;
                for (int i = 0; i < val1.Length; i++)
                {
                    if (!val1[i].Equals(val2[i])) return false;
                }
            }
            return true;
        }

        public override bool Equals(object obj)
        {
            return RuleEquals(obj as InteractionRule);
            //            return KeyEquals(this, obj as InteractionRule);
        }

        public static bool Equals(IInteractionRule one, IInteractionRule other)
        {
            return one.RuleEquals(other as InteractionRule);
            //return KeyEquals(one, other);
        }

        public int GetHashCode(IInteractionRule obj)
        {
            var hc = obj.Publisher.GetHashCode();
            foreach (var pTrigger in obj.PublisherTriggerPropertyNames)
            {
                hc = (hc << 1) ^ (pTrigger.GetHashCode());
            }
            foreach (var condition in obj.Conditions)
            {
                hc = (hc << 1) ^ condition.Key.GetHashCode();
                foreach (var val in condition.Value)
                {
                    hc = (hc << 1) ^ val.GetHashCode();
                }
            }
            foreach (var behavior in obj.Behaviors)
            {
                hc = (hc << 1) ^ behavior.Key.GetHashCode();
            }
            foreach (var subscriber in Subscribers)
            {
                hc = (hc << 1) ^ subscriber.GetHashCode();
            }
            foreach (var pTrigger in obj.SubscriberTriggerPropertyNames)
            {
                hc = (hc << 1) ^ (pTrigger.GetHashCode());
            }

            return hc;
        }

        public override int GetHashCode()
        {
            return GetHashCode(this);
        }

        #endregion

        #region xml serialization

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public static void SaveRulesToXmlFile(IEnumerable<IInteractionRule> rules, string path)
        {
            using (var stream = new FileStream(path, FileMode.Create))
            {
                SaveRulesToXmlStream(stream, rules);
            }
        }

        public static void SaveRulesToXmlStream(Stream stream, IEnumerable<IInteractionRule> rules)
        {
            // Use invariant format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var writer = SerializationService.GetXmlWriter(stream))
                //                using (var writer = XmlDictionaryWriter.CreateTextWriter(stream, Encoding.UTF8, false))
                {
                    SerializationService.SerializeItemList("InteractionRules", typeof(InteractionRule), rules, writer, true);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }

        public static IList<IInteractionRule> LoadRulesFromXmlFile(string filePath)
        {
            using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                return LoadRulesFromXmlStream(stream);
            }
        }

        public static IList<IInteractionRule> LoadRulesFromDirectory(string path)
        {
            var rules = new List<IInteractionRule>();
            var files = Directory.GetFiles(path, "*.xml", SearchOption.TopDirectoryOnly);
            foreach (var file in files)
            {
                var interactionRules = LoadRulesFromXmlFile(file);
                if (interactionRules != null)
                    rules.AddRange(interactionRules);
            }

            return rules;
        }

        public static IList<IInteractionRule> LoadRulesFromXmlStream(Stream stream)
        {
            // Use en-US format for persisting numbers and datetimes.
            CultureInfo culture = Thread.CurrentThread.CurrentCulture;
            try
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                using (var reader = SerializationService.GetXmlReader(stream))
                {
                    return SerializationService.DeserializeItemList<IInteractionRule>("InteractionRules",
                                                                                      typeof(InteractionRule), reader);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = culture;
            }
        }


        public void ReadXml(XmlReader reader)
        {
            if (!reader.Name.Equals("InteractionRule")) return;
            var xmlDictionary = new Dictionary<string, string>();

            reader.ReadStartElement();
            //Hack
            var publisherType = string.Empty;
            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("InteractionRule", StringComparison.InvariantCultureIgnoreCase))
            {
                var name = reader.Name;
                if(name.Equals("Publisher"))
                {
                    publisherType = reader.GetAttribute("Type");
                }
                xmlDictionary.Add(name, reader.ReadInnerXml());
            }

            if (xmlDictionary.ContainsKey("Name")) { Name = xmlDictionary["Name"]; }
            if (xmlDictionary.ContainsKey("Description")) { Description = xmlDictionary["Description"]; }
            if (xmlDictionary.ContainsKey("Author")) { Author = xmlDictionary["Author"]; }

            if (xmlDictionary.ContainsKey("Publisher"))
            {
                var stringReader = new StringReader(xmlDictionary["Publisher"]);
                var pubReader = XmlReader.Create(stringReader);
                if (publisherType == null)
                {
                    Publisher = xmlDictionary["Publisher"];
                }
                else
                {
                    Publisher = publisherType;
                    pubReader.Read();
                    publisherTriggerPropertyNames.Clear();
                    if (pubReader.Name.Equals("PublisherTriggerProperties", StringComparison.InvariantCultureIgnoreCase))
                    {
                        pubReader.Read();
                        while (pubReader.Name.Equals("Property"))
                        {
                            publisherTriggerPropertyNames.Add(pubReader.GetAttribute("name"));
                            pubReader.Read();
                        }
                        pubReader.Read();
                    }
                    pubReader.Read();
                }
            }
            if (xmlDictionary.ContainsKey("Conditions"))
            {
                var rawString = xmlDictionary["Conditions"];

                var finalXML = "<Conditions>" + rawString + "</Conditions>";
                var stringReader = new StringReader(finalXML);
                var pubReader = XmlReader.Create(stringReader);
                var conditionList = SerializationService.DeserializeItemList<ConditionSerializer>("Conditions", typeof(ConditionSerializer), pubReader);
                conditions.Clear();
                foreach (var condition in conditionList)
                    conditions.Add(condition.Name, condition.ConditionTriggerProperties);
            }
            if (xmlDictionary.ContainsKey("Subscribers"))
            {
                var rawString = xmlDictionary["Subscribers"];
                var finalXML = "<Subscribers>" + rawString + "</Subscribers>";
                var stringReader = new StringReader(finalXML);
                var subscriberReader = XmlReader.Create(stringReader);

                subscriberReader.Read();
                subscriberReader.Read();
                while (subscriberReader.Name.Equals("String"))
                {
                    Subscribers.Add(subscriberReader.ReadElementString("String"));
                }
                if (subscriberReader.Name.Equals("SubscriberTriggerProperties", StringComparison.InvariantCultureIgnoreCase))
                {
                    subscriberReader.Read();
                    while (subscriberReader.Name.Equals("Property"))
                    {
                        subscriberTriggerPropertyNames.Add(subscriberReader.GetAttribute("name"));
                        subscriberReader.Read();
                    }
                    subscriberReader.Read();
                }
                subscriberReader.Read();
            }
            //Subscribers = new List<string>(SerializationService.DeserializeItemList<string>("Subscribers", typeof(string), reader));
            if (xmlDictionary.ContainsKey("Behaviors"))
            {
                var rawString = xmlDictionary["Behaviors"];
                var finalXML = "<Behaviors>" + rawString + "</Behaviors>";
                var stringReader = new StringReader(finalXML);
                var behaviorReader = XmlReader.Create(stringReader);
                var behaviorsList = SerializationService.DeserializeItemList<BehaviorSerializer>("Behaviors", typeof(BehaviorSerializer), behaviorReader);
                behaviors.Clear();
                foreach (var behavior in behaviorsList)
                    behaviors.Add(behavior.Name, behavior.Parameters.ToArray());
            }

            while (!reader.NodeType.Equals(XmlNodeType.EndElement) || !reader.Name.Equals("InteractionRule", StringComparison.InvariantCultureIgnoreCase))
            {
                Debug.WriteLine(reader.Name + " reading extra for interaction rule");
                reader.Read();
            }
            reader.ReadEndElement();
            SetupElementAndTriggerDictionary();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("InteractionRule");
            writer.WriteElementString("Name", Name);
            writer.WriteElementString("Description", Description);
            writer.WriteElementString("Author", Author);

            writer.WriteStartElement("Publisher");
            writer.WriteAttributeString("Type", Publisher);

            if (publisherTriggerPropertyNames.Count != 0)
            {
                writer.WriteStartElement("PublisherTriggerProperties");
                foreach (var propName in publisherTriggerPropertyNames)
                {
                    writer.WriteStartElement("Property");
                    writer.WriteAttributeString("name", propName);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            var conditionList = new List<ConditionSerializer>();
            foreach (var key in conditions.Keys)
                conditionList.Add(new ConditionSerializer(key, conditions[key]));
            SerializationService.SerializeItemList("Conditions", typeof(ConditionSerializer), conditionList, writer);

            writer.WriteStartElement("Subscribers");
            foreach (var subscriber in Subscribers)
            {
                writer.WriteElementString("String", subscriber);
            }
            if (subscriberTriggerPropertyNames.Count != 0)
            {
                writer.WriteStartElement("SubscriberTriggerProperties");
                foreach (var propName in subscriberTriggerPropertyNames)
                {
                    writer.WriteStartElement("Property");
                    writer.WriteAttributeString("name", propName);
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            //            SerializationService.SerializeItemList("Subscribers", typeof(string), Subscribers, writer);
            var behaviorList = new List<BehaviorSerializer>();
            foreach (var key in behaviors.Keys)
                behaviorList.Add(new BehaviorSerializer(key, behaviors[key]));

            SerializationService.SerializeItemList("Behaviors", typeof(BehaviorSerializer), behaviorList, writer);
            writer.WriteEndElement();
        }

        #endregion

    }
    /*
     *         public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            reader.ReadStartElement();
            Publisher = reader.ReadElementContentAsString("Publisher", ns);          
            Name = reader.ReadElementContentAsString("Name", ns);

            Description = reader.ReadElementContentAsString("Description", ns);
            Author = reader.ReadElementContentAsString("Author", ns);
            Subscribers = new List<string>(SerializationService.DeserializeItemList<string>("Subscribers", typeof(string), reader));
            var behaviorsList = SerializationService.DeserializeItemList<BehaviorSerializer>("Behaviors", typeof(BehaviorSerializer), reader);
            behaviors.Clear();
            foreach (var behavior in behaviorsList)
                behaviors.Add(behavior.Name, behavior.Parameters.ToArray());

            var conditionList = SerializationService.DeserializeItemList<ConditionSerializer>("Conditions", typeof(ConditionSerializer), reader);
            conditions.Clear();
            foreach (var condition in conditionList)
                conditions.Add(condition.Name, condition.Parameters.ToArray());

            reader.ReadEndElement();
        }


        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            writer.WriteElementString("Publisher", ns, Publisher);
           
            writer.WriteElementString("Name", ns, Name);
            writer.WriteElementString("Description", ns, Description);
            writer.WriteElementString("Author", ns, Author);
            SerializationService.SerializeItemList("Subscribers", typeof(string), Subscribers, writer);
            var behaviorList = new List<BehaviorSerializer>();
            foreach (var key in behaviors.Keys)
                behaviorList.Add(new BehaviorSerializer(key, behaviors[key]));

            SerializationService.SerializeItemList("Behaviors", typeof(BehaviorSerializer), behaviorList, writer);
            var conditionList = new List<ConditionSerializer>();
            foreach (var key in conditions.Keys)
                conditionList.Add(new ConditionSerializer(key, conditions[key]));
            SerializationService.SerializeItemList("Conditions", typeof(ConditionSerializer), conditionList, writer);
    s
        }*/

}
