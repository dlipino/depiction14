﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.API.InteractionEngine
{
    /// <summary>
    /// Filters and applies interaction rules
    /// </summary>
    public class Router
    {
        private BackgroundWorker bgWorkerThread;

        protected InteractionGraph interactionGraph;

        protected readonly MessageQueue messageQueue = new MessageQueue();
        public int ActivityTimeout { set { messageQueue.ActivityTimeout = value; } }
        private bool executing;
        private bool connectedToElementRepository = false;
        private bool interactionsDisabled;

        protected IElementRepository elementRepository;

        protected IInteractionRuleRepository interactionsRepository;

        public InteractionGraph InteractionGraph
        {
            get { return interactionGraph; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [interactions disabled].
        /// </summary>
        /// <value><c>true</c> if [interactions disabled]; otherwise, <c>false</c>.</value>
        public bool InteractionsDisabled
        {
            get { return interactionsDisabled; }
            set { interactionsDisabled = value; }
        }

        #region Constructor
        /// <summary>
        /// Create the interactions router, but don't start it.
        /// </summary>
        /// <param name="elementRepository"></param>
        /// <param name="interactionsRepository"></param>
        public Router(IElementRepository elementRepository, IInteractionRuleRepository interactionsRepository)
        {
            this.elementRepository = elementRepository;
            this.interactionsRepository = interactionsRepository;
//            ProgressMessage = "";
        }

        #endregion

        public void BeginAsync()
        {
            interactionGraph = new InteractionGraph(elementRepository, interactionsRepository.InteractionRules);
            bgWorkerThread = CreateInteractionBackgroundWorker();
            foreach (var element in elementRepository.AllElements)
            {
                RegisterElementForInteractions(element);
            }
            ConnectRouterToElementRepositoryChanges();
        }
        public void EndAsync()
        {
            DisconnectRouteFromElementRepositoryChanges();
            
            if (bgWorkerThread != null)
            {
                bgWorkerThread.CancelAsync();
                messageQueue.AddMessage(null);
            }
        }
        public void CancelAsync()
        {
            DisconnectRouteFromElementRepositoryChanges();
            if (bgWorkerThread != null)
            {
                bgWorkerThread.CancelAsync();
                messageQueue.AddMessage(null);
            }
            messageQueue.CancelWait();
        }
        protected void ConnectRouterToElementRepositoryChanges()
        {
            if (elementRepository != null && !connectedToElementRepository)
            {
                elementRepository.ElementListChange += elementRepository_ElementListChange;
            }
        }
        protected void DisconnectRouteFromElementRepositoryChanges()
        {
            if (elementRepository == null) return;
            connectedToElementRepository = false;
            elementRepository.ElementListChange -= elementRepository_ElementListChange;
        }

        void elementRepository_ElementListChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems)
                    {
                        var elements = item as IEnumerable<IDepictionElement>;
                        if (elements != null)
                        {
                            foreach (var element in elements)
                            {
                                RegisterElementForInteractions(element);
                            }
                            TriggerRouterForMultipleElements(elements);
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                    {
                        var elements = item as IEnumerable<IDepictionElement>;
                        if (elements != null)
                        {
                            foreach (var element in elements)
                            {
                                UnRegisterElementForInteractions(element);
                            }
                            //This part isnt needed, the graph just has to get rerun because it sets thing back to normal state (or at least it should)
                            TriggerRouterForMultipleElements(elements);
                        }
                    }
                    break;
            }
        }

        private BackgroundWorker CreateInteractionBackgroundWorker()
        {
            var bgWorker = new BackgroundWorker();
            bgWorker.DoWork += bgWorkerThread_DoWork;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
            bgWorker.WorkerSupportsCancellation = true;
            bgWorker.RunWorkerAsync(null);
            return bgWorker;
        }

        /// <summary>
        /// Design notes for Router and InteractionGraph, MessageQueue,
        /// (brian and jason) 4/4/2008
        /// 
        /// Our design is concerned with correctness of results at this juncture rather than performance.  That the current design yields correct results in 
        /// every case is an open question at this point, but it works, given the test cases that we have fed it.  We would like to build up
        /// a far more extensive unit test suite before we have a high confidence level in the results.
        /// 
        /// With the current Interaction Graph, we have the concept of passing the graph a set of changeMessages, and based on those changes, 
        /// excecuting the graph to completion.  At that point, our contention is (subject of course to counter examples) that:
        /// 1) All interaction rules that need to be fired for the given changeset are fired, none are missed.
        /// 2) No rule is fired more times than it needs to be.
        /// 
        /// Executing the graph to completion means to process the current changeset and all subsequent changes that are generated during as a result
        /// of the firing of interaction rules.
        /// 
        /// To make this happen we needed to distinguish between change messages that come as a result of user actions, ie, moving a building, etc, and change messages
        /// that come as a result of graph execution.
        /// 
        /// Since the graph execution is done on a background thread, we differentiate between messages coming from that thread and all others.  
        /// If a message comes in as a result of graph execution, that message is routed back to the executing graph so that the graph can update itself based
        /// on the updated changes.  All other change messages get routed to the changeMessageQueue to be consumed the next time the graph is executed.
        /// 
        /// This background threaded consuming of the messages and executing a changeset based on those change messages allows users to 
        /// continue to interact with the story elements, changing, moving, setting properties, etc, and the change execution background 
        /// thread will continue to process those changes until it is caught up and no more change messages are on the queue.
        /// 
        /// InteractionsDisabled property allows temporarily disabling interactions (throwing away changes messages that occur).
        /// It is used now in object creation where the ZOI creation of the object generates a change message, triggering a recalc, 
        /// but as the object has not  yet been added to the repository, the recalc is not only superfluous, but it does nothing 
        /// because the graph only acts on objects that are part of the repository. 
        /// 
        /// If we want to use this same mechanism (InteractionsDisabled) to perform bulk updates we should make so that the messages are not lost, but the 
        /// background thread just suspends processing of them.  That's not how it works now.
        /// 
        /// davidl  Jan 8 2011
        /// 
        /// A desired augmentation is to connect the post set actions to the router so that all inter and intra element changes are run by the interaction graph.
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bgWorkerThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            if (worker == null) return;

//            DoWork(worker, messageQueue.WaitAndPopAll);
            // Do once when syncrhonous.
            while (!worker.CancellationPending)
            {
                try
                {
                    if (worker.CancellationPending)
                        return;
                    IList<DepictionInteractionMessage> messages = messageQueue.WaitAndPopAll();
                    if (worker.CancellationPending)
                        return;
                    if (messages != null)
                    {
                        Processing = true;
                        interactionGraph.ExecuteInteractions(messages);
                    }
                }
                catch (Exception ex)
                {
                    DepictionExceptionHandler.HandleException(ex, false, true);
                }

                Processing = false;
            }
        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (interactionGraph != null)
                interactionGraph.Terminate();
            messageQueue.CancelWait();
        }

        /// <summary>
        /// Registers the interaction.
        /// </summary>
        /// <param name="element">The element that may be involved with interactions at some point.</param>
        public void RegisterElementForInteractions(IDepictionElementBase element)
        {
            element.PropertyChanged += ElementPropertyChangedEventHandler;
        }
        public void UnRegisterElementForInteractions(IDepictionElementBase element)
        {
            element.PropertyChanged -= ElementPropertyChangedEventHandler;
        }

        public void TriggerRouterForMultipleElements(IEnumerable<IDepictionElement> elements)
        {
            
            var triggerList = new List<IDepictionElement>();
            foreach(var element in elements)
            {
                if (element == null) continue;
                triggerList.Add(element);
            }
            if(interactionGraph != null && interactionGraph.ProcessingMessageQueue)
            {
#if DEBUG
                foreach (var element in triggerList)
                {
                    Debug.WriteLine(string.Format("GraphQueue:Queued for {0}", element.ElementType));
                }
#endif
                interactionGraph.AddChangeEvents(triggerList);
            }
            else if (!interactionsDisabled)
            {
#if DEBUG
                foreach (var element in triggerList)
                {
                    Debug.WriteLine(string.Format("MainQueue: Queued for {0}", element.ElementType));
                }
#endif
                messageQueue.AddMessages(triggerList);
            }
        }
        public void TriggerRouterForElementPropertyChange(IDepictionElement element,string propName)
        {
            if (element == null) return;
            if (propName == null) propName = "";
            if (!interactionsRepository.DoesRuleWithElementTypeAndTriggerNameExist(element.ElementType, propName))
            {
                return;
            }

            Debug.WriteLine(propName + " property that changed for " + element.ElementType);
            TriggerRouterForMultipleElements(new[] {element});
        }
        private void ElementPropertyChangedEventHandler(object sender, PropertyChangedEventArgs e)
        {

            var element = sender as IDepictionElement;
            TriggerRouterForElementPropertyChange(element, e.PropertyName);
        }

        private void HandleElementActionAndWait(Action action)
        {
            action();
            //            var start = DateTime.Now;
            //// could be a rare race condition here, but not concerned about that because this is only used in unit tests (bds).
            while (!messageQueue.Empty || Processing)
            {
                Thread.Sleep(10);
                //                if (DateTime.Now - start > TimeSpan.FromSeconds(3)) break;
            }
        }

        public bool Processing
        {
            // TODO: I'm not sure if this should have synchronization.  It's set from the
            // background thread, and read from the UI thread.  (allan)
            get { return executing; }
            protected set
            {
                executing = value;
            }
        }
    }
}
