﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces;

namespace Depiction.API.InteractionEngine
{
    public class InteractionsLibrary
    {
        public InteractionsLibrary()
        {
            Load();
        }

        protected void Load()//This could probably use some modernizing
        {
            //TODO clean this up so more failure paths are dealt with in a nice way
            string productResourceAssembly = DepictionAccess.ProductInformation.ResourceAssemblyName;

            if (string.IsNullOrEmpty(productResourceAssembly))
            {
                //return;//return didn't sit well with some tests
                productResourceAssembly = "Resources.Product.Depiction";
            }
            Assembly resourceAssy = Assembly.Load(new AssemblyName(productResourceAssembly));
            LoadEmbeddedInteractions(resourceAssy, "Interactions");
//            switch (DepictionAccess.ProductInformation.ProductType)
//            {
//                case ProductInformationBase.Prep:
//                    var prepAssembly = Assembly.Load(new AssemblyName("DepictionPrep.Resources"));
//                    LoadEmbeddedInteractions(prepAssembly, "InteractionRules");
//                    break;
//            }
//#if PREP
//            var prepAssembly = Assembly.Load(new AssemblyName("DepictionPrep.Resources"));
//            LoadEmbeddedInteractions(prepAssembly, "InteractionRules");
//#endif
            LoadUserInteractions();
        }

//        protected static string UserInteractionsFilePath
//        {
//            get
//            {
//                return Path.Combine(DepictionAccess.PathService.UserInteractionRulesDirectoryPath,
//                                    DepictionAccess.PathService.UserInteractionRulesFileName);
//            }
//        }

        public IList<IInteractionRule> PermanentInteractions { get; private set; }
        public IList<IInteractionRule> UserInteractions { get; private set; }
        public IList<IInteractionRule> MergedInteractions
        {
            get
            {
                return PermanentInteractions.Union(UserInteractions).ToList();
            }
        }

        #region Helper methods

        protected void LoadEmbeddedInteractions(Assembly assy, string folderName)
        {
            var resourceNames = assy.GetManifestResourceNames();
            var loadedInteractions = new List<IInteractionRule>();

            // Find all resources located in the interactions resource folder, 
            // and load them into the collection of permanent interactions.
            foreach (string name in resourceNames.Where(n => n.StartsWith(string.Format("{0}.{1}.", assy.GetName().Name, folderName))))
            {
                Stream stream = assy.GetManifestResourceStream(name);
                var newRules = InteractionRule.LoadRulesFromXmlStream(stream);
                foreach (var rule in newRules)
                {
                    var fullRule = rule as InteractionRule;
                    if (fullRule != null)
                    {
                        fullRule.IsDefaultRule = true;
                    }
                    if (!loadedInteractions.Contains(rule))
                        loadedInteractions.Add(rule);
                }
            }

            if(PermanentInteractions == null)
            {
                PermanentInteractions = loadedInteractions;
            }else
            {
                PermanentInteractions = PermanentInteractions.Concat(loadedInteractions).ToList();
            }
        }

        protected void LoadUserInteractions()
        {
            UserInteractions = new List<IInteractionRule>();
            if (DepictionAccess.PathService == null) return;
            var fileDir = DepictionAccess.PathService.UserInteractionRulesDirectoryPath;

            var interactions = InteractionRule.LoadRulesFromDirectory(fileDir);
            foreach (var interaction in interactions)
            {
                if (interaction != null && !UserInteractions.Contains(interaction))
                {
                    interaction.IsDisabled = true;
                    UserInteractions.Add(interaction);
                }
            }
            //Get the addins from the 1.3 style addon settings
            var addinDirectoryThirdParty = DepictionAccess.PathService.UserInstalledAddinsDirectoryPath;
           
            if (Directory.Exists(addinDirectoryThirdParty))
            {
                var addinSubDirs = Directory.GetDirectories(addinDirectoryThirdParty, "*", SearchOption.TopDirectoryOnly);
                foreach (var addinFolder in addinSubDirs)
                {
                    var interactionFolder = Path.Combine(addinFolder, "Interactions");
                    if(Directory.Exists(interactionFolder))
                    {
                        try
                        {
                            var otherInteractions = InteractionRule.LoadRulesFromDirectory(interactionFolder);
                            foreach (var interaction in otherInteractions)
                            {
                                if (interaction != null && !UserInteractions.Contains(interaction))
                                {
                                    interaction.IsDisabled = false;
                                    UserInteractions.Add(interaction);
                                }
                            } 
                        }catch
                        {
                            //Just keep going
                        }
                         
                    }
                }
            }
        }

        public void RefreshInteractionLibrary()
        {
            PermanentInteractions = null;
            Load();
        }
        //        public void SaveUserInteractions()
        //        {
        //            // The reason we delete the file if there are no user interactions is, the 
        //            // deserialization throws an exception if if the <InteractionRules/> tag
        //            // is empty.  I don't want to take the time to fix this.  (allan)
        //            if (UserInteractions.Count == 0)
        //                FlushPersistedUserInteractions();
        //            else
        //                InteractionRule.SaveRulesToXmlFile(UserInteractions, UserInteractionsFilePath);
        //        }

        //        public static void FlushPersistedUserInteractions()
        //        {
        //            if (File.Exists(UserInteractionsFilePath))
        //                File.Delete(UserInteractionsFilePath);
        //        }

        #endregion
    }

}
