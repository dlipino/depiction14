﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Depiction.API.Interfaces;
using Depiction.Serialization;

namespace Depiction.API.InteractionEngine
{
    public class BehaviorConditionSerializer : IXmlSerializable
    {
        public BehaviorConditionSerializer()
        {

        }

        public BehaviorConditionSerializer(string name, IEnumerable<IElementFileParameter> parameters)
        {
            Name = name;
            Parameters = parameters;
        }

        public string Name { get; set; }

        public IEnumerable<IElementFileParameter> Parameters { get; set; }

        public XmlSchema GetSchema()
        {
            throw new System.NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
            bool readStart = false;
            if(reader.Name.Equals("Behavior"))
            {
                readStart = true;
                reader.ReadStartElement();
            }
            if (reader.Name.Equals("Name"))
            {
                Name = reader.ReadElementContentAsString();
//                Name = reader.ReadElementContentAsString("Name", ns);
            }
            Parameters = new List<IElementFileParameter>(SerializationService.DeserializeItemList<IElementFileParameter>("Parameters", typeof(ElementFileParameter), reader));
            if(readStart && reader.NodeType.Equals(XmlNodeType.EndElement)) reader.ReadEndElement();

        }

        public void WriteXml(XmlWriter writer)
        {
            var ns = SerializationConstants.DepictionXmlNameSpace;
//            writer.WriteElementString("Name", ns, Name);
            writer.WriteElementString("Name", Name);
            SerializationService.SerializeItemList("Parameters", typeof(ElementFileParameter), Parameters, writer,false);
        }
    }

}
