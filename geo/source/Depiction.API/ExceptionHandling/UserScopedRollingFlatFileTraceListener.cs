using System.Diagnostics;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ObjectBuilder;
using Microsoft.Practices.EnterpriseLibrary.Logging.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging.Formatters;
using Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners;
using Microsoft.Practices.ObjectBuilder;

namespace Depiction.API.ExceptionHandling
{
    public class UserScopedRollingFlatFileTraceListener : RollingFlatFileTraceListener
    {
        public UserScopedRollingFlatFileTraceListener(string fileName, string header, string footer, ILogFormatter formatter, int rollSizeKB,
                                                      string timeStampPattern, RollFileExistsBehavior rollFileExistsBehavior,
                                                      RollInterval rollInterval)
            : base(GetUserScopedFilePath(fileName), header, footer,
                   formatter, rollSizeKB, timeStampPattern, rollFileExistsBehavior, rollInterval)
        {
        }

        protected static string GetUserScopedFilePath(string logFileName)
        {
            return Path.Combine(DepictionAccess.PathService.AppDataDirectoryPath, logFileName);
        }
    }

    [Assembler(typeof(UserScopedRollingFlatFileTraceListenerDataAssembler))]
    public class UserScopedRollingFlatFileTraceListenerData : RollingFlatFileTraceListenerData
    {
    }

    public class UserScopedRollingFlatFileTraceListenerDataAssembler : RollingTraceListenerAssembler
    {


        /// <summary>
        /// This method supports the Enterprise Library infrastructure and is not intended to be used directly from your code.
        /// Builds a <see cref="FlatFileTraceListener"/> based on an instance of <see cref="FlatFileTraceListenerData"/>.
        /// </summary>
        /// <seealso cref="TraceListenerCustomFactory"/>
        /// <param name="context">The <see cref="IBuilderContext"/> that represents the current building process.</param>
        /// <param name="objectConfiguration">The configuration object that describes the object to build. Must be an instance of <see cref="FlatFileTraceListenerData"/>.</param>
        /// <param name="configurationSource">The source for configuration objects.</param>
        /// <param name="reflectionCache">The cache to use retrieving reflection information.</param>
        /// <returns>A fully initialized instance of <see cref="FlatFileTraceListener"/>.</returns>
        public override TraceListener Assemble(IBuilderContext context, TraceListenerData objectConfiguration, IConfigurationSource configurationSource, ConfigurationReflectionCache reflectionCache)
        {
            var castObjectConfiguration = (UserScopedRollingFlatFileTraceListenerData)objectConfiguration;

            ILogFormatter formatter = GetFormatter(context, castObjectConfiguration.Formatter, configurationSource, reflectionCache);

            return new UserScopedRollingFlatFileTraceListener(
                castObjectConfiguration.FileName,
                castObjectConfiguration.Header,
                castObjectConfiguration.Footer,
                formatter,
                castObjectConfiguration.RollSizeKB,
                castObjectConfiguration.TimeStampPattern,
                castObjectConfiguration.RollFileExistsBehavior,
                castObjectConfiguration.RollInterval
                );
        }
    }
}