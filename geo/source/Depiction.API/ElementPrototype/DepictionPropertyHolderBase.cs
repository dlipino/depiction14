﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using Depiction.API.DepictionComparers;
using Depiction.API.EnhancedTypes;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.API.ValueTypeConverters;
using System.Linq;
using Depiction.Serialization;

namespace Depiction.API.ElementPrototype
{//The depictionpropertyholderbase was made in an attempt to keep coders from modifying the property value without triggering
    //a property value change. Sadly that has kind of failed, so much of this is probably not needed, but we still want a way to change the property
    //value and enforcing that the coder decide to cause a change notification.
    public abstract class DepictionPropertyHolderBase : DepictionModelBase,IElementPropertyHolder
    {
        public const string NoChangeText = "Values are equal";

        abstract public bool UseEnhancedPermaText { get; set; }
        abstract public bool UsePropertyNameInHoverText { get; set; }
        //All these calls to linq worry me, while i know they are fast, I also know they get called A LOT, would usinga dictionary be 
        //faster in the long run?
        //protected List<T> properties = new List<T>();
        //key should be lowercase
        protected readonly ThreadSafeDictionary<string, IElementProperty> propertyDictionary = new ThreadSafeDictionary<string, IElementProperty>();

        abstract public SerializableDictionary<string, string[]> ClickActions { get; set; }
        abstract public SerializableDictionary<string, string[]> CreateActions { get; set; }
        abstract public SerializableDictionary<string, string[]> DeleteActions { get; set; }
        abstract public SerializableDictionary<string, string[]> GenerateZoiActions { get; set; }

        public IElementProperty[] OrderedCustomProperties
        {
            //Does this become a problem when going through a for loop?
            get
            {
                var propData = propertyDictionary.Values.Cast<IElementProperty>();
                var sortedProps =
                     from prop in propData
                     orderby prop.Rank ascending
                     select prop;

                return sortedProps.ToArray();//propData.Select(t => t.PropertData).ToArray();

            }
        }

        #region Methods
        /// <summary>
        /// Returns null if successful, returns the exisiting property if it fails.
        /// </summary>
        /// <param name="elementPropertyToAdd"></param>
        /// <param name="useAllNames"></param>
        /// <param name="notifyChangeListeners"></param>
        /// <returns></returns>
        public IElementProperty AddPropertyIfItDoesNotExist(IElementProperty elementPropertyToAdd, bool useAllNames, bool notifyChangeListeners)
        {
            var foundProperty = GetPropertyByInternalName(elementPropertyToAdd.InternalName);
            if (foundProperty == null && useAllNames)
            {
                foundProperty = GetPropertyByDisplayName(elementPropertyToAdd.DisplayName);
            }
            if (foundProperty == null)
            {
                propertyDictionary.Add(elementPropertyToAdd.InternalName.ToLowerInvariant(), elementPropertyToAdd);
                //Man the eid is becoming a real pain in the arse
                if (elementPropertyToAdd.InternalName.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                {
                    var changeResult = SetPropertyValue(elementPropertyToAdd.InternalName.ToLowerInvariant(), elementPropertyToAdd.Value.ToString(), true, false);
                }
                if (elementPropertyToAdd.IsHoverText)
                {
                    NotifyModelPropertyChanged("HoverText");
                }
                if (notifyChangeListeners)
                {
                    NotifyModelPropertyChanged(elementPropertyToAdd.InternalName);
                }
                return null;
            }
            return foundProperty;
        }

        #region propety value/attribute change region

        //This is a labrynth
        protected bool? AddOrReplaceProperty(IElementProperty elementPropertyToAdd, bool useAllNames, bool replaceAttributes, bool notifyChangeListeners)
        {
            var foundProperty = AddPropertyIfItDoesNotExist(elementPropertyToAdd, useAllNames, notifyChangeListeners);
            //Slight hack todeal with 
            if (foundProperty == null) return true;
            //Replace the attributes
            if (replaceAttributes)
            {
                foundProperty.VisibleToUser = elementPropertyToAdd.VisibleToUser;
                foundProperty.Deletable = elementPropertyToAdd.Deletable;
                foundProperty.Editable = elementPropertyToAdd.Editable;
            }

            foundProperty.DisplayName = elementPropertyToAdd.DisplayName;
            //Hovertext is change last in order to trigger the proper change event
            var propName = elementPropertyToAdd.InternalName;
            if (useAllNames)
            {
                propName = foundProperty.InternalName;
            }
            //compare type, if they don't match take the non string version (attempt to convert)
            var origType = foundProperty.ValueType;
            var addedType = elementPropertyToAdd.ValueType;
            bool? changeResult = false;
            //Make sure you don't overwrite an new property value with an old one
            //this generally only happens with live reports.
            if (foundProperty.LastModified > elementPropertyToAdd.LastModified)
                return false;
            if (origType.Equals(addedType))
            {
                changeResult = SetPropertyValue(propName, elementPropertyToAdd.Value, notifyChangeListeners);
            }
            else
            {
                //Type did not match, assume the type that is not the string is correct
                //Unless the propName is EID
                if (propName.Equals("eid", StringComparison.InvariantCultureIgnoreCase))
                {
                    changeResult = SetPropertyValue(propName, elementPropertyToAdd.Value.ToString(), true,
                                                        notifyChangeListeners);
                }
                else
                {
                    if (origType.Equals(typeof(string)) && !addedType.Equals(typeof(string)))
                    {
                        changeResult = SetPropertyValue(propName, elementPropertyToAdd.Value, true,
                                                        notifyChangeListeners);
                    }
                    else if (addedType.Equals(typeof(string)) && !origType.Equals(typeof(string)))
                    {

                        var value = elementPropertyToAdd.Value;
                        try
                        {
                            object convResult = DepictionAPITypeConverter.CheapNumberTypeConverter(value, origType);
                            if (convResult == null)
                            {
                                convResult = TypeDescriptor.GetConverter(origType).ConvertFrom(value);
                            }

                            if (convResult.GetType().Equals(origType))
                            {
                                changeResult = SetPropertyValue(propName, convResult, notifyChangeListeners);
                            }
                        }
                        catch
                        {
                            //What is this supposed to be here for?
                            changeResult = SetPropertyValue(propName, foundProperty.Value, notifyChangeListeners);
                        }
                    }
                    else
                    {
                        var value = elementPropertyToAdd.Value.ToString();
                        try
                        {
                            var lastDitchResult = TypeDescriptor.GetConverter(origType).ConvertFrom(value);

                            if (lastDitchResult.GetType().Equals(origType))
                            {
                                changeResult = SetPropertyValue(propName, lastDitchResult, notifyChangeListeners);
                            }
                        }
                        catch
                        {
                            //What is this supposed to be here for?
                            changeResult = SetPropertyValue(propName, foundProperty.Value, notifyChangeListeners);
                        }
                    }
                }
            }
            if (foundProperty.IsHoverText != elementPropertyToAdd.IsHoverText)
            {
                if (replaceAttributes) foundProperty.IsHoverText = elementPropertyToAdd.IsHoverText;
                if (changeResult == true)
                {
                    NotifyModelPropertyChanged("HoverText");
                }
            }

            return changeResult;
        }
        public bool? AddPropertyOrReplaceValueNotAttributes(IElementProperty elementPropertyToAdd, bool useAllNames, bool notifyChangeListeners)
        {
            return AddOrReplaceProperty(elementPropertyToAdd, useAllNames, false, notifyChangeListeners);
        }

        public bool? AddPropertyOrReplaceValueNotAttributes(IElementProperty elementPropertyToAdd, bool notifyChangeListeners)
        {
            return AddPropertyOrReplaceValueNotAttributes(elementPropertyToAdd, false, notifyChangeListeners);
        }

        public bool? AddPropertyOrReplaceValueNotAttributes(IElementProperty elementPropertyToAdd)
        {
            return AddPropertyOrReplaceValueNotAttributes(elementPropertyToAdd, true);
        }

        public bool? AddPropertyOrReplaceValueAndAttributes(IElementProperty elementPropertyToAdd, bool useAllNames, bool notifyChangeListeners)
        {
            return AddOrReplaceProperty(elementPropertyToAdd, useAllNames, true, notifyChangeListeners);
        }

        public bool? AddPropertyOrReplaceValueAndAttributes(IElementProperty elementPropertyToAdd, bool notifyChangeListeners)
        {
            return AddPropertyOrReplaceValueAndAttributes(elementPropertyToAdd, false, notifyChangeListeners);
        }
        public bool? AddPropertyOrReplaceValueAndAttributes(IElementProperty elementPropertyToAdd)
        {
            return AddPropertyOrReplaceValueAndAttributes(elementPropertyToAdd, true);
        }
        #endregion


        public bool RemovePropertyIfNameAndTypeMatch(IElementProperty elementPropertyBaseToRemove)
        {
            var propComparer = new PropertyComparers.PropertyNameAndTypeComparer<IElementProperty>();

            var result = propertyDictionary.Values.Where(t => propComparer.Equals(elementPropertyBaseToRemove, t));

            var matching = result.ToList();
            if (matching.Count == 0) return false;
            var propToRemove = matching.First();
            //Dispose?
            return propertyDictionary.Remove(propToRemove.InternalName.ToLowerInvariant());
        }

        public bool RemovePropertyWithInternalName(string name, bool alsoMatchDisplayName)
        {
            var loweName = name.ToLowerInvariant();
            if (propertyDictionary.ContainsKey(loweName))
            {
                return propertyDictionary.Remove(loweName);
            }
            if (alsoMatchDisplayName)
            {
                var result = propertyDictionary.Values.Where(t => t.DisplayName.Equals(name, StringComparison.CurrentCultureIgnoreCase));

                var matching = result.ToList();
                if (matching.Count == 0) return false;

                var propToRemove = matching.First();
                return propertyDictionary.Remove(propToRemove.InternalName.ToLowerInvariant());
            }
            return false;
        }

        public bool GetPropertyValue<TPt>(string inputInternalName, out TPt value)
        {//Unit tests?
            var modifiedInternalName = DepictionStringService.ConvertStringToValidInternalName(inputInternalName);
            var internalName = modifiedInternalName.ToLowerInvariant();
            var prop = GetPropertyByInternalName(internalName);
            value = default(TPt);
            if (prop == null) return false;
            var val = prop.Value;
            if (!(val is TPt)) return false; //why does it let me do this is?
            value = (TPt)val;
            return true;
        }

        public bool? SetPropertyValue<TPt>(string inputInternalName, TPt value, bool useInType, bool notifyChange)
        {
            var internalName = DepictionStringService.ConvertStringToValidInternalName(inputInternalName);
            //TODO something funky with read only properties
            var propMatch = GetPropertyByInternalName(internalName);
            if (propMatch == null) return false;
            var prop = propMatch;
            internalName = prop.InternalName;
            var result = new DepictionValidationResult();
            if (useInType)
            {
                result = prop.SetValueAndUpdateType(value, this as IDepictionElement);
            }
            else
            {
                result = prop.SetPropertyValue(value, this as IDepictionElement);
            }

            if (result.ValidationOutput.Equals(ValidationResultValues.InValid)) return false;
            if (result.ValidationOutput.Equals(ValidationResultValues.NoChange)) return null;
            if (propMatch.IsHoverText)
            {
                NotifyModelPropertyChanged("HoverText");
            }
            if (notifyChange)
            {
                NotifyModelPropertyChanged(internalName);
            }
            return true;
        }
        //Does not set the property if value type does not match current type
        public bool? SetPropertyValue<TPt>(string inputInternalName, TPt value, bool notifyChange)
        {
            return SetPropertyValue(inputInternalName, value, false, notifyChange);
        }
        public bool? SetPropertyValue<TPt>(string internalName, TPt value)
        {
            return SetPropertyValue(internalName, value, true);
        }

        public IElementProperty GetPropertyByInternalName(string propertyName)
        {
            var lowerName = propertyName.ToLowerInvariant();
            if (propertyDictionary.ContainsKey(lowerName))
            {
                return propertyDictionary[lowerName];
            }
            return null;
        }

        public IElementProperty GetPropertyByInternalName(string internamName, bool tryDisplayName)
        {
            var prop = GetPropertyByInternalName(internamName);
            if(prop != null) return prop;
            if(!tryDisplayName) return null;
            try
            {
                var props = propertyDictionary.Values.Where(t => t.DisplayName.Equals(internamName, StringComparison.OrdinalIgnoreCase));
                if (props.Count() != 0)
                {
                    prop = props.First();
                    return prop;
                }
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            return null;
        }

        public bool HasPropertyByInternalName(string propertyName)
        {
            if (propertyDictionary.ContainsKey(propertyName.ToLowerInvariant())) return true;
            return false;
        }
        public int NumberOfPropertiesWithDisplayName(string propertyName)
        {
            var count = propertyDictionary.Values.Count(t => t.DisplayName.Equals(propertyName, StringComparison.OrdinalIgnoreCase));
            return count;
        }

        public bool HasPropertyByInternalOrDisplayName(string name)
        {
            if (HasPropertyByInternalName(name)) return true;
            if (NumberOfPropertiesWithDisplayName(name) != 0) return true;
            return false;
        }

        public List<IElementProperty> GetPropertyListClone()
        {
            var outProperties = new List<IElementProperty>();
            var propertyData = propertyDictionary.Values.Cast<IElementProperty>(); ;
            foreach (var prop in propertyData)
            {
                outProperties.Add(prop.DeepClone());
            }
            return outProperties;
        }
        #endregion

        #region Private helpers

//        protected IElementProperty GetPropertyByInternalName(string propertyName)
//        {
//            var lowerName = propertyName.ToLowerInvariant();
//            if (propertyDictionary.ContainsKey(lowerName))
//            {
//                return propertyDictionary[lowerName];
//            }
//            return null;
//        }

        protected IElementProperty GetPropertyByDisplayName(string propertyName)
        {
            var propCount = NumberOfPropertiesWithDisplayName(propertyName);
            if (propCount == 0) return null;
            try
            {
                var props = propertyDictionary.Values.First(t => t.DisplayName.Equals(propertyName, StringComparison.OrdinalIgnoreCase));

                return props;
            }
            catch (InvalidOperationException ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        #endregion

        #region Tooltip

        public string GetToolTipText(string separator, bool usePropNames, bool useSimple)
        {
            string returnVal = GetHoverTextFromProperties(separator, usePropNames, useSimple);
            if (returnVal != null) return returnVal;
            var defaultHovertextProp = GetPropertyByInternalName("DisplayName");

            if (defaultHovertextProp != null)
            {
                defaultHovertextProp.IsHoverText = true;
                return defaultHovertextProp.Value as string;
            }
            return "No tool tip";
        }

        private string GetHoverTextFromProperties(string separator, bool usePropNames, bool useSimple)
        {
            var builder = new StringBuilder();
            bool isFirst = true;
            var elementProps = OrderedCustomProperties;
            //bool hasMultipleHoverTextProperties = elementProps.Count(t => t.IsHoverText) > 1;
            var hoverTextProps = elementProps.Where(t => t.IsHoverText);
            foreach (var property in hoverTextProps)
            {
                var stringValue = "";
                bool isHtml = false;
                //Alas this doesn't actually fix the html in tooltip problem, at least not all of it
                //this will remove the html if normal permatext is used.
                if (property.Value != null && !string.IsNullOrEmpty(property.Value.ToString()))
                {
                    var propString = property.Value.ToString();

                    if (propString.StartsWith("<") && propString.EndsWith(">"))
                    {
                        isHtml = true;
                    }

                    if (!useSimple)
                    {
                        stringValue = propString;
                    }
                    else
                    {
                        if (!isHtml)
                        {
                            stringValue = propString;
                        }
                    }
                }

                if (!isFirst)
                {
                    if (separator.Equals(Environment.NewLine))
                    {
                        builder.AppendLine();
                    }
                    else
                    {
                        builder.Append(separator);
                    }
                }
                if ((usePropNames || typeof(bool).Equals(property.ValueType))&& !isHtml)
                {
                    builder.Append(property.DisplayName);
                    builder.Append(": ");
                }
                builder.Append(stringValue);

                isFirst = false;
            }
            var str = builder.ToString();
            //            if (str.Equals(String.Empty))
            //                return null;
            return str;
        }
        #endregion
        #region Equals Override
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj)) return true;
            var other = obj as DepictionPropertyHolderBase;
            if (other == null) return false;

            if (!propertyDictionary.Keys.Count.Equals(other.propertyDictionary.Keys.Count)) return false;
            //            if (!propertyDictionary.Equals(other.propertyDictionary)) return false;
            foreach (var key in propertyDictionary.Keys)
            {
                if (!other.propertyDictionary.ContainsKey(key)) return false;
                if (!propertyDictionary[key].Equals(other.propertyDictionary[key])) return false;
            }

            //            //This equals is really slow too, really really slow
            //            if (OrderedCustomProperties.Length != other.OrderedCustomProperties.Length) return false;
            //            for (int i = 0; i < OrderedCustomProperties.Length; i++)
            //            {
            //                if (!OrderedCustomProperties[i].Equals(other.OrderedCustomProperties[i])) return false;
            //            }
            return true;
        }

        #endregion
    }
}