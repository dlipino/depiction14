﻿<depictionElement elementType="Depiction.Plugin.Runoff" displayName="Runoff" definitionGroup="Events">
	<properties>
    <property name="Description" displayName="Description" value="Shows the extent of a liquid flowing with gravity from a point source. Use this to indicate a pipeline or water main leak or lava flow, for example" typeName="String" editable="False" deletable="False" />
    <property name="Author" displayName="Author" value="Depiction, Inc." typeName="String" editable="false" deletable="false" />
    <property name="ShowElementPropertiesOnCreate"  value="True" typeName="Boolean" editable="False" visible="False" />
    <property name="ElementClassification"  value="Coverage" typeName="Depiction.API.Enums.ElementClassification, Depiction.API" editable="False" visible="False" />
    <property name="IconPath" displayName="Icon path" value="embeddedresource:Depiction.FluidFlow" typeName="String" deletable="False"/>
    <property name="ShowIcon" displayName="Show icon" value="true" typeName="Boolean" visible="true" deletable="false" editable="True" />
    <property name="ZOIFill" displayName="Fill color" value="#AA0067E9" typeName="Color" deletable="false" />
    <property name="ZOIBorder" displayName="Outline color" value="#AA98E9B0" typeName="Color"  />
    <property name="ZOIShapeType" displayName="ZOI shape type" typeName="ZOIShapeType" value="Polygon" editable="False" deletable="False" visible="False" />
    <property name="Draggable" displayName="Allow dragging" value="True" typeName="Boolean" editable="False" visible="False" />
    <property name="Dependencies" displayName="Dependencies" value="Elevation" typeName="String" editable="False" visible="False" />
    <property name="PlaceableWithMouse"  value="true" editable="false" typeName="Boolean" visible="false" />
    <property name="ElevationTolerance" displayName="Elevation Tolerance" value="0.5 meter" typeName="Depiction.API.ValueObject.Distance, Depiction.API"  >
			<validationRules>
				<validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
					<parameters>
						<parameter value="Depiction.API.ValueObject.Distance, Depiction.API" type="System.RuntimeType" />
						<parameter value="Height tolerance must be a distance." type="System.String" />
					</parameters>
				</validationRule>
				<validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
					<parameters>
						<parameter value="0 meter" type="Depiction.API.ValueObject.Distance, Depiction.API" />
					</parameters>
				</validationRule>
			</validationRules>
		</property>
    <property name="TotalSupply" displayName="Volume" value="10000 gallon" typeName="Depiction.API.ValueObject.Volume, Depiction.API"  >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="Depiction.API.ValueObject.Volume, Depiction.API" type="System.RuntimeType" />
            <parameter value="Volume must be in gallon/liter etc." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0 gallon" type="Depiction.API.ValueObject.Volume, Depiction.API" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="SmoothingFactor" displayName="Smoothing Factor" value="0" typeName="System.Int32"  >
      <validationRules>
        <validationRule type="Depiction.API.Rule.DataTypeValidationRule, Depiction.API">
          <parameters>
            <parameter value="System.Int32" type="System.RuntimeType" />
            <parameter value="Smoothing factor must be a whole number." type="System.String" />
          </parameters>
        </validationRule>
        <validationRule type="Depiction.API.Rule.MinValueValidationRule, Depiction.API">
          <parameters>
            <parameter value="0" type="System.Int32" />
          </parameters>
        </validationRule>
      </validationRules>
    </property>
    <property name="ShowMaxInundation" value="False" typeName="Boolean" displayName="Show maximum inundation"/>
    <property name="Notes" displayName="Additional notes" typeName="String" value=""   />
	</properties>
</depictionElement>