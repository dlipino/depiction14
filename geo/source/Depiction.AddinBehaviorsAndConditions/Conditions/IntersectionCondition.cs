﻿using System.Collections.Generic;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.ElementInterfaces;

namespace Depiction.AddinBehaviorsAndConditions.Conditions
{
    [Condition("IntersectionCondition","Intersection Condition", "Evaluates spatial intersection of two ZOIs")]
    public class IntersectionCondition: ICondition
    {
        public string Name
        {
            get { return "IntersectionCondition"; }
        }
        //hmmm, doubles what is in the xml
        public HashSet<string> ConditionTriggerProperties
        {
            get { return new HashSet<string> { "ZOI", "ZoneOfInfluence" }; }
        }

        public bool Evaluate(IDepictionElementBase triggerElement, IDepictionElementBase affectedElement, object[] inputParameters)
        {
            // If there isn't a zone of influence, we default to true
            if (affectedElement.ZoneOfInfluence == null) return true;

            bool returnValue;

            lock (affectedElement.ZoneOfInfluence)
            {
                if (affectedElement.ZoneOfInfluence.Geometry == null) return true;
                var geom = affectedElement.ZoneOfInfluence.Geometry;
                returnValue = geom.Intersects(triggerElement.ZoneOfInfluence.Geometry);
            }

            return returnValue;
        }
    }
}