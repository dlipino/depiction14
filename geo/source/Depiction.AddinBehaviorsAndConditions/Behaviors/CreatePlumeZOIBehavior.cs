using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.OldValidationRules;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.OldValidationRules;
using Depiction.CoreModel.ValueTypes;
using Depiction.CoreModel.ValueTypes.Measurements;
using GeoAPI.Geometries;
using GisSharpBlog.NetTopologySuite.Geometries;
using Point = System.Windows.Point;

//using GisSharpBlog.NetTopologySuite.Geometries;

namespace Depiction.AddinBehaviorsAndConditions.Behaviors
{
    [Export(typeof(BaseBehavior))]
    [Behavior("CreatePlumeZOI", "Create a plume", "Create a plume")]
    public class CreatePlumeZOIBehavior : BaseBehavior
    {
        private static readonly ParameterInfo[] parameters = 
            new[]
                {
                    new ParameterInfo("WindSpeed", typeof (Speed))
                        {
                            ParameterName = "Wind speed",
                            ParameterDescription = "Wind Speed",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (Speed),
                                                                   "Wind speed must be a speed"),
                                        new MinValueValidationRule(0)
                                    }
                        },
                    new ParameterInfo("WindDirection", typeof (double))
                        {
                            ParameterName = "Wind direction",
                            ParameterDescription = "Wind direction in degrees from north",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "Wind direction must be a number"),
                                        new RangeValidationRule(0, 360)
                                    }
                        },
                    new ParameterInfo("Amount", typeof (double))
                        {
                            ParameterName = "Amount of release",
                            ParameterDescription = "Amount of material released",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "Amount of release must be a number"),
                                        new MinValueValidationRule(0)
                                    }
                        },
                    new ParameterInfo("ElapsedTime", typeof (double))
                        {
                            ParameterName = "Elapsed time",
                            ParameterDescription = "Elapsed time in hours",
                            ValidationRules =
                                new IValidationRule[]
                                    {
                                        new DataTypeValidationRule(typeof (double),
                                                                   "Elapsed time must be a number"),
                                        new MinValueValidationRule(0)
                                    }
                        }
                };

        public override ParameterInfo[] Parameters
        {
            get { return parameters; }
        }

        public string FriendlyName
        {
            get { return "Create plume zone of influence"; }
        }

        public string Description
        {
            get { return "Create plume zone of influence"; }
        }

        protected override BehaviorResult InternalDoBehavior(IDepictionElement subscriber, Dictionary<string, object> parameterBag)
        {
            // several constants control the look of the plume. 
            // I was not sure how big to make a plume. The parameters are the Amount and ElapsedTime.
            // I just multiply the amount and time together. That did not seem big enough 
            // so I added a WeightingFactor. Nothing special about the value. I just picked it
            // because it made the plume big enough.
            //
            // Wind stretches the plume circle into a ellipse. The amount of stretch
            // is determined by the WindRatio constant. When the wind speed reaches
            // the WindRatio constant then the ellipse is twice as long as the original
            // circle. The width of the ellipse is reduced so the total area of the plume
            // is the same.

            const double WeightingFactor = 10.0;
            const double WindRatio = 20.0;

            var windSpeed = (Speed)parameterBag["WindSpeed"];
            var windSpeedKph = windSpeed.GetValue(MeasurementSystem.Metric,MeasurementScale.Normal);
            var windDirection = (double)parameterBag["WindDirection"];
            var releaseAmount = (double)parameterBag["Amount"];
            var elapsedTime = (double)parameterBag["ElapsedTime"];

            double plumeSize = elapsedTime * releaseAmount * WeightingFactor;

            List<Point> ptList;

            if (windSpeedKph == 0.0)
            {
                // no wind, just draw the circle.
                ptList = ShapeCreatingService.CreateCircle(plumeSize / 2.0);
            }
            else
            {
                // set values to area of plume does not change with amount of wind
                double a = plumeSize * (1 + windSpeedKph / WindRatio);

                if (!double.IsNaN(a))
                {
                    double b = plumeSize * plumeSize / a;

                    if (!double.IsNaN(b))
                    {
                        ptList = ShapeCreatingService.CreateEllipse(a, b, 1);

                        // rotate for the wind - remember wind values are where the wind is coming from.
                        ptList = RotatePtList(ptList, -windDirection + 270);
                    }
                    else ptList = ShapeCreatingService.CreateCircle(plumeSize / 2.0);
                }
                else ptList = ShapeCreatingService.CreateCircle(plumeSize / 2.0);
            }

//            subscriber.ClearZoneOfInfluence();
            var coordinates = new List<Coordinate>();
            if (ptList != null && subscriber.Position.IsValid)
            {
                foreach (Point point in ptList)
                {
                    var gPosition = subscriber.Position;
                    var newPos = gPosition.TranslatePosition(point.X, -point.Y, MeasurementSystem.Metric);
                    coordinates.Add(new Coordinate(newPos.Longitude, newPos.Latitude));
                }
                var geometryFactory = new GeometryFactory();
                ILinearRing linearRing = geometryFactory.CreateLinearRing(coordinates.ToArray());
                subscriber.SetZOIGeometry(new DepictionGeometry(geometryFactory.CreatePolygon(linearRing, null)));
//                subscriber.ZoneOfInfluence.SetGeometry(geometryFactory.CreatePolygon(linearRing, null));
//                subscriber.ZoneOfInfluence.FillColor = subscriber.FillColor();
//                subscriber.ZoneOfInfluence.StrokeColor = subscriber.StrokeColor();
//                subscriber.SetElementAreaAndLengthProperty();
            }
            return new BehaviorResult { SubscriberHasChanged = true };
        }

        public List<Point> RotatePtList(List<Point> pointList, double degrees)
        {
            double angle = Math.PI * degrees / 180.0;
            double sinAngle = Math.Sin(angle);
            double cosAngle = Math.Cos(angle);

            var newPtList = new List<Point>();

            foreach (Point point in pointList)
            {
                double x = point.X;
                double y = point.Y;

                var pt = new Point((x * cosAngle + y * sinAngle), (x * -sinAngle + y * cosAngle));
                newPtList.Add(pt);
            }
            return (newPtList);
        }
    }
}