﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.Service;

namespace Depiction.ImporterExtensions.TypeImporters
{
    [DepictionDefaultImporterMetadata("DepictionShapeFileImporter", new[] { ".shp", ".gml",".gpx" },
        FileTypeInformation = "Shape files", DisplayName = "Depiction SHP, GML, and GPX file reader",
        ElementTypesNotSupported = new[] { "Depiction.Plugin.Image" })]
    public class DepictionShapeFileImporter : AbstractDepictionDefaultImporter
    {
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            if (elementLocation == null) return; 
            if (depictionRegion == null)
            {
                var result = DepictionMessageService.FileImportWithUnlimitedAreaWarning();
                if (result.Equals(MessageBoxResult.No))
                {
                    return;
                }
            }
            var fileName = elementLocation.ToString();
            var parameters = new Dictionary<string, object>();
            parameters.Add("FileName", fileName);
            parameters.Add("ElementType", defaultElementType);//Bad this should be changes
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new ShapeFileProviderService();
            var name = string.Format("Reading : {0}", Path.GetFileName(fileName));
            
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }

    public class ShapeFileProviderService : BaseDepictionBackgroundThreadOperation
    {
        #region Overrides of BaseDepictionBackgroundThreadOperation

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var parameters = args as Dictionary<string, object>;
            if (parameters == null) return null;

            var fileName = parameters["FileName"].ToString();
            var elementType = parameters["ElementType"].ToString();
            var regionBounds = parameters["RegionBounds"] as IMapCoordinateBounds;
           
            if (File.Exists(fileName))
            {
                var ogrParser = new OsGeoFileTypeReaderToDepictionElements();
                var prototypes = ogrParser.GetElementPrototypesFromShapeFile(fileName, elementType, regionBounds,
                                                                             "Shape file",
                                                                             "File", false, this);
                var tag = Tags.DefaultFileTag + Path.GetFileName(fileName);
                if (parameters.ContainsKey("Tag"))
                {
                    tag = parameters["Tag"].ToString();
                }
                foreach (var thing in prototypes)
                {
                    thing.Tags.Add(tag);
                }
                return prototypes;
            }
            return null;
        }

        protected override void ServiceComplete(object args)
        {
            var elements = args as List<IElementPrototype>;
            if (elements == null) return;

            if (elements.Count > 0 && DepictionAccess.CurrentDepiction != null)
            {
                UpdateStatusReport(string.Format("Adding element to {0}", DepictionAccess.ProductInformation.StoryName));
                List<IDepictionElement> updatedElements;
                List<IDepictionElement> newElements;
               DepictionAccess.CurrentDepiction.CreateOrUpdateDepictionElementListFromPrototypeList(elements, out newElements,out updatedElements, true,false);
                var newIds = from ne in newElements
                             select ne.ElementKey;
                var oldIds = from oe in updatedElements
                             select oe.ElementKey;
                var elementIdList = newIds.Concat(oldIds).ToList();
                DepictionAccess.CurrentDepiction.RequestElementsWithIdsViewing(elementIdList, new[] { DepictionDialogType.PropertyEditor});
            }
        }

        #endregion
    }
}