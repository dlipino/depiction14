﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Service;
using Depiction.CoreModel.Service.CoreBackgroundServices;

namespace Depiction.ImporterExtensions.TypeImporters
{
    [DepictionDefaultImporterMetadata("DepictionKmlKmzFileImporter", new[] { ".kml", ".kmz" },
        FileTypeInformation = "KML or KMZ Files", DisplayName = "Depiction KML and KMZ importer",
        ElementTypesNotSupported = new[] { "Depiction.Plugin.Image" })]
    //[ElementReader("DepictionKmlKmzReader")]//Is it possible to have double metadata?
    public class DepictionKmlKmzFileImporter : AbstractDepictionDefaultImporter
    {
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            if (elementLocation == null) return;
            if (depictionRegion == null)
            {
                var result = DepictionMessageService.FileImportWithUnlimitedAreaWarning();
                if (result.Equals(MessageBoxResult.No))
                {
                    return;
                }
            }
            var fileName = elementLocation.ToString();
            var parameters = new Dictionary<string, object>();
            parameters.Add("FileName", fileName);
            parameters.Add("ElementType", defaultElementType);
            parameters.Add("RegionBounds", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            var operationThread = new KMLKMZBackgroundService();

            var name = string.Format("Reading KML/KMZ file : {0}", Path.GetFileName(fileName));

            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }
    }
}