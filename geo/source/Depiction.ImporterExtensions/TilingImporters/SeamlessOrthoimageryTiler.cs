﻿using System.ComponentModel.Composition;
using Depiction.API.Interfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.ImporterExtensions.TilingImporters.TileImporterBases;

namespace Depiction.ImporterExtensions.TilingImporters
{
//    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("SeamlessOrthoimageryTileImporter", Description = "Imagery (HRO Seamless)",
    //    DisplayName = "Imagery (HRO Seamless)", ImporterSources = new[] { InformationSource.Web })]
    public class TNMLargeScaleImageryTiler : SeamlessTilerBase
    {
        public TNMLargeScaleImageryTiler()
        {
            tileCacheService = new TileCacheService(SourceName);
        }
        
        public override string SourceName
        {
            get { return "The National Map Imagery"; }
        }

        public override string DisplayName
        {
            get
            {
                return "Imagery (The National Map)";
            }
        }

        public override string LegacyImporterName
        {
            get { return "TNMOrthoimageryTileImporter"; }
        }
        
        protected override string URL { get { return "http://raster.nationalmap.gov/ArcGIS/services/TNM_Large_Scale_Imagery_Overlay/MapServer/WMSServer"; } }
        protected override string layerName { get { return "0,1,2"; } }
        protected override int MaxZoomLevel { get { return 20; } }

    }
    
  
    
}
