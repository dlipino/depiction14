﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Globalization;
using System.Linq;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;

namespace Depiction.ImporterExtensions.TilingImporters
{
    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("MapQuestOpenAerialTileImporter", Description = "Imagery (MapQuest) tile importer. For more go to http://open.mapquest.com",
    //    DisplayName = "Imagery (MapQuest)", ImporterSources = new[] { InformationSource.Web })]
    public sealed class MapQuestTiler : OpenStreetMapTilerBase
    {
        private const string MapquestURL = "http://oatile{0}.mqcdn.com/tiles/1.0.0/sat/{1}/{2}/{3}.png";
        public override string URL { get { return MapquestURL; } }
        public override TileImageTypes TileImageType { get { return TileImageTypes.Satellite; } }
        public override string DisplayName { get { return "Imagery (MapQuest)"; } }
        public override string SourceName { get { return "MapQuestOpenAerialService"; } }
        public override string LegacyImporterName { get { return "MapQuestOpenAerialTileImporter"; } }
        protected override bool UseLetters { get { return false; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(SourceName);
                return cacheService;
            }
        }
    }

    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("OpenStreetMapTileImporter", Description = "Street Map (OpenStreetMap) tile importer",
    //    DisplayName = "Street Map (OpenStreetMap)")]
    public class OpenStreetMapTiler : OpenStreetMapTilerBase
    {
        public override string URL { get { return "http://{0}.tile.openstreetmap.org/{1}/{2}/{3}.png"; } }
        public override string DisplayName { get { return "Street Map (OpenStreetMap)"; } }
        public override string SourceName { get { return "OpenStreetMapService"; } }
        public override string LegacyImporterName { get { return "OpenStreetMapTileImporter"; } }


        public override TileImageTypes TileImageType { get { return TileImageTypes.Street; } }
        protected override bool UseLetters { get { return true; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(SourceName);
                return cacheService;
            }
        }
    }
    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("OpenStreetMapTileImporter", Description = "Street Map (OpenStreetMap) tile importer",
    //    DisplayName = "Street Map (OpenStreetMap)")]
    public class OpenStreetMapHumanitarianTiler : OpenStreetMapTilerBase
    {
        public override string URL { get { return "http://{0}.tile.openstreetmap.fr/hot/{1}/{2}/{3}.png"; } }
        public override string DisplayName { get { return "Street Map Humanitarian(OpenStreetMap)"; } }
        public override string SourceName { get { return "OpenStreetMapHService"; } }
        public override string LegacyImporterName { get { return "OpenStreetMapHTileImporter"; } }


        public override TileImageTypes TileImageType { get { return TileImageTypes.Street; } }
        protected override bool UseLetters { get { return true; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(SourceName);
                return cacheService;
            }
        }
    }
    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("MapQuestOpenStreetTileImporter", Description = "Street map (MapQuest) tile importer. For more go to http://open.mapquest.com",
    //    DisplayName = "Street Map (MapQuest)", ImporterSources = new[] { InformationSource.Web })]
    public class MapQuestOpenStreetMapTiler : OpenStreetMapTilerBase
    {
        
        public override string URL { get { return "http://otile{0}.mqcdn.com/tiles/1.0.0/osm/{1}/{2}/{3}.jpg"; } }
        public override string DisplayName { get { return "Street Map (MapQuest)"; } }
        public override string SourceName { get { return "MapQuestOpenStreetService"; } }
        public override string LegacyImporterName { get { return "MapQuestOpenStreetTileImporter"; } }
        public override TileImageTypes TileImageType { get { return TileImageTypes.Street; } }
        protected override bool UseLetters { get { return false; } }
        private TileCacheService cacheService;
        protected override TileCacheService CacheService
        {
            get
            {
                if (cacheService == null)
                    cacheService = new TileCacheService(SourceName);
                return cacheService;
            }
        }
        //public static string Name { get { return "Street Map (OpenStreetMap)"; } }
    }


    public abstract class OpenStreetMapTilerBase : ITiler
    {
        public abstract string URL { get; }
        public abstract string DisplayName { get; }
        public abstract string SourceName { get; }
        public abstract TileImageTypes TileImageType { get; }
        public abstract string LegacyImporterName { get; }
        protected abstract bool UseLetters { get; }
        public int PixelWidth { get { return 256; } }
        public bool DoesOwnCaching { get { return false; } }
        protected abstract TileCacheService CacheService { get; }

        public int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            int col = (int)(Math.Floor((latLong.Longitude + 180.0) / 360.0 * Math.Pow(2.0, zoom)));
            if (col >= (int)Math.Pow(2, zoom))
                col = (int)Math.Pow(2, zoom) - 1;
            if (col <= 0)
                col = 0;
            return col;
        }

        public int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            int row = (int)(Math.Floor((1.0 - Math.Log(Math.Tan(latLong.Latitude* Math.PI / 180.0) + 1.0 / Math.Cos(latLong.Latitude * Math.PI / 180.0)) / Math.PI) / 2.0 * Math.Pow(2.0, zoom)));
            if (row >= (int)Math.Pow(2, zoom))
                row = (int)Math.Pow(2, zoom) - 1;
            if (row <= 0)
                row = 0;
            return row;
        }

        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            return col/Math.Pow(2.0, zoomLevel)*360.0 - 180;
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            double n = Math.PI - 2.0*Math.PI*row/Math.Pow(2.0, zoom);
            return 180.0/Math.PI*Math.Atan(0.5*(Math.Exp(n) - Math.Exp(-n)));
        }

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            double widthInDegrees = TileColToTopLeftLong(tileToGet.Column + 1, zoomLevel) - TileColToTopLeftLong(tileToGet.Column, zoomLevel);
            double heightInDegrees = TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);

            var uri = new Uri(GetTileUrl(tileToGet.Row, tileToGet.Column, zoomLevel));
            var tile = new TileModel(zoomLevel, tileToGet.Row, tileToGet.Column,
                            uri,
                            new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel), TileColToTopLeftLong(tileToGet.Column, zoomLevel)),
                            new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
            return tile;
        }

        private readonly Random random = new Random();

        public string GetTileUrl(int row, int column, int zoom)
        {
            int next = random.Next(1, 4);
            object nextObj;
            if (UseLetters)
                nextObj = Convert.ToChar(next + 96);
            else
                nextObj = next;

            return string.Format(CultureInfo.InvariantCulture, URL, nextObj, zoom, column, row);
        }

        public int GetZoomLevel(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int i;

            for (i = 1; i < maxZoomLevel; i += 1)
            {
                double tilesAcross = Math.Abs(boundingBox.Left - boundingBox.Right)*(1 << i)/360D;
                if (tilesAcross > minTilesAcross) break;
            }

            return i;
        }

        #region methods

        public IList<TileModel> GetTiles(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = LatitudeToRowAtZoom(boundingBox.TopRight, zoomLevel);
            int col = LongitudeToColAtZoom(boundingBox.BottomLeft, zoomLevel);
            int lastCol = LongitudeToColAtZoom(boundingBox.TopRight, zoomLevel);
            int lastRow = LatitudeToRowAtZoom(boundingBox.BottomLeft, zoomLevel);

            var tiles = new List<TileModel>();

            var tilesToGet = TilesToGet(lastCol, col, lastRow, row);
            

            foreach (var tileToGet in tilesToGet)
            {
                var tile = GetTileModel(tileToGet, zoomLevel);

                tiles.Add(tile);
            }

            return tiles;
        }
        

        //<summary>
         //Returns the list of tiles to get ordered in increasing distance from the center
         //of the area being tiled.
         //</summary>
        private IEnumerable<TileXY> TilesToGet(int lastCol, int col, int lastRow, int row)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j }));
                }
            }

            IEnumerable<TileXY> sortedTiles = 
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }

        #endregion

    }
}