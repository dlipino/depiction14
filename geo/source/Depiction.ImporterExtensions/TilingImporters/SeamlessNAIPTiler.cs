﻿using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.TileServiceHelpers;
using Depiction.ImporterExtensions.TilingImporters.TileImporterBases;

namespace Depiction.ImporterExtensions.TilingImporters
{
//    [Export(typeof(ITiler))]
    //[DepictionDefaultImporterMetadata("NAIPSeamlessImporter", Description = "Imagery (NAIP)", DisplayName = "Imagery (NAIP)")]
    public sealed class SeamlessNAIPTiler : SeamlessTilerBase
    {

        public SeamlessNAIPTiler()
        {
            tileCacheService = new TileCacheService(SourceName);
        }

        protected override string URL { get { return "http://raster.nationalmap.gov/ArcGIS/services/Orthoimagery/USGS_EDC_Ortho_NAIP/ImageServer/WMSServer"; } }

        protected override string layerName { get { return "0"; } }

        public override string DisplayName
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        return "Aerial Imagery (USDA)";
                    default:
                        return "Imagery (NAIP)";
                }
            }
        }

        public override string SourceName
        {
            get { return "NAIP"; }
        }
        public override string LegacyImporterName
        {
            get { return "NAIPSeamlessImporter"; }
        }
        protected override int MaxZoomLevel { get { return 18; } }
    }

}