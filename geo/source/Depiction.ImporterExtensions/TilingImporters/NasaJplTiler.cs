﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.APIUnmanaged.Service;

namespace Depiction.ImporterExtensions.TilingImporters
{
    [Export(typeof(ITiler))]
    public sealed class NasaJPLTiler : ITiler
    {
        
        private readonly Dictionary<int, double> zoomLevels = new Dictionary<int, double>();

        public NasaJPLTiler()
        {
            BuildZoomLevels();
        }

        public TileImageTypes TileImageType
        {
            get { return TileImageTypes.Satellite; }
        }

        public string DisplayName
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        return "Satellite Imagery (NASA)"; 
                    default:
                        return "Imagery (NASA Landsat 7)";
                }
            }
        }

        public string SourceName
        {
            get { return "NASAJPLTile"; }
        }

        public string LegacyImporterName
        {
            get { return "NASAJPLImporter"; }
        }

        public int PixelWidth
        {
            get { return 512; }
        }

        public bool DoesOwnCaching
        {
            get { return false; }
        }


        private void BuildZoomLevels()
        {
            //4 degress is the max suggested by NASA for this tiling service.
            double degreesCovered = 16;
            int i = 0;

            do
            {
                while (i < 5)
                {
                    i++;
                    zoomLevels.Add(i, degreesCovered);
                }
                i++;
                zoomLevels.Add(i, degreesCovered);
                degreesCovered /= 2;
            } while (degreesCovered > 0.03125);
        }

        public int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            return Convert.ToInt32(Math.Floor((latLong.Longitude + 180) / zoomLevels[zoom]));
        }

        public int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom)
        {
            var row = (90 - latLong.Latitude) / zoomLevels[zoom];
            row = row > 0 ? Math.Floor(row) : Math.Ceiling(row);
            return Math.Abs(Convert.ToInt32(row));
        }

        public int GetZoomLevel(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int i = 1;
            double width = boundingBox.Right - boundingBox.Left;

            while (zoomLevels.ContainsKey(i))
            {
                double tilesAcross = width / zoomLevels[i];
                if (tilesAcross > minTilesAcross) break;
                i++;
            }

            if (!zoomLevels.ContainsKey(i))
                i--;

            return i;
        }


        //http://wms.jpl.nasa.gov/wms.cgi?request=GetMap&layers=global_mosaic&srs=EPSG:4326&width=512&height=512&bbox={0},{1},{2},{3}&format=image/jpeg&version=1.1.1&styles=visual
        public IList<TileModel> GetTiles(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            int zoomLevel = GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = LatitudeToRowAtZoom(boundingBox.TopRight, zoomLevel);
            int col = LongitudeToColAtZoom(boundingBox.BottomLeft, zoomLevel);
            int lastCol = LongitudeToColAtZoom(boundingBox.TopRight, zoomLevel);
            int lastRow = LatitudeToRowAtZoom(boundingBox.BottomLeft, zoomLevel);

            var tiles = new List<TileModel>();

            var tilesToGet = TilesToGet(lastCol, col, lastRow, row);
            

            foreach (var tileToGet in tilesToGet)
            {
                //west, south, east, north
                var tile = GetTileModel(tileToGet, zoomLevel);
                tiles.Add(tile);
            }
            return tiles;
        }

                //<summary>
         //Returns the list of tiles to get ordered in increasing distance from the center
         //of the area being tiled.
         //</summary>
        private IEnumerable<TileXY> TilesToGet(int lastCol, int col, int lastRow, int row)
        {
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j }));
                }
            }

            IEnumerable<TileXY> sortedTiles = 
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }
        
        public double TileColToTopLeftLong(int col, int zoomLevel)
        {
            return (zoomLevels[zoomLevel] * col) - 180;
        }

        public double TileRowToTopLeftLat(int row, int zoom)
        {
            return 90 - zoomLevels[zoom] * row;
        }

        public TileModel GetTileModel(TileXY tileToGet, int zoomLevel)
        {
            double widthInDegrees = TileColToTopLeftLong(tileToGet.Column + 1, zoomLevel) - TileColToTopLeftLong(tileToGet.Column, zoomLevel);
            double heightInDegrees = TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);
            //west, south, east, north
            var uri =
                new Uri(
                    string.Format(
                        "http://wms.jpl.nasa.gov/wms.cgi?request=GetMap&layers=global_mosaic&srs=EPSG:4326&width=512&height=512&bbox={0},{1},{2},{3}&format=image/jpeg&version=1.1.1&styles=visual",
                        TileColToTopLeftLong(tileToGet.Column, zoomLevel),
                        TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel),
                        TileColToTopLeftLong(tileToGet.Column + 1, zoomLevel),
                        TileRowToTopLeftLat(tileToGet.Row, zoomLevel)));
            var tile = new TileModel(zoomLevel, tileToGet.Row, tileToGet.Column,
                            uri,
                            new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel), TileColToTopLeftLong(tileToGet.Column, zoomLevel)),
                            new LatitudeLongitude(TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
            return tile;
        }
    }
}
