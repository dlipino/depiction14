﻿using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.CoreModel.Service.CoreBackgroundServices;


namespace Depiction.ImporterExtensions.WxSImporters
{
    [DepictionDefaultImporterMetadata("WcsElementImporter", Description = "Gets wcs elements for a story",DisplayName = "Wcs Element Importer")]
    class WcsElementImporter : AbstractDepictionDefaultImporter
    {
        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds area, Dictionary<string, string> parameters)
        {
            var operationParams = new Dictionary<string, object>();
            foreach (var pair in parameters)
            {
                operationParams.Add(pair.Key, pair.Value);
            }
            operationParams.Add("Area", area);
            var name = "Getting elevation from WCS provider";
            if (parameters.ContainsKey("name"))
            {
                name = parameters["name"];
            }
            var wcsProvider = new WcsElementProviderBackground();
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(wcsProvider);
            wcsProvider.UpdateStatusReport(name);
            wcsProvider.StartBackgroundService(operationParams);
        }
    }
}