using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.APIUnmanaged.Service;

namespace Depiction.ImporterExtensions.FloodMapImporter
{
    [DepictionDefaultImporterMetadata("FloodMapImporter", Description = "Flood Hazard Map SFHA from FEMA",
        DisplayName = "Flood Hazard Map SFHA (FEMA)")]
    public class FloodMapImporter : AbstractDepictionDefaultImporter 
    {
        #region Overrides of AbstractDepictionDefaultImporter

        public override void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> inParameters)
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("Area", depictionRegion);
            if (inParameters != null)
            {
                foreach (var param in inParameters)
                {
                    parameters.Add(param.Key, param.Value);
                }
            }
            //            var operationThread = new HistoricalWildfireImporterBackgroundService();
            var operationThread = new WmsElementProviderBackground();
            var name = string.Format("Flood Hazard Map SFHA (FEMA)");
//        http://hazards.fema.gov/wmsconnector/wmsconnector/com.esri.wms.Esrimap/NFHL?
//            layerName:HDM:Q3_FLOOD
            var layerNames = "HDM:Q3_FLOOD";
            parameters.Add("layerName", layerNames);
            if (!parameters.ContainsKey("name"))
            {
                parameters.Add("name", name);
            }
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(operationThread);
            operationThread.UpdateStatusReport(name);
            operationThread.StartBackgroundService(parameters);
        }

        #endregion
    }
}