using System.Collections.Generic;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.AddinObjects.MEFMetadata;
using Depiction.API.Interfaces.GeoTypeInterfaces;

namespace Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter
{
    [DepictionDefaultImporterMetadata("OpenStreetMapRoadNetworkImporter", Description = "Gets Road Network from Open StreetMaps", 
        DisplayName = "Open Street Map road network importer" )]
    public class OpenStreetMapRoadNetworkImporter : AbstractDepictionDefaultImporter
    {
        #region Implementation of IDepictionDefaultImporter

        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            var operationParams = new Dictionary<string, object>();
            operationParams.Add("Area", depictionRegion);
            foreach (var param in parameters)
            {
                operationParams.Add(param.Key, param.Value);
            }
            var streetmapService = new OpenStreetMapRoadnetworkImporterService();
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(streetmapService);
            streetmapService.StartBackgroundService(operationParams);
        }

        #endregion
    }
}