using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Linq;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.RoadNetwork;
using Depiction.CoreModel.ElementLibrary;
using Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter.OSMObjects;
using Point = System.Windows.Point;

namespace Depiction.ImporterExtensions.RoadNetworkImporters.OSMRoadNetworkImporter
{
    public class OpenStreetMapRoadnetworkImporterService : BaseDepictionBackgroundThreadOperation
    {
        private const int maxNumberOfThreads = 12;
        private const double cachedTileDiameterInDegrees = .015;//was .03, but there are issues with the amount of info which stops the transfer in certain areas
        //TODO send this as a parameter from webservice
        private string osmURL = "http://open.mapquestapi.com/xapi/api/0.6/way[highway=motorway|motorway_link|trunk|trunk_link|primary|primary_link|secondary|tertiary|unclassified|road|residential|living_street|service|track][bbox={0},{1},{2},{3}]";

        readonly Dictionary<long, OSMNode> nodes = new Dictionary<long, OSMNode>();
        readonly Dictionary<long, OSMWay> ways = new Dictionary<long, OSMWay>();
        private int numberOfWorkers;
        private readonly object numberOfWorkersLock = new object();
        private readonly object tilesRetrievedLock = new object();
        private double totalTiles;
        private double tilesRetrieved;
        private TileCacheService cacheService;
        public bool ReplaceCachedInformation { get { return Settings.Default.ReplaceCachedFiles; } }

        #region Constructor

        public OpenStreetMapRoadnetworkImporterService()
        {
            cacheService=new TileCacheService("OSMRoadNetwork");
        }

        #endregion

        #region Overrides of BaseDepictionBackgroundThreadOperation

        override protected bool ServiceSetup(Dictionary<string, object> args)
        {
            UpdateStatusReport("Open Street Map Road network");
            return true;
        }

        protected override object ServiceAction(object args)
        {
            var argDictionary = args as Dictionary<string, object>;
            if (argDictionary == null) return null;
            if (!argDictionary.ContainsKey("Area")) return null;
            var area = argDictionary["Area"] as IMapCoordinateBounds;

            if (argDictionary.ContainsKey("url"))
            {
                osmURL = argDictionary["url"].ToString();
            }

            var roadNetworkElement = CreateRoadNetworkElement(area, osmURL);
            return roadNetworkElement;
        }
        protected override void ServiceComplete(object args)
        {
            var argElement = args as IDepictionElement;
            if (argElement != null)
            {
                UpdateStatusReport(string.Format("Road network downloaded, adding to {0}",DepictionAccess.ProductInformation.StoryName));
                DepictionAccess.CurrentDepiction.AddElementGroupToDepictionElementList(new[] { argElement }, false);
            }
        }
        #endregion

        #region protecteed methdos

        protected IDepictionElementBase CreateRoadNetworkElement(IMapCoordinateBounds area, string newOsmUrl)
        {
            if (!string.IsNullOrEmpty(newOsmUrl))
            {
                osmURL = newOsmUrl;
            }
            var firstCol = getColumn(area.Left);
            var lastCol = getColumn(area.Right);
            var firstRow = getRow(area.Top);
            var lastRow = getRow(area.Bottom);

            totalTiles = (lastCol - firstCol + 1) * (lastRow - firstRow + 1);

            for (int col = firstCol; col <= lastCol; col++)
            {
                for (int row = firstRow; row <= lastRow; row++)
                {
                    while (numberOfWorkers >= maxNumberOfThreads)
                    {
                        Thread.Sleep(100);
                    }
                    if (!ServiceStopRequested)
                    {
                        BackgroundWorker osmRoadNetworkTileGetter = new BackgroundWorker();
                        osmRoadNetworkTileGetter.DoWork += osmRoadNetworkTileGetter_DoWork;
                        osmRoadNetworkTileGetter.RunWorkerCompleted += osmRoadNetworkTileGetter_RunWorkerCompleted;
                        Thread.Sleep(100);
                        osmRoadNetworkTileGetter.RunWorkerAsync(new Point(col, row));

                        lock (numberOfWorkersLock)
                        {
                            numberOfWorkers++;
                        }
                    }
                }
            }

            while (numberOfWorkers > 0) Thread.Sleep(100);
            if (ServiceStopRequested) return null;
            var roadSegments = OSMHelperMethods.GetTaggedEdgeListFromOSMData(nodes, ways, area, this);
            var validHighwayValues = new[] { "motorway", "motorway_link", "trunk", "trunk_link", "primary", "primary_link", "secondary", "tertiary", "unclassified", "road", "residential", "living_street", "service", "track" };
            double totalWays = ways.Count(w => w.Value.Tags.ContainsKey("highway") && validHighwayValues.Contains(w.Value.Tags["highway"]));

            var message = string.Format("Generating RoadNetwork: {0} roads", totalWays);
            UpdateStatusReport(message);
            var element = GeneratePrimaryRoadNetworkElement(roadSegments, "Road Network");
            return element;
        }

        #endregion
        #region tile thread methods

        void osmRoadNetworkTileGetter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lock (tilesRetrievedLock)
            {
                tilesRetrieved++;
                var message = string.Format("Retrieved {0} of {1} tiles", tilesRetrieved, totalTiles);
                UpdateStatusReport(message);
            }
            lock (numberOfWorkersLock)
            {
                numberOfWorkers--;
            }
        }

        void osmRoadNetworkTileGetter_DoWork(object sender, DoWorkEventArgs e)
        {
            Point p = (Point)e.Argument;

            int col = (int)p.X;
            int row = (int)p.Y;
            if (ServiceStopRequested) return;
            var fileName = "OSMRoadNetwork_row_" + row + "_col_" + col + ".xml";
            string fullxmlFileName;
            if(cacheService == null)
            {
                fullxmlFileName = DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName);
            }else
            {
                fullxmlFileName = cacheService.GetCacheFullStoragePath(fileName);
            }

            if (!File.Exists(fullxmlFileName) || ReplaceCachedInformation)
            {
                var box = GetBoundingBox(col, row);
                fullxmlFileName = GetXML(string.Format(CultureInfo.InvariantCulture, osmURL, box.Left, box.Bottom, box.Right, box.Top), fullxmlFileName, 0);
            }

            if (string.IsNullOrEmpty(fullxmlFileName)) return;

            try
            {
                OSMNode[] nodesInFile;
                OSMWay[] waysInFile;
                OSMHelperMethods.GetNodesAndWays(fullxmlFileName, out nodesInFile, out waysInFile,this);

                lock (nodes)
                {
                    foreach (var node in nodesInFile)
                    {
                        if (ServiceStopRequested) break;
                        if (!nodes.ContainsKey(node.NodeID))
                            nodes.Add(node.NodeID, node);
                    }
                }

                lock (ways)
                {
                    foreach (var way in waysInFile)
                    {
                        if (ServiceStopRequested) break;
                        if (!ways.ContainsKey(way.WayID))
                            ways.Add(way.WayID, way);
                    }
                }
            }
            catch (Exception)
            {
                // Don't keep a cached file that failed to process.

                if (File.Exists(fullxmlFileName))
                    File.Delete(fullxmlFileName);
                // DepictionAccess.PathService.DeleteCachedFile(fileName);
            }
        }
        #endregion

        #region public methods

        public IDepictionElementBase GeneratePrimaryRoadNetworkElement(IEnumerable<RoadSegment> segments, string displayName)
        {
            var roadNetwork =
                DepictionAccess.CurrentDepiction.CompleteElementRepository.GetFirstElementByTag("PrimaryRoadNetwork");
            if (roadNetwork == null)
            {
                var prototype = DepictionAccess.ElementLibrary.GetPrototypeByFullTypeName("Depiction.Plugin.RoadNetwork");
                roadNetwork = ElementFactory.CreateElementFromPrototype(prototype);
            }
            if (roadNetwork == null) return null;
            roadNetwork.SetPropertyValue("displayname", displayName);
            if (ServiceStopRequested) return null;
            var roadGraph = BuildRoadGraph(segments);
            roadNetwork.AddPropertyOrReplaceValueAndAttributes(new DepictionElementProperty("RoadGraph", string.Empty, roadGraph, null, null) { VisibleToUser = false });
            if (ServiceStopRequested) return null;
            var geom = roadGraph.ConvertToGeometry();
            if (geom == null) return null;
            var zoi = new ZoneOfInfluence(geom);
            if (zoi.IsEmpty) return null;

            roadNetwork.SetInitialPositionAndZOI(null, zoi);
            roadNetwork.Tags.Add("PrimaryRoadNetwork");
            // if (roadNetwork.ZoneOfInfluence.IsEmpty) return null;
            if (ServiceStopRequested) return null;
            return roadNetwork;
        }

        public RoadGraph BuildRoadGraph(IEnumerable<RoadSegment> roadSegments)
        {
            var roadGraph = new RoadGraph();

            //construct the graph from road segments
            foreach (var ls in roadSegments)
            {
                roadGraph.AddVertex(ls.Source);
                roadGraph.AddVertex(ls.Target);
                roadGraph.AddEdge(ls);
            }
            return roadGraph;
        }

        #endregion

        #region private helpers

        #region static methods
        

        private static int getColumn(double Longitude)
        {
            return Convert.ToInt32(Math.Floor((Longitude + 180) / cachedTileDiameterInDegrees));
        }

        private static int getRow(double Latitude)
        {
            return Convert.ToInt32(Math.Floor(Math.Abs(Latitude - 80) / cachedTileDiameterInDegrees));
        }

        private static IMapCoordinateBounds GetBoundingBox(int column, int row)
        {
            var topLeft = new LatitudeLongitude(80 - (row * cachedTileDiameterInDegrees), (column * cachedTileDiameterInDegrees) - 180);
            return new MapCoordinateBounds(topLeft, new LatitudeLongitude(topLeft.Latitude - cachedTileDiameterInDegrees, topLeft.Longitude + cachedTileDiameterInDegrees));
        }
        #endregion

        private string GetXML(string url, string fileName, int exceptionCount)
        {
            //var webClient = new WebClient();
            //var buffer = new byte[0];
            if (ServiceStopRequested) return null;

            var webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.AutomaticDecompression = DecompressionMethods.GZip;
            HttpWebResponse webResponse = null;
            if (ServiceStopRequested) return null;
            try
            {
                webResponse = (HttpWebResponse)webRequest.GetResponse();
            }
            catch (WebException ex)
            {
                // Retry 3 times if there is a web exception (time-out)
                if (ServiceStopRequested)
                {
                    if (webResponse != null) webResponse.Close();
                    return null;
                }

                if (exceptionCount < 3)
                {
                    return GetXML(url, fileName, exceptionCount + 1);
                }
                DepictionAccess.NotificationService.DisplayMessageString(ex.Message, 5);
            }
            if (ServiceStopRequested || webResponse == null)
            {
                if (webResponse != null) webResponse.Close();
                return null;
            }
            var buffer = webResponse.GetResponseStream();
            if (buffer == null) return string.Empty;
            using (var fileStream = new FileStream(fileName, FileMode.Create))
            {
                buffer.CopyTo(fileStream);
            }
            // if this file was created while we were waiting, let's not redo it, why would this occur
//            var valuetoReturn = DepictionAccess.PathService.RetrieveCachedFilePathIfCached(fileName) ??
//                                DepictionAccess.PathService.CacheFile(webResponse.GetResponseStream(), fileName);
            webResponse.Close();
            return fileName;
        }



        #endregion
    }
}