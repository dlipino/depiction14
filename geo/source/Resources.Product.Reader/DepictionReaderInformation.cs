﻿using Depiction.API.AbstractObjects;

namespace Resources.Product.Reader
{
    public class DepictionReaderInformation : ProductInformationBase
    {

        public override string ProductType { get { return Reader; } }
        public override string ProductName
        {
            get { return "Depiction: Reader"; }
        }
        public override string ProductNameInternalUsage { get { return "DepictionReader"; } }
        public override string ResourceAssemblyName { get { return "Resources.Product.Reader"; } }//. format seems to work for using getAssembly
        public override string SplashScreenPath { get { return "Images/FreeReaderSplash.png"; } }
//        public override string AppIconResourceName { get { return "Images/DepictionReader.ico"; } }
        public override string LicenseFileName { get { return string.Empty; } }
        public override string AboutText
        {
            get
            {
                return string.Format("{0}\n\n{1} All other trademarks are the property of their respective owners.",ProductVersion, DepictionCopyright);
            }
        }
        public override string EulaFileAssemblyLocation
        {
            get { return "Docs.DepictionReaderEULA.xps"; }
        }
    }
}