﻿using System.ComponentModel;
using System.Windows;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.AddElements
{

    /// <summary>
    /// Interaction logic for AddElementsHelpingWindow.xaml
    /// </summary>
    public partial class AddElementsHelpingWindow
    {
        private const string normalAdd =
              "To cancel add mode, click the \"Done\" button, or click the right mouse button.\r\n\r\nTo drag the world, hold \"Shift\" while dragging with the left mouse button.";
        //        For lines & shapes: 
        private const string linesAndShapes =
            "Click to add vertices or 'Ctrl'+click to remove last.\r\nTo complete your element, click the \"Done\" button, or click the right mouse button.\r\nTo drag the background, hold \"Shift\" while dragging with the left mouse button.";

        //For user-drawn routes: 
        private const string userDrawnRoute =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route, click the \"Done\" button,  or click the right mouse button.\r\nTo drag the background, hold \"Shift\" while dragging with the left mouse button.";

        //For road network routes: 
        private const string roadNetworkRoute =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route and snap to the road network, click the \"Done\"button, or click the right mouse button.\r\nTo drag the background, hold \"Shift\" while dragging with the left mouse button.";

        //For multiple lines & shapes: 
        private const string linesAndShapesMulti =
            "Click to add vertices or 'Ctrl'+click to remove last.\r\nTo complete your element, or click the right mouse button. To stop adding elements, click the \"Done\" button.\r\nTo drag the background, hold \"Shift\" while dragging with the left mouse button.";

        //For multiple user-drawn routes: 
        private const string userDrawnRouteMulti =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route, or click the right mouse button. To stop adding routes, click the \"Done\" button.\r\nTo drag the background, hold \"Shift\" while dragging with the left mouse button.";

        //For multiple road network routes: 
        private const string roadNetworkRouteMulti =
            "Click to add waypoints or 'Ctrl'+click to remove last.\r\nTo complete your route and snap to the road network, or click the right mouse button.\r\nTo stop adding routes, click the \"Done\" button.\r\nTo drag the background, hold \"Shift\" while dragging with the left mouse button.";


        public string PlaceElementHelperText
        {
            get { return (string)GetValue(PlaceElementHelperTextProperty); }
            set { SetValue(PlaceElementHelperTextProperty, value); }
        }
        // Using a DependencyProperty as the backing store for PlaceElementHelperText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlaceElementHelperTextProperty =
            DependencyProperty.Register("PlaceElementHelperText", typeof(string), typeof(AddElementsHelpingWindow), new UIPropertyMetadata(normalAdd));
        
        public AddElementsHelpingWindow()
        {
            InitializeComponent();
            Topmost = true;
            DataContextChanged += AddElementsHelpingDialog_DataContextChanged;
//            Activated += AddElementsHelpingWindow_Activated;
        }

        void AddElementsHelpingWindow_Activated(object sender, System.EventArgs e)
        {
//            if(Owner !=null)
//            {
//                Owner.Activate();
//            }
        }

        void AddElementsHelpingDialog_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldValue = e.OldValue as INotifyPropertyChanged;
            var newValue = e.NewValue as INotifyPropertyChanged;
            if (newValue != null)
            {
                newValue.PropertyChanged += DataContext_PropertyChanged;
            }
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= DataContext_PropertyChanged;
            }
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var castSender = sender as DepictionEnhancedMapViewModel;
            if (castSender == null) return;
            if (e.PropertyName.Equals("CurrentAddPrototype"))
            {
                var proto = castSender.CurrentAddPrototype;
                if (proto == null)
                {//Annotations
                    PlaceElementHelperText = normalAdd;
                    return;
                }
               
                Title = string.Format("Adding {0}", proto.DisplayName);
                if (proto.AddByZOI)
                {
                    var multiAdd = castSender.doMultiAdd;
                    if (proto.ElementType.Equals("Depiction.Plugin.RouteRoadNetwork"))
                    {
                        PlaceElementHelperText = multiAdd ? roadNetworkRouteMulti : roadNetworkRoute;
                    }
                    else if (proto.ElementType.Equals("Depiction.Plugin.RouteUserDrawn"))
                    {
                        PlaceElementHelperText = multiAdd ? userDrawnRouteMulti : userDrawnRoute;
                    }
                    else
                    {
                        PlaceElementHelperText = multiAdd ? linesAndShapesMulti : linesAndShapes;
                    }
                }
                else
                {
                    PlaceElementHelperText = normalAdd;
                }
            }
        }
    }
}
