﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Depiction.CoreModel.Service;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.View.Dialogs.AddElements
{
    /// <summary>
    /// Interaction logic for AddContentAdvanced.xaml
    /// </summary>
    public partial class AddContentAdvanced
    {
        public static readonly DependencyProperty IsNotLoadingProperty = DependencyProperty.Register("IsNotLoading", typeof(bool), typeof(AddContentAdvanced), new UIPropertyMetadata(true));
        public static readonly DependencyProperty IsUrlInvalidProperty = DependencyProperty.Register("IsUrlInvalid", typeof(bool), typeof(AddContentAdvanced), new UIPropertyMetadata(false));
        public static readonly DependencyProperty LayerNamesProperty = DependencyProperty.Register("LayerNames", typeof(ObservableCollection<string>), typeof(AddContentAdvanced), new UIPropertyMetadata(null));
        public static readonly DependencyProperty MapFormatsProperty = DependencyProperty.Register("MapFormats", typeof(ObservableCollection<string>), typeof(AddContentAdvanced), new UIPropertyMetadata(null));
        public static readonly DependencyProperty MapStylesProperty = DependencyProperty.Register("MapStyles", typeof(ObservableCollection<string>), typeof(AddContentAdvanced), new UIPropertyMetadata(null));
        public static readonly DependencyProperty WebUrlProperty = DependencyProperty.Register("WebUrl", typeof(string), typeof(AddContentAdvanced), new UIPropertyMetadata(null));
        DepictionWxsToElementService WebServiceController { get; set; }
        Dictionary<string, string> LayerNameDictionary { get; set; }
        public AddContentAdvanced()
        {
            InitializeComponent();
            showContentButton.Click += showContentButton_Click;
            webServiceTypeComboBox.ItemsSource = Enum.GetValues(typeof(ServerType));
            webServiceTypeComboBox.SelectedIndex = 0;
            webServiceTypeComboBox.SelectionChanged += webServiceTypeComboBox_SelectionChanged;
            webLayersListBox.SelectionChanged += webLayersComboBox_SelectionChanged;
            webServiceTextBox.TextChanged += webServiceTextBox_TextChanged;
            mapStyleBox.SelectionChanged += mapStyleBox_SelectionChanged;
            mapFormatsBox.SelectionChanged += mapFormatsBox_SelectionChanged;

            DataContextChanged += AddContentAdvanced_DataContextChanged;
            //            LayerNameDictionary = new Dictionary<string, string>();
        }

        void mapFormatsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WebServiceController != null && mapFormatsBox.SelectedItem != null)
            {
                WebServiceController.ImageFormat = mapFormatsBox.SelectedItem.ToString();
            }
        }

        void mapStyleBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WebServiceController != null && mapStyleBox.SelectedItem != null)
            {
                WebServiceController.Style = mapStyleBox.SelectedItem.ToString();
            }
        }

        void AddContentAdvanced_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newService = e.NewValue as DepictionWxsToElementService;
            WebServiceController = null;
           
            if (newService != null)
            {
                WebServiceController = newService;
                IsNotLoading = true;
            }else
            {
                IsNotLoading = false;
            }
        }
        void webServiceTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (LayerNames != null)
            {
                LayerNames.Clear();
                LayerNameDictionary.Clear();
                webLayersListBox.ItemsSource = null;
            }
            if (MapStyles != null)
                MapStyles.Clear();
            if (MapFormats != null)
                MapFormats.Clear();
            if (WebServiceController != null)
            {
                WebServiceController.ClearAdditionalInfo();
                WebServiceController.WebServiceType = webServiceTypeComboBox.SelectedItem.ToString();
            }
        }
//        http://137.227.242.85/ArcGIS/services/FWS_Wetlands_WMS/mapserver/wmsserver? 
        void webServiceTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (LayerNames != null)
            {
                LayerNames.Clear();
                LayerNameDictionary.Clear();
                webLayersListBox.ItemsSource = null;
            }
            if (MapStyles != null)
                MapStyles.Clear();
            if (MapFormats != null)
                MapFormats.Clear();
            if (WebServiceController != null)
            {
                WebServiceController.ClearAdditionalInfo();
                WebServiceController.ServiceURL = webServiceTextBox.Text;
            }
        }

        public ServerType ServerType
        {
            get { return (ServerType)webServiceTypeComboBox.SelectedItem; }
        }

        public ObservableCollection<string> MapStyles
        {
            get { return (ObservableCollection<string>)GetValue(MapStylesProperty); }
            set { SetValue(MapStylesProperty, value); }
        }

        public ObservableCollection<string> MapFormats
        {
            get { return (ObservableCollection<string>)GetValue(MapFormatsProperty); }
            set { SetValue(MapFormatsProperty, new ObservableCollection<string>(value.Where(r => r.Equals("image/png") || r.Equals("image/jpeg")))); }
        }

        public ObservableCollection<string> LayerNames
        {
            get { return (ObservableCollection<string>)GetValue(LayerNamesProperty); }
            set { SetValue(LayerNamesProperty, value); }
        }

        public string SelectedFormat
        {
            get { return MapFormats == null ? null : (string)mapFormatsBox.SelectedItem.ToString(); }
        }

        public string SelectedLayer
        {
            get
            {
                var item = webLayersListBox.SelectedItem;
                if (item is KeyValuePair<string, string>)
                {
                    return ((KeyValuePair<string, string>)item).Key;
                } return string.Empty;
            }
        }

        public string SelectedMapStyle
        {
            get { return MapStyles == null ? null : (string)mapStyleBox.SelectedItem.ToString(); }
        }

        public string WebUrl
        {
            get { return (string)GetValue(WebUrlProperty); }
            set { SetValue(WebUrlProperty, value); }
        }

        public bool IsNotLoading
        {
            get { return (bool)GetValue(IsNotLoadingProperty); }
            set { SetValue(IsNotLoadingProperty, value); }
        }

        public bool IsUrlInvalid
        {
            get { return (bool)GetValue(IsUrlInvalidProperty); }
            set { SetValue(IsUrlInvalidProperty, value); }
        }

        private void webLayersComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (WebServiceController != null)
            {
                var added = webLayersListBox.SelectedItems;
                if (added != null && added.Count > 0)
                {
                    WebServiceController.LayerNames = string.Empty;
                    bool pastFirst = false;

                    foreach (var name in added)
                    {
                        if (!(name is KeyValuePair<string, string>)) continue;
                        var pair = (KeyValuePair<string, string>)name;
                        if (!pastFirst)
                        {
                            pastFirst = true;
                            WebServiceController.FirstLayerTitle = pair.Key;
                        }
                        else
                        {
                            WebServiceController.LayerNames += ",";
                        }
                        WebServiceController.LayerNames += pair.Key;
                    }
                }
            }
            RaiseGetMapStyles(new EventArgs());
        }

        private void showContentButton_Click(object sender, RoutedEventArgs e)
        {
            RaiseGetLayers(new EventArgs());
        }


        private void RaiseGetLayers(EventArgs e)
        {

            //   http://www.pdc.org/wms/wmservlet/PDC_Active_Hazards?
            //http://129.194.231.185/cgi-bin/mapserv?map=/www/geodataportal/htdocs/mod_map/geo_wms_complete_all_years.map&SERVICE=WMS&VERSION=1.1.1&REQUEST=getcapabilities
            // http://portal.depiction.com/geoserver/wfs
            //http://wildfire.cr.usgs.gov/wmsconnector/com.esri.wms.Esrimap/geomac_wms
            //https://hazards.fema.gov/gis/nfhl/services/public/NFHLWMS/MapServer/WMSServer
            if (WebServiceController == null) return;
            IsNotLoading = false;
            BackgroundWorker layerGettingWorker = new BackgroundWorker();
            layerGettingWorker.DoWork += layerGettingWorker_DoWork;
            layerGettingWorker.RunWorkerCompleted += layerGettingWorker_RunWorkerCompleted;
            var args = new Dictionary<string, string>();
            args.Add("server", WebServiceController.ServiceURL);
            args.Add("type", ServerType.ToString());
            layerGettingWorker.RunWorkerAsync(args);
        }

        void layerGettingWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var args = e.Argument as Dictionary<string, string>;
            if (args == null) return;
            var server = args["server"];
            var type = args["type"];
            var layerNames = WebServiceController.GetLayerNamesFromServer(server, type);
            e.Result = layerNames;
        }

        void layerGettingWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var layerNames = e.Result as Dictionary<string, string>;
            if (layerNames == null)
            {
                IsNotLoading = true;
                return;
            }
            // TODO: Somehow determine if the server URL is valid.  If not, set sendingElement.IsUrlInvalid = true, to display a notice.
            if (layerNames.Count > 0)
            {
                LayerNameDictionary = layerNames;
                LayerNames = new ObservableCollection<string>(layerNames.Values);
                webLayersListBox.ItemsSource = LayerNameDictionary;// LayerNames;

                IsUrlInvalid = false;
            }
            else
            {
                IsUrlInvalid = true;
            }

            IsNotLoading = true;
        }

        private void RaiseGetMapStyles(EventArgs e)
        {
            GetFormats();
            GetMapStyles();
        }
        public void GetFormats()
        {

            IsNotLoading = false;
            if (WebServiceController == null) return;
            var mapFormats = WebServiceController.GetFormatsFromLayer(SelectedLayer);
            if (mapFormats != null && mapFormats.Count>0)
            {
                MapFormats = new ObservableCollection<string>(mapFormats);//this thing selects only jpeg and png
                mapFormatsBox.ItemsSource = MapFormats;
            }
            else
            {
                mapFormatsBox.ItemsSource = null;
            }
            IsNotLoading = true;
        }

        public void GetMapStyles()
        {
            IsNotLoading = false;
            if (WebServiceController == null) return;
            var mapStyles = WebServiceController.GetMapStylesFromLayer(SelectedLayer);
            if (mapStyles != null && mapStyles.Count >0)
            {
                MapStyles = new ObservableCollection<string>(mapStyles);
                mapStyleBox.ItemsSource = MapStyles;
            }
            else
            {
                mapStyleBox.ItemsSource = null;
            }

            IsNotLoading = true;
        }
    }
}