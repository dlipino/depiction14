﻿using System.Diagnostics;
using System.Windows.Navigation;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ChooseDisplayNameHeaderDialog.xaml
    /// </summary>
    public partial class EulaDialog
    {
        public EulaDialog()
        {
            InitializeComponent();
            IsVisibleChanged += EulaDialog_IsVisibleChanged;
            eulaDocument.FitToWidth();
//            eulaDocument.Zoom = 90;
//              if(DomainAccess.IsTrialMode)
//            {
//                var xpsDoc = loadDoc("DepictionReaderEULA.xps");
//                eulaDocument.Zoom = 90;//This is the zoom level that fits the whole page, it is kind of a hack since i couldn't get Fit to Width to work
//                eulaDocument.Document = xpsDoc.GetFixedDocumentSequence();
//            }else
//            {
//                var xpsDoc = loadDoc("DepictionEula.xps");
//                eulaDocument.Zoom = 90;//This is the zoom level that fits the whole page, it is kind of a hack since i couldn't get Fit to Width to work
//                eulaDocument.Document = xpsDoc.GetFixedDocumentSequence();
//            }
        }

        void EulaDialog_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if(e.NewValue.Equals(true))
            {
                eulaDocument.FitToWidth();
            }
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }
    }
}