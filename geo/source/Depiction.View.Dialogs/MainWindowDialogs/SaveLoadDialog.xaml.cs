﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Depiction.CoreModel;
using Depiction.CoreModel.DepictionPersistence;
using Depiction.Serialization;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for SaveLoadDialog.xaml
    /// </summary>
    public partial class SaveLoadDialog
    {
        private DepictionSaveLoadManager saveLoadManager = null;
        public SaveLoadDialog()
        {

            InitializeComponent();
            var depictApp = Application.Current as DepictionApplication;
            if (depictApp != null)
            {
                saveLoadManager = depictApp.saveLoadManager;
                saveLoadManager.SaveLoadProgressChanged += saveLoadManager_SaveLoadProgressChanged;
            }
            ShowInTaskbar = true;
            MaxWidth = 350;
            cancelButton.Click += cancelButton_Click;
        }

        void saveLoadManager_SaveLoadProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            var extraInfo = e.UserState;
            var currentProgress = saveLoadProgressBar.Value;
            var newProgress = e.ProgressPercentage;
            if (extraInfo != null)
            {
                progressLabel.Text = e.UserState.ToString();

            }
            if (e.ProgressPercentage.Equals(int.MaxValue))
            {
                saveLoadProgressBar.IsIndeterminate = true;
            }
            else if (e.ProgressPercentage != -1)
            {
                saveLoadProgressBar.IsIndeterminate = false;
                saveLoadProgressBar.Value = newProgress;
            }

        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            SerializationService.CancelLoadOrSave();
            progressLabel.Text = "Please wait while canceling completes";
        }

        protected override void OnClosed(EventArgs e)
        {
            SerializationService.CancelLoadOrSave();
            if (saveLoadManager != null)
                saveLoadManager.SaveLoadProgressChanged -= saveLoadManager_SaveLoadProgressChanged;
            saveLoadManager = null;
            saveLoadProgressBar.IsIndeterminate = false;
            base.OnClosed(e);
        }
        public override void OnApplyTemplate()
        {
            var close = (Button)Template.FindName("PART_Close", this);
            if (null != close)
            {
                close.IsEnabled = false;
                close.Visibility = Visibility.Collapsed;
            }
            base.OnApplyTemplate();
        }

        override protected void MainThreadShow(bool show)
        {
            if (Dispatcher != null && !Dispatcher.CheckAccess())
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action<bool>(MainThreadShow), show);
                return;
            }
            if (show)
            {
                ShowDialog(); //Show();// 
            }
            else
            {
                SerializationService.CancelLoadOrSave();
                DataContext = null;
                Close();

                if (Owner != null)
                {
                    Owner.Focus();
                }
            }
        }
    }
}
