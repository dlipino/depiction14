using System.Windows;
using Depiction.View.Dialogs.Properties;

namespace Depiction.View.Dialogs.MainWindowDialogs.MenuDialogs
{
    /// <summary>
    /// Interaction logic for FileMenu.xaml
    /// </summary>
    public partial class AdvancedMenu
    {
        public AdvancedMenu()
        {
            InitializeComponent();
            SetLocationToStoredValue();

        }
        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.AdvancedDialogRestoreBounds = RestoreBounds;
            }
            base.LocationOrSizeChanged_Updated();
        }
        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.AdvancedDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = WindowStartupLocation.Manual;
                var bounds = Settings.Default.AdvancedDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                //                Width = bounds.Width;
                //                Height = bounds.Height;
            }
        }
    }
}