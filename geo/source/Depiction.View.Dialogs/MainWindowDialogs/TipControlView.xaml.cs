﻿using System.Diagnostics;
using System.Windows.Navigation;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    public partial class TipControlView
    {
        public TipControlView()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                case ProductInformationBase.DepictionRW:
                case ProductInformationBase.OsintInformation:
                    MainPanel.Children.Remove(DefaultTextPanel);
                    break;
                default:
                    MainPanel.Children.Remove(PrepTextPanel);
                    break;
            }
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            var url = e.Uri.ToString();
            Process.Start(url);
        }
    }
}