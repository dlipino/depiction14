﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for AddinDisplayerDialog.xaml
    /// </summary>
    public partial class AddinDisplayerDialog
    {
        public AddinDisplayerDialog()
        {
            InitializeComponent();
            switch (DepictionAccess.ProductInformation.ProductType)
            {

                case ProductInformationBase.OsintInformation:
                    Title = string.Format("{0} Add-ons", DepictionAccess.ProductInformation.ProductName);
                    break;
            }
            DataContextChanged += AddinDisplayerDialog_DataContextChanged;
        }

        private void AddinDisplayerDialog_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
//            switch (DepictionAccess.ProductInformation.ProductType)
//            {
//                case ProductInformationBase.Debug:
//                    break;
//                default:
//                    var tabControl = AddonTabControl;
//                    var tabControlItem = DefaultAddonTab;
//                    if (tabControl != null && tabControlItem != null)
//                    {
//                        tabControl.Items.Remove(tabControlItem);
//                    }
//                    break;
//            }
#if !DEBUG
            //This only seems to work here. It does not work on the applyTemplateCall
            var tabControl =AddonTabControl;
            var tabControlItem = DefaultAddonTab;
            if(tabControl != null && tabControlItem != null)
            {
                tabControl.Items.Remove(tabControlItem);
            }
#endif
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)//Resharper LIES!!!!
        {
            Process.Start(e.Uri.ToString());

        }
    }
}
