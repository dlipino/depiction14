﻿using System;
using System.Windows;
using System.Windows.Media;
using Depiction.View.Dialogs.ColorComb;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for AnnotationEditingDialog.xaml
    /// </summary>
    public partial class AnnotationEditingDialog
    {

        public AnnotationEditingDialog()
        {
            InitializeComponent();
            Closed += AnnotationDialogClosed;
        }
        //Interesting why did resharper decide to remove the _Closed
        void AnnotationDialogClosed(object sender, EventArgs e)
        {
            var dc = DataContext as MapAnnotationViewModel;
            if (dc == null) return;
            dc.EditingProperties = false;
        }

        private void BtnOkClick(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as MapAnnotationViewModel;
            if (dc == null) return;
            var fColor = btnForeground.Background as SolidColorBrush;
            var bColor = btnBackground.Background as SolidColorBrush;
            var text = txtAnnotationText.Text;
            dc.AnnotationText = text;
            dc.BackgroundColor = bColor;
            dc.ForegroundColor = fColor;
            Close();
        }

        private void BtnDeleteClick(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as MapAnnotationViewModel;
            if (dc == null) return;
            Close();
        }

        private void BtnForegroundClick(object sender, RoutedEventArgs e)
        {
            var currentColor = btnForeground.Background as SolidColorBrush;
            var newColor = ColorPickerDialog.PickColorModal(currentColor);
            if (newColor != null) btnForeground.Background = newColor;
        }

        private void BtnBackgroundClick(object sender, RoutedEventArgs e)
        {
            var currentColor = btnBackground.Background as SolidColorBrush;
            var newColor = ColorPickerDialog.PickColorModal(currentColor);
            if (newColor != null) btnBackground.Background = newColor;
        }
    }
}