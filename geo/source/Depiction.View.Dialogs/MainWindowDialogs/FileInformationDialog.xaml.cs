﻿using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ChooseDisplayNameHeaderDialog.xaml
    /// </summary>
    public partial class FileInformationDialog
    {
        public FileInformationDialog()
        {
            InitializeComponent();
            ShowInfoButton = true;
            Title = "Story information";
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Reader:
                    titlePanel.IsEnabled = false;
                    authorPanel.IsEnabled = false;
                    descriptionPanel.IsEnabled = false;
                    break;
//                case ProductInformationBase.OsintInformation:
//                    Title = string.Format("{0} story information", DepictionAccess.ProductInformation.ProductName);
//                    break;
            }
        }
    }
}