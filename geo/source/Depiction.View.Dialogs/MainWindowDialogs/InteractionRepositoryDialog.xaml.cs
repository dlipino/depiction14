﻿using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.View.Dialogs.HelperClasses;
using Depiction.View.Dialogs.Properties;
using Depiction.ViewModels.DialogViewModels;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ElementDefinitionEditorDialogWindow.xaml
    /// </summary>
    public partial class InteractionRepositoryDialog
    {
        #region constructor
        public InteractionRepositoryDialog()
        {
            InitializeComponent();
            affectedElementsListBox.ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;
            affectedElementsListBox.SelectionChanged += affectedElementsListBox_SelectionChanged;
            SetLocationToStoredValue();
        }
        #endregion

        #region Location and size things
        
        protected override void LocationOrSizeChanged_Updated()
        {
            if (!RestoreBounds.IsEmpty)
            {
                Settings.Default.InteractionsEditorDialogRestoreBounds = RestoreBounds;
            }
        }

        public override void SetLocationToStoredValue()
        {
            if (!Settings.Default.InteractionsEditorDialogRestoreBounds.IsEmpty)
            {
                WindowStartupLocation = WindowStartupLocation.Manual;
                var bounds = Settings.Default.InteractionsEditorDialogRestoreBounds;
                Top = bounds.Top;
                Left = bounds.Left;
                Width = bounds.Width;
                Height = bounds.Height;
            }
            base.SetLocationToStoredValue();
        }
        #endregion

        void ItemContainerGenerator_StatusChanged(object sender, System.EventArgs e)
        {
            if (affectedElementsListBox.ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                affectedElementsListBox.SelectionChanged -= affectedElementsListBox_SelectionChanged;
                var dc = DataContext as InteractionRepositoryDialogViewModel;
                if (dc == null) return;
                //                affectedElementsListBox.SelectItems(dc.DisplayedInteraction.Subscribers.Cast<object>());
                affectedElementsListBox.SelectItems(dc.AffectedElements.Cast<object>());
                affectedElementsListBox.SelectionChanged += affectedElementsListBox_SelectionChanged;
            }
        }

        private void affectedElementsListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var dc = DataContext as InteractionRepositoryDialogViewModel;
            if (dc == null) return;
            if (dc.ModifiedAffectedElements != null)
            {
                dc.ModifiedAffectedElements = affectedElementsListBox.SelectedItems.Cast<IElementPrototype>();
            }
        }
    }
    public class ConditionsViewTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is KeyValuePair<string, HashSet<string>>)
                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("KeyValueTreeTemplate");
            if (item is KeyValuePair<string, IElementFileParameter[]>)
                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource("KeyValueTreeTemplate");
            if (item is IElementFileParameter)
            {
                return (DataTemplate)((FrameworkElement)container).FindResource("IElementFileParamaterTemplate");
            }
            //            if (item is MenuElementPropertyViewModel)
            //            {
            //                return (DataTemplate)((FrameworkElement)container).FindResource("PropertyDataTemplate");
            //            }
            return base.SelectTemplate(item, container);
        }
    }

}
