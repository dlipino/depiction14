﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;
using Depiction.API;
using Depiction.API.AbstractObjects;

namespace Depiction.View.Dialogs.MainWindowDialogs
{
    /// <summary>
    /// Interaction logic for ChooseDisplayNameHeaderDialog.xaml
    /// </summary>
    public partial class AboutDialog
    {
        public AboutDialog()
        {
            InitializeComponent();
            IsVisibleChanged += AboutDialog_IsVisibleChanged; 
            switch (DepictionAccess.ProductInformation.ProductType)
            {

                case ProductInformationBase.OsintInformation:
                    Title = string.Format("{0} Add-ons", DepictionAccess.ProductInformation.ProductName);
                    break;
            }
        }

        private void AboutDialog_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue.Equals(true))
            {
                aboutDocument.FitToWidth();
            }
        }

        private void requestNavigateHandler(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }
    }
}