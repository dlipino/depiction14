﻿using System.Windows;
using Depiction.View.Dialogs.LookLessElementAndRevealerDialogs;
using Depiction.ViewModelNew.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.HelperDialogs
{
    public class ElementImageRegistrationHelperControl : CommonControlFrame2
    {
        static ElementImageRegistrationHelperControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementImageRegistrationHelperControl), new FrameworkPropertyMetadata(typeof(ElementImageRegistrationHelperControl)));
        }
        
        protected override void DefaultCloseCancelOrders()
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc == null) return;
            dc.EndGeoLocateElementCommand.Execute(null);
        }
    }
}