﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.View.Dialogs.LookLessElementAndRevealerDialogs;
using Depiction.ViewModelNew.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.HelperDialogs
{
    public class ElementZOIEditHelpingDialog : CommonControlFrame2
    {
        static ElementZOIEditHelpingDialog()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ElementZOIEditHelpingDialog), new FrameworkPropertyMetadata(typeof(ElementZOIEditHelpingDialog)));
        }
       public int TopOffset { get; set; }

        #region Constructor
        public ElementZOIEditHelpingDialog()
        {
            TopOffset = 50;

            ShowInfoButton = true;
            AssociatedHelpFile = "edit-zoi.html";
            Loaded += dialog_Loaded;
            Closed += ElementZOIEditHelpingDialog_Closed;
        }

        #endregion

        void ElementZOIEditHelpingDialog_Closed(object sender, EventArgs e)
        {
            //Hack to deal with manage content delete while in edit mode
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc != null) dc.EndZOIEditCommand.Execute(null);
            //Hack to deal with focus race condition.
            TraversalRequest tReq = new TraversalRequest(FocusNavigationDirection.Next);
            MoveFocus(tReq);
        }

        protected override void DefaultCloseCancelOrders()
        {//Solves problem with binding removal
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc == null) return;
            dc.EndZOIEditCommand.Execute(null);
        }

        private void dialog_Loaded(object sender, RoutedEventArgs e)
        {
            CenterWidthAndGivenTop(TopOffset);
            var parent = Parent as FrameworkElement;
            if (parent != null)
            {
                parent.SizeChanged += Parent_SizeChanged;
            }
        }

        void Parent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (!IsVisible) return;
            if (Parent is FrameworkElement)
            {
                //                 var parentRect = new RectangleGeometry
                //                 {
                //                     Rect = LayoutInformation.GetLayoutSlot(((FrameworkElement)Parent))
                //                 };

                //var myRect = new RectangleGeometry(new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), ActualWidth, ActualHeight), 0, 0, RenderTransform);
                //if (!parentRect.Bounds.Contains(myRect.Bounds))
                //{
                CenterWidthAndGivenTop(TopOffset);
                //}
            }
        }

        protected override void Show()
        {
            BringToFront();
            Visibility = Visibility.Visible;
            Focus();
            var c = Parent as Canvas;
            if (Parent is Canvas && c != null)
            {

                var rect = LayoutInformation.GetLayoutSlot(c); //This sometimes returnins an empty (0 size) rect
                if (rect.Width == 0 && rect.Height == 0)
                {
                    rect = new Rect(0, 0, c.ActualWidth, c.ActualHeight);
                }

                var parentRect = new RectangleGeometry
                {
                    Rect = rect
                };

                var myRect = new RectangleGeometry(new Rect(Canvas.GetLeft(this), Canvas.GetTop(this), ActualWidth, ActualHeight), 0, 0, RenderTransform);
                if (!parentRect.Bounds.Contains(myRect.Bounds))
                {
                    CenterWidthAndGivenTop(TopOffset);
                }
            }
        }
    }
}
