﻿using System.ComponentModel;
using System.Windows;
//using System.Windows.Automation.Peers;//WHat was this for?
using System.Windows.Controls;

namespace Depiction.View.Dialogs.MainDialogParts
{
    /// <summary>
    /// </summary>
    public class ExpanderItemsControl : HeaderedItemsControl
    {
        #region Constructors

        static ExpanderItemsControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ExpanderItemsControl), new FrameworkPropertyMetadata(typeof(ExpanderItemsControl)));
        }

        #endregion

        #region Properties

        #region ExpandDirection

        /// <summary>
        /// ExpandDirection specifies to which direction the content will expand
        /// </summary> 
        [Bindable(true), Category("Behavior")]
        public ExpandDirection ExpandDirection
        {
            get { return (ExpandDirection)GetValue(ExpandDirectionProperty); }
            set { SetValue(ExpandDirectionProperty, value); }
        }

        /// <summary>
        /// The DependencyProperty for the ExpandDirection property. 
        /// </summary> 
        public static readonly DependencyProperty ExpandDirectionProperty =
            Expander.ExpandDirectionProperty.AddOwner(typeof(ExpanderItemsControl));

        #endregion ExpandDirection

        #region IsExpanded

        /// <summary>
        ///     The DependencyProperty for the IsExpanded property.
        /// </summary> 
        //[CommonDependencyProperty]
        public static readonly DependencyProperty IsExpandedProperty
            = Expander.IsExpandedProperty.AddOwner(typeof(ExpanderItemsControl),
                                                   new FrameworkPropertyMetadata(
                                                       true /* default value */,
                                                       FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal,
                                                       OnIsExpandedChanged)); 
        
        private static void OnIsExpandedChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var ep = (ExpanderItemsControl) d; 

            bool newValue = (bool) e.NewValue; 

            // Fire accessibility event
            //ExpanderAutomationPeer peer = UIElementAutomationPeer.FromElement(ep) as ExpanderAutomationPeer;
            //if(peer != null) 
            //{
            //    peer.RaiseExpandCollapseAutomationEvent(!newValue, newValue); 
            //} 

            if (newValue) 
            {
                ep.OnExpanded();
            }
            else 
            {
                ep.OnCollapsed(); 
            } 
        }

        /// <summary>
        /// </summary> 
        [Bindable(true), Category("Appearance")]
        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        #endregion IsExpanded

        #region IsHeaderVisibleWhenCollapsed

        /// <summary>
        ///     The DependencyProperty for the IsHeaderVisibleWhenCollapsed property.
        ///     Default Value: true
        /// </summary> 
        public static readonly DependencyProperty IsHeaderVisibleWhenCollapsedProperty =
            DependencyProperty.Register(
                "IsHeaderVisibleWhenCollapsed",
                typeof(bool),
                typeof(ExpanderItemsControl),
                new FrameworkPropertyMetadata(
                    true /* default value */,
                    FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal));

        /// <summary>
        /// </summary> 
        [Bindable(true), Category("Appearance")]
        public bool IsHeaderVisibleWhenCollapsed
        {
            get { return (bool)GetValue(IsHeaderVisibleWhenCollapsedProperty); }
            set { SetValue(IsHeaderVisibleWhenCollapsedProperty, value); }
        }

        #endregion IsHeaderVisibleWhenCollapsed

        /// <summary>
        /// Expanded event.
        /// </summary> 
        public static readonly RoutedEvent ExpandedEvent =
            Expander.ExpandedEvent.AddOwner(typeof(ExpanderItemsControl));

        /// <summary>
        /// Expanded event. It is fired when IsExpanded changed from false to true. 
        /// </summary>
        public event RoutedEventHandler Expanded 
        { 
            add { AddHandler(ExpandedEvent, value); }
            remove { RemoveHandler(ExpandedEvent, value); } 
        }

        /// <summary>
        /// Collapsed event. 
        /// </summary>
        public static readonly RoutedEvent CollapsedEvent = 
            Expander.CollapsedEvent.AddOwner(typeof(ExpanderItemsControl));

        /// <summary> 
        /// Collapsed event. It is fired when IsExpanded changed from true to false.
        /// </summary> 
        public event RoutedEventHandler Collapsed 
        {
            add { AddHandler(CollapsedEvent, value); } 
            remove { RemoveHandler(CollapsedEvent, value); }
        }

        #endregion Properties

        //-------------------------------------------------------------------- 
        // 
        //  Protected Methods
        // 
        //--------------------------------------------------------------------

        #region Protected Methods
 
        /// <summary>
        /// A virtual function that is called when the IsExpanded property is changed to true. 
        /// Default behavior is to raise an ExpandedEvent. 
        /// </summary>
        protected virtual void OnExpanded() 
        {
            RoutedEventArgs args = new RoutedEventArgs {RoutedEvent = (ExpandedEvent), Source = this};
            RaiseEvent(args);
        } 
 
        /// <summary>
        /// A virtual function that is called when the IsExpanded property is changed to false. 
        /// Default behavior is to raise a CollapsedEvent.
        /// </summary>
        protected virtual void OnCollapsed()
        { 
            RaiseEvent(new RoutedEventArgs(CollapsedEvent, this));
        } 
 
        #endregion    
    }
}