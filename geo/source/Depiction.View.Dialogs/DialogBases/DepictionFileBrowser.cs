﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.DialogBases;
using Depiction.API.HelperObjects;
using Depiction.API.MEFRepository;
using Depiction.API.Properties;
using Microsoft.Win32;

namespace Depiction.View.Dialogs.DialogBases
{
    public class DepictionFileBrowser
    {
        #region IFileBrowser Members

        public string ChooseFile(string defaultDirectory, FileFilter[] fileFilters, out FileFilter selectedFilter, string title)
        {
            int selectedFilterIndex;
            selectedFilter = null;
            string filter = GetFilters(fileFilters, out selectedFilterIndex);

            var dialog = new OpenFileDialog { Filter = filter, FilterIndex = selectedFilterIndex + 1, };
            if (string.IsNullOrEmpty(title)) dialog.Title = string.Format("Select {0} file to load", DepictionAccess.ProductInformation.StoryName);
            else dialog.Title = title;
            if (Directory.Exists(defaultDirectory)) dialog.InitialDirectory = defaultDirectory;

            if ((bool)dialog.ShowDialog())
            {
                selectedFilter = fileFilters[dialog.FilterIndex - 1];
                return dialog.FileName;
            }
            return null;
        }

        private static string GetFilters(FileFilter[] fileFilters, out int selectedFilterIndex)
        {
            string filter = string.Empty;
            selectedFilterIndex = 0;
            if (fileFilters.Length == 0) return filter;
            FileFilter selectedFilter = fileFilters.FirstOrDefault(f => f.Selected);
            selectedFilterIndex = fileFilters.Length - 1;

            for (int i = 0; i < fileFilters.Length; i++)
            {
                filter += string.Format("{0}|{1}|", fileFilters[i].Description, fileFilters[i].Wildcard);
                if (fileFilters[i] == selectedFilter) selectedFilterIndex = i;
            }

            filter = filter.Trim('|');
            filter += ";";
            return filter;
        }
        public string GetSaveFileName(string defaultDirectory, FileFilter[] fileFilters, out FileFilter selectedFilter)
        {
            return GetSaveFileName(defaultDirectory, fileFilters, out selectedFilter, null, null);
        }
        public string GetSaveFileName(string defaultDirectory, FileFilter[] fileFilters, out FileFilter selectedFilter,
            string initialFileName, string title)
        {
            selectedFilter = null;
            int selectedFilterIndex;
            string filter = GetFilters(fileFilters, out selectedFilterIndex);

            var dialog = new SaveFileDialog { Filter = filter, FilterIndex = selectedFilterIndex + 1 };
            if (string.IsNullOrEmpty(title)) title = "Select file name for a save";
            dialog.Title = title;
            if (string.IsNullOrEmpty(initialFileName)) initialFileName = "";
            dialog.FileName = initialFileName;
            if (Properties.ContainsKey("AddExtension"))
            {
                dialog.AddExtension = (bool)Properties["AddExtension"];
                dialog.DefaultExt = Properties.ContainsKey("DefaultExt") ? (string)Properties["DefaultExt"] : null;
            }

            if (defaultDirectory != null && Directory.Exists(defaultDirectory)) dialog.InitialDirectory = defaultDirectory;
            var dialogResult = dialog.ShowDialog();

            if (dialogResult == false)
            {
                return string.Empty;
            }
            selectedFilter = fileFilters[dialog.FilterIndex - 1];
            return dialog.FileName;
        }

        public Dictionary<string, object> Properties
        {
            get { return properties; }
        }
        private readonly Dictionary<string, object> properties = new Dictionary<string, object>();

        #endregion

        #region Methods for openeing the save/load dialogs

        private static readonly FileFilter[] fileOpenSaveDialogFilterString = new[]
                                                                                  {
                                                                                      FileFilter.CreateFileFilter(DepictionAccess.ProductInformation.FileExtension,
                                                                                                       DepictionAccess.ProductInformation.ProductName,true),
                                                                                       
                                                                                      new FileFilter
                                                                                          {
                                                                                              Description = "All Files (*.*)", 
                                                                                              Wildcard = "*.*"
                                                                                          } 
                                                                                  };

        #endregion

        #region getting and image file

        public string GetImageFileToLoad()
        {
            string fileToOpen;
            try
            {
                var loadDirectory = Settings.Default.LastIconDirectory;

                if (string.IsNullOrEmpty(loadDirectory) || !Directory.Exists(loadDirectory))
                {
                    loadDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                FileFilter selectedFilter;

                var filters = CreateAllImageFiltersFromReaderAddins();
                filters.Add(new FileFilter { Description = "All Files (*.*)", Wildcard = "*.*", Selected = true });

                fileToOpen = ChooseFile(loadDirectory, filters.ToArray(), out selectedFilter, "Select image file to open");
                if (string.IsNullOrEmpty(fileToOpen))
                {
                    return string.Empty;
                }

                if (!Path.HasExtension(fileToOpen))
                {
                    var message = "Cannot open this type of file.";
                    var title = "File type error";
                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                    return string.Empty;
                }

                Settings.Default.LastIconDirectory = Path.GetDirectoryName(fileToOpen);
                Settings.Default.Save();
            }
            finally
            {

            }
            return fileToOpen;
        }
        #endregion

        #region Depiction file loading/saving
        /// <summary>
        /// Get a depiction file name to load, an empty string return means the action was canceled
        /// </summary>
        /// <returns></returns>
        public string GetDepictionToLoad()
        {
            string fileToOpen;
            try
            {
                var loadDirectory = Settings.Default.LastDepictionLoadDirectory;

                if (string.IsNullOrEmpty(loadDirectory) || !Directory.Exists(loadDirectory))
                {
                    loadDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
                FileFilter selectedFilter;
                var loadTitle = string.Format("Select {0} to load", DepictionAccess.ProductInformation.StoryName);
                fileToOpen = ChooseFile(loadDirectory, fileOpenSaveDialogFilterString, out selectedFilter, loadTitle);
                if (string.IsNullOrEmpty(fileToOpen))
                {
                    return string.Empty;
                }

                if (!Path.HasExtension(fileToOpen))
                {
                    var message = "Cannot open this type of file.";
                    var title = "File type error";
                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                    return string.Empty;
                }

                var fileExtenstion = Path.GetExtension(fileToOpen);
                //var depictionProduct = new DepictionProductInformation();//Why would this be used?
                if (!fileExtenstion.Equals(DepictionAccess.ProductInformation.FileExtension, StringComparison.InvariantCultureIgnoreCase))
                {
                    if (fileExtenstion.Equals(DepictionAccess.ProductInformation.FileExtension, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // WARN that will not be able to save to DPN.
                        var title = string.Format("Opening file created by {0}", DepictionAccess.ProductInformation.ProductName);
                        var message = string.Format(
                            "This file was created by {0}, not {2}. You may open it, but will only be able to save it as a {1} file readable only by {2}; it will NOT be readable by {0}.\n\nDo you wish to continue?",
                            DepictionAccess.ProductInformation.ProductName,
                            DepictionAccess.ProductInformation.StoryName,
                            DepictionAccess.ProductInformation.ProductName);
                        var confirmer = DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title,
                                                                 MessageBoxButton.YesNo);
                        if (confirmer.Equals(MessageBoxResult.No))
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        var message = "Cannot open this type of file.";
                        var title = "File type error";
                        DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                        return string.Empty;
                    }
                }

                Settings.Default.LastDepictionLoadDirectory = Path.GetDirectoryName(fileToOpen);
                Settings.Default.Save();
            }
            finally
            {

            }
            return fileToOpen;
        }

        /// <summary>
        /// Get a depictino file name to save to. Empty means the action was cancelled
        /// </summary>
        /// <returns></returns>
        public string GetDepictionToSave()
        {
            var saveDirectory = Settings.Default.LastDepictionSaveDirectory;
            if (string.IsNullOrEmpty(saveDirectory))
            {
                saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            FileFilter selectedFilter;
            var currentDepiction = DepictionAccess.CurrentDepiction;
            var baseFileName = "";
            if (currentDepiction != null)
            {
                baseFileName = currentDepiction.DepictionsFileName;
            }

            var saveTitle = string.Format("Save {0}", DepictionAccess.ProductInformation.StoryName);
            var saveFileName = GetSaveFileName(saveDirectory, fileOpenSaveDialogFilterString, out selectedFilter, baseFileName, saveTitle);

            while (!string.IsNullOrEmpty(saveFileName) && saveFileName.Length > 245)
            {
                if (saveFileName.Length > 245)
                {
                    var message = string.Format(
                        "The path plus file name for your {1} file must be less than 245 characters long. The file path you chose is: {0}",
                        saveFileName, DepictionAccess.ProductInformation.StoryName);
                    var title = "File name length error";
                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                }
                saveFileName = GetSaveFileName(saveDirectory, fileOpenSaveDialogFilterString, out selectedFilter);
            }

            if (string.IsNullOrEmpty(saveFileName))
            {
                var title = "File name error";
                //                var message = string.Format("Save {0} dialog failed to return a file path.",
                //                                            ProductAndFolderService._productInformation.StoryName);
                //
                //                DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                return string.Empty;
            }
            Settings.Default.LastDepictionSaveDirectory = Path.GetDirectoryName(saveFileName);
            Settings.Default.Save();
            return saveFileName;
        }
        #endregion

        #region All non depcition file loading

        public KeyValuePair<string, IDepictionImporterBase> GetElementFileToLoad(string startLocation, FileFilter desiredFilter)
        {
            string fileToOpen;
            FileFilter selectedFilter;
            var defaultReturn = new KeyValuePair<string, IDepictionImporterBase>(startLocation, null);
            try
            {
                var loadDirectory = Settings.Default.LastFileLoadDirectory;

                if (!string.IsNullOrEmpty(startLocation))
                {
                    var startPath = startLocation;
                    if (File.Exists(startLocation))
                        startPath = Path.GetDirectoryName(startLocation);
                    if (Directory.Exists(startPath))
                    {
                        loadDirectory = startPath;
                    }
                }

                if (string.IsNullOrEmpty(loadDirectory) || !Directory.Exists(loadDirectory))
                {
                    loadDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }


                var allFilters = new List<FileFilter>();
                if (desiredFilter == null)
                {
                    allFilters = CreateAllFileReaderAddinFilters();
                    allFilters.Add(new FileFilter { Description = "All Files (*.*)", Wildcard = "*.*", Selected = true });
                }
                else
                {
                    desiredFilter.Selected = true;
                    allFilters.Add(desiredFilter);
                    if (!desiredFilter.Wildcard.Equals("*.*"))
                    {
                        allFilters.Add(new FileFilter { Description = "All Files (*.*)", Wildcard = "*.*", Selected = false });
                    }
                }

                fileToOpen = ChooseFile(loadDirectory, allFilters.ToArray(), out selectedFilter, "Select file to open");

                if (string.IsNullOrEmpty(fileToOpen))
                {
                    return defaultReturn;
                }

                if (!Path.HasExtension(fileToOpen))
                {
                    var message = "Cannot open this type of file.";
                    var title = "File type error";
                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                    return defaultReturn;
                }

                var fileExtenstion = Path.GetExtension(fileToOpen);

                //                if (!fileExtenstion.Equals(selectedFilter.E, StringComparison.InvariantCultureIgnoreCase))
                if (selectedFilter != null && !selectedFilter.Wildcard.ToLowerInvariant().Contains(fileExtenstion.ToLowerInvariant().Trim())
                    && !selectedFilter.Wildcard.Contains("*"))
                {
                    var message = "Cannot open this type of file.";
                    var title = "File type error";
                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                    return defaultReturn;
                }

                Settings.Default.LastFileLoadDirectory = Path.GetDirectoryName(fileToOpen);
                Settings.Default.Save();
            }
            finally
            {

            }
            if (selectedFilter != null)
            {
                return new KeyValuePair<string, IDepictionImporterBase>(fileToOpen, selectedFilter.FileIOType as IDepictionDefaultImporter);
            }
            return new KeyValuePair<string, IDepictionImporterBase>(fileToOpen, null);
        }

        public KeyValuePair<string, IDepictionElementExporter> GetElementFileToSaveWithExporter(string startLocation, FileFilter desiredFilter)
        {
            var saveDirectory = Settings.Default.LastFileSaveDirectory;
            if (!string.IsNullOrEmpty(startLocation))
            {
                if (Directory.Exists(startLocation))
                {
                    saveDirectory = startLocation;
                }
                else
                {
                    var startPath = Path.GetDirectoryName(startLocation);

                    if (Directory.Exists(startPath))
                    {
                        saveDirectory = startPath;
                    }
                }
            }
            if (string.IsNullOrEmpty(saveDirectory))
            {
                saveDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            var allFilters = new List<FileFilter>();
            if (desiredFilter == null) allFilters = CreateAllFileWriterAddinFilters();
            else
            {
                allFilters.Add(desiredFilter);
                desiredFilter.Selected = true;
                if (!desiredFilter.Wildcard.Equals("*.*"))
                {
                    allFilters.Add(new FileFilter { Description = "All Files (*.*)", Wildcard = "*.*", Selected = false });
                }
            }
            FileFilter selectedFilter;
            var saveFileName = GetSaveFileName(saveDirectory, allFilters.ToArray(), out selectedFilter);

            while (!string.IsNullOrEmpty(saveFileName) && saveFileName.Length > 245)
            {
                if (saveFileName.Length > 245)
                {
                    var message = string.Format(
                        "The path plus file name for your {1} file must be less than 245 characters long. The file path you chose is: {0}",
                        saveFileName, "file");
                    var title = "File name length error";
                    DepictionMessageBox.ShowDialog(Application.Current.MainWindow, message, title, MessageBoxButton.OK);
                }
                saveFileName = GetSaveFileName(saveDirectory, allFilters.ToArray(), out selectedFilter);
            }

            if (string.IsNullOrEmpty(saveFileName))
            {
                return new KeyValuePair<string, IDepictionElementExporter>(startLocation, null); ;
            }
            Settings.Default.LastFileSaveDirectory = Path.GetDirectoryName(saveFileName);
            Settings.Default.Save();
            if (selectedFilter != null)
            {
                return new KeyValuePair<string, IDepictionElementExporter>(saveFileName, selectedFilter.FileIOType as IDepictionElementExporter);
            }
            return new KeyValuePair<string, IDepictionElementExporter>(saveFileName, null);
        }

        #endregion

        #region writer addin stuff

        private static List<FileFilter> CreateAllFileWriterAddinFilters()
        {
            var filterList = new List<FileFilter>();
            var exporters = AddinRepository.Instance.GetExporters();
            //
            foreach (var exporter in exporters)
            {//TODO undo this 
                var fileInfo = exporter.Key.FileTypeInformation;
                if (!string.IsNullOrEmpty(fileInfo))
                {
                    filterList.Add(CreateFilterFromDescriptionAndExtensionList(fileInfo, exporter.Key.ExtensionsSupported, exporter.Value));
                }

            }
            return filterList;
        }
        #endregion

        #region reader addin stuff

        private static FileFilter CreateFilterFromDescriptionAndExtensionList(string inDescription, IEnumerable<string> extensions, IDepictionAddonBase reader)
        {
            var imageExtensions = string.Empty;
            var filterDescription = inDescription + " (";
            var count = 0;
            foreach (var type in extensions)
            {
                if (count != 0)
                {
                    imageExtensions += ";*" + type;
                    filterDescription += (", " + type);
                }
                else
                {
                    imageExtensions += ("*" + type);
                    filterDescription += type;
                }

                count++;
            }
            filterDescription += ")";
            var filter = new FileFilter { Description = filterDescription, Wildcard = imageExtensions, FileIOType = reader };
            return filter;
        }
        private static List<FileFilter> CreateAllFileReaderAddinFilters()
        {
            var filterList = new List<FileFilter>();
            var importers = AddinRepository.Instance.DefaultImporters;
            //
            foreach (var importer in importers)
            {
                if (string.IsNullOrEmpty(importer.Key.FileTypeInformation)) continue;
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        var typeInfoString = importer.Key.FileTypeInformation.ToLowerInvariant();
                        if (typeInfoString.Contains("elevation") || typeInfoString.Contains("kml") ||
                            typeInfoString.Contains("shape") || typeInfoString.Contains("roadnetwork")) continue;
                        break;
                }
                //#if PREP
                //                var typeInfoString = importer.Key.FileTypeInformation.ToLowerInvariant();
                //                if (typeInfoString.Contains("elevation") || typeInfoString.Contains("kml") ||
                //                    typeInfoString.Contains("shape") || typeInfoString.Contains("roadnetwork")) continue;
                //#endif  
                filterList.Add(CreateFilterFromDescriptionAndExtensionList(importer.Key.FileTypeInformation,
                   importer.Key.ExtensionsSupported, importer.Value));

            }
            var nonDefault = AddinRepository.Instance.NonDefaultImporters;
            //
            foreach (var importer in nonDefault)
            {
                if (string.IsNullOrEmpty(importer.Key.FileTypeInformation)) continue;
                filterList.Add(CreateFilterFromDescriptionAndExtensionList(importer.Key.FileTypeInformation,
                   importer.Key.ExtensionsSupported, importer.Value));

            }
            return filterList;
        }

        private static List<FileFilter> CreateAllImageFiltersFromReaderAddins()
        {
            var filterList = new List<FileFilter>();
            var importers = AddinRepository.Instance.DefaultImporters;

            foreach (var importer in importers)
            {
                var fileDescription = importer.Key.FileTypeInformation;
                if (string.IsNullOrEmpty(fileDescription)) continue;
                fileDescription = fileDescription.ToLowerInvariant();
                if (fileDescription.Contains("image"))
                {
                    filterList.Add(CreateFilterFromDescriptionAndExtensionList(fileDescription, importer.Key.ExtensionsSupported, importer.Value));
                }
            }
            var nonDefaultImporters = AddinRepository.Instance.NonDefaultImporters;

            foreach (var importer in nonDefaultImporters)
            {
                var fileDescription = importer.Key.FileTypeInformation;
                if (string.IsNullOrEmpty(fileDescription)) continue;
                fileDescription = fileDescription.ToLowerInvariant();
                if (fileDescription.Contains("image"))
                {
                    filterList.Add(CreateFilterFromDescriptionAndExtensionList(fileDescription, importer.Key.ExtensionsSupported, importer.Value));
                }
            }
            return filterList;
        }
        #endregion

        #region helper methods


        #endregion

    }
}