﻿using System.Windows.Input;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.View.Dialogs.ElementZOIEditing
{
    /// <summary>
    /// Interaction logic for ElementZOIEditHelpingWindow.xaml
    /// </summary>
    public partial class ElementZOIEditHelpingWindow
    {
        public ElementZOIEditHelpingWindow()
        {
            InitializeComponent();
        }
        override protected void SoftCloseWindow()
        {
            var dc = DataContext as DepictionEnhancedMapViewModel;
            if (dc != null) dc.EndZOIEditCommand.Execute(null);
            if (dc != null) dc.EndZOIEditCommand.Execute(null);
            //Hack to deal with focus race condition.
            TraversalRequest tReq = new TraversalRequest(FocusNavigationDirection.Next);
            MoveFocus(tReq);
            base.SoftCloseWindow();
            
        }
    }
}
