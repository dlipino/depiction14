﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Depiction.API.HelperObjects;
using Depiction.API.StaticAccessors;
using Depiction.View.Dialogs.DialogBases;

namespace Depiction.View.Dialogs.IconSelector
{
    public partial class IconSelectorDialog : INotifyPropertyChanged
    {//SOOOOOO tired, not sure how much of this is actually good code . . .

        public event PropertyChangedEventHandler PropertyChanged;
        private DepictionIconPath selectedDefaultIconPath;
        private DepictionIconPath selectedUserIconPath;
        public DepictionIconPath SelectedDefaultIconPath
        {
            get { return selectedDefaultIconPath; }
            set { selectedDefaultIconPath = value; NotifyPropertyChanged("SelectedDefaultIconPath"); }
        }
        public DepictionIconPath SelectedUserIconPath
        {
            get { return selectedUserIconPath; }
            set { selectedUserIconPath = value; NotifyPropertyChanged("SelectedUserIconPath"); }
        }
        public IconSelectionType IconSelectionTypeValue { get; set; }
        public Dictionary<DepictionIconPath, ImageSource> UserImageDictionary
        {
            get
            {
                //Hack, not very efficient but should work
                var propData = ProductAndFolderService.UserImageDictionary;
                var sortedProps =
                     from prop in propData
                     orderby prop.Key.Path.ToLowerInvariant() ascending
                     select prop.Key;
                var orderedDict = new Dictionary<DepictionIconPath, ImageSource>();
                foreach(var key in sortedProps)
                {
                    orderedDict.Add(key, propData[key]);
                }
                return orderedDict;
            }
        }
        #region dep props


        public string IconFileName
        {
            get { return (string)GetValue(IconFileNameProperty); }
            set { SetValue(IconFileNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IconFileName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IconFileNameProperty =
            DependencyProperty.Register("IconFileName", typeof(string), typeof(IconSelectorDialog), new UIPropertyMetadata(string.Empty));



        #endregion

        #region COnstructor

        public IconSelectorDialog()
        {
            InitializeComponent();
            UserResources.Checked += ResourceButton_Checked;
            AppResources.Checked += ResourceButton_Checked;
            LoadResources.Checked += ResourceButton_Checked;

            cancelButton.Click += cancelButton_Click;
            okButton.Click += okButton_Click;
            browseButton.Click += browseButton_Click;
            Focusable = true;
            Topmost = true;
        }

        #endregion
        #region evetn methods

        void okButton_Click(object sender, RoutedEventArgs e)
        {
            //            //Copy the file to the user icons path, this might always be accurate, but it will work everytime for 
            //            if (IconSelectionTypeValue == IconSelectionType.File && DepictionAccess.PathService != null)
            //            {
            //                if(File.Exists(IconFileName))
            //                {
            //                    var fileName = Path.GetFileName(IconFileName);
            //                    File.Copy(IconFileName, Path.Combine(DepictionAccess.PathService.UserElementIconDirectoryPath, fileName));
            //                }
            //            }

            DialogResult = true;
        }

        void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        void browseButton_Click(object sender, RoutedEventArgs e)
        {
            var fileBrowser = new DepictionFileBrowser();
            var file = fileBrowser.GetImageFileToLoad();
            IconFileName = file;
        }

        void ResourceButton_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)AppResources.IsChecked)
            {
                IconSelectionTypeValue = IconSelectionType.Default;

            }
            else if ((bool)UserResources.IsChecked)
            {
                IconSelectionTypeValue = IconSelectionType.User;

            }
            else if ((bool)LoadResources.IsChecked)
            {
                IconSelectionTypeValue = IconSelectionType.File;
            }
        }

        #endregion
        #region static creator

        static public DepictionIconPath SelectIconSource(DepictionIconPath originalPath)
        {
            try
            {
                var mainWindow = Application.Current.MainWindow;

                var iconSelector = new IconSelectorDialog { Owner = mainWindow };
                iconSelector.SelectedDefaultIconPath = originalPath;
                iconSelector.SelectedUserIconPath = originalPath;
                iconSelector.SetToFileSelection(originalPath);

                bool? dialogResult = iconSelector.ShowDialog();

                if ((bool)dialogResult)
                {
                    var result = iconSelector.SelectedDefaultIconPath;

                    var selectionType = iconSelector.IconSelectionTypeValue;
                    if (Equals(selectionType, IconSelectionType.User))
                    {
                        result = iconSelector.SelectedUserIconPath;
                    }
                    else if (Equals(selectionType, IconSelectionType.File))
                    {
                        var fileName = iconSelector.IconFileName;
                        result = new DepictionIconPath(IconPathSource.File, fileName);
                    }
                    return result;
                }
            }

            catch (Exception ex)
            {
                return null;
            }
            return null;
        }

        #endregion
        public void SetToFileSelection(DepictionIconPath filePath)
        {
            if (filePath.Source.Equals(IconPathSource.File))
            {
                if (UserImageDictionary.ContainsKey(filePath))
                {
                    UserResources.IsChecked = true;
                }
                else
                {
                    LoadResources.IsChecked = true;
                    IconFileName = filePath.Path;
                }
            }
        }

        #region Implementation of INotifyPropertyChanged

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        public enum IconSelectionType
        {
            Default,
            User,
            File
        }
    }

}
