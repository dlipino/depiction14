﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Depiction.CoreModel;
using Depiction.ViewModels.ViewModels;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.View.Dialogs.HelperClasses
{
    /// <summary>
    /// Interaction logic for CommonDataGrid.xaml
    /// </summary>
    public partial class CommonDataGrid
    {
        private bool groupNodesNeedUpdating = true;

        #region Dependency props
        

        public bool UseExpandedTreeView
        {
            get { return (bool)GetValue(UseExpandedTreeViewProperty); }
            set { SetValue(UseExpandedTreeViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for UseExpandedTreeView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UseExpandedTreeViewProperty =
            DependencyProperty.Register("UseExpandedTreeView", typeof(bool), typeof(CommonDataGrid), new UIPropertyMetadata(true));
        

        #endregion
        #region Constructor

        public CommonDataGrid()
        {
            InitializeComponent();
            ItemTemplateSelector = new TreeTemplateSelector();
            DataContextChanged += CommonDataGrid_DataContextChanged;
            LayoutUpdated += ItemsPresenter_LayoutUpdated;
        }

        #endregion

        #region even connecting

        void CommonDataGrid_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var oldDC = e.OldValue as ElementSelectionListViewModel;
            var newDC = e.NewValue as ElementSelectionListViewModel;
            if (oldDC != null)
            {
                oldDC.PropertyChanged -= ElementSelectionListViewModel_PropertyChanged;
            }
            if (newDC != null)
            {
                newDC.PropertyChanged += ElementSelectionListViewModel_PropertyChanged;
            }
        }

        private void ItemsPresenter_LayoutUpdated(object sender, EventArgs e)
        {
            if (groupNodesNeedUpdating && IsVisible)
            {
                UpdateAllGroupParentCheckBoxes();
            }
        }
        private void ElementSelectionListViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            if (propName.Equals("ElementBindList", StringComparison.InvariantCultureIgnoreCase))
            {
                groupNodesNeedUpdating = true;
            }
            else if (propName.Equals("SelectionFilter", StringComparison.InvariantCultureIgnoreCase))
            {
                UpdateAllGroupParentCheckBoxes();
            }
            else if (propName.Equals("DisplayerToControl", StringComparison.InvariantCultureIgnoreCase))
            {
                groupNodesNeedUpdating = true;
            }
        }

        #endregion

        #region public helpers
        public void UpdateAllGroupParentCheckBoxes()
        {
            groupNodesNeedUpdating = true;
            foreach (var singleItem in Items)
            {
                var item = singleItem as CollectionViewGroup;
                if (item == null) continue;
                CheckAndSetStateOfCollectionViewGroup(item);
            }
            groupNodesNeedUpdating = false;
            var dc = DataContext as ElementSelectionListViewModel;
            if (dc == null) return;
            dc.UpdateSelectedCount();
        }
        #endregion
        #region private methods
        private void TreeViewItemLoaded(object sender, RoutedEventArgs e)
        {
            if (!(sender is TreeViewItem)) return;
            var tvi = (TreeViewItem)sender;
            if (!(tvi.DataContext is CollectionViewGroup)) return;

            CheckAndSetStateOfCollectionViewGroup((CollectionViewGroup)tvi.DataContext);//, tvi);
        }
        private void CheckNodeState(object sender, RoutedEventArgs e)
        {
            var checkBox = sender as CheckBox;
            if (checkBox == null) return;
            var dataContext = checkBox.DataContext;
            var isChecked = checkBox.IsChecked;


            bool selected = false;
            var elementChanges = new HashSet<string>();
            if (dataContext is CollectionViewGroup)
            {
                var group = dataContext as CollectionViewGroup;

                foreach (MenuElementViewModel item in group.Items)
                {
                    if (item != null)
                    {
                        selected = (bool)isChecked;
                        item.IsSelected = selected;
                        elementChanges.Add(item.ElementID);
                    }
                }
                UpdateAllGroupParentCheckBoxes();
            }
            else if (dataContext is MenuElementViewModel)
            {
                var elemVM = (MenuElementViewModel)dataContext;
                elemVM.IsSelected = (bool)isChecked;
                selected = elemVM.IsSelected;
                elementChanges.Add(elemVM.ElementID);
                UpdateAllGroupParentCheckBoxes();
            }

            var app = Application.Current as DepictionApplication;
            if(app == null) return;
            var currentDepiction = app.CurrentDepiction;
            if(currentDepiction == null) return;
            var mainDC = DataContext as ElementViewSelectionListViewModel;
            if (mainDC == null) return;
            var displayerToControl = mainDC.DisplayerToControl;
            if (displayerToControl == null) return;
            var elementModels = currentDepiction.GetElementsWithIds(elementChanges.ToList());
            if (selected)
            {
                displayerToControl.AddElementListToModel(elementModels);
            }
            else
            {
                displayerToControl.RemoveElementListFromModel(elementModels);
            }
        }

        private void CheckAndSetStateOfCollectionViewGroup(CollectionViewGroup group)
        {
            TreeViewItem topNode = ItemContainerGenerator.ContainerFromItem(group) as TreeViewItem;
            if (group == null || topNode == null) return;

            bool? value = false;
            var firstChildSelection = false;
            //A linq call might work here var selectionTypes = group.Items.Distinct();

            for (int idx = 0; idx < group.Items.Count; ++idx)
            {
                var isChildSelected = ((MenuElementViewModel)group.Items[idx]).IsSelected;

                if (idx == 0)
                {
                    firstChildSelection = isChildSelected;
                    value = isChildSelected;
                }
                else if (firstChildSelection != isChildSelected)
                {
                    value = null;
                    break;
                }
            }
            topNode.Tag = value;
            groupNodesNeedUpdating = false;

        }
        #endregion
//        //Very clever mr. overland (at least i think you added this to commondatagrid)
//        public event EventHandler ElementDoubleClicked;
//
//        private void LeafDoubleClicked(object sender, MouseButtonEventArgs e)
//        {
//            if (ElementDoubleClicked != null)
//                ElementDoubleClicked(sender, e);
//        }
    }

    public class TreeTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is CollectionViewGroup)
                return (HierarchicalDataTemplate)((FrameworkElement)container).FindResource(new DataTemplateKey(typeof(CollectionViewGroup)));
            if (item is MenuElementViewModel)
            {
                return (DataTemplate)((FrameworkElement)container).FindResource("MenuElementView");
            }
            return base.SelectTemplate(item, container);
        }
    }


}