﻿using System.Windows;
using System.Windows.Media;

namespace Depiction.View.Dialogs.LicenseDialogs
{
    /// <summary>
    /// Interaction logic for ProxyCheckWindow.xaml
    /// </summary>
    public partial class ProxyCheckWindow : IProxyTestWindow
    {
        [System.Runtime.InteropServices.DllImport("msvcrt.dll")]
        public static extern int _controlfp(int IN_New, int IN_Mask);
        private int DefaultCW;

        public bool returnValue;
        public string UserName
        {
            get { return (string)GetValue(UserNameProperty); }
            set { SetValue(UserNameProperty, value); }
        }

        public static  readonly DependencyProperty UserNameProperty =
            DependencyProperty.Register("UserName", typeof(string), typeof(ProxyCheckWindow), new UIPropertyMetadata(null));

        //public bool GetUseProxy()
        //{
          
        //}
        //public bool GetUseProxyAuth()
        //{
            
        //}
        //public string GetProxyServer()
        //{
            
        //}
        //public short GetProxyServerPort()
        //{
            
        //}
        //public string GetUserName()
        //{
            
        //}
        //public string GetUserPassword()
        //{
            
        //}


        public bool UseProxy
        {
            get { return (bool)GetValue(UseProxyProperty); }
            set { SetValue(UseProxyProperty, value); }
        }
        public static readonly DependencyProperty UseProxyProperty =
            DependencyProperty.Register("UseProxy", typeof(bool), typeof(ProxyCheckWindow), new UIPropertyMetadata(null));

        public bool UseProxyAuth
        {
            get { return (bool)GetValue(UseProxyAuthProperty); }
            set { SetValue(UseProxyAuthProperty, value); }
        }
        public static readonly DependencyProperty UseProxyAuthProperty =
            DependencyProperty.Register("UseProxyAuth", typeof(bool), typeof(ProxyCheckWindow), new UIPropertyMetadata(null));

        public string ProxyServer
        {
            get { return (string)GetValue(ProxyServerProperty); }
            set { SetValue(ProxyServerProperty, value); }
        }

        public static readonly DependencyProperty ProxyServerProperty =
            DependencyProperty.Register("ProxyServer", typeof(string), typeof(ProxyCheckWindow), new UIPropertyMetadata(null));

        public double ProxyServerPort
        {
            get { return (double)GetValue(ProxyServerPortProperty); }
            set { SetValue(ProxyServerPortProperty, value); }
        }

        public static readonly DependencyProperty ProxyServerPortProperty =
            DependencyProperty.Register("ProxyServerPort", typeof(double), typeof(ProxyCheckWindow), new UIPropertyMetadata(null));

        public string CheckPointURL
        {
            get;
            set;
        }

        public string PasswordString { get { return passwordText.Password; } }

        private int quickTestReturnCode;
        public int QuickTestReturnCode { get { return quickTestReturnCode; } set { quickTestReturnCode = value; } }

        public ProxyCheckWindow()
        {
            InitializeComponent();
            btnTestConnection.Click += btnTestConnection_Click;
            btnOk.Click += btnOk_Click;
            btnCancel.Click += btnCancel_Click;
            DefaultCW = _controlfp(0, 0);

            // Insert code required on object creation below this point.
        }

        void btnOk_Click(object sender, RoutedEventArgs e)
        {
            returnValue = true;
            Close();
        }
        void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            returnValue = false;
            Close();

        }
        void btnTestConnection_Click(object sender, RoutedEventArgs e)
        {
            SSCProtectorClass SSCPConnectTest = new SSCProtectorClass();
            _controlfp(DefaultCW, 0xfffff);

            bool useProxy = UseProxy;

            SSCPConnectTest.QuickTestCheckPointConnection(CheckPointURL,
                                                          UseProxy,
                                                          ProxyServer,
                                                          (short)ProxyServerPort,
                                                          UseProxyAuth,
                                                          UserName,
                                                          PasswordString,
                                                          out quickTestReturnCode);
            _controlfp(DefaultCW, 0xfffff);



            //Gives the user a visual confirmation if a connection has been established or not
            if ((RETURNCODES)QuickTestReturnCode == RETURNCODES.TRUE_CONNECTED)
            {
                connectionFailureLabel.Content = "Connection Success";
                connectionFailureLabel.Foreground = Brushes.Green;
//                ConnectStat.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                connectionFailureLabel.Content = "Connection Failure";
                connectionFailureLabel.Foreground = Brushes.Red;
//                ConnectStat.Text = "Connection Failure";
//                ConnectStat.ForeColor = System.Drawing.Color.Red;
            }
        }


        bool? IProxyTestWindow.ShowDialog()
        {
            ShowDialog();
            return returnValue;
        }
    }
}