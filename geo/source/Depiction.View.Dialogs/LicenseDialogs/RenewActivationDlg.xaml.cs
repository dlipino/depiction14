using System;
using System.ComponentModel;
using System.Windows;

namespace Depiction.View.Dialogs.LicenseDialogs
{
    /// <summary>
    /// Interaction logic for InputSerialNumberWindow.xaml
    /// </summary>
    public partial class RenewActivationDlg 
    {
        #region Events
//
        public event Action OkClicked;
        public event Action RequestManualActivationClicked;
        public event Action ManuallyRenewButtonClicked;
        #endregion

        public RenewActivationDlg()
        {
            InitializeComponent();
            authorizationCodeText.TextChanged += ActivateDepictionWindow.serialNumberText_TextChanged;
            manualRenewalText.TextChanged += ActivateDepictionWindow.serialNumberText_TextChanged;
        }

        public string AuthorizationCode
        {
            get { return (string)GetValue(AuthorizationCodeProperty); }
            set { SetValue(AuthorizationCodeProperty, value); }
        }



        public string ManualRenewalCode
        {
            get { return (string)GetValue(ManualRenewalCodeProperty); }
            set { SetValue(ManualRenewalCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ManualRenewalCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ManualRenewalCodeProperty =
            DependencyProperty.Register("ManualRenewalCode", typeof(string), typeof(RenewActivationDlg), new UIPropertyMetadata(null));



        public static readonly DependencyProperty AuthorizationCodeProperty =
            DependencyProperty.Register("AuthorizationCode", typeof(string), typeof(RenewActivationDlg), new UIPropertyMetadata(null));

        public string HelperText
        {
            get { return helperMessage.Text; }
            set { helperMessage.Text = value; }
        }


        private void RequestManualActivationButton_Click(object sender, RoutedEventArgs e)
        {
            if (RequestManualActivationClicked != null)
                RequestManualActivationClicked();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (OkClicked != null)
                OkClicked();
        }

        private void btnManuallyActivate_Click(object sender, RoutedEventArgs e)
        {
            if (ManuallyRenewButtonClicked != null)
                ManuallyRenewButtonClicked();

        }

    }
}