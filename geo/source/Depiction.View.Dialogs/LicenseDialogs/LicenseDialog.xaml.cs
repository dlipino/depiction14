﻿using System.ComponentModel;
using System.Windows;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModels.DialogViewModels;

namespace Depiction.View.Dialogs.LicenseDialogs
{
    /// <summary>
    /// Interaction logic for LicenseDialog.xaml
    /// </summary>
    public partial class LicenseDialog
    {
        public LicenseDialog()
        {
            InitializeComponent();
            DataContextChanged += LicenseDialog_DataContextChanged;
        }
        void LicenseDialog_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newDC = e.NewValue as LicenseDialogVM;
            var oldDC = e.OldValue as LicenseDialogVM;
            if (oldDC != null)
            {
                oldDC.PropertyChanged -= DataContext_PropertyChanged;
            }
            if (newDC == null) return;
            newDC.PropertyChanged += DataContext_PropertyChanged;
        }

        private void DataContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var abVM = sender as IDepictionDialogVM;
            if (abVM == null) return;
            var prop = e.PropertyName;
            if (prop.Equals("IsDialogVisible"))
            {
                if (abVM.IsDialogVisible)
                {
                    Show();
                }
                else
                {
                    Hide();
                }
            }
        }
    }
}