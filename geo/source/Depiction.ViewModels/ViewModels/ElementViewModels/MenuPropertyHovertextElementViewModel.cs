using System.ComponentModel;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MenuPropertyHovertextElementViewModel : ViewModelBase
    {
        internal IDepictionElementBase OwningElementModel { get; set; }
        internal IElementProperty PropertyModel { get; set; }
        #region Variables
        
        private bool hovertextChanged;
        private bool isHoverText;

        #endregion

        #region Properties
        public string PropertyTypeString { get { return PropertyModel.ValueType.Name; } }
        public string DisplayName { get; private set; }
        public bool HoverTextChanged { get { return hovertextChanged; } set { hovertextChanged = value; NotifyPropertyChanged("HoverTextChanged"); } }
        public bool IsHoverText
        {
            get { return isHoverText; }
            set
            {
                if (!Equals(PropertyModel.IsHoverText, value))
                {
                    HoverTextChanged = true;
                }
                isHoverText = value; NotifyPropertyChanged("IsHoverText");
            }
        }
        #endregion

        #region Constructor

        public MenuPropertyHovertextElementViewModel(IElementProperty inPropertyModel, IDepictionElementBase owningElement)
        {
            OwningElementModel = owningElement;
            PropertyModel = inPropertyModel;
            DisplayName = inPropertyModel.DisplayName;
            isHoverText = PropertyModel.IsHoverText;
            PropertyModel.PropertyChanged += PropertyModel_PropertyChanged;
        }

        #endregion

        #region destruction

        protected override void OnDispose()
        {
            PropertyModel.PropertyChanged -= PropertyModel_PropertyChanged;
            OwningElementModel = null;
            PropertyModel = null; 
            base.OnDispose();
        }

        #endregion
        
        #region Methods
        void PropertyModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var propName = e.PropertyName;
            if (propName.Equals("IsHoverText"))
            {
                IsHoverText = PropertyModel.IsHoverText;
            }
            if(propName.Equals("Value") && IsHoverText)
            {
                hovertextChanged = true;
            }
        }

        #endregion
    }
}