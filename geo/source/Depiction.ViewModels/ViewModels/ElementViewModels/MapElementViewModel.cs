﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.API.ValueTypeConverters;
using Depiction.API.ValueTypes;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.ViewModels.ViewModelInterfaces;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MapElementViewModel : ViewModelBase, IMapDraggable //can this derive from manage content viewmodel?
    {
        public IDepictionElement ElementModel { get; set; }
        #region semi hack properties
        public bool ElementUpdated
        {
            get
            {
                var hackModel = ElementModel as DepictionElementParent;
                if (hackModel != null)
                {
                    return hackModel.ElementUpdated;
                }
                return false;
            }
            set
            {
                var hackModel = ElementModel as DepictionElementParent;
                if (hackModel != null)
                {
                    hackModel.ElementUpdated = value;
                }
            }
        }
        #endregion

        #region Variables

        private string toolTipText;
        private string enhancedToolTipText;
        private bool isInEditMode;
        private Visibility viewVisibility = Visibility.Visible;
        private double modelIconSize;
        #endregion

        #region Properties

        public string ElementKey { get { return ElementModel.ElementKey; } }

        public string DisplayName
        {
            get
            {
                string name;
                ElementModel.GetPropertyValue("DisplayName", out name);
                return name;
            }
        }

        //So it looks like the MapPixelLocation and ElementImagePosition are just about the same thing...hopefully
        public TranslateTransform ElementImagePosition { get; private set; }
        public Point MapPixelLocation
        {
            get
            {
                if (ElementModel.IsGeolocated)
                {
                    LatitudeLongitude latLong;
                    if (ElementModel.GetPropertyValue("Position", out latLong))
                    {
                        if (DepictionAccess.GeoCanvasToPixelCanvasConverter == null) return new Point();
                        return DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(latLong);
                    }
                }
                return new Point();
            }
        }
        public bool IsViewHittestable
        {
            get
            {
                if (ElementModel.ElementType.Equals("Depiction.Plugin.RoadNetwork"))
                {
                    return false;
                }
                return true;
            }
        }
        public bool IsElementPreview { get; set; }
        public bool IsElementInEditMode
        {
            get { return isInEditMode; }
            set
            {
                isInEditMode = value;
                NotifyPropertyChanged("IsElementInEditMode");
                NotifyPropertyChanged("IconVisibility");
            }
        }

        //Temp hack for dealing with waypoint draggablility issues
        public bool IsElementIconDraggable
        {
            get
            {
                bool val;
                var result = ElementModel.GetPropertyValue("IconDraggable", out val);
                if (result) return val;
                return IsElementZOIDraggable;
            }
        }
        public bool IsElementZOIDraggable
        {
            get
            {
                bool val;
                var result = ElementModel.GetPropertyValue("Draggable", out val);
                if (result) return val;
                return false;
            }
        }

        public bool Active
        {
            get
            {
                bool val;
                var result = ElementModel.GetPropertyValue("Active", out val);
                if (result) return val;
                return false;
            }
        }

        public Visibility ViewVisibility { get { return viewVisibility; } set { viewVisibility = value; NotifyPropertyChanged("ViewVisibility"); } }
        public bool UsePermaText { get { return ElementModel.UsePermaText; } set { ElementModel.UsePermaText = value; NotifyPropertyChanged("UsePermaText"); } }
        public bool UseEnhancedPermaText
        {
            get { return ElementModel.UseEnhancedPermaText; }
            set { ElementModel.UseEnhancedPermaText = value; NotifyPropertyChanged("UseEnhancedPermaText"); }
        }
        public bool UsePropertyNameInHoverText
        {
            get { return ElementModel.UsePropertyNameInHoverText; }
            set { ElementModel.UsePropertyNameInHoverText = value; NotifyPropertyChanged("UsePropertyNameInHoverText"); }
        }

        protected bool ShowIcon
        {
            get
            {
                bool showIcon;
                if (ElementModel.GetPropertyValue("ShowIcon", out showIcon)) return showIcon;
                return true;
            }
        }
        public SolidColorBrush IconBorderColor
        {
            get
            {
                Color color;
                if (ElementModel.GetPropertyValue("IconBorderColor", out color))
                {
                    if (color.A == 0) return null;
                    var brush = new SolidColorBrush(color);
                    brush.Freeze();
                    return brush;
                }
                return Brushes.PaleTurquoise;
            }
        }
        public DepictionIconBorderShape IconBorderShape
        {
            get
            {
                DepictionIconBorderShape borderShape;
                //Enough of these calls can probably slow down a draw!!
                if (ElementModel.GetPropertyValue("IconBorderShape", out borderShape))
                {
                    return borderShape;
                }
                return DepictionIconBorderShape.None;
            }
        }
        public double IconSize
        {
            get
            {
                double doubleSize;
                //Enough of these calls can probably slow down a draw!!
                var hasProp = ElementModel.GetPropertyValue("IconSize", out doubleSize);

                if (hasProp)
                {
                    if (doubleSize != modelIconSize)
                    {
                        modelIconSize = doubleSize;
                    }
                    return doubleSize;
                }
                return 24;
            }

        }
        public ImageSource IconSource
        {//This should be different from FullImageSource in the sense that this should be thumb of a real image
            get
            {
                var iconPath = ElementModel.IconPath;
                return DepictionIconPathTypeConverter.ConvertDepictionIconPathToImageSource(iconPath);
            }
        }
        public ImageSource FullImageSource
        {
            get
            {
                if (ElementModel.ImageMetadata == null) return null;

                var iconPath = ElementModel.ImageMetadata.DepictionImageName;
                if (iconPath.IsValid)
                {
                    switch (iconPath.Source)
                    {
                        case IconPathSource.File:
                            if (Application.Current.Resources.Contains(iconPath.Path))
                            {
                                return Application.Current.Resources[iconPath.Path] as ImageSource;
                            }
                            break;
                        default:
                            return null;
                    }
                }
                return null;
            }
        }
        public Visibility IconVisibility
        {
            get
            {
                if (IsElementPreview) return Visibility.Visible;
                if (ViewVisibility != Visibility.Visible) return Visibility.Collapsed;
                if (!ShowIcon) return Visibility.Collapsed;
                if (IsElementInEditMode) return Visibility.Collapsed;
                //                return Visibility.Visible;
                //Man I'm nto sure how importatn this classification thing is
                ElementClassification classification;
                if (!ElementModel.GetPropertyValue("Classification", out classification)) return Visibility.Visible;

                switch (classification)
                {
                    case ElementClassification.MapsAndImagery:
                        return Visibility.Collapsed;
                    case ElementClassification.HiResMapsAndImagery:
                        return Visibility.Collapsed;
                    case ElementClassification.UserImagery:
                        return Visibility.Collapsed;
                    case ElementClassification.Coverage:
                        if (IsElementZOIDraggable) return Visibility.Visible;
                        var prop = ElementModel.GetPropertyByInternalName("PlaceableWithMouse");
                        if (prop != null && prop.Value.Equals(true)) return Visibility.Visible;
                        return Visibility.Collapsed;
                    case ElementClassification.UsableCoverage:
                        if (IsElementZOIDraggable) return Visibility.Visible;
                        var prop1 = ElementModel.GetPropertyByInternalName("PlaceableWithMouse");
                        if (prop1 != null && prop1.Value.Equals(true)) return Visibility.Visible;
                        return Visibility.Collapsed;
                    default:
                        return Visibility.Visible;
                }
            }
        }

        public string ToolTipText
        {
            get
            {
                if (toolTipText == null) return ElementModel.GetToolTipText(Environment.NewLine, UsePropertyNameInHoverText,true);
                return toolTipText;
            }
        }
        public string EnhancedToolTipText
        {
            get
            {
                if (enhancedToolTipText == null) return ElementModel.GetToolTipText(Environment.NewLine, UsePropertyNameInHoverText,false);
                return enhancedToolTipText;
            }
        }

        public MapZoneOfInfluenceViewModel ZOI { get; private set; }
        public bool IsZOIEditable
        {
            get
            {
                bool editable;
                if (!ElementModel.GetPropertyValue("CanEditZoi", out editable))
                {
                    return false;
                }
                return editable;

            }
        }
        public double ImageWidth { get; set; }
        public double ImageHeight { get; set; }
        public RotateTransform ImageRotation { get; set; }
        #endregion

        #region Permatext properties

        public IPermaText PermaText { get { return ElementModel.PermaText; } }
        public double PermaTextX { get { return PermaText.PermaTextX; } set { PermaText.PermaTextX = value; NotifyPropertyChanged("PermaTextX"); } }
        public double PermaTextY { get { return PermaText.PermaTextY; } set { PermaText.PermaTextY = value; NotifyPropertyChanged("PermaTextY"); } }
        public double PermaTextWidth { get { return PermaText.PixelWidth; } set { PermaText.PixelWidth = value; NotifyPropertyChanged("PermaTextWidth"); } }
        public double PermaTextHeight { get { return PermaText.PixelHeight; } set { PermaText.PixelHeight = value; NotifyPropertyChanged("PermaTextHeight"); } }
        private double permaTextCenterX = 0;
        private double permaTextCenterY = 0;
        public double PermaTextCenterX { get { return permaTextCenterX; } set { permaTextCenterX = value; NotifyPropertyChanged("PermaTextCenterX"); } }
        public double PermaTextCenterY { get { return permaTextCenterY; } set { permaTextCenterY = value; NotifyPropertyChanged("PermaTextCenterY"); } }

        #endregion

        #region Commands


        #endregion

        #region COnstructor
        public MapElementViewModel(IDepictionElement element)
        {
            ElementModel = element;
            ElementImagePosition = new TranslateTransform();
            if (element.IsGeolocated)
            {
                var point = MapPixelLocation;
                ElementImagePosition.X = point.X;
                ElementImagePosition.Y = point.Y;
            }

            ZOI = new MapZoneOfInfluenceViewModel(element);

            ImageRotation = new RotateTransform(0, .5, .5);
            if (ElementModel.ImageMetadata != null && ElementModel.ImageMetadata.IsGeoReferenced)
            {
                var metadata = ElementModel.ImageMetadata;
                var pixelBounds = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(metadata.GeoBounds);
                ImageRotation.Angle = metadata.RotationDegrees;
                SetElementImagePosition(pixelBounds, ImageRotation);
            }

            toolTipText = ElementModel.GetToolTipText(Environment.NewLine, UsePropertyNameInHoverText,true);
            enhancedToolTipText = ElementModel.GetToolTipText(Environment.NewLine, UsePropertyNameInHoverText,false);
            ElementModel.PropertyChanged += ElementModel_PropertyChanged;
        }
        #endregion
        #region Destruction
        protected override void OnDispose()
        {
            if (ElementModel != null)
            {
                ElementModel.PropertyChanged -= ElementModel_PropertyChanged;
                ElementModel.PropertyChanged -= ElementModel_PropertyChanged;
            }
            ElementModel = null;
            ImageRotation = null;
            ElementImagePosition = null;

            if (ZOI != null) ZOI.Dispose();
            ZOI = null;

            base.OnDispose();
        }
        #endregion
        #region Event methods

        void ElementModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Render, new Action<object, PropertyChangedEventArgs>(ElementModel_PropertyChanged), sender, e);
                return;
            }

            var name = e.PropertyName;
            if (name.Equals("Position",StringComparison.InvariantCultureIgnoreCase))
            {
                ElementImagePosition.X = MapPixelLocation.X;
                ElementImagePosition.Y = MapPixelLocation.Y;
                //What should happen if a movement happens while in edit mode?
                ZOI.UpdateZOIViewFromModel();
                NotifyPropertyChangedNoDispatcher("MapPixelLocation");
            }
            else if (name.Equals("HideImage", StringComparison.InvariantCultureIgnoreCase))
            {
                var model = sender as DepictionElementParent;
                if (model != null)
                {
                    if (model.HideImage)
                    {
                        ViewVisibility = Visibility.Collapsed;
                    }
                    else
                    {
                        ViewVisibility = Visibility.Visible;
                    }
                }
            }else if (name.Equals("CanEditZoi",StringComparison.InvariantCultureIgnoreCase))
            {
                NotifyPropertyChangedNoDispatcher("IsZOIEditable");
            }
            else if (name.Equals("ImageMetadata", StringComparison.InvariantCultureIgnoreCase))
            {
                var metadata = ElementModel.ImageMetadata;
                var pixelBounds = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(metadata.GeoBounds);
                ImageRotation.Angle = metadata.RotationDegrees;
                SetElementImagePosition(pixelBounds, ImageRotation);
                NotifyPropertyChangedNoDispatcher("FullImageSource");
            }
            else if (name.Equals("HoverText", StringComparison.InvariantCultureIgnoreCase))
            {
                toolTipText = null;
                enhancedToolTipText = null;
                NotifyPropertyChangedNoDispatcher("ToolTipText");
                if(ElementModel.UseEnhancedPermaText)
                {
                    NotifyPropertyChangedNoDispatcher("EnhancedToolTipText");  
                }
            }
            else if (name.Equals("ZOIBorder", StringComparison.InvariantCultureIgnoreCase))
            {
                Color val;
                if (ElementModel.GetPropertyValue(name, out val))
                {
                    ZOI.BorderBrush = new SolidColorBrush(val);
                }
            }
            else if (name.Equals("ZOIFill", StringComparison.InvariantCultureIgnoreCase))
            {
                Color val;
                if (ElementModel.GetPropertyValue(name, out val))
                {
                    ZOI.FillBrush = new SolidColorBrush(val);
                }
            }
            else if (name.Equals("ZOILineThickness", StringComparison.InvariantCultureIgnoreCase))
            {
                double val;
                if (ElementModel.GetPropertyValue(name, out val))
                {
                    ZOI.LineThickness = val;
                }
            }
            else if (name.Equals("IconPath", StringComparison.InvariantCultureIgnoreCase))
            {
                NotifyPropertyChangedNoDispatcher("IconSource");
            }
            //Temp ahck, for now this will be circular to get to the models waypoints
            else if (name.Equals("IconSize", StringComparison.InvariantCultureIgnoreCase))
            {
                double size;
                if (ElementModel.GetPropertyValue(name, out size))
                {
                    foreach (var waypoint in ElementModel.Waypoints)
                    {
                        waypoint.IconSize = size;
                    }
                }
                NotifyPropertyChangedNoDispatcher(name);
            }
            else if (name.Equals("ZoneOfInfluence", StringComparison.InvariantCultureIgnoreCase))
            {
                ZOI.UpdateZOIViewFromModel();
                NotifyPropertyChangedNoDispatcher("ZoneOfInfluence");
            }
            else if (name.Equals("ShowIcon", StringComparison.InvariantCultureIgnoreCase))
            {
                NotifyPropertyChangedNoDispatcher("IconVisibility");
            }
            else
            {
                NotifyPropertyChangedNoDispatcher(name);
            }
        }
        #endregion
        #region helper methods

        public void UpdateModelImageMetadata(IMapCoordinateBounds geoBounds, double rotation)
        {
            ElementModel.UpdateImageMetadata(geoBounds, rotation);
        }

        protected void SetElementImagePosition(Rect newBounds, RotateTransform angle)
        {
            SetElementPosition(newBounds.TopLeft);
            ImageWidth = newBounds.Width;
            ImageHeight = newBounds.Height;
            ImageRotation = angle;
            NotifyPropertyChanged("ImageWidth");
            NotifyPropertyChanged("ImageHeight");
            NotifyPropertyChanged("ImageRotation");
        }
        public void SetElementPosition(Point position)
        {
            //            ElementModel.SetPropertyValue("Position", pixelToMapConverter.GeoCanvasToWorld(position));
            ElementModel.Position = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(position);

        }


        #endregion
    }
}