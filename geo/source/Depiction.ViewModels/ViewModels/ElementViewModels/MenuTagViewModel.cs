using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.ElementViewModels
{
    public class MenuTagViewModel:ViewModelBase
    {
        private bool delete;
        private bool isPreview;
        public string DisplayName { get; set; }
        public bool Delete { get{ return delete;} set{ delete = value;NotifyPropertyChanged("Delete");} }
        public bool Deletable { get{ return true;} }
        public bool IsPreview { get { return isPreview; } set{ isPreview = value;NotifyPropertyChanged("IsPreview");} }
        
        public MenuTagViewModel(string tag)
        {
            DisplayName = tag;
        }
    }
}