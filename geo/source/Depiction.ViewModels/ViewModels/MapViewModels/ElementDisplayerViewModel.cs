﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.CoreModel.AbstractDepictionObjects;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModelInterfaces;
using Depiction.ViewModels.ViewModels.ElementViewModels;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class ElementDisplayerViewModel<T> : ViewModelBase, IElementDisplayerViewModel where T : IDepictionBaseDisplayer
    {
        public event Action<IElementDisplayerViewModel, DepictionDisplayerType> DisplayerTypeChange;
        public T DisplayerModel { get; protected set; }

        private DelegateCommand countDisplayerElements;

        protected string elementCountDisplay = "-";
        protected IGeoConverter DepictionPixelToMapConverter { get; set; }
        #region variables

        protected List<MapElementViewModel> tempElements = new List<MapElementViewModel>();
        
        #endregion

        public ReadOnlyCollection<string> ElementIDsInDisplayer
        {
            get { return DisplayerModel.ElementIdsInDisplayer; }
        }

        public ElementViewModelManager ViewModelManager { get; private set; }

        #region Properties
        public string DisplayerName
        {
            get { return DisplayerModel.DisplayerName; }
            set { DisplayerModel.DisplayerName = value; NotifyPropertyChanged("DisplayerName"); }
        }
        public string ModelID { get { return DisplayerModel.DisplayerID; } }
        public IDepictionBaseDisplayer BaseDisplayerModel { get { return DisplayerModel; } }
        public string ElementCount
        {
            get
            {
              return elementCountDisplay;
            }
        }
        virtual public DepictionDisplayerType DisplayerType { get { return DisplayerModel.DisplayerType; } }

        #endregion
        #region Constructor

        public ElementDisplayerViewModel(T model, IDepictionStory mainDepiction, IGeoConverter geoVsPixelConverter)
        {
            DisplayerModel = model;// mainDepiction.MainMap;
            DepictionPixelToMapConverter = geoVsPixelConverter;
            ViewModelManager = new ElementViewModelManager();
            //Used when loading a depiction
            if (mainDepiction != null && !DepictionAppViewModel.DelayLoad)
            {
                var loadedElements = mainDepiction.GetElementsWithIds(DisplayerModel.ElementIdsInDisplayer.ToList());
                AddElementListToDisplayer(loadedElements);
            }

            DisplayerModel.VisibleElementsChange += DisplayerModel_VisibleElementsChange;
        }

        #endregion
        #region Destruction

        protected override void OnDispose()
        {
            DisplayerModel.VisibleElementsChange -= DisplayerModel_VisibleElementsChange;
            ViewModelManager.ClearElementVMSet();
            base.OnDispose();
        }
        #endregion

        #region Event methods

        void DisplayerModel_VisibleElementsChange(object sender, NotifyCollectionChangedEventArgs changedElements)//  List<IDepictionElementBase> elementToAddRemove, AddRemoveEnum AddOrRemove)
        {
            switch (changedElements.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    var newItems = changedElements.NewItems.OfType<IDepictionElement>();
                    AddElementListToDisplayer(newItems);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    var oldItems = changedElements.OldItems.OfType<IDepictionElement>();
                    RemoveElementListFromDisplayer(oldItems);
                    break;
            }
        }
        #endregion

        #region Commands

        public ICommand CountVisibleElementsCommand
        {
            get
            {
                if (countDisplayerElements == null)
                {
                    countDisplayerElements = new DelegateCommand(CountVisibleElements);
                }
                return countDisplayerElements;
            }
        }

        private void CountVisibleElements()
        {
            elementCountDisplay = DisplayerModel.VisibleElementCount();
            NotifyPropertyChanged("ElementCount");
        }
        protected void ClearElementCount()
        {
            if(!elementCountDisplay.Equals("-"))
            {
                elementCountDisplay = "-";
                NotifyPropertyChanged("ElementCount");
            }
        }
        #endregion

        #region Public add/remove

        public void AddElementListToModel(List<IDepictionElement> elementsToAdd)
        {
            DisplayerModel.AddElementList(elementsToAdd);
        }
        public void RemoveElementListFromModel(List<IDepictionElement> elementsToRemove)
        {
            DisplayerModel.RemoveElementList(elementsToRemove);
        }
        #endregion
        #region adding elemetns that need furthor input so they are only added to the visual and not the model

        internal void AddTempElementVM(MapElementViewModel tempElementVM)
        {
            tempElements.Add(tempElementVM);
            ViewModelManager.AddElementVM(tempElementVM);
        }
        internal void RemoveTempElementVM(string tempElementKey)
        {
            var matching = tempElements.Where(t => t.ElementKey.Equals(tempElementKey)).ToList();
            foreach (var match in matching)
            {
                tempElements.Remove(match);
                ViewModelManager.RemoveElementVM(match);
            }
        }

        internal void RemoveAllTempElementVMs()
        {
            foreach (var tempElement in tempElements)
            {
                ViewModelManager.RemoveElementVM(tempElement);
            }
            tempElements.Clear();
        }

        #endregion
        
        #region Methods do do things on the displayer

        internal void AddElementListToDisplayer(IEnumerable<IDepictionElement> elementsToAdd)
        {
            if (elementsToAdd == null) return;
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<IEnumerable<IDepictionElement>>(AddElementListToDisplayer), elementsToAdd);
                return;
            }
#if DEBUG
            DepictionDisplayerBase.stopWatch = Stopwatch.StartNew();
#endif
            var viewModelsToAdd = new List<MapElementViewModel>();
            foreach (var element in elementsToAdd)
            {
                if (!DisplayerModel.IsElementPresent(element.ElementKey)) continue;
                viewModelsToAdd.Add( new MapElementViewModel(element));
            }
            //This can probably get pumped to threading to make the app seem more responsive
           ViewModelManager.AddElementVMSet(viewModelsToAdd);
#if DEBUG
            Debug.WriteLine(string.Format("Adding to elements took {0} seconds", 
                (DepictionDisplayerBase.stopWatch.Elapsed.Seconds)));
#endif
        }

        internal void RemoveElementListFromDisplayer(IEnumerable<IDepictionElement> elementsToRemove)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<IEnumerable<IDepictionElement>>(RemoveElementListFromDisplayer), elementsToRemove);
                return;
            }
            if (elementsToRemove == null) return;
            var allKeysToRemove = new List<string>();
            foreach (var element in elementsToRemove)
            {
                var parent = element;
                if (parent == null) continue;

                allKeysToRemove.AddRange(parent.GetAllWaypointKeys());
            }

            foreach (var key in allKeysToRemove)
            {
                if (DisplayerModel.IsElementPresent(key)) continue;//Can't remember why this is here
                ViewModelManager.RemoveElementWithKey(key);
            }

            GC.Collect();
        }
        protected Dictionary<string, MapElementViewModel> CreateMapViewModelForElement(IDepictionElement element)
        {
            var vmList = new Dictionary<string, MapElementViewModel>();
            var parent = new MapElementViewModel(element);
            vmList.Add(element.ElementKey, parent);

            return vmList;
        }

        protected void NotifyDisplayerTypeChange()
        {
            if (DisplayerTypeChange != null)
            {
                DisplayerTypeChange.Invoke(this, DisplayerType);
            }
        }

        #endregion

    }

}