﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.ViewModels.Properties;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.MapControlDialogViewModels;
using Depiction.ViewModels.ViewModels.QuickstartViewModels;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{//TODO this needs to be cleanup and reorganized, actually all the mapview models need to a good reorganizing
    public class DepictionMapAndMenuViewModel : DepictionEnhancedMapViewModel
    {
        //The view connects to this method, also seems like a hack
        public event Action<ElementPropertyDialogContentVM> RequestShowElementProperty;//TODO whre does this go?

        #region Property variables

        //        private bool mainMenuVisible = true;
        private bool fileDialogVisible;
        private bool toolsDialogVisible;
        private bool depictionApplictionMenuVisible;

        #endregion

        #region Common variables

        private DelegateCommand<object> showElementPropertyInfo;
        private DelegateCommand<object> showElementHoverTextInfo;
        private DelegateCommand<List<string>> deleteElement;
        private DelegateCommand<List<string>> exportElementToEmail;
        private DelegateCommand<List<string>> exportElementToFile;
        private DelegateCommand<List<string>> exportElementToGenericFile;

        #endregion

        #region Properties

        public QuickstartDataDialogVM QuickStartDialogViewModel { get; set; }
        public TipControlDialogVM TipControlDialogViewModel { get; set; }
        public AddContentDialogVM AddContentDialogViewModel { get; set; }
        public DisplayContentDialogVM DisplayContentDialogViewModel { get; set; }
        public AddinDisplayerDialogViewModel AddinDisplayerDialogViewModel { get; set; }
        public ManageContentDialogVM ManageContentDialogViewModel { get; set; }

        #endregion

        #region Menu Properties
        //        "{Binding Source={x:Static p:Settings.Default}, Path=MyCollectionOfStrings}"
        public bool MainMenuVisible
        {
            get { return Settings.Default.MenuOpen; }
            set { Settings.Default.MenuOpen = value; Settings.Default.Save(); NotifyPropertyChanged("MainMenuVisible"); }
        }

        public bool FileDialogVisible
        {
            get { return fileDialogVisible; }
            set { fileDialogVisible = value; NotifyPropertyChanged("FileDialogVisible"); }
        }
        public bool ToolsDialogVisible
        {
            get { return toolsDialogVisible; }
            set { toolsDialogVisible = value; NotifyPropertyChanged("ToolsDialogVisible"); }
        }

        public bool DepictionApplictionMenuVisible
        {
            get { return depictionApplictionMenuVisible; }
            set { depictionApplictionMenuVisible = value; NotifyPropertyChanged("DepictionApplictionMenuVisible"); }
        }
        #endregion
        #region Command Propertiers

        #region Can do command helpers
        protected bool AreMultipleElementsSelectedForExport(object arg)
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    return false;
            }
            //#if PREP
            //            return false;
            //#endif
            if (!(arg is List<string>)) return false;
            var list = arg as List<string>;
            if (list.Count == 0) return false;
            return true;
        }

        #endregion

        #region toggling element perma text

        private DelegateCommand<object> toggleElementPermaTextCommand;

        public ICommand ToggleElementPermaTextCommand
        {
            get
            {
                if (toggleElementPermaTextCommand == null)
                {
                    toggleElementPermaTextCommand = new DelegateCommand<object>(ToggleElementPermaText, AreMultipleElementsSelectedForInfo) { Text = "Show permatext" };
                }
                return toggleElementPermaTextCommand;
            }
        }
        public const string hidePermatext = "Hide element permatext";
        public const string showPermatext = "Show element permatext";
        private void ToggleElementPermaText(object singleOrMultipleElements)
        {
            if (singleOrMultipleElements is List<string>)
            {
                var idList = singleOrMultipleElements as List<string>;
                var modelList = WorldModel.GetElementsWithIds(idList).ToList();
                foreach (var element in modelList)
                {
                    element.UsePermaText = !element.UsePermaText;
                    if (element.UsePermaText) toggleElementPermaTextCommand.Text = hidePermatext;
                    else toggleElementPermaTextCommand.Text = showPermatext;
                }
            }
            else if (singleOrMultipleElements is IDepictionElementBase)
            {
                var element = singleOrMultipleElements as IDepictionElementBase;
                element.UsePermaText = !element.UsePermaText;
                if (element.UsePermaText) toggleElementPermaTextCommand.Text = hidePermatext;
                else toggleElementPermaTextCommand.Text = showPermatext;
            }
            else if (singleOrMultipleElements is MapElementViewModel)
            {
                var elementVM = singleOrMultipleElements as MapElementViewModel;
                elementVM.UsePermaText = !elementVM.UsePermaText;
                if (elementVM.UsePermaText) toggleElementPermaTextCommand.Text = hidePermatext;
                else toggleElementPermaTextCommand.Text = showPermatext;
            }
        }

        #endregion

        #region showing the element info dialog

        string showPropText = "Show element properties";
        string showHoverTextText = "Set element hovertext";
        public ICommand ShowElementHoverTextInfoCommand
        {
            get
            {
                if (showElementHoverTextInfo == null)
                {
                    showElementHoverTextInfo = new DelegateCommand<object>(ShowSelectedElementsHoverText, AreMultipleElementsSelectedForInfo) { Text = showHoverTextText };
                }
                return showElementHoverTextInfo;
            }
        }
        public ICommand ShowElementPropertyInfoCommand
        {
            get
            {
                if (showElementPropertyInfo == null)
                {
                    showElementPropertyInfo = new DelegateCommand<object>(ShowSelectedElementsProperties, AreMultipleElementsSelectedForInfo) { Text = showPropText };
                }
                return showElementPropertyInfo;
            }
        }

        private bool AreMultipleElementsSelectedForInfo(object singleOrMultipleElements)
        {
            if (singleOrMultipleElements is List<string>)
            {
                var idList = singleOrMultipleElements as List<string>;

                if (idList.Count == 0) return false;
                if (idList.Count == 1) showElementPropertyInfo.Text = showPropText;
                else showElementPropertyInfo.Text = "Show properties for selected elements";
                return true;

            }
            if (singleOrMultipleElements is IDepictionElementBase)
            {
                showElementPropertyInfo.Text = showPropText;
                return true;
            }
            if (singleOrMultipleElements is MapElementViewModel)
            {
                showElementPropertyInfo.Text = showPropText;
                return true;
            }
            return false;
        }
        protected void ShowSelectedElementsProperties(object singleOrMultipleElementsOrIds)
        {
            ShowSelectedElementsPropertiesInTab(singleOrMultipleElementsOrIds, ElementSettingsModality.Properties);
        }
        protected void ShowSelectedElementsHoverText(object singleOrMultipleElementsOrIds)
        {
            ShowSelectedElementsPropertiesInTab(singleOrMultipleElementsOrIds, ElementSettingsModality.Hovertext);
        }

        protected void ShowSelectedElementsPropertiesInTab(object singleOrMultipleElementsOrIds, ElementSettingsModality tabToShow)
        {
            if (singleOrMultipleElementsOrIds is List<string>)
            {
                var idList = singleOrMultipleElementsOrIds as List<string>;
                var modelList = WorldModel.GetElementsWithIds(idList).ToList();

                if (modelList.Count == 0) return;
                if (modelList.Count == 1)
                {
                    ShowElementPropertyInfoDialog(new ElementPropertyDialogContentVM(modelList[0]) { ElementSettingsModality = tabToShow });
                    return;
                }
                ShowElementPropertyInfoDialog(new ElementPropertyDialogContentVM(modelList) { ElementSettingsModality = tabToShow });
            }
            else if (singleOrMultipleElementsOrIds is IDepictionElement)
            {
                ShowElementPropertyInfoDialog(new ElementPropertyDialogContentVM((IDepictionElement)singleOrMultipleElementsOrIds) { ElementSettingsModality = tabToShow });
            }
            else if (singleOrMultipleElementsOrIds is MapElementViewModel)
            {
                ShowElementPropertyInfoDialog(new ElementPropertyDialogContentVM((MapElementViewModel)singleOrMultipleElementsOrIds) { ElementSettingsModality = tabToShow });
            }
        }

        override protected void ShowElementPropertyInfoDialog(ElementPropertyDialogContentVM elementPropertiesVM)
        {
            if (RequestShowElementProperty != null)
            {
                RequestShowElementProperty.Invoke(elementPropertiesVM);
            }
        }

        #endregion

        #region Deleting elements commands

        public ICommand DeleteElementCommand
        {
            get
            {
                if (deleteElement == null)
                {
                    deleteElement = new DelegateCommand<List<string>>(DeleteElement, AreMultipleElementsSelectedForDelete) { Text = "Delete element(s)" };
                }
                return deleteElement;
            }
        }

        private bool AreMultipleElementsSelectedForDelete(List<string> list)
        {
            if (list.Count == 0) return false;
            if (list.Count == 1) deleteElement.Text = "Delete element";
            else deleteElement.Text = "Delete elements";
            return true;
        }

        private void DeleteElement(List<string> elementIDToRemove)
        {
            if (elementIDToRemove == null) return;
            if (elementIDToRemove.Count == 0) return;
            var elemName = "elements";
            if (elementIDToRemove.Count == 1) elemName = "element";
            var result =
                DepictionAccess.NotificationService.DisplayInquiryDialog(
                    string.Format("Do you want to delete {0} {1}?", elementIDToRemove.Count, elemName), "Delete Elements",
                    MessageBoxButton.YesNo);

            if (result.Equals(MessageBoxResult.No)) return;

            if (WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIEdit)) WorldCanvasModeVM = DepictionCanvasMode.Normal;
            WorldModel.DeleteElementsWithIdsFromDepictionElementList(elementIDToRemove);

            GC.Collect();//This is a bad place for GC, since the above method starts the chain as opposed to finishing it
        }

        #endregion

        #region Exporting to email

        public ICommand ExportToEmailCommand
        {
            get
            {
                if (exportElementToEmail == null)
                {
                    exportElementToEmail = new DelegateCommand<List<string>>(ExportToEmail, AreMultipleElementsSelectedForExport) { Text = "Send elements via email" };
                }
                return exportElementToEmail;
            }
        }

        private void ExportToEmail(List<string> obj)
        {
            var exporters = AddinRepository.Instance.GetExporters();

            foreach (var exporter in exporters)
            {
                if (exporter.Key.Name.Contains("ElementEmailSender"))
                {
                    var elements = WorldModel.GetElementsWithIds(obj);
                    exporter.Value.ExportElements("none", elements);//(elements, "none", null));ation(elements, "none", null);
                    return;
                }
            }
        }

        #endregion


        #region Generic exporter, for now real world

        public ICommand ExportToGenericFileCommand
        {
            get
            {
                if (exportElementToGenericFile == null)
                {
                    exportElementToGenericFile = new DelegateCommand<List<string>>(ExportToGenericFile, AreMultipleElementsSelectedForExport) { Text = "Export to RealWorld readable file" };
                }
                return exportElementToGenericFile;
            }
        }
        private void ExportToGenericFile(List<string> obj)
        {
            var result = DepictionAppViewModel.GetFileToSaveNameAndExporter(string.Empty);
            var saveType = result.Value;
            if (saveType == null) return;
            var elements = WorldModel.GetElementsWithIds(obj);
            saveType.ExportElements(result.Key, elements);
        }
        #endregion
        #region csv export command

        public ICommand ExportToFileCommand
        {
            get
            {
                if (exportElementToFile == null)
                {
                    exportElementToFile = new DelegateCommand<List<string>>(ExportToFile, AreMultipleElementsSelectedForExport) { Text = "Export elements to file" };
                }
                return exportElementToFile;
            }
        }

        private void ExportToFile(List<string> obj)
        {
            var message =
                "Use the Save as file type select box in the next window to select which type will be saved:";

            message +=
                "\n1. Geography Markup Language (GML) is an industry standard for exchanging information between different systems." +
                "\n2. CSV is a comma separated value file, and can be edited in any spreadsheet software." +
                "\n\nTake note that in either format, the exported file:" +
                "\n- Will create and give exported elements EIDs, if they don't currently have one"+
                "\n- Might not retain some of the rich properties and behaviors of certain elements." +
                "\n- Cannot store Route, Elevation data or Image elements." +
                "\n\nDo you want to continue exporting?";

            var messageResult = DepictionAccess.NotificationService.DisplayInquiryDialog(message, "Export element(s) to file",
                                                                     MessageBoxButton.YesNo);
            if (messageResult.Equals(MessageBoxResult.No)) return;
            var result = DepictionAppViewModel.GetFileToSaveNameAndExporter(string.Empty);
            var saveType = result.Value;
            if (saveType == null) return;
            var elements = WorldModel.GetElementsWithIds(obj);
            saveType.ExportElements(result.Key, elements);
        }

        #endregion

        #endregion

        #region Constructor
        //Not optimized at all!!
        public DepictionMapAndMenuViewModel(IDepictionStory depiction)
            : base(depiction)
        {

            DisplayContentDialogViewModel = new DisplayContentDialogVM(depiction.CompleteElementRepository.ElementsGeoLocated,
                                    this) { DialogDisplayName = "Display content" };

            QuickStartDialogViewModel = new QuickstartDataDialogVM() { DialogDisplayName = "Quickstart" };
            TipControlDialogViewModel = new TipControlDialogVM { DialogDisplayName = "Depiction tips" };
            AddContentDialogViewModel = new AddContentDialogVM(this) { DialogDisplayName = "Add content" };
            ManageContentDialogViewModel = new ManageContentDialogVM(depiction.CompleteElementRepository) { DialogDisplayName = "Manage content" };

            //Seems like this should be declared earlier since it does not depend on the map, but that app
            AddinDisplayerDialogViewModel = new AddinDisplayerDialogViewModel { DialogDisplayName = "Add-ons" };//new UserAddinImporter()) { DialogDisplayName = "Addins" };

            //MapCommands.Add(AddContentDialogViewModel.ToggleDialogVisibilityCommand);
            //MapCommands.Add(DisplayContentDialogViewModel.ToggleDialogVisibilityCommand);

            GenericElementCommands.Add(ShowElementPropertyInfoCommand);
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    GenericElementCommands.Add(ShowElementHoverTextInfoCommand);
                    break;
            }
            //#if PREP
            //            GenericElementCommands.Add(ShowElementHoverTextInfoCommand);
            //#endif
            GenericElementCommands.Add(ToggleElementPermaTextCommand);
            GenericElementCommands.Add(DeleteElementCommand);
        }

        #endregion

        #region Destruction

        protected override void OnDispose()
        {
            AddContentDialogViewModel.Dispose();
            DisplayContentDialogViewModel.Dispose();
            AddinDisplayerDialogViewModel.Dispose();
            ManageContentDialogViewModel.Dispose();
            base.OnDispose();
        }

        #endregion
        #region Attach to the model event methods

        override protected void AttachEventsToModel()
        {
            base.AttachEventsToModel();
            //Move these to dialogs that they eventually get called on?
            //tsk tsk these only take care of the dialogs, not the main map
            WorldModel.ElementListChange += DepictionWorldModel_ElementListChange;
            WorldModel.CompleteElementRepository.ElementGeoLocationUpdated += ElementRepositoryGeocoded_ElementGeoLocationUpdated;
            WorldModel.CompleteElementRepository.TagsChanged += ElementRepositoryGeocoded_TagsChanged;
        }

        override protected void DetachEventsFromModel()
        {
            base.DetachEventsFromModel();
            WorldModel.ElementListChange -= DepictionWorldModel_ElementListChange;
            WorldModel.CompleteElementRepository.ElementGeoLocationUpdated -= ElementRepositoryGeocoded_ElementGeoLocationUpdated;
            WorldModel.CompleteElementRepository.TagsChanged -= ElementRepositoryGeocoded_TagsChanged;
        }

        //Where shouold these be? They can probalby be moved into the view models
        void ElementRepositoryGeocoded_TagsChanged()
        {
            ManageContentDialogViewModel.UpdateAllElementTags();
            DisplayContentDialogViewModel.ElementsForMapPlacementVM.UpdateElementTagList();
        }

        void ElementRepositoryGeocoded_ElementGeoLocationUpdated(IDepictionElement obj)
        {
            ShiftElementRepositoryLocation(new List<IDepictionElement> { obj });
        }

        void DepictionWorldModel_ElementListChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (DepictionAccess.DispatchAvailableAndNeeded())
            {
                DepictionAccess.DepictionDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action<object, NotifyCollectionChangedEventArgs>(DepictionWorldModel_ElementListChange),
                    sender, e);
                return;
            }
            switch (e.Action)
            {
                #region Add
                case NotifyCollectionChangedAction.Add:
                    try
                    {
                        var elementsNew = new List<IDepictionElement>();
                        foreach (var item in e.NewItems)
                        {

                            var elements = item as IEnumerable<IDepictionElement>;
                            if (elements != null)
                            {
                                elementsNew.AddRange(elements);
                            }
                        }
                        AddElementModelsToWorldViewModel(elementsNew);
                    }
                    catch { }

                    break;
                #endregion

                #region Remove
                case NotifyCollectionChangedAction.Remove:
                    try
                    {
                        var elementsOld = new List<IDepictionElement>();
                        foreach (var item in e.OldItems)
                        {
                            var elements = item as IEnumerable<IDepictionElement>;
                            if (elements != null)
                            {
                                elementsOld.AddRange(elements);
                            }
                        }
                        RemoveElementModelsFromWorldViewModel(elementsOld);
                    }
                    catch { }
                    break;
                #endregion
            }
        }
        protected void ShiftElementRepositoryLocation(IEnumerable<IDepictionElement> elementsToShift)
        {
            ManageContentDialogViewModel.RemoveElementVMsThatMatch(elementsToShift);
            DisplayContentDialogViewModel.RemoveElementVMsThatMatch(elementsToShift);
            DisplayContentDialogViewModel.CreateVMFromElementListAndAdd(elementsToShift);
            ManageContentDialogViewModel.CreateVMFromModelsAndAdd(elementsToShift);
        }

        protected void AddElementModelsToWorldViewModel(IEnumerable<IDepictionElement> elements)
        {
            ManageContentDialogViewModel.CreateVMFromModelsAndAdd(elements);
            DisplayContentDialogViewModel.CreateVMFromElementListAndAdd(elements);
        }
        protected void RemoveElementModelsFromWorldViewModel(IEnumerable<IDepictionElement> elements)
        {
            ManageContentDialogViewModel.RemoveElementVMsThatMatch(elements);
            DisplayContentDialogViewModel.RemoveElementVMsThatMatch(elements);
        }
        #endregion

        #region Override methods

        override public void GetDataForPoint(Point location)
        {
            base.GetDataForPoint(location);
            //            MapLocationInformation += "  any extra information";
            var elementList = WorldModel.CompleteElementRepository.AllElements.ToList();
            //Bruteforce
            foreach (var element in elementList)
            {
                var value = element.GetDataForPosition("Terrain",
                                                       StaticMainWindowToGeoCanvasConverterViewModel.
                                                           GeoCanvasToWorld(location));
                if (!string.IsNullOrEmpty(value))
                {
                    MapLocationInformation += (" " + value);
                }
            }
        }
        protected override void StartGeoLocate(List<string> elementIDToGeoLocate)
        {
            base.StartGeoLocate(elementIDToGeoLocate);
            ManageContentDialogViewModel.IsDialogVisible = false;
        }
        protected override bool EndRegionSelection(Rect pixelRegion)
        {
            var complete = base.EndRegionSelection(pixelRegion);
            if (!complete) return false;
            var window = (Application.Current.MainWindow as IDepictionMainWindow);
            if (window == null || DepictionAccess.CurrentDepiction == null) return false;
            QuickStartDialogViewModel.UpdateQSStuffInBackground(this);
            TipControlDialogViewModel.TipControlType = TipControlTypeEnum.MapTopControl;

            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    if (!TipControlDialogViewModel.HideControlOnStartup)
                    {
                        TipControlDialogViewModel.CreateFlowDocumentFromEmbeddedResourceFile("DepictionPrepMapTipText.rtf");
                        TipControlDialogViewModel.IsDialogVisible = true;
                    }
                    break;
                case ProductInformationBase.DepictionRW:
                    QuickStartDialogViewModel.IsDialogVisible = true;
                    TipControlDialogViewModel.CreateFlowDocumentFromEmbeddedResourceFile("DepictionRealWorldPostQSTipText.rtf");
                    if (!TipControlDialogViewModel.HideControlOnStartup)
                    {
                        QuickStartDialogViewModel.DialogsToOpenAfterClose.Add(TipControlDialogViewModel);
                    }
                    break;
                case ProductInformationBase.OsintInformation:
                    QuickStartDialogViewModel.IsDialogVisible = true;
                    TipControlDialogViewModel.CreateFlowDocumentFromEmbeddedResourceFile("OsintInformationPostQSTipText.rtf");
                    if (!TipControlDialogViewModel.HideControlOnStartup)
                    {
                        QuickStartDialogViewModel.DialogsToOpenAfterClose.Add(TipControlDialogViewModel);
                    }
                    break;
                default:
                    QuickStartDialogViewModel.IsDialogVisible = true;
                    if (!TipControlDialogViewModel.HideControlOnStartup)
                    {
                        QuickStartDialogViewModel.DialogsToOpenAfterClose.Add(TipControlDialogViewModel);
                    }
                    break;
            }
            return true;
        }

        public override void AddElementListToWorldModel(IEnumerable<IDepictionElement> elements)
        {
            base.AddElementListToWorldModel(elements);
            ManageContentDialogViewModel.IsDialogVisible = true;
        }
        #endregion
    }
}
