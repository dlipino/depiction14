﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Depiction.API;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MVVM;
using Depiction.ViewModels.MapConverters;
using Depiction.ViewModels.ViewModelHelpers;


namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class WorldMapViewModel : BaseMapViewModel
    {
        static public IGeoConverter StaticMainWindowToGeoCanvasConverterViewModel { get; set; }
        public event Func<Rect, Rect> DepictionPixelViewportChangeRequested;
        #region Property variables

        private double northQuadrantHeight;
        private double southQuadrantHeight;
        private double eastQuadrantWidth;
        private double westQuadrantWidth;

        private double quadrantOpacity = .3;

        protected Rect pixelViewport = Rect.Empty;
        protected Rect requestedPixelViewport = Rect.Empty;
        ScaleTransform inverseScale = new ScaleTransform(1, 1, 0, 0);
        ScaleTransform vmScale = new ScaleTransform(1, 1, 0, 0);
        TranslateTransform vmTranslate = new TranslateTransform(0, 0);
        private Point currentCenter = new Point(0, 0);//Grr, this might need to change

        #endregion

        #region Command variables

        private DelegateCommand<PanDirection> panCommand;
        private DelegateCommand<ZoomDirection> zoomCommand;
        protected DelegateCommand centerMapCommand;

        #endregion

        #region Constructor

        public WorldMapViewModel() : this(null) { }

        public WorldMapViewModel(IDepictionStory depiction):base(depiction)
        {
            WorldScalePanTransformVM = new TransformGroup();
            WorldScalePanTransformVM.Children.Add(vmScale);
            WorldScalePanTransformVM.Children.Add(vmTranslate);
            OffsetTransform = new TranslateTransform(0, 0);
            WestNorthTranslate = new TranslateTransform(0, 0);
            if (WorldModel == null) { SetWorldCanvasOffset(0, 0); }
            else
            {
                StaticMainWindowToGeoCanvasConverterViewModel = new ScreenToMapPixelConverter(DepictionAccess.GeoCanvasToPixelCanvasConverter, WorldScalePanTransformVM);
                SetWorldCanvasOffset(DepictionAccess.GeoCanvasToPixelCanvasConverter.Offsetx, DepictionAccess.GeoCanvasToPixelCanvasConverter.Offsety);
                currentCenter = new Point();
                if (WorldModel.DepictionGeographyInfo.DepictionMapWindow.IsValid)
                {
                    var storedGeoWindowSize = WorldModel.DepictionGeographyInfo.DepictionMapWindow;
                    var pixelRect = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(storedGeoWindowSize);
                    PixelViewport = RequestPixelViewportChangeFromView(pixelRect);
                   
                }else
                {//Set the intial zoom this is arbitrary
                    ZoomToPoint(.1, currentCenter);
                }
            }
            MapCommands.Add(CenterOnRegionCommand);
        }

        #endregion
        
        #region Destruction

        protected override void OnDispose()
        {
            base.OnDispose();
        }

        #endregion
        #region Command region

        #region Pan command and methods

        public ICommand PanCommand
        {
            get
            {
                if (panCommand == null)
                {
                    return panCommand = new DelegateCommand<PanDirection>(PanInDesiredDirection, CanPanInDirection);
                }
                return panCommand;
            }
        }

        private void PanInDesiredDirection(PanDirection panDirection)
        {
            const double magicTranslateValue = 50;
            PanInDesiredDirection(panDirection, new Point(magicTranslateValue, magicTranslateValue));
        }

        private bool CanPanInDirection(PanDirection arg) { return true; }

        #endregion

        #region Zoom command and methods

        public ICommand ZoomCommand
        {
            get
            {
                if (zoomCommand == null)
                {
                    return zoomCommand = new DelegateCommand<ZoomDirection>(ZoomCommandInstructions, CanZoomInDirection);
                }
                return zoomCommand;
            }
        }
        private void ZoomCommandInstructions(ZoomDirection obj)
        {
            ZoomInDirectionAroundPoint(obj, new Point(double.NaN, double.NaN));
        }

        public bool CanZoomInDirection(ZoomDirection arg) { return true; }
        #endregion

        #region center on region or start command and methods

        public ICommand CenterOnRegionCommand
        {
            get
            {
                if (centerMapCommand == null)
                {
                    centerMapCommand = new DelegateCommand(CenterOriginMapLocation, CanCenterOnOriginLocation);
                    centerMapCommand.Text = "Center on geolocated center";
                }
                return centerMapCommand;
            }
        }
        protected bool CanCenterOnOriginLocation()
        {
            if (WorldModel == null) return false;
            if (WorldModel.DepictionGeographyInfo.DepictionStartLocation.IsValid) return true;
            return false;
        }

        virtual protected void CenterOriginMapLocation()
        {
            if (!WorldModel.DepictionGeographyInfo.DepictionStartLocation.IsValid) return;
            var latLongCenter = WorldModel.DepictionGeographyInfo.DepictionStartLocation;
            pixelViewport = Rect.Empty;//Silently reset the viewport since the center normally comes from the center//hack
            CurrentCenter = DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(latLongCenter);
        }

        public void UpdateCenterOnRegionCommand()
        {
            centerMapCommand.Text = "Center on geolocated center";
            if (WorldModel == null) return;
            if (!WorldModel.DepictionGeographyInfo.DepictionRegionBounds.IsValid) return;
            centerMapCommand.Text = "Center on region center";
        }
        #endregion

        #endregion

        #region Properties

        public ScaleTransform InverseScaleTransform
        {
            get { return inverseScale; }
            private set
            {
                inverseScale = value; 
                NotifyPropertyChanged("InverseScaleTransform");
                NotifyPropertyChanged("Scale");
            }
        }

        public Point CurrentCenter { get { return CalcCenterFromViewport(); } set { currentCenter = value; NotifyPropertyChanged("CurrentCenter"); } }
        //This has to be set externally since the view model has no idea of its views container size. Although on a load . . 

        public Rect PixelViewport
        {
            get { return pixelViewport; }
            set
            {
                pixelViewport = value;
                CalcCenterFromViewport(); //I really don't like this, but im too tired to get the formula right for internal calculations
               SetDepictionGeoViewportFromPixelBounds(pixelViewport);
                NotifyPropertyChanged("PixelViewport");
            }
        }

        public double Scale { get { return vmScale.ScaleX; } }

        public TranslateTransform OffsetTransform { get; set; }
        public TransformGroup WorldScalePanTransformVM { get; set; }
        public double Translate { get; set; }//Dummy property

        #region for controling offset

        public double NorthQuadrantHeight
        {
            get { return northQuadrantHeight; }
            private set { northQuadrantHeight = value; NotifyPropertyChanged("NorthQuadrantHeight"); }
        }

        public double SouthQuadrantHeight
        {
            get { return southQuadrantHeight; }
            private set { southQuadrantHeight = value; NotifyPropertyChanged("SouthQuadrantHeight"); }
        }

        public double WestQuadrantWidth
        {
            get { return westQuadrantWidth; }
            private set { westQuadrantWidth = value; NotifyPropertyChanged("WestQuadrantWidth"); }
        }

        public double EastQuadrantWidth
        {
            get { return eastQuadrantWidth; }
            private set { eastQuadrantWidth = value; NotifyPropertyChanged("EastQuadrantWidth"); }
        }

        public double QuadrantOpacity
        {
            get { return quadrantOpacity; }
            set { quadrantOpacity = value; NotifyPropertyChanged("QuadrantOpacity"); }
        }
        public TranslateTransform WestNorthTranslate { get; private set; }

        #endregion

        #endregion

        #region Methods
        #region Event adding removing

        override protected void AttachEventsToModel()
        {
            WorldModel.DepictionViewportChangeRequested += WorldModel_DepictionViewportChangeRequested;
        }

        override protected void DetachEventsFromModel()
        {

            WorldModel.DepictionViewportChangeRequested -= WorldModel_DepictionViewportChangeRequested;
        }

        #endregion

        #region Public methods
        IMapCoordinateBounds WorldModel_DepictionViewportChangeRequested(IMapCoordinateBounds requestedVisualBounds)
        {
            var availableRect = RequestPixelViewportChangeFromView(DepictionAccess.GeoCanvasToPixelCanvasConverter.WorldToGeoCanvas(requestedVisualBounds));
            PixelViewport = availableRect;
            return DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(availableRect);
            //The request has to go to the view. If a view exists it will set the vm PixelViewport-> m 
        }
        protected void SetDepictionGeoViewportFromPixelBounds(Rect pixelBounds)
        {
            if (DepictionAccess.GeoCanvasToPixelCanvasConverter == null || WorldModel == null) return;
            WorldModel.UpdateModelViewportBounds(DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(pixelBounds));
        }
        public Rect RequestPixelViewportChangeFromView(Rect requestedRect)
        {
            if(DepictionPixelViewportChangeRequested != null && !requestedRect.IsEmpty)
            {
                return DepictionPixelViewportChangeRequested.Invoke(requestedRect);
            }
            return requestedRect;
        }

        public void SetWorldCanvasOffset(double xOffset, double yOffset)
        {
            //This only works if the offsets are positive
            double mapSize = ViewModelConstants.MapSize;
            var dW = mapSize - xOffset;
            var dH = mapSize - yOffset;
            double jitterIncr = 1d;
            //I don't normally like using the ? operator, but it makes sense here and keeps code from
            //gettin too long
            NorthQuadrantHeight = yOffset == 0 ? yOffset : yOffset + jitterIncr;
            SouthQuadrantHeight = dH == 0 ? dH : dH + jitterIncr;
            EastQuadrantWidth = dW == 0 ? dW : dW + jitterIncr;
            WestQuadrantWidth = xOffset == 0 ? xOffset : xOffset + jitterIncr;

            WestNorthTranslate.X = -xOffset;
            WestNorthTranslate.Y = -yOffset;
            OffsetTransform.X = xOffset;
            OffsetTransform.Y = yOffset;
        }

        #endregion
        #region Helper methods
        public Point CalcCenterFromViewport()
        {
            if (PixelViewport.Equals(Rect.Empty)) return currentCenter;
            var xC = PixelViewport.Left + (PixelViewport.Right - PixelViewport.Left) / 2d;
            var yC = PixelViewport.Top + (PixelViewport.Bottom - PixelViewport.Top) / 2d;
            return new Point(xC, yC);
        }

        #endregion

        #region Pan/Zoom methods

        public void PanInDesiredDirection(PanDirection panDirection, Point distance)
        {
            bool miniChange = false;
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                miniChange = true;
            }

            double horizontalAmountToMove = distance.X;
            double verticalAmountToMove = distance.Y;
            if (miniChange)
            {
                verticalAmountToMove = verticalAmountToMove / 10;
                horizontalAmountToMove = horizontalAmountToMove / 10;
            }
            switch (panDirection)
            {
                case PanDirection.Up:
                    DragWorldCanvas(0, verticalAmountToMove);
                    break;
                case PanDirection.Down:
                    DragWorldCanvas(0, -verticalAmountToMove);
                    break;
                case PanDirection.Left:
                    DragWorldCanvas(horizontalAmountToMove, 0);
                    break;
                case PanDirection.Right:
                    DragWorldCanvas(-horizontalAmountToMove, 0);
                    break;
                case PanDirection.Mouse:
                    DragWorldCanvas(horizontalAmountToMove, verticalAmountToMove);
                    break;
            }
            NotifyPropertyChanged("Translate");
        }

        public void ZoomInDirectionAroundPoint(ZoomDirection zoomDirection, Point centerPoint)
        {
            bool miniChange = false;
            if (centerPoint.X.Equals(double.NaN) || centerPoint.Y.Equals(double.NaN))
            {
                centerPoint = CalcCenterFromViewport();
            }

            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) { miniChange = true; }

            switch (zoomDirection)
            {
                case ZoomDirection.In:
                    if (miniChange) ZoomToPoint(ViewModelConstants.ZoomRatioInFine, centerPoint);
                    else ZoomToPoint(ViewModelConstants.ZoomRatioInCoarse, centerPoint);
                    break;
                case ZoomDirection.Out:
                    if (miniChange) ZoomToPoint(ViewModelConstants.ZoomRatioOutFine, centerPoint);
                    else ZoomToPoint(ViewModelConstants.ZoomRatioOutCoarse, centerPoint);
                    break;
            }
            NotifyPropertyChanged("Scale");
        }
        #endregion
        #region Transform change methods

        public void SetShift(double x, double y)
        {
            vmTranslate.X = x;
            vmTranslate.Y = y;
            NotifyPropertyChanged("Translate");
        }
        public void SetScale(double scale)
        {
            vmScale.ScaleX = vmScale.ScaleY = scale;
            InverseScaleTransform = new ScaleTransform(1 / scale, 1 / scale, 0, 0);
            NotifyPropertyChanged("Scale");

        }
        protected void DragWorldCanvas(double pixelXDirection, double pixelYDirection)
        {
            vmTranslate.X += pixelXDirection;
            vmTranslate.Y += pixelYDirection;
        }

        protected void ZoomToPoint(double zoom, Point pt)
        {
            //set some semi arbitrary limits to the scaling
            if (Scale < ViewModelConstants.MinZoomFactor && zoom < 1) return;
            if (Scale > ViewModelConstants.MaxZoomFactor && zoom > 1) return;
            //I would love to find some way to calc the center offset from here
            var originShift = ShiftCalc(zoom, Scale, pt);

            vmTranslate.X += originShift.X;
            vmTranslate.Y += originShift.Y;
            vmScale.ScaleX = vmScale.ScaleY *= zoom;
            InverseScaleTransform = new ScaleTransform(1 / Scale, 1 / Scale, 0, 0);
        }
        #endregion
        private Point ShiftCalc(double zoomFactor, double oldZoom, Point pt)
        {
            var x = (1 - zoomFactor) * pt.X;
            var y = (1 - zoomFactor) * pt.Y;
            var translationX = x * oldZoom;
            var translationY = y * oldZoom;
            return new Point(translationX, translationY);
        }

        #endregion
    }
}