using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.EnhancedTypes;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;
using Depiction.CoreModel;
using Depiction.CoreModel.DepictionObjects;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.HelperClasses;
using Depiction.CoreModel.TypeConverter;
using Depiction.CoreModel.ValueTypes;
using Depiction.ViewModels.MapShapeViewModels;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.BackgroundServiceViewModels;
using Depiction.ViewModels.ViewModels.MapControlDialogViewModels;
using Depiction.ViewModels.ViewModels.PrototypeViewModels;
using Depiction.ViewModels.ViewModels.TileViewModels;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class DepictionMapQuickAddAndTileViewModel : DepictionMapAndMenuViewModel
    {
        //place for left over stuff
        private ObservableCollection<ElementPrototypeViewModel> quickAddList = new ObservableCollection<ElementPrototypeViewModel>();//ok so the quick add isnt exactly done very well
        private DelegateCommand startMapSelection;
        private DelegateCommand<ITiler> getTileImageForAnAreaCommand;
        private WorldOutlineModel worldOutlineModel = null;
        //For later use
        public IMapCoordinateBounds MapBoundsOfInterest { get; set; }

        public ObservableNotifiableCollection<MapOutlineViewModel> AllMapOutlines { get; set; }
        public BackgroundServiceManageViewModel ServiceManagerVM { get; set; }
        public TileDisplayerViewModel TileDisplayerVM { get; private set; }
        public ITiler CurrentTileProvider { get; set; }
        public bool IsQuickAddVisible
        {
            get { return Settings.Default.QuickAddVisible; }
            set { Settings.Default.QuickAddVisible = value; }
        }

        public ObservableCollection<ElementPrototypeViewModel> QuickAddList { get { return quickAddList; } }

        public ICommand GetTileImageForAnAreaCommand
        {
            get
            {
                if (getTileImageForAnAreaCommand == null)
                {
                    getTileImageForAnAreaCommand = new DelegateCommand<ITiler>(GetTileImageForArea);
                    getTileImageForAnAreaCommand.Text = "High res";
                }
                return getTileImageForAnAreaCommand;
            }

        }
        //Kind of a hack to get this to work right, later the parameter will be a dictionary, although im not sure if that is good form or not
        private void GetTileImageForArea(ITiler tiler)
        {
            Debug.WriteLine(MapBoundsOfInterest);
            if (tiler == null) return;
            TiledElementImporter tileImporter = new TiledElementImporter(tiler);
            var param = new Dictionary<string, string> { { "Tag", Tags.DefaultHighResTag } };
            tileImporter.ImportElements(null, "Image", MapBoundsOfInterest, param);
        }

        public ICommand StartMapSelectionCommand
        {
            get
            {
                if (startMapSelection == null)
                {
                    startMapSelection = new DelegateCommand(StartMapSelection, CanSelectElements);
                    startMapSelection.Text = "Click start area selection. Shift+Click to begin arbitrary area selection.";
                }
                return startMapSelection;
            }
        }

        private bool CanSelectElements()
        {
            if (WorldCanvasModeVM.Equals(DepictionCanvasMode.AreaSelection) || WorldCanvasModeVM.Equals(DepictionCanvasMode.Normal))
            {
                return true;
            }
            return false;
        }

        private void StartMapSelection()
        {
            if (WorldCanvasModeVM.Equals(DepictionCanvasMode.AreaSelection))
            {
                WorldCanvasModeVM = DepictionCanvasMode.Normal;
            }
            else
            {
                if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                {
                    WorldCanvasModeVM = DepictionCanvasMode.ArbitraryAreaSelection;
                }
                else
                {
                    WorldCanvasModeVM = DepictionCanvasMode.AreaSelection;
                }
            }
        }

        #region quick start visibility toggle(probably a duplicated somewhere else)
        DelegateCommand toggleQuickAddVisiblityCommand;
        public ICommand ToggleQuickAddVisiblityCommand
        {
            get
            {
                if (toggleQuickAddVisiblityCommand == null)
                {
                    toggleQuickAddVisiblityCommand = new DelegateCommand(ToggleDisplayContentDialog);
                }
                EditQuickstartVisibilityCommandText(toggleQuickAddVisiblityCommand, "quick add");
                return toggleQuickAddVisiblityCommand;
            }
        }

        private void ToggleDisplayContentDialog()
        {
            IsQuickAddVisible = !IsQuickAddVisible;
            EditQuickstartVisibilityCommandText(toggleQuickAddVisiblityCommand, "quick add");
        }
        private void EditQuickstartVisibilityCommandText(DelegateCommand command, string elementName)
        {
            if (IsQuickAddVisible)
            {
                command.Text = string.Format("Hide {0}", elementName);
            }
            else
            {
                command.Text = string.Format("Show {0}", elementName);
            }
        }
        #endregion
        #region constructor

        public DepictionMapQuickAddAndTileViewModel(IDepictionStory depiction, WorldOutlineModel outlineModel)
            : base(depiction)
        {
            quickAddList = new ObservableCollection<ElementPrototypeViewModel>();
            CreateQuickAddList();
            Settings.Default.PropertyChanged += Default_PropertyChanged;
            IsQuickAddVisible = Settings.Default.QuickAddVisible;
            ServiceManagerVM = new BackgroundServiceManageViewModel(DepictionAccess.BackgroundServiceManager);
            NotifyPropertyChanged("ServiceManagerVM");
            MapCommands.Add(ToggleQuickAddVisiblityCommand);
            MapCommands.Add(AddAnnotationCommand);
            CanvasVMModeChanged += DepictionMapQuickAddAndTileViewModel_CanvasVMModeChanged;
            WorldModel.RequestedElementsInDialogType += WorldModel_EditRequestedElementProperties;
            //Outline setting
            worldOutlineModel = outlineModel;
            UpdateWorldOutlines();
        }

        void DepictionMapQuickAddAndTileViewModel_CanvasVMModeChanged(DepictionCanvasMode obj)
        {
            if (startMapSelection == null) return;
            if (WorldCanvasModeVM.Equals(DepictionCanvasMode.AreaSelection) || WorldCanvasModeVM.Equals(DepictionCanvasMode.ArbitraryAreaSelection))
            {
                startMapSelection.Text = "End depiction map selection.";
            }
            else
            {
                startMapSelection.Text = "Click to begin area selection. Shift+Click to begin arbitrary area selection.";
            }
        }

        #endregion

        #region Destructor

        protected override void OnDispose()
        {
            if (WorldModel != null)
            {
                WorldModel.RequestedElementsInDialogType -= WorldModel_EditRequestedElementProperties;
            }
            Settings.Default.PropertyChanged -= Default_PropertyChanged;
            foreach (var protoVM in quickAddList)
            {
                protoVM.Dispose();
            }
            base.OnDispose();
        }

        #endregion

        #region Event attachments

        void Default_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var name = e.PropertyName;
            if (name.Contains("QuickAddElements"))
            {
                CreateQuickAddList();
            }
            if (name.Contains("QuickAddVisible"))
            {
                NotifyPropertyChanged("IsQuickAddVisible");
            }
        }
        #endregion

        protected void UpdateWorldOutlines()
        {
            AllMapOutlines = new ObservableNotifiableCollection<MapOutlineViewModel>();
            foreach (var outline in worldOutlineModel.IPolygonsForWorld)
            {
                var list = DepictionGeometryToEnhancedPointListWithChildrenConverter.IGeometryToEnhancedPointListWithChildren(
                    outline, DepictionAccess.GeoCanvasToPixelCanvasConverter);
                if (list != null && list.Count != 0)
                {
                    AllMapOutlines.Add(new MapOutlineViewModel(list));
                }
            }
            NotifyPropertyChanged("AllMapOutlines");
        }

        protected override bool EndRegionSelection(Rect pixelRegion)
        {
            var success = base.EndRegionSelection(pixelRegion);
            var currentTiler = TileDisplayerVM.Tiler;
            TileDisplayerVM.Tiler = new EmptyTiler();
            TileDisplayerVM.Tiler = currentTiler; //lol
            if (!success) return false;
            UpdateWorldOutlines();
            //TileDisplayerVM.
            //TileDisplayerVM.RefreshTiles();
            return true;
        }

        #region public methods

        public List<string> GetAllElementThatInteresectGeoBounds(List<ILatitudeLongitude> mapGeoBoundPoints)
        {
            var elementIds = new List<string>();
            var app = Application.Current as DepictionApplication;
            if (app == null) return elementIds;
            var currentDepiction = app.CurrentDepiction;
            if (currentDepiction == null) return elementIds;
            IEnumerable<IDepictionElement> elementModels = currentDepiction.GetElementsWithIds(MainMapDisplayerViewModel.ElementIDsInDisplayer);
            //get elements from visible revealers too
            foreach (var revealer in currentDepiction.Revealers)
            {
                var visibleElements = revealer.GetDisplayedElements();
                if (visibleElements != null && visibleElements.Count != 0)
                {
                    elementModels = elementModels.Union(visibleElements);
                }
            }
            var comparableBounds = new DepictionGeometry(mapGeoBoundPoints);
            foreach (var elementModel in elementModels)
            {
                if (comparableBounds.Intersects(elementModel.ZoneOfInfluence.Geometry))
                {
                    elementIds.Add(elementModel.ElementKey);
                    elementModel.ElementUpdated = false;
                }
            }
            //  ShowSelectedElementsProperties(elementIds);
            bool addToExisting = false;
            if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
            {
                addToExisting = true;
            }
            AddElementListToManageContent(elementIds, addToExisting);
            return elementIds;
        }

        #endregion
        //The connection to the model, this will eventually take over, possibly
        void WorldModel_EditRequestedElementProperties(IEnumerable<string> elementIds, DepictionDialogType[] dialogs)
        {
            if (dialogs == null) return;
            foreach (var dialog in dialogs)
            {
                switch (dialog)
                {
                    case DepictionDialogType.ManageContent:
                        AddElementListToManageContent(elementIds, false);
                        break;
                    case DepictionDialogType.ManageContentNonGeoAligned:
                        AddElementListToManageContent(elementIds, false);
                        ManageContentDialogViewModel.ManageModality = ManageContentModality.NonGeoAligned;
                        break;
                    case DepictionDialogType.PropertyEditor:
                        ShowSelectedElementsProperties(elementIds);
                        break;
                    case DepictionDialogType.PropertyEditorHoverText:
                        ShowSelectedElementsProperties(elementIds);
                        break;
                }
            }
        }

        protected void AddElementListToManageContent(IEnumerable<string> elementIds, bool addToExisting)
        {
            ManageContentDialogViewModel.SelectDesiredGeoLocatedElements(elementIds, addToExisting);
            ManageContentDialogViewModel.IsDialogVisible = true;
        }
        protected void CreateQuickAddList()
        {
            quickAddList.Clear();
            quickAddList.Add(new ElementPrototypeViewModel(new DepictionAnnotation()));
            var defaultList = Settings.Default.QuickAddElements;
            if (defaultList.Count == 1 && defaultList.Contains("Empty"))
            {
                //not guarenteed to be a complete element name
                Settings.Default.QuickAddElements = DepictionAccess.ProductInformation.ProductQuickAddElements;
                Settings.Default.Save();
                defaultList = Settings.Default.QuickAddElements;
                //return;//ok this is a hack which might not even be used now. before it stopped a recreation of the
                //quickadd due to an event trigger on he save.
            }

            //TODO add the defaults that tim wants if the initial quickadd is empty
            //this might be easier to do in the settings part
            if (defaultList != null)
            {
                foreach (var type in defaultList)
                {
                    var proto = DepictionAccess.ElementLibrary.GuessPrototypeFromString(type);
                    if (proto != null)
                    {
                        quickAddList.Add(new ElementPrototypeViewModel(proto));
                    }
                }
            }
        }
        public void AttachTilerVMToMainMap(TileDisplayerViewModel tiler)
        {
            TileDisplayerVM = tiler;
            tiler.SelectedTileProvider = tiler.Tiler;//Hack to invoke a UI change
            NotifyPropertyChanged("TileDisplayerVM");
        }
    }
}