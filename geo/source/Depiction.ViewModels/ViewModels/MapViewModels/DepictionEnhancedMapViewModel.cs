﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.CoreModel.DepictionObjects.Elements;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ElementLibrary;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModels.ElementViewModels;
using Depiction.ViewModels.ViewModels.PrototypeViewModels;

namespace Depiction.ViewModels.ViewModels.MapViewModels
{
    public class DepictionEnhancedMapViewModel : DepictionBasicMapViewModel
    {
        public bool doMultiAdd;//Small hack
        private MapElementViewModel currentEditElement;
        private DelegateCommand<ElementPrototypeViewModel> startElementAdd;
        private DelegateCommand endElementMouseAdd;
        private DelegateCommand<List<string>> startGeoLocateElement;
        private DelegateCommand<ImageRegistrationViewModel> endGeoLocateElement;
        private DelegateCommand<MapElementViewModel> startZOIEdit;
        private DelegateCommand<MapElementViewModel> endZOIEdit;

        private DelegateCommand<List<string>> centerOnElement;

        #region Properties

        public ElementPrototypeViewModel CurrentAddPrototype { get; protected set; }
        public MapElementViewModel CurrentEditElement
        {
            get { return currentEditElement; }
            private set
            {
                if (currentEditElement != null)
                {
                    EndElementZOIEdit(null);
                }
                currentEditElement = value;
            }
        }
        public ImageRegistrationViewModel CurrentGeolocateElement { get; private set; }

        #endregion

        #region Constructor

        public DepictionEnhancedMapViewModel(IDepictionStory depiction)
            : base(depiction)
        {
            GenericElementCommands.Add(StartGeoLocateElementCommand);
            topRevealer = null;
        }

        #endregion

        #region Dispose?

        protected override void OnDispose()
        {
            topRevealer = null;
            base.OnDispose();
        }
        #endregion

        #region command enabled helper
        protected bool IsElementGeoAlignable(List<string> arg)
        {
            if (!IsASingleElementSelected(arg)) return false;
            var possibleModels = WorldModel.GetElementsWithIds(arg);
            if (possibleModels.Count != 1) return false;
            var elementModel = possibleModels[0];
            if (!elementModel.ZoneOfInfluence.DepictionGeometryType.Equals(DepictionGeometryType.Point)) return false;
            if(!elementModel.ImageMetadata.CanBeManuallyGeoAligned) return false;

            return true;
        }
        protected bool IsASingleElementSelected(List<string> arg)
        {
            if (arg == null) return false;
            if (arg.Count != 1) return false;

            return true;
        }
        #endregion

        #region Element add command (mouse)

        public ICommand StartElementAddCommand
        {
            get
            {
                if (startElementAdd == null)
                {
                    startElementAdd = new DelegateCommand<ElementPrototypeViewModel>(StartElementAdd);
                }
                return startElementAdd;
            }
        }

        private void StartElementAdd(ElementPrototypeViewModel elementTypeToAdd)
        {
            StartElementMouseAdd(elementTypeToAdd, false);
        }

        #endregion
        #region End elemetn add command and methods

        public ICommand EndElementAddCommand
        {
            get
            {
                if (endElementMouseAdd == null)
                {
                    endElementMouseAdd = new DelegateCommand(EndElementAdd);
                    endElementMouseAdd.Text = "Click to end element add mode";
                }
                return endElementMouseAdd;
            }
        }
        private void EndElementAdd()
        {
            if (WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIAdd) && CurrentEditElement != null)
            {
                var prototype = CurrentAddPrototype;
                CompleteZOIMouseAdd();
                //Hack, sortof

                CurrentAddPrototype = prototype;
                NotifyPropertyChanged("CurrentAddPrototype");
                WorldCanvasModeVM = DepictionCanvasMode.ZOIAdd;
                if (!doMultiAdd)
                {
                    ResetCanvasStateToNormal();
                }
            }
            else
            {
                //WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIAdd) && 
                if (CurrentEditElement != null)
                {
                    RemoveElementPreviewVM(CurrentEditElement);
                }
                ResetCanvasStateToNormal();
            }
        }
        #endregion
        #region Center on element command

        public ICommand CenterOnElementCommand
        {
            get
            {
                if (centerOnElement == null)
                {
                    centerOnElement = new DelegateCommand<List<string>>(CenterOnElement, IsASingleElementSelected) { Text = "Center map on element" };
                }
                return centerOnElement;
            }
        }

        protected void CenterOnElement(List<string> obj)
        {
            if (obj == null) return;
            if (obj.Count != 1) return;
            var elementModels = WorldModel.GetElementsWithIds(obj);
            if (elementModels.Count != 1) return;
            var elementModel = elementModels[0];
            var latLong = elementModel.Position;
            elementModel.ElementUpdated = true;
            CenterOnLatLongLocation(latLong);
        }

        #endregion
        #region Start image (perhaps non image) geo locate command this can really be cleaned up
        //TODO clean up all this, by mouse, by location, via image etc.
        public ICommand StartGeoLocateElementCommand
        {
            get
            {
                if (startGeoLocateElement == null)
                {
                    startGeoLocateElement = new DelegateCommand<List<string>>(StartGeoLocate, IsElementGeoAlignable);
                    startGeoLocateElement.Text = "Geo-locate element";
                }
                return startGeoLocateElement;
            }
        }

        virtual protected void StartGeoLocate(List<string> elementIDToGeoLocate)
        {
            if (elementIDToGeoLocate.Count != 1) return;
            var possibleModels = WorldModel.GetElementsWithIds(elementIDToGeoLocate);
            if (possibleModels.Count != 1) return;
            var elementModel = possibleModels[0];
            if (elementModel.IsGeolocated && string.IsNullOrEmpty(elementModel.ImageMetadata.ImageFilename))
            {
                CenterOnElement(elementIDToGeoLocate);
            }

            if (!string.IsNullOrEmpty(elementModel.ImageMetadata.ImageFilename))
            {
                var screenPixelBounds = StaticMainWindowToGeoCanvasConverterViewModel.WorldToGeoCanvas(GetGeoRectInteriorToViewport(pixelViewport));
                if (elementModel.ImageMetadata.IsGeoReferenced)
                {
                    var metadata = elementModel.ImageMetadata;
                    screenPixelBounds = StaticMainWindowToGeoCanvasConverterViewModel.WorldToGeoCanvas(metadata.GeoBounds);
                    var parentModel = elementModel as DepictionElementParent;
                    if (parentModel != null)
                    {
                        parentModel.HideImage = true;
                    }
                }
                var elementVM = new MapElementViewModel(elementModel);//this is kind of ridiculus,why?
                CurrentGeolocateElement = new ImageRegistrationViewModel(screenPixelBounds, elementVM);
                RegionSelectionBounds = screenPixelBounds;//minor hack, not sure what it does anymore
                NotifyPropertyChanged("CurrentGeolocateElement");
                WorldCanvasModeVM = DepictionCanvasMode.ImageElementRegistration;
                //It would be nice to hide the old current visible image.
            }
            else
            {
                CurrentAddPrototype = new ElementPrototypeViewModel(elementModel);
                NotifyPropertyChanged("CurrentAddPrototype");//This turns on the add mode
                WorldCanvasModeVM = DepictionCanvasMode.PointElementRegistration;
            }
            
        }
        #endregion

        #region End geolocate element command

        public ICommand EndGeoLocateElementCommand
        {
            get
            {
                if (endGeoLocateElement == null)
                {
                    endGeoLocateElement = new DelegateCommand<ImageRegistrationViewModel>(EndGeoLocate);
                }
                return endGeoLocateElement;
            }
        }
        private void EndGeoLocate(ImageRegistrationViewModel regResult)
        {
            if (CurrentGeolocateElement == null)
            {
                if(CurrentAddPrototype != null)
                {
                    WorldCanvasModeVM = DepictionCanvasMode.Normal;
                    CurrentAddPrototype = null;
                    NotifyPropertyChanged("CurrentAddPrototype");
                }
                return;
            }
            var vm = CurrentGeolocateElement.parentVM;
            NotifyPropertyChanged("CurrentGeolocateElement");
            var parentModel = vm.ElementModel as DepictionElementParent;
            if (parentModel != null)
            {
                parentModel.HideImage = false;
            }
            if (regResult == null)
            {
                //MOre magic race condition fixing, i really really hope to fix this soon dec 2011 happened after removing commoncontrolframe
                WorldCanvasModeVM = DepictionCanvasMode.Normal;
                vm.ViewVisibility = Visibility.Visible;
                CurrentGeolocateElement = null;
                return;
            }
            vm = regResult.parentVM;
            var geoBounds = StaticMainWindowToGeoCanvasConverterViewModel.GeoCanvasToWorld(regResult.ScreenImagePixelBounds);
            vm.UpdateModelImageMetadata(geoBounds, regResult.Rotation.Angle);
            vm.ViewVisibility = Visibility.Visible;

            WorldModel.AddElementListToMainMap(new List<IDepictionElement> { vm.ElementModel });
            //Order changed due to some magic race condition which i don't feel like hunting yet dec 2011
            WorldCanvasModeVM = DepictionCanvasMode.Normal;
            CurrentGeolocateElement = null;
            CurrentAddPrototype = null;
            NotifyPropertyChanged("CurrentAddPrototype");
        }

        #endregion

        #region Start element zoi edit

        public ICommand StartZOIEditCommand
        {
            get
            {
                if (startZOIEdit == null)
                {
                    startZOIEdit = new DelegateCommand<MapElementViewModel>(StartElementZOIEdit);
                }
                return startZOIEdit;
            }
        }

        private void StartElementZOIEdit(MapElementViewModel zoiElement)
        {
            //Small race condition, to deal with later
            CurrentEditElement = null;
            WorldCanvasModeVM = DepictionCanvasMode.ZOIEdit;
            CurrentEditElement = zoiElement;
            NotifyPropertyChanged("CurrentEditElement");
            CurrentEditElement.IsElementInEditMode = true;
        }

        #endregion

        #region End element zoi edit command

        public ICommand EndZOIEditCommand
        {
            get
            {
                if (endZOIEdit == null)
                {
                    endZOIEdit = new DelegateCommand<MapElementViewModel>(EndElementZOIEdit);
                }
                return endZOIEdit;
            }
        }
        //This part isn't pretty or totally understandable, next pass will hopefully clean this up.
        private void EndElementZOIEdit(MapElementViewModel zoiElement)
        {

            if (zoiElement != null)
            {
                if(zoiElement.ZOI != null) zoiElement.ZOI.UpdateModelFromViewModel();
            }
            else
            {
                if (currentEditElement != null && currentEditElement.ZOI != null)
                {
                    currentEditElement.ZOI.UpdateZOIViewFromModel();
                }
            }
            if (currentEditElement != null ) currentEditElement.IsElementInEditMode = false;
            currentEditElement = null;
            NotifyPropertyChanged("CurrentEditElement");
            //Order is import to beat the focus race condition (ggrrr)
            WorldCanvasModeVM = DepictionCanvasMode.Normal;
        }

        #endregion

        #region Adding helpers

        public void CompleteZOIMouseAdd()
        {
            if (!WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIAdd) || CurrentEditElement == null) return;
            var vertexCount = CurrentEditElement.ZOI.ZOIToDraw[0].Outline.Count;//this could fail, but i really never should
            RemoveElementPreviewVM(CurrentEditElement);

            if (CurrentAddPrototype.ZOIShape().Equals(ZOIShapeType.UserPolygon))
            {
                if (vertexCount >= 3)
                {
                    CurrentEditElement.IsElementPreview = false;
                    CurrentEditElement.ZOI.CloseAndFillZOIToDraw();
                    AddElementVMToWorldModel(CurrentEditElement);
                }
            }
            if (CurrentAddPrototype.ZOIShape().Equals(ZOIShapeType.UserLine))
            {
                if (vertexCount >= 2)
                {
                    CurrentEditElement.IsElementPreview = false;
                    AddElementVMToWorldModel(CurrentEditElement);
                }
            }
            CurrentEditElement = null;
            endElementMouseAdd.Text = "Click to end element add mode";
        }

        public void ClearElementHolderProperties()
        {
            CurrentEditElement = null;
            CurrentGeolocateElement = null;
            CurrentAddPrototype = null;
            NotifyPropertyChanged("CurrentAddPrototype");
        }
        public void ResetCanvasStateToNormal()
        {
            ClearElementHolderProperties();
            WorldCanvasModeVM = DepictionCanvasMode.Normal;
            CurrentAddPrototype = null;
            NotifyPropertyChanged("CurrentAddPrototype");
        }
        virtual protected void ShowElementPropertyInfoDialog(ElementPropertyDialogContentVM elementPropertiesVM)
        {//the location of the show element property stuff is in flux
        }

        public void StartElementMouseAdd(ElementPrototypeViewModel elementTypeToAdd, bool multiAdd)
        {
            doMultiAdd = multiAdd;
            ClearElementHolderProperties();
            if (elementTypeToAdd.IsPlaceableByMouse)
            {
                WorldCanvasModeVM = DepictionCanvasMode.Normal;
                CurrentAddPrototype = elementTypeToAdd;
                NotifyPropertyChanged("CurrentAddPrototype");
                //Order is important
                if (elementTypeToAdd.ElementType.Equals("Depiction.Annotation"))
                {
                    WorldCanvasModeVM = DepictionCanvasMode.AnnotationAdd;
                }
                else
                {
                    if (elementTypeToAdd.AddByZOI)
                    {
                        WorldCanvasModeVM = DepictionCanvasMode.ZOIAdd;
                    }
                    else
                    {
                        WorldCanvasModeVM = DepictionCanvasMode.ElementAdd;
                    }
                }
            }
            else if (elementTypeToAdd.IsAddableByFile) { }
        }
        public void AddPrototypeToLocation(ElementPrototypeViewModel prototype, Point location, bool centerOnLocation)
        {
            if (prototype == null) return;
            var model = prototype.Model;
            if (model is IElementPrototype)
            {
                var elementModel = ElementFactory.CreateElementFromPrototype((IElementPrototype)model);
                elementModel.SetInitialPositionAndZOI(
                    DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(location), null);
                var initialTag = Tags.DefaultMouseTag;
                elementModel.Tags.Add(initialTag);
                //                elementModel.Position = GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(location);
                var viewModel = new MapElementViewModel(elementModel);
                if (WorldCanvasModeVM.Equals(DepictionCanvasMode.ZOIAdd) && CurrentEditElement == null)
                {
                    CurrentEditElement = viewModel;
                    CurrentEditElement.IsElementPreview = true;
                    MainMapDisplayerViewModel.AddTempElementVM(CurrentEditElement);
                    endElementMouseAdd.Text = "Click to complete element zoi and start new element";
                    return;
                }

                viewModel.IsElementPreview = false;
                bool showPreview;
                //TODO this should probably check for the oncreate or something (maybe?)
                if (elementModel.GetPropertyValue("ShowElementPropertiesOnCreate", out showPreview))
                {
                    if (showPreview)
                    {
                        viewModel.IsElementPreview = showPreview;
                        MainMapDisplayerViewModel.AddTempElementVM(viewModel);
                        var dialogContent = new ElementPropertyDialogContentVM(viewModel);
                        ShowElementPropertyInfoDialog(dialogContent);
                        if (!doMultiAdd)
                        {
                            EndElementAdd();
                        }
                        return;
                    }
                }
                DoHokeyElementActions(elementModel);
                AddElementVMToWorldModel(viewModel);
                if (!doMultiAdd)
                {
                    EndElementAdd();
                }
            }
            else if (model is IDepictionElement)
            {//Re-geo-locating an element by mouse
                var existingModel = (IDepictionElement)model;
                var geoLocation = DepictionAccess.GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(location);
                existingModel.Position = geoLocation;
                WorldModel.AddElementListToMainMap(new List<IDepictionElement> { existingModel });
                if (centerOnLocation)
                {
                    CenterOnLatLongLocation(geoLocation);
                }
                EndElementAdd();
            }
            else if (model is IDepictionAnnotation)
            {
                AddAnnotationAtLocation(location);
                if (!doMultiAdd)
                {
                    EndElementAdd();
                }
            }
        }
        public void AddTempElementWithPropertyDialogForElement(IDepictionElement element)
        {
            var viewModel = new MapElementViewModel(element);
            viewModel.IsElementPreview = true;
            MainMapDisplayerViewModel.AddTempElementVM(viewModel);
            var dialogContent = new ElementPropertyDialogContentVM(viewModel);
            ShowElementPropertyInfoDialog(dialogContent);
        }
        public void AddElementVMToWorldModel(MapElementViewModel elementVM)
        {
            var elementModel = elementVM.ElementModel;
            var position = elementModel.Position;
            if (elementVM.IsElementPreview)
            {
                MainMapDisplayerViewModel.RemoveTempElementVM(elementVM.ElementKey);

            }
            DoHokeyElementActions(elementModel);
            WorldModel.AddElementGroupToDepictionElementList(new List<IDepictionElement> { elementModel });

            WorldModel.AddElementListToMainMap(new List<IDepictionElement> { elementModel });
            //if (elementVM.IsElementPreview)
            // {
            // elementModel.Position = null;
            // elementModel.Position = position;
            //}//Hopefully something else takes care of the clean up

        }
        public void DoHokeyElementActions(IDepictionElement elementModel)
        {
            if (elementModel == null) return;
            if (elementModel.CreateActions != null)
            {
                var behaviorDict = AddinRepository.Instance.GetBehaviors();
                foreach (var action in elementModel.CreateActions)
                {
                    var key = action.Key;
                    var behaviorList = behaviorDict.Where(t => t.Key.BehaviorName.Equals(key)).ToList();
                    if (behaviorList.Count == 1)
                    {
                        var behavior = behaviorList[0].Value;
                        behavior.DoBehavior(elementModel, action.Value);
                    }
                }
            }
            if (elementModel.GenerateZoiActions != null)
            {
                var behaviorDict = AddinRepository.Instance.GetBehaviors();
                foreach (var action in elementModel.GenerateZoiActions)
                {
                    var key = action.Key;
                    var behaviorList = behaviorDict.Where(t => t.Key.BehaviorName.Equals(key)).ToList();
                    if (behaviorList.Count == 1)
                    {
                        var behavior = behaviorList[0].Value;
                        behavior.DoBehavior(elementModel, action.Value);
                    }
                }
            }
        }

        public void RemoveElementPreviewVM(MapElementViewModel elementPreview)
        {
            if (elementPreview.IsElementPreview)
            {
                MainMapDisplayerViewModel.RemoveTempElementVM(elementPreview.ElementKey);
            }
        }
        virtual public void AddElementListToWorldModel(IEnumerable<IDepictionElement> elements)
        {
            WorldModel.AddElementGroupToDepictionElementList(elements);
            //This part might have to be optional
            WorldModel.AddElementListToMainMap(elements);
        }
        //        public void FillScreenWithElements(IElementPrototype elementType)
        //        {
        //            var pixelRect = PixelViewport;
        ////            Console.WriteLine(PixelViewport);
        //            var w = pixelRect.Width;
        //            var h = pixelRect.Height;
        //            int wC = 30;//100;//
        //            int hC = 30;//60;//
        //            int rowCount = (int)(w / wC);
        //            int colCount = (int)(h / hC);
        //            //            AddElementFromPrototypeModelAtPosition(elementType,pixelRect.TopLeft);
        //            //            AddElementFromPrototypeModelAtPosition(elementType, pixelRect.TopRight);
        //            //            AddElementFromPrototypeModelAtPosition(elementType, pixelRect.BottomLeft);
        //            //            AddElementFromPrototypeModelAtPosition(elementType, pixelRect.BottomRight);
        //            var modelList = new List<IDepictionElementBase>();
        //            for (int i = 0; i < wC; i++)
        //            {
        //                for (int j = 0; j < hC; j++)
        //                {
        //                    var location = new Point(pixelRect.Left + (i * rowCount), pixelRect.Top + (j * colCount));
        //                    //                    AddElementFromPrototypeModelAtPosition(elementType,location);
        //
        //                    var elementModel = AddElementWithZOI(location);
        //                    //                    var elementModel = ElementFactory.CreateElementFromPrototype(elementType);
        //                    //                    elementModel.Position = GeoCanvasToPixelCanvasConverter.GeoCanvasToWorld(location);
        //                    modelList.Add(elementModel);
        //
        //                }
        //            }
        //            Console.WriteLine("Done creating all the models");
        //            AddElementListToWorldModel(modelList);
        //            Console.WriteLine("Done adding all the models to element repos");
        //
        //            //            WorldModel.AddSingleElementToDepictionElementList(elementModel);
        //
        //        }
        #endregion
    }
}
