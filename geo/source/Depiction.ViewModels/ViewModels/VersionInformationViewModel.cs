﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels
{
    public class VersionInformationViewModel : ViewModelBase
    {
        private static VersionInformationViewModel instance;

        private VersionInformationViewModel()
        { }

        public static VersionInformationViewModel Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new VersionInformationViewModel();
                }
                return instance;
            }
        }

        private string versionWithBuild;
        public string VersionWithBuild
        {
            get { return versionWithBuild; }
            set
            {
                versionWithBuild = value;
                NotifyPropertyChanged("VersionWithBuild");
            }
        }

        private string _expirationDate;
        public string ExpirationDate
        {
            get
            {
                return _expirationDate ?? "Never";
            }
            set
            {
                _expirationDate = value;
                NotifyPropertyChanged("ExpirationDate");
            }
        }

        private string version;
        public string Version
        {
            get { return version; }
            set
            {
                version = value;
                NotifyPropertyChanged("Version");
            }
        }

        private bool isTrialMode;
        public bool IsTrialMode
        {
            get { return isTrialMode; }
            set
            {
                isTrialMode = value;
                NotifyPropertyChanged("IsTrialMode");
            }
        }

        private string serialNumber;
        public string SerialNumber
        {
            get { return serialNumber; }
            set
            {
                serialNumber = value;
                NotifyPropertyChanged("SerialNumber");
            }
        }
    }
}