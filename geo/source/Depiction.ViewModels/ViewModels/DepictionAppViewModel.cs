﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.DepictionConfiguration;
using Depiction.API.ExceptionHandling;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MessagingObjects;
using Depiction.API.MVVM;
using Depiction.API.Service;
using Depiction.API.ValueTypes;
using Depiction.CoreModel;
using Depiction.CoreModel.DepictionObjects;
using Depiction.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.DialogViewModels;
using Depiction.ViewModels.ViewModels.MapViewModels;
using Depiction.ViewModels.ViewModels.TileViewModels;


namespace Depiction.ViewModels.ViewModels
{
    public class DepictionAppViewModel : ViewModelBase
    {
        //Kind of a hack, but should just be temp
        public InteractionGraph InteractionGraph { get; set; }

        public string DepictionTitle
        {
            get
            {
                var baseType = ProductInformationType.ProductName + " " + VersionInfo.SimpleAppVersion;
                if (MapViewModel != null)
                {
                    var fileName = MapViewModel.WorldModel.DepictionsFileName;
                    if (!string.IsNullOrEmpty(fileName))
                    {
                        baseType += " : " + Path.GetFileName(fileName);
                    }
                }
                return baseType;
            }
        }
        private DepictionApplication depictionApp;

        static public event Func<string, FileFilter, KeyValuePair<string, IDepictionElementExporter>> GetFileNameToSaveAndExporter;
        static public event Func<string, FileFilter, KeyValuePair<string, IDepictionImporterBase>> GetFileNameToLoadAndImporter;

        public TileDisplayerViewModel TileDisplayerVM { get; set; }

        #region command variables

        private DelegateCommand<CurrentAppDepictionScreen> changeAppScreenCommand;
        private DelegateCommand exitCommand;

        #endregion

        private CurrentAppDepictionScreen currentScreen = CurrentAppDepictionScreen.Welcome;
        private bool acceptFileDrops = false;
        public CurrentAppDepictionScreen CurrentScreen
        {
            get { return currentScreen; }
            private set
            {
                currentScreen = value;
                if (MapViewModel != null)
                {
                    MapViewModel_CanvasVMModeChanged(MapViewModel.WorldCanvasModeVM);
                }
                NotifyPropertyChanged("CurrentScreen");
            }
        }
        private DepictionMapQuickAddAndTileViewModel mapViewModel;

        #region Dialog vms
        public DepictionAppHelpDialogVM AppGeneralHelpDialogVM { get; private set; }
        public OptionsDialogVM DepictionPropertySettingsDialogVM { get; private set; }
        public AboutDialogVM AboutDialogVM { get; private set; }
        public EulaDialogVM EulaDialogVM { get; private set; }
        public UpdateDialogVM UpdateDialogVM { get; private set; }
//        public LicenseDialogVM LicenseDialogVM { get; private set; }
        public ToolsDialogVM ToolsDialogVM { get; private set; }
        public FileDialogVM FileDialogVM { get; private set; }
        public AdvancedDialogVM AdvancedDialogVM { get; private set; }
        public BugSubmitterDialogVM BugSubmitterDialogVM { get; private set; }
        public SaveLoadDialogVM SaveLoadDialogVM { get; private set; }
        public DepictionPrintDialogVM PrintDialogVM { get; set; }
        public ElementDefinitionEditorDialogViewModel ElementDefinitionEditorVm { get; set; }
        public InteractionRepositoryDialogViewModel InteractionRepositoryVM { get; set; }
        public DepictionFileInformationDialogViewModel FileInformationVM { get; set; }
        public TipControlDialogVM DepictionStartTipDialogVM { get; set; }

        public DepictionMapQuickAddAndTileViewModel MapViewModel
        {
            get { return mapViewModel; }
            private set { mapViewModel = value; NotifyPropertyChanged("MapViewModel"); }
        }

        #endregion

        public bool IsDepictionPrinting { get; set; }
        public bool AcceptFileDrops
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return false;
                }
                //#if IS_READER
                //                return false;
                //#endif
                return acceptFileDrops;
            }
            set
            {
                acceptFileDrops = value;
                NotifyPropertyChanged("AcceptFileDrops");
            }
        }

        #region _productInformation stuff

        static public ProductInformationBase ProductInformationType { get { return DepictionAccess.ProductInformation; } }

        #endregion

        #region Constuctor

        public DepictionAppViewModel(FogBugzPoller bugzPoller)//, ILicenseService licenseService)
        {
            CurrentScreen = CurrentAppDepictionScreen.Welcome;
            AppGeneralHelpDialogVM = new DepictionAppHelpDialogVM();
            DepictionPropertySettingsDialogVM = new OptionsDialogVM();
            AboutDialogVM = new AboutDialogVM();
            EulaDialogVM = new EulaDialogVM();
            UpdateDialogVM = new UpdateDialogVM();
            FileDialogVM = new FileDialogVM();
            AdvancedDialogVM = new AdvancedDialogVM();
            ToolsDialogVM = new ToolsDialogVM();
//            LicenseDialogVM = new LicenseDialogVM(licenseService);
            BugSubmitterDialogVM = new BugSubmitterDialogVM(bugzPoller);
            PrintDialogVM = new DepictionPrintDialogVM();
            ElementDefinitionEditorVm = new ElementDefinitionEditorDialogViewModel();
            InteractionRepositoryVM = new InteractionRepositoryDialogViewModel();
            FileInformationVM = new DepictionFileInformationDialogViewModel();
            DepictionStartTipDialogVM = new TipControlDialogVM();
            TileDisplayerVM = new TileDisplayerViewModel();
            var depictApp = Application.Current as DepictionApplication;
            if (depictApp != null)
            {
                SaveLoadDialogVM = new SaveLoadDialogVM(depictApp.saveLoadManager);
                SaveLoadDialogVM.IsModel = true;
            }
            //GeoCoder = DepictionAccess.GeoCodingService;

            if (Application.Current != null)
            {
                depictionApp = Application.Current as DepictionApplication;
                if (depictionApp != null)
                {
                    depictionApp.DepictionChanged += depictionApp_DepictionChanged;
                }
            }
        }
        #endregion

        #region Connection to the map canvas view model
        static internal bool DelayLoad = true;
        void depictionApp_DepictionChanged(IDepictionStory oldDepiction, IDepictionStory newDepiction)
        {
            Debug.WriteLine("Depiction story change from depiction app view model");
            if (DepictionAccess.BackgroundServiceManager != null)
            {
                DepictionAccess.BackgroundServiceManager.ResetServiceManager();
            }

            if (oldDepiction != null)
            {
                if (MapViewModel != null)
                {
                    MapViewModel.CanvasVMModeChanged -= MapViewModel_CanvasVMModeChanged;
                    MapViewModel.Dispose();
                    MapViewModel = null;
                }
                FileInformationVM.IsDialogVisible = false;
                InteractionRepositoryVM.IsDialogVisible = false;
                ElementDefinitionEditorVm.IsDialogVisible = false;
                FileDialogVM.IsDialogVisible = false;
                AdvancedDialogVM.IsDialogVisible = false;
                ToolsDialogVM.IsDialogVisible = false;
                AppGeneralHelpDialogVM.IsDialogVisible = false;
            }
            GC.Collect();
            if (newDepiction != null)
            {
                Debug.WriteLine("Depiction story change from depiction: Stoping tyling");
                //if (TileDisplayerVM != null) TileDisplayerVM.StopTiling();
                var depApp = Application.Current as DepictionApplication;
                //Hmm, maybe the viewmodel should have a setter, maybe, i guess it might mess with datacontext changing
                //if the depictionstory changes but not the vm
                Debug.WriteLine("Depiction story change from depiction: creating map view model");

                var newViewModel = new DepictionMapQuickAddAndTileViewModel(newDepiction, depApp.WorldOutlines);

                newViewModel.CanvasVMModeChanged += MapViewModel_CanvasVMModeChanged;
                //Hack for element commands
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        break;
                    default:
                        newViewModel.GenericElementCommands.Add(ElementDefinitionEditorVm.LoadPrototypeElementKeyCommand);
                        break;
                }

                if (depApp != null)
                {

                    //Ack for now load everything everytime :(
                    //newViewModel.AddContentDialogViewModel.SetElementPrototypeVMs(AllElementPrototypes);
                    Debug.WriteLine("Depiction story change from depiction: refreshing quickstart");
                    newViewModel.QuickStartDialogViewModel.UpdateQSStuffInBackground(newViewModel);
                    Debug.WriteLine("Depiction story change from depiction: attaching tiler");
                    newViewModel.AttachTilerVMToMainMap(TileDisplayerVM);

                    Debug.WriteLine("Depiction story change from depiction: setting to open street maps");
                    //HACK!! Openstreetmap. This is soooo bad, this should be done when the story gets created, not in the view
                    //model
                    if (newViewModel.WorldCanvasModeVM.Equals(DepictionCanvasMode.RegionSelection))
                    {
                        TileDisplayerVM.TileProviderType = TileImageTypes.Street;
                        if (TileDisplayerVM.SelectableTileProviders.Any())
                        {
//#if DEBUG
                            var selectedTiler = TileDisplayerVM.SelectableTileProviders.First();
//#else
//                            //this kills debug becuase of pinvoke issues
//                            var selectedTiler = TileDisplayerVM.SelectableTileProviders.
//                                    FirstOrDefault(t => t.DisplayName.ToLower().Contains("naip"));
//                            if (selectedTiler == null) selectedTiler = TileDisplayerVM.SelectableTileProviders.First();
//#endif
                            TileDisplayerVM.SelectedTileProvider = selectedTiler;
//                            TileDisplayerVM.TileProviderType = selectedTiler.TileImageType;
                        }
                        else
                        {

//                            TileDisplayerVM.TileProviderType = TileImageTypes.None;
                            TileDisplayerVM.SelectedTileProvider = TileDisplayerVM.SelectableTileProviders.First();
//                            TileDisplayerVM.TileProviderType = TileDisplayerVM.SelectedTileProvider.TileImageType;
                        }
                    }
                }
                MapViewModel = newViewModel;
                // mapViewModel.DisplayContentDialogViewModel.CurrentActiveDisplayerType = DepictionDisplayerType.MainMap;//.CurrentActiveDisplayer = MapViewModel.MainMapDisplayerViewModel;
                CurrentScreen = CurrentAppDepictionScreen.World;
                Debug.WriteLine("Depiction story change from depiction: setting interaction graph");
                InteractionGraph = DepictionAccess.InteractionsRunner.InteractionGraph;
                NotifyPropertyChanged("InteractionGraph");
                InteractionRepositoryVM.SetInteractionsLibrary(newDepiction.InteractionRuleRepository);
            }
            else
            {
                if (TileDisplayerVM != null)
                {
                    //TileDisplayerVM.StopTiling();
                }
                if (MapViewModel != null) MapViewModel.Dispose();
                MapViewModel = null;
            }

            NotifyPropertyChanged("GlobalPanCommand");
            NotifyPropertyChanged("GlobalZoomCommand");
            UpdateDepictionTitle();

            if (DelayLoad && newDepiction != null)
            {
                var timer = new DispatcherTimer();
                timer.Interval = new TimeSpan(0, 0, 1);
                var count = MapViewModel.MainMapDisplayerViewModel.DisplayerModel.ElementIdsInDisplayer.Count();
                foreach (var revealer in MapViewModel.Revealers)
                {
                    var addable = revealer as RevealerDisplayerViewModel<IDepictionRevealer>;
                    if (addable != null)
                    {
                        count += addable.BaseDisplayerModel.ElementIdsInDisplayer.Count();
                    }
                }
                DepictionMessage message = null;
                if (count > 0)
                {
                    message = new DepictionMessage("Drawing elements");
                    DepictionAccess.NotificationService.DisplayMessage(message);
                }
                timer.Tick += new EventHandler(delegate(object sender, EventArgs e)
                                                   {
                                                       var loadedElements = newDepiction.GetElementsWithIds(MapViewModel.MainMapDisplayerViewModel.DisplayerModel.ElementIdsInDisplayer.ToList());
                                                       MapViewModel.MainMapDisplayerViewModel.AddElementListToDisplayer(loadedElements);
                                                       foreach (var revealer in MapViewModel.Revealers)
                                                       {
                                                           var addable = revealer as RevealerDisplayerViewModel<IDepictionRevealer>;
                                                           if (addable != null)
                                                           {
                                                               loadedElements = newDepiction.GetElementsWithIds(addable.BaseDisplayerModel.ElementIdsInDisplayer.ToList());
                                                               addable.AddElementListToDisplayer(loadedElements);
                                                           }
                                                       }
                                                       timer.Stop();
                                                       if (message != null) DepictionAccess.NotificationService.RemoveMessage(message);

                                                   });
                timer.Start();
            }
        }


        void MapViewModel_CanvasVMModeChanged(DepictionCanvasMode canvasMode)
        {
            if (canvasMode.Equals(DepictionCanvasMode.Normal) && CurrentScreen.Equals(CurrentAppDepictionScreen.World))
            {
                AcceptFileDrops = true;
            }
            else
            {
                AcceptFileDrops = false;
            }
        }
        #endregion
        public void UpdateDepictionTitle()
        {
            NotifyPropertyChanged("DepictionTitle");
        }

        #region Tutorial command

        public ICommand ShowTutorialsCommand
        {
            get
            {
                var showTutorialsCommand = new DelegateCommand(ShowTutorials);
                showTutorialsCommand.Text = string.Format("Go to {0} tutorial page",DepictionAccess.ProductInformation.ProductName);
                return showTutorialsCommand;
            }
        }

        private void ShowTutorials()
        {

            string url = string.Format("{0}/tutorials",DepictionAccess.ProductInformation.ProductWebpage);
            DepictionInternetConnectivityService.OpenBrowserInOrderTo(url, "view tutorials");
        }

        #endregion

        #region Purchase command  last minute hack

        static public ICommand PurchaseDepictionCommand
        {
            get
            {
                var purchaseDepictionCommand = new DelegateCommand(PurchaseDepiction);
                purchaseDepictionCommand.Text = string.Format("Go to {0} purchase page",DepictionAccess.ProductInformation.ProductName);
                return purchaseDepictionCommand;
            }
        }

        static private void PurchaseDepiction()
        {

            string url = string.Format("{0}/purchase",DepictionAccess.ProductInformation.ProductWebpage);

            if (DepictionAccess.ProductInformation.ProductType.Equals(ProductInformationBase.Reader))//is DepictionReaderProductInformationB)
            {
                url = string.Format("{0}?src=reader",url);
            }

            DepictionInternetConnectivityService.OpenBrowserInOrderTo(url, string.Format("Purchase {0}",DepictionAccess.ProductInformation.ProductName));
        }
        #endregion

        //        #region Update depiction command  last minute hack
        //
        //        public ICommand UpdateDepictionCommand
        //        {
        //            get
        //            {
        //                var updateDepictionCommand = new DelegateCommand(UpdateDepiction);
        //                updateDepictionCommand.Text = "Update depiction";
        //                return updateDepictionCommand;
        //            }
        //        }
        //
        //        private void UpdateDepiction()
        //        {
        //            UpdateDialogVM.IsDialogVisible = true;
        //        }
        //        #endregion


        #region Change screen command

        public ICommand ChangeAppScreenCommand
        {
            get
            {
                if (changeAppScreenCommand == null)
                {
                    changeAppScreenCommand = new DelegateCommand<CurrentAppDepictionScreen>(ChangeDepictionScreen);
                }
                return changeAppScreenCommand;
            }
        }

        private void ChangeDepictionScreen(CurrentAppDepictionScreen newScreen)
        {
            if (CurrentScreen.Equals(newScreen)) return;
            if (CurrentScreen.Equals(CurrentAppDepictionScreen.World))
            {
                PrintDialogVM.IsDialogVisible = false;
                if (depictionApp != null && depictionApp.CurrentDepiction != null)
                {
                    if (depictionApp.CurrentDepiction.DepictionNeedsSaving)
                    {
                        ApplicationCommands.SaveAs.Execute(true, Application.Current.MainWindow);
                    }
                    if (depictionApp.ContinueApplicationRequest)
                    {
                        depictionApp.SetNewDepictionStory(null);
                    }
                    else
                    {
                        depictionApp.ContinueApplicationRequest = true;
                        return;
                    }
                    depictionApp.ContinueApplicationRequest = true;
                }
            }

            CurrentScreen = newScreen;
            GC.Collect();
        }

        #endregion

        #region CHange to map location screen

        public ICommand ToDepictionWorldCanvasAtLocationCommand
        {
            get
            {
                return new DelegateCommand<string>(ToMapScreenAtLatLongLocation);
            }
        }

        private void ToMapScreenAtLatLongLocation(string stringLocation)
        {
            //Send the string to the geolocation
            var geocodeResult = DepictionAccess.GeoCodingService.GeoCodeRawStringAddress(stringLocation);
            if (geocodeResult == null || !geocodeResult.IsValid)
            {
                var message = string.Format("Could not geo-code the desired location : {0}", stringLocation);
                DepictionAccess.NotificationService.DisplayMessageString(message, 4);
                return;
            }
            var result = geocodeResult.Position;
            ILatitudeLongitude startLatLong = new LatitudeLongitude(47.6505, -122.3215);//Seattle
            DepictionAccess.InteractionsLibrary.RefreshInteractionLibrary();
            DepictionAccess.GeoCodingService.AddLocationPairToStartCache(stringLocation, result);
            var config = DepictionUserConfigurationManager.DepictionUserConfiguration();
            if (config == null) return;
            var section = DepictionUserConfigurationManager.GetSectionDefaultDepictionConfigurationSection(config);

            section.RecentStartLocation = stringLocation;
            config.Save(ConfigurationSaveMode.Full);
            //            DepictionUserConfigurationManager.DepictionUserConfiguration().Save(ConfigurationSaveMode.Full);

            startLatLong = result;
            NotifyPropertyChanged("PreviousLocations");
            Debug.WriteLine("Done with geo locating");

            var newDepiction = new DepictionStory(startLatLong);
            Debug.WriteLine("Done creating new depiction story");
            if (depictionApp != null)
            {
                depictionApp.SetNewDepictionStory(newDepiction);
            }
            Debug.WriteLine("Done creating setting new depiction story view models etc");
        }
        //Man i don't like this
        public GeolocatedLocationRecords PreviousLocations
        {
            get
            {
                var config = DepictionUserConfigurationManager.DepictionUserConfiguration();
                if (config == null) return new GeolocatedLocationRecords();
                var section = DepictionUserConfigurationManager.GetSectionDefaultDepictionConfigurationSection(config);

                return section.SelectStartGeolocations;
            }
        }

        public GeolocatedLocationElement RecentLocation
        //        public string RecentLocation
        {
            get
            {
                var config = DepictionUserConfigurationManager.DepictionUserConfiguration();
                if (config == null)
                    return null;
                var section = DepictionUserConfigurationManager.GetSectionDefaultDepictionConfigurationSection(config);
                for (int i = 0; i < section.SelectStartGeolocations.Count; i++)
                {
                    if (section.SelectStartGeolocations[i].LocationName.Equals(section.RecentStartLocation))
                    {
                        return section.SelectStartGeolocations[i];
                    }
                }
                return null;
                //                return section.RecentStartLocation;
            }
        }

        #endregion

        #region Exit command
        public ICommand ExitCommand
        {
            get
            {
                if (exitCommand == null)
                {
                    exitCommand = new DelegateCommand(Exit);
                    exitCommand.Text = string.Format("Exit {0}", DepictionAccess.ProductInformation.ProductName);
                }
                return exitCommand;
            }
        }

        private void Exit()
        {
            Application.Current.MainWindow.Close();
        }
        #endregion

        #region File browser methods

        static public KeyValuePair<string, IDepictionImporterBase> GetFileToLoadAndImporter(string startDir)
        {
            var fileName = new KeyValuePair<string, IDepictionImporterBase>();

            if (GetFileNameToLoadAndImporter != null)
            {
                fileName = GetFileNameToLoadAndImporter.Invoke(startDir, null);
            }

            return fileName;
        }

        static public string GetFileNameToLoad(string startDir, FileFilter filter)
        {
            var fileName = new KeyValuePair<string, IDepictionImporterBase>();
            if (filter == null) filter = FileFilter.AllTypesFilter;
            if (GetFileNameToLoadAndImporter != null)
            {
                fileName = GetFileNameToLoadAndImporter.Invoke(startDir, filter);
            }
            if (Directory.Exists(fileName.Key))
            {
                return "";
            }
            return fileName.Key;
        }
        public static KeyValuePair<string, IDepictionElementExporter> GetFileToSaveNameAndExporter(string startDir)//, List<IDepictionElementExporter> writers)
        {
            var fileName = new KeyValuePair<string, IDepictionElementExporter>();
            if (GetFileNameToSaveAndExporter != null)
            {
                fileName = GetFileNameToSaveAndExporter.Invoke(startDir, null);
            }
            return fileName;
        }

        static public string GetFileToSaveName(string startDir, FileFilter filter)
        {
            if (filter == null) filter = FileFilter.AllTypesFilter;
            var fileName = new KeyValuePair<string, IDepictionElementExporter>();
            if (GetFileNameToSaveAndExporter != null)
            {
                fileName = GetFileNameToSaveAndExporter.Invoke(startDir, filter);
            }
            if (Directory.Exists(fileName.Key))
            {
                return "";
            }
            return fileName.Key;
        }


        #endregion
    }
}