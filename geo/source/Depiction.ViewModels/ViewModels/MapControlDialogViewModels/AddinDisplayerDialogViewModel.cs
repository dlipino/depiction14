using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.MapControlDialogViewModels
{
    public class AddinDisplayerDialogViewModel : DialogViewModelBase
    {
//        private UserAddinImporter userAddinImporter;

        private DelegateCommand refreshUserAddins;
        private DelegateCommand<object> showPropertyDialogCommand;
        private DelegateCommand<object> showActivationDialogCommand;

        #region Properties

        public bool DisplayDefaultAddons
        {
            get
            {
                return false;
            }
        }

        public List<object> DepictionDefaultAddinMetadata
        {
            get
            {
                return AddinRepository.Instance.GetDefaultAddinMetadata();
            }
        }
        public Dictionary<IBaseMetadata, IDepictionAddonBase> Depiction3rdPartyAddins
        {
            get
            {
                var d = AddinRepository.Instance.Addons;
                var a = AddinRepository.Instance.GetNonDefaultImporters();
                var b = AddinRepository.Instance.GetBehaviors();
                var c = new Dictionary<IBaseMetadata, IDepictionAddonBase>();

                foreach (var importer in a)
                {
                    c.Add(importer.Key, importer.Value);
                }
                foreach (var behavior in b)
                {
                    if (behavior.Value is IDepictionAddonBase)
                        c.Add(behavior.Key, (IDepictionAddonBase)behavior.Value);
                }

                foreach (var addon in d)
                {
                    if (!addon.Key.IsDefault)
                        c.Add(addon.Key, addon.Value);
                }
                return c;
            }
        }
        #endregion
        #region commands

        #region refresh command and methods
        public ICommand RefreshUserAddinsCommand
        {
            get
            {
                if (refreshUserAddins == null)
                {
                    refreshUserAddins = new DelegateCommand(RefreshUserAddins);
                }
                return refreshUserAddins;
            }
        }

        private void RefreshUserAddins()
        {
//            //TODO fix this
//            if (userAddinImporter == null) return;
//            userAddinImporter.ImportUserAddins();
//            Addins = new Dictionary<IDepictionExampleAddinMetadata, IDepictionExampleAddin>();
//            foreach (var addins in userAddinImporter.UserAddins)
//            {
//                Addins.Add(addins.Key, addins.Value);
//            }
//            NotifyPropertyChanged("Addins");
        }
        #endregion

        #region Display the user user created dialog for modifing the addin attributes

        public ICommand ShowPropertyModifyingDialogCommand
        {
            get
            {
                if(showPropertyDialogCommand == null)
                {
                    showPropertyDialogCommand = new DelegateCommand<object>(ShowPropertyDialog,
                                                                            DoesAddinHavePropertyDialog);
                }
                return showPropertyDialogCommand;
            }
        }

        private bool DoesAddinHavePropertyDialog(object arg)
        {
            var addon = arg as IDepictionAddonBase;
            if (addon == null) return false;
            if (addon.AddonConfigView != null) return true;
            return false;
        }

        private void ShowPropertyDialog(object obj)
        {
            var addon = obj as IDepictionAddonBase;
            if (addon == null) return;

            var mainWindow = (Application.Current.MainWindow as IDepictionMainWindow);
            //Will this work?
            if (mainWindow == null) return;

//            var commonControl = addon.AddonConfigView as CommonControlFrameBase;
//            if (commonControl != null)
//            {
//                mainWindow.AddContentControlToMapCanvas(commonControl);
//                commonControl.IsShown = true;
//                commonControl.CenterWidthAndGivenTop(100);
//                return;
//            }


            var window = addon.AddonConfigView as Window;
            if(window != null)
            {
                window.Owner = Application.Current.MainWindow;
                window.Show();
                return;
            }
        }

        #endregion

        #region copypasta from above but for activation dialog

        public ICommand ShowActivationDialogCommand
        {
            get
            {
                if (showActivationDialogCommand == null)
                {
                    showActivationDialogCommand = new DelegateCommand<object>(ShowActivationDialog,
                                                                            DoesAddinHaveActivationDialog);
                    showActivationDialogCommand.Text = "Activate Add-on";
                }
                return showActivationDialogCommand;
            }
        }

        private bool DoesAddinHaveActivationDialog(object arg)
        {
            var addon = arg as IDepictionAddonBase;
            if (addon == null) return false;
            if (addon.AddonActivationView != null) return true;
            return false;
        }

        private void ShowActivationDialog(object obj)
        {
            var addon = obj as IDepictionAddonBase;
            if (addon == null) return;

            var mainWindow = (Application.Current.MainWindow as IDepictionMainWindow);
            //Will this work?
            if (mainWindow == null) return;

//            var commonControl = addon.AddonActivationView as CommonControlFrameBase;
//            if (commonControl != null)
//            {
//                mainWindow.AddContentControlToMapCanvas(commonControl);
//                commonControl.IsShown = true;
//                commonControl.CenterWidthAndGivenTop(100);
//                return;
//            }


            var window = addon.AddonActivationView as Window;
            if (window != null)
            {
                window.Owner = Application.Current.MainWindow;
                window.Show();
                return;
            }
        }

        #endregion

        #endregion

        #region Constructor
        //        public AddinDisplayerDialogViewModel(UserAddinImporter userImporter)
//        {
//            userAddinImporter = userImporter;
//        }
        
        #endregion

    }
}