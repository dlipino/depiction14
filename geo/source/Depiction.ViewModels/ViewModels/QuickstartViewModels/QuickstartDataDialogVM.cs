﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.AddinObjects.Interfaces;
using Depiction.API.AddinObjects.Interfaces.Metadata;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExceptionHandling;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.MEFRepository;
using Depiction.API.MVVM;
using Depiction.CoreModel.DepictionObjects.Repositories;
using Depiction.CoreModel.ExtensionMethods;
using Depiction.CoreModel.WebService;
using Depiction.ViewModels.ViewModelHelpers;
using Depiction.ViewModels.ViewModels.MapViewModels;

namespace Depiction.ViewModels.ViewModels.QuickstartViewModels
{
    public class QuickstartDataDialogVM : DialogViewModelBase
    {//There needs to be a general model for this.
        private int quickstartCount = 0;
        private DelegateCommand<IList> acceptQuickstartData;

        public int QuickStartCount { get { return quickstartCount; } }
        public List<IQuickstartItem> QuickStartItems { get; set; }
        public List<IDepictionImporterBase> ImageByAreaImporters { get; set; }
        public CollectionViewSource QuickstartViewSource { get; private set; }

        public List<DialogViewModelBase> DialogsToOpenAfterClose { get; private set; }
        #region Commands
        //Used in add content dialog, kind of a hack
        public ICommand AcceptQuickstartDataCommand
        {
            get
            {
                if (acceptQuickstartData == null)
                {
                    acceptQuickstartData = new DelegateCommand<IList>(LoadQSData);
                }
                return acceptQuickstartData;
            }
        }

        #endregion

        #region static helpers

        static public void LoadSelectedQuickstartSources(IList objectList)
        {
            if (objectList == null) return;
            var selectedQuickstartList = objectList.Cast<QuickstartViewModel>();
            if (selectedQuickstartList == null) return;
            IDepictionStoryExtensionMethods.createRevealerCount = 0;
            var showWMSDialog = true;
            var elementTypeKey = string.Empty;
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    showWMSDialog = false;
                    elementTypeKey = "elementType";//TODO the keys 
                    break;
            }
            //#if PREP
            //            var showWMSDialog = false;
            //            var elementTypeKey = "elementType";//TODO the keys 
            //#endif
            var region = DepictionAccess.CurrentDepiction.RegionBounds;
            foreach (var quickStartData in selectedQuickstartList)
            {
                IDepictionImporterBase elementImporter = null;
                var tiler = AddinRepository.Instance.Tilers.FirstOrDefault(t => t.LegacyImporterName.Equals(quickStartData.QuickstartModel.AddinName));
                if (tiler != null)
                {
                    elementImporter = new TiledElementImporter(tiler);
                    //elementImporter.ImportElements(null, "Depiction.Plugin.Image", region, parame);
                }
                
                if (elementImporter == null)
                    elementImporter = AddinRepository.Instance.GetElementImporterByName(quickStartData.QuickstartModel.AddinName);


                if (elementImporter == null) continue;
                var parameters = quickStartData.QuickstartModel.Parameters;
                if (!parameters.ContainsKey("name"))
                    parameters.Add("name", quickStartData.QuickstartModel.Name);
                if (!parameters.ContainsKey("description"))
                    parameters.Add("description", quickStartData.QuickstartModel.Description);
                if (!parameters.ContainsKey("Tag"))
                    parameters.Add("Tag", Tags.DefaultQuickStartTag + quickStartData.Name);
                
                var defaultParams = quickStartData.QuickstartModel.Parameters;
#if DEBUG
                var urlString = "url";
                if (defaultParams.ContainsKey(urlString))
                {
                    var url = defaultParams[urlString];
                    if (url.Contains("portal.depiction.com"))
                    {
                        var modUrl = url.Replace("portal.depiction.com", "geoserver.depiction.local");
                        defaultParams[urlString] = modUrl;
                    }
                }
#endif
                elementImporter.ImportElements(null, "", region, defaultParams);

                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Prep:
                        if (defaultParams.ContainsKey(elementTypeKey))
                        {
                            showWMSDialog = defaultParams[elementTypeKey].ToLowerInvariant().Contains("image");
                        }
                        break;
                }
            }

            switch (DepictionAccess.ProductInformation.ProductType)
            {
                case ProductInformationBase.Prep:
                    if (showWMSDialog)
                    {
                        DepictionAccess.NotificationService.DisplayInquiryDialog(
                            "One or more of your Quickstart sources is an image that will be loaded into a movable, sizable revealer. If the source has no data in your area, then these images will come in empty and can be deleted.",
                            "WMS Importing", MessageBoxButton.OK);
                    }
                    break;
            }
        }

        static public void RefreshQuickStartViewModelItems(QuickstartDataDialogVM quickstartVM, List<IQuickstartItem> modelItems)
        {
            List<QuickstartViewModel> quickStartDataItems = new List<QuickstartViewModel>();
            foreach (var qsItem in modelItems)
            {
                quickStartDataItems.Add(new QuickstartViewModel { QuickstartModel = qsItem });
            }
            quickstartVM.SetQuickStartSources(quickStartDataItems);
        }
        #endregion

        #region Constructor

        public QuickstartDataDialogVM()
        {
            ImageByAreaImporters = new List<IDepictionImporterBase>();
            DialogsToOpenAfterClose = new List<DialogViewModelBase>();
            IsDialogVisible = false;
        }

        #endregion

        #region helper methods

        private void LoadQSData(IList objectList)
        {
            LoadSelectedQuickstartSources(objectList);
            IsDialogVisible = false;
        }


        public void SetQuickStartSources(List<QuickstartViewModel> quickstartItems)
        {
            QuickstartViewSource = new CollectionViewSource();
            QuickstartViewSource.Source = quickstartItems;
            QuickstartViewSource.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            QuickstartViewSource.View.Refresh();

            quickstartCount = quickstartItems.Count;
            NotifyPropertyChanged("QuickstartViewSource");
            NotifyPropertyChanged("QuickStartCount");
        }
        public List<IQuickstartItem> GetAvailableQuickstartItemsForRegion(IMapCoordinateBounds regionBounds)
        {
            var quickstartItems = new List<IQuickstartItem>();
            //Get stuff from depiction servers, for now these are more important than the ones
            //that are generated at addinrepo compose time
            var currentRegionCodes = WebServiceFacade.GetRegionCodesForRegion(regionBounds);
            //Presumably this picks up the addins the none default be created by depiction so their name is
            //set correctly in the portal
            var webQuickStartSources = WebServiceFacade.GetPortalQuickstartSourcesByRegionCodes(currentRegionCodes);
            string importerName = "ImporterName";
            var availableElementNames = DepictionAccess.ElementLibrary.AllDepictionDefinitions;
            var elementTypeStringKey = "elementType";
            if (webQuickStartSources != null)
            {
                foreach (var item in webQuickStartSources)
                {
                    //The parameter keys are picked up from the portal. ie the portal announces what kind of addin
                    //while use its info.
                    if (item.Parameters.Keys != null)
                    {
                        try
                        {
                            if (item.Parameters.ContainsKey(importerName))
                                item.AddinName = item.Parameters[importerName];

                            var tiler = AddinRepository.Instance.Tilers.FirstOrDefault(t => t.LegacyImporterName.Equals(item.AddinName));
                            if (tiler != null)
                            {
                                var tileImporter = new TiledElementImporter(tiler);
                                ImageByAreaImporters.Add(tileImporter);
                                quickstartItems.Add(item);
                                continue;
                            }
                            //gets from all importers including non default addons
                            var elementImporter = AddinRepository.Instance.GetElementImporterByName(item.AddinName);
                            if (elementImporter == null) continue;
                            //Temp hack for adding high res images from context menu
                            //Alas i have forgotten what this does
                            ImageByAreaImporters.Add(elementImporter);
                            //Make sure there are no duplicates
                            var addinName = item.AddinName;
                            var name = item.Name;
                            if (quickstartItems.Count(t => t.AddinName.Equals(addinName) && t.Name.Equals(name)) != 0) continue;
                            //Make sure the elements that the extension requires are available.
                            if (item.Parameters.ContainsKey(elementTypeStringKey))
                            {
                                var completeElementType = item.Parameters[elementTypeStringKey];
                                if(availableElementNames.Any(t => t.ElementType.Contains(completeElementType)))
                                {
                                    quickstartItems.Add(item);
                                }else
                                {
                                    //Hacks to make non depiction elements work with the hard coded quickstart
                                    var typeSplit = completeElementType.Split('.');
                                    var simpleType = typeSplit[typeSplit.Length - 1];
                                    var matchingElements =
                                        availableElementNames.Where(t => t.ElementType.Contains(simpleType)).ToList();
                                    if (matchingElements.Any())
                                    {
                                        item.Parameters[elementTypeStringKey] = matchingElements[0].ElementType;
                                        quickstartItems.Add(item);
                                    }
                                }
                              
                            }
                            else
                            {
                                quickstartItems.Add(item);
                            }
                        }
                        catch (Exception ex)
                        {
                            DepictionExceptionHandler.HandleException(ex, false, true);
                        }
                    }
                }
            }

            //what a pain, mapquest is not in portal so it needs to be added seperately
            //because the addon system is a mess im cheating and just finding mapquest via tiler system or not
            //going brute force style
            foreach (var defaultimporter in AddinRepository.Instance.DefaultImporters)
            {
                var key = defaultimporter.Key;
                if (!key.ImporterSources.Contains(InformationSource.Web)) continue;
                var qsItem = new QuickstartItem
                {
                    AddinName = key.Name,
                    ElementType = "",
                    Name = key.DisplayName,
                    Parameters = new Dictionary<string, string>(),
                    Description = key.Description,
                };
                
                var addinName = qsItem.AddinName;
                var name = qsItem.Name;
                if (quickstartItems.Count(t => t.AddinName.Equals(addinName) && t.Name.Equals(name)) != 0) continue;
                quickstartItems.Add(qsItem);
            }
            //OK now we need to get the non default addins that are not found in the portal but are still valid for
            //the region codes and quickstart

            foreach (var nonDefaultImporter in AddinRepository.Instance.NonDefaultImporters)
            {
                var metadata = nonDefaultImporter.Key;
                IQuickstartItem qsItem = null;
                //First check if it is a quickstart
                if (metadata.ImporterSources.Contains(InformationSource.Web))
                {
                    //now check if region matches
                    if (metadata.ValidRegions.Length == 0)//No specification == global
                    {
                        qsItem = GetQSItemFromNonDefaultImporterMetadata(metadata);
                    }
                    else
                    {
                        if (currentRegionCodes != null)
                        {
                            var intersect = metadata.ValidRegions.Intersect(currentRegionCodes);
                            if (intersect.Count() != 0)
                            {
                                qsItem = GetQSItemFromNonDefaultImporterMetadata(metadata);
                            }
                        }
                    }
                }
                if (qsItem == null) continue;
                var addinName = qsItem.AddinName;
                var name = qsItem.Name;
                if (quickstartItems.Where(t => t.AddinName.Equals(addinName) && t.Name.Equals(name)).Count() != 0) continue;
                quickstartItems.Add(qsItem);
            }
            //Update all references to quickstartitems, basically if ?? not sure what the update was supposed to do
            //AddinRepository.Instance.FileQuickstartItems = updatedQuickStart.ToArray();
            return quickstartItems;
        }
        //Somewhat legacy so i don't entirely know why the parameters are useful in this case
        private IQuickstartItem GetQSItemFromNonDefaultImporterMetadata(IDepictionExtensionIOMetadata metaData)
        {

            var item = new QuickstartItem
            {
                AddinName = metaData.Name,
                ElementType = "",
                Name = metaData.DisplayName,
                Parameters = new Dictionary<string, string>(),
                Description = metaData.Description,
            };
            return item;

        }
        #endregion

        #region Overrides

        protected override void ToggleDialogVisibility(bool? obj)
        {
            if (obj == false && DialogsToOpenAfterClose != null)
            {
                foreach (var dialogVM in DialogsToOpenAfterClose)
                {
                    dialogVM.IsDialogVisible = true;
                }
            }
            base.ToggleDialogVisibility(obj);
        }

        #endregion

        #region helpers for setting qs data in the background

        public void UpdateQSStuffInBackground(DepictionMapAndMenuViewModel newViewModel)
        {
            BackgroundWorker quickstartWorker = new BackgroundWorker();
            quickstartWorker.RunWorkerCompleted += quickstartWorker_RunWorkerCompleted;
            quickstartWorker.DoWork += quickstartWorker_DoWork;
            quickstartWorker.RunWorkerAsync(newViewModel);
        }

        private void quickstartWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var newViewModel = e.Argument as DepictionMapAndMenuViewModel;
            if (newViewModel == null) return;
            var region = newViewModel.WorldModel.RegionBounds;
            var items = GetAvailableQuickstartItemsForRegion(region);
            var uniqueQSFromFile = new List<IQuickstartItem>();
            if (AddinRepository.Instance.FileQuickstartItems != null)
            {
                var fileQSItems = AddinRepository.Instance.FileQuickstartItems;
                uniqueQSFromFile = fileQSItems.Except(items, new QuickStartItemNameTypeComparer<IQuickstartItem>()).ToList();
            }
            QuickStartItems = items.Concat(uniqueQSFromFile).ToList();
            e.Result = newViewModel;
        }

        private void quickstartWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var newViewModel = e.Result as DepictionMapAndMenuViewModel;
            if (newViewModel == null) return;
            RefreshQuickStartViewModelItems(this, QuickStartItems);
            newViewModel.AddContentDialogViewModel.SetQuickStartListVMs(newViewModel.QuickStartDialogViewModel);
            switch (DepictionAccess.ProductInformation.ProductType)
            {
                //TODO race condition with user click multiple roadnetworks get loaded if user selects the region too fast
                case ProductInformationBase.Prep:
                    var name = DepictionAccess.CurrentDepiction.DepictionsFileName;
                    if (!string.IsNullOrEmpty(name)) return;
                    var region = DepictionAccess.CurrentDepiction.RegionBounds;
                    if (!region.IsValid) return;
                    var allQSSources = newViewModel.AddContentDialogViewModel.QuickStartItems.Source as IEnumerable<QuickstartViewModel>;
                    if (allQSSources == null) return;
                    var qsToLoad = new List<QuickstartViewModel>();
                    Debug.WriteLine("getting things from qs");
                    foreach (var qs in allQSSources)
                    {
                        if (qs.QuickstartModel.AddinName.Equals("OpenStreetMapRoadNetworkImporter") ||
                            qs.QuickstartModel.Name.Equals("Elevation (NED 30m)"))
                        {
                            qsToLoad.Add(qs);
                        }
                    }
                    LoadSelectedQuickstartSources(qsToLoad);
                    break;
            }
        }

        #endregion
    }
}