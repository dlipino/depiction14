﻿using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class DepictionAppHelpDialogVM : DialogViewModelBase
    {
        public DepictionAppHelpDialogVM()
        {
           
            switch (DepictionAccess.ProductInformation.ProductType)
            {

                case ProductInformationBase.OsintInformation:
                    DialogDisplayName = string.Format("{0} help", DepictionAccess.ProductInformation.ProductName);
                    break;
                default:
                    DialogDisplayName = "Depiction help";
                    break;
            }
        }
    }
}