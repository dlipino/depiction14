﻿using Depiction.API.MVVM;
using Depiction.CoreModel.DepictionPersistence;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class SaveLoadDialogVM : DialogViewModelBase
    {
//        private DepictionSaveLoadManager modelBase;

        public int Percentage { get; set; }
        public string SaveLoadMessage { get; set; }
        public SaveLoadDialogVM(DepictionSaveLoadManager manager)
        {
//            modelBase = manager;
//            modelBase.SaveLoadProgressChanged += modelBase_SaveLoadProgressChanged;
            IsTemporaryDialog = true;
            SaveLoadMessage = "Starting to load or save";
            Percentage = 0;

        }

        //This turns out to be fairly painful and thus doesn't actually update what we need a lot of the time
//        void modelBase_SaveLoadProgressChanged(object sender, ProgressChangedEventArgs e)
//        {
//            if(e.UserState != null)
//            {
//                SaveLoadMessage = e.UserState.ToString();
////                NotifyPropertyChanged("SaveLoadMessage");
//            }
//            if(e.ProgressPercentage != -1)
//            {
//                Percentage = e.ProgressPercentage;
////                NotifyPropertyChanged("Percentage");
//            }
//        }
        protected override void OnDispose()
        {
//            modelBase.SaveLoadProgressChanged -= modelBase_SaveLoadProgressChanged;
            base.OnDispose();
        }
    }
}
