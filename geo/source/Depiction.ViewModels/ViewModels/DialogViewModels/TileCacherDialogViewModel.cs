﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces;
using Depiction.API.MVVM;
using Depiction.API.TileServiceHelpers;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class TileCacherDialogViewModel : DialogViewModelBase
    {
        private readonly ITiler tiler;
        private List<TileModel> tiles;

        public TileCacherDialogViewModel(ITiler tiler)
        {
            
            this.tiler = tiler;
            ZoomLevels = new List<int>();
            for (int i = 1; i <= 18; i++)
                ZoomLevels.Add(i);
            SelectedZoomLevel = 18;
            CalculateTileCount();
        }

        private int selectedZoomLevel;
        public int SelectedZoomLevel
        {
            get { return selectedZoomLevel; }
            set 
            { 
                selectedZoomLevel = value;
                CalculateTileCount();
            }
        }

        private void CalculateTileCount()
        {
            tiles = new List<TileModel>();
            for (int i = 1; i < selectedZoomLevel; i++)
            {
                tiles.AddRange(tiler.GetTiles(DepictionAccess.CurrentDepiction.RegionBounds, i));
            }
            TileCount = string.Format("{0} tiles",tiles.Count);
            NotifyPropertyChanged("TileCount");
        }

        public string TileCount { get; set; }
        //private int tileCount;
        //public int TileCount
        //{
        //    get { 
        //        int count = 0;

        //        tiles = new List<TileModel>();
        //        for (int i = 1; i < selectedZoomLevel; i++)
        //        {
        //            tiles.AddRange(tiler.GetTiles(DepictionAccess.CurrentDepiction.RegionBounds, i));

        //        }

        //        return count;
        //    }
        //    set
        //    {
                
        //    }
        //}

        public List<int> ZoomLevels { get; set; }
        private DelegateCommand gatherTilesCommand;
        public ICommand GatherTilesCommand
        {
            get
            {
                if (gatherTilesCommand == null)
                    gatherTilesCommand = new DelegateCommand(GatherTiles);
                return gatherTilesCommand;
            }
        }

        private void GatherTiles()
        {
            var tileCacher = new TileCacherThing(tiler, tiles);
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(tileCacher);
            tileCacher.StartBackgroundService(null);
        }
    }


    public class TileCacherThing : BaseDepictionBackgroundThreadOperation
    {

        private int maxThreads = 50;
        private int runningThreads = 0;
        private object runningThreadsLock = new object();


        int tilesGathered = 0;
        object tilesGatheredLock = new object();
        private readonly ITiler _tiler;
        private readonly List<TileModel> tiles;
        private readonly int maxZoomLevel;


        private TileCacheService cacheService;
        public override string ServiceName
        {
            get { return "TileCacherThing"; }
        }

        public override bool IsServiceRefreshable
        {
            get { return false; }
        }

        public TileCacherThing(ITiler tiler, List<TileModel> tiles)
        {
            _tiler = tiler;
            this.tiles = tiles;
            this.maxZoomLevel = maxZoomLevel;
        }

        protected override bool ServiceSetup(Dictionary<string, object> args)
        {
            return true;
        }

        protected override object ServiceAction(object args)
        {
            bool multiThread = true;


            cacheService = new TileCacheService(_tiler.SourceName);

            if (multiThread)
            {
                double[] intervals = new double[maxThreads];
                for (int i = 0; i < maxThreads; i++)
                    intervals[i] = (1.0 / maxThreads) * i;

                foreach (var interval in intervals)
                {
                    BackgroundWorker worker = new BackgroundWorker();
                    List<TileModel> tilesSubset = tiles.GetRange((int)(tiles.Count * interval), (int)(tiles.Count * 1.0 / maxThreads));
                    worker.DoWork += worker_DoWork;
                    worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
                    worker.RunWorkerAsync(tilesSubset);
                }

                while (runningThreads > 0)
                {
                    Thread.Sleep(100);

                    double percent = ((double)(tilesGathered + 1) / (tiles.Count + 1)) * 100;
                    UpdateStatusReport(string.Format("Downloading tile {0} of {1} ({2}%)", tilesGathered + 1, tiles.Count + 1, percent.ToString("F2")));
                }
            }
            else
            {


                for (int i = 0; i < tiles.Count; i++)
                {
                    if (ServiceStopRequested)
                        break;
                    TileFetcher.DoFetchTile(_tiler, tiles[i], cacheService);
                    double percent = ((double)(i + 1) / (tiles.Count + 1)) * 100;
                    UpdateStatusReport(string.Format("Downloading tile {0} of {1} ({2}%)", i + 1, tiles.Count + 1, percent.ToString("F2")));
                }
            }
            return null;

        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            lock (runningThreadsLock)
            {
                runningThreads--;
            }
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {

            lock (runningThreadsLock)
            {
                runningThreads++;
            }
            List<TileModel> tiles = e.Argument as List<TileModel>;
            if (tiles == null) return;

            foreach (var tile in tiles)
            {
                TileFetcher.DoFetchTile(_tiler, tile, cacheService);

                lock (tilesGatheredLock)
                {
                    tilesGathered++;
                }
            }
        }

        protected override void ServiceComplete(object args)
        {

        }
    }
}