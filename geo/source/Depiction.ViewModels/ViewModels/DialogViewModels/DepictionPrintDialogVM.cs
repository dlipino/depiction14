﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class DepictionPrintDialogVM : DialogViewModelBase
    {
        public DepictionPrintDialogVM()
        {

            DialogDisplayName = "Depiction print";
        }
    }
}
