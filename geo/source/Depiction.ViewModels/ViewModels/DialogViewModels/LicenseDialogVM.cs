﻿using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.Interfaces;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class LicenseDialogVM : DialogViewModelBase
    {
        #region Varialbles

        private DelegateCommand deactivateCommand;
        private ILicenseService currentLicenseService;
        private string license = string.Empty;

        #endregion

        #region Properteis

        public string LicenseButtonText
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return "Purchase Depiction";
                    case ProductInformationBase.OsintInformation:
                        return string.Format("Deactivate {0}",DepictionAccess.ProductInformation.ProductName);
                    default:
                        return "Deactivate Depiction";
                }
            }
        }

        public ICommand DeactivateDepictionCommand
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return DepictionAppViewModel.PurchaseDepictionCommand;
                }
                if(deactivateCommand == null)
                {
                    deactivateCommand = new DelegateCommand(DeactivateLicense);
                    deactivateCommand.Text = string.Format("Deactivate {0}", DepictionAccess.ProductInformation.ProductName);
                }
                return deactivateCommand;
            }
        }

       
        public string LicenseKey
        {
            get
            {
                switch (DepictionAccess.ProductInformation.ProductType)
                {
                    case ProductInformationBase.Reader:
                        return "Depiction Reader";
                }
                if (currentLicenseService == null) return "No License";
                if(string.IsNullOrEmpty(license))
                {
                    license = currentLicenseService.SerialNumber;
                }
                return license;
            }
        }

        #endregion

        #region Constructor
        public LicenseDialogVM(ILicenseService licenseService)
        {
            DialogDisplayName = string.Format("{0} license", DepictionAccess.ProductInformation.ProductName); 
            currentLicenseService = licenseService;
        }
        #endregion

        private void DeactivateLicense()
        {
            if (currentLicenseService == null) return;
            currentLicenseService.DeactivateLicense();
        }
    }
}