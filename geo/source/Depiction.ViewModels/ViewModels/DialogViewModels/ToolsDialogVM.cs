﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class ToolsDialogVM : DialogViewModelBase
    {
        public ToolsDialogVM()
        {
            DialogDisplayName = "Tools menu";
        }
    }
}