﻿using Depiction.API.MVVM;

namespace Depiction.ViewModels.ViewModels.DialogViewModels
{
    public class UpdateDialogVM : DialogViewModelBase
    {
        public UpdateDialogVM()
        {

            DialogDisplayName = "Depiction update";
        }
    }
}