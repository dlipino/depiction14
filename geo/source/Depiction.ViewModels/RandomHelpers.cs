﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using Depiction.API.HelperObjects;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.Service;
using Depiction.CoreModel.DepictionObjects.Elements;

namespace Depiction.ViewModels
{
    public class RandomHelpers
    {
        static public List<EnhancedPointListWithChildren> CreateAZOI(Point position)
        {
            var zoiPoints = new List<EnhancedPointListWithChildren>();
            var points = new EnhancedPointListWithChildren();
            points.IsClosed = true;
            points.IsFilled = true;
            var x = position.X;
            var y = position.Y;
            points.Outline.Add(position);
            points.Outline.Add(new Point(x + 50, y));
            points.Outline.Add(new Point(x + 50, y + 50));
            points.Outline.Add(new Point(x, y + 50));
            //            points.Outline.Add(position);

            var hole = new EnhancedPointList();
            hole.IsClosed = true;
            hole.IsFilled = true;
            x += 20;
            y += 20;
            hole.Outline.Add(new Point(x, y));
            hole.Outline.Add(new Point(x + 10, y));
            hole.Outline.Add(new Point(x + 10, y + 10));
            hole.Outline.Add(new Point(x, y + 10));
            //            hole.Outline.Add(new Point(x,y));
            points.Children.Add(hole);
            zoiPoints.Add(points);
            return zoiPoints;
        }

        private static IDepictionElement element;
        static public void AddImageToElementModelInBackground(IDepictionElement inElement, string fileName)
        {
            element = inElement;
            BackgroundWorker worker = new BackgroundWorker();

            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync(fileName);
        }

        static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                var partialName = e.Result.ToString();
                var metadata = new DepictionImageMetadata("file:" + partialName, "File");
                element.SetPropertyValue("DisplayName", partialName);
                element.SetImageMetadata(metadata);
//                element.ImageMetadata = metadata;
            }
        }

        static void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                var fileName = e.Argument.ToString();
                if (!File.Exists(fileName)) return;

//                var partialName = Path.GetFileName(fileName);
                string partialName;
                var imageSource = BitmapSaveLoadService.LoadImageAndStoreInDepictionImageResources(fileName, out partialName);
                if (Application.Current != null)
                {
                    var currentApp = Application.Current as IDepictionApplication;
                    if (currentApp != null)
                    {
                        var depiction = currentApp.CurrentDepiction;
                        if (!depiction.ImageResources.Contains(partialName))
                        {
                            depiction.ImageResources.Add(partialName, imageSource);
                        }
                    }
                    e.Result = partialName;
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}