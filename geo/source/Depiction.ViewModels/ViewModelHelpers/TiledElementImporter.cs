﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Depiction.API;
using Depiction.API.AddinObjects.AbstractObjects;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.ExtensionMethods;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.Properties;
using Depiction.API.TileServiceHelpers;
using Depiction.API.ValueTypes;
using Depiction.APIUnmanaged.Service;

namespace Depiction.ViewModels.ViewModelHelpers
{
    /// <summary>
    /// An abstract class containing some common methods in a tiling service.
    /// </summary>
    public sealed class TiledElementImporter : AbstractDepictionDefaultImporter
    {
        #region fields
        private readonly ITiler tiler;
        public event Action<TileModel> BackgroundTileReceived;
        public event Action<string> TileServiceInformation;


        private bool cancelled;
        #endregion

        #region abstract properties

        //protected abstract double PixelsPerTile { get { return tiler.PixelWidth; } }

        #endregion

        #region properties

        //public abstract TileImageTypes TileImageType { get; }
        //public Size TotalSize { get; private set; }
        //public bool ReplaceCachedImages { get { return Settings.Default.ReplaceCachedFiles; } }
        //public bool IsCanceled { get { return cancelled; } }

        #endregion

        #region constructor
        public TiledElementImporter(ITiler tiler)
        {
            this.tiler = tiler;
        }

        #endregion

        #region abstract methods

        //protected abstract int LongitudeToColAtZoom(ILatitudeLongitude latLong, int zoom);
        //protected abstract int LatitudeToRowAtZoom(ILatitudeLongitude latLong, int zoom);
        //protected abstract double TileColToTopLeftLong(int col, int zoomLevel);
        //protected abstract double TileRowToTopLeftLat(int row, int zoom);
        //protected abstract TileModel GetTile(int row, int column, int zoom);

        #endregion

        #region methods

        //protected static double RadToDeg(double d)
        //{
        //    return d * 180 / Math.PI;
        //}

        //protected static double DegToRad(double d)
        //{
        //    return d * Math.PI / 180.0;
        //}

        //protected abstract int GetZoomLevel(IMapCoordinateBounds boundingBox);

        public string CreateTileFileName(IMapCoordinateBounds bounds)
        {
            var fileName = tiler.SourceName;
            var boundString = string.Format("_{0:0.##}_{1:0.##}_to_{2:0.##}_{3:0.##}", bounds.Top, bounds.Left, bounds.Bottom, bounds.Right);
            fileName += boundString;
            return fileName + ".jpg";
        }

        ////<summary>
        ////Returns the list of tiles to get ordered in increasing distance from the center
        ////of the area being tiled.
        ////</summary>
        private IEnumerable<TileXY> TilesToGet(UTMLocation utmLocation, int lastCol, int col, int lastRow, int row)
        {
            if (lastCol < col)
            {
                var temp = lastCol;
                lastCol = col;
                col = temp;
            }
            if (lastRow < row)
            {
                var temp = lastRow;
                lastRow = row;
                row = temp;
            }
            var tilesToGet = new List<KeyValuePair<int, TileXY>>();
            int centerRow = row + (lastRow - row) / 2;
            int centerCol = col + (lastCol - col) / 2;

            for (int j = row; j <= lastRow; j++)
            {
                for (int i = col; i <= lastCol; i++)
                {
                    var distance = (j - centerRow) * (j - centerRow) + (i - centerCol) * (i - centerCol);
                    tilesToGet.Add(new KeyValuePair<int, TileXY>(distance, new TileXY { Column = i, Row = j, UTMZoneNumber = utmLocation.UTMZoneNumber}));
                }
            }

            IEnumerable<TileXY> sortedTiles =
                  from pair in tilesToGet
                  orderby pair.Key ascending, pair.Value.Column ascending, pair.Value.Row ascending
                  select pair.Value;

            return sortedTiles;
        }
        #endregion

        #region events
        
        public int PixelWidth
        {
            get { return tiler.PixelWidth; }
        }

        public string DisplayName
        {
            get { return tiler.DisplayName; }
        }
        
        public string CreateImageFromTilesInBounds(IMapCoordinateBounds boundingBox, int minTilesAcross, int maxZoomLevel)
        {
            if (tiler is IUTMTiler)
            {
                if (boundingBox.TopLeft.ConvertToUTM().UTMZoneNumber != boundingBox.TopRight.ConvertToUTM().UTMZoneNumber)
                    return null;                
            }
            //const int maxZoomLevel = 17;
            //cancelled = false;

            int zoomLevel = tiler.GetZoomLevel(boundingBox, minTilesAcross, maxZoomLevel);
            int row = tiler.LatitudeToRowAtZoom(boundingBox.TopRight, zoomLevel);
            int col = tiler.LongitudeToColAtZoom(boundingBox.BottomLeft, zoomLevel);
            int lastRow = tiler.LatitudeToRowAtZoom(boundingBox.BottomLeft, zoomLevel);
            int lastCol = tiler.LongitudeToColAtZoom(boundingBox.TopRight, zoomLevel);


            var tilesToGet = TilesToGet(boundingBox.TopLeft.ConvertToUTM(), lastCol, col, lastRow, row);
            var tilesRetrieved = new List<TileModel>();
            int count = 0;
            int total = tilesToGet.Count();
            //if (tiler is IUTMTiler)
            //{
                
            //}
            //else
            //{
            //    //double widthInDegrees = tiler.TileColToTopLeftLong(col + 1, zoomLevel) - tiler.TileColToTopLeftLong(col, zoomLevel);
            //}
            foreach (var tileToGet in tilesToGet)
            {
                if (TileServiceInformation != null)
                {
                    TileServiceInformation(string.Format("{0:0%} ", count / (double)total));
                }
                //double heightInDegrees = tiler.TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - tiler.TileRowToTopLeftLat(tileToGet.Row + 1, zoomLevel);

                //var uri = new Uri(tiler.GetTileUrl(tileToGet.Row, tileToGet.Column, zoomLevel));
                var tileModel = tiler.GetTileModel(tileToGet, zoomLevel);
                //var tileModel = new TileModel(zoomLevel, tileToGet.Row, tileToGet.Column,
                //                uri,
                //                new LatitudeLongitude(tiler.TileRowToTopLeftLat(tileToGet.Row, zoomLevel), tiler.TileColToTopLeftLong(tileToGet.Column, zoomLevel)),
                //                new LatitudeLongitude(tiler.TileRowToTopLeftLat(tileToGet.Row, zoomLevel) - heightInDegrees, tiler.TileColToTopLeftLong(tileToGet.Column, zoomLevel) + widthInDegrees));
                TileCacheService cacheService = new TileCacheService(tiler.SourceName);
                TileFetcher.DoFetchTile(tiler, tileModel, cacheService);
                if (tileModel.Image != null)
                {
                    tilesRetrieved.Add(tileModel);
                }
                count++;
                //if (!cancelled) continue;
                //return null;
            }
            var totalSize = new Size(((lastCol + 1) - col) * PixelWidth,
                                     Math.Abs((lastRow + 1) - row) * PixelWidth);
            var image = TileImageFileBuilder(boundingBox, tilesRetrieved, totalSize);
            //var image = TiledImageBuilderService.GetTiledImage(boundingBox, tilesRetrieved, totalSize);
            //cancelled = false;
            return image;
        }

        private string TileImageFileBuilder(IMapCoordinateBounds boundingBox, List<TileModel> tiles,
                                                       Size totalSize)
        {
            var imagePath = TiledImageBuilderService.GetTiledImage(boundingBox, tiles, totalSize, CreateTileFileName(boundingBox));
            return imagePath;
        }

        public void CancelRequest()
        {
            cancelled = true;
        }

        #endregion


        override public void ImportElements(object elementLocation, string defaultElementType, IMapCoordinateBounds depictionRegion, Dictionary<string, string> parameters)
        {
            var imageAddingService = new TileProviderSaveFileBackgroundService(this, depictionRegion);
            DepictionAccess.BackgroundServiceManager.AddBackgroundService(imageAddingService);
            var dict = new Dictionary<string, object>();
            foreach (var key in parameters.Keys)
            {
                dict.Add(key, parameters[key]);
            }
            imageAddingService.StartBackgroundService(dict);
        }
    }
}