using System.Windows;
using System.Windows.Media;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;

namespace Depiction.ViewModels.MapConverters
{
    public class ScreenToMapPixelConverter : IGeoConverter
    {
        private IGeoConverter pixelConverter;
        private readonly Transform transform;

        #region Constructor
        public ScreenToMapPixelConverter(IGeoConverter pConverter, Transform tForm)
        {
            transform = tForm;
            pixelConverter = pConverter;
        }
        #endregion
        public void UpdatePixelConverter(IGeoConverter newPixelConverter)
        {
            pixelConverter = newPixelConverter;
        }

        public Point WorldToGeoCanvas(ILatitudeLongitude latLon)
        {
            Point pnt = pixelConverter.WorldToGeoCanvas(latLon);
            return transform.Transform(pnt);
        }

        public Rect WorldToGeoCanvas(IMapCoordinateBounds mapBounds)
        {
            var topLeftGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Top, mapBounds.Left));
            var bottomRightGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Bottom, mapBounds.Right));
            return new Rect(topLeftGeo, bottomRightGeo);
        }

        public ILatitudeLongitude GeoCanvasToWorld(Point p)
        {
            Point trPnt = transform.Inverse.Transform(p);
            return pixelConverter.GeoCanvasToWorld(trPnt);
        }

        public IMapCoordinateBounds GeoCanvasToWorld(Rect rect)
        {
            var topLeftGeo = GeoCanvasToWorld(rect.TopLeft);
            var bottomRightGeo = GeoCanvasToWorld(rect.BottomRight);
            return new MapCoordinateBounds(topLeftGeo, bottomRightGeo);
        }

        public double Offsetx
        {
            get { return pixelConverter.Offsetx; }
        }

        public double Offsety
        {
            get { return pixelConverter.Offsety; }
        }
    }
}