using System;
using System.Windows;
using Depiction.API.Interfaces.GeoTypeInterfaces;
using Depiction.API.ValueTypes;
using Depiction.ViewModels.ViewModelInterfaces.MapConverterInterface;
using ProjNet.Converters.WellKnownText;
using ProjNet.CoordinateSystems;
using ProjNet.CoordinateSystems.Transformations;

namespace Depiction.ViewModels.MapConverters
{
    public class MercatorCoordinateConverter : IGeoConverter
    {
        private double hMetersPerPixel;
        // These get recalculated only when the extent changes
        private Point topLeftCartesian;
        private double vMetersPerPixel;
        private readonly ICoordinateTransformation transformer;

        public MercatorCoordinateConverter(IGeoAndPixelExtent extent, double offsetX, double offsetY)
        {
            const string mercatorWKT =
                "PROJCS[\"Popular Visualisation CRS / Mercator\", " +
                    "GEOGCS[\"Popular Visualisation CRS\",  " +
                        "DATUM[\"WGS84\",    " +
                            "SPHEROID[\"WGS84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7059\"]],  " +
                        "AUTHORITY[\"EPSG\",\"6055\"]], " +
                    "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]], " +
                    "UNIT[\"degree\", 0.0174532925199433, AUTHORITY[\"EPSG\", \"9102\"]], " +
                    "AXIS[\"E\", EAST], AXIS[\"N\", NORTH], AUTHORITY[\"EPSG\",\"4055\"]]," +
                "PROJECTION[\"Mercator\"]," +
                "PARAMETER[\"semi_minor\",6378137]," +
                "PARAMETER[\"False_Easting\", 0]," +
                "PARAMETER[\"False_Northing\", 0]," +
                "PARAMETER[\"Central_Meridian\", 0]," +
                "PARAMETER[\"Latitude_of_origin\", 0]," +
                "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]]," +
                "AXIS[\"East\", EAST], AXIS[\"North\", NORTH]," +
                "AUTHORITY[\"EPSG\",\"3785\"]]";


            IProjectedCoordinateSystem utmCS = CoordinateSystemWktReader.Parse(mercatorWKT) as IProjectedCoordinateSystem;

            const string geoWKT =   
                "GEOGCS[\"GCS_WGS_1984\"," +
                    "DATUM[\"D_WGS_1984\",SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
                    "PRIMEM[\"Greenwich\",0]," +
                    "UNIT[\"Degree\",0.0174532925199433]" +
                "]";
            IGeographicCoordinateSystem geoCS = CoordinateSystemWktReader.Parse(geoWKT) as IGeographicCoordinateSystem;

            CoordinateTransformationFactory ctfac = new CoordinateTransformationFactory();

            //// MORE THAN MEETS THE EYE!!
            transformer = ctfac.CreateFromCoordinateSystems(geoCS, utmCS);

            InitializeExtent(extent);

            Offsetx = offsetX;
            Offsety = offsetY;
        }

        #region IGeoConverter Members

        public Point WorldToGeoCanvas(ILatitudeLongitude latLon)
        {
            double[] pointCartesian = SphericalMercator.FromLonLat(latLon.Longitude, latLon.Latitude);// transformer.MathTransform.Transform(new[] { latLon.Longitude, latLon.Latitude });
            double x = (pointCartesian[0] - topLeftCartesian.X)/hMetersPerPixel;
            double y = (pointCartesian[1] - topLeftCartesian.Y)/vMetersPerPixel;
            return new Point(x - Offsetx, y - Offsety);
        }

        public Rect WorldToGeoCanvas(IMapCoordinateBounds mapBounds)
        {
            var topLeftGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Top,mapBounds.Left));
            var bottomRightGeo = WorldToGeoCanvas(new LatitudeLongitude(mapBounds.Bottom, mapBounds.Right));
            return new Rect(topLeftGeo, bottomRightGeo);
        }

        public ILatitudeLongitude GeoCanvasToWorld(Point p)
        {
            double px = p.X + Offsetx;
            double py = p.Y + Offsety;
            double x = (px*hMetersPerPixel) + topLeftCartesian.X;
            double y = (py*vMetersPerPixel) + topLeftCartesian.Y;
            double[] pointWorld = SphericalMercator.ToLonLat(x, y);// transformer.MathTransform.Inverse().Transform(new[] { x, y });
            if (pointWorld[0] < -180)
                pointWorld[0] = -180;
            else if (pointWorld[0] > 180)
                pointWorld[0] = 180;
            if (pointWorld[1] > 90)
                pointWorld[1] = 90;
            else if (pointWorld[1] < -90)
                pointWorld[1] = -90;
            return new LatitudeLongitude(pointWorld[1], pointWorld[0]);
        }

        public IMapCoordinateBounds GeoCanvasToWorld(Rect rect)
        {
            var topLeftGeo = GeoCanvasToWorld(rect.TopLeft);
            var bottomRightGeo = GeoCanvasToWorld(rect.BottomRight);
            return new MapCoordinateBounds(topLeftGeo, bottomRightGeo);
        }

        #endregion

        public double Offsetx { get; private set; }
        public double Offsety { get; private set; }

        private void InitializeExtent(IGeoAndPixelExtent extent)
        {
            var tl = transformer.MathTransform.Transform(new[] { extent.NorthWest.Longitude, extent.NorthWest.Latitude });
            topLeftCartesian = new Point(tl[0], tl[1]);
            var br = transformer.MathTransform.Transform(new[] { extent.SouthEast.Longitude, extent.SouthEast.Latitude });
            vMetersPerPixel = (br[1] - topLeftCartesian.Y) / extent.HeightInPixels;
            hMetersPerPixel = (br[0] - topLeftCartesian.X) / extent.WidthInPixels;
        }
    }


    public class SphericalMercator
    {
        private readonly static double radius = 6378137;
        private static double D2R = Math.PI / 180;
        private static double HALF_PI = Math.PI / 2;

        public static double[] FromLonLat(double lon, double lat)
        {
            double lonRadians = (D2R * lon);
            double latRadians = (D2R * lat);

            double x = radius * lonRadians;
            double y = radius * Math.Log(Math.Tan(Math.PI * 0.25 + latRadians * 0.5));

            return new [] {x, y};
        }

        public static double[] ToLonLat(double x, double y)
        {
            double ts;
            ts = Math.Exp(-y / (radius));
            double latRadians = HALF_PI - 2 * Math.Atan(ts);

            double lonRadians = x / (radius);

            double lon = (lonRadians / D2R);
            double lat = (latRadians / D2R);

            return new [] {lon, lat};
        }
    }
}