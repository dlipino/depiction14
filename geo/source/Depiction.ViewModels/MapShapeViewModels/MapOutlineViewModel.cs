using System.Collections.Generic;
using System.Windows.Media;
using Depiction.API.HelperObjects;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.MapShapeViewModels
{
    public class MapOutlineViewModel : ViewModelBase
    {
        private Geometry outlineGeometry;
        private Brush outlineBorderColor = Brushes.Olive;

        public Geometry OutlineGeometry
        {
            get { return outlineGeometry; }
            set
            {
                outlineGeometry = value;
                NotifyPropertyChanged("OutlineGeometry");
            }
        }

        public Brush OutlineBorderColor
        {
            get { return outlineBorderColor; }
            set
            {
                outlineBorderColor = value;
                if(!outlineBorderColor.IsFrozen)
                {
                    outlineBorderColor.Freeze();
                }
                NotifyPropertyChanged("OutlineBorderColor");
            }
        }
        #region Constructor

        public MapOutlineViewModel(List<EnhancedPointListWithChildren> pixelBounds)
        {
            outlineGeometry = ViewModelHelperMethods.CreateGeometryFromEnhancedPointListWithChildren(pixelBounds);
            outlineBorderColor = Brushes.Olive;
            outlineBorderColor.Freeze();
        }


        #endregion
    }
}