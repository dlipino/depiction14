using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Depiction.API;
using Depiction.API.AbstractObjects;
using Depiction.API.HelperObjects;
using Depiction.API.InteractionEngine;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.API.MVVM;
using Depiction.ViewModels.ViewModels;

namespace Depiction.ViewModels.DialogViewModels
{
    public class InteractionRepositoryDialogViewModel : DialogViewModelBase
    {
        private IInteractionRuleRepository interactionRepository;
        private IInteractionRule selectedRule;//Used with visual connection
        private IInteractionRule originalRule;
        private IInteractionRule displayedRule;
        private bool viewAllInteractionProperties;
        private IElementPrototype causeElementDefinition;
        private List<IElementPrototype> affectedElementDefinitions = new List<IElementPrototype>();
        private List<IElementPrototype> modifiedAffectedElementDefinitions = null;// new List<IElementPrototype>();

        private DelegateCommand loadInteractionFromFileCommand;
        private DelegateCommand exportInteractionToFileCommand;
        private DelegateCommand makeDuplicateOfCurrentInteractionCommand;
        private DelegateCommand updateOrSaveCurrentInteractionCommand;
        private DelegateCommand deleteCurrentInteractionCommand;
        private DelegateCommand enableAllInteractionsCommand;
        private DelegateCommand disableAllInteractionsCommand;

        public IEnumerable<IInteractionRule> AllInteractions
        {
            get
            {
                if (interactionRepository == null) return new List<IInteractionRule>();
                return interactionRepository.InteractionRules;
            }
        }
        public IEnumerable<IElementPrototype> AllElements
        {
            get
            {
                if (DepictionAccess.ElementLibrary != null)
                {
                    return DepictionAccess.ElementLibrary.AllDepictionDefinitions;
                }
                return new List<IElementPrototype>();
            }
        }
        public IElementPrototype CausingElement
        {
            get
            {
                return causeElementDefinition;
            }
            set
            {
                causeElementDefinition = value;
                var name = value.ElementType;
                //Hacky
                if (displayedRule != null && !displayedRule.IsDefaultRule)
                {
                    displayedRule.Publisher = name;
                }
            }
        }
        public IEnumerable<IElementPrototype> AffectedElements
        {
            get
            {
                if (modifiedAffectedElementDefinitions == null)
                {
                    return affectedElementDefinitions;
                }
                return modifiedAffectedElementDefinitions;
            }
        }
        //This is here because changing selectyion was wiping the original one, and I needed an intermediate to
        //catch, but not propagete the change
        public IEnumerable<IElementPrototype> ModifiedAffectedElements
        {
            get
            {
                return modifiedAffectedElementDefinitions;
            }
            set
            {
                modifiedAffectedElementDefinitions = value.ToList();
                //Hakcy
                if (displayedRule != null && !displayedRule.IsDefaultRule)
                {
                    displayedRule.Subscribers.Clear();
                    foreach (var affectedElement in modifiedAffectedElementDefinitions)
                    {
                        displayedRule.Subscribers.Add(affectedElement.ElementType);
                    }
                }
            }
        }

        public IInteractionRule DisplayedInteraction
        {
            get { return displayedRule; }
            set
            {
                displayedRule = value;
                NotifyPropertyChanged("DisplayedInteraction");
            }
        }

        public IInteractionRule SelectedInteraction
        {
            get { return selectedRule; }
            set//Most of this should be moved to a method
            {
                var previous = selectedRule;
                if (value != null && !ContinueWithPossibleChange())
                {
                    selectedRule = previous;
                }
                else
                {
                    selectedRule = value;
                    SetDisplayedInteractionWithCopy(selectedRule);
                }
                NotifyPropertyChanged("SelectedInteraction");
            }
        }

        public bool ViewAllInteractionProperties
        {
            get { return viewAllInteractionProperties; }
            set
            {
                viewAllInteractionProperties = value;
                NotifyPropertyChanged("ViewAllInteractionProperties");
            }
        }

        #region Command properties
        public ICommand EnableAllInteractionsCommand
        {
            get
            {
                if (enableAllInteractionsCommand == null)
                {
                    enableAllInteractionsCommand = new DelegateCommand(EnableAllInteractions) { Text = "Enable all interactions" };
                }
                return enableAllInteractionsCommand;
            }
        }
        public ICommand DisableAllInteractionsCommand
        {
            get
            {
                if (disableAllInteractionsCommand == null)
                {
                    disableAllInteractionsCommand = new DelegateCommand(DisableAllInteractions) { Text = "Disable all interactions" };
                }
                return disableAllInteractionsCommand;
            }
        }
        public ICommand LoadInteractionFromFileCommand
        {
            get
            {
                if (loadInteractionFromFileCommand == null)
                {
                    loadInteractionFromFileCommand = new DelegateCommand(FindInteractionFileToLoad) { Text = "Load interaction file" };
                }
                return loadInteractionFromFileCommand;
            }
        }
        public ICommand ExportInteractionToFileCommand
        {
            get
            {
                if (exportInteractionToFileCommand == null)
                {
                    exportInteractionToFileCommand = new DelegateCommand(ExportSelectedInteractionRule, IsInteractionSelected) { Text = "Export interaction to file" };
                }
                return exportInteractionToFileCommand;
            }
        }

        public ICommand UpdateOrSaveCurrentInteractionCommand
        {
            get
            {
                if (updateOrSaveCurrentInteractionCommand == null)
                {
                    updateOrSaveCurrentInteractionCommand = new DelegateCommand(SaveCurrentInteraction, CanSaveOrUpdateCurrentInteraction) { Text = "Update/Save current interaction" };
                }
                return updateOrSaveCurrentInteractionCommand;
            }
        }

        public ICommand DeleteCurrentInteractionCommand
        {
            get
            {
                if (deleteCurrentInteractionCommand == null)
                {
                    deleteCurrentInteractionCommand = new DelegateCommand(DeleteCurrentInteraction, CanDeleteSelectedInteraction) { Text = "Delete current interaction" };
                }
                return deleteCurrentInteractionCommand;
            }
        }


        public ICommand CopyInteractionCommand
        {
            get
            {
                if (makeDuplicateOfCurrentInteractionCommand == null)
                {
                    makeDuplicateOfCurrentInteractionCommand = new DelegateCommand(MakeDuplicateOfCurrentInteraction, CanDuplicateCurrentInteraction) { Text = "Add copy of interaction to library" };
                }
                return makeDuplicateOfCurrentInteractionCommand;
            }
        }

        #endregion

        #region Constructor

        public InteractionRepositoryDialogViewModel()
        {
            switch (DepictionAccess.ProductInformation.ProductType)
            {

                case ProductInformationBase.OsintInformation:
                    DialogDisplayName = string.Format("{0} interaction viewer", DepictionAccess.ProductInformation.ProductName);
                    break;
                default:

                    DialogDisplayName = "Depiction interaction viewer";
                    break;
            }
        }

        #endregion

        public void DisableAllInteractions()
        {
            foreach (var interaction in interactionRepository.InteractionRules)
            {
                interaction.IsDisabled = true;
            }
            NotifyPropertyChanged("AllInteractions");
        }
        public void EnableAllInteractions()
        {
            foreach (var interaction in interactionRepository.InteractionRules)
            {
                interaction.IsDisabled = false;
            }
            NotifyPropertyChanged("AllInteractions");
        }

        public void FindInteractionFileToLoad()
        {
            var userElementPath = "";
            if (DepictionAccess.PathService != null)
            {
                userElementPath = DepictionAccess.PathService.UserInteractionRulesDirectoryPath;
            }
            var interactionFileName = DepictionAppViewModel.GetFileNameToLoad(userElementPath, FileFilter.InteractionTypeFilter);
            if (string.IsNullOrEmpty(interactionFileName)) return;
            if (!File.Exists(interactionFileName)) return;

            var loadedRules = InteractionRule.LoadRulesFromXmlFile(interactionFileName);
            foreach (var rule in loadedRules)
            {
                interactionRepository.AddInteractionRule(rule, false);
            }
            NotifyForVisualChanges();
        }
        protected void ExportSelectedInteractionRule()
        {
            if (SelectedInteraction == null) return;
            var userElementPath = "";
            if (DepictionAccess.PathService != null)
            {
                userElementPath = DepictionAccess.PathService.UserInteractionRulesDirectoryPath;
            }
            var interactionFileName = DepictionAppViewModel.GetFileToSaveName(userElementPath, FileFilter.InteractionTypeFilter);
            if (string.IsNullOrEmpty(interactionFileName)) return;
            var extension = ".xml";
            var bareFileName = Path.GetFileNameWithoutExtension(interactionFileName);
            var currentExtension = Path.GetExtension(interactionFileName);
            if (string.IsNullOrEmpty(currentExtension))
            {
                interactionFileName = bareFileName + extension;
            }
            InteractionRule.SaveRulesToXmlFile(new List<IInteractionRule> { SelectedInteraction }, interactionFileName);
            DepictionAccess.NotificationService.DisplayMessageString(
                string.Format("{0} has been saved", SelectedInteraction.Name), 4);
        }

        public void SetInteractionsLibrary(IInteractionRuleRepository newInteractionRepository)
        {
            interactionRepository = newInteractionRepository;
            NotifyForVisualChanges();
        }

        private void MakeDuplicateOfCurrentInteraction()
        {
            if (!ContinueWithPossibleChange()) return;
            if (SelectedInteraction == null) return;
            var interactionCopy = SelectedInteraction.MakeFunctionalCopyOfInteractionRule();
            interactionCopy.Name = "";
            interactionCopy.Author = "";
            originalRule = interactionCopy;
            SelectedInteraction = null;
            SetDisplayedInteractionWithCopy(interactionCopy);
        }

        private bool CanDuplicateCurrentInteraction()
        {
            if (!IsInteractionSelected()) return false;
            return true;
        }

        private bool CanSaveOrUpdateCurrentInteraction()
        {
            if (DisplayedInteraction == null) return false;
            if (DisplayedInteraction.IsDefaultRule) return false;
            if (string.IsNullOrEmpty(DisplayedInteraction.Name)) return false;

            return true;
        }

        private bool IsInteractionSelected()
        {
            if (selectedRule == null) return false;
            return true;
        }

        private void SaveCurrentInteraction()
        {
            originalRule = DisplayedInteraction.DeepClone();
            IInteractionRule ruleToRemove = null;
            foreach (var interaction in interactionRepository.InteractionRules)
            {
                if (interaction.KeyEquals(originalRule))
                {
                    ruleToRemove = interaction;
                }
            }
            if (ruleToRemove != null) interactionRepository.RemoveInteractionRule(ruleToRemove);
            interactionRepository.AddInteractionRule(originalRule);

            NotifyForVisualChanges();

            SelectedInteraction = originalRule;
        }

        private bool CanDeleteSelectedInteraction()
        {
            if (!IsInteractionSelected()) return false;
            if (selectedRule.IsDefaultRule) return false;
            if (!interactionRepository.InteractionRules.Contains(SelectedInteraction)) return false;
            return true;
        }

        private void DeleteCurrentInteraction()
        {
            if (interactionRepository.InteractionRules.Contains(SelectedInteraction))
            {
                interactionRepository.RemoveInteractionRule(SelectedInteraction);
            }
            NotifyForVisualChanges();
        }

        private bool SetDisplayedInteractionWithCopy(IInteractionRule ruleToDisplay)
        {
            causeElementDefinition = null;
            modifiedAffectedElementDefinitions = null;
            affectedElementDefinitions = new List<IElementPrototype>();
            if (ruleToDisplay == null)
            {
                return false;
            }
            if (DisplayedInteraction == null || !DisplayedInteraction.Equals(ruleToDisplay))
            {
                //Race condition if the property is used
                displayedRule = ruleToDisplay.DeepClone();
                originalRule = ruleToDisplay;
            }

            SetCauseAndAffectedForRule(DisplayedInteraction);
            NotifyPropertyChanged("DisplayedInteraction");
            NotifyPropertyChanged("AllElements");
            modifiedAffectedElementDefinitions = affectedElementDefinitions;// new List<IElementPrototype>();
            return true;
        }
        private void SetCauseAndAffectedForRule(IInteractionRule rule)
        {
            try
            {
                var singleElement = AllElements.First(t => t.ElementType.Equals(rule.Publisher));
                causeElementDefinition = singleElement;
                NotifyPropertyChanged("CausingElement");
            }
            catch
            { }
            var allElements = AllElements;
            var affected = from a in allElements
                           from s in rule.Subscribers
                           where a.ElementType.Equals(s)
                           select a;

            affectedElementDefinitions = affected.ToList();
            NotifyPropertyChanged("AffectedElements");
        }

        private bool ContinueWithPossibleChange()
        {
            if (DisplayedInteraction == null) return true;
            if (string.IsNullOrEmpty(DisplayedInteraction.Name)) return true;
            if (displayedRule.RuleEquals(originalRule)) return true;

            var message =
                string.Format(
                    "You have unsaved changes in the \"{0}\" interaction.\n\nClick Yes to apply your changes.\nClick No to discard your changes.\nClick Cancel to continue editing \"{0}\".",
                    DisplayedInteraction.Name);
            var result = DepictionAccess.NotificationService.DisplayInquiryDialog(message, "Save changes",
                                                                     MessageBoxButton.YesNoCancel);
            if (result == MessageBoxResult.Yes)
            {
                //Update selected rule with limite
                //  originalRule = DisplayedInteraction.DeepClone();
                SaveCurrentInteraction();
            }
            else if (result == MessageBoxResult.No)
            {

            }
            else if (result == MessageBoxResult.Cancel)
            {
                return false;
            }
            return true;
        }

        protected void NotifyForVisualChanges()
        {
            NotifyPropertyChanged("AllInteractions");
            //            NotifyPropertyChanged("AllElements");
            //            NotifyPropertyChanged("AffectedElements");

        }
    }
}