using System.Windows.Input;
using Depiction.API;
using Depiction.API.Interfaces.DepictionStoryInterfaces;
using Depiction.API.MVVM;

namespace Depiction.ViewModels.DialogViewModels
{
    public class DepictionFileInformationDialogViewModel : DialogViewModelBase
    {
        private string author = string.Empty;
        private string title = string.Empty;
        private string description = string.Empty;

        #region properties

        public string Author{get{ return author;} set{ author = value; NotifyPropertyChanged("Author");}}
        public string Title { get { return title; } set { title = value; NotifyPropertyChanged("Title"); } }
        public string Description { get { return description; } set { description = value; NotifyPropertyChanged("Description"); } }

        protected IDepictionStoryMetadata Metadata
        {
            get
            {
                if(DepictionAccess.CurrentDepiction == null) return null;

                return DepictionAccess.CurrentDepiction.Metadata;
            }
        }

        #region Command Properties

        public ICommand ApplyChangesCommand
        {
            get
            {
                return new DelegateCommand(ApplyChangesToStory, ModificationsExist);
            }
        }
        public ICommand AcceptChangesCommand
        {
            get
            {
                return new DelegateCommand(AcceptChangesToStory);
            }
        }

        #endregion

        #endregion

        #region constructor
        
        public DepictionFileInformationDialogViewModel()
        {
            DialogDisplayName = "File information";
        }

        #endregion

        protected override void ToggleDialogVisibility(bool? obj)
        {
            base.ToggleDialogVisibility(obj);
            if (IsDialogVisible && Metadata != null)
            {
                Author = Metadata.Author;
                Title = Metadata.Title;
                Description = Metadata.Description;
            }
        }
        private void AcceptChangesToStory()
        {
            ApplyChangesToStory();
            IsDialogVisible = false;
        }


        private bool ModificationsExist()
        {
            if(Metadata == null) return false;
            if (!author.Equals(Metadata.Author)) return true;
            if (!title.Equals(Metadata.Title)) return true;
            if (!description.Equals(Metadata.Description)) return true;
            return false;
        }

        private void ApplyChangesToStory()
        {
            Metadata.Author = author;
            Metadata.Title = title;
            Metadata.Description = description;
        }
    }
}