﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ValueConverters
{
    public class DepictionCanvasModeToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DepictionCanvasMode)
            {
                var type = parameter.ToString();
                var screen = ((DepictionCanvasMode)value).ToString().ToLower();
                if (type.ToLower().Equals(screen))
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CurrentAppDepictionScreen.Welcome;
        }
    }
}