﻿namespace Depiction.ViewModels.ValueConverters
{
    public static class ViewModelConverters
    {
        public static readonly StartScreenTypeToVisibilityConverter StartScreenTypeToVisibility = new StartScreenTypeToVisibilityConverter();

        public static readonly ManageModalityToSelectionBoolConverter ManageModalityToSelectionBool = new ManageModalityToSelectionBoolConverter();
        public static readonly DepictionElementAdjustModeToBoolConverter DepictionElementAdjustModeToBool = new DepictionElementAdjustModeToBoolConverter();
        public static readonly DisplayerTypeToThumbZIndexConverter DisplayerTypeToThumbZIndex = new DisplayerTypeToThumbZIndexConverter();
        public static readonly DepictionCanvasModeToVisibilityConverter DepictionCanvasModeToVisibility = new DepictionCanvasModeToVisibilityConverter();
        public static readonly DepictionCanvasModeToBoolConverter DepictionCanvasModeToBool = new DepictionCanvasModeToBoolConverter();
        public static readonly DepictionAddModeToBoolConverter DepictionAddModeToBool = new DepictionAddModeToBoolConverter();
    }
}