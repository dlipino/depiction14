﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ValueConverters
{//This converter should eventually get killed off. Not needed because datatriggers can what this does (and do it better)
    public class StartScreenTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is CurrentAppDepictionScreen)
            {
                var type = parameter.ToString();
                var screen = ((CurrentAppDepictionScreen)value).ToString().ToLower();
                if (type.ToLower().Equals(screen))
                {
                    return Visibility.Visible;
                }
                if(type.Equals("general") && !screen.Equals("world"))
                {
                    return Visibility.Visible;
                }
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return CurrentAppDepictionScreen.Welcome;
        }
    }
}
