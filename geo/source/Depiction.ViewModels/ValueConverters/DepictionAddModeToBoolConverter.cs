﻿using System;
using System.Globalization;
using System.Windows.Data;
using Depiction.ViewModels.ViewModels.MapControlDialogViewModels;

namespace Depiction.ViewModels.ValueConverters
{
    public class DepictionAddModeToBoolConverter : IValueConverter
    {
        #region Implementation of IValueConverter

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var param = parameter.ToString();
            if (value.ToString().ToLowerInvariant().Equals(param.ToLowerInvariant()))
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var selected = (bool)value;
            var enumVal = Enum.Parse(targetType, parameter.ToString());
            if (selected)
            {
                return enumVal;
            }
            return AddContentMode.Unknown;
        }

        #endregion
    }
}