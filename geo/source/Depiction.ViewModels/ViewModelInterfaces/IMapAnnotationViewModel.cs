﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IMapAnnotationViewModel : INotifyPropertyChanged, IMapDraggable
    {
        SolidColorBrush BackgroundColor { get; set; }
        SolidColorBrush ForegroundColor { get; set; }
        string AnnotationText { get; set; }
        double TextBoxWidth { get;  }
        double IconSize { get; }
        Visibility AnnotationVisibility { get; set; }

        bool IsGettingEdited { get; set; }
        bool IsAnnotationExpanded { get; set; }
        void AdjustVisibilityWithScale(double currentScale);
        void AdjustWidth(double distance);
    }
}