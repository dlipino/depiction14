﻿using System.Windows;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IElementRevealerDisplayerViewModel : IElementDisplayerViewModel
    {
        new DepictionDisplayerType DisplayerType { get; set; }
        double Scale { get; set; }
        bool PermaTextVisible { get; set; }
        double MinContentLocationX { get; set; }
        double MinContentLocationY { get; set; }
        bool MouseByPassEnabled { get; set; }
        bool RevealerMaximized { get; set; }
        bool RevealerBorderVisible { get; set; }
        bool RevealerAnchored { get; set; }
        Geometry ClipArea { get; }
        Rect ClipBounds { get; }
        double PixelHeight { get; set; }
        double PixelWidth { get; set; }
        void SetClipBounds(Rect newBounds);
        TranslateTransform TopLeftTransform { get; }
        void SetTopLeft(double top, double left);
        double RevealerOpacity { get; set; }
    }
}