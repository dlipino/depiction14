﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.Interfaces;
using Depiction.API.Interfaces.ElementInterfaces;
using Depiction.ViewModels.ViewModelHelpers;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IElementDisplayerViewModel : INotifyPropertyChanged, IDisposable
    {
        event Action<IElementDisplayerViewModel, DepictionDisplayerType> DisplayerTypeChange;
        DepictionDisplayerType DisplayerType { get;  }
        IDepictionBaseDisplayer BaseDisplayerModel { get; }
        string ModelID { get; }
        string DisplayerName { get; set; }

        ReadOnlyCollection<string> ElementIDsInDisplayer { get; }
        ElementViewModelManager ViewModelManager{get;}

        void AddElementListToModel(List<IDepictionElement> elementToAdd);
        void RemoveElementListFromModel(List<IDepictionElement> elementToRemove);

    }
}