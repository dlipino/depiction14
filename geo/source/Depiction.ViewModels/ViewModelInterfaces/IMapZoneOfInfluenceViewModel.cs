﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;
using Depiction.API.CoreEnumAndStructs;
using Depiction.API.HelperObjects;

namespace Depiction.ViewModels.ViewModelInterfaces
{
    public interface IMapZoneOfInfluenceViewModel : INotifyPropertyChanged
    {
        List<EnhancedPointListWithChildren> ZOIToDraw { get; set; }
        Geometry ZOIDrawGeometry { get; }
        
        Brush FillBrush { get; }
        Brush BorderBrush { get; }
        double LineThickness { get; }

        void ShiftZOI(Point offset);
        bool AddPointToEnd(Point pointToAdd);
        bool AddVisualPointToEnd(Point pointToAdd,bool drawClosedGeometry);
        bool RemovePoint(Point pointToRemove);
        bool InsertPointBefore(Point pointToInsertBefore, Point newPoint);
        //Hacks for temp line thickness adjustment
        void UpdateLineThickness(double multiplier);
        DepictionGeometryType ZOIGeometryType { get; }
        ZOIShapeType BaseShapeType { get; }
    }
}