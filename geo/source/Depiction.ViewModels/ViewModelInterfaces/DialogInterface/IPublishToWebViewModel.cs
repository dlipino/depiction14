﻿using System;
using System.Windows.Input;

namespace Depiction.ViewModelNew.ViewModelInterfaces.DialogInterface
{
    public interface IPublishToWebViewModel
    {
        ICommand PublishToWebCommand{ get;}
        string Title { get; set; }
        string Description { get; set; }
        string Tags { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        event EventHandler PublishToWebCompleted;
    }
}