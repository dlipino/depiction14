﻿using System;
using System.IO;
using SSCProt;
namespace StartSoftwareShield
{
    class Program
    {

        #region SoftwareShield_ClientProtectorDefinitions
        // the ClientProtector server
        private static SSCProtectorClass SSCP;

       
        #endregion

        #region SoftwareShield_FPUControlWord_WorkAround
        /* /////////////////////////////////////////////////////////////
		 * This functionality may be required in some .NET applications 
		 * because the .NET framework expects the floating point control 
		 * word to mask all exceptions internally. However the 
		 * SoftwareShield COM servers expect a standard FPU exception mask. 
		 * 
		 * If you are having strange exceptions that disappear when you 
		 * comment out the creation of your SoftwareShield COM servers - 
		 * use the workaround shown in this module to get and reset the 
		 * default control word.  This should only have to be done 
		 * imemdiately after the COM servers constructtion call.
		 * 
		 * See the help on "Linking to your Application in C#" for more info.
		 * /////////////////////////////////////////////////////////////
		*/

        [System.Runtime.InteropServices.DllImport("msvcrt.dll")]
        public static extern int _controlfp(int IN_New, int IN_Mask);
        private static int DefaultCW;
        #endregion

        static string MainLicenseFileName = "licensev1-15.lic";
        const string MainLicenseFilePassword = "austrian_6_abode_._epitome";
        const string GlobalAuthorizationCodePassword = "featuring_CAMPANILE_2_newscast";
        const int FingerPrintOptionsCode = 7;
        private static string LicenseFolder;

        [STAThread]
        static void Main(string[] args)
        {
            if(args.Length!=1)
            {
                return;
            }
            LicenseFolder = args[0];
            MainLicenseFileName = args[0] + "\\" + MainLicenseFileName;
            //
            // Required for 
            // lets get the default control word
            DefaultCW = _controlfp(0, 0);
            //
            try
            {
                SSCP = new SSCProtectorClass();

                // restore the FPU control word .NET expects
                _controlfp(DefaultCW, 0xfffff);

            }
            catch (Exception)
            {
                return;
            }

            int return_code;
            const int debugFlags = 0;
            SSCP.StartUp(MainLicenseFileName, MainLicenseFilePassword,
                         GlobalAuthorizationCodePassword, FingerPrintOptionsCode,
                         debugFlags, out return_code);
            CheckActivationStatus();
        }

        private static void CheckActivationStatus()
        {

            string activationFileName = LicenseFolder + "\\Activation_log.txt";
            bool isAuthorized;

            // find out if the copy protection has been released for this system
            SSCP.IsSystemAuthorized(out isAuthorized);
            if(isAuthorized)
            {
                
                //create the activation_log.txt file
                //write the projString string to fileName
                StreamWriter sr = File.CreateText(activationFileName);
                sr.Write("Activated");
                sr.Close();
            }
            else//delete the file
            {
                File.Delete(activationFileName);
            }
            
        }
    }
}
