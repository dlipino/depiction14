// StartDepiction.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "winuser.h"
#include "StartDepiction.h"
#include <windows.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <time.h>
#include <windows.h>

using namespace std;

#define dotNet35	"Software\\Microsoft\\NET Framework Setup\\NDP\\v3.5"
#define depiction   "Software\\@COMPANY_NAME@\\@PRODUCT_NAME@"

#define dotNet30WPF "Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0\\Setup\\Windows Presentation Foundation"
#define dotNet30WCF "Software\\Microsoft\\NET Framework Setup\\NDP\\v3.0\\Setup\\Windows Communication Foundation"

#define MAX_LOADSTRING 100
HKEY hKey;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
PROCESS_INFORMATION pi = {0};
PROCESS_INFORMATION pi_2 = {0};

int dayarray[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};  
int	DaysInMonth ;

SYSTEMTIME	st;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK		EnumChildProc(HWND WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_STARTDEPICTION, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_STARTDEPICTION));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_STARTDEPICTION));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_STARTDEPICTION);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_STARTDEPICTION));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
	typedef struct 
	{
		DWORD ProcessId;
		BOOL hWndFound;
	}
	PID_WNDHANDLEFOUND;

bool isLeapYear(int year)
{
    bool isLeap = false;
    if((year%4)==0)
    {
        isLeap=true;
        if((year%100)==0 && year > 1582 && (year%400)!=0)
			isLeap=false;
    }
	return isLeap;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   BOOL bSuccess;
   HWND hWnd, dialog;
   DWORD resultCode;
   DWORD SPresult, result_size;
   DWORD TargetDate, DayOfLastUpdateCheck;
   DWORD SilentUpdate, UpdaterCycleTime ;

   // DO NOT EDIT THESE LINES. They are modified by nant.
   BOOL IS_TRIAL = false; //This should be false
   // END OF NANT STUFF

   //  void GetProcessID(LPCTSTR pProcessName, std::vector<DWORD>& SetOfPID);
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, SW_HIDE);  // SW_HIDE is the magic!!
   UpdateWindow(hWnd);

   bool Result_Net3Point5 = RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT(dotNet35), 0, KEY_READ, &hKey) == ERROR_SUCCESS;

   SPresult = 0;
   if(Result_Net3Point5)
	   RegQueryValueEx(hKey, _T("SP"), NULL, NULL, (BYTE *) &SPresult,&result_size);
   RegCloseKey(hKey);

   bool Result_WPF = RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT(dotNet30WPF), 0, KEY_READ, &hKey) == ERROR_SUCCESS;
   RegCloseKey(hKey);

   bool Result_WCF = RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT(dotNet30WCF), 0, KEY_READ, &hKey) == ERROR_SUCCESS;
   RegCloseKey(hKey);

   if ( SPresult < 1 || (!Result_WPF && !Result_WCF) ){
	if ( SPresult < 1 ) 
//    dialog = CreateDialogW(GetModuleHandle(NULL), 
//							MAKEINTRESOURCE(IDD_DIALOG2), 
//							NULL, 
//                            NULL); 

//    ShowWindow(dialog, SW_SHOW); 

		 MessageBoxW(NULL, TEXT("In order for @PRODUCT_NAME@ to run on your computer\r\n\
Microsoft .NET Framework 3.5 SP1 must be installed. Either\r\n\r\n\
1) it is occuring presently and needs to complete before starting @PRODUCT_NAME@ or\r\n\
2) go to the following site for information regarding the installation of Microsoft\r\n    .NET Framework 3.5 SP1\r\n\n\
    http://www.depiction.com/net-35-installation-instructions \r\n\r\n\r\n"),TEXT("@PRODUCT_NAME@"),MB_OK);
	
     if (!Result_WPF && !Result_WCF)
		MessageBoxW(NULL, TEXT("In order for @PRODUCT_NAME@ to run on your computer\r\n\
Microsoft .NET Framework 3.0 must be enabled. To do that\r\n\r\n\
1) Go to Control Panel (Classic View) and select Program and Features\r\n\
2) Select 'Turn Windows features on or off' on the left hand panel\r\n\
3) Check the box 'Microsoft .NET Framework 3.0' and click OK\r\n\r\n\
When .NET 3.0 has finished configuring, @PRODUCT_NAME@ will be able to run successfully on your computer."),TEXT("@PRODUCT_NAME@"),MB_OK);
   }
   else
   {
	if (!IS_TRIAL) {	
		GetSystemTime(&st);
// Do we have a registry entry?
		bool Result_Depiction = RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT(depiction), 0, KEY_QUERY_VALUE | KEY_SET_VALUE, &hKey) == ERROR_SUCCESS;

// If yes, get the last day that the updater was called 
		if (Result_Depiction) {
			resultCode = RegQueryValueEx(hKey, _T("DayOfLastUpdateCheck"), NULL, NULL, (BYTE *) &DayOfLastUpdateCheck, &result_size);

			if (isLeapYear(st.wYear) && st.wMonth == 2)
				DaysInMonth = dayarray[st.wMonth] + 1 ;
			else
				DaysInMonth = dayarray[st.wMonth];

			UpdaterCycleTime = 7; // incase the registry query fails
			resultCode = RegQueryValueEx(hKey, _T("UpdaterCycleTime"), NULL, NULL, (BYTE *) &UpdaterCycleTime, &result_size);
	
			TargetDate = DayOfLastUpdateCheck + UpdaterCycleTime ;
			if (TargetDate > DaysInMonth)
				if (st.wMonth == 1) 	
					TargetDate -= dayarray[12];
				else
					TargetDate -= dayarray[st.wMonth-1];
	
			if ( st.wDay >= TargetDate) {
				resultCode = RegQueryValueEx(hKey, _T("SilentUpdate"), NULL, NULL, (BYTE *) &SilentUpdate, &result_size);
				SilentUpdate = 1 ;
				resultCode = RegSetValueEx(hKey, _T("SilentUpdate"), NULL, REG_DWORD, (BYTE *) &SilentUpdate, result_size);

				STARTUPINFO si_2 = {0};
				bSuccess = ::CreateProcess(_T("@PRODUCT_NAME@Updater.exe"),NULL,NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si_2,&pi_2);
			}
		}
		RegCloseKey(hKey);
	} //(!IS_TRIAL)
//	
//	Now call Depiction and persist the dialog box until Depiction comes up

	STARTUPINFO si ={0};
	si.cb = sizeof(si);  

	int	trial = 0;
	int	maxStartAttempts = 60;  //Sniff out Depiction.exe attempts (First loop - 30 secs, Second loop - 90 secs)

	dialog = CreateDialogW(GetModuleHandle(NULL), 
							MAKEINTRESOURCE(IDD_DIALOG1), 
							NULL, 
                            NULL); 

    ShowWindow(dialog, SW_SHOW); 

//	BOOL bSuccess = ::CreateProcess(_T("C:\\WINDOWS\\notepad.exe"),NULL,NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi);
	bSuccess = ::CreateProcess(_T("@PRODUCT_NAME@.exe"),NULL,NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi);
 
	HWND dskHandle = GetDesktopWindow(); 

	PID_WNDHANDLEFOUND myData;
	myData.hWndFound = FALSE;
	myData.ProcessId = pi.dwProcessId; 

	BOOL bReturn = false;

	while(pi.dwProcessId == 0 && trial++ < maxStartAttempts)
	{
		Sleep(500); // Sleep for 1/2 second
	}

	if ( pi.dwProcessId != 0 )
	{
		trial = 0;
		while( myData.hWndFound == FALSE && trial++ < maxStartAttempts)
		{
			bReturn = EnumChildWindows(dskHandle,EnumChildProc,(LPARAM)&myData);
			Sleep(1000); // Sleep for 1. second
		}
	}
	DestroyWindow(dialog);  // Then destroy our little message

	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

   }

    exit(0);

}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL CALLBACK EnumChildProc(HWND hwndChild, LPARAM lParam) 
{ 
	PID_WNDHANDLEFOUND* pData = (PID_WNDHANDLEFOUND*)lParam;
	
	DWORD processId = NULL;
	DWORD result;
	
	result = GetWindowThreadProcessId(hwndChild, &processId );
	
	if ( processId == pData->ProcessId ) {
		pData->hWndFound = TRUE;
		return FALSE;
	}
	else
		return TRUE;  // must return TRUE in order to iterate
}
